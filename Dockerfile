ARG PYTHON_VERSION
FROM python:$PYTHON_VERSION

COPY ./ /home/
WORKDIR /home

#RUN apt-get update && \
#    apt-get upgrade -y && \
#    apt-get install -y libopenmpi-dev

RUN pip install --upgrade pip && \
    pip install .

CMD ["tmap-run-tests", "--ttype", "serial", "--failfast"]