#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/ndarrayobject.h>
#include <numpy/ndarraytypes.h>

// Additional imports here if needed
// ....
#include <random>

#if PY_MAJOR_VERSION >= 3
    #define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#else
    #define MOD_INIT(name) PyMODINIT_FUNC init##name(void)
#endif

// Personalised errors
static PyObject *XunError;

// Personalised functions
static PyObject * Py_sum_const_vec(PyObject *self, PyObject *args);
static PyObject * Py_sum_const_mat(PyObject *self, PyObject *args);
static PyObject * Py_weird_samples(PyObject *self, PyObject *args);

// List of functions visible in the module
static PyMethodDef XunMethods[] = {
  {"sum_const_vec", Py_sum_const_vec, METH_VARARGS, "Sum constant to vector. Not in-place"},
  {"sum_const_mat", Py_sum_const_mat, METH_VARARGS, "Sum constant to matrix. Not in-place"},
  {"weird_samples", Py_weird_samples, METH_VARARGS, "Get n weird samples"},
  {NULL, NULL, 0, NULL}        /* Sentinel */
};

/* Init Package function definition */
MOD_INIT(xun)
{
    PyObject *m;

#if PY_MAJOR_VERSION >= 3
    static struct PyModuleDef moduledef = {
      PyModuleDef_HEAD_INIT,
      "xun",           /* m_name */
      "The amazing Xun module",  /* m_doc */
      -1,                  /* m_size */
      XunMethods,          /* m_methods */
      NULL,                /* m_reload */
      NULL,                /* m_traverse */
      NULL,                /* m_clear */
      NULL,                /* m_free */
    };
#endif
    
#if PY_MAJOR_VERSION >= 3
    m = PyModule_Create(&moduledef);
    if (m == NULL) return NULL;
#else
    m = Py_InitModule3("xun", XunMethods, "The amazing Xun module");
    if (m == NULL) return;
#endif

    import_array();

    XunError = PyErr_NewException(const_cast<char *>("xun.error"), NULL, NULL);
    Py_INCREF(XunError);
    PyModule_AddObject(m, "error", XunError);

#if PY_MAJOR_VERSION >= 3
    return m;
#endif
}

//
// FUNCTION DEFINITIONS
//
static PyObject *
Py_sum_const_vec(PyObject *self, PyObject *args)
{
  /* VARIABLES */
  // INPUT
  double c;
  PyArrayObject *py_a;    // NumPy array
  npy_intp dims_a[1];     // Sizes of the array
  int ndim_a = 1;         // Number of dimensions of the array
  double* a;              // C array
  // OUTPUT
  PyArrayObject *py_out;  // NumPy array
  double* out;            // C array
  
  // PARSE ARGUMENTS
  // Get a double and an array as an input
  if (!PyArg_ParseTuple(args, "dO!", &c, &PyArray_Type, &py_a))
    return NULL;
  if (NULL == py_a) return NULL;
  
  // CONVERT INPUT 
  // Check that the array is double
  if (PyArray_DESCR(py_a)->type_num != NPY_DOUBLE){
    PyErr_SetString(PyExc_ValueError,
                    "Array must be of type double.");
    return NULL; }
  // Check that the array is 1d
  if (PyArray_NDIM(py_a) != ndim_a){
    PyErr_SetString(PyExc_ValueError,
                    "Array must be 1 dimensional.");
    return NULL; }
  // Get the data and its dimensions
  PyArray_Descr *descr_a = PyArray_DescrFromType(NPY_DOUBLE);
  Py_INCREF(py_a);
  PyArray_AsCArray((PyObject **) &py_a, (void **) &a, dims_a, ndim_a, descr_a);

  // ALLOCATE OUTPUT
  py_out = (PyArrayObject *)PyArray_SimpleNew(ndim_a, dims_a, NPY_DOUBLE);

  // Get the output data and its dimensions
  Py_INCREF(py_out);
  PyArray_AsCArray((PyObject **) &py_out, (void **) &out, dims_a, ndim_a, descr_a);
  
  // PERFORM SUMMATION (just stupid iteration..)
  for( int i=0; i<dims_a[0]; i++ ){
    out[i] = c + a[i];
  }
  
  // Free C-like arrays
  PyArray_Free((PyObject *) py_a, (void *) a);
  PyArray_Free((PyObject *) py_out, (void *) out);

  // Return the NumPy Array
  return Py_BuildValue("N", py_out);
}

static PyObject *
Py_sum_const_mat(PyObject *self, PyObject *args)
{
  /* VARIABLES */
  // INPUT
  double c;
  PyArrayObject *py_a;    // NumPy array
  npy_intp dims_a[2];     // Sizes of the array
  int ndim_a = 2;         // Number of dimensions of the array
  double** a;              // C array
  // OUTPUT
  PyArrayObject *py_out;  // NumPy array
  double** out;            // C array
  
  // PARSE ARGUMENTS
  // Get a double and an array as an input
  if (!PyArg_ParseTuple(args, "dO!", &c, &PyArray_Type, &py_a))
    return NULL;
  if (NULL == py_a) return NULL;
  
  // CONVERT INPUT 
  // Check that the array is double
  if (PyArray_DESCR(py_a)->type_num != NPY_DOUBLE){
    PyErr_SetString(PyExc_ValueError,
                    "Array must be of type double.");
    return NULL; }
  // Check that the array is 1d
  if (PyArray_NDIM(py_a) != ndim_a){
    PyErr_SetString(PyExc_ValueError,
                    "Array must be 1 dimensional.");
    return NULL; }
  // Get the data and its dimensions
  PyArray_Descr *descr_a = PyArray_DescrFromType(NPY_DOUBLE);
  Py_INCREF(py_a);
  PyArray_AsCArray((PyObject **) &py_a, (void ***) &a, dims_a, ndim_a, descr_a);

  // ALLOCATE OUTPUT
  py_out = (PyArrayObject *)PyArray_SimpleNew(ndim_a, dims_a, NPY_DOUBLE);

  // Get the output data and its dimensions
  Py_INCREF(py_out);
  PyArray_AsCArray((PyObject **) &py_out, (void ***) &out, dims_a, ndim_a, descr_a);
  
  // Perform summation (just stupid iteration..)
  for(int i=0; i<dims_a[0]; i++){
    for(int j=0; j<dims_a[1]; j++){
      out[i][j] = c + a[i][j];
    }
  }
  
  // Free C-like arrays
  PyArray_Free((PyObject *) py_a, (void *) a);
  PyArray_Free((PyObject *) py_out, (void *) out);

  // Return the NumPy Array
  return Py_BuildValue("N", py_out);
}

static PyObject *
Py_weird_samples(PyObject *self, PyObject *args)
{
  /* VARIABLES */
  // INPUT
  int n;
  // OUTPUT
  int ndim = 2;            // 2d output array shape (n,1)
  npy_intp dims[2];        // Shape of the array
  PyArrayObject *py_out;   // NumPy array
  double** out;            // C array
  
  // PARSE ARGUMENTS
  // Get a double and an array as an input
  if (!PyArg_ParseTuple(args, "i", &n))
    return NULL;

  // ALLOCATE OUTPUT
  dims[0] = n;
  dims[1] = 1;
  py_out = (PyArrayObject *)PyArray_SimpleNew(ndim, dims, NPY_DOUBLE);
  // Get the output data and its dimensions
  PyArray_Descr *descr = PyArray_DescrFromType(NPY_DOUBLE);
  Py_INCREF(py_out);
  PyArray_AsCArray((PyObject **) &py_out, (void ***) &out, dims, ndim, descr);
  
  // PERFORM SAMPLING
  // Get n samples from the Weibull distribution
  // http://www.cplusplus.com/reference/random/weibull_distribution/
  std::default_random_engine generator;
  std::weibull_distribution<double> distribution(2.0,4.0);
  for(int i=0; i<n; i++){
    out[i][0] = distribution(generator);
  }

  // Free C-like arrays
  PyArray_Free((PyObject *) py_out, (void *) out);

  // Return the NumPy Array
  return Py_BuildValue("N", py_out);
}

