#!/bin/bash

# CMD=pudb3
CMD=python
# CMD="sbatch submit_slurm.sh python"
NPROCS=80

DSET=DurbinData
N=945
DIST="Sigma0.25-${N}obs"
NHYPER=2
SAVE_EVERY=1
BUILD_OPTS="--email=dabi@mit.edu"

DO_TRIM=1  # Do var diag, smoothing and mcmc for progressively trimmed distribution
TRIM_NUM=3
TRIM_DEN=5
# TRIM_STEPS=(945)
TRIM_STEPS=(74)
# TRIM_STEPS=(567 340 204 122 73 43 25 15 9 5 3)
# TRIM_STEPS=(5 3)

ORDERS=(7) #(1 2 3 5) 
SPAN=total,total,total,total
QTYPE=3
# QNUM_LIST=(10000 100000 100000)
DELTA_QNUM=(9) #(8) #(5 5 2) #(5 5 5 4)
BATCH_SIZES=(1e12) #(10000 10000 10000 10000 10000)
ADAPT=tol-sequential
ADAPT_TOL=1e-4
MONIT_QTYPE=3
MONIT_QNUM_LIST=(20) #(300000)
# DELTA_MONIT_QNUM=2
TOL=1e-3 # 1e-4
DERS=2
REG_LIST=0,1e-7,1e-7,1e-6,1e-6,1e-5,1e-5
# REG_LIST=0,1e-5,5e-5,1e-4,5e-4,1e-3,1e-3
# REG_LIST=0,1e-5,1e-5,5e-5,1e-4,5e-4,5e-4 

HSPAN=total
POW_HORD=1
DELTA_HQNUM=13
DELTA_HMONIT_QNUM=5
HADAPT=none # tol-sequential
HTOL_ADAPT=1e-3
HTOL=1e-6
HREG_LIST=(1e-10) #(1e-5) 

VAR_DIAG_QNUM=100000
VAR_DIAG_TIMES=10
MCMC_BASE=0
MCMC_INCREMENT=10000
MCMC_TIMES=10
MCMC_OPT="--mcmc-skip=10 --mcmc-ess-skip=1"
MC_INCREMENT=100000
MC_TIMES=1

# echo "BUILDING APPROXIMATIONS"
# echo ""
# # echo "Starting Laplace approximation"
# # tmap-laplace \
# #   --dist=data/${DSET}/Distributions/${DIST}/Distribution.dill \
# #   --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
# #   --tol=1e-4 -f --log=20

# for idx in ${!ORDERS[*]}
# do
#     echo ""
#     echo "Starting order: ${ORDERS[$idx]}"
#     # QNUM=${QNUM_LIST[$idx]}
#     QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
#     MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
#     # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
#     # HORDER=1
#     HORDER=$((${ORDERS[$idx]}**$POW_HORD))
#     HQNUM=$(($HORDER+$DELTA_HQNUM))
#     HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
#     # REG=${REG_LIST[$idx]}
#     HREG=${HREG_LIST[$idx]}
#     $CMD ../../../scripts/tmap-sequential-tm \
#         --dist=data/${DSET}/Distributions/${DIST}/Distribution.dill \
#         --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
#         --mtype=intsq --span=$SPAN --btype=poly --order=${ORDERS[$idx]} --qtype=${QTYPE} --qnum=${QNUM},${QNUM},${QNUM},${QNUM} \
#         --ders=${DERS} --tol=${TOL} --reg=${REG_LIST} --maxit=1000 \
#         --adapt=$ADAPT --adapt-tol=${ADAPT_TOL} --continue-on-error \
#         --monitor-qtype=${MONIT_QTYPE} \
#         --monitor-qnum=${MONIT_QNUM},${MONIT_QNUM},${MONIT_QNUM},${MONIT_QNUM} \
#         --hyper-mtype=intsq --hyper-span=${HSPAN} --hyper-btype=poly --hyper-order=${HORDER} \
#         --hyper-qtype=3 --hyper-qnum=${HQNUM},${HQNUM} \
#         --hyper-tol=${HTOL} --hyper-reg=${HREG} --hyper-maxit=1000 \
#         --hyper-adapt=${HADAPT} --hyper-adapt-tol=${HTOL_ADAPT} \
#         --hyper-monitor-qtype=3 --hyper-monitor-qnum=${HMONIT_QNUM},${HMONIT_QNUM} \
#         --nprocs=${NPROCS} --batch=${BATCH_SIZES[$idx]} \
#         -v --reload --safe-mode=${SAVE_EVERY} ${BUILD_OPTS}
# done

# echo ""
# echo "-------------------------------------------------------"
# echo "VARIANCE DIAGNOSTICS"
# # echo ""
# # echo "Starting Laplace approximation"
# # for n in `seq 1 $VAR_DIAG_TIMES`;
# # do
# #     VAR_DIAG_SAMPLES=$(($n*$VAR_DIAG_QNUM))
# #     tmap-postprocess \
# #         --input=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
# #         --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4-POST.dill \
# #         --var-diag=exact-base --var-diag-qtype=0 --var-diag-qnum=${VAR_DIAG_SAMPLES} -v
# # done

# for idx in ${!ORDERS[*]}
# do
#     echo ""
#     echo "Starting order: ${ORDERS[$idx]}"
#     # QNUM=${QNUM_LIST[$idx]}
#     QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
#     MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
#     # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
#     # HORDER=1
#     HORDER=$((${ORDERS[$idx]}**$POW_HORD))
#     HQNUM=$(($HORDER+$DELTA_HQNUM))
#     HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
#     # REG=${REG_LIST[$idx]}
#     HREG=${HREG_LIST[$idx]}
    
#     for n in `seq 1 $VAR_DIAG_TIMES`;
#     do
#         VAR_DIAG_SAMPLES=$(($n*$VAR_DIAG_QNUM))
# 	$CMD ../../../scripts/tmap-postprocess \
#             --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
#             --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
#             --var-diag=exact-base --var-diag-qtype=0 --var-diag-qnum=${VAR_DIAG_SAMPLES} \
#             -v --nprocs=${NPROCS}
#     done
# done

echo ""
echo "-------------------------------------------------------"
echo "RANDOM PULLBACK CONDITIONALS"
# echo ""
# echo "Starting Laplace approximation"
# for n in `seq 1 $VAR_DIAG_TIMES`;
# do
#     VAR_DIAG_SAMPLES=$(($n*$VAR_DIAG_QNUM))
#     tmap-postprocess \
#         --input=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
#         --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4-POST.dill \
#         --var-diag=exact-base --var-diag-qtype=0 --var-diag-qnum=${VAR_DIAG_SAMPLES} -v
# done

for idx in ${!ORDERS[*]}
do
    echo ""
    echo "Starting order: ${ORDERS[$idx]}"
    # QNUM=${QNUM_LIST[$idx]}
    QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
    MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
    # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
    # HORDER=1
    HORDER=$((${ORDERS[$idx]}**$POW_HORD))
    HQNUM=$(($HORDER+$DELTA_HQNUM))
    HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
    # REG=${REG_LIST[$idx]}
    HREG=${HREG_LIST[$idx]}

    $CMD ../../../scripts/tmap-postprocess \
         --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
         --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
         --random-conditionals=approx-base --rndc-n-points-x-ax=20 \
	 --rndc-n-plots-x-ax=6 \
         --no-plotting -v --nprocs=${NPROCS}
done

# echo ""
# echo "---------------------------------------------------"
# echo "METROPOLIS HASTINGS WITH INDEPENDENT PROPOSALS"
# # echo ""
# # echo "Starting Laplace approximation"
# # for n in `seq 1 $MCMC_TIMES`;
# #     do
# #     MCMC_SAMPLES=$(($n*$MCMC_INCREMENT+$MCMC_BASE))
# #     $CMD ../../../scripts/tmap-postprocess \
# #         --input=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
# #         --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4-POST.dill \
# #         --mcmc=mhind --mcmc-samples=${MCMC_SAMPLES} ${MCMC_OPT} \
# #         -v --log=20 --no-plotting
# # done

# for idx in ${!ORDERS[*]}
# do
#     echo ""
#     echo "Starting order: ${ORDERS[$idx]}"
#     # QNUM=${QNUM_LIST[$idx]}
#     QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
#     MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
#     # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
#     # HORDER=1
#     HORDER=$((${ORDERS[$idx]}**$POW_HORD))
#     HQNUM=$(($HORDER+$DELTA_HQNUM))
#     HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
#     # REG=${REG_LIST[$idx]}
#     HREG=${HREG_LIST[$idx]}
#     for n in `seq 1 $MCMC_TIMES`;
#     do
#         MCMC_SAMPLES=$(($n*$MCMC_INCREMENT+$MCMC_BASE))
#         $CMD ../../../scripts/tmap-postprocess \
#             --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
#             --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
#             --mcmc=mhind --mcmc-samples=${MCMC_SAMPLES} ${MCMC_OPT} \
#             -v --log=20 --nprocs=${NPROCS} #--no-plotting
#     done
# done

# echo ""
# echo "---------------------------------------------------"
# echo "MONTE CARLO -- SMOOTHING MARGINAL"
# # echo ""
# # echo "Starting Laplace approximation"
# # tmap-postprocess \
# #   --input=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
# #   --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4-POST.dill \
# #   --quadrature=approx-target --quadrature-qtype=0 \
# #   --quadrature-qnum=${MC_SAMPLES} -v --log=20 --no-plotting

# for idx in ${!ORDERS[*]}
# do
#     echo ""
#     echo "Starting order: ${ORDERS[$idx]}"
#     # QNUM=${QNUM_LIST[$idx]}
#     QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
#     MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
#     # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
#     # HORDER=1
#     HORDER=$((${ORDERS[$idx]}**$POW_HORD))
#     HQNUM=$(($HORDER+$DELTA_HQNUM))
#     HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
#     # REG=${REG_LIST[$idx]}
#     HREG=${HREG_LIST[$idx]}

#     for n in `seq 1 $MC_TIMES`;
#     do
#         MC_SAMPLES=$(($n*$MC_INCREMENT))
#         $CMD ../../../scripts/tmap-postprocess \
#             --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
#             --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
#             --quadrature=approx-target --quadrature-qtype=0 \
#             --quadrature-qnum=${MC_SAMPLES} -v --log=20 --nprocs=${NPROCS} --no-plotting
#     done
# done

# echo ""
# echo "---------------------------------------------------"
# echo "MONTE CARLO -- FILTERING MARGINAL"

# for idx in ${!ORDERS[*]}
# do
#     echo ""
#     echo "Starting order: ${ORDERS[$idx]}"
#     # QNUM=${QNUM_LIST[$idx]}
#     QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
#     MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
#     # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
#     # HORDER=1
#     HORDER=$((${ORDERS[$idx]}**$POW_HORD))
#     HQNUM=$(($HORDER+$DELTA_HQNUM))
#     HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
#     # REG=${REG_LIST[$idx]}
#     HREG=${HREG_LIST[$idx]}
#     $CMD ../../../scripts/tmap-sequential-postprocess \
#         --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
#         --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
#         --filtering-quadrature --filt-quad-qtype=0 \
#         --filt-quad-qnum=${MC_SAMPLES} -v --log=20 --no-plotting \
#         --nprocs=${NPROCS}
# done

# TRIMMED ANALYSIS
NTRIM=$N
if [ $DO_TRIM -eq 1 ]; then
    for NTRIM in "${TRIM_STEPS[@]}"; do
    # while [ $NTRIM -gt 1 ]; do
        echo ""
        echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo "                 TRIM = ${NTRIM}"

        echo ""
        echo "-------------------------------------------------------"
        echo "VARIANCE DIAGNOSTICS"
        for idx in ${!ORDERS[*]}
        do
            echo ""
            echo "Starting order: ${ORDERS[$idx]}"
            # QNUM=${QNUM_LIST[$idx]}
            QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
            MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
            # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
            # HORDER=1
            HORDER=$((${ORDERS[$idx]}**$POW_HORD))
            HQNUM=$(($HORDER+$DELTA_HQNUM))
            HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
            # REG=${REG_LIST[$idx]}
            HREG=${HREG_LIST[$idx]}
            
            for n in `seq 1 $VAR_DIAG_TIMES`;
            do
                VAR_DIAG_SAMPLES=$(($n*$VAR_DIAG_QNUM))
                $CMD ../../../scripts/tmap-sequential-postprocess \
                    --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
                    --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
                    --trim=${NTRIM} \
                    --var-diag=exact-base --var-diag-qtype=0 --var-diag-qnum=${VAR_DIAG_SAMPLES} \
                    -v --nprocs=${NPROCS}
            done
        done

        echo ""
        echo "---------------------------------------------------"
        echo "METROPOLIS HASTINGS WITH INDEPENDENT PROPOSALS"
        for idx in ${!ORDERS[*]}
        do
            echo ""
            echo "Starting order: ${ORDERS[$idx]}"
            # QNUM=${QNUM_LIST[$idx]}
            QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
            MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
            # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
            # HORDER=1
            HORDER=$((${ORDERS[$idx]}**$POW_HORD))
            HQNUM=$(($HORDER+$DELTA_HQNUM))
            HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
            # REG=${REG_LIST[$idx]}
            HREG=${HREG_LIST[$idx]}
            for n in `seq 1 $MCMC_TIMES`;
            do
                MCMC_SAMPLES=$(($n*$MCMC_INCREMENT+$MCMC_BASE))
                $CMD ../../../scripts/tmap-sequential-postprocess \
                    --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
                    --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
                    --trim=${NTRIM} \
                    --mcmc=mhind --mcmc-samples=${MCMC_SAMPLES} ${MCMC_OPT} \
                    -v --log=20 --nprocs=${NPROCS} #--no-plotting
            done
        done

        # echo ""
        # echo "---------------------------------------------------"
        # echo "MONTE CARLO -- SMOOTHING MARGINAL"
        # for idx in ${!ORDERS[*]}
        # do
        #     echo ""
        #     echo "Starting order: ${ORDERS[$idx]}"
        #     # QNUM=${QNUM_LIST[$idx]}
        #     QNUM=$((${ORDERS[$idx]}+${DELTA_QNUM[$idx]}))
        #     MONIT_QNUM=${MONIT_QNUM_LIST[$idx]}
        #     # MONIT_QNUM=$(($QNUM+$DELTA_MONIT_QNUM))
        #     # HORDER=1
        #     HORDER=$((${ORDERS[$idx]}**$POW_HORD))
        #     HQNUM=$(($HORDER+$DELTA_HQNUM))
        #     HMONIT_QNUM=$(($HQNUM+$DELTA_HMONIT_QNUM))
        #     # REG=${REG_LIST[$idx]}
        #     HREG=${HREG_LIST[$idx]}

        #     for n in `seq 1 $MC_TIMES`;
        #     do
        #         MC_SAMPLES=$(($n*$MC_INCREMENT))
        #         $CMD ../../../scripts/tmap-sequential-postprocess \
        #             --input=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}.dill \
        #             --output=data/${DSET}/Distributions/${DIST}/Seq-intsq-${SPAN}-poly-o${ORDERS[$idx]}-q${QTYPE}-qn${QNUM}-tol${TOL}-reg${REG_LIST}-${ADAPT}-${ADAPT_TOL}-hintsq-h${HSPAN}-hpoly-ho${HORDER}-hq3-hqn${HQNUM}-htol${HTOL}-hreg${HREG}-${HADAPT}-${HTOL_ADAPT}-POST.dill \
        #             --trim=${NTRIM} \
        #             --quadrature=approx-target --quadrature-qtype=0 \
        #             --quadrature-qnum=${MC_SAMPLES} -v --log=20
	# 	    --nprocs=${NPROCS} --no-plotting
        #     done
        # done

        # # Update trim number
        # NTRIM=$((NTRIM*TRIM_NUM/TRIM_DEN))
    done

fi
