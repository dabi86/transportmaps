#!/bin/bash
#SBATCH --job-name=tm-unit-tests
#SBATCH --workdir=/master/home/dabi/VC-Projects/Software/Mine/Current/transport-maps/Examples/cluster/voyager/
#SBATCH --output=job.%J.out
#SBATCH --error=job.%J.err
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --mail-user=dabi@mit.edu

mpirun $*
