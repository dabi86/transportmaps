import matplotlib.pyplot as plt
import numpy as np

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions.Examples.BiochemicalOxygenDemand as BOD

TM.setLogLevel(20)

# Prior ranges (could be retrievered from pi)
amin = 0.4
amax = 1.2
bmin = 0.01
bmax = 0.31

# Increase nobs to increase dimension
nobs = 2
sig2 = 1e-3
times = np.arange(1,nobs+1)
pi = BOD.JointDistribution(times, sig2)

# Construct the joint whitening map
T_params = Maps.CompositeMap(
    Maps.FrozenLinearDiagonalTransportMap(
        np.array([amin, bmin]),
        np.array([amax-amin, bmax-bmin]) ),
    Maps.FrozenGaussianToUniformDiagonalTransportMap(2) )
I = Maps.IdentityTransportMap(nobs)
JTW = Maps.TriangularListStackedTransportMap(
    [I, T_params], [ [i for i in range(nobs)] , [nobs, nobs+1]])

# Pullback pi
pull_JTW_pi = DIST.PullBackTransportMapDistribution(JTW, pi)

# Define map (no exact map)
order = 2
T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
    pi.dim, order, 'full')

# Setup problem
rho = DIST.StandardNormalDistribution(pi.dim)
push_rho = DIST.PushForwardTransportMapDistribution(T, rho)

# Solve
qtype = 3                # Gauss quadrature
qparams = [5] * pi.dim  # Quadrature order
reg = None               # No regularization
tol = 1e-5               # Optimization tolerance
ders = 2                 # Use gradient and Hessian
log = push_rho.minimize_kl_divergence(
    pull_JTW_pi, qtype=qtype, qparams=qparams, regularization=reg,
    tol=tol, ders=ders)

# Plot pullback (should be Gaussian)
pull_T_JTW_pi = DIST.PullBackTransportMapDistribution(T, pull_JTW_pi)
DIAG.plotAlignedConditionals(pull_T_JTW_pi)

