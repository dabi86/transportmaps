import matplotlib.pyplot as plt
import numpy as np

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

TM.setLogLevel(20)

# Define distribution
a = 1.
b = 1.
mu = np.zeros(2)
sigma2 = np.array([[1., 0.9],[0.9, 1.]])
pi = DIST.BananaDistribution(a, b, mu, sigma2)

# Define map (order 2 should be exact)
order = 2
T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
    2, order, 'full')

# Setup problem
rho = DIST.StandardNormalDistribution(2)
push_rho = DIST.PushForwardTransportMapDistribution(T, rho)

# Solve
qtype = 3           # Gauss quadrature
qparams = [10] * 2  # Quadrature order
reg = None          # No regularization
tol = 1e-5         # Optimization tolerance
ders = 2            # Use gradient and Hessian
log = push_rho.minimize_kl_divergence(
    pi, qtype=qtype, qparams=qparams, regularization=reg,
    tol=tol, ders=ders)

# Plot pullback (should be Gaussian)
pull_pi = DIST.PullBackTransportMapDistribution(T, pi)
DIAG.plotAlignedConditionals(pull_pi, do_diag=False)

# Plot pushforward
x = np.linspace(-4,4,50)
y = np.linspace(-9,3,50)
xx,yy = np.meshgrid(x,y)
X2d = np.vstack( (xx.flatten(),yy.flatten()) ).T
pdf2d = pi.pdf(X2d).reshape(xx.shape)
levels_pdf2d = np.linspace(np.min(pdf2d),np.max(pdf2d),10)
approx_pdf = push_rho.pdf(X2d).reshape(xx.shape)
plt.figure()
plt.contour(xx, yy, pdf2d, levels=levels_pdf2d);
plt.contour(xx, yy, approx_pdf, linestyles='dashed', levels=levels_pdf2d);
plt.show(False)

# Pushforward quadratures
(xq,wq) = push_rho.quadrature(qtype=3, qparams=[2,2])
plt.figure()
plt.contour(xx, yy, pdf2d);
plt.scatter(xq[:,0], xq[:,1], c='k', s=50.*wq+10.);
plt.show(False)