import copy

import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

import numpy as np
import numpy.random as npr

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Algorithms.Adaptivity as ALGADAPT
import TransportMaps.Diagnostics as DIAG
from TransportMaps.Distributions.Examples import FactorizedBananaDistribution

params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)
figfmt_list = ['svg', 'pdf', 'eps', 'png']

npr.seed(1)

TM.setLogLevel(10)

mux = 0.5
sigma2x = 0.8
sigma2y = 0.2
pi = FactorizedBananaDistribution(
    mu0=mux, sigma0=np.sqrt(sigma2x), sigma1=np.sqrt(sigma2y))
pi_copy = copy.deepcopy(pi)

rho = DIST.StandardNormalDistribution(dim=2)

fig,ax = DIAG.plotAlignedConditionals(
    pi_copy, do_diag=False, show_axis=False, numPointsXax=60)
for figfmt in figfmt_list:
    fig.savefig('banana.' + figfmt)    

tm = TM.Default_IsotropicIntegratedSquaredDiagonalTransportMap(dim=2, order=1)

solve_params = {
    'qtype': 0,
    'qparams': {'n_samps' : 1000}
}
validator = DIAG.SampleAverageApproximationKLMinimizationValidator(
    eps=1e-2, eps_abs=1e-2,max_nsamps=20000, lower_n=10)
# solve_params = {
#     'qtype': 3,
#     'qparams': [6,6]
# }
# validator = None

builder = ALGADAPT.FirstVariationKullbackLeiblerBuilder(
    rho, pi, tm, validator=validator, eps_bull=1e-2,
    solve_params=solve_params, use_fv_hess=True,
    regression_builder=ALGADAPT.L2RegressionBuilder({'tol': 1e-10}),
    coeff_trunc=1e-3
)

(tm, _) = builder.solve()

n_coeffs = []
for i, tm in enumerate(builder.transport_map_list):
    n_coeffs.append(tm.n_coeffs)

    push_tm_rho = DIST.PushForwardTransportMapDistribution(tm, rho)
    fig,ax = DIAG.plotAlignedConditionals(
        push_tm_rho, do_diag=False, show_axis=False, numPointsXax=60)
    for figfmt in figfmt_list:
        fig.savefig('MCbanana-push-step%d.' % i + figfmt)
    
    pull_tm_pi = DIST.PullBackTransportMapDistribution(tm, pi_copy)
    fig,ax = DIAG.plotAlignedConditionals(
        pull_tm_pi, do_diag=False, show_axis=False, numPointsXax=60)
    for figfmt in figfmt_list:
        fig.savefig('MCbanana-pull-step%d.' % i + figfmt)

    fig = DIAG.plotGradXMap(tm, title=None, show_cbar=False, show_ticks=False)
    for figfmt in figfmt_list:
        fig.savefig('MCbanana-gradx-step%d.' % i + figfmt)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.semilogy(n_coeffs, builder.variance_diagnostic_list, 'o--k')
plt.show(False)
for figfmt in figfmt_list:
    fig.savefig('MCbanana-conv.' + figfmt)

# tm = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(dim=2, order=2)
# push_tm_rho = DIST.PushForwardTransportMapDistribution(tm, rho)
# solve_params = {'qtype': 3, 'qparams': [20,20], 'tol':1e-12}
# push_tm_rho.minimize_kl_divergence(pi, **solve_params)

# pull_tm_pi = DIST.PullBackTransportMapDistribution(tm, pi)
# (x, w) = rho.quadrature(**solve_params)
# gt = TM.grad_t_kl_divergence(x, rho, pull_tm_pi)

