import os
import os.path
import dill as pickle
import copy

import numpy as np
import numpy.random as npr

import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

STORE_FIG = True
SHOW_FIG = True
FIG_DIR = 'data/Figures/'

PLOT_CONDITIONALS = True

params = {'legend.fontsize': 'x-large',
          'axes.labelsize': 'x-large',
          'axes.titlesize':'x-large',
          'xtick.labelsize':'x-large',
          'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)

def store_fig(fig, name):
    figfmt_list = ['svg', 'pdf', 'eps', 'png']
    for figfmt in figfmt_list:
        fig.savefig(name + '.' + figfmt, bbox_inches='tight')

nobs = 4
sig = np.sqrt(1e-2)#1.024e-3 )
FNAME = 'bod-nobs%d-sig%.1e-fun-mc-reg1e-4-run-test' % (nobs, sig)
with open('data/' + FNAME + '.dill', 'rb') as istr:
    builder = pickle.load(istr)

if not os.path.exists(FIG_DIR + FNAME):
    os.makedirs(FIG_DIR + FNAME)

pi_copy = copy.deepcopy(builder.target_distribution)

fig = DIAG.plotAlignedConditionals(
    pi_copy, numPointsXax=60, range_vec=[-4,4], show_flag=SHOW_FIG)
# if STORE_FIG:
#     store_fig(fig, 'banana', figfmt_list)
    
n_coeffs = []
for i, tm in enumerate(builder.transport_map_list):
    n_coeffs.append(tm.n_coeffs)

    # push_tm_rho = DIST.PushForwardTransportMapDistribution(tm, rho)
    # fig = DIAG.plotAlignedConditionals(
    #     push_tm_rho, numPointsXax=60)
    # if STORE_FIG:
    #     store_fig(fig, 'banana-push-step%d' % i, figfmt_list)

    if PLOT_CONDITIONALS:
        pull_tm_pi = DIST.PullBackTransportMapDistribution(tm, pi_copy)
        fig,_ = DIAG.plotAlignedConditionals(
            pull_tm_pi, numPointsXax=60, show_flag=SHOW_FIG)
        if STORE_FIG:
            store_fig(fig, FIG_DIR + FNAME + '/bod-pull-step%d' % i)

        if i == 0:
            data = DIAG.computeRandomConditionals(
                pull_tm_pi, numPointsXax=60, num_conditionalsXax=2)
            fig = DIAG.plotRandomConditionals(data=data, show_flag=SHOW_FIG)
            Q_rand_list = data.Q_rand_list
        else:
            fig = DIAG.plotRandomConditionals(
                pull_tm_pi, numPointsXax=60, num_conditionalsXax=2,
                Q_rand_list=Q_rand_list, show_flag=SHOW_FIG)
        if STORE_FIG:
            store_fig(fig, FIG_DIR + FNAME + '/bod-rand-pull-step%d' % i)

        fig = DIAG.plotGradXMap(tm, title=None, show_cbar=False,
                                show_ticks=False, show_flag=SHOW_FIG)
        if STORE_FIG:
            store_fig(fig, FIG_DIR + FNAME + '/bod-gradx-step%d' % i)

fig = plt.figure()
ax = fig.add_subplot(111)
ncspmet = [nc for nc, spmet in zip(
    n_coeffs, builder.spmet_list) if spmet]
ax.semilogy(n_coeffs, builder.variance_diagnostic_list, 'o--k')
ax.grid(b=True, which='major', linewidth=1.)
ax.grid(b=True, which='minor', linewidth=0.3)
ax.set_xlabel("Number of coefficients")
ax.set_ylabel(r"$\mathbb{V}\,\left[\log \rho / T^\sharp \pi^\prime\right]$")

ax2 = ax.twiny()
xlim = ax.get_xlim()
ax2.set_xlim(xlim)
ax2.set_xticks(n_coeffs)
#exponent = len(str(builder.qparams_list[0])) - 1 #* 10**-exponent
print(builder.qparams_list)
ax2.set_xticklabels([ "%.0f" % (dictionary['n_samps'] ) for dictionary in builder.qparams_list ])
offset_text = r'x'#$\mathregular{10^{%d}}$' % exponent
# ax2.xaxis.offsetText.set_visible(False)
# ax2.text(1.0, 1.05, offset_text, transform=ax2.transAxes,
#          horizontalalignment='right', verticalalignment='top',
#          size='x-large')
ax2.set_xlabel("Number of Sparse Grid points (" + offset_text + ")")
if SHOW_FIG:
    plt.show(False)
if STORE_FIG:
    store_fig(fig, FIG_DIR + FNAME + '/bod-conv')




