import os.path
import dill as pickle

import numpy as np
import numpy.random as npr

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Samplers as SAMPS
import TransportMaps.Distributions.Examples.BiochemicalOxygenDemand as BOD

npr.seed(1)

TM.setLogLevel(20)

STORE_FIG = True
FIG_DIR = 'data/Figures/'
SHOW_FIG = False

NPROCS = 10

SINGLE_VAR_DIAG = False
SINGLE_PLOT_CONDITIONALS = False
SINGLE_PLOT_PUSHFORWARD = False
SINGLE_PLOT_MARGINALS = False
NMAP_MARGINALS = 7
NPOINTS_MARGINALS = int(1e6)
# SINGLE_MAP_CORRECTION = True

PLOT_MEAN_VAR_DIAG = True
N_MEAN_VAR_DIAG = 10000

params = {'legend.fontsize': 'x-large',
          'axes.labelsize': 'x-large',
          'axes.titlesize':'x-large',
          'xtick.labelsize':'x-large',
          'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)

def store_fig(fig, name):
    figfmt_list = ['svg', 'pdf', 'eps', 'png']
    for figfmt in figfmt_list:
        fig.savefig(name + '.' + figfmt, bbox_inches='tight')

# Prior ranges (could be retrievered from pi)
amin = 0.4
amax = 1.2
bmin = 0.01
bmax = 0.31
        
nobs = 4
sig = np.sqrt(1.024e-3)
FNAME = 'bod-nobs%d-sig%.1e' % (nobs, sig)
# FNAME = 'bod-nobs%d-sig%.1e-run' % (nobs, sig)
with open('data/' + FNAME + '.dill', 'rb') as istr:
    builder = pickle.load(istr)

joint_pi = builder.target_distribution.base_distribution # BOD.JointDistribution
JTW = builder.target_distribution.transport_map # Joint whitening map
TW = JTW.tm_list[1] # Posterior whitening map

if not os.path.exists(FIG_DIR + FNAME):
    os.makedirs(FIG_DIR + FNAME)
    
mpi_pool = TM.get_mpi_pool()
mpi_pool.start(NPROCS)

try:
    if SINGLE_VAR_DIAG or SINGLE_PLOT_MARGINALS or \
       SINGLE_PLOT_CONDITIONALS or SINGLE_PLOT_PUSHFORWARD:
        # Sample an observation and the corresponding parameters (synthetic)
        smpl = joint_pi.rvs(1)
        real_par = smpl[0,nobs:]
        obs = smpl[0,:nobs]

        # Build the corresponding posterior distribution
        pi = BOD.PosteriorDistribution(
            obs, joint_pi.times, sigma2=sig**2)

        for i, T in enumerate(builder.transport_map_list):
            print("Working on map %i/%i" % (i+1, len(builder.transport_map_list)))

            # Construct the conditioning map
            inv_smpl = T.inverse(obs[np.newaxis,:])  # This performs inversion only of the head
            TC = Maps.ConditionalTriangularTransportMap(T, inv_smpl[0, :])

            # Construct the composition (TW \circ TC)
            TWoTC = Maps.CompositeMap(TW, TC)

            # Construct the push-forward approximation to the posterior
            rho = DIST.StandardNormalDistribution(2)
            push_TWoTC_rho = DIST.PushForwardTransportMapDistribution(TWoTC, rho)

            # Construct the pull-back of the posterior
            pull_TWoTC_pi = DIST.PullBackTransportMapDistribution(TWoTC, pi)

            if SINGLE_PLOT_CONDITIONALS:
                # Plot pullback conditionals
                fig, _ = DIAG.plotAlignedConditionals(
                    pull_TWoTC_pi, do_diag=False, show_flag=SHOW_FIG)
                store_fig(fig, FIG_DIR + FNAME + '/bod-aligned-conditional-%d' % i)

            if SINGLE_VAR_DIAG:
                # Compute variance diagnostic
                var = DIAG.variance_approx_kl(rho, pull_TWoTC_pi, qtype=3, qparams=[8,8])
                print("Variance diagnostic: %e" % var)

            if SINGLE_PLOT_PUSHFORWARD:
                xa = np.linspace(amin+1e-4, amax-1e-4, 50)
                xb = np.linspace(bmin+1e-4, bmax-1e-4, 50)
                XA, XB = np.meshgrid(xa, xb)
                xx = np.vstack((XA.flatten(), XB.flatten())).T

                pos_pdf = pi.pdf(xx).reshape(XA.shape)
                scatter_tuple = (['x'], [xx])
                approx_pdf = TM.mpi_map(
                    'pdf', scatter_tuple=scatter_tuple,
                    obj=push_TWoTC_rho, mpi_pool=mpi_pool).reshape(XA.shape)
                # zmax = max(np.max(pos_pdf),np.max(approx_pdf))
                # lvls = np.linspace(0, zmax*0.95, 20)
                nlvls = 10
                fig = plt.figure()
                ax = fig.add_subplot(111)
                lvls = np.linspace(0, np.max(pos_pdf)*0.95, nlvls)
                ax.contour(XA, XB, pos_pdf, levels=lvls)
                lvls = np.linspace(0, np.max(approx_pdf)*0.95, nlvls)
                ax.contour(XA, XB, approx_pdf, levels=lvls, linestyles='dashed')
                ax.scatter(real_par[0], real_par[1],
                           color='k', zorder=2, s=120)
                ax.set_xlim(amin,amax)
                ax.set_ylim(bmin,bmax)
                ax.get_xaxis().set_ticks([])
                ax.get_yaxis().set_ticks([])
                if SHOW_FIG:
                    plt.show(False)
                store_fig(fig, FIG_DIR + FNAME + '/bod-aligned-pushforward-%d' % i)

            if SINGLE_PLOT_MARGINALS and \
               NMAP_MARGINALS == i:
                # Plot marginal of the posterior
                pos_smpl = push_TWoTC_rho.rvs(NPOINTS_MARGINALS)

                # Unbiased (MH) sampling of the target
                sampler = SAMPS.MetropolisHastingsIndependentProposalsSampler(
                    pull_TWoTC_pi, rho)
                (xx,_) = sampler.rvs(NPOINTS_MARGINALS, mpi_pool_tuple=(mpi_pool, None))
                yy = TWoTC.evaluate(xx)

                fig,handle = DIAG.plotAlignedMarginals(
                    yy, pos_smpl, show_flag=False, show_axis=True, do_diag=False,
                    range_vec=[[0.4, 1.2],[0.01,0.31]], title=None)
                ax = handle['axarr'][0,0] 
                ax.scatter(real_par[0], real_par[1],
                           color='k', zorder=2, s=120)
                if SHOW_FIG:
                    plt.show(False)
                store_fig(fig, FIG_DIR + FNAME + '/bod-aligned-marginal-%i' % i)

    if PLOT_MEAN_VAR_DIAG:
        smpl = joint_pi.rvs(N_MEAN_VAR_DIAG)
        obs = smpl[:,:nobs]
        rho = DIST.StandardNormalDistribution(2)

        var_diag = np.zeros((N_MEAN_VAR_DIAG, len(builder.transport_map_list)))
        for j, T in enumerate(builder.transport_map_list):
            print("Computing expected variance diagnostic for map %d/%d" % (
                j+1,len(builder.transport_map_list)))
            scatter_tuple = (['x'],[obs])
            inv_obs = TM.mpi_map( # This performs inversion only of the head
                'inverse', scatter_tuple=scatter_tuple, obj=T, mpi_pool=mpi_pool)

            for i in range(N_MEAN_VAR_DIAG):
                print("Sample %d/%d" % (i+1,N_MEAN_VAR_DIAG), end="\r")
                pi = BOD.PosteriorDistribution(
                    obs[i,:], joint_pi.times, sigma2=sig**2)

                # Construct the conditioning map
                TC = Maps.ConditionalTriangularTransportMap(T, inv_obs[i, :])

                # Construct the composition (TW \circ TC)
                TWoTC = Maps.CompositeMap(TW, TC)

                # Construct the pull-back of the posterior
                pull_TWoTC_pi = DIST.PullBackTransportMapDistribution(TWoTC, pi)

                # Compute variance diagnostic
                var_diag[i,j] = DIAG.variance_approx_kl(
                    rho, pull_TWoTC_pi, qtype=3, qparams=[8,8])

        # # Filter NaN
        # mask = ~np.isnan(var_diag)
        # vdiag_list = [ vd[m] for vd, m in zip(var_diag.T, mask.T) ]
                
        # Box plot
        flierprops = {'marker': '.'}
        boxprops = {'linewidth': 2}
        whiskerprops = {'linewidth': 2}
        medianprops = {'linewidth': 2}
        capprops = {'linewidth': 2}
        labels = [ "%d" % T.n_coeffs for T in builder.transport_map_list ]
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.boxplot(var_diag, whis=[5,95],
                   flierprops=flierprops, boxprops=boxprops,
                   whiskerprops=whiskerprops, medianprops=medianprops,
                   capprops=capprops)

        if len(builder.transport_map_list) > 6:
            ax.set_xticklabels(labels, rotation=-45)
        else:
            ax.set_xticklabels(labels)
        ax.set_yscale('log')
        ax.grid(b=True, which='major', linewidth=1.)
        ax.grid(b=True, which='minor', linewi4dth=.3)
        ax.set_xlabel("Number of coefficients")
        ax.set_ylabel(r"$\mathbb{V}\,\left[\log \rho / C_{\bf y}^\sharp \pi\right]$")
        if SHOW_FIG:
            plt.show(False)
        store_fig(fig, FIG_DIR + FNAME + '/bod-expected-variance-diagnostic')
        
finally:
    mpi_pool.stop()
        