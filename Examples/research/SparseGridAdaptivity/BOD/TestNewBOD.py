import numpy as np
import numpy.random as npr

import src.TransportMaps.Maps as MAPS
import src.TransportMaps.Maps.Decomposable as MAPSDEC
import TransportMaps.Distributions as DIST
import TransportMaps.Distributions.Examples.BiochemicalOxygenDemand.BODDistributions as BOD
import TransportMaps.FiniteDifference as FD

nobs = 3
amin = 0.4
amax = 1.2
bmin = 0.01
bmax = 0.31

# Original implementation joint
d1 = BOD.JointDistributionOld(nobs)

# New implementation joint (requires pullback through a map)
dnew = BOD.JointDistribution(np.arange(nobs)+1.)
T_params = Maps.CompositeMap(
    Maps.FrozenLinearDiagonalTransportMap(
        np.array([amin, bmin]),
        np.array([amax-amin, bmax-bmin]) ),
    Maps.FrozenGaussianToUniformDiagonalTransportMap(2) )
I = Maps.IdentityTransportMap(nobs)
T2 = Maps.TriangularListStackedTransportMap([I, T_params], [[i for i in range(nobs)] , [nobs, nobs + 1]])
d2 = DIST.PullBackTransportMapDistribution(T2, dnew)

smpl = dnew.rvs(1)

# Posterior distribution
dpos = BOD.PosteriorDistribution(
    obs=smpl[0,:3], times=np.arange(nobs)+1. )

# Test pdf and gradients
x = npr.randn(10, nobs+2)
xx = x[:,:]
xx[:,3] = 0.4 + 0.8 * npr.random(xx.shape[0])
xx[:,4] = 0.01 + 0.30 * npr.random(xx.shape[0])

# Test finite difference
mmap = dnew.factors[0][1].muMap
FD.check_grad_x(mmap.evaluate, mmap.grad_x, xx[:,3:], 1e-4)
FD.check_grad_x(mmap.grad_x, mmap.hess_x, xx[:,3:], 1e-4)
FD.check_grad_x(dnew.log_pdf, dnew.grad_x_log_pdf, x, 1e-4)
FD.check_grad_x(d2.log_pdf, d2.grad_x_log_pdf, x, 1e-4)

lpdf1 = d1.log_pdf(x)
lpdf2 = d2.log_pdf(x)
assert( np.isclose(np.var(lpdf1 - lpdf2),0) )

glpdf1 = d1.grad_x_log_pdf(x)
glpdf2 = d2.grad_x_log_pdf(x)
assert( np.isclose(np.var(glpdf1 - glpdf2),0) )

hlpdf1 = d1.hess_x_log_pdf(x)
hlpdf2 = d2.hess_x_log_pdf(x)
assert( np.isclose(np.var(hlpdf1 - hlpdf2),0) )

# Test posterior gradients
FD.check_grad_x(dpos.log_pdf, dpos.grad_x_log_pdf, xx[:,3:], 1e-4)
FD.check_grad_x(dpos.grad_x_log_pdf, dpos.hess_x_log_pdf, xx[:,3:], 1e-4)