import sys
import os
import shutil
import dill as pickle
import copy

import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

import numpy as np
import numpy.random as npr

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import src.TransportMaps.Maps as MAPS
import TransportMaps.Algorithms.Adaptivity as ALGADAPT
import TransportMaps.Diagnostics as DIAG

NPROCS = 1
RND_SEED = 1

def store(tm, obj, fname):
    if os.path.exists(fname):
        shutil.copyfile(fname, fname + '.bak')
    with open(fname, 'wb') as ostr:
        pickle.dump(obj, ostr)
    try:
        os.remove(fname + '.bak')
    except:
        pass

TM.setLogLevel(20)

nobs = 4
sig2 = 1e-2#1.024e-3 
FNAME = 'bod-nobs%d-sig%.1e' % (nobs, np.sqrt(sig2))
with open('data/' + FNAME + '.dill', 'rb') as istr:
    pi = pickle.load(istr)

OUT_FNAME = 'data/' + FNAME + '-fun-mc-run-test2.dill'
# if os.path.exists(OUT_FNAME):
#     print("Output file already exists. Terminating.")
#     sys.exit(0)
    
# pi = DIST.PushForwardTransportMapDistribution(
#     MAPS.PermutationTransportMap([nobs, nobs+1] + list(range(nobs))),
#     pi_tar)
    
pi_copy = copy.deepcopy(pi)

rho = DIST.StandardNormalDistribution(dim=pi.dim)

tm = TM.Default_IsotropicIntegratedSquaredDiagonalTransportMap(
    dim=pi.dim, order=1, btype='fun')
# tm = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(dim=pi.dim, order=1)

# Start a pool of processes
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)
else:
    mpi_pool = None

# if mpi_pool is not None:
#     mpi_pool.mod_import([(None, 'numpy.random', 'npr')])
#     def set_seed(RND_SEED):
#         print("Setting seed: %d" % RND_SEED)
#         npr.seed(RND_SEED)
#     bcast_tuple = (['RND_SEED'],[RND_SEED])
#     TM.mpi_map(set_seed, bcast_tuple=bcast_tuple, mpi_pool=mpi_pool, concatenate=False)
npr.seed(RND_SEED)

solve_params = {
    'ders': 1,
    'qtype': 4,
    'tol': 1e-2,
    'mpi_pool': mpi_pool,
    'batch_size': int(1e8),
    # 'regularization': {'type': 'L2', 'alpha': 1e-4}
}
validator = DIAG.DimensionAdaptiveSparseGridKLMinimizationValidator(
    eps=1e-3)
    # eps=5e-3, eps_abs=5e-3, max_nsamps=200000)

regression_params = {
    'batch_size_list': [(None,None,None)] * min(4, nobs+2) + [(None,None,1000)] * (nobs-2),
    'mpi_pool_list': [None] * min(6,nobs+2) + [mpi_pool] * (nobs-4)
}
regression_builder = ALGADAPT.L2RegressionBuilder(regression_params)

# coeff_trunc = 1e-4
# coeff_trunc = {'truncation_type': 'percentage', 'p': 0.5}
coeff_trunc = {'truncation_type': 'constant', 'n': 10}
# coeff_trunc = {'truncation_type': 'manual'}
prune_trunc = {'truncation_type': 'manual'}

builder = ALGADAPT.FirstVariationKullbackLeiblerBuilder(
    rho, pi, tm, validator=validator, eps_bull=1e-1,
    solve_params=solve_params,
    regression_builder=regression_builder,
    coeff_trunc=coeff_trunc,
    use_fv_hess=True, prune_trunc=prune_trunc, avar_trunc=1e-3,
    line_search_params={'maxiter': 40},
    verbosity=1, interactive=True,
)
builder.callback = store
builder.callback_kwargs = {
    'obj': builder,
    'fname': OUT_FNAME}

try:    
    (tm, _) = builder.solve()
finally:
    if mpi_pool is not None:
        mpi_pool.stop()

