import sys
import dill as pickle
import numpy as np
import matplotlib.pyplot as plt
from TransportMaps.Misc import cmdinput
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
from TransportMaps.Distributions.Examples.BiochemicalOxygenDemand import \
    JointDistribution, PosteriorDistribution

# Prior ranges (could be retrievered from pi)
amin = 0.4
amax = 1.2
bmin = 0.01
bmax = 0.31

nobs = 4
sig2 = 1e-3
times = np.arange(1,nobs+1)
pi = JointDistribution(times, sig2)

# Visualize some posteriors to see get an idea of the particular setting
instr = 'n'
while instr == 'n':
    smpl = pi.rvs(1)
    obs = smpl[0,:nobs]
    par = smpl[0,nobs:]
    pipos = PosteriorDistribution(obs, times, sig2)
    _, axh = DIAG.plotAlignedConditionals(
        pipos, do_diag=False, range_vec=[[0.4,1.2],[0.01,0.31]],
        numPointsXax=100)
    ax = axh['axarr'][0,0]
    ax.scatter(par[0], par[1], color='k', zorder=1, s=120)
    instr = None
    while instr not in ['n','q','s']:
        instr = cmdinput("Select between (n)ew realization, (q)uit or (s)ave: ")
    if instr == 'q':
        sys.exit(0)

# Construct the joint whitening map
T_params = Maps.CompositeMap(
    Maps.FrozenLinearDiagonalTransportMap(
        np.array([amin, bmin]),
        np.array([amax-amin, bmax-bmin]) ),
    Maps.FrozenGaussianToUniformDiagonalTransportMap(2) )
I = Maps.IdentityTransportMap(nobs)
JTW = Maps.TriangularListStackedTransportMap(
    [I, T_params], [ [i for i in range(nobs)] , [nobs, nobs+1]])

# Pullback pi
pull_JTW_pi = DIST.PullBackTransportMapDistribution(JTW, pi)

with open('data/bod-nobs%d-sig%.1e.dill' % (nobs, np.sqrt(sig2)), 'wb') as ostr:
    pickle.dump(pull_JTW_pi, ostr)