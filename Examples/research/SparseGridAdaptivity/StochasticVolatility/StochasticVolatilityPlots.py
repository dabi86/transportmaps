import os
import os.path
import dill as pickle
import copy

import numpy as np
import numpy.random as npr

import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

MC = False

if MC:
    trim_n = -1
    neval_log_pdf = [
        1350000, 3656000, 5164000, 6992000, 13996000, 20240000, 28004000, 37496000,
    ]
    neval_gx_log_pdf = [
        1320000, 3554000, 5164000, 6992000, 13652000, 19798000, 27464000, 36858000,
    ]
else:
    trim_n = -2
    neval_log_pdf = [
        115561, 344410, 750300, 1121883, 1588861, 2166561, 2796128, 3133487, None, None
    ]
    neval_gx_log_pdf = [
        122719, 339068, 736182, 1101159 , 1555068, 2118856, 2734126, 3056151, None, None
    ]

NEVAL = True
STORE_FIG = True
SHOW_FIG = True
FIG_DIR = 'data/Figures/'

PLOT_CONDITIONALS = True
NCOND = 7

params = {'legend.fontsize': 'x-large',
          'axes.labelsize': 'x-large',
          'axes.titlesize':'x-large',
          'xtick.labelsize':'x-large',
          'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)

def store_fig(fig, name):
    figfmt_list = ['svg', 'pdf', 'eps', 'png']
    for figfmt in figfmt_list:
        fig.savefig(name + '.' + figfmt, bbox_inches='tight')

nobs = 36
sig2 = 0.25#1.024e-3
if MC:
    FNAME = 'Distribution-sig%0.2f-%dobs-mc-run' % (sig2, nobs)
else:
    FNAME = 'Distribution-sig%0.2f-%dobs-run' % (sig2, nobs)

with open('data/' + FNAME + '.dill', 'rb') as istr:
    builder = pickle.load(istr)

if not os.path.exists(FIG_DIR + FNAME):
    os.makedirs(FIG_DIR + FNAME)

pi_copy = copy.deepcopy(builder.target_distribution)

fig = DIAG.plotAlignedConditionals(
    pi_copy, numPointsXax=60, range_vec=[-4,4], show_flag=SHOW_FIG,
    dimensions_vec=list(range(NCOND))
)
# if STORE_FIG:
#     store_fig(fig, 'banana', figfmt_list)
    
n_coeffs = []
for i, tm in enumerate(builder.transport_map_list):
    n_coeffs.append(tm.n_coeffs)

    # push_tm_rho = DIST.PushForwardTransportMapDistribution(tm, rho)
    # fig = DIAG.plotAlignedConditionals(
    #     push_tm_rho, numPointsXax=60)
    # if STORE_FIG:
    #     store_fig(fig, 'banana-push-step%d' % i, figfmt_list)

    if PLOT_CONDITIONALS:
        pull_tm_pi = DIST.PullBackTransportMapDistribution(tm, pi_copy)
        fig,_ = DIAG.plotAlignedConditionals(
            pull_tm_pi, numPointsXax=60, show_flag=SHOW_FIG,
            dimensions_vec=list(range(NCOND))
        )
        if STORE_FIG:
            store_fig(fig, FIG_DIR + FNAME + '/sv-pull-step%d' % i)

        if i == 0:
            data = DIAG.computeRandomConditionals(
                pull_tm_pi, numPointsXax=60, num_conditionalsXax=2)
            fig = DIAG.plotRandomConditionals(data=data, show_flag=SHOW_FIG)
            Q_rand_list = data.Q_rand_list
        else:
            fig = DIAG.plotRandomConditionals(
                pull_tm_pi, numPointsXax=60, num_conditionalsXax=2,
                Q_rand_list=Q_rand_list, show_flag=SHOW_FIG)
        if STORE_FIG:
            store_fig(fig, FIG_DIR + FNAME + '/sv-rand-pull-step%d' % i)

        fig = DIAG.plotGradXMap(tm, title=None, show_cbar=False,
                                show_ticks=False, show_flag=SHOW_FIG)
        if STORE_FIG:
            store_fig(fig, FIG_DIR + FNAME + '/sv-gradx-step%d' % i)

# Discretization number
fig = plt.figure()
ax = fig.add_subplot(111)
ncspmet = [nc for nc, spmet in zip(
    n_coeffs, builder.spmet_list) if spmet]
ax.semilogy(n_coeffs[:trim_n], builder.variance_diagnostic_list[:trim_n], 'o--k')
ax.grid(b=True, which='major', linewidth=1.)
ax.grid(b=True, which='minor', linewidth=0.3)
ax.set_xlabel("Number of coefficients")
ax.set_ylabel(r"$\mathbb{V}\,\left[\log \rho / T^\sharp \pi^\prime\right]$")
ax2 = ax.twiny()
xlim = ax.get_xlim()
ax2.set_xlim(xlim)
ax2.set_xticks(n_coeffs[:trim_n])
#exponent = len(str(builder.qparams_list[0])) - 1 #* 10**-exponent
print(builder.qparams_list)
exponent = len(str(builder.qparams_list[0])) - 1
ax2.set_xticklabels([ "%.1f" % (n * 10**-exponent)
                      for n in builder.qparams_list[:trim_n] ])
offset_text = r'x$\mathregular{10^{%d}}$' % exponent
ax2.set_xlabel("Number of discretization points (" + offset_text + ")")
if SHOW_FIG:
    plt.show(False)
if STORE_FIG:
    store_fig(fig, FIG_DIR + FNAME + '/sv-conv')

# Number of function evaluations
fig = plt.figure()
ax = fig.add_subplot(111)
ncspmet = [nc for nc, spmet in zip(
    n_coeffs, builder.spmet_list) if spmet]
ax.semilogy(n_coeffs[:trim_n], builder.variance_diagnostic_list[:trim_n], 'o--k')
ax.grid(b=True, which='major', linewidth=1.)
ax.grid(b=True, which='minor', linewidth=0.3)
ax.set_xlabel("Number of coefficients")
ax.set_ylabel(r"$\mathbb{V}\,\left[\log \rho / T^\sharp \pi^\prime\right]$")
ax2 = ax.twiny()
xlim = ax.get_xlim()
ax2.set_xlim(xlim)
ax2.set_xticks(n_coeffs[:trim_n])
#exponent = len(str(builder.qparams_list[0])) - 1 #* 10**-exponent
exponent = len(str(neval_log_pdf[0])) - 1
ax2.set_xticklabels([ "%.0f" % (n * 10**-exponent)
                      for n in neval_log_pdf[:trim_n] ])
offset_text = r'x$\mathregular{10^{%d}}$' % exponent
ax2.set_xlabel("Number of function evaluations (" + offset_text + ")")
if SHOW_FIG:
    plt.show(False)
if STORE_FIG:
    store_fig(fig, FIG_DIR + FNAME + '/sv-conv-feval')

# Number of gradient evaluations
fig = plt.figure()
ax = fig.add_subplot(111)
ncspmet = [nc for nc, spmet in zip(
    n_coeffs, builder.spmet_list) if spmet]
ax.semilogy(n_coeffs[:trim_n], builder.variance_diagnostic_list[:trim_n], 'o--k')
ax.grid(b=True, which='major', linewidth=1.)
ax.grid(b=True, which='minor', linewidth=0.3)
ax.set_xlabel("Number of coefficients")
ax.set_ylabel(r"$\mathbb{V}\,\left[\log \rho / T^\sharp \pi^\prime\right]$")
ax2 = ax.twiny()
xlim = ax.get_xlim()
ax2.set_xlim(xlim)
ax2.set_xticks(n_coeffs[:trim_n])
#exponent = len(str(builder.qparams_list[0])) - 1 #* 10**-exponent
exponent = len(str(neval_log_pdf[0])) - 1
ax2.set_xticklabels([ "%.0f" % (n * 10**-exponent)
                      for n in neval_log_pdf[:trim_n] ])
offset_text = r'x$\mathregular{10^{%d}}$' % exponent
ax2.set_xlabel("Number of gradient evaluations (" + offset_text + ")")
if SHOW_FIG:
    plt.show(False)
if STORE_FIG:
    store_fig(fig, FIG_DIR + FNAME + '/sv-conv')


