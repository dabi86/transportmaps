#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import numpy as np
import scipy.io as sio
import TransportMaps.Algorithms.SparsityIdentification as SI

# set parameters
nsamps       = np.floor(np.logspace(1,4,num=20))
data_type    = 1
MC_runs      = 10
order        = 2
ordering     = SI.ReverseCholesky()
deltas       = [2,2.5,3,3.5,4,4.5,5]
delta_string = ['2','2.5','3','3.5','4','4.5','5']

# setup test case
if data_type == 0:
    DATAFILE = '../data_samples/copula_data/power_data-s-3.txt'
    name_string = 'power'
    OMEGAFILE = '../data_samples/copula_data/power_omega-s-3.txt'
elif data_type == 1:
    DATAFILE = './data/cdf_data-s-3.txt'
    name_string = 'cdf'
    OMEGAFILE = './data/cdf_omega-s-3.txt'

# load data and true graph
data_all = np.loadtxt(DATAFILE)
omega_true = np.loadtxt(OMEGAFILE)

for N in nsamps:
    for iter in range(MC_runs):

        # select random subset of data
        rand_samples = np.random.randint(0,data_all.shape[0],int(N))
        data = data_all[rand_samples,:]

        # Scale the data
        mean_vec = np.mean(data,axis=0)
        print("mean_vec = ",mean_vec)
        data1 = data - mean_vec
        print("shifted data = ",data1)
        inv_var = 1./(np.var(data1,axis=0))
        print("inv var = ",inv_var)
        inv_std = np.diag(np.sqrt(inv_var))
        rescaled_data = np.dot(data1,inv_std)
        print("rescaled data = ", rescaled_data[0:2,:],"\n")
        print("****************************************************\n")
        processed_data = rescaled_data

        # run SING for each delta
        for i, delta in enumerate(deltas):
            recovered_gp = SI.SING(processed_data, order, ordering, delta)
            print("gp = ",recovered_gp)
           
            # extract adjacency graph from GP 
            dim = recovered_gp.shape[0]
            adjacency = np.zeros([dim, dim])
            adjacency[recovered_gp != 0] = 1
            
            # save GP and adjacency graph
            np.savetxt('./outputMC/cdf_adj-%s-%d-%d-%s-%d-%d.txt' %(name_string,N,dim,delta_string[i],order,iter), adjacency, fmt='%1.3f')
            np.savetxt('./outputMC/cdf_gp-%s-%d-%d-%s-%d-%d.txt' %(name_string,N,dim,delta_string[i],order,iter), recovered_gp, fmt='%1.3f')

# -- END OF FILE --
