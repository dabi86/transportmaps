GPP(TLAI)_sXX_ave.dat : average steady state output for Gross Primary Productivity or Total Leaf Are Index for 96 sites each. 
		      : so basically, 192 different QoIs, 2999 samples each.


note that some fraction of outputs are 0. You should probably simply remove them from the data set.


3 more files that in principle should irrelevant for density estimation
=======================================================================
input.dat             : 2999x66 matrix (2999 inputs, randomly distributed in 66-dimensional parameter space), scaled to -1,1
prange.dat            : range of all 66 inputs 
pnames.dat            : names for all 66 input 



