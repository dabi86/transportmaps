import numpy as np
import os

directory = 'acme_data_raw/'

for filename in os.listdir(directory):
    data = np.loadtxt(directory+filename, comments='%')

    data1 = data[data != 0]
    clean_data = data1[~np.isnan(data1)]

    np.savetxt('acme_data_clean/'+filename, clean_data)#, fmt='%.1f')
