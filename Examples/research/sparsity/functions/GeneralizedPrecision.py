'''This file computes the gradient with respect to map coefficients of the
hessian of the log target pdf. It also computes the variance of the generalized
precision matrix omega.'''
import warnings
import time
import numpy as np

nax = np.newaxis
def gen_precision(pb_density, data):
    # Compute omega (expectation over samples)
    hess_samps = pb_density.hess_x_log_pdf(data)
    gen_prec = np.mean(np.abs(hess_samps), axis=0)

    return gen_prec

def grad_a_omega(pb_density, data):

    eta = pb_density.base_density
    S = pb_density.transport_map
    n = data.shape[0]

    # Compute grad_a_grad_x and reshape as array
    dadxS_list = S.grad_a_grad_x(data)
    # print("n coeffs = ",S.get_n_coeffs(),"$$$$$$$$$$$$$$$$$$$$$$")
    dadxS = np.zeros((data.shape[0], S.get_n_coeffs(), S.dim, S.dim))
    start = 0
    for k in range(len(dadxS_list)):
        # print("dadxS_list[k].shape = ",dadxS_list[k].shape,"**********************")
        # print("dadxS---.shape = ",dadxS[:,start:start+dadxS_list[k].shape[1],k,0:(k+1)].shape,"**********************")
        # dadxS[:,start:start+dadxS_list[k].shape[1],k,0:(k+1)] = dadxS_list[k]
        # above only works for full map
        dadxS[:,start:start+dadxS_list[k].shape[1],k,0:(dadxS_list[k].shape[-1])] = dadxS_list[k]
        start+=dadxS_list[k].shape[1]

    # Compute grad_a and reshape as array
    daS_list = S.grad_a(data)
    daS = np.zeros((data.shape[0], S.get_n_coeffs(), S.dim))
    start = 0
    for k in range(len(daS_list)):
        daS[:,start:start+daS_list[k].shape[1],k] = daS_list[k]
        start+=daS_list[k].shape[1]

    # Compute grad_a_hess_x and reshape as array
    dadx2S_list = S.grad_a_hess_x(data)
    dadx2S = np.zeros((data.shape[0], S.get_n_coeffs(), S.dim, S.dim, S.dim))
    start = 0
    for k in range(len(dadx2S_list)):
        # print("dadx2S_list[k].shape = ",dadxS_list[k].shape,"**********************")
        # print("dadx2S---.shape = ",dadxS[:,start:start+dadxS_list[k].shape[1],k,0:(k+1)].shape,"**********************")
        # dadx2S[:,start:start+dadx2S_list[k].shape[1],k,0:(k+1),0:(k+1)] = dadx2S_list[k]
        # above only works for full map
        dadx2S[:,start:start+dadx2S_list[k].shape[1],k,0:(dadx2S_list[k].shape[-1]),0:(dadx2S_list[k].shape[-1])] = dadx2S_list[k]
        start+=dadx2S_list[k].shape[1]

    # Compute derivatives in omega gradient
    #dx2logeta = eta.hess_x_log_pdf(S.evaluate(data)).reshape((n, S.dim**2)) #n x d^2
    dx2logeta = eta.hess_x_log_pdf(S.evaluate(data)) #n x d x d
    dxS = S.grad_x(data) # n x d x d
    dxlogeta = eta.grad_x_log_pdf(S.evaluate(data)) # n x d
    dx2S = S.hess_x(data) # n x d x d x d
    # daS = S.grad_a(data) # n x m x d
    # dadxS = S.grad_a_grad_x(data) # n x m x d x d
    # dadx2S = S.grad_a_hess_x(data) # n x m x d x d x d

    # matrix multiplication
    # grad_a_omega part 1: log(\eta \circ S)
    A1 = np.einsum('...ij,...hjk->...hik', dx2logeta, dadxS)
    A1 = np.einsum('...hij,...ik->...hjk', A1, dxS)
    A2 = np.einsum('...ij,...jk->...ik', dx2logeta, dxS)
    A2 = np.einsum('...ij,...hik->...hjk', A2, dadxS)
    B1 = np.einsum('...ij,...hj->...hi', dx2logeta, daS)
    B2 = np.einsum('...hi,...ijk->...hjk', B1, dx2S)
    C = np.einsum('...i,...hijk->...hjk', dxlogeta, dadx2S)
    grad_omega_pt1 = A1 + A2 + B2 + C
    # print("shape of pt1 = ", grad_omega_pt1.shape)

    # matrix multiplication
    # grad_a_omega part 2: dlog(det(grad S))
    grad_omega_pt2 = S.grad_a_hess_x_log_det_grad_x(data)
    # print("shape of pt2 = ", grad_omega_pt2.shape)

    # Compute grad omega (expectation over samples)
    grad_omega = grad_omega_pt1 + grad_omega_pt2
    omega_jac = np.mean(np.multiply(grad_omega, np.sign(grad_omega)), axis=0)
    print("shape of omega_jac = ", omega_jac.shape)

    return omega_jac

def var_omega(pb_density, data):
    n = data.shape[0]
    dim = pb_density.transport_map.dim
    # print("DIMENSION = ",dim,"****************************")

    omega_CI_jac = grad_a_omega(pb_density, data)

    fisher_info_samps = pb_density.hess_a_log_pdf(data)
    fisher_info = - np.mean(fisher_info_samps,axis=0)
    #print("fisher_info = ", fisher_info)
    var_a = 1/n * np.linalg.inv(fisher_info)
    #print("var_a = ",var_a)
    
    var_omega = np.zeros([dim, dim])
    for i in range(dim):
        for j in range(dim):
            temp = np.dot(var_a,omega_CI_jac[:,i,j])
            var_omega[i,j] = np.dot(omega_CI_jac[:,i,j].T,temp)

    return var_omega
