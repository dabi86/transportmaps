'''This file computes the gradient with respect to map coefficients of the hessian of the log target pdf'''
import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from tabulate import tabulate
from scikits.sparse.cholmod import cholesky
from scipy.sparse import csc_matrix
from scipy.sparse import csr_matrix

import SpectralToolbox.Spectral1D as S1D

import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.tests.TestFunctions as TF

import ExtraDensities
import GaussianGraphs as gg

nax = np.newaxis
def grad_a_omega(pb_density, data):

    # Compute omega (expectation over samples)
    hess_samps = pb_density.hess_x_log_pdf(data)
    omega = np.mean(np.abs(hess_samps), axis=0)

    eta = pb_density.base_density
    S = pb_density.transport_map
    n = data.shape[0]

    # Compute grad_a_grad_x and reshape as array
    dadxS_list = S.grad_a_grad_x(data)
    dadxS = np.zeros((data.shape[0], S.get_n_coeffs(), S.dim, S.dim))
    start = 0
    for k in range(len(dadxS_list)):
        dadxS[:,start:start+dadxS_list[k].shape[1],k,0:(k+1)] = dadxS_list[k]
        start+=dadxS_list[k].shape[1]

    # Compute grad_a and reshape as array
    daS_list = S.grad_a(data)
    daS = np.zeros((data.shape[0], S.get_n_coeffs(), S.dim))
    start = 0
    for k in range(len(daS_list)):
        daS[:,start:start+daS_list[k].shape[1],k] = daS_list[k]
        start+=daS_list[k].shape[1]

    # Compute grad_a_hess_x and reshape as array
    dadx2S_list = S.grad_a_hess_x(data)
    dadx2S = np.zeros((data.shape[0], S.get_n_coeffs(), S.dim, S.dim, S.dim))
    start = 0
    for k in range(len(dadx2S_list)):
        dadx2S[:,start:start+dadx2S_list[k].shape[1],k,0:(k+1),0:(k+1)] = dadx2S_list[k]
        start+=dadx2S_list[k].shape[1]

    # Compute derivatives in omega gradient
    #dx2logeta = eta.hess_x_log_pdf(S.evaluate(data)).reshape((n, S.dim**2)) #n x d^2
    dx2logeta = eta.hess_x_log_pdf(S.evaluate(data)) #n x d x d
    dxS = S.grad_x(data) # n x d x d
    dxlogeta = eta.grad_x_log_pdf(S.evaluate(data)) # n x d
    dx2S = S.hess_x(data) # n x d x d x d
    # daS = S.grad_a(data) # n x m x d
    # dadxS = S.grad_a_grad_x(data) # n x m x d x d
    # dadx2S = S.grad_a_hess_x(data) # n x m x d x d x d

    # matrix multiplication
    # grad_a_omega part 1: log(\eta \circ S)
    A1 = np.einsum('...ij,...hjk->...hik', dx2logeta, dadxS)
    A1 = np.einsum('...hij,...ik->...hjk', A1, dxS)
    A2 = np.einsum('...ij,...jk->...ik', dx2logeta, dxS)
    A2 = np.einsum('...ij,...hik->...hjk', A2, dadxS)
    B1 = np.einsum('...ij,...hj->...hi', dx2logeta, daS)
    B2 = np.einsum('...hi,...ijk->...hjk', B1, dx2S)
    C = np.einsum('...i,...hijk->...hjk', dxlogeta, dadx2S)
    grad_omega_pt1 = A1 + A2 + B2 + C
    # print("shape of pt1 = ", grad_omega_pt1.shape)

    # matrix multiplication
    # grad_a_omega part 2: dlog(det(grad S))
    grad_omega_pt2 = S.grad_a_hess_x_log_det_grad_x(data)
    # print("shape of pt2 = ", grad_omega_pt2.shape)

    # Compute grad omega (expectation over samples)
    grad_omega = grad_omega_pt1 + grad_omega_pt2
    omega_jac = np.mean(np.multiply(grad_omega, np.sign(grad_omega)), axis=0)
    print("shape of omega_jac = ", omega_jac.shape)

    return omega, omega_jac

def var_omega(pb_density, data, omega_CI_jac):
    fisher_info_samps = pb_density.hess_a_log_pdf(data)
    fisher_info = - np.mean(fisher_info_samps,axis=0)
    print("fisher_info = ", fisher_info)
    var_a = 1/n * np.linalg.inv(fisher_info)
    print("var_a = ",var_a)
    
    var_omega = np.zeros([dim, dim])
    for i in range(dim):
        for j in range(dim):
            temp = np.dot(var_a,omega_CI_jac[:,i,j])
            var_omega[i,j] = np.dot(omega_CI_jac[:,i,j].T,temp)
    return var_omega

if __name__ == "__main__":

    #initial problem setup
    dim = 4
    setup = {}
    pi = {}
    setup['mu'] = np.zeros(dim)
    setup['sigma2'] = np.identity(dim)
    setup['dim'] = dim

    # create the data
    nsamps = 2000
    graph = gg.GridGraph(dim)
    # returns perfect ordering
    data = graph.rvs(nsamps)
    n = data.shape[0]
    Omega = graph.omega()
    print("initial omega = ")
    print(Omega)
    
    #going from target (samples) to standard normal
    pi['target_density'] = DENS.StandardNormalDensity(dim)
    #will keep support map but shouldn't need it in this example
    pi['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.zeros(dim),
                                                              np.ones(dim))
    # APPROXIMATION
    MONOTONE = 'intexp'
    tol = 1e-5

    # REGULARIZATION
    # None: No regularization
    # L2: L2 regularization
    REG = None
    
    # L2 error estimation
    # 3: Gauss quadrature of order n
    # 0: Monte Carlo quadrature with n point
    qtype = 0
    qparams = nsamps
    tit_intest = str(qtype) + '-' + str(qparams)
    
    # Gradient information
    # 0: derivative free
    # 1: gradient information
    # 2: Hessian information
    ders = 2
    
    # Approximation orders
    order = 1

    # Target density to be approximated L^\sharp \pi_{\rm tar}
    target_density = DENS.PullBackTransportMapDensity(pi['support_map'],
                                                      pi['target_density'])

    # define base_density from samples
    pi['base_density'] = ExtraDensities.SamplesDensity(data)

    # Build the transport map (isotropic for each entry)
    tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
                   setup['dim'], order, active_vars=None)
    
    # Construct density, Density T_\sharp \pi
    tm_density = DENS.PushForwardTransportMapDensity(tm_approx, pi['base_density'])
   
    # solve for tmap
    log_entry_solve = tm_density.minimize_kl_divergence(target_density, qtype=qtype,
                                                            qparams=qparams,
                                                            regularization=REG,
                                                            tol=tol, ders=ders)
    pb_density = DENS.PullBackTransportMapDensity(tm_approx, pi['target_density'])
    omega_CI, omega_CI_jac = grad_a_omega(pb_density, data)
    # print("omega_CI = ",omega_CI)
    # print("omega_CI_jac = ", omega_CI_jac)

    var_omega_CI = var_omega(pb_density, data, omega_CI_jac)
    print("var_omega = ", var_omega_CI)

