import warnings
import numpy as np
import scipy.stats as stats
import TransportMaps.Densities as DENS

#define a class for the target density based on samples
class SamplesDensity(DENS.Density):
    r""" Arbitrary density built from samples

    Args:
      samples (type, dimension?): independent samples
    """

    def __init__(self, samples):
        super(SamplesDensity,self).__init__(samples.shape[1])
        self.samples = samples

    def rvs(self, m, *args, **kwargs):
        if m > self.samples.shape[0]:
            raise ValueError("Number of rvs must be less than or equal to length of samples provided")
        
        msamples = self.samples[0:m,:]
        return msamples
    
    def quadrature(self, qtype, qparams, *args, **kwargs):
        if qtype == 0:
            x = self.rvs(qparams)
            w = np.ones(qparams)/qparams
        else:
            raise NotImplementedError("Not implemented because sample density only has quadrature via samples")
        return (x,w)

    #if no analytical form of pdf
    def kde(self, x, params=None):
        r""" Evaluate :math:`\pi(x)`
        """
        if self.samples.shape[1] == 1:
            samps = self.samples.flatten()
        else:
            samps = self.samples

        # kde takes data in form (#dim, #data)
        density = stats.gaussian_kde(samps.T)
        pdf_kde = density.evaluate(x.T)
        return pdf_kde 

    def log_kde(self, x, params=None):
        if self.samples.shape[1] == 1:
            samps = self.samples.flatten()
        else:
            samps = self.samples
        # kde takes data in form (#dim, #data)
        density = stats.gaussian_kde(samps.T)
        pdf_kde = density.evaluate(x.T)
        return np.log(pdf_kde)

class PC_Density(DENS.Density):
    def __init__(self,alpha0,alpha1,alpha2):

        self.alpha0 = alpha0
        self.alpha1 = alpha1
        self.alpha2 = alpha2

        #set coefficients needed for density expression
        self.alpha = -self.alpha1/2/self.alpha2
        self.beta = self.alpha2
        self.gamma = pow(self.alpha1,2)/4/self.alpha2 - self.alpha0 + self.alpha2

    def pdf(self,x,params=None):
        out = 1./np.sqrt(2*np.pi)*(
              np.exp(-1/2.*pow(self.alpha + pow((x + self.gamma)/self.beta,1/2),2)) +
              np.exp(-1/2.*pow(self.alpha - pow((x +
                  self.gamma)/self.beta,1/2),2))) * np.abs(1/2./self.beta) * pow((x + self.gamma)/self.beta,-1/2.)  
        return out.flatten()

    def log_pdf(self,x,params=None):
        out = np.log(1./np.sqrt(2*np.pi)*(
              np.exp(-1/2.*pow(self.alpha + pow((x + self.gamma)/self.beta,1/2),2)) +
              np.exp(-1/2.*pow(self.alpha - pow((x +
                  self.gamma)/self.beta,1/2),2))) * np.abs(1/2./self.beta) * pow((x + self.gamma)/self.beta,-1/2.))
        return out.flatten()

    #def mean_log_pdf(self):
    def rvs(self,m):
        nax = np.newaxis
        norm_samp = stats.norm.rvs(size=m)
        samps = self.alpha0 + self.alpha1*norm_samp +\
            self.alpha2*(np.multiply(norm_samp,norm_samp) - 1.)
        samps = samps[:,nax]
        return samps
        
    def quadrature(self, qtype, qparams):
        if qtype == 0:
            x = qparams
            l = qparams.shape[0]
            w = np.ones(l)/l
        else:
            raise NotImplementedError("Not implemented")
        return (x,w)

class Laplace(DENS.Density):
  def __init__(self, loc, scale):
    self.loc = loc
    self.scale = scale

  def pdf(self,x,params=None):
    out = stats.laplace.pdf(x, loc = self.loc, scale = self.scale)
    return out.flatten()

  def log_pdf(self,x,params=None):
    out = stats.laplace.logpdf(x, loc = self.loc, scale = self.scale)
    return out.flatten()

  def rvs(self,n):
    out = stats.laplace.rvs(size=n)
    return out

  def quadrature(self,qtype,qparams):
    if qtype == 0:
      x = qparams
      l = qparams.shape[0]
      w = np.ones(l)/l
    else:
      raise NotImplementedError("Not implemented")
    return (x,w)

class Logistic(DENS.Density):
  def __init__(self):
      self.params = []

  def pdf(self,x,params=None):
    out = stats.logistic.pdf(x)
    return out.flatten()

  def log_pdf(self,x,params=None):
    out = stats.logistic.logpdf(x)
    return out.flatten()

  def rvs(self,n):
    out = stats.logistic.rvs(size=n)
    return out

  def quadrature(self,qtype,qparams):
    if qtype == 0:
      x = qparams
      l = qparams.shape[0]
      w = np.ones(l)/l
    else:
      raise NotImplementedError("Not implemented")
    return (x,w)

class Cauchy(DENS.Density):
  def __init__(self, loc, scale):
    self.loc = loc
    self.scale = scale

  def pdf(self,x,params=None):
    out = stats.cauchy.pdf(x, loc = self.loc, scale = self.scale)
    return out.flatten()

  def log_pdf(self,x,params=None):
    out = stats.cauchy.logpdf(x, loc = self.loc, scale = self.scale)
    return out.flatten()

  def rvs(self,n):
    out = stats.cauchy.rvs(size=n)
    return out

  def quadrature(self,qtype,qparams):
    if qtype == 0:
      x = qparams
      l = qparams.shape[0]
      w = np.ones(l)/l
    else:
      raise NotImplementedError("Not implemented")
    return (x,w)

class GumbelR(DENS.Density):
  def __init__(self, scale):
    self.scale = scale

  def pdf(self,x,params=None):
    out = stats.gumbel_r.pdf(x, scale = self.scale)
    return out.flatten()

  def log_pdf(self,x,params=None):
    out = stats.gumbel_r.logpdf(x, scale = self.scale)
    return out.flatten()

  def rvs(self,n):
    out = stats.gumbel_r.rvs(scale=self.scale, size=n)
    return out

  def quadrature(self,qtype,qparams):
    if qtype == 0:
      x = qparams
      l = qparams.shape[0]
      w = np.ones(l)/l
    else:
      raise NotImplementedError("Not implemented")
    return (x,w)

class MixedNormal(DENS.Density):
  def __init__(self, mu1, sigma1, mu2, sigma2):
    self.mu1 = mu1
    self.sigma1 = sigma1
    self.mu2 = mu2
    self.sigma2 = sigma2

  def pdf(self,x,params=None):
    out = .5*stats.norm.pdf(x, loc = self.mu1, scale = self.sigma1) + .5*stats.norm.pdf(x,
        loc = self.mu2, scale = self.sigma2)
    return out.flatten()

  def log_pdf(self,x,params=None):
    pdf = .5*stats.norm.pdf(x, loc = self.mu1, scale = self.sigma1) + .5*stats.norm.pdf(x,
        loc = self.mu2, scale = self.sigma2)
    out = np.log(pdf)
    return out.flatten()

  def rvs(self,n):
    data1 = stats.norm.rvs(self.mu1, self.sigma1, size=int(n/2))
    data2 = stats.norm.rvs(self.mu2, self.sigma2, size=int(n/2))
    out = np.concatenate([data1,data2])
    return out

  def quadrature(self,qtype,qparams):
    if qtype == 0:
      x = qparams
      l = qparams.shape[0]
      w = np.ones(l)/l
    else:
      raise NotImplementedError("Not implemented")
    return (x,w)

