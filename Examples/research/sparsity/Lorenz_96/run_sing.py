#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import numpy as np
import matplotlib 
import matplotlib.pyplot as plt
import scipy.io as sio
import TransportMaps.Algorithms.SparsityIdentification as SI

# define parameters
nsamps       = np.floor(np.logspace(1,4,num=10))
name_string  = 'L96'
order        = 1
d            = 20
ordering     = SI.ReverseCholesky()
deltas       = [1,2,3,4,5]
delta_string = ['1','2','3','4','5']

# load data from file
DATAFILE = './data/L96_data.mat'
all_data = sio.loadmat(DATAFILE)
all_data = all_data['data']

for N in nsamps:

    # extract subset of data
    data = all_data[:int(N),:]
    print('data.shape = ',data.shape)

    # scale the data
    mean_vec = np.mean(data,axis=0)
    print("mean_vec = ",mean_vec)
    data1 = data - mean_vec
    print("shifted data = ",data1)
    inv_var = 1./(np.var(data1,axis=0))
    print("inv var = ",inv_var)
    inv_std = np.diag(np.sqrt(inv_var))
    rescaled_data = np.dot(data1,inv_std)
    print("rescaled data = ", rescaled_data[0:2,:],"\n")
    print("****************************************************\n")
    processed_data = rescaled_data

    # run SING for each delta
    for i, delta in enumerate(deltas):
        recovered_gp = SI.SING(processed_data, order, ordering, delta)
        print("gp = ",recovered_gp)
        
        # extra adjacency matrix from GP
        dim = recovered_gp.shape[0]
        adjacency = np.zeros([dim, dim])
        adjacency[recovered_gp != 0] = 1
       
        # plot GP and adjacency matrix 
        fig, axs = plt.subplots(1,2, figsize=(12,4))
        
        im1 = axs[0].imshow(adjacency)
        plt.colorbar(im1, ax=axs[0])
        axs[0].set_xticks(np.arange(d))
        axs[0].set_yticks(np.arange(d))

        im2 = axs[1].imshow(recovered_gp/np.max(recovered_gp))
        plt.colorbar(im2, ax=axs[1])
        axs[1].set_xticks(np.arange(d))
        axs[1].set_yticks(np.arange(d))
        
        fig.savefig("./output/adj-gp-%s-%d-%d-%s-%d.pdf" %(name_string,N,dim,delta_string[i],order))

# -- END OF FILE --
