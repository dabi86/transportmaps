% Author: Ricardo Baptista and Alessio Spantini and Youssef Marzouk
% Date:   May 2019
%
% See LICENSE.md for copyright information
%

%% ----------------------------
%% SETUP DA PROBLEM
%% ----------------------------

% Define number of variables
d = 20;

% Define number of steps
T = 10000;
T_SpinUp = 1000;

% Define time-stepping
dt = 0.01;
dt_iter = 40;

% set initial condition for data generation & spin-up
m0 = zeros(d,1);
C0 = eye(d);
x0 = (m0 + sqrtm(C0)*randn(d,1))';

% Setup forward operator
F = 8;
for_op = @(xt, t, dt) rk4(@(t,y) lorenz96(t,y,F), xt, t, dt);

% define model
model = struct;
model.d       = d;
model.dt      = dt;
model.dt_iter = dt_iter;
model.m0      = m0;
model.C0      = C0;
model.x0      = x0;
model.for_op  = for_op;

% run dynamics and generate data
model = generate_data(model, T);

% remove spin-up samples
data = model.xt(:,T_SpinUp:end);
save('L96_data','model','data');

% plot auto-correlation between the chain elements
figure('position',[0,0,1500,1500])
for i=1:d
	subplot(4,5,i)
	autocorr(data(i,:))
end
print('-depsc','autocorr_L96')
% -- END OF FILE --
