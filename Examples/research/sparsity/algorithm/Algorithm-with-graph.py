#
# This file is part of TransportMaps.
#

import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from tabulate import tabulate
from scikits.sparse.cholmod import cholesky
from scipy.sparse import csc_matrix
from scipy.sparse import csr_matrix

import SpectralToolbox.Spectral1D as S1D

import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.tests.TestFunctions as TF

import ExtraDensities
import GaussianGraphs as gg
import GeneralizedPrecision
import NodeOrdering

import itertools

# fix random seed
#np.random.seed(4)

def sing(graph, data0, omega0, order, ordering, delta):
    #initial setup
    dim = graph.dim
    nsamps = data0.shape[0]
    nax = np.newaxis
    setup = {}
    pi = {}
    setup['mu'] = np.zeros(dim)
    setup['sigma2'] = np.identity(dim)
    setup['dim'] = dim
    
    #print("initial omega or adjacency matrix = ")
    print(omega0)
    # create random permutation
    random_perm = np.random.permutation(dim)
    # inverse of random perm
    inv_random_perm = [0] * dim
    for i, p in enumerate(random_perm):
        inv_random_perm[p] = i
    # permute the ordering of the variables, randomly
    data = data0[:,random_perm]
    Omega = omega0[:,random_perm][random_perm,:]
    #print("permuted omega = ")
    print(Omega)
    
    #going from target (samples) to standard normal
    #pi['base_density'] = ExtraDensities.SamplesDensity(data)
    pi['target_density'] = DENS.StandardNormalDensity(dim)
    #will keep support map but shouldn't need it in this example
    pi['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.zeros(dim),
                                                              np.ones(dim))
    # APPROXIMATION
    MONOTONE = 'intexp'
    
    # REGULARIZATION
    # None: No regularization
    # L2: L2 regularization
    REG = None
    # REG = {'type': 'None',
    #        'alpha': 1e-3}
    if REG is None:
        tit_reg = 'None'
    elif REG['type'] == 'L2':
        tit_reg = REG['type'] + '-' + str(REG['alpha'])
    
    # L2 error estimation
    # 3: Gauss quadrature of order n
    # 0: Monte Carlo quadrature with n point
    qtype = 0
    qparams = nsamps
    tit_intest = str(qtype) + '-' + str(qparams)
    
    # Gradient information
    # 0: derivative free
    # 1: gradient information
    # 2: Hessian information
    ders = 2
    
    # MC samples / # of samples in pi data file
    # Tolerance
    tol = 1e-5
    
    # Generate samples of reference and target
    ###ref_samp = test['base_density'].rvs(N_samp)
    ###T_samp = test['support_map']( test['target_map'](ref_samp) )
    
    # ref_samp used for plotting (reset if needed)
    #ref_samp = data
    #T_samp = pi['target_density'].rvs(N_samp)
    
    # Define header for the output log
    if MONOTONE == 'polyconst':
        tit_type = 'poly_enf'
        log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
    elif MONOTONE == 'intexp':
        tit_type = 'monot'
        if ders in [0,1]:
            log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
        else:
            log_header = ['Order','#F.ev.','#J.ev','#H.ev','$L^2$-err']
    
    log = [ ]
    
    # Target density to be approximated L^\sharp \pi_{\rm tar}
    target_density = DENS.PullBackTransportMapDensity(pi['support_map'],
                                                      pi['target_density'])
    
    #use all variables to begin
    active_vars=None
    #set constant tolerance, for now
    tau = 3.e-2
    #set initial sparsity level
    sparsity = [0]
    sparsityIncreasing = True
    #set initial 
    n_active_vars = [(np.power(dim,2) + dim)/2]
    #create list to store permutations
    perm_list = []
    #create total_perm vector
    total_perm = np.arange(dim)
    
    while sparsityIncreasing:
    
       # define base_density from samples
       pi['base_density'] = ExtraDensities.SamplesDensity(data)
    
       # Build the transport map (isotropic for each entry)
       #active_vars = [[0],[0,1],[0,1,2],[2,3],[3,4]]
       tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
              setup['dim'], order, active_vars=active_vars)
       # Construct density
       # Density T_\sharp \pi
       tm_density = DENS.PushForwardTransportMapDensity(tm_approx, pi['base_density'])
        
       # SOLVE
       log_entry_solve = tm_density.minimize_kl_divergence(target_density, qtype=qtype,
                                                           qparams=qparams,
                                                           regularization=REG,
                                                           tol=tol, ders=ders)
       log_entry = [order] + log_entry_solve
       
       pb_density = DENS.PullBackTransportMapDensity(tm_approx, pi['target_density'])

       omegaHat = GeneralizedPrecision.gen_precision(pb_density, data)
       
       # reorder omega and compute l2 error
       Omega_reorder = Omega[:,total_perm][total_perm,:]
       # print("\n\nFrob. Error= ", np.linalg.norm(Omega_reorder - omegaHat, ord='fro'))
    
       # threshold omegaHat and find cholesky ordering
       gp_var = GeneralizedPrecision.var_omega(pb_density, data)
       tau = delta * np.sqrt(gp_var) # set tolerance as square root of variance
       #omegaHat[np.abs(omegaHat) < tau] = 0
       new_omegaHat_matrix = omegaHat
       new_omegaHat_diagonal = np.copy(np.diag(omegaHat))
       #print("omega is currently.....\n",omegaHat)
       #print("diagonal:\n", new_omegaHat_diagonal)
       new_omegaHat_matrix[np.abs(omegaHat) < tau] = 0
       new_omegaHat_matrix[np.diag_indices_from(omegaHat)] = new_omegaHat_diagonal
       omegaHat = new_omegaHat_matrix

       # reorder variables and data
       perm_vect = ordering.perm(omegaHat)
       # perm_vect = NodeOrdering.min_fill(omegaHat)
       # apply to omegaHat
       omegaHat = omegaHat[:,perm_vect][perm_vect,:]
       # apply re-ordering to data
       data = data[:, perm_vect]
    
       # add permutation to perm_list
       perm_list.append(perm_vect)
       #print(perm_vect)
    
       # convolve permutation with total_perm
       total_perm = total_perm[perm_vect]
       #print(total_perm)
       inverse_perm = [0] * dim
       for i, p in enumerate(total_perm):
           inverse_perm[p] = i
    
       # extract lower triangular matrix
       omegaHatLower = np.tril(omegaHat)
    
       # variable elimination moving from highest node (dim-1) to node 2 (at most)
       for i in range(dim-1,1,-1):
           non_zero_ind  = np.where(omegaHatLower[i,:i] != 0)[0]
           if len(non_zero_ind) > 1:
               co_parents = list(itertools.combinations(non_zero_ind,2))
               for j in range(len(co_parents)):
                   row_index = max(co_parents[j])
                   col_index = min(co_parents[j])
                   omegaHatLower[row_index, col_index] = 1.0
    
       # find list of active_vars   
       active_vars = []
       for i in range(dim):
           actives = np.where(omegaHatLower[i,:] != 0)
           active_list = list(set(actives[0]) | set([i]))
           active_list.sort(key=int)
           active_vars.append(active_list)
    
       
       print("\n\nOmegaHat = ", omegaHat)
       #print("\n\nOmegaHatLower = ", omegaHatLower)
       #print("\n\nActive_Vars = ", active_vars)
    
       # find n_active_vars
       n_active_vars.append(np.sum([len(x) for x in active_vars]))
    
       # find current sparsity level
       sparsity.append(n_active_vars[0] - n_active_vars[-1])
       #print(sparsity)
    
       # set sparsityIncreasing
       if sparsity[-1] <= sparsity[-2]:
           sparsityIncreasing = False
    
    #compare omega to original
    o2 = omegaHat[:,inverse_perm][inverse_perm,:]
    o3 = o2[:,inv_random_perm][inv_random_perm,:]
    #print("Original omega = \n",omega0)
    print("Recovered omega = \n",o3)
    
    return o3

# # count errors made by algorithm
# error1 = 0 #not as bad: component is zero, but alg finds it non-zero
# error2 = 0 #bad: component is non-zero, but alg sets it zero
# for i in range(dim):
#     for j in range(dim):
#         if omega0[i,j] == 0 and o3[i,j] != 0:
#             error1 += 1
#         elif omega0[i,j] != 0 and o3[i,j] == 0:
#             error2 += 1

# print("error 1 type  = ",error1)
# print("error 2 type  = ",error2)
