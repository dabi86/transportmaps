#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import TransportMaps.Algorithms.SparsityIdentification as SI
# import Algorithm
# import NodeOrdering as no
# import NonGaussianGraphs as ngg
import scipy.io as sio

d = 5 # max d = 40
nsamps = 118
DATAFILE = '../data_samples/gene_data/gene_datafile.txt'
data = np.genfromtxt(DATAFILE,delimiter=' ',dtype=float)
# data = np.log(data[1:d+1,-118:])
# take first and last genes
data1 = np.log(data[1:6,-118:])
data2 = np.log(data[35:,-118:])
data = np.concatenate((data1,data2))
print(data.shape)
data = data.T
# scale the data
mean_vec = np.mean(data,axis=0)
data1 = data - mean_vec
inv_var = 1./(np.var(data1,axis=0))
inv_std = np.diag(np.sqrt(inv_var))
rescaled_data = np.dot(data1,inv_std)
print("rescaled data = ", rescaled_data[0:2,:],"\n")
print("****************************************************\n")
# add gaussian noise
#dim = data.shape[1]
#mean_noise = np.zeros(dim)
#cov_noise = 1e-1*np.eye(dim)
#noise = np.random.multivariate_normal(mean_noise, cov_noise, nsamps)
#print("noise = ", noise[0:2,:],"\n")
#print("****************************************************\n")
#processed_data = rescaled_data + noise
#print("processed data = ", processed_data[0:2,:],"\n")
#print("****************************************************\n")
#print("covariance of rescaled data = ", np.cov(rescaled_data.T))
#print("****************************************************\n")
#print("covariance of processed data = ", np.cov(processed_data.T))

order = 2
ordering = SI.ReverseCholesky()
# deltas = [3.,3.5,4.,4.5,5.,5.5,6.]
# delta_string = [3.,3.5,4.,4.5,5.,5.5,6.]
deltas = [2]
delta_string = ['2']
for i, delta in enumerate(deltas):
    recovered_gp = SI.SING(rescaled_data, order, ordering, delta)
    
    dim = recovered_gp.shape[0]
    adjacency = np.zeros([dim, dim])
    adjacency[recovered_gp != 0] = 1
    
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    im1 = ax1.imshow(adjacency)
    im2 = ax2.imshow(recovered_gp/np.max(recovered_gp))
    fig2.colorbar(im2)
    
    plt.show()
    
    # fig1.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/gene_data/adj-%d-%d-%s-%d.pdf" %(nsamps,dim,delta_string[i],order))
    # fig2.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/gene_data/gp-%d-%d-%s-%d.pdf" %(nsamps,dim,delta_string[i],order))
