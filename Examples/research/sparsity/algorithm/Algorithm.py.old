# This file contains SING algorithm
# inputs: data (n x d), polynomial order, node ordering method, tolerance delta
# output: sparse edge set
#

import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from tabulate import tabulate
from scikits.sparse.cholmod import cholesky
from scipy.sparse import csc_matrix
from scipy.sparse import csr_matrix

import SpectralToolbox.Spectral1D as S1D

import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.tests.TestFunctions as TF

import ExtraDensities
import GaussianGraphs as gg
import GeneralizedPrecision
import NodeOrdering

import itertools

def SING(data, p_order, n_ordering, delta):
    # Initial setup
    # data is (n x d)
    dim = data.shape[1]
    n_samps = data.shape[0]
    nax = np.newaxis
    pi = {}

    # Target density is standard normal
    pi['target_density'] = DENS.StandardNormalDensity(dim)
    # Support map is the identity
    pi['support_map'] = MAPS.FrozenLinearDiagonalTransportMap(np.zeros(dim), np.ones(dim))
    # APPROXIMATION
    MONOTONE = 'intexp'
    # REGULARIZATION
    REG = None
    # L2 error estimation
    # 3: Gauss quadrature of order n
    # 0: Monte Carlo quadrature with n point
    qtype = 0
    qparams = n_samps
    # Gradient information
    # 0: derivative free
    # 1: gradient information
    # 2: Hessian information
    ders = 2
    # Tolerance in optimization
    tol = 1e-5
    # Log stores information from optimization
    log = [ ]

    # Target density to be approximated
    target_density = DENS.PullBackTransportMapDensity(pi['support_map'],
                                                      pi['target_density'])
    # All variables are active variables to begin
    active_vars=None
    # Set initial sparsity level
    sparsity = [0]
    sparsityIncreasing = True
    # Number of active variables
    n_active_vars = [(np.power(dim,2) + dim)/2]
    # Create list to store permutations
    perm_list = []
    # Create total_perm vector
    total_perm = np.arange(dim)
    
    while sparsityIncreasing:
    
       # Define base_density from samples
       pi['base_density'] = ExtraDensities.SamplesDensity(data)
    
       # Build the transport map (isotropic for each entry)
       #tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
       #        dim, p_order, active_vars=active_vars)
       tm_approx = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
              dim, p_order, active_vars=active_vars)
       # Construct density T_\sharp \pi
       tm_density = DENS.PushForwardTransportMapDensity(tm_approx, pi['base_density'])
        
       # SOLVE
       log_entry_solve = tm_density.minimize_kl_divergence(target_density, qtype=qtype,
                                                           qparams=qparams,
                                                           regularization=REG,
                                                           tol=tol, ders=ders)
       # log_entry = [p_order] + log_entry_solve
       
       pb_density = DENS.PullBackTransportMapDensity(tm_approx, pi['target_density'])

       # Compute generalized precision
       omegaHat = GeneralizedPrecision.gen_precision(pb_density, data)
       
       # Compute variance of generalized precision
       gp_var = GeneralizedPrecision.var_omega(pb_density, data)
       # Compute tolerance (matrix)
       tau = delta * np.sqrt(gp_var) # set tolerance as square root of variance

       # Save diagonal elements
       omegaHat_diagonal = np.copy(np.diag(omegaHat))
       # Threshold omegaHat
       omegaHat[np.abs(omegaHat) < tau] = 0
       # Put diagonal elements back in
       omegaHat[np.diag_indices_from(omegaHat)] = omegaHat_diagonal

       # Reorder variables and data
       perm_vect = n_ordering.perm(omegaHat)
       # Apply to omegaHat
       omegaHat = omegaHat[:,perm_vect][perm_vect,:]
       # Apply re-ordering to data
       data = data[:, perm_vect]
    
       # Add permutation to perm_list
       perm_list.append(perm_vect)
       # Convolve permutation with total_perm
       total_perm = total_perm[perm_vect]
       inverse_perm = [0] * dim
       for i, p in enumerate(total_perm):
           inverse_perm[p] = i
    
       # Extract lower triangular matrix
       omegaHatLower = np.tril(omegaHat)
    
       # Variable elimination moving from highest node (dim-1) to node 2 (at most)
       for i in range(dim-1,1,-1):
           non_zero_ind  = np.where(omegaHatLower[i,:i] != 0)[0]
           if len(non_zero_ind) > 1:
               co_parents = list(itertools.combinations(non_zero_ind,2))
               for j in range(len(co_parents)):
                   row_index = max(co_parents[j])
                   col_index = min(co_parents[j])
                   omegaHatLower[row_index, col_index] = 1.0
    
       # Find list of active_vars
       active_vars = []
       for i in range(dim):
           actives = np.where(omegaHatLower[i,:] != 0)
           active_list = list(set(actives[0]) | set([i]))
           active_list.sort(key=int)
           active_vars.append(active_list)
    
       # Find n_active_vars
       n_active_vars.append(np.sum([len(x) for x in active_vars]))
    
       # Find current sparsity level
       sparsity.append(n_active_vars[0] - n_active_vars[-1])
    
       # Set sparsityIncreasing
       if sparsity[-1] <= sparsity[-2]:
           sparsityIncreasing = False
   
    # Recovered omega (same variable order as input ordering)
    rec_omega = omegaHat[:,inverse_perm][inverse_perm,:]
    
    return rec_omega
