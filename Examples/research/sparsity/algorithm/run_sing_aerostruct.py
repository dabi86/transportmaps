#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import matplotlib
# matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import TransportMaps.Algorithms.SparsityIdentification as SI

nsamps = 10000
DATAFILE = '../data_samples/aerostruct/aerostruct_matout.mat'
all_data = sio.loadmat(DATAFILE)
output_data = all_data['mc_output']

# chose subset of data
# 0-41 are loads
# 42-83 are deflections
# 84: normalized fuel burn
# 85: lift coefficient
# 86: failure criteria
var_list = range(6)
# var_list = range(52,64)
var_list = range(0,41,6) #x:0,y:1,z:2
var_list = [0,1,2,6,7,8,42,43,44,45,46,47,86] #
var_list = [0,1,2,6,7,8,42,43,44,45,46,47,86] #
var_list = [0,1,2,6,7,8,30,31,32,36,37,38]
var_list = [30,31,32,36,37,38]
var_list = [0,1,2,30,31,32,36,37,38] # forces: makes a nice grid, order=1, delta=5, noise:1e-7
# var_list = [0,1,2,6,7,8,30,31,32,36,37,38] # forces: order=1, delta=2, noise:1e-7, missing three edges but reveals block structure
# var_list = [0,1,2,6,7,8,36,37,38] # also works with order=1, delta=2
# var_list = [3,4,5,33,34,35,39,40,41] # moments: order=1, delta=5, reveals same structure as forces
# var_list = [42,43,44,78,79,80] # deflections: order=1, delta=2
# var_list = [2,8,44,50,86] #
# var_list = [2,44,85] #
# var_list = [0,1,2,30,31,32] # forces: order=1, delta=5, noise:1e-7
data2 = output_data[:nsamps,var_list[0]]
for i, v in enumerate(var_list[1:]):
    data2 = np.vstack((data2,output_data[:nsamps,v]))
data = data2.T
# Scale the data
mean_vec = np.mean(data,axis=0)
print("mean_vec = ",mean_vec)
data1 = data - mean_vec
print("shifted data = ",data1)
inv_var = 1./(np.var(data1,axis=0))
print("inv var = ",inv_var)
inv_std = np.diag(np.sqrt(inv_var))
rescaled_data = np.dot(data1,inv_std)
print("rescaled data = ", rescaled_data[0:2,:],"\n")
print("****************************************************\n")
# add gaussian noise
dim = data.shape[1]
mean_noise = np.zeros(dim)
# TESTING NOISE
cov_noise = 1e-7*np.eye(dim)
noise = np.random.multivariate_normal(mean_noise, cov_noise, nsamps)
print("noise = ", noise[0:2,:],"\n")
print("****************************************************\n")
processed_data = rescaled_data + noise
print("processed data = ", processed_data[0:2,:],"\n")
print("****************************************************\n")
print("covariance of rescaled data = ", np.cov(rescaled_data.T))
print("****************************************************\n")
print("covariance of processed data = ", np.cov(processed_data.T))
print("condition number of covariance of processed data = ", np.linalg.cond(np.cov(processed_data.T)))

order = 1
ordering = SI.ReverseCholesky()
# deltas = [3.,3.5,4.,4.5,5.,5.5,6.]
# delta_string = [3.,3.5,4.,4.5,5.,5.5,6.]
deltas = [5]
delta_string = ['5']
for i, delta in enumerate(deltas):
    recovered_gp = SI.SING(processed_data, order, ordering, delta)
    print("gp = ",recovered_gp)
    
    dim = recovered_gp.shape[0]
    adjacency = np.zeros([dim, dim])
    adjacency[recovered_gp != 0] = 1
    
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    im1 = ax1.imshow(adjacency)
    fig1.colorbar(im1)
    im2 = ax2.imshow(recovered_gp/np.max(recovered_gp))
    fig2.colorbar(im2)
    
    plt.show()
    
    # fig1.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/satellite/adj-%d-%d-%s-%d.pdf" %(nsamps,dim,delta_string[i],order))
    # fig2.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/satellite/gp-%d-%d-%s-%d.pdf" %(nsamps,dim,delta_string[i],order))
