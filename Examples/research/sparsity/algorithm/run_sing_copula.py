#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import matplotlib
# matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import TransportMaps.Algorithms.SparsityIdentification as SI

nsamps = 2000
data_type = 1
if data_type == 0:
    DATAFILE = '../data_samples/copula_data/power_data-s-3.txt'
    name_string = 'power'
    OMEGAFILE = '../data_samples/copula_data/power_omega-s-3.txt'
elif data_type == 1:
    DATAFILE = '../data_samples/copula_data/cdf_data.txt'
    name_string = 'cdf'
    data = np.loadtxt(DATAFILE)
    OMEGAFILE = '../data_samples/copula_data/cdf_omega.txt'
    omega_true = np.loadtxt(DATAFILE)

data = np.loadtxt(DATAFILE)
omega_true = np.loadtxt(OMEGAFILE)
data = data[:nsamps,:]
print('data.shape = ',data.shape)

# Scale the data
mean_vec = np.mean(data,axis=0)
print("mean_vec = ",mean_vec)
data1 = data - mean_vec
print("shifted data = ",data1)
inv_var = 1./(np.var(data1,axis=0))
print("inv var = ",inv_var)
inv_std = np.diag(np.sqrt(inv_var))
rescaled_data = np.dot(data1,inv_std)
print("rescaled data = ", rescaled_data[0:2,:],"\n")
print("****************************************************\n")
# add gaussian noise
# dim = data.shape[1]
# mean_noise = np.zeros(dim)
# # TESTING NOISE
# cov_noise = 1e-7*np.eye(dim)
# noise = np.random.multivariate_normal(mean_noise, cov_noise, nsamps)
# print("noise = ", noise[0:2,:],"\n")
# print("****************************************************\n")
# processed_data = rescaled_data + noise
# print("processed data = ", processed_data[0:2,:],"\n")
# print("****************************************************\n")
# print("covariance of rescaled data = ", np.cov(rescaled_data.T))
# print("****************************************************\n")
# print("covariance of processed data = ", np.cov(processed_data.T))
processed_data = rescaled_data

order = 2
ordering = SI.ReverseCholesky()
# deltas = [3.,3.5,4.,4.5,5.,5.5,6.]
# delta_string = [3.,3.5,4.,4.5,5.,5.5,6.]
deltas = [2]
delta_string = ['2']
for i, delta in enumerate(deltas):
    recovered_gp = SI.SING(processed_data, order, ordering, delta)
    print("gp = ",recovered_gp)
    
    dim = recovered_gp.shape[0]
    adjacency = np.zeros([dim, dim])
    adjacency[recovered_gp != 0] = 1
    
    fig1, ax1 = plt.subplots()
    im1 = ax1.imshow(adjacency)
    fig1.colorbar(im1)
    plt.xticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['1','2','3','4','5','6','7','8','9','10'])
    plt.yticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['1','2','3','4','5','6','7','8','9','10'])
    fig2, ax2 = plt.subplots()
    im2 = ax2.imshow(recovered_gp/np.max(recovered_gp))
    fig2.colorbar(im2)
    plt.xticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['1','2','3','4','5','6','7','8','9','10'])
    plt.yticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['1','2','3','4','5','6','7','8','9','10'])
    fig3, ax3 = plt.subplots()
    im3 = ax3.imshow(omega_true)
    fig3.colorbar(im3)
    plt.xticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['1','2','3','4','5','6','7','8','9','10'])
    plt.yticks([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['1','2','3','4','5','6','7','8','9','10'])
    
    # plt.show()
    
    fig1.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/copula/adj-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[i],order))
    fig2.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/copula/gp-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[i],order))
    fig3.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/copula/true-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[i],order))
