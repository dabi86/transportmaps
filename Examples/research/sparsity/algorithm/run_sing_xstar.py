#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import matplotlib
# matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import TransportMaps.Algorithms.SparsityIdentification as SI
from scipy.stats import norm
from scipy.stats import uniform
np.random.seed(1)

nsamps = 10000
p = 4
X = norm.rvs(size=(nsamps,p))
W = uniform.rvs(size=(nsamps,p))
print(X.shape)
Xstar = np.zeros((nsamps,p))
# # like paper
# for i in range(p-1):
#     Xstar[:,i] = X[:,i+1]*np.sign(X[:,i])
# Xstar[:,p-1] = X[:,0]*np.sign(X[:,p-1])

# # chain???
# for i in range(p-1):
#     Xstar[:,i] = X[:,i+1]*X[:,i]
# Xstar[:,p-1] = X[:,0]*X[:,p-1]

# # chain??? .............NOT DONE.......................
# for i in range(0,p-1):
#     Xstar[:,i+1] = X[:,i] + X[:,i+1]
# Xstar[:,0] = X[:,0] + X[:,p-1]

# set random graph
s = 3 # controls level of sparsity
s_string = '3'
Omega = np.zeros((p,p)) #inverse correlation matrix, to be filled
# chain graph
np.fill_diagonal(Omega,1)
np.fill_diagonal(Omega[1:],.4)
np.fill_diagonal(Omega[:,1:],.4)
# for i in range(p):
#     edge_i = 0
#     for j in range(i):
#         # sample Y_i^1, Y_i^2, Y_j^1, Y_j^2
#         y_i = np.random.rand(2,1)
#         y_j = np.random.rand(2,1)
#         p_ij = 1/(2*np.pi)*np.exp(-np.linalg.norm(y_i - y_j)**2/2/s)
#         alpha = np.random.rand(1)
#         if p_ij > alpha:
#             if edge_i < 4:
#                 Omega[i][j] = .245
#                 Omega[j][i] = .245
#                 edge_i +=1
#     Omega[i][i] = 1

print("Omega = ", Omega)
# covariance matrix
Sigma = np.linalg.inv(Omega)
mu1 = np.ones((p,))*2
mu2 = np.ones((p,))*-2
norm_data1 = np.random.multivariate_normal(mu1, Sigma, nsamps)
norm_data2 = np.random.multivariate_normal(mu2, Sigma, nsamps)
data = np.append(norm_data1,norm_data2,axis=0)

orders = [1,2,3,4]
ordering = SI.ReverseCholesky()
# deltas = [3.,3.5,4.,4.5,5.,5.5,6.]
# delta_string = [3.,3.5,4.,4.5,5.,5.5,6.]
deltas = [2]
delta_string = ['2']
name_string = 'multimodal2-2'
xticks = [1,2,3,4]
yticks = [4,3,2,1]
# for i, delta in enumerate(deltas):
for i, order in enumerate(orders):
    recovered_gp = SI.SING(data, order, ordering, deltas[0])
    print("gp = ",recovered_gp)
    
    dim = recovered_gp.shape[0]
    adjacency = np.zeros([dim, dim])
    adjacency[recovered_gp != 0] = 1
    
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    im1 = ax1.imshow(adjacency)#, extent=[1,4,4,1])#,interpolation='none')
    plt.xticks([0, 1, 2, 3], ['1','2','3','4'])
    plt.yticks([3, 2, 1, 0], ['4','3','2','1'])
    fig1.colorbar(im1)
    im2 = ax2.imshow(recovered_gp/np.max(recovered_gp))#, extent=[1,4,4,1])#,interpolation='none')
    plt.xticks([0, 1, 2, 3], ['1','2','3','4'])
    plt.yticks([3, 2, 1, 0], ['4','3','2','1'])
    # ax2.xaxis.set_major_locator(plt.MaxNLocator(4))
    fig2.colorbar(im2)
    
    # plt.show()
    
    fig1.savefig("./adj-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[0],orders[i]))
    #fig1.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/multimodal/adj-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[0],orders[i]))
    #fig2.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/multimodal/gp-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[0],orders[i]))

# fig3, ax3 = plt.subplots()
# im3 = ax3.imshow(Omega, extent=[1,4,4,1])
# fig3.colorbar(im3)
# fig3.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/multimodal/true-%s.pdf" %(name_string)) #,nsamps,dim,delta_string[0],orders[i]))
