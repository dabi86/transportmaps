#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import Algorithm
import NodeOrdering as no
import NonGaussianGraphs as ngg
import scipy.io as sio
import h5py

nsamps = 5000
dim = 8
f = h5py.File('/home/rmorriso/Dropbox/Sparsity/StochasticVolatility/SVH-n10-sigma025-sequential-postprocess.dill.hdf5','r')
samples = f['quadrature']['0'][:,:]
data = samples[:nsamps,:dim]
# Scale the data
mean_vec = np.mean(data,axis=0)
data1 = data - mean_vec
inv_var = 1./(np.var(data1,axis=0))
inv_std = np.diag(np.sqrt(inv_var))
rescaled_data = np.dot(data1,inv_std)

order = 2
ordering = no.ReverseCholesky()
# deltas = [3.,3.5,4.,4.5,5.,5.5,6.]
# delta_string = [3.,3.5,4.,4.5,5.,5.5,6.]
deltas = [2]
delta_string = ['2']
for i, delta in enumerate(deltas):
    recovered_gp = Algorithm.SING(rescaled_data, order, ordering, delta)
    
    np.savetxt("gp-sv.txt",recovered_gp)
