#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2016 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Daniele Bigoni
# E-mail: dabi@limitcycle.it
#

import math
# import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

import SpectralToolbox.Spectral1D as S1D

import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.tests.TestFunctions as TF

import ExtraDensities

nax = np.newaxis

# PLOTTING
#PLOTTING = False
#fsize = (6,5)
#discr = 51

#set up target distribution
#right now, 1D gaussian
setup = {}
sand = {}
setup['mu'] = np.array([0.])
setup['sigma2'] = np.array([[1.]])
setup['dim'] = 1
#setup['refplotspan'] = np.array([[-7.,7.]])
#setup['plotspan'] = np.array([[-6.,6.]])
#title = 'Inverse_Rosenblatt_dimension_%s' % (str(setup['dim']))

#tests available:
#0: sandia samples (hermite gaussian polynomial chaos expansions)
#1: laplace distribution
#2: cauchy distribution
#3: gumbel (right skew) distribution
#4: mixed gaussian
testn = 1
if testn == 0:
    #not complete, see mfs-kl.py
    datafile = '../../../rosenblatttests/samples_invros/samples_d1_n'+str(nsamps)+'_r2.dat'
    data = np.loadtxt(datafile, comments="%")
elif testn == 1:
    target_den = ExtraDensities.Laplace(loc=0,scale=1)
elif testn == 2:
    target_den = ExtraDensities.Cauchy(loc=0,scale=1)
elif testn == 3:
    gumscale = 1.5
    target_den = ExtraDensities.GumbelR(scale=gumscale)
elif testn == 4:
    m1 = 0; s1 = 1;
    m2 = 5; s2 = 1;
    target_den = ExtraDensities.MixedNormal(mu1=m1, mu2 = m2, sigma1 = s1,
            sigma2 = s2)
elif testn == 5:
    mu = 0; sigma = 3;
    data = stats.norm.rvs(mu, sigma, size=nsamps)

#samples and pdf of exact to compute error of approximate density
n_error = 2e4
error_samps = target_den.rvs(n_error)
error_samps = error_samps[:,nax]

#file to save output
filename = 'kl_div_'+str(testn)+'.txt'
file = open(filename,'w')

sand['target_density'] = DENS.StandardNormalDensity(1)
#will keep support map (of target) but shouldn't need it in this example
sand['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.array([0.]),
                                                            np.array([1.]))

# APPROXIMATION
MONOTONE = 'intexp'

# REGULARIZATION
# None: No regularization
# L2: L2 regularization
REG = None
# REG = {'type': 'None',
#        'alpha': 1e-3}
if REG is None:
    tit_reg = 'None'
elif REG['type'] == 'L2':
    tit_reg = REG['type'] + '-' + str(REG['alpha'])

#norm_samp = stats.norm.rvs(size=n_error)
#samp_error = coeffs[0] + coeffs[1]*norm_samp + coeffs[2]*np.multiply(norm_samp,norm_samp)
#samp_error = samp_error[:,nax]

#pc_pdf = PC_Density(alpha,beta,gamma)
for N_samp in (100,300,500,700,1000,3000,5000,10000):
    for n_sets in range(10):
        # # DEBUG START
        print("\nNumber of samples: %d - N exp: %d" % (N_samp, n_sets))
        #create dataset
        data = target_den.rvs(N_samp)
        if data.ndim == 1:
            data = data[:,nax]
        
        #going from target (samples) to standard normal
        sand['base_density'] = ExtraDensities.SamplesDensity(data)
        
        # L2 error estimation
        # 3: Gauss quadrature of order n
        # 0: Monte Carlo quadrature with n point
        qtype = 0
        qparams = math.floor(data.shape[0]/1)
        tit_intest = str(qtype) + '-' + str(qparams)
        
        # Gradient information
        # 0: derivative free
        # 1: gradient information
        # 2: Hessian information
        ders = 2
        
        # Approximation orders
        orders = [1,3,5]
        # orders = [5]
        # Tolerance
        tol = 1e-6
        
        # Generate samples of reference and target
        ###ref_samp = test['base_density'].rvs(N_samp)
        ###T_samp = test['support_map']( test['target_map'](ref_samp) )
        
        ref_samp = data
        T_samp = sand['target_density'].rvs(N_samp)
        
        # PLOTTING PRE-COMPUTATION
        # Start plotting of the 1d map
        #if PLOTTING:
        #    pspan = setup['plotspan']
        #    rspan = setup['refplotspan']
        #
        #    # Plot the 1d map
        #    X1d = np.vstack( (np.linspace(rspan[0,0],rspan[0,1],discr),
        #                      np.zeros((setup['dim']-1,discr))) ).T
        #    #t1 = sand['support_map']( sand['target_map'](X1d) )[:,0] #original code
        #    #compute map S = F_eta^-1 (F_z) where z ~ \pi and \eta ~ N
        #    num_bins = 100;
        #    counts, bin_edges = np.histogram(data_sup,bins=num_bins,normed=True)
        #    dx = bin_edges[1] - bin_edges[0]
        #    ecdf = np.cumsum(counts*dx)
        #    ecdf_eval = np.interp(X1d[:,0],bin_edges[1:],ecdf)
        #    t1 = stats.norm.ppf(ecdf_eval)
        #
        #    fig_map_1d = plt.figure()
        #    ax_map_1d = fig_map_1d.add_subplot(1,1,1)
        #    ax_map_1d.plot(X1d[:,0], t1, label='Empirical CDF')
        #
        #    # Plot the 1d distribution
        #    fig_kde_1d = plt.figure()
        #    ax_kde_1d = fig_kde_1d.add_subplot(1,1,1)
        #    # Exact distribution 1d
        #    X1d_kde = np.linspace(pspan[0,0],pspan[0,1],discr).reshape((discr,1))
        #    # Plot analytic
        ##TODO.................
        #    pdf1d = sand['target_density'].pdf(X1d_kde)
        #    ax_kde_1d.plot(X1d_kde, pdf1d, label='$\pi^1_{tar}$')
        
        # # Define header for the output log
        # if MONOTONE == 'polyconst':
        #     tit_type = 'poly_enf'
        #     log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
        # elif MONOTONE == 'intexp':
        #     tit_type = 'monot'
        #     if ders in [0,1]:
        #         log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
        #     else:
        #         log_header = ['Order','#F.ev.','#J.ev','#H.ev','$L^2$-err']
                
        for i_ord,order in enumerate(orders):
            print("\nOrder: %d" % order)
        
            # Build the transport map (isotropic for each entry)
            approx_list = []
            active_vars = []
            for i in range(setup['dim']):
                c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (i+1)
                c_orders_list = ([ order ] * i) + [0]
                c_approx = FUNC.MonotonicFullOrderLinearSpanApproximation(c_basis_list, c_orders_list)
                e_basis_list = [S1D.ConstantExtendedHermiteProbabilistsFunction()] * (i+1)
                e_orders_list = [order] * (i+1)
                e_approx = FUNC.MonotonicFullOrderLinearSpanApproximation(e_basis_list, e_orders_list)
                approx = FUNC.MonotonicIntegratedExponentialApproximation(c_approx, e_approx)
                approx_list.append( approx )
                active_vars.append( range(i+1) )
            tm_approx = Maps.IntegratedExponentialTriangularTransportMap(active_vars, approx_list)
            # Construct density
            # Density T_\sharp \pi
            tm_density = DENS.PushForwardTransportMapDensity(tm_approx, sand['base_density'])
            # Target density to be approximated L^\sharp \pi_{\rm tar}
            target_density = DENS.PullBackTransportMapDensity(sand['support_map'],
                                                              sand['target_density'])
             
            # SOLVE
            log_entry_solve = tm_density.minimize_kl_divergence(target_density, qtype=qtype,
                                                                qparams=qparams,
                                                                regularization=REG,
                                                                tol=tol, ders=ders)
        
            # Construct approximate density L_\sharp T_\sharp \pi
            approx_density = DENS.PushForwardTransportMapDensity(sand['support_map'], tm_density)
        
            # Push forward reference samples in two steps
            # (1) Map x from \pi to y from T_\sharp \pi
            # (2) Map y from T_\sharp \pi to z from L_\sharp T_\sharp \pi
            tmp = tm_density.map_samples_base_to_target( ref_samp )
            fapp_samp = approx_density.map_samples_base_to_target( tmp ) 
        
            # Pullback standard normal samples to original samples
            #norm_samp = stats.norm.rvs(size=n_samps)
            #norm_samp = norm_samp[:,nax]
            #new_samp = tm_density.map_samples_target_to_base( norm_samp )
            #new_samp_sup = sand['support_mapL'].evaluate(sand['support_mapE'].evaluate(new_samp))
        
            # PLOT
        #    if PLOTTING:
        #        # Map to T_\sharp \pi
        #        t1_approx_tmp = tm_density.map_samples_base_to_target(X1d)
        #        # Map to L_\sharp T_\sharp \pi
        #        t1_approx = approx_density.map_samples_base_to_target(t1_approx_tmp)[:,0]
        #        # Plot 1d transport map
        #        ax_map_1d.plot(X1d[:,0], t1_approx, label='Order %d' % order)
        #
        #        # Plot the 1d distribution
        #        if setup['dim'] == 1:
        #            pdf1d_approx = approx_density.pdf(X1d_kde)
        #        else:
        #            t1_kde_approx = stats.gaussian_kde(fapp_samp[:,0][:,np.newaxis].T)
        #            pdf1d_approx = t1_kde_approx(X1d_kde.T)
        #        ax_kde_1d.plot(X1d_kde, pdf1d_approx, label='Order %d' % order)
        #
        #        # Plot pullback of inverse map
        #        if setup['dim'] == 1:
        #             plt.figure(order+2)
        #             p1 = plt.hist(data[:,0],bins=20,normed=1,alpha=.5)
        #             p2 = plt.hist(new_samp_sup[:,0],bins=20,normed=1,alpha=.5,color='r')
        #             plt.legend(["Original samples","New samples"])
        #             plt.title("Order %s" %(order))
        #
        ##             plt.figure(order+3)
        ##             p1 = plt.hist(data_sup[:,0],bins=20,normed=1,alpha=.5)
        ##             p2 = plt.hist(new_samp[:,0],bins=20,normed=1,alpha=.5,color='r')
        ##             plt.legend(["Original samples","New samples"])
        ##             plt.title("Order %s" %(order))
        ##
        ##             plt.figure(order+4)
        ##             samp_test1 = sand['support_mapL'].evaluate(sand['support_mapE'].evaluate(data_sup))
        ##             samp_test2 = sand['support_mapL'].evaluate(sand['support_mapE'].evaluate(new_samp))
        ##             p1 = plt.hist(samp_test1[:,0],bins=20,normed=1,alpha=.5)
        ##             p2 = plt.hist(samp_test2[:,0],bins=20,normed=1,alpha=.5,color='r')
        ##             plt.legend(["Original samples","New samples"])
        ##             plt.title("Order %s" %(order))
                
            # compute error between exact pdf and approx pullback density
            pb_den = DENS.PullBackTransportMapDensity(tm_approx,
                sand['target_density'])
            kl_error = TM.kl_divergence(target_den,pb_den,qtype=0,qparams=error_samps)
            print(kl_error)
             
    #        z = y.reshape((len(y),1))
    #        z1 = y1.reshape((len(y1),1))
    #        z2 = y2.reshape((len(y2),1))
    #        print('\n---hello1---\n')
    #        pb_eval = pb_den.pdf(z)
    #        pb_eval1 = pb_den.pdf(z1)
    #        pb_eval2 = pb_den.pdf(z2)
    #        #print(pb_eval[0:5])
    #        #print(z[0:5])
    #        print('\n---hello2---\n')
    #    
    #    #    if PLOTTING:
    #    #        plt.figure(9)
    #    #        p1 = plt.plot(y,pb_eval)
    #    #        p2 = plt.plot(y,exact_pdf,'k')
    #        #plt.legend(["Order %s" %(order), "Exact pdf"])
    #        #kl = stats.entropy(pb_eval,exact_pdf)
    #        #compute integrated square error
    #        #err1 = np.trapz(pb_eval,y)
    #        #diff = np.trapz(np.square(pb_eval - exact_pdf),y)
    #       # diff1 = np.trapz(np.square(pb_eval1 - exact_pdf1),y1)
    #       # diff2 = np.trapz(np.square(pb_eval2 - exact_pdf2),y2)
    #       # plt.plot(y,np.square(pb_eval - exact_pdf),'b',y1,np.square(pb_eval1 -
    #       #     exact_pdf1),'r',y2,np.square(pb_eval2 - exact_pdf2),'g')
    #       # plt.legend()
    #       # plt.show()
    #        #intsqerr = np.trapz(np.square(pb_eval - exact_pdf),y)
    #        den_ex = lambda x: 1./np.sqrt(2*np.pi)*(
    #          np.exp(-1/2.*pow(alpha + pow((x + gamma)/beta,1/2),2)) +
    #          np.exp(-1/2.*pow(alpha - pow((x + gamma)/beta,1/2),2))) * np.abs(1/2./beta) * pow((x + gamma)/beta,-1/2.)
    #        mean_integrand = lambda x: x*1./np.sqrt(2*np.pi)*(
    #          np.exp(-1/2.*pow(alpha + pow((x + gamma)/beta,1/2),2)) +
    #          np.exp(-1/2.*pow(alpha - pow((x + gamma)/beta,1/2),2))) * np.abs(1/2./beta) * pow((x + gamma)/beta,-1/2.)
    ##compute first moments
    #        mean_ex = integrate.quad(mean_integrand,-gamma,np.inf)
    #        mean_ap = np.trapz(np.multiply(pb_eval,y),y)
    #        mean_diff = mean_ex[0] - mean_ap
    #
    #        var_integrand = lambda x: np.square(mean_ex[0] - x)*1./np.sqrt(2*np.pi)*(
    #          np.exp(-1/2.*pow(alpha + pow((x + gamma)/beta,1/2),2)) +
    #          np.exp(-1/2.*pow(alpha - pow((x + gamma)/beta,1/2),2))) * np.abs(1/2./beta) * pow((x + gamma)/beta,-1/2.)
    #        var_ex = integrate.quad(var_integrand,-gamma,np.inf)
    #        var_ap = np.trapz(np.multiply(pb_eval,np.square(y-mean_ap)),y)
    #        var_diff = var_ex[0] - var_ap
    #        print('exact variance = '+str(var_ex))
    #        print('approximate variance = '+str(var_ap))
    #        print('difference in mean = '+str(mean_diff))
    #        print('difference in variance = '+str(var_diff))
    
            #den1 = integrate.quad(den1,-gamma,np.inf)
            #intsqerr = integrate.quad(pb_den.pdf,-gamma,np.inf)
            #print(intsqerr)
            
            file.write(str(N_samp)+' '+str(order)+' '+str(kl_error)+'\n')
        
        
        #if PLOTTING:
        #    fig_map_1d.gca().legend(loc=2)
        #    fig_kde_1d.gca().legend()
        #    #plt.show(False)
        #    plt.show()
        
        # # Decorate map plotting
        # # ax_map.set_ylim([f(X[0]), f(X[-1])])
        # ax_map.scatter(x, np.zeros(x.shape))
        # ax_map.set_xlabel('x')
        # ax_map.set_ylabel('$T(x)$')
        # # ax_map.set_ylabel('$ (F^{-1} \circ F_{ref})x $')
        # # ax_map.set_ylim([np.min(exact_tm),np.max(exact_tm)])
        # ax_map.legend(loc='best')
        # ax_map.grid(True)
        
        # # fig_map.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-Approximation.pdf' % \
        # #                 (title, tit_type, tit_reg, tit_intest))
        # # fig_map.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-Approximation.eps' % \
        # #                 (title, tit_type, tit_reg, tit_intest))
        
        # # Decorate kde plotting
        # ax_kde.set_xlabel('x')
        # ax_kde.set_ylabel('PDF')
        # ax_kde.legend(loc='best')
        
        # # fig_kde.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-DistributionApproximation.pdf' % \
        # #                 (title, tit_type, tit_reg, tit_intest))
        # # fig_kde.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-DistributionApproximation.eps' % \
        # #                 (title, tit_type, tit_reg, tit_intest))
        
        # plt.show(False)
file.close()
