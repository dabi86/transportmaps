#plot histograms of the Rosenblatt test samples

#Rosenblatt tests (copied from README)
'''
in samples_invros.tar there are a total of 360 files with names samples_dX_nY_rZ.dat 
X: dimensionality = 1,2,3,5,10,15
Y: ensemble size  = 100,300,500,1000,5000,10000
Z: replica        = 0 to 9

Files are in a simple, text format,i.e. Y-by-X matrix.

Samples are drawn from second order joint HG PC expansions with randomly chosen
coefficient values. From it one can extract exact mean vector and covariance matrix.
Either these, or their estimates from the initial samples, will be used to measure
the performance of pdf-to-pdf maps.

For simplicity, let's generate ensembles of the same size, 
i.e. for each samples_dX_nY_rZ.dat file, create one samples_dX_nY_rZ_new.dat
'''
import matplotlib.pyplot as plt
import numpy as np

nax = np.newaxis

dim = 1;
n = 100;
rep = 0;

#read in the data
datafile = '../../../rosenblatttests/samples_invros/samples_d'+str(dim)+'_n'+str(n)+'_r'+str(rep)+'.dat'
data = np.loadtxt(datafile, comments="%")
if data.ndim == 1:
    data = data[:,nax]

#analytical stuff for 1d
#read in the coefficients
datafile = '../../../rosenblatttests/pcfs/pcf_d'+str(dim)+'_n'+str(n)+'_r'+str(rep)+'.dat'
coeffs = np.loadtxt(datafile, comments="%")
alpha = -coeffs[1]/2/coeffs[2]
beta = coeffs[2]
gamma = pow(coeffs[1],2)/4/coeffs[2] - coeffs[0] + coeffs[2]

epsilon = 1.e-1
if beta > 0:
    y = np.linspace(-gamma + epsilon, +10, 1000)
else:
    y = np.linspace(-gamma - epsilon, -10, 1000)
pdf = 1./np.sqrt(2*np.pi)*(
      np.exp(-1/2.*pow(alpha + pow((y + gamma)/beta,1/2),2)) +
      np.exp(-1/2.*pow(alpha - pow((y + gamma)/beta,1/2),2))) * np.abs(1/2./beta) * pow((y + gamma)/beta,-1/2.)

print(data.ndim)
for i in range(dim):
    print(data.shape)
    plt.figure(i)
    plt.hist(data[:,i],bins=20,alpha=.5,normed=True)

    plt.plot(y,pdf,'r')

plt.show()
