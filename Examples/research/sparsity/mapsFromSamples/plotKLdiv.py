#plot histograms of the Rosenblatt test samples

#Rosenblatt tests (copied from README)
'''
in samples_invros.tar there are a total of 360 files with names samples_dX_nY_rZ.dat 
X: dimensionality = 1,2,3,5,10,15
Y: ensemble size  = 100,300,500,1000,5000,10000
Z: replica        = 0 to 9

Files are in a simple, text format,i.e. Y-by-X matrix.

Samples are drawn from second order joint HG PC expansions with randomly chosen
coefficient values. From it one can extract exact mean vector and covariance matrix.
Either these, or their estimates from the initial samples, will be used to measure
the performance of pdf-to-pdf maps.

For simplicity, let's generate ensembles of the same size, 
i.e. for each samples_dX_nY_rZ.dat file, create one samples_dX_nY_rZ_new.dat
'''
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
from matplotlib import rc
from math import *

rc('text',usetex=True)
font={'family' : 'normal',
    'weight' : 'normal',
    'size' :14}
matplotlib.rc('font',**font)

nax = np.newaxis

n_sets = 10
n_ord = 2
#read in the data
testn = 0
if testn == 0:
    title = "Target: PC Density"
elif testn == 1:
    title = "Target: Laplace Density"
elif testn == 2:
    title = "Target: Cauchy Density"
elif testn == 3:
    title = "Target: Right Gumbel Density"
elif testn == 4:
    title = "Target: Mixed Normals Density"

#datafile = 'kl_div/kl_div_'+str(testn)+'.txt'
datafile = 'kl_div/kldiv0.txt'
data = np.loadtxt(datafile, comments="%")

kl = data[:,2]

x = np.array([100,300,500,750,1000,3000,5000,7000,10000])
y1 = np.zeros(len(x))
y2 = np.zeros(len(x))
y3 = np.zeros(len(x))

for i in range(len(x)):
    y1[i] = 1./n_sets*np.sum(kl[(n_ord*n_sets)*i:(n_ord*n_sets)*(i+1):n_ord])
    y2[i] = 1./n_sets*np.sum(kl[(n_ord*n_sets)*i+1:(n_ord*n_sets)*(i+1)+1:n_ord])
    y3[i] = 1./n_sets*np.sum(kl[(n_ord*n_sets)*i+2:(n_ord*n_sets)*(i+1)+2:n_ord])

slope1,int1 = np.polyfit(np.log(x),np.log(np.abs(y1)),1)
slope2,int2 = np.polyfit(np.log(x),np.log(np.abs(y2)),1)
slope3,int3 = np.polyfit(np.log(x),np.log(np.abs(y3)),1)
print('Order 1 slope: '+str(slope1))
print('Order 3 slope: '+str(slope2))
print('Order 5 slope: '+str(slope3))

fig_kl = plt.figure()
ax_kl = fig_kl.add_subplot(1,1,1)
ax_kl.set_title(title)
ax_kl.plot(x[1:],y1[1:],'b',linewidth=2,label='Order 1')
ax_kl.plot(x[1:],y2[1:],'r',linewidth=2,label='Order 3')
ax_kl.plot(x[1:],y3[1:],'g',linewidth=2,label='Order 5')

ax_kl.set_xlabel('$N$ samples')
ax_kl.set_ylabel('KL divergence')
fig_kl.gca().legend()

fig_kl_log = plt.figure()
ax_kl_log = fig_kl_log.add_subplot(1,1,1)
ax_kl_log.set_title(title)
ax_kl_log.loglog(x,np.abs(y1),'b',linewidth=2,label='Order 1')
ax_kl_log.loglog(x,np.abs(y2),'r',linewidth=2,label='Order 3')
ax_kl_log.loglog(x,np.abs(y3),'g',linewidth=2,label='Order 3')

ax_kl_log.set_xlabel('$N$ samples')
ax_kl_log.set_ylabel('KL divergence')
fig_kl_log.gca().legend()
plt.show()

#p11 = plt.plot(x,slope1*x+(1.1)*int1,'b--')
#p21 = plt.plot(x,slope2*x+(1.1)*int2,'r--')
#p31 = plt.plot(x,slope3*x+(1.1)*int3,'g--')
#plt.savefig('plots/kl_div_'+str(testn)+'.pdf')
