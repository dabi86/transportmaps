#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2016 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Daniele Bigoni
# E-mail: dabi@limitcycle.it
#

import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from tabulate import tabulate

import SpectralToolbox.Spectral1D as S1D

import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.tests.TestFunctions as TF

nax = np.newaxis

# PLOTTING
PLOTTING = False
fsize = (6,5)
discr = 31

# Get test=sand title and parameters
#title, setup, test = TF.get(TESTN)
setup = {}
sand = {}
setup['mu'] = np.array([0.,0.])
setup['sigma2'] = np.array([[1.,0.],[0.,1.]])
setup['dim'] = 2
setup['refplotspan'] = np.array([[-7.,7.]])
setup['plotspan'] = np.array([[-5.,5.]])
title = 'Inverse_Rosenblatt_dimension_%s' % (str(setup['dim']))

#read in the data
datafile = "../../../rosenblatttests/samples_invros/samples_d2_n1000_r0.dat"
data = np.loadtxt(datafile, comments="%")
if data.ndim == 1:
    data = data[:,nax]

#define a class for the target density based on samples
class SamplesDensity(DENS.Density):
    r""" Arbitrary density built from samples

    Args:
      samples (type, dimension?): independent samples
    """

    def __init__(self, samples):
        super(SamplesDensity,self).__init__(samples.shape[1])
        self.samples = samples

    def rvs(self, m):
        if m > self.samples.shape[0]:
            raise ValueError("Number of rvs must be less than or equal to length of samples provided")
        
        msamples = self.samples[0:m,:]
        return msamples

    def quadrature(self, qtype, qparams):
        if qtype == 0:
            x = self.rvs(qparams)
            w = np.ones(qparams)/qparams
        else:
            raise NotImplementedError("Not implemented because sample density only has quadrature via samples")
        return (x,w)

#going from target (samples) to standard normal
sand['base_density'] = SamplesDensity(data)
sand['target_density'] = DENS.StandardNormalDensity(2)
#will keep support map but shouldn't need it in this example
sand['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.array([0., 0.]),
                                                            np.array([1.,1.]))
# APPROXIMATION
MONOTONE = 'intexp'

# REGULARIZATION
# None: No regularization
# L2: L2 regularization
REG = None
# REG = {'type': 'None',
#        'alpha': 1e-3}
if REG is None:
    tit_reg = 'None'
elif REG['type'] == 'L2':
    tit_reg = REG['type'] + '-' + str(REG['alpha'])

# L2 error estimation
# 3: Gauss quadrature of order n
# 0: Monte Carlo quadrature with n point
qtype = 0
qparams = 50
tit_intest = str(qtype) + '-' + str(qparams)

# Gradient information
# 0: derivative free
# 1: gradient information
# 2: Hessian information
ders = 2

# Approximation orders
orders = [1,3,5]
# MC samples / # of samples in sand data file
N_samp = data.shape[0]
# Tolerance
tol = 1e-10

# Generate samples of reference and target
###ref_samp = test['base_density'].rvs(N_samp)
###T_samp = test['support_map']( test['target_map'](ref_samp) )

ref_samp = data
T_samp = sand['target_density'].rvs(N_samp)

# PLOTTING PRE-COMPUTATION
# Start plotting of the 1d map
if PLOTTING:
    pspan = setup['plotspan']
    rspan = setup['refplotspan']

    # Plot the 1d map
#    X1d = np.vstack( (np.linspace(rspan[0,0],rspan[0,1],discr),
#                      np.zeros((setup['dim']-1,discr))) ).T
#    t1 = sand['support_map']( sand['target_map'](X1d) )[:,0]#no analytical map, does not work
#    fig_map_1d = plt.figure()
#    ax_map_1d = fig_map_1d.add_subplot(1,1,1)
#    ax_map_1d.plot(X1d[:,0], t1, label='exact')
#
    # Plot the 1d distribution
    fig_kde_1d = plt.figure()
    ax_kde_1d = fig_kde_1d.add_subplot(1,1,1)
    # Exact distribution 1d
    X1d_kde = np.linspace(pspan[0,0],pspan[0,1],discr).reshape((discr,1))
    if setup['dim'] == 1:
        # Plot analytic
        pdf1d = sand['target_density'].pdf(X1d_kde)
        ax_kde_1d.plot(X1d_kde, pdf1d, label='$\pi^1_{tar}$')
    else:
        # Gaussian KDE approximation
        T1_kde = stats.gaussian_kde(T_samp[:,0][:,np.newaxis].T)
        pdf1d = T1_kde(X1d_kde.T)
        ax_kde_1d.plot(X1d_kde, pdf1d, label='kde-$\pi^1_{tar}$')

    if setup['dim'] >= 2:
        # Plot the 2d transport map
#        x = np.linspace(rspan[0,0],rspan[0,1],discr)
#        y = np.linspace(rspan[1,0],rspan[1,1],discr)
#        xx,yy = np.meshgrid(x,y)
#        X2d = np.vstack( (xx.flatten(),yy.flatten(),np.zeros((setup['dim']-2,discr**2))) ).T
#        t2 = sand['support_map']( sand['target_map'](X2d) )[:,1]
#        fig_maps2d = []
#        ax_maps = []
#        levels_maps2d = np.linspace(np.min(t2),np.max(t2),20)
#        for i,order in enumerate(orders):
#            fig_maps2d.append( plt.figure() )
#            ax_map = fig_maps2d[-1].add_subplot(1,1,1)
#            ax_map.contour(xx,yy,t2.reshape(xx.shape),
#                           levels=levels_maps2d)
#            ax_maps.append( ax_map )
#            plt.title("Order %d" % order)

        # Plot PDF 2d
        fig_pdf2d = []
        axs_pdf2d = []
        x = np.linspace(pspan[0,0],pspan[0,1],discr)
        y = np.linspace(pspan[1,0],pspan[1,1],discr)
        xx2d,yy2d = np.meshgrid(x,y)
        X2d_kde = np.vstack( (xx2d.flatten(),yy2d.flatten(),
                              np.zeros((setup['dim']-2,discr**2))) ).T
        if setup['dim'] == 2:
            pdf2d = sand['target_density'].pdf(X2d_kde)
        else:
            T2_kde = stats.gaussian_kde(T_samp[:,:1].T)
            pdf2d = T2_kde(X2d_kde.T)
        levels_pdf2d = np.linspace(np.min(pdf2d),np.max(pdf2d),20)
        for i,order in enumerate(orders):
            fig_pdf2d.append( plt.figure() )
            ax_pdf2d = fig_pdf2d[-1].add_subplot(1,1,1)
            ax_pdf2d.contour(xx2d,yy2d,pdf2d.reshape(xx2d.shape),
                             levels=levels_pdf2d)
            axs_pdf2d.append( ax_pdf2d )
            plt.title("Order %d" % order)

# Define header for the output log
if MONOTONE == 'polyconst':
    tit_type = 'poly_enf'
    log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
elif MONOTONE == 'intexp':
    tit_type = 'monot'
    if ders in [0,1]:
        log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
    else:
        log_header = ['Order','#F.ev.','#J.ev','#H.ev','$L^2$-err']

log = [ ]

for i_ord,order in enumerate(orders):
    print("\nOrder: %d" % order)

    # Build the transport map (isotropic for each entry)
    approx_list = []
    active_vars = []
    for i in range(setup['dim']):
        c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (i+1)
        c_orders_list = ([ order ] * i) + [0]
        c_approx = FUNC.MonotonicFullOrderLinearSpanApproximation(c_basis_list, c_orders_list)
        e_basis_list = [S1D.HermiteProbabilistsFunction()] * (i+1)
        e_orders_list = [order] * (i+1)
        e_approx = FUNC.MonotonicFullOrderLinearSpanApproximation(e_basis_list, e_orders_list)
        approx = FUNC.MonotonicIntegratedExponentialApproximation(c_approx, e_approx)
        approx_list.append( approx )
        active_vars.append( range(i+1) )
    tm_approx = Maps.IntegratedExponentialTriangularTransportMap(active_vars, approx_list)
    # Construct density
    # Density T_\sharp \pi
    tm_density = DENS.PushForwardTransportMapDensity(tm_approx, sand['base_density'])
    # Target density to be approximated L^\sharp \pi_{\rm tar}
    target_density = DENS.PullBackTransportMapDensity(sand['support_map'],
                                                      sand['target_density'])
     
    # SOLVE
    log_entry_solve = tm_density.minimize_kl_divergence(target_density, qtype=qtype,
                                                        qparams=qparams,
                                                        regularization=REG,
                                                        tol=tol, ders=ders)
    log_entry = [order] + log_entry_solve

    # Construct approximate density L_\sharp T_\sharp \pi
    approx_density = DENS.PushForwardTransportMapDensity(sand['support_map'], tm_density)

    # Push forward reference samples in two steps
    # (1) Map x from \pi to y from T_\sharp \pi
    # (2) Map y from T_\sharp \pi to z from L_\sharp T_\sharp \pi
    tmp = tm_density.map_samples_base_to_target( ref_samp )
    fapp_samp = approx_density.map_samples_base_to_target( tmp ) 

    # PLOT
    if PLOTTING:
        # Map to T_\sharp \pi
#X1d not defined for samples
#        t1_approx_tmp = tm_density.map_samples_base_to_target(X1d)
#        # Map to L_\sharp T_\sharp \pi
#        t1_approx = approx_density.map_samples_base_to_target(t1_approx_tmp)[:,0]
#        # Plot 1d transport map
#        ax_map_1d.plot(X1d[:,0], t1_approx, label='ord %d' % order)

        # Plot the 1d distribution
        if setup['dim'] == 1:
            pdf1d_approx = approx_density.pdf(X1d_kde)
        else:
            t1_kde_approx = stats.gaussian_kde(fapp_samp[:,0][:,np.newaxis].T)
            pdf1d_approx = t1_kde_approx(X1d_kde.T)
        ax_kde_1d.plot(X1d_kde, pdf1d_approx, label='ord %d' % order)


        if setup['dim'] >= 2:
#            # Map to T_\sharp \pi
#            t2_approx_tmp = tm_density.map_samples_base_to_target(X2d)
#            # Map to L_\sharp T_\sharp \pi
#            t2_approx = approx_density.map_samples_base_to_target(t2_approx_tmp)[:,1]
#            # Plot 2d transport map
#            ax_maps[i_ord].contour(xx2d, yy2d, t2_approx.reshape(xx2d.shape),
#                                   linestyles='dashed', levels=levels_maps2d)

            # Plot the 2d distribution
            if setup['dim'] == 2:
                pdf2d_approx = approx_density.pdf(X2d_kde)
            else:
                t2_kde_approx = stats.gaussian_kde(fapp_samp[:,:1].T)
                pdf2d_approx = t2_kde_approx(X2d_kde.T)
            axs_pdf2d[i_ord].contour(xx2d,yy2d,pdf2d_approx.reshape(xx2d.shape),
                                     linestyles='dashed', levels=levels_pdf2d)
    
    # Compute L2 error
    dist = np.sqrt( np.sum((T_samp - fapp_samp)**2., axis=1) )
    log_entry.append( np.sqrt( np.sum( dist**2. ) / float(N_samp) ) )
    
    log.append( log_entry )

if PLOTTING:
#    fig_map_1d.gca().legend()
    fig_kde_1d.gca().legend()
    #plt.show(False)
    plt.show()

# # Decorate map plotting
# # ax_map.set_ylim([f(X[0]), f(X[-1])])
# ax_map.scatter(x, np.zeros(x.shape))
# ax_map.set_xlabel('x')
# ax_map.set_ylabel('$T(x)$')
# # ax_map.set_ylabel('$ (F^{-1} \circ F_{ref})x $')
# # ax_map.set_ylim([np.min(exact_tm),np.max(exact_tm)])
# ax_map.legend(loc='best')
# ax_map.grid(True)

# # fig_map.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-Approximation.pdf' % \
# #                 (title, tit_type, tit_reg, tit_intest))
# # fig_map.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-Approximation.eps' % \
# #                 (title, tit_type, tit_reg, tit_intest))

# # Decorate kde plotting
# ax_kde.set_xlabel('x')
# ax_kde.set_ylabel('PDF')
# ax_kde.legend(loc='best')

# # fig_kde.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-DistributionApproximation.pdf' % \
# #                 (title, tit_type, tit_reg, tit_intest))
# # fig_kde.savefig('Figs/KLdiv-%s-%s-%s-%s-MonotoneMap1d-DistributionApproximation.eps' % \
# #                 (title, tit_type, tit_reg, tit_intest))

# plt.show(False)

print(" ")
print(tabulate(log, headers=log_header))
print(" ")

print(tabulate(log, headers=log_header, tablefmt="latex", floatfmt=".2e"))
