import numpy as np
import GaussianGraphs as gg
from math import *

# distribution type
graph_list =[gg.ChainGraph, gg.GridGraph]

for graph_idx, graph in enumerate(graph_list):

    # Name datafile using graph_obj
    datafile = 'MC_Alg/data_mc_alg_errors_' + graph.__name__ +'2.txt'

    # read in datafile
    data = np.loadtxt(datafile, comments='%')

    # fix unique values for parameters
    dim      = np.unique(data[:,0])
    n        = np.unique(data[:,1])
    delta    = np.unique(data[:,4])
    
    for i,dim_i in enumerate(dim):
        for j,delta_j in enumerate(delta):
            for k,n_k in enumerate(n):

                # find indices for the given dim and delta
                dim_delta = np.intersect1d(np.where(data[:,0] == dim_i), \
                        np.where(data[:,4] == delta_j))
                dim_delta_n = np.intersect1d(dim_delta, np.where(data[:,1] == n_k))

                # count and print number
                count_dim_delta_n = len(dim_delta_n)
                print('graph: %s, dim: %d, delta: %.1f, samps: %d, count: %d' %(graph.__name__, dim_i, delta_j, n_k, count_dim_delta_n))
