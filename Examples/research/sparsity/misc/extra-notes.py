
    # ********************************* GENERATE SPECIFIC 5D GRAPH *********************************
    ##start with random matrix
    #A = np.random.random([dim,dim])
    ##zero out upper triangular
    #A = np.tril(A)
    ##A = np.diag(np.diag(A))
    ##set some elements in lower triangular to zero (these will propagate)
    #A[1,0] = 0; A[3,0] = 0; A[3,1] = 0; A[4,0] = 0; A[4,1] = 0; A[4,2] = 0;
    #
    ##A \times A^T is SPD
    #Omega = np.dot(A,A.T)
    #
    ## compute Sigma (once) and find variances
    #Sigma_temp = np.linalg.inv(Omega)
    #Var_temp = np.diag(np.sqrt(np.diag(Sigma_temp)))
    #
    ## normalize A and compute new Omega
    #Atild = np.dot(Var_temp,A)
    #Omega = np.dot(Atild,Atild.T)
    #Sigma = np.dot(np.linalg.inv(Atild.T), np.linalg.inv(Atild))
    #
    #print("\nNew Sigma= ", Sigma)
    #
    #L = np.linalg.cholesky(Omega)
    #print("\ncholesky factor L of Omega= \n",L)
    ##take cholesky factor to generate samples
    #Sigma = np.linalg.inv(Omega)
    #K = np.linalg.cholesky(Sigma)
    #x =  DENS.StandardNormalDensity(dim).rvs(nsamps)
    #data = (np.dot(K,x.T)).T
    #print(data.shape)
    # **************************** END GENERATE SPECIFIC 5D GRAPH ******************************
    
    #datafile = "../../../rosenblatttests/samples_invros/samples_d2_n1000_r0.dat"
    #data = np.loadtxt(datafile, comments="%")
    #if data.ndim == 1:
    #    data = data[:,nax]


       # PRINT LINEAR MAP
       #Tmean = np.zeros(dim)
       #w = np.array([np.sqrt(1/np.sqrt(2*np.pi)),np.sqrt(1/np.sqrt(2*np.pi))])
       # TODO: check orders
       # for i, approx in enumerate(tm_approx.approx_list):
        #   print("component %d: "%i)
         #  print("    constant: %s "%str(approx.c.get_coeffs()))
          # Tmean[i] = approx.c.get_coeffs()[0]*(w[0]**(i+1))
       
       # print("\nOmega = \n",Omega)
       
       # EVALUATE MAP ON IDENTITY TO RECOVER MATRIX
       #S = (tm_approx.evaluate(np.identity(setup['dim']))).T - \
       #        tm_approx.evaluate(np.zeros((1,setup['dim']))).T
       #gx = tm_approx.grad_x(np.zeros((1,setup['dim'])))
       # for i in range(setup['dim']):
       ##    S[:,i] = S[:,i] #- Tmean
       
       #print("\n\nS = ",S)
       #print("\n\nS^T*S = ",np.dot(S.T,S))
       #omegaHat = np.dot(S.T,S)
       ##in case not gaussian, then would need following:
    #############THIS IS NOT COMPLETE##########################
       # # Evaluate Log pdf hessian at all samples
       # hess_samps = pb_density.hess_x_log_pdf(data)
    
       # # Compute information matrix
       # omegaTilde = np.mean(np.abs(hess_samps), axis=0)
    #############END: THIS IS NOT COMPLETE#####################

# ********************RUN SING WITH USER INPUT**********************************************
# def run_SING(DATAFILE, ORDER, ORDERING, DELTA):
#     # # open file
#     # file = open(file_name,'a')

#     data = np.loadtxt(DATAFILE)
#     orderings = [no.ReverseCholesky(), no.MinFill(), no.MinDegree()]
#     ordering = orderings[int(ORDERING)]
#     delta = float(DELTA)

#     recovered_gp = Algorithm.SING(data, int(ORDER), ordering, delta)
#     return recovered_gp

# def usage():
#     print('run_SING.py -o <order> -r <ordering> -l <delta> <datafile>')

# def main(argv):
#     # defaults
#     ORDER = 1
#     ORDERING = 1
#     DELTA = 2
#     try:
#         opts, args = getopt.getopt(argv, 'ho:r:l:', ['help','order','ordering','delta'])
#     except getopt.GetoptError:
#         usage()
#         sys.exit(2)
#     for opt, arg in opts:
#         if opt in ('-h', '--help'):
#             usage()
#             sys.exit()
#         elif opt in ('-o', '--order'):
#             ORDER = arg
#         elif opt in ('-r', '--ordering'):
#             ORDERING = arg
#         elif opt in ('-l', '--delta'):
#             DELTA = arg

#     DATAFILE = args[-1]
#     run_SING(DATAFILE, ORDER, ORDERING, DELTA)

#if __name__ == '__main__':
#    main(sys.argv[1:])

