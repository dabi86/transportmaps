import NonGaussianGraphs as ngg
import ExtraDensities

import numpy as np

import TransportMaps as TM
import TransportMaps.Densities as DENS

dim = 4
num_sets = 1
graph = ngg.Rademacher(dim)

# extract true omega (same as precision matrix)
omegaTrue = graph.edgeSet()

# compute map-from-samples based omega

nax = np.newaxis
setup = {}
pi = {}
setup['mu'] = np.zeros(dim)
setup['sigma2'] = np.identity(dim)
setup['dim'] = dim

# active_vars = graph.active_vars()
active_vars = None

#going from target (samples) to standard normal
#pi['base_density'] = ExtraDensities.SamplesDensity(data)
pi['target_density'] = DENS.StandardNormalDensity(dim)
#will keep support map but shouldn't need it in this example
pi['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.zeros(dim),
                                                          np.ones(dim))
# APPROXIMATION
MONOTONE = 'intexp'

# REGULARIZATION
# None: No regularization
# L2: L2 regularization
REG = None
# REG = {'type': 'None',
#        'alpha': 1e-3}

# Gradient information
# 0: derivative free
# 1: gradient information
# 2: Hessian information
ders = 2

# Approximation orders
orders = [1,3,5]
# orders = [1]
# Tolerance
tol = 1e-3

spar = 1
# Target density to be approximated L^\sharp \pi_{\rm tar}
target_density = DENS.PullBackTransportMapDensity(pi['support_map'],
                                                  pi['target_density'])

# loop for number 
for n_samps in [2000]:#[10, 50, 100, 500, 1000, 5000, 10000]:
    for j in range(num_sets):

        # generate dataset for each MC run
        data = graph.rvs(n_samps)
        print('\nRun: %d, Size: %d' %(j, n_samps))
        
        # L2 error estimation
        # 3: Gauss quadrature of order n
        # 0: Monte Carlo quadrature with n point
        qtype = 0
        qparams = n_samps
        tit_intest = str(qtype) + '-' + str(qparams)

        # define base_density from samples
        pi['base_density'] = ExtraDensities.SamplesDensity(data)

        for i_ord,order in enumerate(orders):
            print("\nSpar: %d Order: %d" % (spar, order))
   
            # Build the transport map (isotropic for each entry)
            tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
                   setup['dim'], order, active_vars=active_vars)
            # Construct density
            # Density T_\sharp \pi
            tm_density = DENS.PushForwardTransportMapDensity(tm_approx, pi['base_density'])
             
            # SOLVE
            log_entry_solve = tm_density.minimize_kl_divergence(target_density, qtype=qtype,
                                                                qparams=qparams,
                                                                regularization=REG,
                                                                tol=tol, ders=ders)
    
            # compute expectation of abs val of hessian log pullback
            # should be same as omegaTrue
            pb_density = DENS.PullBackTransportMapDensity(tm_approx, pi['target_density'])

            # Evaluate Log pdf hessian at all samples
            hess_samps = pb_density.hess_x_log_pdf(data)

            # Compute information matrix
            omegaTilde = np.mean(np.abs(hess_samps), axis=0)

            # EVALUATE MAP ON IDENTITY TO RECOVER MATRIX
            # S = (tm_approx.evaluate(np.identity(setup['dim']))).T - \
            #         tm_approx.evaluate(np.zeros((1,setup['dim']))).T
            # # gx = tm_approx.grad_x(np.zeros((1,setup['dim'])))
            # omegaHat = np.dot(S.T,S)
            
            print("\nomegaTilde: ", omegaTilde)
            # print("\nomegaHat: ", omegaHat)

            # compute entrywise error in S and Omega
            #S_error = np.abs(S - Kinv)
            # Omega_error = np.abs(omegaHat - omegaTrue)

            # reshape S_error and Omega_error matrices
            #S_error = np.reshape(S_error, (1, dim*dim))[0]
            # Omega_error = np.reshape(Omega_error, (1, dim*dim))[0]

