#
# This file runs the algorithm given a set of samples
#
import warnings
import time
import matplotlib
# matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
import TransportMaps.Algorithms.SparsityIdentification as SI
from scipy.stats import norm
from scipy.stats import uniform
np.random.seed(1)

nsamps = 2000
halfn = np.int(nsamps/2)
dim = 4

Omega = np.zeros((dim,dim)) #inverse correlation matrix, to be filled
# chain graph
np.fill_diagonal(Omega,1)
np.fill_diagonal(Omega[1:],.4)
np.fill_diagonal(Omega[:,1:],.4)
# for i in range(p):
#     edge_i = 0
#     for j in range(i):
#         # sample Y_i^1, Y_i^2, Y_j^1, Y_j^2
#         y_i = np.random.rand(2,1)
#         y_j = np.random.rand(2,1)
#         p_ij = 1/(2*np.pi)*np.exp(-np.linalg.norm(y_i - y_j)**2/2/s)
#         alpha = np.random.rand(1)
#         if p_ij > alpha:
#             if edge_i < 4:
#                 Omega[i][j] = .245
#                 Omega[j][i] = .245
#                 edge_i +=1
#     Omega[i][i] = 1

print("Omega = ", Omega)
# covariance matrix
Sigma = np.linalg.inv(Omega)
mu1 = np.ones((dim,))*2
mu2 = np.ones((dim,))*-2
norm_data1 = np.random.multivariate_normal(mu1, Sigma, halfn)
norm_data2 = np.random.multivariate_normal(mu2, Sigma, halfn)
data = np.append(norm_data1,norm_data2,axis=0)

print("data.shape = ",data.shape)
# plt.scatter(data[:,2],data[:,3])
# plt.hist(data[:,2])
# plt.show()
def f(x, mu, Omega):
    sum = 0
    for i in range(dim):
        for j in range(dim):
            sum += Omega[i][j]*(x[i]*mu[j] + mu[i]*x[j])
    return sum

def partialkf(x, mu, Omega, k):
    sum = 0
    for i in range(dim):
        sum += Omega[i][k]*mu[i]
    return 2*sum

def gp1_kl(x, mu, Omega, k, l):
    return -.5*Omega[k][l]

def gp2_kl(x, mu, Omega, k, l):
    F = f(x, mu, Omega)
    pk = partialkf(x, mu, Omega, k)
    # print("pk = ", pk)
    pl = partialkf(x, mu, Omega, l)
    # print("pl = ", pl)
    A = -np.exp(-F)*pk*pl
    B = 1 + np.exp(-F)
    C = 1 + np.exp(-F)/(1 + np.exp(-F))
    return A/B*C

# take mean
gp_hat = np.zeros((dim,dim))
for k in range(dim):
    for l in range(dim):
        for n in range(nsamps):
            gp_hat[k][l] += np.abs(gp1_kl(data[n,:], mu1, Omega, k, l) + gp2_kl(data[n,:], mu1, Omega, k, l))
        gp_hat[k][l] /= nsamps

gp_hat /= np.max(gp_hat)
fig, ax = plt.subplots()
im = ax.imshow(gp_hat)
fig.colorbar(im)
plt.xticks([0, 1, 2, 3], ['1','2','3','4'])
plt.yticks([3, 2, 1, 0], ['4','3','2','1'])
# plt.show()
fig.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/multimodal/true-gp.pdf")#,nsamps,dim,delta_string[0],orders[i]))
# fig.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/multimodal/true-%s.pdf" %(name_string)) #,nsamps,dim,delta_string[0],orders[i]))


# orders = [1,2,3,4]
# orders = [4]
# ordering = SI.ReverseCholesky()
# # deltas = [3.,3.5,4.,4.5,5.,5.5,6.]
# # delta_string = [3.,3.5,4.,4.5,5.,5.5,6.]
# deltas = [2]
# delta_string = ['2']
# name_string = 'multimodal2-2'
# # for i, delta in enumerate(deltas):
# for i, order in enumerate(orders):
#     recovered_gp = SI.SING(data, order, ordering, deltas[0])
#     print("gp = ",recovered_gp)
    
#     dim = recovered_gp.shape[0]
#     adjacency = np.zeros([dim, dim])
#     adjacency[recovered_gp != 0] = 1
    
#     fig1, ax1 = plt.subplots()
#     fig2, ax2 = plt.subplots()
#     im1 = ax1.imshow(adjacency)#,interpolation='none')
#     fig1.colorbar(im1)
#     im2 = ax2.imshow(recovered_gp/np.max(recovered_gp))#,interpolation='none')
#     fig2.colorbar(im2)
    
#     # plt.show()
    
#     fig1.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/multimodal/adj-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[0],orders[i]))
#     fig2.savefig("/home/rmorriso/repos/transport-maps/Examples/research/sparsity/recovered_genprec_figs/multimodal/gp-%s-%d-%d-%s-%d.pdf" %(name_string,nsamps,dim,delta_string[0],orders[i]))

