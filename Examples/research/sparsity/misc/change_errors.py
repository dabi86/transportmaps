import numpy as np
import os

directory = 'MC_Alg_backup/'
for filename in os.listdir(directory):
    data = np.loadtxt(directory+filename, comments='%')

    data[:,6:8] = data[:,6:8]/2.

    newfilename = filename
    np.savetxt('MC_Alg/'+newfilename, data, fmt='%.1f')
