#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Authors: Transport Map Team
# E-mail: tmteam@mit.edu
# Website: transport-maps.mit.edu
# Support: transport-maps.mit.edu/qa/
#

import matplotlib.pyplot as plt
import numpy as np
import itertools

from TransportMaps.Defaults import Default_IsotropicIntegratedSquaredTriangularTransportMap, Default_IsotropicIntegratedExponentialTriangularTransportMap
from TransportMaps.Distributions.FrozenDistributions import StandardNormalDistribution
from TransportMaps.Distributions.TransportMapDistributions import PushForwardTransportMapDistribution, PullBackTransportMapDistribution
from TransportMaps.Distributions.DistributionBase import DistributionFromSamples
from GeneralizedPrecision import gen_precision, var_omega
from RegularizedMap import regularized_map
from NodeOrdering import MinFill, MinDegree, ReverseCholesky
import pdb
__all__ = ['SING']

# inputs: data (n x d), polynomial order, node ordering method, tolerance delta
# output: sparse edge set
def SING(data, p_order, ordering, delta, REG=None):
    r""" Identify a sparse edge set of the undirected graph corresponding to the data.

    Inputs: data
    :math:`{\bf x}_i, i = 1,\dots,n`,
    polynomial order of the transport map representation,
    ordering scheme used to reorder the variables,
    tolerance
    :math:`\delta`.
    optional argument REG dictionary with keys 'type' and 'reweight'

    Output: the generalized precision :math:`\Omega`
    """
    # Initial setup
    # data is (n x d)
    dim = data.shape[1]
    n_samps = data.shape[0]
    nax = np.newaxis
    pi = {}

    # Target density is standard normal
    eta = StandardNormalDistribution(dim)
    # Quadrature type and params
    # 0: Monte Carlo quadrature with n point
    qtype = 0
    qparams = n_samps
    # Gradient information
    # 0: derivative free
    # 1: gradient information
    # 2: Hessian information
    ders = 2
    # Tolerance in optimization
    tol = 1e-5
    # Log stores information from optimization
    log = [ ]

    # All variables are active variables to begin
    active_vars=None
    
    # Define base_density from samples
    pi = DistributionFromSamples(data)
    
    # Solve for approximate map
    tm_approx = regularized_map(eta, pi, data, qtype, qparams, dim, p_order, active_vars, tol, ders, REG)
    pb_density = PullBackTransportMapDistribution(tm_approx, eta)

    # Compute generalized precision
    omegaHat = gen_precision(pb_density, data)
    # Compute variance of generalized precision
    gp_var = var_omega(pb_density, data)
    # Compute tolerance (matrix)
    tau = delta * np.sqrt(np.abs(gp_var))#delta * np.sqrt(np.abs(gp_var)) # set tolerance as square root of variance
    pdb.set_trace()
    print(tau)

    # Save diagonal elements
    omegaHat_diagonal = np.copy(np.diag(omegaHat))
    # Threshold omegaHat
    omegaHat[np.abs(omegaHat) < tau] = 0
    # Put diagonal elements back in
    omegaHat[np.diag_indices_from(omegaHat)] = omegaHat_diagonal
       
    return omegaHat
