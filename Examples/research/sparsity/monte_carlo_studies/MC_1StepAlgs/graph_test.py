# This file runs statistical studies of the algorithm for a 
# set of parameter values (order, delta, number of samples)

import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import SparsityIdentificationNonGaussian as SI
import GaussianGraphs as gg
import NonGaussianGraphs as ngg
import pdb

# parameters to be varied
# dimension, order, ordering, delta, number of samples, number of runs

font = {'family': 'serif', 'weight': 'normal', 'size':6}
matplotlib.rc('font', **font)


def run_tests(model, order, ordering):

    # define number of repeated iterations
    n_iter = 1#10

    # define vector for delta
    delta = np.linspace(1.0, 4.0, 7)
    delta_title = ['1p0','1p5','2p0','2p5','3p0','3p5','4p0']

    # define number of samples
    n_samps = np.array([5000])
    #n_samps = np.linspace(500,5000,10)
    #n_samps = np.append(n_samps, [50,100,250])
    #n_samps = np.sort(n_samps)

    # extract dimension
    dim = model.dim

    # define regularization
    REG_title = 'No_Reg'
    REG_list = None

    # define file to save data
    file_name = 'Data/data_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.txt'
    file = open(file_name,'a')

    # define object for data generating distribution
    true_graph = model.graph()

    # declare vectors to store errors
    type1 = np.zeros((n_iter,len(n_samps),len(delta)))
    type2 = np.zeros((n_iter,len(n_samps),len(delta)))
    tot_error = np.zeros((n_iter,len(n_samps),len(delta)))

    for run in range(n_iter):

        # import data and true graph
        data = model.rvs(int(np.max(n_samps)))

        # re-scale data
        mean_vec = np.mean(data,axis=0)
        data = data - mean_vec
        inv_var = 1./(np.var(data,axis=0))
        inv_std = np.diag(np.sqrt(inv_var))
        data = np.dot(data,inv_std)

        for j,n in enumerate(n_samps):
            for k,d in enumerate(delta):

                # run SING
                print('-----------------------------------------------------')
                print('SING - Delta: ' + str(d) + ', Samples: ' + str(n) + ', Run: ' + str(run+1) + '/' + str(n_iter))
                recovered_gp = SI.SING(data[:int(n),:], order, ordering, d, REG_list)
                plt.close('all')

                print(recovered_gp)

                # extract graph
                dim = recovered_gp.shape[0]
                adjacency = np.zeros([dim, dim])
                adjacency[recovered_gp != 0] = 1

                # compute difference of graphs 
                graph_diff = adjacency - true_graph

                # compute errors
                type1[run,j,k] = np.count_nonzero(graph_diff == 1)
                type2[run,j,k] = np.count_nonzero(graph_diff == -1)
                if np.sum(np.abs(graph_diff)) == 0:
                    tot_error[run,j,k] = 1
                
                # Plot generalized precision
                fig,ax = plt.subplots(1,2,subplot_kw={'xticks': [], 'yticks': []}) 

                cax0 = ax[0].imshow(np.abs(recovered_gp), interpolation='nearest')
                ax[0].set_title('$E[\partial^2_{i,j} \log(\pi)]$ - Thresholded')
                fig.colorbar(cax0, ax=ax[0], orientation='horizontal')

                cax1 = ax[1].imshow(adjacency, interpolation='nearest')
                ax[1].set_title('G - Thresholded')
                fig.colorbar(cax1, ax=ax[1], orientation='horizontal')

                fig.set_tight_layout(True)
                fig.savefig('Figures/gprec_' + model.type + '_dim' + str(dim) +
                        '_run' + str(run) + '_samps' + str(int(n)) + '_delta' +
                        delta_title[k] + '.pdf')

                # print results
                print('Type 1 Err: ' + str(type1[run,j,k]) + ', Type 2 Err: ' +
                        str(type2[run,j,k]) + ', Total Err: ' +
                        str(tot_error[run,j,k]))
                print(' ')

                # save data to file
                file.write(str(dim) + ' ' + str(order) + ' ' +  str(d) + ' ' +
                        str(n) + ' ' + str(run) + ' ' + str(k) + ' ' +
                        str(type1[run,j,k]) + ' ' + str(type2[run,j,k]) + ' ' +
                        str(tot_error[run,j,k]) + '\n')

    # close file
    file.close()

    # plot results
    fmts = ['--o','--o','--o','--o','-.o','-.o','-.o','-.o']
    colors = ['b','r','g','c','b','r','g','c']
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(1,1,1)
    for k in range(len(delta)):
        ax1.errorbar(n_samps, np.mean(tot_error[:,:,k],axis=0),
                yerr=np.std(tot_error[:,:,k],axis=0), fmt=fmts[k], color=colors[k],
                label=str(delta[k]))
    ax1.set_title('Probability of Success')
    ax1.set_xlabel('$N$')
    ax1.set_ylabel('$P(G = \hat{G})$')
    ax1.legend()
    fig1.savefig('Figures/succ_prob_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

    for k in range(len(delta)):

        fig2 = plt.figure()
        ax2 = fig2.add_subplot(1,1,1)
        
        ax2.errorbar(n_samps, np.mean(type1[:,:,k],axis=0),
            yerr=np.std(type1[:,:,k],axis=0), fmt='--o',color='b', label='Type 1 Error')
        ax2.errorbar(n_samps, np.mean(type2[:,:,k],axis=0), 
            yerr=np.std(type2[:,:,k],axis=0), fmt='--o',color='r', label='Type 2 Errpr')
        
        ax2.set_title('Error Counts - $\delta = ' + str(delta[k]) + '$')
        ax2.set_xlabel('$N$')
        ax2.set_ylabel('Error Counts')
        ax2.legend()
        fig2.savefig('Figures/errors_' + model.type + '_dim' + str(dim) +
                '_ord' + str(order) + '_delta' + delta_title[k] + '.pdf')


if __name__ == '__main__':

    # define graphs (model + dimension)
    model_list = [ngg.Rademacher(8), gg.StarGraph(9), gg.GridGraph(9), ngg.StochVolHyper(9), ngg.StochVolChain(9)]

    # Set order and ordering
    order = 2
    ordering = SI.ReverseCholesky()

    for i in range(len(model_list)):
        run_tests(model_list[i], order, ordering)

