import GaussianGraphs as gg
from mc_sparsity import mc_sparsity

# Declare graph types
graphs = [gg.ChainGraph, gg.StarGraph, gg.GridGraph]

# Declare dimensions
#dim_list = [9, 25, 49, 100]
dim_list = [4, 9, 16]

for graph_type in graphs:
    for dim in dim_list:

        graph_obj = graph_type(dim)
        mc_sparsity(graph_obj)
