# This file runs statistical studies of the algorithm for a 
# set of parameter values (order, delta, number of samples)

import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import SparsityIdentificationNonGaussian as SI
import GaussianGraphs as gg
import NonGaussianGraphs as ngg

from TransportMaps.Distributions.FrozenDistributions import StandardNormalDistribution
from TransportMaps.Distributions import GaussianDistribution
from TransportMaps.Diagnostics import variance_approx_kl
from TransportMaps.Distributions.DistributionBase import DistributionFromSamples
from TransportMaps.Distributions.TransportMapDistributions import PushForwardTransportMapDistribution, PullBackTransportMapDistribution

# parameters to be varied
# dimension, order, ordering, delta, number of samples, number of runs

font = {'family': 'serif', 'weight': 'normal', 'size':6}
matplotlib.rc('font', **font)

def run_tests(model, order, ordering):

    # define number of repeated iterations
    n_iter = 10

    # define delta
    delta = 0.0

    # define number of samples
    n_samps = np.linspace(500,5000,10)
    n_samps = np.append(n_samps, [50,100,250])
    n_samps = np.sort(n_samps)

    # define the number of test samples
    test_samps = 2000

    # extract dimension
    dim = model.dim

    # define regularization list
    REG_title = ['No_Reg','L1_Reg','Rew_L1']
    REG_list = [None, {'type':'L1','reweight':False}, {'type':'L1','reweight':True}]

    # define file to save data
    file_name = 'Data/data_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.txt'
    file = open(file_name,'a')

    # define object for data generating distribution
    true_graph = model.graph()
    true_omega = model.omega()
    pi = GaussianDistribution(np.zeros(dim), np.linalg.inv(true_omega))

    # declare matrices to store error
    omega_err = np.zeros((len(REG_list),n_iter,len(n_samps)))
    kl_diag = np.zeros((len(REG_list),n_iter,len(n_samps)))

    for run in range(n_iter):

        # import data and true graph
        data = model.rvs(int(np.max(n_samps)))

        # re-scale data
        mean_vec = np.mean(data,axis=0)
        data = data - mean_vec
        inv_var = 1./(np.var(data,axis=0))
        inv_std = np.diag(np.sqrt(inv_var))
        data = np.dot(data,inv_std)

        for j,n in enumerate(n_samps):
            for k in range(len(REG_list)):

                # run SING
                print('-----------------------------------------------------')
                print('SING Reg: ' + REG_title[k] + ', Samples: ' + str(n) + ', Run: ' + str(run+1) + '/' + str(n_iter))
                tm_approx, est_omega = SI.SING(data[:int(n),:], order, ordering, REG_list[k])

                # compute eta/reference and pullback density
                eta = StandardNormalDistribution(dim)
                pull_S = PullBackTransportMapDistribution(tm_approx, eta)

                # Compute errors
                omega_err[k,run,j] = np.linalg.norm(true_omega -
                        est_omega)/np.linalg.norm(true_omega)
                kl_diag[k,run,j] = variance_approx_kl(pull_S, pi, qtype=0,
                        qparams = test_samps) 

                # save data to file
                file.write(str(dim) + ' ' + str(order) + ' ' +  str(delta) + ' ' +
                        str(n) + ' ' + str(run) + ' ' + str(k) + ' ' +
                        str(omega_err[k,run,j]) + ' ' + str(kl_diag[k,run,j]) + '\n')

    # close file
    file.close()

    # plot results
    for k in range(len(REG_list)):

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        
        ax.errorbar(n_samps, np.mean(omega_err[k,:,:],axis=0),
            yerr=np.std(omega_err[k,:,:],axis=0), fmt='--o',color='b',
            label=r'$\|\Omega - \tilde{\Omega}\|$')
        ax.errorbar(n_samps, np.mean(kl_diag[k,:,:],axis=0), 
            yerr=np.std(kl_diag[k,:,:],axis=0), fmt='--o',color='r',
            label=r'$D_{KL}(\pi||S^{\sharp}\rho)$')
        
        ax.set_title('Convergence of Errors - ' + REG_title[k])
        ax.set_xlabel('$N$')
        ax.set_ylabel('Errors')
        ax.set_yscale('log')
        ax.legend()
        fig.savefig('Figures/errors_' + REG_title[k] + '_' + model.type + '_dim' + str(dim) +
                '_ord' + str(order) + '.pdf')

if __name__ == '__main__':

    # define graphs (model + dimension)
    model_list = [gg.ChainGraph(9), gg.StarGraph(9), gg.GridGraph(9)]
    
    # Set order and ordering
    order = 2
    ordering = SI.ReverseCholesky()

    for i in range(len(model_list)):
        run_tests(model_list[i], order, ordering)

