
# set of parameter values (order, delta, number of samples)

import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import SparsityIdentificationNonGaussian as SI
import GaussianGraphs as gg
import NonGaussianGraphs as ngg

from TransportMaps.Distributions.FrozenDistributions import StandardNormalDistribution
from TransportMaps.Distributions import GaussianDistribution
from TransportMaps.Diagnostics import variance_approx_kl
from TransportMaps.Distributions.DistributionBase import DistributionFromSamples
from TransportMaps.Distributions.TransportMapDistributions import PushForwardTransportMapDistribution, PullBackTransportMapDistribution

# parameters to be varied
# dimension, order, ordering, delta, number of samples, number of runs

font = {'family': 'serif', 'weight': 'normal', 'size':6}
matplotlib.rc('font', **font)

def run_tests(model, order, ordering):

    # define run parameters
    n_iter = 10
    dim = model.dim
    REG_list  = [None, {'type':'L1','reweight':False},{'type':'L1','reweight':True}]
    REG_title = ['No_Reg','L1_Reg','Rew_L1'] 
    n_samps = np.linspace(500,5000,10)
    n_samps = np.append(n_samps, [50,100,250])
    n_samps = np.sort(n_samps)
    
    # define file to load data
    file_name = 'Data/data_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.txt'
    data = np.loadtxt(file_name,delimiter=" ")

    # declare matrices to store error
    omega_err = np.zeros((len(REG_list),n_iter,len(n_samps)))
    kl_diag = np.zeros((len(REG_list),n_iter,len(n_samps)))

    # extract columns from file
    dim_ar   = data[:,0]
    order_ar = data[:,1]
    dt_ar    = data[:,2]
    n_ar     = data[:,3]
    run_ar   = data[:,4]
    k_ar     = data[:,5]

    for run in range(n_iter):
        for j,n in enumerate(n_samps):
            for k in range(len(REG_list)):

                idx1 = np.where(run_ar == run)
                idx2 = np.where(n_ar == n)
                idx3 = np.where(k_ar == k)
                idx = np.intersect1d(np.intersect1d(idx1, idx2), idx3)
                
                # save data in vectors
                omega_err[k,run,j] = data[idx,6]
                kl_diag[k,run,j]   = data[idx,7]

    # plot results
    for k in range(len(REG_list)):

        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        
        ax.errorbar(n_samps, np.mean(omega_err[k,:,:],axis=0),
            yerr=np.std(omega_err[k,:,:],axis=0), fmt='--o',color='b',
            label=r'$\|\Omega - \tilde{\Omega}\|$')
        ax.errorbar(n_samps, np.mean(kl_diag[k,:,:],axis=0), 
            yerr=np.std(kl_diag[k,:,:],axis=0), fmt='--o',color='r',
            label=r'$D_{KL}(\pi||S^{\sharp}\rho)$')
        
        ax.set_title('Convergence of Errors - ' + REG_title[k])
        ax.set_xlabel('$N$')
        ax.set_ylabel('Errors')
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.legend()
        fig.savefig('Figures_replot/errors_' + REG_title[k] + '_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

if __name__ == '__main__':

    # define graphs (model + dimension)
    model_list = [gg.ChainGraph(9), gg.StarGraph(9), gg.GridGraph(9)]
    
    # Set order and ordering
    order = 1
    ordering = SI.ReverseCholesky()

    for i in range(len(model_list)):
        run_tests(model_list[i], order, ordering)

