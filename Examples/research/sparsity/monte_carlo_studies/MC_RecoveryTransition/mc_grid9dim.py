# This file runs statistical studies of the algorithm for a 
# set of parameter values (order, delta, number of samples)

import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import TransportMaps.Algorithms.SparsityIdentification as SI
import GaussianGraphs as gg
import NonGaussianGraphs as ngg

font = {'family': 'serif', 'weight': 'normal', 'size':6}
matplotlib.rc('font', **font)

# parameters to be varied
# dimension, order, ordering, delta, number of samples, number of runs

# define fixed set of parameters
dim = 9
model = gg.GridGraph(dim)

order = 2
ordering = SI.ReverseCholesky()

# define number of repeated iterations
n_iter = 5

# define file to save data
file_name = 'Data/data_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.txt'
file = open(file_name,'a')

# define grid for delta and number of samples
delta = np.linspace(0.25,2.0,8)
n_samps = np.linspace(200,1000,5)

# define regularization list
REG_list = [None, {'type':'L1','reweight':False}, {'type':'L1','reweight':True}]

# declare vectors to store errors
type1 = np.zeros((n_iter,len(delta),len(n_samps),len(REG_list)))
type2 = np.zeros((n_iter,len(delta),len(n_samps),len(REG_list)))
tot_error = np.zeros((n_iter,len(delta),len(n_samps),len(REG_list)))

# declare vector to store timing results
time_res = np.zeros((n_iter,len(delta),len(n_samps),len(REG_list)))

# define object for data generating distribution
true_graph = model.graph()

for run in range(n_iter):
    
    # import data and true graph
    data = model.rvs(int(np.max(n_samps)))

    # re-scale data
    mean_vec = np.mean(data,axis=0)
    data = data - mean_vec
    inv_var = 1./(np.var(data,axis=0))
    inv_std = np.diag(np.sqrt(inv_var))
    data = np.dot(data,inv_std)
    
    for i,dt in enumerate(delta):
        for j,n in enumerate(n_samps):
            
            # import data
            dataset = data[:int(n),:]
            
            for k in range(len(REG_list)):

                print('------------------------------------------------------')
                print('SING Run ' + str(run+1) + '/' + str(n_iter) + ' - ' +
                        str(dt) + 'delta, ' + str(int(n)) + ' samples') 

                # run SING with no regularization
                print('REG: ', k)
                t0 = time.time()
                recovered_gp = SI.SING(dataset, order, ordering, dt, REG_list[k])
                time_res[run,i,j,k] = time.time() - t0

                # extract graph
                dim = recovered_gp.shape[0]
                adjacency = np.zeros([dim, dim])
                adjacency[recovered_gp != 0] = 1

                # compute difference of graphs 
                graph_diff = adjacency - true_graph

                # compute errors
                type1[run,i,j,k] = np.count_nonzero(graph_diff == 1)
                type2[run,i,j,k] = np.count_nonzero(graph_diff == -1)
                if np.sum(np.abs(graph_diff)) == 0:
                    tot_error[run,i,j,k] = 1
            
                print(' ')

                # save data
                file.write(str(dim) + ' ' + str(order) + ' ' + str(dt) + ' ' + str(n) + ' ' +
                        str(run) + ' ' + str(k) + ' ' + str(type1[run,i,j,k]) + ' ' +
                        str(type2[run,i,j,k]) + ' ' + str(tot_error[run,i,j,k]) + '\n')

# close data file
file.close()

# plot results
titles = ['No Reg','L1 Reg','Rew L1']
extents_x = [np.min(delta)-0.1, np.max(delta)+0.1]
extents_y = [np.min(n_samps)-50, np.max(n_samps)+50]
n_par = (dim**2-dim)/2.0

fig,ax = plt.subplots(1,len(REG_list))
for k in range(len(REG_list)):
    cax = ax[k].imshow(np.mean(tot_error[:,:,:,k],axis=0), extent=extents_x +
            extents_y, interpolation='none', aspect='auto')
    ax[k].set_title('$P(G = \hat{G})$ - ' + titles[k])
    ax[k].set_xlabel('$\delta$')
    ax[k].set_ylabel('$N$')
    fig.colorbar(cax,ax=ax[k],ticks=[0,0.5,1],orientation='vertical')
fig.savefig('Figures/succ_prob_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

for k in range(len(REG_list)):
    
    fig2,ax2 = plt.subplots(1,2)

    cax0 = ax2[0].imshow(np.mean(type1[:,:,:,k],axis=0), extent=extents_x +
            extents_y, interpolation='none', aspect='auto')
    ax2[0].set_title('Type 1 Error - ' + titles[k])
    ax2[0].set_xlabel('$\delta$')
    ax2[0].set_ylabel('$N$')
    plt.colorbar(cax0,ax=ax2[0],ticks=[0,n_par/2.,n_par],orientation='vertical')
 
    cax1 = ax2[1].imshow(np.mean(type2[:,:,:,k],axis=0), extent=extents_x +
            extents_y, interpolation='none', aspect='auto')
    ax2[1].set_title('Type 2 Error - ' + titles[k])
    ax2[1].set_xlabel('$\delta$')
    ax2[1].set_ylabel('$N$')
    plt.colorbar(cax1,ax=ax2[1],ticks=[0,n_par/2.,n_par],orientation='vertical')

    fig2.savefig('Figures/errors_reg' + str(k) + '_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

# reshape matrix
time_res = time_res.reshape((3,n_iter*len(delta)*len(n_samps))) 

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.boxplot(time_res.T)
ax.set_xticklabels(['No Reg','L1','Rew L1'])
ax.set_title('Computation Time for Different Algorithms')
ax.set_ylabel('Time (s)')
ax.legend()
fig.savefig('Figures/alg_time_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')
