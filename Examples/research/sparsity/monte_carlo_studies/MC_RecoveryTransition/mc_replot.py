# This file runs statistical studies of the algorithm for a 
# set of parameter values (order, delta, number of samples)

import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import TransportMaps.Algorithms.SparsityIdentification as SI
import GaussianGraphs as gg
import NonGaussianGraphs as ngg

font = {'family': 'serif', 'weight': 'normal', 'size':6}
matplotlib.rc('font', **font)

# define file to open data
dim = 6
order = 2
modeltype = 'stocvolhyper'
file_name = 'Data/data_stocvolhyper_dim6_ord2.txt'
data = np.loadtxt(file_name, delimiter=" ")

# define number of repeated iterations
n_iter = 5

# define grid for delta and number of samples
delta = np.linspace(0.25,2.0,8)
n_samps = np.linspace(200,1000,5)

# define regularization list
REG_list = [None, {'type':'L1','reweight':False}, {'type':'L1','reweight':True}]

# declare vectors to store errors
type1 = np.zeros((n_iter,len(delta),len(n_samps),len(REG_list)))
type2 = np.zeros((n_iter,len(delta),len(n_samps),len(REG_list)))
tot_error = np.zeros((n_iter,len(delta),len(n_samps),len(REG_list)))

# Extract columns from file
dim_ar   = data[:,0]
order_ar = data[:,1]
dt_ar    = data[:,2]
n_ar     = data[:,3]
run_ar   = data[:,4]
k_ar     = data[:,5]

for run in range(n_iter):
    for i,dt in enumerate(delta):
        for j,n in enumerate(n_samps):
            for k in range(len(REG_list)):

                idx1 = np.where(run_ar == run)
                idx2 = np.where(dt_ar == dt)
                idx3 = np.where(n_ar == n)
                idx4 = np.where(k_ar == k)
                idx = np.intersect1d(np.intersect1d(np.intersect1d(idx1, idx2), idx3), idx4)

                # save data in vectors
                type1[run,i,j,k] = data[idx,6]
                type2[run,i,j,k] = data[idx,7]
                tot_error[run,i,j,k] = data[idx,8]


# plot results
titles = ['No Reg','L1 Reg','Rew L1']
extents_x = [np.min(delta)-0.1, np.max(delta)+0.1]
extents_y = [np.min(n_samps)-50, np.max(n_samps)+50]
n_par = (dim_ar[0]**2-dim_ar[0])/2.0

fig,ax = plt.subplots(1,len(REG_list))
for k in range(len(REG_list)):
    cax = ax[k].imshow(np.mean(tot_error[:,:,:,k],axis=0), extent=extents_x +
            extents_y, interpolation='nearest', aspect='auto')
    ax[k].set_title('$P(G = \hat{G})$ - ' + titles[k])
    ax[k].set_xlabel('$\delta$')
    ax[k].set_ylabel('$N$')
    fig.colorbar(cax,ax=ax[k],ticks=[0,0.5,1],orientation='vertical')
plt.tight_layout()
fig.savefig('Figs_Replot/succ_prob_' + modeltype + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

for k in range(len(REG_list)):
    
    fig2,ax2 = plt.subplots(1,2)

    cax0 = ax2[0].imshow(np.mean(type1[:,:,:,k],axis=0), extent=extents_x +
            extents_y, interpolation='nearest', aspect='auto',vmin=0,
            vmax=n_par)
    ax2[0].set_title('Type 1 Error - ' + titles[k])
    ax2[0].set_xlabel('$\delta$')
    ax2[0].set_ylabel('$N$')
    plt.colorbar(cax0,ax=ax2[0],ticks=[0,n_par/2.,n_par],orientation='vertical')
 
    cax1 = ax2[1].imshow(np.mean(type2[:,:,:,k],axis=0), extent=extents_x +
            extents_y, interpolation='nearest', aspect='auto',vmin=0,
            vmax=n_par)
    ax2[1].set_title('Type 2 Error - ' + titles[k])
    ax2[1].set_xlabel('$\delta$')
    ax2[1].set_ylabel('$N$')
    plt.colorbar(cax1,ax=ax2[1],ticks=[0,n_par/2.,n_par],orientation='vertical')

    plt.tight_layout()

    fig2.savefig('Figs_Replot/errors_reg' + str(k) + '_' + modeltype + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

