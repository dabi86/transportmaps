# This file runs statistical studies of the algorithm for a 
# set of parameter values (order, delta, number of samples)

import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import TransportMaps.Algorithms.SparsityIdentification as SI
import GaussianGraphs as gg
import NonGaussianGraphs as ngg

# parameters to be varied
# dimension, order, ordering, delta, number of samples, number of runs

# define fixed set of parameters
dim = 9
model = gg.GridGraph(dim)

order = 1
ordering = SI.MinFill()

delta = 2.0

# define number of repeated iterations
n_iter = 5

# define file to save data
file_name = 'Data/data_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.txt'
file = open(file_name,'a')

# define number of samples
n_samps = np.linspace(100,1000,10)

# declare vectors to store errors
type1 = np.zeros((3,n_iter,len(n_samps)))
type2 = np.zeros((3,n_iter,len(n_samps)))
tot_error = np.zeros((3,n_iter,len(n_samps)))

# declare vector to store timing results
time_res = np.zeros((3,n_iter,len(n_samps)))

# define object for data generating distribution
true_graph = model.graph()

for i in range(n_iter):
    
    # import data and true graph
    data = model.rvs(int(np.max(n_samps)))

    # re-scale data
    mean_vec = np.mean(data,axis=0)
    data = data - mean_vec
    inv_var = 1./(np.var(data,axis=0))
    inv_std = np.diag(np.sqrt(inv_var))
    data = np.dot(data,inv_std)

    for i_n,n in enumerate(n_samps):

        # import data
        dataset = data[:int(n),:]

        print('------------------------------------------------------')
        print('SING Run ' + str(i+1) + '/' + str(n_iter) + ' - ' + str(int(n)) + ' samples') 

        # run SING with no regularization
        print('No Regularization')
        REG = None
        t0 = time.time()
        recovered_gp = SI.SING(dataset, order, ordering, delta, REG)
        time_res[0,i,i_n] = time.time() - t0

        # extract graph
        dim = recovered_gp.shape[0]
        adjacency = np.zeros([dim, dim])
        adjacency[recovered_gp != 0] = 1

        # compute difference of graphs 
        graph_diff = adjacency - true_graph

        # compute errors
        type1[0,i,i_n] = np.count_nonzero(graph_diff == 1)
        type2[0,i,i_n] = np.count_nonzero(graph_diff == -1)
        if np.sum(np.abs(graph_diff)) == 0:
            tot_error[0,i,i_n] = 1
        
        print(' ')

        # run SING with l1 regularization
        print('L1 Regularization')
        REG = {'type':'L1','reweight':False}
        t0 = time.time()
        recovered_gp = SI.SING(dataset, order, ordering, delta, REG) 
        time_res[1,i,i_n] = time.time() - t0

        # extract graph
        dim = recovered_gp.shape[0]
        adjacency = np.zeros([dim, dim])
        adjacency[recovered_gp != 0] = 1

        # compute difference of graphs 
        graph_diff = adjacency - true_graph

        # compute errors
        type1[1,i,i_n] = np.count_nonzero(graph_diff == 1)
        type2[1,i,i_n] = np.count_nonzero(graph_diff == -1)
        if np.sum(np.abs(graph_diff)) == 0:
            tot_error[1,i,i_n] = 1
 
        print(' ')

        # run SING with reweighted l1 regularization
        print('RL1 Regularization')
        REG = {'type':'L1','reweight':True}
        t0 = time.time()
        recovered_gp = SI.SING(dataset, order, ordering, delta, REG) 
        time_res[2,i,i_n] = time.time() - t0

        # extract graph
        dim = recovered_gp.shape[0]
        adjacency = np.zeros([dim, dim])
        adjacency[recovered_gp != 0] = 1

        # compute difference of graphs 
        graph_diff = adjacency - true_graph

        # compute errors
        type1[2,i,i_n] = np.count_nonzero(graph_diff == 1)
        type2[2,i,i_n] = np.count_nonzero(graph_diff == -1)
        if np.sum(np.abs(graph_diff)) == 0:
            tot_error[2,i,i_n] = 1

        print(' ')

        # save data
        file.write(str(dim) + ' ' + str(order) + ' ' + str(delta) + ' ' +
                str(n) + ' ' + str(i) + ' ' + str(0) + ' ' + str(type1[0,i,i_n]) + ' ' +
                str(type2[0,i,i_n]) + ' ' + str(tot_error[0,i,i_n]) + '\n')
        file.write(str(dim) + ' ' + str(order) + ' ' + str(delta) + ' ' +
                str(n) + ' ' + str(i) + ' ' + str(1) + ' ' + str(type1[1,i,i_n]) + ' ' +
                str(type2[1,i,i_n]) + ' ' + str(tot_error[1,i,i_n]) + '\n')
        file.write(str(dim) + ' ' + str(order) + ' ' + str(delta) + ' ' +
                str(n) + ' ' + str(i) + ' ' + str(2) + ' ' + str(type1[2,i,i_n]) + ' ' +
                str(type2[2,i,i_n]) + ' ' + str(tot_error[2,i,i_n]) + '\n')

# close data file
file.close()

# plot results
fig1 = plt.figure()
ax1 = fig1.add_subplot(1,1,1)
ax1.errorbar(n_samps, np.mean(tot_error[0,:,:],axis=0), np.std(tot_error[0,:,:],axis=0), marker='o', color = 'b', label='No Reg')
ax1.errorbar(n_samps, np.mean(tot_error[1,:,:],axis=0), np.std(tot_error[1,:,:],axis=0), marker='o', color = 'r', label='L1')
ax1.errorbar(n_samps, np.mean(tot_error[2,:,:],axis=0), np.std(tot_error[2,:,:],axis=0), marker='o', color = 'g', label='Rew L1')
ax1.set_title('Probability of Success')
ax1.set_xlabel('$N$')
ax1.set_ylabel('$P(G = \hat{G})$')
ax1.legend()
fig1.savefig('Figures/succ_prob_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

fig2 = plt.figure()
ax2 = fig2.add_subplot(1,1,1)
ax2.errorbar(n_samps, np.mean(type1[0,:,:],axis=0), np.std(type1[0,:,:],axis=0),
        marker='o',color='b',label='Type 1 Error')
ax2.errorbar(n_samps, np.mean(type2[0,:,:],axis=0), np.std(type2[0,:,:],axis=0),
        marker='o',color='r',label='Type 2 Error')
ax2.set_title('Error Counts - No Regularization')
ax2.set_xlabel('$N$')
ax2.set_ylabel('$Errors$')
ax2.legend()
fig2.savefig('Figures/errors_noreg_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

fig3 = plt.figure()
ax3 = fig3.add_subplot(1,1,1)
ax3.errorbar(n_samps, np.mean(type1[1,:,:],axis=0), np.std(type1[1,:,:],axis=0),
        marker='o',color='b',label='Type 1 Error')
ax3.errorbar(n_samps, np.mean(type2[1,:,:],axis=0), np.std(type2[1,:,:],axis=0),
        marker='o',color='r',label='Type 2 Error')
ax3.set_title('Error Counts - L1 Regularization')
ax3.set_xlabel('$N$')
ax3.set_ylabel('$Errors$')
ax3.legend()
fig3.savefig('Figures/errors_l1_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

fig4 = plt.figure()
ax4 = fig4.add_subplot(1,1,1)
ax4.errorbar(n_samps, np.mean(type1[2,:,:],axis=0), np.std(type1[2,:,:],axis=0),
        marker='o',color='b',label='Type 1 Error')
ax4.errorbar(n_samps, np.mean(type2[2,:,:],axis=0), np.std(type2[2,:,:],axis=0),
        marker='o',color='r',label='Type 2 Error')
ax4.set_title('Error Counts - Rew L1 Regularization')
ax4.set_xlabel('$N$')
ax4.set_ylabel('$Errors$')
ax4.legend()
fig4.savefig('Figures/errors_rl1_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

# reshape matrix
time_res = time_res.reshape((3,n_iter*len(n_samps))) 

fig5 = plt.figure()
ax5 = fig5.add_subplot(1,1,1)
ax5.boxplot(time_res.T)
ax5.set_xticklabels(['No Reg','L1','Rew L1'])
ax5.set_title('Computation Time for Different Algorithms')
ax5.set_ylabel('Time (s)')
ax5.legend()
fig5.savefig('Figures/alg_time_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')
