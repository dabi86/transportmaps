import numpy as np
from scipy.stats import norm
from scipy.stats import bernoulli
import itertools
import warnings
import h5py

class Rademacher():
    def __init__(self,dim):
        if dim%2 != 0:
            raise ValueError('Input Dimension Must be an Even Number')
        self.dim = dim
        self.type = 'rademacher'

    def edgeSet(self):
        omega = np.eye(self.dim)
        for i in range(0,self.dim,2):
            omega[i,i+1] = 1
            omega[i+1,i] = 1
        return omega

    def sigma(self):
        sigma = np.eye(self.dim)
        return sigma

    def rvs(self, N):
        rvs = np.zeros([N, self.dim])
        for i in range(0,self.dim,2):
            rvs[:,i] = norm.rvs(size=N)
            rade = 2*bernoulli.rvs(p=.5, size=N) - 1
            rvs[:,i+1] = rvs[:,i] * rade
            #print(rvs.shape[1])
        return rvs

    def graph(self):
        # returns graph adjancency matrix (i.e. sparsity of generalized precision)
        graph = self.edgeSet()
        return graph

class Uniform():
    def __init__(self,dim):
        if dim%2 != 0:
            raise ValueError('Input Dimension Must be an Even Number')
        self.dim = dim
        self.type = 'uniform'

    def sigma(self):
        sigma = np.eye(self.dim)
        return sigma

    def rvs(self, N):
        rvs = np.zeros([N, self.dim])
        for i in range(0,self.dim,2):
            rvs[:,i] = norm.rvs(size=N)
            unif = norm.rvs(size=N)
            rvs[:,i+1] = rvs[:,i] * unif
            print(rvs.shape[1])
        return rvs

    def graph(self):
        # returns graph adjancency matrix (i.e. sparsity of generalized precision)
        graph = np.eye(self.dim)
        for i in range(0,self.dim,2):
            graph[i,i+1] = 1
            graph[i+1,i] = 1
        return graph

    def n_edges(self):
        return self.dim/2.

class StochVolHyper():
    def __init__(self, dim):
        # dimension includes number of variables + hyperparameters
        self.dim = dim
        self.type = 'stocvolhyper'

    def rvs(self, N):
        if N > 1e5:
            raise ValueError('Number of samples must be less than 1e5')
        f = h5py.File('/home/rsb/Dropbox/Sparsity/StochasticVolatility/SVH-n10-sigma025-sequential-postprocess.dill.hdf5','r')
        samples = f['quadrature']['0'][:,:]
        rand_st = np.random.randint(np.floor(samples.shape[0]/N))
        rvs = samples[N*rand_st:N*(rand_st+1),:self.dim]
        return rvs

    def graph(self):
        graph = np.eye(self.dim) + np.eye(self.dim,k=-1) + np.eye(self.dim,k=+1)
        graph[:,:2] = np.ones((self.dim,2))
        graph[:2,:] = np.ones((2,self.dim))
        return graph

    def n_edges(self):
        n_edges = (self.dim - 1) + 1 + self.dim*3
        return n_edges

class StochVolChain():
    def __init__(self, dim):
        # dimension includes number of variables (no hyperparameters)
        self.dim = dim
        self.type = 'stocvol'

    def rvs(self, N):
        if N > 1e5:
            raise ValueError('Number of samples must be less than 1e5')
        f = h5py.File('/home/rsb/Dropbox/Sparsity/StochasticVolatility/SVH-n10-sigma025-sequential-postprocess.dill.hdf5','r')
        samples = f['quadrature']['0'][:,:]
        rand_st = np.random.randint(np.floor(samples.shape[0]/N))
        rvs = samples[N*rand_st:N*(rand_st+1),2:2+self.dim]
        return rvs

    def graph(self):
        graph = np.eye(self.dim) + np.eye(self.dim,k=-1) + np.eye(self.dim,k=+1)
        return graph

    def n_edges(self):
        n_edges = self.dim - 1
        return n_edges

