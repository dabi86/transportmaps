# This file runs statistical studies of the algorithm for a 
# set of parameter values (order, delta, number of samples)

import warnings
import time
import matplotlib
matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import TransportMaps.Algorithms.SparsityIdentification as SI
import GaussianGraphs as gg
import NonGaussianGraphs as ngg

# parameters to be varied
# dimension, order, ordering, delta, number of samples, number of runs

# define fixed set of parameters
dim = 6
model = gg.ChainGraph(dim)

order = 2
ordering = SI.MinFill()

delta = 2.0
REG = {'type':'L1','reweight':True}

# define number of repeated iterations
n_iter = 1

# define number of samples
n_samps = np.array([100])#np.linspace(100,1000,10)

# declare vectors to store errors
type1 = np.zeros((n_iter,len(n_samps)))
type2 = np.zeros((n_iter,len(n_samps)))
tot_error = np.zeros((n_iter,len(n_samps)))

# define object for data generating distribution
true_graph = model.graph()

for i in range(n_iter):
    
    # import data and true graph
    data = model.rvs(int(np.max(n_samps)))

    # re-scale data
    mean_vec = np.mean(data,axis=0)
    data = data - mean_vec
    inv_var = 1./(np.var(data,axis=0))
    inv_std = np.diag(np.sqrt(inv_var))
    data = np.dot(data,inv_std)

    for i_n,n in enumerate(n_samps):

        print('SING Run ' + str(i) + '/' + str(n_iter) + ' - ' + str(n) + ' samples') 

        # import data and run SING
        dataset = data[:int(n),:]
        recovered_gp = SI.SING(dataset, order, ordering, delta, REG)

        print(' ')

        # extract graph
        dim = recovered_gp.shape[0]
        adjacency = np.zeros([dim, dim])
        adjacency[recovered_gp != 0] = 1

        # compute difference of graphs 
        graph_diff = adjacency - true_graph

        # compute errors
        type1[i,i_n] = np.count_nonzero(graph_diff == 1)
        type2[i,i_n] = np.count_nonzero(graph_diff == -1)
        if np.sum(np.abs(graph_diff)) == 0:
            tot_error[i,i_n] = 1


# plot results
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(1,1,1)
#ax1.errorbar(n_samps, np.mean(tot_error,axis=0), np.std(tot_error,axis=0), marker='o', color = 'b')
#ax1.set_title('Probability of Success')
#ax1.set_xlabel('$N$')
#ax1.set_ylabel('$P(G = \hat{G})$')
#fig1.savefig('Figures/succ_prob_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')

#fig2 = plt.figure()
#ax2 = fig2.add_subplot(1,1,1)
#ax2.errorbar(n_samps, np.mean(type1,axis=0), np.std(type1,axis=0),
#        marker='o',color='b',label='Type 1 Error')
#ax2.errorbar(n_samps, np.mean(type2,axis=0), np.std(type2,axis=0),
#        marker='o',color='r',label='Type 2 Error')
#ax2.set_title('Error Counts')
#ax2.set_xlabel('$N$')
#ax2.set_ylabel('$Errors$')
#fig2.savefig('Figures/errors_' + model.type + '_dim' + str(dim) + '_ord' + str(order) + '.pdf')
