#
# This file runs the algorithm many times and computes the number of type 1 and type 2 errors made.
#

import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import GaussianGraphs as gg
import NonGaussianGraphs as ngg
import Algorithm
import NodeOrdering as no

from mpi4py import MPI
rank = MPI.COMM_WORLD.Get_rank()

def mc_alg_errors(graph_obj):
    dim = graph_obj.dim

    num_sets = 20

    # open file
    graph_str = type(graph_obj).__name__
    file_name = 'MC_Alg/data_mc_alg_errors_' + graph_str + '.txt'
    file = open(file_name,'a')
    
    if type(graph_obj).__name__ == 'ChainGraph' or 'GridGraph':
        orders = [1]
    else:
        orders = [2]

    # orderings = [no.ReverseCholesky, no.MinFill, no.MinDegree]
    orderings = [no.ReverseCholesky()] #, no.MinDegree]
    # loop for number 
    #for nsamps in [10, 50, 100, 500, 1000, 5000, 10000]:
    for nsamps in [125, 250, 500, 1000, 2000]:
    # for nsamps in [1000, 2000]:
        for i_ord,order in enumerate(orders):
            for i_ordering, ordering in enumerate(orderings):
                for delta in [.25, .5, .75, 1., 1.25, 1.5, 1.75]:
                # for delta in [1.9, 2.0, 2.1, 2.2]:
                # for delta in range(2,10):
                    for j in range(num_sets):
                         # create the data, and true omega
                         # returns perfect ordering
                         data0 = graph_obj.rvs(nsamps)
                         omega0 = graph_obj.omega()
                         recovered_gp = Algorithm.sing(graph_obj, data0, omega0, order, ordering, delta)
                         # count errors made by algorithm
                         error1 = 0 #not as bad: component is zero, but alg finds it non-zero
                         error2 = 0 #bad: component is non-zero, but alg sets it zero
                         for ii in range(dim):
                             for jj in range(ii):
                                 if omega0[ii,jj] == 0 and recovered_gp[ii,jj] != 0:
                                     error1 += 1
                                 elif omega0[ii,jj] != 0 and recovered_gp[ii,jj] == 0:
                                     error2 += 1
                    
                         # print results in file
                         file.write(str(dim) + ' ' + str(nsamps) + ' ' + str(order) + ' ' + str(ordering.id) + ' ' + str(delta) + ' ' + str(j) + ' ' + str(error1) + ' ' + str(error2) +'\n')
    file.close()

# Declare graph types
if rank == 0:
    graphs = [gg.ChainGraph]
    dim_list = [4, 8]
elif rank == 1:
    graphs = [gg.ChainGraph]
    dim_list = [12]
elif rank == 2:
    graphs = [gg.GridGraph]
    dim_list = [4, 9]

#dim_list = [9, 25, 49, 100]

for graph_type in graphs:
    for dim in dim_list:

        graph_obj = graph_type(dim)
        mc_alg_errors(graph_obj)
