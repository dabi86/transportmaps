#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2016 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Daniele Bigoni
# E-mail: dabi@limitcycle.it
#

import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from tabulate import tabulate
from scikits.sparse.cholmod import cholesky
from scipy.sparse import csc_matrix
from scipy.sparse import csr_matrix

import SpectralToolbox.Spectral1D as S1D

import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.tests.TestFunctions as TF

import ExtraDensities

def mc_sparsity(graph_obj):

    # fix random seed
    #np.random.seed(4)
    
    #initial setup
    #dim = 5
    dim = graph_obj.dim
    nax = np.newaxis
    setup = {}
    pi = {}
    setup['mu'] = np.zeros(dim)
    setup['sigma2'] = np.identity(dim)
    setup['dim'] = dim
    
    #create the data
    #nsamps = 5000
    num_sets = 1000
    
    #dist_number = 0
    #start with random matrix
    #A = np.random.random([dim,dim])
    #zero out upper triangular
    #A = np.tril(A)
    #A = np.diag(np.diag(A))
    #set some elements in lower triangular to zero (these will propagate)
    #A[2,0] = 0; A[3,0] = 0; A[3,1] = 0; A[4,0] = 0; A[4,1] = 0; A[4,2] = 0;
    
    #A \times A^T is SPD
    #Omega = np.dot(A,A.T)
    
    # compute Sigma (once) and find variances
    #Sigma_temp = np.linalg.inv(Omega)
    #Var_temp = np.diag(np.sqrt(np.diag(Sigma_temp)))
    
    # normalize A and compute new Omega
    #Atild = np.dot(Var_temp,A)
    #Omega = np.dot(Atild,Atild.T)
    #Sigma = np.dot(np.linalg.inv(Atild.T), np.linalg.inv(Atild))
    
    # retrieve omega and sigma
    Omega = graph_obj.omega()
    Sigma = graph_obj.sigma()

    print("\nNew Sigma= ", Sigma)
    
    L = np.linalg.cholesky(Omega)
    print("\ncholesky factor L of Omega= \n",L)
    #take cholesky factor to generate samples
    #Sigma = np.linalg.inv(Omega)
    K = np.linalg.cholesky(Sigma)
    Kinv = np.linalg.inv(K)
    
    # set list of different sparsity levels
    diag_active_vars = [[i] for i in range(0, dim)]
    #active_vars_list = [None, [[0],[0,1],[1,2],[2,3],[3,4]], [[0],[1],[2],[3],[4]]]
    active_vars_list = [None, graph_obj.active_vars(), diag_active_vars]
    print(active_vars_list[1])

    #going from target (samples) to standard normal
    #pi['base_density'] = ExtraDensities.SamplesDensity(data)
    pi['target_density'] = DENS.StandardNormalDensity(dim)
    #will keep support map but shouldn't need it in this example
    pi['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.zeros(dim),
                                                              np.ones(dim))
    # APPROXIMATION
    MONOTONE = 'intexp'
    
    # REGULARIZATION
    # None: No regularization
    # L2: L2 regularization
    REG = None
    # REG = {'type': 'None',
    #        'alpha': 1e-3}
    if REG is None:
        tit_reg = 'None'
    elif REG['type'] == 'L2':
        tit_reg = REG['type'] + '-' + str(REG['alpha'])
    
    
    # Gradient information
    # 0: derivative free
    # 1: gradient information
    # 2: Hessian information
    ders = 2
    
    # Approximation orders
    #orders = [1,3,5]
    orders = [1]
    # Tolerance
    tol = 1e-5
    
    # Generate samples of reference and target
    ###ref_samp = test['base_density'].rvs(n_samp)
    ###T_samp = test['support_map']( test['target_map'](ref_samp) )
    
    # ref_samp used for plotting (reset if needed)
    #ref_samp = data
    #T_samp = pi['target_density'].rvs(n_samp)
    
    # Define header for the output log
    if MONOTONE == 'polyconst':
        tit_type = 'poly_enf'
        log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
    elif MONOTONE == 'intexp':
        tit_type = 'monot'
        if ders in [0,1]:
            log_header = ['Order','#F.ev.','#J.ev','$L^2$-err']
        else:
            log_header = ['Order','#F.ev.','#J.ev','#H.ev','$L^2$-err']
    
    log = [ ]
    
    # Target density to be approximated L^\sharp \pi_{\rm tar}
    target_density = DENS.PullBackTransportMapDensity(pi['support_map'],
                                                      pi['target_density'])
    
    # open file
    graph_str = type(graph_obj).__name__ + str(graph_obj.dim)
    file_name = 'MC_Runs/data_mc_sparsity_' + graph_str + '.txt'
    file = open(file_name,'a')
    
    # loop for number 
    #for n_samps in [10, 50, 100, 500, 1000, 5000, 10000]:
    for n_samps in [125, 250, 500, 1000, 2000]:
        for j in range(num_sets):
    
            # generate dataset for each MC run
            #x =  DENS.StandardNormalDensity(dim).rvs(n_samps)
            #data = (np.dot(K,x.T)).T
            data = graph_obj.rvs(n_samps)
            print('\nRun: %d, Size: %d' %(j, n_samps))
            
            # L2 error estimation
            # 3: Gauss quadrature of order n
            # 0: Monte Carlo quadrature with n point
            qtype = 0
            qparams = n_samps
            tit_intest = str(qtype) + '-' + str(qparams)
    
            # define base_density from samples
            pi['base_density'] = ExtraDensities.SamplesDensity(data)
    
            #use all variables to begin
            #active_vars=None
            #set constant tolerance, for now
            #tau = 1.e-1
            #set initial sparsity level
            #sparsity = [0]
            #sparsityIncreasing = True
            #set initial 
            #n_active_vars = [(np.power(dim,2) + dim)/2]
            #create list to store permutations
            #perm_list = []
            #create total_perm vector
            #total_perm = np.arange(dim)
    
            for spar in range(len(active_vars_list)):
    
                for i_ord,order in enumerate(orders):
                    print("\nSpar: %d Order: %d" % (spar, order))
       
                    # Build the transport map (isotropic for each entry)
                    #active_vars = [[0],[0,1],[0,1,2],[2,3],[3,4]]
                    tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
                           setup['dim'], order, active_vars=active_vars_list[spar])
                    # Construct density
                    # Density T_\sharp \pi
                    tm_density = DENS.PushForwardTransportMapDensity(tm_approx, pi['base_density'])
                     
                    # SOLVE
                    log_entry_solve = tm_density.minimize_kl_divergence(target_density, qtype=qtype,
                                                                        qparams=qparams,
                                                                        regularization=REG,
                                                                        tol=tol, ders=ders)
                    #log_entry = [order] + log_entry_solve
            
                    # PRINT LINEAR MAP
                    #Tmean = np.zeros(dim)
                    #w = np.array([np.sqrt(1/np.sqrt(2*np.pi)),np.sqrt(1/np.sqrt(2*np.pi))])
                    # TODO: check orders
                   # for i, approx in enumerate(tm_approx.approx_list):
                     #   print("component %d: "%i)
                      #  print("    constant: %s "%str(approx.c.get_coeffs()))
                       # Tmean[i] = approx.c.get_coeffs()[0]*(w[0]**(i+1))
        
                    #print("\nOmega = \n",Omega)
        
                    # EVALUATE MAP ON IDENTITY TO RECOVER MATRIX
                    S = (tm_approx.evaluate(np.identity(setup['dim']))).T - \
                            tm_approx.evaluate(np.zeros((1,setup['dim']))).T
                    # gx = tm_approx.grad_x(np.zeros((1,setup['dim'])))
                    # for i in range(setup['dim']):
                    #    S[:,i] = S[:,i] #- Tmean
        
                    #print("\n\nS = ",S)
                    #print("\n\nS^T*S = ",np.dot(S.T,S))
                    omegaHat = np.dot(S.T,S)
                    
                    # compute entrywise error in S and Omega
                    #S_error = np.abs(S - Kinv)
                    #Omega_error = np.abs(omegaHat - Omega)
    
                    # reshape S_error and Omega_error matrices
                    #S_error = np.reshape(S_error, (1, dim*dim))[0]
                    #Omega_error = np.reshape(Omega_error, (1, dim*dim))[0]
    
                    # reshape S and omegaHat
                    S = np.reshape(S, (1, dim*dim))[0]
                    omegaHat = np.reshape(omegaHat, (1, dim*dim))[0]

                    # print results in file
                    file.write(str(n_samps) + ' ' + str(j) + ' ' + str(spar) + ' ' +
                            str(order) + ' ' + ' '.join('{0:.4f}'.format(s) for s in S) +
                            ' ' + ' '.join('{0:.4f}'.format(o) for o in omegaHat)
                            + '\n')
    
                    # reorder omega and compute l2 error
                    #Omega_reorder = Omega[:,total_perm][total_perm,:]
                    #print("\n\nL2 Error= ", np.linalg.norm(Omega_reorder - omegaHat, ord='fro'))
    
                    # threshold omegaHat and find cholesky ordering
                    #omegaHat[np.abs(omegaHat) < tau] = 0
                    #chol_factor = cholesky(csr_matrix(omegaHat))
                    #perm_vect = chol_factor.P()
                    
                    #print("\n\nOmegaHat_Treshold = ", omegaHat)
                    
                    # reverse ordering and apply to omegaHat
                    #perm_vect = np.flipud(perm_vect)
                    #omegaHat = omegaHat[:,perm_vect][perm_vect,:]
    
                    # apply re-ordering to data
                    #data = data[:, perm_vect]
    
                    # add permutation to perm_list
                    #perm_list.append(perm_vect)
                    #print(perm_vect)
    
                    # convolve permutation with total_perm
                    #total_perm = total_perm[perm_vect]
                    #print(total_perm)
    
                    # find list of active_vars   
                    #omegaHatLower = np.tril(omegaHat)
                    #active_vars = []
                    #for i in range(dim):
                    #    actives = np.where(omegaHatLower[i,:] != 0)
                    #    #actives = np.where(np.abs(omegaHatLower[i,:]) >= tau)
                    #    active_vars.append(list(set(actives[0]) | set([i])))
    
                    #print("\n\nOmegaHat = ", omegaHat)
                    #print("\n\nActive_Vars = ", active_vars)
    
                    # find n_active_vars
                    #n_active_vars.append(np.sum([len(x) for x in active_vars]))
    
                    # find current sparsity level
                    #sparsity.append(n_active_vars[0] - n_active_vars[-1])
                    #print(sparsity)
    
                    # set sparsityIncreasing
                    #if sparsity[-1] <= sparsity[-2]:
                    #    sparsityIncreasing = False
    
                    
    
                    # Construct approximate density L_\sharp T_\sharp \pi
                    #approx_density = DENS.PushForwardTransportMapDensity(pi['support_map'], tm_density)
    
                    # Push forward reference samples in two steps
                    # (1) Map x from \pi to y from T_\sharp \pi
                    # (2) Map y from T_\sharp \pi to z from L_\sharp T_\sharp \pi
                   # tmp = tm_density.map_samples_base_to_target( ref_samp )
                   # fapp_samp = approx_density.map_samples_base_to_target( tmp ) 
        
                   # 
                   # # Compute L2 error
                   # dist = np.sqrt( np.sum((T_samp - fapp_samp)**2., axis=1) )
                   # log_entry.append( np.sqrt( np.sum( dist**2. ) / float(n_samp) ) )
                   # 
                   # log.append( log_entry )
        
        # PRINT ERROR TABLE
        #print(" ")
        #print(tabulate(log, headers=log_header))
        #print(" ")
        #
        #print(tabulate(log, headers=log_header, tablefmt="latex", floatfmt=".2e"))
    
    file.close()
    
    #import resource
    #print ('Memory usage (MB): ',
    #        resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.)
