#
# This file runs the algorithm many times and computes the number of type 1 and type 2 errors made.
#

import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import Algorithm
import NodeOrdering as no
import h5py

from mpi4py import MPI
rank = MPI.COMM_WORLD.Get_rank()

def mc_alg_errors(dim, rank):
    print('rank = ',rank)
    # Number of data runs
    num_sets = 100
    # Map order
    order = 2

    # open file
    f = h5py.File('/home/rmorriso/Dropbox/Sparsity/StochasticVolatility/SVH-n10-sigma025-sequential-postprocess.dill.hdf5','r')
    samples = f['quadrature']['0'][:,:]
    data = samples[:,2:]

    # open file for saving data
    file_name = 'stochvol_data/errors_stochvol' + str(rank) + '.txt'
    file = open(file_name,'a')

    ordering = no.ReverseCholesky()
    # loop for number 
    for nsamps in [125, 250, 500, 1000, 2000, 4000]:
        for delta in [1.0, 2.0, 3.0, 4.0, 5.0]:
            for j in range(num_sets):
                data_local = data[j*nsamps:(j+1)*nsamps,:dim]
                # Scale the data
                mean_vec = np.mean(data_local,axis=0)
                data_local = data_local - mean_vec
                inv_var = 1./(np.var(data_local,axis=0))
                inv_std = np.diag(np.sqrt(inv_var))
                rescaled_data = np.dot(data_local,inv_std)
                # Run SING algorithm
                recovered_gp = Algorithm.SING(rescaled_data,  order, ordering, delta)
                # count errors made by algorithm
                omega0 = np.eye(dim) + np.eye(dim,k=-1) + np.eye(dim,k=+1)
                error1 = 0 #not as bad: component is zero, but alg finds it non-zero
                error2 = 0 #bad: component is non-zero, but alg sets it zero
                for ii in range(dim):
                    for jj in range(ii):
                        if omega0[ii,jj] == 0 and recovered_gp[ii,jj] != 0:
                            error1 += 1
                        elif omega0[ii,jj] != 0 and recovered_gp[ii,jj] == 0:
                            error2 += 1
            
                # print results in file
                file.write(str(dim) + ' ' + str(nsamps) + ' ' + str(order) + ' ' + str(ordering.id) + ' ' + str(delta) + ' ' + str(j) + ' ' + str(error1) + ' ' + str(error2) +'\n')
    file.close()

# Declare graph types
if rank == 0:
    dim = 4
elif rank == 1:
    dim = 6
elif rank == 2:
    dim = 8

mc_alg_errors(dim, rank)
