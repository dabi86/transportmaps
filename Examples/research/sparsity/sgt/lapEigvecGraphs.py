#
import warnings
import time
import matplotlib
# matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import TransportMaps.Algorithms.SparsityIdentification as SI
import TransportMaps.Distributions.FrozenDistributions as FD

p = 20
i1 = 1
i2 = 18
gtype = 0

if gtype == 0:
    g = FD.ChainGraphGaussianDistribution(p)
    A = g.graph
    A[0,-1] = 1
    A[-1,0] = 1
elif gtype == 1:
    g = FD.GridGraphGaussianDistribution(p)
    A = g.graph
elif gtype == 2:
    g = FD.StarGraphGaussianDistribution(p)
    A = g.graph

np.fill_diagonal(A,0)
edges = np.copy(A)
d = np.sum(A,axis=0)
D = np.diag(d)
L = D - A
print(L)

l, v = np.linalg.eig(L)
idx = l.argsort()[::1]
l = l[idx]
v = v[:,idx]

print(l)
for i in range(p):
    for j in range(p):
        if edges[i,j] == 1:
            plt.plot((v[i,i1],v[j,i1]),(v[i,i2],v[j,i2]))

# plt.plot(v[:,i2],v[:,i1])
# plt.plot((v[-1,i2],v[0,i2]),(v[-1,i1],v[0,i1]))
plt.show()
