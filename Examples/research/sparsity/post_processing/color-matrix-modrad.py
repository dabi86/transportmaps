import matplotlib
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np
from numpy import loadtxt
import matplotlib.patches as mpatches

rc('text',usetex=True)
font={'family' : 'normal',
    'weight' : 'bold',
    'size' :24}
matplotlib.rc('font',**font)

# dataFile = "adj-norm.txt"
dataFile = "temp/adj-mr-beta1.txt"
A = loadtxt(dataFile,comments="%")
p = 10
A[np.nonzero(A)] = 1
# for i in range(4):
i = 0
adj = A[10*i:10*(i+1),:]
#     print(adj)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.imshow(adj,interpolation='nearest', cmap = plt.cm.YlGnBu, alpha=.9, extent=[1,10,10,1])

# ax.text(2,10.2,'Orbit')
# ax.text(10.2,2,'Power')
# ax.annotate('Attitude', xy=(6, 6.5), xytext=(7.5, 10.2), arrowprops=dict(facecolor='black', shrink=0.08))

    # plt.savefig('temp/matrix-slots-mr-%s-nips.pdf' %i)#%s%s%s.pdf' %(k1,k2,k3))
plt.savefig('temp/matrix-slots-mr-beta1-nips.pdf')
#d = [];
#for i in range(0,50):
 #d.extend(data[i,2:])
# A = -A
# red_patch = mpatches.Patch(color='red', label='Negative')
# blue_patch = mpatches.Patch(color='blue', label='Positive')
# white_patch = mpatches.Patch(color='white', ec='black', label='Zero')
# plt.legend(handles=[red_patch,blue_patch,white_patch],bbox_to_anchor=(1.3,1.0))
# plt.show()
