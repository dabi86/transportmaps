# post process the mc study data of the sparse maps from samples

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import matplotlib
from matplotlib import rc
from math import *

#import cmocean
import GaussianGraphs as gg
import NonGaussianGraphs as ng

rc('text',usetex=True)
font={'family' : 'normal',
    'weight' : 'normal',
    'size' :14}
matplotlib.rc('font',**font)

# distribution type
#graph_list =[gg.ChainGraph, gg.GridGraph]
#graph_names =['Chain graph', 'Grid graph']
#deg = [2,4]
# graph_list = [gg.GridGraph]
graph_names = ['stochVol']
deg = 2

PLOTTING = False

# for graph_idx, graph in enumerate(graph_list):

# Name datafile using graph_obj
datafile = '../monte_carlo_studies/stochvol_data/errors_stochvol.txt'
#datafile = 'MC_Alg/errors' + graph.__name__ +'.txt'

# read in datafile
data = np.loadtxt(datafile, comments='%')

# fix unique values for parameters
dim      = np.unique(data[:,0])
n        = np.unique(data[:,1])
porder   = np.unique(data[:,2])
ordering = np.unique(data[:,3])
delta    = np.unique(data[:,4])

# -------------------------------------
# Success probability plot

#dim_style  = ['-o','-^','-*','-v','-+','-x','-D','-p']
delta_style = ['b','c','r','y','orange','r','m','k']

success_avg = np.zeros((len(dim),len(delta),len(n)))

# setup plots for convergence studies
# fig_success = plt.figure()
# ax_success = fig_success.add_subplot(1,1,1)

#delta = [4.0, 6.0, 8.0]

# for i,dim_i in enumerate(dim):
#     for j,delta_j in enumerate(delta):
#         for k,n_k in enumerate(n):

#             # find indices for the given dim and delta
#             dim_delta = np.intersect1d(np.where(data[:,0] == dim_i), \
#                     np.where(data[:,4] == delta_j))
#             dim_delta_n = np.intersect1d(dim_delta, np.where(data[:,1] == n_k))

#             # extract errors from both rows
#             type1_error = data[dim_delta_n,6:7]
#             type2_error = data[dim_delta_n,7:8]
#             total_error = type1_error + type2_error

#             # compute success probability
#             success = np.ones(len(total_error))
#             success[np.where(total_error[:,0] > 0)] = 0
#             success_avg[i,j,k] = np.mean(success)
#             print('graph: %s, dim: %d, delta: %.1f, samps: %d' %(graph.__name__, dim_i, delta_j, n_k))

#         ax_success.plot(n/(np.log(dim_i)*deg[graph_idx]), success_avg[i,j,:], '-', color=delta_style[j], linewidth=2, \
#                  label='Dim: %d, Delta: %.1f' %(dim_i, delta_j))
# ax_success.set_title(graph_names[graph_idx])
# ax_success.set_ylabel('Success probability')
# ax_success.set_xlabel('$n/(d \log{p})$')
# # ax_success.set_xlim([0, 750])
# ax_success.set_ylim([0.0, 1.0])

# black_line = mlines.Line2D([],[],color='black',label='2')
# blue_line = mlines.Line2D([],[],color='blue',label='4')
# cyan_line = mlines.Line2D([],[],color='cyan',label='6')
# red_line = mlines.Line2D([],[],color='red',label='8')
# ax_success.legend(handles=[blue_line,cyan_line,red_line],labels=['4','6','8'],loc=4,title='$\delta$')
# #fig_success.savefig('MC_Alg_Plots/success_prob_'+graph.__name__+'.pdf')
# fig_success.savefig('MC_Alg_Plots/success_prob_'+graph.__name__+'-CSE.pdf')

for i,dim_i in enumerate(dim):

    # define total edges
    tot_edges = dim_i - 1
    
    # set up plots for delta study
    fig_delta = plt.figure()
    ax_delta = fig_delta.add_subplot(1,1,1)
    for k,n_k in enumerate(n):
        num_fp = [0] * len(delta)
        num_fn = [0] * len(delta)
        for j,delta_j in enumerate(delta):

            # find indices for the given dim and delta
            dim_delta = np.intersect1d(np.where(data[:,0] == dim_i), \
                    np.where(data[:,4] == delta_j))
            dim_delta_n = np.intersect1d(dim_delta, np.where(data[:,1] == n_k))

            # extract errors from both rows
            num_fp[j] = np.mean(data[dim_delta_n,6:7])
            num_fn[j] = np.mean(data[dim_delta_n,7:8])
            # print('graph: %s, dim: %d, delta: %.1f, samps: %d' %(graph.__name__, dim_i, delta_j, n_k))
        #print(type1_error)
        #print(type2_error)

        #compute true positive
        #norm_1mfp = 1 - num_fp/(((dim_i)**2 - dim_i)/2. - graph(dim_i).n_edges())
        #norm_1mfn = 1 - num_fn/(graph(dim_i).n_edges())
        norm_1mfp = 1 - num_fp/(((dim_i)**2 - dim_i)/2. - tot_edges)
        norm_1mfn = 1 - num_fn/tot_edges
        
        sum_pos = norm_1mfp + norm_1mfn
        max_idx = np.argmax(sum_pos)
        ax_delta.plot(norm_1mfn[max_idx],norm_1mfp[max_idx],'k*',markersize=7)

        # ax_delta.plot(norm_1mfn, norm_1mfp, linewidth=2, label='n= %d, $\delta^*$= %.1f' %(n_k, delta[max_idx]))
        ax_delta.plot(norm_1mfn, norm_1mfp, linewidth=2, label='%4d,  %.1f' %(n_k, delta[max_idx]))

    ax_delta.set_title('ROC curve')
    ax_delta.legend(title=' $\phantom{---------------}  n,\quad \quad \,\,  \delta^*$', loc ='best')
    ax_delta.set_xlim([0, 1])
    ax_delta.set_ylim([0, 1])
    ax_delta.set_xlabel('1 - FN')
    ax_delta.set_ylabel('1 - FP')
    fig_delta.savefig('../monte_carlo_studies/MC_Plots/deltaplots/'+str(int(dim_i))+'.pdf')

if PLOTTING == True:
    plt.show()
