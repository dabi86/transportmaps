# post process the mc study data of the sparse maps from samples

import matplotlib.pyplot as plt
import numpy as np
import matplotlib
from matplotlib import rc
from math import *

#import cmocean
import GaussianGraphs as gg

rc('text',usetex=True)
font={'family' : 'normal',
    'weight' : 'normal',
    'size' :16}
matplotlib.rc('font',**font)

# distribution type
graph_list = [gg.ChainGraph, gg.StarGraph, gg.GridGraph]
graph_names = ['Chain graph', 'Star graph', 'Grid graph']
dim_list   = [16]

PLOTTING = False

for g, graph_type in enumerate(graph_list):
    for d, dim_num in enumerate(dim_list):
        
       # setup plots for convergence studies
        fig_S_err = plt.figure()
        ax_S_err = fig_S_err.add_subplot(1,1,1)
        
        fig_S_var = plt.figure()
        ax_S_var = fig_S_var.add_subplot(1,1,1)

        fig_S_bias = plt.figure()
        ax_S_bias = fig_S_bias.add_subplot(1,1,1)

        fig_Om_err = plt.figure()
        ax_Om_err = fig_Om_err.add_subplot(1,1,1)
        
        fig_Om_var = plt.figure()
        ax_Om_var = fig_Om_var.add_subplot(1,1,1)

        fig_Om_bias = plt.figure()
        ax_Om_bias = fig_Om_bias.add_subplot(1,1,1)

        # Create object
        graph_obj = graph_type(dim_num)

        # Extract S_true and Omega_true
        Omega_true = graph_obj.omega()
        S_true = np.linalg.inv(np.linalg.cholesky(graph_obj.sigma()))

        # Resize S_true and Omega_true
        Omega_true_res = np.reshape(Omega_true, (1, dim_num*dim_num))[0]
        S_true_res = np.reshape(S_true, (1, dim_num*dim_num))[0]

        # Name datafile using graph_obj
        datafile = '../monte_carlo_studies/MC_Runs/data_mc_sparsity_' + type(graph_obj).__name__ + str(dim_num)+'.txt'
        
        # read in datafile
        data = np.loadtxt(datafile, comments='%')
        
        # fix unique values for parameters
        N     = np.unique(data[:,0])
        spar  = np.unique(data[:,2])
        order = np.unique(data[:,3])
        
        # find dimension of distribution
        dim_sq = int((data.shape[1] - 4)/2)
        
        spar_marker  = ['o','^','*']
        spar_style = ['b','r','g']
        spar_names = ['Under','Exact','Over']
        
        for i in range(len(spar)):
            for j in range(len(order)):
        
               # find indices for the given spar and order
                spar_order_int = np.intersect1d(np.where(data[:,2] == spar[i]), \
                        np.where(data[:,3] == order[j]))
        
                # find S_error for all n in N
                S_mse_fro_list   = []
                Om_mse_fro_list  = []
                S_var_fro_list   = []
                Om_var_fro_list  = []
                S_bias_fro_list  = []
                Om_bias_fro_list = []

                for n in N:
        
                    # find intersection of spar_order_int and n=N
                    k_ind = np.intersect1d(spar_order_int, np.where(data[:,0] == n))
        
                    # extract S and Om data for k_ind rows
                    S_data_rows = data[k_ind,4:4+dim_sq]
                    Om_data_rows = data[k_ind,4+dim_sq:]

                    # compute mean of data rows
                    S_data_mean = np.mean(S_data_rows, axis=0)
                    Om_data_mean = np.mean(Om_data_rows, axis=0)

                    # -------------------------------------------------------------

                    # Compute MSE of S and Om mean with respect to true S, Om
                    S_mse_rows = S_data_rows - S_true_res
                    Om_mse_rows = Om_data_rows - Omega_true_res

                    S_fro_mse_row = []
                    Om_fro_mse_row = []
                    for row in range(S_mse_rows.shape[0]):
                        # take mean of frobenius error vector
                        S_fro_mse_row.append(np.power(np.linalg.norm(S_mse_rows[row,:,np.newaxis],'fro'),2))
                        Om_fro_mse_row.append(np.power(np.linalg.norm(Om_mse_rows[row,:,np.newaxis],'fro'),2))
                    
                    # take mean of frobenius mse vector
                    S_mse_fro_list.append(np.mean(S_fro_mse_row))
                    Om_mse_fro_list.append(np.mean(Om_fro_mse_row))
                    
                    # -------------------------------------------------------------

                    # Compute variance of S and Om
                    S_err_rows  = S_data_rows - S_data_mean
                    Om_err_rows = Om_data_rows - Om_data_mean
                     
                    # extract frobenius norm error for each row
                    S_fro_var_row = []
                    Om_fro_var_row = []
                    for row in range(S_err_rows.shape[0]):
                        S_fro_var_row.append(np.power(np.linalg.norm(S_err_rows[row,:,np.newaxis],'fro'),2))
                        Om_fro_var_row.append(np.power(np.linalg.norm(Om_err_rows[row,:,np.newaxis],'fro'),2))

                    # take mean of frobenius error vector
                    S_var_fro_list.append(np.mean(S_fro_var_row))
                    Om_var_fro_list.append(np.mean(Om_fro_var_row))      
 
                    # -------------------------------------------------------------

                    # Compute bias
                    S_bias_fro = np.power(np.linalg.norm((S_data_mean - S_true_res)[:,np.newaxis],'fro'),2)
                    Om_bias_fro = np.power(np.linalg.norm((Om_data_mean - Omega_true_res)[:,np.newaxis],'fro'),2)
                    
                    # insert bias in list
                    S_bias_fro_list.append(S_bias_fro)
                    Om_bias_fro_list.append(Om_bias_fro)

                # add point to plot
                #plt.plot(N, S_error_fro_list, spar_style[i], color=order_style[j], linewidth=2)
                ax_S_err.loglog(N, S_mse_fro_list, color=spar_style[i],\
                        linewidth=2, linestyle='-', marker=spar_marker[i],label=spar_names[i])
                ax_Om_err.loglog(N, Om_mse_fro_list, color=spar_style[i],\
                        linewidth=2, linestyle='-', marker=spar_marker[i],label=spar_names[i])
                ax_S_var.loglog(N, S_var_fro_list, color=spar_style[i],\
                        linewidth=2, linestyle='-', marker=spar_marker[i],label=spar_names[i])
                ax_Om_var.loglog(N, Om_var_fro_list, color=spar_style[i],\
                        linewidth=2, linestyle='-', marker=spar_marker[i],label=spar_names[i])
                ax_S_bias.loglog(N, S_bias_fro_list, color=spar_style[i],\
                        linewidth=2, linestyle='-', marker=spar_marker[i],label=spar_names[i])
                ax_Om_bias.loglog(N, Om_bias_fro_list, color=spar_style[i],\
                        linewidth=2, linestyle='-', marker=spar_marker[i],label=spar_names[i])

        ticks = [100, 1000];

        ax_S_err.set_title(graph_names[g]+',  p = '+str(dim_num))
        ax_Om_err.set_title(graph_names[g]+',  p = '+str(dim_num))
        ax_S_err.set_xlabel('Number of samples')
        ax_Om_err.set_xlabel('Number of samples')
        ax_S_err.set_ylabel('MSE of map $S$')
        ax_Om_err.set_ylabel('MSE of precision matrix $\hat{\Omega}$')
        ax_S_err.legend(title = 'Sparsity level',loc=3)
        ax_Om_err.legend(title = 'Sparsity level',loc=3)
        ax_S_err.set_xticks(ticks)
        ax_Om_err.set_xticks(ticks)
        
        ax_S_var.set_title(graph_names[g]+',  p = '+str(dim_num))
        ax_Om_var.set_title(graph_names[g]+',  p = '+str(dim_num))
        ax_S_var.set_xlabel('Number of samples')
        ax_Om_var.set_xlabel('Number of samples')
        ax_S_var.set_ylabel('Average variance in map $S$')
        # ax_Om_var.set_ylabel('Average variance in precision matrix $\Omega$')
        ax_Om_var.set_ylabel('Average variance in $\hat{\Omega}$')
        ax_S_var.legend(title = 'Sparsity level',loc=3)
        ax_Om_var.legend(title = 'Sparsity level',loc=3)
        ax_S_var.set_xticks(ticks)
        ax_Om_var.set_xticks(ticks)
        
        ax_S_bias.set_title(graph_names[g]+',  p = '+str(dim_num))
        ax_Om_bias.set_title(graph_names[g]+',  p = '+str(dim_num))
        ax_S_bias.set_xlabel('Number of samples')
        ax_Om_bias.set_xlabel('Number of samples')
        ax_S_bias.set_ylabel('Bias squared in map $S$')
        # ax_Om_bias.set_ylabel('Bias squared in precision matrix $\Omega$')
        ax_Om_bias.set_ylabel('Bias squared in $\hat{\Omega}$')
        ax_S_bias.legend(title = 'Sparsity level',loc=3)
        ax_Om_bias.legend(title = 'Sparsity level',loc=3)
        ax_S_bias.set_xticks(ticks)
        ax_Om_bias.set_xticks(ticks)
        
        fig_S_err.savefig('temp/S_fro_error_'+type(graph_obj).__name__+str(dim_num)+'nips.pdf')
        fig_Om_err.savefig('temp/Om_fro_error_'+type(graph_obj).__name__+str(dim_num)+'nips.pdf')

        fig_S_var.savefig('temp/S_fro_var_'+type(graph_obj).__name__+str(dim_num)+'nips.pdf')
        fig_Om_var.savefig('temp/Om_fro_var_'+type(graph_obj).__name__+str(dim_num)+'nips.pdf')

        fig_S_bias.savefig('temp/S_fro_bias_'+type(graph_obj).__name__+str(dim_num)+'nips.pdf')
        fig_Om_bias.savefig('temp/Om_fro_bias_'+type(graph_obj).__name__+str(dim_num)+'nips.pdf')
        
        # -------------------------------------------------------------
        
        # # find maximum entries to set colorbar
        # spar_without_2 = np.where(data[:,2] != 2)
        # max_S_cbar = np.max(np.mean(data[spar_without_2,4:4+dim_sq][0], axis=0) - S_true_res)
        # max_Om_cbar = np.max(np.mean(data[spar_without_2,4+dim_sq:][0], axis=0) - Omega_true_res)

        # # plotting for error in matrix elements (max order, max N)
        # for i in range(len(spar)):
        
        #     # find max order and max N
        #     order_max = np.max(order)
        #     N_max = np.max(N)
        
        #     # find indices for given sparsity and max order,N
        #     k_ind = np.intersect1d(np.where(data[:,0] == N_max), np.where(data[:,3] == order_max))
        #     k_ind = np.intersect1d(k_ind, np.where(data[:,2] == spar[i]))
        
        #     # extract data for given rows
        #     S_data_rows  = data[k_ind,4:4+dim_sq]
        #     Om_data_rows = data[k_ind,4+dim_sq:]
        
        #     # compute means of entries
        #     S_data_mean  = np.mean(S_data_rows, axis=0)
        #     Om_data_mean = np.mean(Om_data_rows, axis =0)
       
        #     # Compute error in mean
        #     S_err = np.abs(S_data_mean - S_true_res)
        #     Om_err = np.abs(Om_data_mean - Omega_true_res)

        #     # reshape S_err and Om_err
        #     S_err = np.reshape(S_err, (dim_num, dim_num))
        #     Om_err = np.reshape(Om_err, (dim_num, dim_num))

        #     # plot results for S
        #     fig_S_mat = plt.figure()
        #     ax_S_mat = fig_S_mat.add_subplot(1,1,1)
        #     if i!=2:
        #         res = ax_S_mat.imshow(S_err, vmin=0, vmax=max_S_cbar, cmap='jet', interpolation='nearest')
        #     else: 
        #         res = ax_S_mat.imshow(S_err, vmin=0, cmap='jet', interpolation='nearest')
        #     fig_S_mat.colorbar(res)
        #     ax_S_mat.set_title('$S$ error: Sparsity = %d, N = %d' %(spar[i], N_max))
        
        #     # plot results for Omega
        #     fig_Om_mat = plt.figure()
        #     ax_Om_mat = fig_Om_mat.add_subplot(1,1,1)
        #     if i!=2:
        #         res = ax_Om_mat.imshow(Om_err, vmin=0, vmax=max_Om_cbar, cmap='jet', interpolation='nearest')
        #     else:
        #         res = ax_Om_mat.imshow(Om_err, vmin=0, cmap='jet', interpolation='nearest')
        #     fig_Om_mat.colorbar(res)
        #     ax_Om_mat.set_title('$\Omega$ error: Sparsity = %d, N = %d' %(spar[i], N_max))
           
        #     fig_S_mat.savefig('MC_Plots/S_matrix_error_'+type(graph_obj).__name__+str(dim_num)+'spar'+str(int(spar[i]))+'.pdf')
        #     fig_Om_mat.savefig('MC_Plots/Om_matrix_error_'+type(graph_obj).__name__+str(dim_num)+'spar'+str(int(spar[i]))+'.pdf')
        
    if PLOTTING == True:
        plt.show()
