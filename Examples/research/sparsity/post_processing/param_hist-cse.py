# plot histograms of the elements of the map $S$ and matrix $\Omega$

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
import numpy as np
import matplotlib
from matplotlib import rc
matplotlib.rc('axes',edgecolor='lightgray')
from math import *
from scipy.stats import norm

import GaussianGraphs as gg

rc('text',usetex=True)
font={'family' : 'normal',
    'weight' : 'normal',
    'size' :14}
matplotlib.rc('font',**font)
#matplotlib.rcParams['xtick.labelsize'] = 2#label_size

# distribution type
#graph_list = [gg.GridGraph]
#dim_list   = [16]
graph_list = [gg.ChainGraph, gg.StarGraph, gg.GridGraph]
graph_names = ['Chain graph', 'Star graph', 'Grid graph']
dim_list   = [4, 9, 16]

PLOTTING = False

# def gaussian(x, mu, var):
#     return 1./np.sqrt(2*np.pi*var) * np.exp(-(x - mu)**2. / (2 * var))

for g, graph_type in enumerate(graph_list):
    for d, dim_num in enumerate(dim_list):
        
        # Create object
        graph_obj = graph_type(dim_num)

        # Extract S_true and Omega_true
        Omega_true = graph_obj.omega()
        S_true = np.linalg.inv(np.linalg.cholesky(graph_obj.sigma()))

        # Resize S_true and Omega_true
        #Omega_true_res = np.reshape(Omega_true, (1, dim_num*dim_num))[0]
        #S_true_res = np.reshape(S_true, (1, dim_num*dim_num))[0]

        # Name datafile using graph_obj
        print(type(graph_obj).__name__)
        datafile = 'MC_Runs/data_mc_sparsity_' + type(graph_obj).__name__ + str(dim_num)+'.txt'
        
        # read in datafile
        data = np.loadtxt(datafile, comments='%')
        
        # fix unique values for parameters
        N     = np.unique(data[:,0])
        spar  = np.unique(data[:,2])
        order = np.unique(data[:,3])
        
        # find dimension of distribution
        dim_sq = int((data.shape[1] - 4)/2)

        # set plotting parameters
        N_plot = np.max(N)
        spar_plot = 0
        order_plot = 1

        # setup plots for convergence studies
        fig_S  = plt.figure()
        fig_Om = plt.figure()

        # find indices for the given spar and order
        spar_order_int = np.intersect1d(np.where(data[:,2] == spar_plot), \
                        np.where(data[:,3] == order_plot))
        
        # find intersection of spar_order_int and n=N_plot
        k_ind = np.intersect1d(spar_order_int, np.where(data[:,0] == N_plot))
 
        # extract S and Om data for k_ind rows
        S_data_rows = data[k_ind,4:4+dim_sq]
        Om_data_rows = data[k_ind,4+dim_sq:]

        # compute mean of data rows
        #S_data_mean = np.mean(S_data_rows, axis=0)
        #Om_data_mean = np.mean(Om_data_rows, axis=0)

        for i in range(dim_num):
            for j in range(i+1):

                # add subplots
                ax_S = fig_S.add_subplot(dim_num, dim_num, dim_num*i + j + 1)
                ax_Om = fig_Om.add_subplot(dim_num, dim_num, dim_num*i + j + 1)

                ##ax_S.tight_layout()
                ax_S.tick_params(which='both',bottom='off',top='off',left='off',right='off',labelbottom='off',labelleft='off')
                ax_Om.tick_params(which='both',bottom='off',top='off',left='off',right='off',labelbottom='off',labelleft='off')

                # generate histograms
                ax_S.hist(S_data_rows[:,dim_num*i + j], 15, normed=1, facecolor='blue', alpha = 0.5)
                oh = ax_Om.hist(Om_data_rows[:,dim_num*i + j], 15, normed=1, facecolor='blue', alpha = 0.5, label='Data')

                # plot true value
                ax_S.axvline(S_true[i,j], color = 'black', linewidth = 1)
                ov = ax_Om.axvline(Omega_true[i,j], color = 'black', linewidth = 1, label='True value')

                #compute mean and variance of map elements
                true_S = S_true[i,j]
                true_Om = Omega_true[i,j]
                mean_S = np.mean(S_data_rows[:,dim_num*i + j])
                mean_Om = np.mean(Om_data_rows[:,dim_num*i + j])
                var_S = np.var(S_data_rows[:,dim_num*i + j])
                var_Om = np.var(Om_data_rows[:,dim_num*i + j])

                if var_S > 0:
                    dom_S = ax_S.get_xlim()
                    dom_Om = ax_Om.get_xlim()
                    xs = np.linspace(dom_S[0], dom_S[1],40)
                    xo = np.linspace(dom_Om[0], dom_Om[1],40)
                    ax_S.plot(xs,norm.pdf(xs,loc=mean_S,scale=np.sqrt(var_S)))
                    op = ax_Om.plot(xo,norm.pdf(xo,loc=mean_Om,scale=np.sqrt(var_Om)), label='Associated normal')
                    print(mean_S)

                # ax_S.text(.2,.1,'{0:.2f}'.format(true_S)+', '+'{0:.2f}'.format(mean_S)+', '+'{0:.2e}'.format(var_S),
                #         ha='center',va='bottom',transform=ax_S.transAxes,
                #         fontsize=2,
                #         bbox={'facecolor':'lightskyblue','pad':2,'alpha':.8})
                # ax_Om.text(.2,.1,'{0:.2f}'.format(true_Om)+', '+'{0:.2f}'.format(mean_Om)+', '+'{0:.2e}'.format(var_Om),
                #         ha='center',va='bottom',transform=ax_Om.transAxes,
                #         fontsize=2,
                #         bbox={'facecolor':'lightskyblue','pad':2,'alpha':.8})

                # fix axis labels
                #ax_S.set_yticklabels([])
                #ax_Om.set_yticklabels([])

                #ax_S.set_xticklabels([])
                #ax_Om.set_xticklabels([])

                #ax_S.locator_params(axis='x',nbins=2)

        fig_S.suptitle('$S$: '+graph_names[g]+', p = '+str(dim_num))
        fig_Om.suptitle('$\Omega$: '+graph_names[g]+', p = '+str(dim_num))
        # ax_S.legend()
        blue_patch = mpatches.Patch(color='blue',alpha=.5,label='Data')
        black_line = mlines.Line2D([],[],color='black',label='True value')
        fig_Om.legend(handles=[blue_patch,black_line],labels=['Data','True value'])
        
        fig_S.savefig('MC_Plots/S_hist_'+type(graph_obj).__name__+str(dim_num)+'spar'+str(spar_plot)+'CSE.pdf')
        fig_Om.savefig('MC_Plots/Om_hist_'+type(graph_obj).__name__+str(dim_num)+'spar'+str(spar_plot)+'CSE.pdf')

        # -------------------------------------------------------------
        
        if PLOTTING == True:
            plt.show()
