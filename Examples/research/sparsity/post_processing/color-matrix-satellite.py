import matplotlib
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np
from numpy import loadtxt
import matplotlib.patches as mpatches

rc('text',usetex=True)
font={'family' : 'normal',
    'weight' : 'bold',
    'size' :14}
matplotlib.rc('font',**font)

dataFile = "temp/adj-satellite.txt"
A = loadtxt(dataFile,comments="%")
p = 10
A[np.nonzero(A)] = 1
# for i in range(1):
i = 0
adj = A[10*i:10*(i+1),:]
#     print(adj)
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
# ax.imshow(adj,interpolation='nearest', cmap = plt.cm.bwr, alpha=.9, extent=[1,10,1,10])
ax.imshow(A, cmap = plt.cm.YlGnBu, alpha=.9, extent=[1,10,10,1])
ax.text(2,.8,'Orbit')
# ax.text(6,10.2,'Attitude')
ax.text(10.2,8,'Power')
# ax.text(0,11,'Attitude')
# ax.text(11,1,'Power')

ax.annotate('Attitude', xy=(6, 4.5), xytext=(7.5, .8), arrowprops=dict(facecolor='black', shrink=0.08))
# ax.annotate('local max', xy=(2, 1), xytext=(3, 1.5),
#              arrowprops=dict(facecolor='black', shrink=0.05),
#              )


# ax.annotate('CC DD', fontsize=20, xy=(5, 11),
#             xycoords='data', xytext=(150, -6),
#             textcoords='offset points',
#             arrowprops=dict(width = 5.,
#             headwidth = 15.,
#             frac = 0.2,
#             shrink = 0.05,
#             linewidth = 2,
#             color = 'red')
                                                        # )


plt.savefig('matrix-slots-satellite-nips.pdf')#%s%s%s.pdf' %(k1,k2,k3))
#d = [];
#for i in range(0,50):
 #d.extend(data[i,2:])
# A = -A
# red_patch = mpatches.Patch(color='red', label='Negative')
# blue_patch = mpatches.Patch(color='blue', label='Positive')
# white_patch = mpatches.Patch(color='white', ec='black', label='Zero')
# plt.legend(handles=[red_patch,blue_patch,white_patch],bbox_to_anchor=(1.3,1.0))
# plt.show()
