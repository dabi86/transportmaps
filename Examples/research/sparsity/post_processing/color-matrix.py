import matplotlib
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np
from numpy import loadtxt
import matplotlib.patches as mpatches

rc('text',usetex=True)
font={'family' : 'normal',
    'weight' : 'bold',
    'size' :24}
matplotlib.rc('font',**font)

dataName = "gp-sv"
dataFile = "temp/gp-sv.txt"
# dataName = "adj-sv-strong"
# dataFile = "temp/adj-sv-strong.txt"
A = loadtxt(dataFile,comments="%")
maxA = np.max(A)
A = (1/maxA)*A
# A[np.nonzero(A)] = 1
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
# ax.imshow(A,interpolation='nearest', cmap = plt.cm.YlGnBu, alpha=.9, extent=[1,8,8,1])
im = ax.imshow(A, cmap = plt.cm.YlGnBu, alpha=.9)#, extent=[1,8,8,1])
plt.xticks(range(8),('$\mu$','$\phi$','$Z_1$','$Z_2$','$Z_3$','$Z_4$','$Z_5$','$Z_6$'))
plt.yticks(range(8),('$\mu$','$\phi$','$Z_1$','$Z_2$','$Z_3$','$Z_4$','$Z_5$','$Z_6$'))

fig.colorbar(im)

# ax.text(2,10.2,'Orbit')
# ax.text(10.2,2,'Power')
# ax.annotate('Attitude', xy=(6, 6.5), xytext=(7.5, 10.2), arrowprops=dict(facecolor='black', shrink=0.08))

plt.savefig('temp/matrix-slots-'+dataName+'-nips.pdf')#%s%s%s.pdf' %(k1,k2,k3))
