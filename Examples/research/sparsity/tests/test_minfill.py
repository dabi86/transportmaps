import numpy as np
import GaussianGraphs as gg
import NodeOrdering as no

dim = 9
a = gg.ChainGraph(dim).omega()
random_perm = np.random.permutation(dim)
a0 = a[:,random_perm][random_perm,:]
# inverse of random perm
irp = [0] * dim
for i, p in enumerate(random_perm):
    irp[p] = i
print("irp = ", irp)

perm = no.min_fill(a)

a1= a0[:,perm][perm,:]

# inverse perm
ip = [0] * dim
for i, p in enumerate(perm):
    ip[p] = i
print("ip = ", ip)

a2 = a1[:,ip][ip,:]
a3 = a2[:,irp][irp,:]
# a = a[:,inverse_perm][inverse_perm,:]
print(a, a3)
