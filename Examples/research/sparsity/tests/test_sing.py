#!/usr/bin/env/python

#
# This file runs the algorithm many times and computes the number of type 1 and type 2 errors made.
#
import sys, getopt
import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import TransportMaps.Algorithms.SparsityIdentification as SI
import TransportMaps.Distributions.FrozenDistributions as FD

def run_sing(GRAPH, DIM, NSAMPS, ORDER, ORDERING, DELTA):
    # # open file
    # graph_str = type(graph_obj).__name__ + str(graph_obj.dim)
    # file_name = 'MC_Alg/data_mc_alg_errors_' + graph_str + '.txt'
    # file = open(file_name,'a')
    dim = int(DIM)
    if GRAPH == '0':
        graph = FD.ChainGraphGaussianDistribution(dim)
    elif GRAPH == '1':
        graph = FD.StarGraphGaussianDistribution(dim)
    elif GRAPH == '2':
        graph = FD.GridGraphGaussianDistribution(dim)
    elif GRAPH == '3':
        graph = FD.RelaxedRademacherDistribution(dim)
    else:
        print('Graph not implemented.')

    orderings = [SI.ReverseCholesky(), SI.MinFill(), SI.MinDegree()]
    ordering = orderings[int(ORDERING)]
    # create the data, and true omega
    # returns perfect ordering
    nsamps = int(NSAMPS)
    data0 = graph.rvs(nsamps)
    graph0 = graph.graph
    delta = float(DELTA)

    recovered_gp = SI.SING(data0, int(ORDER), ordering, delta)
    # count errors made by algorithm
    error1 = 0 #not as bad: component is zero, but alg finds it non-zero
    error2 = 0 #bad: component is non-zero, but alg sets it zero
    for ii in range(dim):
        for jj in range(ii+1):
            if graph0[ii,jj] == 0 and recovered_gp[ii,jj] != 0:
                error1 += 1
            elif graph0[ii,jj] != 0 and recovered_gp[ii,jj] == 0:
                error2 += 1
    print('Error type 1 = ', error1)
    print('Error type 2 = ', error2)
                    # print results in file
                    # file.write(str(nsamps) + ' ' + str(j) + ' ' + str(order) + ' ' + str(i_ordering) + ' ' + str(error1) + ' ' + str(error2) +'\n')
    # file.close()

def usage():
    print('run_algorithm.py -g <graph> -d <dim> -n <nsamps> -o <order> -r <ordering> -l <delta>')

def main(argv):
    # defaults
    GRAPH = None
    DIM = 2
    NSAMPS = 100
    ORDER = 1
    ORDERING = 1
    DELTA = 2
    try:
        opts, args = getopt.getopt(argv, 'hg:d:n:o:r:l:', ['help','graph','dim','nsamps','order','ordering','delta'])
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            usage()
            sys.exit()
        elif opt in ('-g', '--graph'):
            GRAPH = arg
        elif opt in ('-d', '--dim'):
            DIM = arg
        elif opt in ('-n', '--nsamps'):
            NSAMPS = arg
        elif opt in ('-o', '--order'):
            ORDER = arg
        elif opt in ('-r', '--ordering'):
            ORDERING = arg
        elif opt in ('-l', '--delta'):
            DELTA = arg

    run_sing(GRAPH, DIM, NSAMPS, ORDER, ORDERING, DELTA)

if __name__ == '__main__':
    main(sys.argv[1:])
