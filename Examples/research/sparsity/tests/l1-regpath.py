#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2016 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Daniele Bigoni
# E-mail: dabi@limitcycle.it
#

# import matplotlib
import matplotlib.pyplot as plt
# from matplotlib import rc
import numpy as np
import h5py

import TransportMaps as TM
import TransportMaps.Distributions as DIST

import ExtraDensities
import GaussianGraphs as gg

# rc('text',usetex=True)
# font={'family' : 'normal',
#     'weight' : 'normal',
#     'size' :14}
# matplotlib.rc('font',**font)

# TM.setLogLevel(10)
nax = np.newaxis

#weighting type: 0 = no reg, 1 = l1 reg, 2 = reweighted l1
WEIGHT = 2
nsamps = 1000
dim = 4

testn = 1
if testn == 1:
    slicedata = 3
    f = h5py.File('/home/rmorriso/Dropbox/Sparsity/StochasticVolatility/SVH-n10-sigma025-sequential-postprocess.dill.hdf5','r')
    samples = f['quadrature']['0'][:,:]
    rawdata = samples[slicedata*nsamps:(slicedata+1)*nsamps,2:dim+2]
    # Scale the data
    mean_vec = np.mean(rawdata,axis=0)
    data = rawdata - mean_vec
    inv_var = 1./(np.var(data,axis=0))
    inv_std = np.diag(np.sqrt(inv_var))
    data = np.dot(data,inv_std)
    
    rawdata = samples[slicedata*20000:(slicedata+1)*20000,2:dim+2]
    mean_vec = np.mean(rawdata,axis=0)
    data2 = rawdata - mean_vec
    inv_var = 1./(np.var(data2,axis=0))
    inv_std = np.diag(np.sqrt(inv_var))
    data_test = np.dot(data2,inv_std)

elif testn == 2:
    graph_obj = gg.ChainGraph(dim)
    # graph_str = type(graph_obj).__name__ + str(graph_obj.dim)
    # file_name = 'MC_Runs/data_mc_sparsity_' + graph_str + '.txt'
    # file = open(file_name,'a')
    data = graph_obj.rvs(nsamps)
    data_test = graph_obj.rvs(20000)#nsamps)

eta = DIST.StandardNormalDistribution(dim)

# Gradient information
# 0: derivative free
# 1: gradient information
# 2: Hessian information
ders = 1

# Approximation orders
#orders = [1,3,5]
order = 2
# Tolerance
tol = 1e-6
tol_l1 = 1e-6

threshold = 1e-6

# Create list to store coeffs and variance
coeff_l1 = []
map_var  = []
coeff_nnz  = []
log_lik = []
bic = []
aic = []
obj_val = []
# params for reweighted l1 optimization
max_iter = 10
tol_soln = 1e-6
eps = 1e-3

def reweighted_l1(eta, qtype, qparams, REG, tol, ders, max_iter, tol_soln, eps, n_coeffs, x0):
    err_soln = 1
    iter_count = 0

    # Compute initial estimate for map and get weights
    log = tm_density.minimize_kl_divergence(eta, qtype=qtype, qparams=qparams, \
            regularization=REG, tol=tol, ders=ders, x0=x0)
    coeffs = tm_density.get_coeffs()

    while(err_soln > tol_soln and iter_count < max_iter):

        print('Iter: ', str(iter_count), ', Err: ', str(err_soln), ' Spar: ', str(np.count_nonzero(coeffs)))
        print(str((np.abs(coeffs) < 1e-6).sum()))
        # Update weights based on coeffs
        weights = 1./(np.abs(coeffs) + eps)
        REG['l1_weights'] = weights

        #print('\n')
        #print("coeffs: ", coeffs)
        #print("weights: ", weights)
        #print('\n')

        # Compute new map and update weights
        log = tm_density.minimize_kl_divergence(eta, qtype=qtype, \
            qparams=qparams, regularization=REG, tol=tol, ders=ders)
        new_coeffs = tm_density.get_coeffs()
        #print(new_coeffs - coeffs)

        # Compute err_soln
        err_soln = np.linalg.norm(new_coeffs - coeffs)
        coeffs = new_coeffs

        # Update counter
        iter_count = iter_count + 1

    return new_coeffs, REG, log

first_iter = 1
# loop for lambda
lambda_list = np.logspace(-6,0,20)
for lambda_t in lambda_list:
    print(lambda_t)

    # --------------------- RUN L1 REGULARIZATION ---------------------------
    
    # L2 error estimation
    # 3: Gauss quadrature of order n
    # 0: Monte Carlo quadrature with n point
    qtype = 0
    qparams = nsamps

    # define base_density from samples
    pi = ExtraDensities.SamplesDensity(data)

    # Build the transport map (isotropic for each entry)
    tm_approx = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
              dim, order, active_vars=None)
    
    n_coeffs = tm_approx.get_n_coeffs()
    # Construct density T_\sharp \pi
    tm_density = DIST.PushForwardTransportMapDistribution(tm_approx, pi)

    # SOLVE
    if WEIGHT == 1:
        # Set regularization in REG
        REG = {'type': 'L1', 'alpha': lambda_t, 'tol_l1': tol_l1, 'l1_weights': np.ones(n_coeffs)}
        # x0 = None
        if first_iter == 1:
            x0 = None
        else:
            x0 = coeff_iter
        log_entry_solve = tm_density.minimize_kl_divergence(eta, qtype=qtype,
                                                         qparams=qparams,
                                                         regularization=REG,
                                                         tol=tol, ders=ders, x0=x0)
        coeff_iter = tm_density.get_coeffs()
    elif WEIGHT == 2:
        # Set regularization in REG
        REG = {'type': 'L1', 'alpha': lambda_t, 'tol_l1': tol_l1, 'l1_weights': np.ones(n_coeffs)}
        x0 = None
        #if first_iter == 1:
        #    x0 = None
        #else:
        #    x0 = coeff_iter
        coeff_iter, REG, log_entry_solve = reweighted_l1(eta, qtype, qparams, REG, tol, ders, max_iter, tol_soln, eps, n_coeffs, x0)

    else:
        # Set regularization in REG
        REG = {'type': None }
        x0 = None
        log_entry_solve = tm_density.minimize_kl_divergence(eta, qtype=qtype,
                                                         qparams=qparams,
                                                         regularization=REG,
                                                         tol=tol, ders=ders, x0=x0)
        coeff_iter = tm_density.get_coeffs()

    first_iter = 0
    # Save vectors of coeffs
    print("map coeffs = ",coeff_iter)
    coeff_iter[abs(coeff_iter) < threshold] = 0.0#tol_l1] = 0.0
    print("map coeffs = ",coeff_iter)
    #print("density coeffs = ",coeff_iter)
    #print("map coeffs = ",coeff_iter2)
    coeff_l1.append(coeff_iter)
    coeff_nnz.append(np.count_nonzero(coeff_iter))
    print("number of nonzero coeffs = ",coeff_nnz[-1])

    # Evaluate samples under T_\sharp \pi and compute log likelihood
    pull_S = DIST.PullBackTransportMapDistribution(tm_approx, eta)
    log_lik_test = np.sum(pull_S.log_pdf(data_test))
    log_lik.append(log_lik_test)

    # Evaluate log-likelihood for data used to create map
    log_lik_trial = np.sum(pull_S.log_pdf(data))

    # compute bic and aic
    bic.append(np.log(nsamps)*np.count_nonzero(coeff_iter) - 2*log_lik_trial)
    aic.append(2*np.count_nonzero(coeff_iter) - 2*log_lik_trial)

    # compute kl value
    obj_val.append(log_entry_solve['fval'] - lambda_t*np.sum(REG['l1_weights']*coeff_iter))

    #eta_tilde_samps = tm_density_l1.map_samples_base_to_target( data_test )
    #log_lik_test = -dim*20000/2.*np.log(2*np.pi) -0.5*np.sum(np.sum(np.abs(eta_tilde_samps)**2,axis=-1))
    #log_lik.append(log_lik_test)

    # Compute variance in approximation
    # pull_S = DIST.PullBackTransportMapDistribution(tm_approx, pi['target_density'])
    # map_var.append(DIAG.variance_approx_kl(pull_S, target_den, qtype=qtype, qparams=qparams))

    # fig = DIAG.plotAlignedConditionals(tm_density_l1, range_vec=[-5,5],
    #         numPointsXax=20, fig=plt.figure(figsize=(6.5,6.5)), show_flag=False)
    # plt.show()

   # # EVALUATE MAP ON IDENTITY TO RECOVER MATRIX
   #  S = (tm_approx.evaluate(np.identity(dim))).T - \
   #          tm_approx.evaluate(np.zeros((1,dim))).T
   #  meanS = tm_approx.evaluate(np.zeros((1,dim))).T
   #  # GP = np.dot(S.T,S)
   #  plt.figure()
   #  # plt.imshow(np.log10(np.abs(S)))
   #  plt.imshow(np.abs(S))
   #  # axsm = figs.add_subplot(1,2,2)
   #  # axsm.imshow(meanS)
   #  print("mean of S = ",meanS)
   #  plt.colorbar()
   #  plt.show()

#****************Solve for map with correct active variables*****************************
#active_vars = graph_obj.active_vars()
#map_av = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
#              dim, order, active_vars=active_vars)
#density_av = DIST.PushForwardTransportMapDistribution(map_av, pi)
# SOLVE
#log_entry_solve = density_av.minimize_kl_divergence(eta, qtype=qtype,
#                                                    qparams=qparams,
#                                                    regularization=None,
#                                                    tol=tol, ders=ders)
# Evaluate samples under T_\sharp \pi and compute log likelihood
#eta_tilde_samps_av = density_av.map_samples_base_to_target( data_test )
#log_lik_av = -dim*20000/2.*np.log(2*np.pi) -0.5*np.sum(np.sum(np.abs(eta_tilde_samps_av)**2,axis=-1))

#**********************Get samples from true map*****************************
#gp = graph_obj.omega()
#K = np.linalg.cholesky(gp)
#samps_true_map = (np.dot(K.T,data_test.T)).T
#log_lik_true = -dim*20000/2.*np.log(2*np.pi) -0.5*np.sum(np.sum(np.abs(samps_true_map)**2,axis=-1))

# convert list to array
#coeff_l1 = np.array(coeff_l1)
#coeff_nnz = np.array(coeff_nnz)
#log_lik = np.array(log_lik)

# plot absolute value of coeffs vs lambda
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.semilogx(lambda_list, coeff_l1,marker='o')
ax.set_title('Coefficients vs $\lambda$')
ax.set_xlabel('$\lambda$')
ax.set_ylabel('$| \alpha_{i}|$')
#fig.savefig('coeff-values-l1.pdf')

# plot absolute value of coeffs vs lambda
figz = plt.figure()
axz = figz.add_subplot(1,1,1)
axt = axz.twinx()
axz.semilogx(lambda_list, coeff_nnz,marker='o',color='b')
axz.semilogx(lambda_list, obj_val, marker='*',color='k')
axt.semilogx(lambda_list, bic, marker='o',color='r')
axt.semilogx(lambda_list, aic, marker='*',color='g')
axt.set_ylabel('$BIC/AIC$')
axz.set_title('Number of non-zero coefficients vs $\lambda$')
axz.set_xlabel('$\lambda$')
axz.set_ylabel('$||\boldsymbol{\alpha}||_0$')
#figz.savefig('coeff-vs-lambda-l1.pdf')

# plot error vs lambda
#figl = plt.figure()
#axl = figl.add_subplot(1,1,1)
#axl.loglog(lambda_list, -1*log_lik, linewidth=2, marker='*')
#axl.set_title('Log-likelihood vs $\lambda$')
#axl.set_xlabel('$\lambda$')
#axl.set_ylabel('Log-likelihood')
#axl.axhline(log_lik_av, linewidth=2, color='r')
#axl.axhline(log_lik_true, linewidth=2, color='c')

# plot error vs lambda
# figv = plt.figure()
# axv = figv.add_subplot(1,1,1)
# axv.loglog(lambda_list, map_var, linewidth=2, marker='*')
# axv.set_title('Variance vs $\lambda$')
# axv.set_xlabel('$\lambda$')
# axv.set_ylabel('Variance')

plt.show()
