#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2016 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Daniele Bigoni
# E-mail: dabi@limitcycle.it
#

import warnings
import time
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from tabulate import tabulate
from scikits.sparse.cholmod import cholesky
from scipy.sparse import csc_matrix
from scipy.sparse import csr_matrix

import SpectralToolbox.Spectral1D as S1D

import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.tests.TestFunctions as TF

import ExtraDensities

nax = np.newaxis

#tests available:
#1: laplace distribution
#2: cauchy distribution
#3: gumbel (right skew) distribution
#4: mixed gaussian
#5: gaussian
#6: logistic (heavy tails)
testn = 1
N_samp = 1000
if testn == 1:
    target_den = ExtraDensities.Laplace(loc=0,scale=1)
elif testn == 2:
    target_den = ExtraDensities.Cauchy(loc=0,scale=1)
elif testn == 3:
    gumscale = 1.5
    target_den = ExtraDensities.GumbelR(scale=gumscale)
elif testn == 4:
    m1 = 0; s1 = 1;
    m2 = 5; s2 = 1;
    target_den = ExtraDensities.MixedNormal(mu1=m1, mu2 = m2, sigma1 = s1,
            sigma2 = s2)
elif testn == 5:
    mu = 0; sigma = 3;
    data = stats.norm.rvs(mu, sigma, size=N_samp)
elif testn == 6:
    target_den = ExtraDensities.Logistic()

data = target_den.rvs(N_samp)
if data.ndim == 1:
    data = data[:,nax]

dim = 1

pi = {}
#going from target (samples) to standard normal
#pi['base_density'] = ExtraDensities.SamplesDensity(data)
pi['target_density'] = DENS.StandardNormalDensity(dim)
#will keep support map but shouldn't need it in this example
pi['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.zeros(dim),
                                                          np.ones(dim))

# Gradient information
# 0: derivative free
# 1: gradient information
# 2: Hessian information
ders = 2

# Approximation orders
#orders = [1,3,5]
order = 10
# Tolerance
tol = 1e-6
tol_l1 = 1e-6#1e-15#1e-4

# Target density to be approximated L^\sharp \pi_{\rm tar}
target_density = DENS.PullBackTransportMapDensity(pi['support_map'],
                                                  pi['target_density'])

# open file
#graph_str = type(graph_obj).__name__ + str(graph_obj.dim)
#file_name = 'MC_Runs/data_mc_sparsity_' + graph_str + '.txt'
#file = open(file_name,'a')

# loop for number 
lambda_list = [2e-1]
for lambda_t in lambda_list:

    # L2 error estimation
    # 3: Gauss quadrature of order n
    # 0: Monte Carlo quadrature with n point
    qtype = 0
    qparams = N_samp

    # define base_density from samples
    pi['base_density'] = ExtraDensities.SamplesDensity(data)

    # --------------------- RUN L1 REGULARIZATION ---------------------------

    # Build the transport map (isotropic for each entry)
    tm_approx = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
              dim, order, active_vars=None)
    
    n_coeffs = tm_approx.get_n_coeffs()
    # Set regularization in REG
    #REG = {'type': 'L1', 'alpha': lambda_t}
    # REG = {'type': 'L1', 'alpha': lambda_t, 'tol_l1': tol}
    REG = {'type': 'L1', 'alpha': lambda_t, 'tol_l1': tol_l1, 'l1_weights': np.ones(n_coeffs)}

    # Construct density T_\sharp \pi
    tm_density_l1 = DENS.PushForwardTransportMapDensity(tm_approx, pi['base_density'])

    # SOLVE
    log_entry_solve = tm_density_l1.minimize_kl_divergence(target_density, qtype=qtype,
                                                        qparams=qparams,
                                                        regularization=REG,
                                                        tol=tol, ders=ders)

    # --------------------- RUN NO REGULARIZATION ---------------------------

    # Set regularization to None
    REG = None

    # Build the transport map (isotropic for each entry)
    tm_approx = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
              dim, order, active_vars=None)
    
    # Construct density T_\sharp \pi
    tm_density_nreg = DENS.PushForwardTransportMapDensity(tm_approx, pi['base_density'])

    # SOLVE
    log_entry_solve = tm_density_nreg.minimize_kl_divergence(target_density, qtype=qtype,
                                                        qparams=qparams,
                                                        regularization=REG,
                                                        tol=tol, ders=ders)

# Get vectors of coeffs
coeff_l1   = tm_density_l1.get_coeffs()
coeff_l1[abs(coeff_l1) < tol] = 0.0
coeff_nreg = tm_density_nreg.get_coeffs()

fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.semilogy(np.arange(len(coeff_l1)), np.abs(coeff_l1), color='r', linestyle='',marker='o',label='L1')
ax.semilogy(np.arange(len(coeff_l1)), np.abs(coeff_nreg), color='b', linestyle='',marker='*',label='NO REG')
ax.set_title('L1 vs. NO regularization')
ax.set_xlabel('Coefficients')
ax.set_ylabel('$|\alpha_{i}|$')
ax.legend(title = 'Reg.',loc=3)

plt.show()
