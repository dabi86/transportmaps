#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2016 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Daniele Bigoni
# E-mail: dabi@limitcycle.it
#

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

import ExtraDensities

nax = np.newaxis

#tests available:
#1: laplace distribution
#2: cauchy distribution
#3: gumbel (right skew) distribution
#4: mixed gaussian
#5: gaussian
#6: logistic (heavy tails)
testn = 3
N_samp = 1000
if testn == 1:
    target_den = ExtraDensities.Laplace(loc=0,scale=1)
elif testn == 2:
    target_den = ExtraDensities.Cauchy(loc=0,scale=1)
elif testn == 3:
    gumscale = 1.5
    target_den = ExtraDensities.GumbelR(scale=gumscale)
elif testn == 4:
    m1 = -3; s1 = 1;
    m2 = 2; s2 = 1;
    target_den = ExtraDensities.MixedNormal(mu1=m1, mu2 = m2, sigma1 = s1,
            sigma2 = s2)
elif testn == 5:
    mu = 0; sigma = 3;
    data = stats.norm.rvs(mu, sigma, size=N_samp)
elif testn == 6:
    target_den = ExtraDensities.Logistic()

data = target_den.rvs(N_samp)
if data.ndim == 1:
    data = data[:,nax]

dim = 1

pi = {}
#going from target (samples) to standard normal
#pi['base_density'] = ExtraDensities.SamplesDensity(data)
pi['target_density'] = DIST.StandardNormalDistribution(dim)
#will keep support map but shouldn't need it in this example
pi['support_map'] = Maps.FrozenLinearDiagonalTransportMap(np.zeros(dim),
                                                          np.ones(dim))

# Gradient information
# 0: derivative free
# 1: gradient information
# 2: Hessian information
ders = 2

# Approximation orders
#orders = [1,3,5]
order = 9
# Tolerance
tol = 1e-5

# Target density to be approximated L^\sharp \pi_{\rm tar}
target_density = DIST.PullBackTransportMapDistribution(pi['support_map'],
                                                  pi['target_density'])

# Create list to store coeffs and variance
coeff_l1 = []
map_var  = []

# Setup figure for plotting densities
fig_kde_1d = plt.figure()
ax_kde_1d = fig_kde_1d.add_subplot(1,1,1)
ax_kde_1d.set_title('Densities with changing $\lambda$')
ax_kde_1d.set_xlabel('$x$')
ax_kde_1d.set_ylabel('$S_{\sharp}\eta$')
#ax_kde_1d.legend()

# Evaluate and plot target den
X1d_kde = np.linspace(-10,10,100).reshape((100,1))
pdf1d = target_den.pdf(X1d_kde)
ax_kde_1d.plot(X1d_kde, pdf1d, label='$\pi$')

# loop for lambda
lambda_list = np.logspace(-5,2,10)
for lambda_t in lambda_list:
    print(lambda_t)

    # --------------------- RUN L1 REGULARIZATION ---------------------------
    
    # L2 error estimation
    # 3: Gauss quadrature of order n
    # 0: Monte Carlo quadrature with n point
    qtype = 0
    qparams = N_samp

    # define base_density from samples
    pi['base_density'] = ExtraDensities.SamplesDensity(data)

    # Set regularization in REG
    REG = {'type': 'L1', 'alpha': lambda_t, 'tol_l1': tol}

    # Build the transport map (isotropic for each entry)
    tm_approx = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
              dim, order, active_vars=None)
    
    # Construct density T_\sharp \pi
    tm_density_l1 = DIST.PushForwardTransportMapDistribution(tm_approx, pi['base_density'])

    # SOLVE
    log_entry_solve = tm_density_l1.minimize_kl_divergence(target_density, qtype=qtype,
                                                        qparams=qparams,
                                                        regularization=REG,
                                                        tol=tol, ders=ders)

    # Save vectors of coeffs
    coeff_iter = tm_density_l1.get_coeffs()
    coeff_iter[abs(coeff_iter) < tol] = 0.0
    coeff_l1.append(coeff_iter)

    # Compute variance in approximation
    # pull_S = DIST.PullBackTransportMapDistribution(tm_density_l1, pi['target_density'])
    pull_S = DIST.PullBackTransportMapDistribution(tm_approx, pi['target_density'])
    map_var.append(DIAG.variance_approx_kl(pull_S, target_den, qtype=qtype, qparams=qparams))

    # Evaluate mapped density
    pdf1d = pull_S.pdf(X1d_kde)
    ax_kde_1d.plot(X1d_kde, pdf1d, label='$\lambda$ = '+str(lambda_t))
    

# convert list to array
coeff_l1 = np.array(coeff_l1)

# plot absolute value of coeffs vs lambda
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.semilogx(lambda_list, coeff_l1,marker='o')
ax.set_title('Coefficients vs $\lambda$')
ax.set_xlabel('$\lambda$')
ax.set_ylabel('$| \alpha_{i}|$')

ax_kde_1d.legend()

# plot error vs lambda
figv = plt.figure()
axv = figv.add_subplot(1,1,1)
axv.loglog(lambda_list, map_var, linewidth=2, marker='*')
axv.set_title('Variance vs $\lambda$')
axv.set_xlabel('$\lambda$')
axv.set_ylabel('Variance')

plt.show()
