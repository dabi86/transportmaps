clear; close all; clc

% define parameters
d = 5;
Sigma = eye(d);
a = 1;
b = 1;

% define map for each variable
T = @(x) banana_pushforward(x,a,b);

% generate samples
N  = 100000;
x  = mvnrnd(zeros(1,d),Sigma,N);
Tx = T(x);

% plot samples
figure;
plot3(Tx(:,1), Tx(:,2), Tx(:,3), '.b');
xlabel('$x_{1}$')
ylabel('$x_{2}$')
zlabel('$x_{3}$')

figure;
plot3(Tx(:,2), Tx(:,3), Tx(:,4), '.b');
xlabel('$x_{2}$')
ylabel('$x_{3}$')
zlabel('$x_{4}$')

figure
for i=1:5; 
	subplot(2,3,i); 
	histogram(Tx(:,i))
	xlabel(['$x_{' num2str(i) '}$']) 
end

% save data
data = Tx;
save('banana','data')

