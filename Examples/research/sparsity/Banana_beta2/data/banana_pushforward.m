function T = banana_pushforward(X, a, b)

% determine d
[N,d] = size(X);
T = zeros(N,d);

% evaluate map T(X)
T(:,1) = a*X(:,1);
for i=2:d
    T(:,i) = -b*((a*X(:,1)).^2 + a^2) + X(:,i)/a;
end

end
