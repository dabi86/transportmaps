#
# This file generates nonparanormal data
#
import warnings
import time
import matplotlib
# matplotlib.use("pdf")
import matplotlib.pyplot as plt
import numpy as np
import scipy.io as sio
from scipy.stats import norm
from scipy.stats import multivariate_normal
import scipy.integrate as integrate

# set number of variables
p = 5
g_type = 1
DATAFILE = 'banana.mat'

# define transformation functions
if g_type == 0:
    # power transformation
    def g_0(t,alpha_j):
        return np.sign(t)*(np.abs(t)**alpha_j)
    def integrand(t, mu_j, sigma_j, alpha_j):
        A = (g_0(t-mu_j, alpha_j))**2
        B = norm.pdf(t, loc=mu_j, scale=sigma_j)
        return A*B
    def g_j(z_j, mu_j, sigma_j, alpha_j):
        A, errA = integrate.quad(integrand, -np.inf, np.inf, args=(mu_j, sigma_j, alpha_j))
        return sigma_j * g_0(z_j - mu_j, alpha_j) / np.sqrt(A) + mu_j

elif g_type == 1:
    # gaussian CDF transformation
    def g_0(t, mu_g0, sigma_g0):
        alpha = 0#-1
        beta = 1
        return (beta - alpha)*norm.cdf(t, loc=mu_g0, scale=sigma_g0) + alpha #map from alpha to beta
    def integrand1(t, mu_g0, sigma_g0, mu_j, sigma_j):
        return norm.cdf(t, loc=mu_g0, scale=sigma_g0) * norm.pdf(t, loc=mu_j, scale=sigma_j)
    def integrand2(y, mu_g0, sigma_g0, mu_j, sigma_j):
        B, errB = integrate.quad(integrand1, -np.inf, +np.inf, args=(mu_g0, sigma_g0, mu_j, sigma_j))
        return (norm.cdf(y, mu_g0, sigma_g0) - B)**2 * norm.pdf(y, mu_j, sigma_j)
    def g_j(z_j, mu_g0, sigma_g0, mu_j, sigma_j):
        A = g_0(z_j, mu_g0, sigma_g0)
        B, errB = integrate.quad(integrand1, -np.inf, +np.inf, args=(mu_g0, sigma_g0, mu_j, sigma_j))
        C, errC = integrate.quad(integrand2, -np.inf, +np.inf, args=(mu_g0, sigma_g0, mu_j, sigma_j))
        return sigma_j * (A - B) / np.sqrt(C) + mu_j

# load data from file
all_data = sio.loadmat(DATAFILE)
data = all_data['data']
data = data[:20000,:]
n = data.shape[0]
print(data.shape)
#norm_data = np.random.multivariate_normal(mu_0, Sigma_0, n)
Sigma_0 = np.cov(data.T)
mu_0 = np.zeros((p,))#(1,p))
print(Sigma_0.shape)
npn_data = np.zeros((n,p))

if g_type == 0:
    alpha = np.array([2,3,4,3,2,1,3,3,2,4]) # *************************NEEDS TO BE GENERALIZED TO P DIM*************************
    for j in range(p):
        mu_j = mu_0[j]
        sigma_j = np.sqrt(Sigma_0[j][j])
        alpha_j = alpha[j]
        npn_data[:,j] = g_j(data[:,j], mu_j, sigma_j, alpha_j)
    #np.savetxt('power_omega.txt', omega_0, fmt='%1.3f')
    np.savetxt('power_data.txt', npn_data, fmt='%1.3f')

elif g_type == 1:
    mu_g0 = 0.05
    sigma_g0 = .4
    for j in range(p):
        mu_j = mu_0[j]
        sigma_j = np.sqrt(Sigma_0[j][j])
        npn_data[:,j] = g_j(data[:,j], mu_g0, sigma_g0, mu_j, sigma_j)
    #np.savetxt('cdf_omega.txt', omega_0, fmt='%1.3f')
    np.savetxt('cdf_data.txt', npn_data, fmt='%1.3f')

# plt.hist(npn_data[:,0],bins=100,normed=True)
# plt.hist(npn_data[:,1],bins=100,normed=True)
# plt.show()

# filename=open('copula_data.txt')
# filename.close()

