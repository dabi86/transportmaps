import matplotlib.pyplot as plt

import numpy as np
import numpy.random as npr
import scipy.stats as stats

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import src.TransportMaps.Maps as MAPS
import TransportMaps.Likelihoods as LKL
import TransportMaps.Distributions.Inference as DISTINF
import TransportMaps.Diagnostics as DIAG
import TransportMaps.FiniteDifference as FD

from aux import \
    GammaDistribution, GammaMixtureDistribution, \
    VerticalMeasurementMap, HorizontalMeasurementMap

NPROCS = 3

TM.setLogLevel(20)

npr.seed(1)

p = .2
k1 = 2.5
l1 = 0.25
t1 = .5
k2 = 14.
l2 = 0.25
t2 = .25
pi_HI1 = GammaMixtureDistribution(1., k1, l1, t1, k2, l2, t2)
pi_HI2 = GammaMixtureDistribution(0., k1, l1, t1, k2, l2, t2)
pi_HI = GammaMixtureDistribution(p, k1, l1, t1, k2, l2, t2)

# Start a pool of processes
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)
else:
    mpi_pool = None

try:

    x = np.linspace(0, 7, 100).reshape((100,1))
    plt.figure()
    plt.plot(x, p * pi_HI1.pdf(x), 'k')
    plt.plot(x, (1-p) * pi_HI2.pdf(x), 'b')
    plt.plot(x, pi_HI.pdf(x), 'r')
    plt.show(False)

    ######
    # Create map from samples T for ice thickness distribution
    # 1) Generate samples (on the real line)
    N = 5000
    E = Maps.FrozenExponentialDiagonalTransportMap(1)
    pull_E_pi = DIST.PullBackTransportMapDistribution(E, pi_HI)
    xx, ww = pull_E_pi.quadrature(0, N)
    # 2) Center and rescale
    xmean = np.mean(xx)
    xstd = np.std(xx)
    L = Maps.FrozenLinearDiagonalTransportMap(
        np.array([xmean]), np.array([xstd]) )
    xL = L.inverse(xx)
    EL = Maps.CompositeMap(E, L)
    pull_EL_pi = DIST.PullBackTransportMapDistribution(EL, pi_HI)
    # 3) Learning: density estimation / map from samples
    T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
        1, 9, btype='poly')
    push_T_pull_EL_pi = DIST.PushForwardTransportMapDistribution(T, pull_EL_pi)
    rho1d = DIST.StandardNormalDistribution(1)
    push_T_pull_EL_pi.minimize_kl_divergence(
        rho1d, x=xL, w=ww,
        regularization={'type':'L2', 'alpha':5e-2}
    )
    # 4) Assemble approximation
    pull_T_rho1d = DIST.PullBackTransportMapDistribution(T, rho1d)
    push_L_pull_T_rho1d = DIST.PushForwardTransportMapDistribution(L, pull_T_rho1d)
    push_EL_pull_T_rho1d = DIST.PushForwardTransportMapDistribution(
        E, push_L_pull_T_rho1d)

    # Invert map T (through minimization of kl)
    Tm1 = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
        1, 17, btype='poly')
    push_Tm1_rho1d = DIST.PushForwardTransportMapDistribution(Tm1, rho1d)
    log = push_Tm1_rho1d.minimize_kl_divergence(
        pull_T_rho1d,
        # qtype=3, qparams=[30],
        qtype=0, qparams=100000,
        regularization={'type':'L2', 'alpha':1e-3}
    )
    
    # # Invert map T (through regression)
    # Tm1 = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
    #     1, 50, btype='fun')
    # xr = np.linspace(-15,15,3000)[:,np.newaxis]
    # wr = np.ones(len(xr))/len(xr)
    # scatter_tuple = (['x'], [xr])
    # Tinv_xr = TM.mpi_map(
    #     'inverse', obj=T, scatter_tuple=scatter_tuple,
    #     mpi_pool=mpi_pool)
    # log = Tm1.regression(
    #     Tinv_xr, x=xr, w=wr, maxit=5000, tol=1e-6,
    #     # regularization={'type': 'L2', 'alpha': 5e-3}
    # )

    push_Tm1_rho1d = DIST.PushForwardTransportMapDistribution(Tm1, rho1d)
    push_LTm1_rho1d = DIST.PushForwardTransportMapDistribution(L, push_Tm1_rho1d)
    push_ELTm1_rho1d = DIST.PushForwardTransportMapDistribution(E, push_LTm1_rho1d)

    xQ = np.linspace(-20, 20, 100)[:,np.newaxis]
    Tinv_xQ = TM.mpi_map(
        'inverse', obj=T, scatter_tuple=(['x'],[xQ]),
        mpi_pool=mpi_pool)
    Tm1_xQ = Tm1.evaluate(xQ)
    plt.figure()
    plt.plot(xQ, Tinv_xQ, 'r')
    plt.plot(xQ, Tm1_xQ, 'k')
    plt.show(False)

    pdf = pi_HI.pdf(x)
    approx_pdf_pullT = push_EL_pull_T_rho1d.pdf(x)
    approx_pdf_pushQ = TM.mpi_map(
        'pdf', obj=push_ELTm1_rho1d,
        scatter_tuple=(['x'],[x]),
        mpi_pool=mpi_pool)
    plt.figure()
    plt.plot(x, pdf, 'r')
    plt.plot(x, approx_pdf_pullT, 'k', label=r'$T^\sharp$')
    plt.plot(x, approx_pdf_pushQ, 'b', label=r'$Q_\sharp$')
    plt.legend()
    plt.show(False)

    # LINK = MAPS.ListCompositeMap([E, L, Tm1])
    # push_LINK_rho1d = DIST.PushForwardTransportMapDistribution(LINK, rho1d)
    
    # approx_pi_HI = push_LINK_rho1d
    # pi_sigma_w = GammaDistribution(5000, 0., 0.001)
    # flag = FD.check_grad_x(
    #     pi_sigma_w.log_pdf, pi_sigma_w.grad_x_log_pdf, pi_sigma_w.rvs(10), 1e-4)
    # flag = FD.check_hess_x_from_grad_x(
    #     pi_sigma_w.grad_x_log_pdf, pi_sigma_w.hess_x_log_pdf, pi_sigma_w.rvs(10), 1e-4)
    # # pi_sigma_w = DIST.GaussianDistribution( np.array([5]), np.array([[0.025]]) )

    # #eps_sigma = 0.2 * 1e-3
    # # eps_sigma = 5 * 1e-2
    # eps_sigma = 2 * 1e-1
    # pi_noise = DIST.GaussianDistribution( np.zeros(1), np.array([[ eps_sigma**2 ]]) )

    # pi_prior = DIST.FactorizedDistribution(
    #     [(pi_sigma_w, [0], []),
    #      (pi_HI, [1], [])] )

    # prior_samps = pi_prior.rvs(1000000)
    # plt.figure()
    # plt.hist2d(prior_samps[:,0], prior_samps[:,1], bins=100)
    # plt.show(False)

    # approx_pi_prior = DIST.FactorizedDistribution(
    #     [(pi_sigma_w, [0], []),
    #      (approx_pi_HI, [1], [])] )

    # # Set up Bayesian problem
    # # F = VerticalMeasurementMap()
    # F = HorizontalMeasurementMap()
    # flag = FD.check_grad_x(F.evaluate, F.grad_x, pi_prior.rvs(10), 1e-4)
    # flag = FD.check_hess_x_from_grad_x(
    #     F.grad_x, F.hess_x, pi_prior.rvs(10), 1e-4)
    # generating_value = pi_prior.rvs(1)        
    # obs = ( F.evaluate(generating_value) + pi_noise.rvs(1) ).flatten()
    # logL = LKL.AdditiveLogLikelihood(obs, pi_noise, F)
    # pi = DISTINF.BayesPosteriorDistribution(logL, approx_pi_prior)

    # # Transform the distribution to the real line
    # WMAP = MAPS.ListStackedMap([E, LINK], [[0],[1]])
    # WMAP_approx_pi_prior = DIST.FactorizedDistribution(
    #     [( DIST.PullBackTransportMapDistribution(E, pi_sigma_w), [0], [] ),
    #      ( rho1d, [1], [])] )
    # WMAP_logL = LKL.AdditiveLogLikelihood(
    #     obs, pi_noise, MAPS.CompositeMap(F, WMAP))
    # WMAP_pi = DISTINF.BayesPosteriorDistribution(
    #     WMAP_logL, WMAP_approx_pi_prior)
    # DIAG.plotAlignedConditionals(WMAP_pi, do_diag=False)

    # rho = DIST.StandardNormalDistribution(pi.dim)

    # # Laplace approximation
    # lap_approx = TM.laplace_approximation(WMAP_pi)
    # lap_map = MAPS.LinearTransportMap.build_from_Gaussian(lap_approx)
    # pull_lap_WMAP_pi = DIST.PullBackTransportMapDistribution(lap_map, WMAP_pi)
    # var_diag_lap = DIAG.variance_approx_kl(
    #     rho, pull_lap_WMAP_pi, qtype=3, qparams=[15,15])
    # print("Laplace variance diagnostic: %e" % var_diag_lap)
    # DIAG.plotAlignedConditionals(pull_lap_WMAP_pi, do_diag=False)

    # # Solve variantional problem
    # tm = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(pi.dim, 5)
    # push_tm_rho = DIST.PushForwardTransportMapDistribution(tm, rho)

    # # TM.setLogLevel(10)
    # log = push_tm_rho.minimize_kl_divergence(
    #     pull_lap_WMAP_pi, qtype=3, qparams=[10,10], tol=1e-4,
    #     regularization={'type':'L2', 'alpha': 1e-3})

    # pull_tm_pull_lap_WMAP_pi = DIST.PullBackTransportMapDistribution(
    #     tm, pull_lap_WMAP_pi)
    # var_diag = DIAG.variance_approx_kl(
    #     rho, pull_tm_pull_lap_WMAP_pi, qtype=3, qparams=[15,15])
    # print("Map variance diagnostic: %e" % var_diag)
    # DIAG.plotAlignedConditionals(pull_tm_pull_lap_WMAP_pi, do_diag=False)

    # push_lap_push_tm_rho = DIST.PushForwardTransportMapDistribution(lap_map, push_tm_rho)
    # push_WMAP_push_lap_push_tm_rho = DIST.PushForwardTransportMapDistribution(
    #     WMAP, push_lap_push_tm_rho)

    # posterior_samps = push_WMAP_push_lap_push_tm_rho.rvs(100000)
    # plt.figure()
    # plt.hist2d(posterior_samps[:,0], posterior_samps[:,1], bins=100)
    # plt.show(False)

finally:
    if mpi_pool is not None:
        mpi_pool.stop()