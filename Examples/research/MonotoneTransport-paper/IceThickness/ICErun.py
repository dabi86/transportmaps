import sys
import os
import shutil
import dill as pickle
import copy

import numpy as np
import numpy.random as npr

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import src.TransportMaps.Maps as MAPS
import TransportMaps.Algorithms.Adaptivity as ALGADAPT
import TransportMaps.Diagnostics as DIAG

NPROCS = 10
RND_SEED = 1

def store(tm, obj, fname):
    if os.path.exists(fname):
        shutil.copyfile(fname, fname + '.bak')
    with open(fname, 'wb') as ostr:
        pickle.dump(obj, ostr)
    try:
        os.remove(fname + '.bak')
    except:
        pass

TM.setLogLevel(10)

nloc = 121
nobs = 9
FNAME = 'ice-nloc%d-nobs%d' % (nloc, nobs)
with open('data/' + FNAME + '.dill', 'rb') as istr:
    pi = pickle.load(istr)

OUT_FNAME = 'data/' + FNAME + '-mc-reg1e-4-run.dill'
if os.path.exists(OUT_FNAME):
    print("Output file already exists. Terminating.")
    sys.exit(0)
    
# pi = DIST.PushForwardTransportMapDistribution(
#     MAPS.PermutationTransportMap([nobs, nobs+1] + list(range(nobs))),
#     pi_tar)
    
pi_copy = copy.deepcopy(pi)

# # Laplace pull
# lap = TM.laplace_approximation(pi)
# L = MAPS.LinearTransportMap.build_from_Gaussian(lap)
# pi = DIST.PullBackTransportMapDistribution(L, pi)

rho = DIST.StandardNormalDistribution(dim=pi.dim)

tm = TM.Default_IsotropicIntegratedSquaredDiagonalTransportMap(
    dim=pi.dim, order=1, btype='fun')
# tm = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(dim=pi.dim, order=1)

# Start a pool of processes
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)
else:
    mpi_pool = None

# if mpi_pool is not None:
#     mpi_pool.mod_import([(None, 'numpy.random', 'npr')])
#     def set_seed(RND_SEED):
#         print("Setting seed: %d" % RND_SEED)
#         npr.seed(RND_SEED)
#     bcast_tuple = (['RND_SEED'],[RND_SEED])
#     TM.mpi_map(set_seed, bcast_tuple=bcast_tuple, mpi_pool=mpi_pool, concatenate=False)
npr.seed(RND_SEED)

solve_params = {
    'qtype': 0,
    'qparams': 5000,
    'regularization': {'type': 'L2', 'alpha': 1e-4},
    'mpi_pool': mpi_pool,
    'batch_size': int(1e9)
}
validator = DIAG.SampleAverageApproximationKLMinimizationValidator(
    eps=5e-3, eps_abs=5e-3, max_nsamps=20000, lower_n=10)

# solve_params = {
#     'qtype': 3,
#     'qparams': [5]*pi.dim,
#     # 'regularization': {'type': 'L2', 'alpha': 1e-3},
#     'mpi_pool': mpi_pool,
#     # 'batch_size': int(1e8)
# }
# validator = None

regression_params = {}
regression_builder = ALGADAPT.L2RegressionBuilder(regression_params)

# coeff_trunc = 1e-4
# coeff_trunc = {'type': 'percentage', 'p': 0.5}
coeff_trunc = {'type': 'constant', 'val': 10}
# coeff_trunc = {'type': 'manual'}
prune_trunc = {'type': 'manual', 'val': None}

builder = ALGADAPT.FirstVariationKullbackLeiblerBuilder(
    rho, pi, tm, validator=validator, eps_bull=1e-1,
    solve_params=solve_params,
    regression_builder=regression_builder,
    coeff_trunc=coeff_trunc,
    use_fv_hess=True, prune_trunc=prune_trunc, avar_trunc=1e-3,
    line_search_params={'maxiter': 40},
    verbosity=0, interactive=True,
)
builder.callback = store
builder.callback_kwargs = {
    'obj': builder,
    'fname': OUT_FNAME}

try:    
    (tm, _) = builder.solve()
finally:
    if mpi_pool is not None:
        mpi_pool.stop()

