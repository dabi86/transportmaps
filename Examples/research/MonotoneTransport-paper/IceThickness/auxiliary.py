import numpy as np
import scipy.stats as stats
import scipy.special as scisp

import TransportMaps as TM
import TransportMaps.Distributions as DIST


class GammaDistribution(DIST.Distribution):
    def __init__(self, k, l, t):
        self.k = k
        self.l = l
        self.t = t
        self.d1 = stats.gamma(k, loc=l, scale=t)
        super(GammaDistribution, self).__init__(dim=1)
    def rvs(self, m, *args, **kwargs):
        return self.d1.rvs(m).reshape((m,1))
    def log_pdf(self, x, *args, **kwargs):
        leq_mask = x[:,0]-self.l <= 0
        g_mask = np.logical_not(leq_mask)
        out = np.zeros(x.shape[0])
        out[leq_mask] = - np.inf
        out[g_mask] = (self.k - 1) * np.log(x[g_mask,0] - self.l) \
                      - (x[g_mask,0] - self.l) / self.t \
                      - scisp.gammaln(self.k) \
                      - self.k * np.log(self.t)
        return out
    def grad_x_log_pdf(self, x, *args, **kwargs):
        leq_mask = x[:,0]-self.l <= 0
        g_mask = np.logical_not(leq_mask)
        out = np.zeros((x.shape[0],1))
        out[leq_mask,0] = np.nan
        out[g_mask,0] = (self.k - 1) / (x[g_mask,0] - self.l) - 1./self.t
        return out
    def hess_x_log_pdf(self, x, *args, **kwargs):
        leq_mask = x[:,0]-self.l <= 0
        g_mask = np.logical_not(leq_mask)
        out = np.zeros((x.shape[0],1,1))
        out[leq_mask,0,0] = np.nan
        out[g_mask,0,0] = - (self.k - 1) / (x[g_mask,0] - self.l)**2
        return out

class GammaMixtureDistribution(DIST.Distribution):
    def __init__(self, p, k1, l1, t1, k2, l2, t2):
        self.p = p
        self.k1 = k1
        self.k2 = k2
        self.t1 = t1
        self.t2 = t2
        self.d1 = stats.gamma(k1, loc=l1, scale=t1)
        self.d2 = stats.gamma(k2, loc=l2, scale=t2)
        self.bern = stats.bernoulli(self.p)
        super(GammaMixtureDistribution, self).__init__(dim=1)        
    def rvs(self, n, *args, **kwargs):
        mask = self.bern.rvs(n).astype(bool)
        n1 = np.sum(mask)
        n2 = n - n1
        out = np.zeros((n,1))
        out[mask,0] = self.d1.rvs(n1)
        out[np.logical_not(mask),0] = self.d2.rvs(n2)
        return out
    def quadrature(self, qtype, qparams, *args, **kwargs):
        if qtype == 0:
            return (self.rvs(qparams), np.ones(qparams)/qparams)
        else:
            raise NotImplementedError()
    def pdf(self, x, *args, **kwargs):
        return self.p * self.d1.pdf(x) + (1-self.p) * self.d2.pdf(x)

class VerticalMeasurementMap(Maps.Map):
    def __init__(self):
        super(VerticalMeasurementMap, self).__init__(2, 1)
    @TM.cached()
    @TM.counted
    def evaluate(self, x, *args, **kwargs):
        wI = x[:,[0]]
        hI = x[:,[1]]
        return wI / np.sqrt(4 * hI**2 + 1)
    @TM.cached()
    @TM.counted
    def grad_x(self, x, *args, **kwargs):
        wI = x[:,0]
        hI = x[:,1]
        out = np.zeros((x.shape[0], self.dim_out, self.dim_in))
        out[:,0,0] = 1. / np.sqrt(4 * hI**2 + 1)
        out[:,0,1] = -4 * wI * hI / np.sqrt(4 * hI**2 + 1)**3
        return out
    @TM.cached()
    @TM.counted
    def hess_x(self, x, *args, **kwargs):
        wI = x[:,0]
        hI = x[:,1]
        out = np.zeros((x.shape[0], self.dim_out, self.dim_in, self.dim_in))
        out[:,0,0,1] = - 4 * hI / np.sqrt(4 * hI**2 + 1)**3
        out[:,0,1,0] = out[:,0,0,1]
        out[:,0,1,1] = - 4 * wI / np.sqrt(4 * hI**2 + 1)**3 \
                       + 48 * wI * hI**2 / np.sqrt(4 * hI**2 + 1)**5
        return out
    # @TM.cached()
    # @TM.counted
    # def action_hess_x(self, x, dx, *args, **kwargs):
    #     wI = x[:,0]
    #     hI = x[:,1]
    #     # out = np.zeros((x.shape[0], self.dim_out, self.dim_in))        
    #     out = np.zeros((x.shape[0], self.dim_out, self.dim_in, self.dim_in))
    #     out[:,0,0,1] = - 4 * hI / np.sqrt(4 * hI**2 + 1)**3
    #     out[:,0,1,0] = out[:,0,0,1]
    #     out[:,0,1,1] = - 4 * wI / np.sqrt(4 * hI**2 + 1)**3 \
    #                    + 48 * wI * hI**2 / np.sqrt(4 * hI**2 + 1)**5
    #     return out

class HorizontalMeasurementMap(Maps.Map):
    def __init__(self):
        super(HorizontalMeasurementMap, self).__init__(2, 1)
    @TM.cached()
    @TM.counted
    def evaluate(self, x, *args, **kwargs):
        wI = x[:,[0]]
        hI = x[:,[1]]
        return wI * ( np.sqrt(4*hI**2 + 1) - 2 * hI )
    @TM.cached()
    @TM.counted
    def grad_x(self, x, *args, **kwargs):
        wI = x[:,0]
        hI = x[:,1]
        out = np.zeros((x.shape[0], self.dim_out, self.dim_in))
        out[:,0,0] = np.sqrt(4*hI**2 + 1) - 2 * hI
        out[:,0,1] = wI * (4 * hI / np.sqrt(4*hI**2 + 1) - 2)
        return out
    @TM.cached()
    @TM.counted
    def hess_x(self, x, *args, **kwargs):
        wI = x[:,0]
        hI = x[:,1]
        out = np.zeros((x.shape[0], self.dim_out, self.dim_in, self.dim_in))
        den = np.sqrt(4*hI**2 + 1)
        out[:,0,0,1] = 4 * hI / den - 2
        out[:,0,1,0] = out[:,0,0,1]
        out[:,0,1,1] = wI * (4 / den**3)
        return out

class SpatiallyDistributedMeasurementMap(Maps.Map):
    # In the following we assume that the inputs to the map evaluation
    # are [w, H], where w is the conductivity (scalar) and
    # H is a vector of heights at all the discretization points.
    # The map represents the measurement as selected locations.
    def __init__(self, dim_grid, mmap, idxs_list):
        self.mmap = mmap
        self.idxs_list = idxs_list
        super(SpatiallyDistributedMeasurementMap, self).__init__(
            dim_grid + 1, len(self.idxs_list) )
    @TM.cached()
    @TM.counted
    def evaluate(self, x, *args, **kwargs):
        out = np.zeros((x.shape[0], self.dim_out))
        for i, idx in enumerate(self.idxs_list):
            out[:,[i]] = self.mmap.evaluate(
                np.hstack( (x[:,[0]], x[:,[idx+1]]) ) )
        return out
    @TM.cached()
    @TM.counted
    def grad_x(self, x, *args, **kwargs):
        out = np.zeros((x.shape[0], self.dim_out, self.dim_in))
        for i, idx in enumerate(self.idxs_list):
            gx = self.mmap.grad_x(
                np.hstack( (x[:,[0]], x[:,[idx+1]]) ) )
            out[:,i,0] = gx[:,0,0]
            out[:,i,idx+1] = gx[:,0,1]
        return out
    @TM.cached()
    @TM.counted
    def hess_x(self, x, *args, **kwargs):
        out = np.zeros((x.shape[0], self.dim_out,
                        self.dim_in, self.dim_in))
        for i, idx in enumerate(self.idxs_list):
            hx = self.mmap.hess_x(
                np.hstack( (x[:,[0]], x[:,[idx+1]]) ) )
            out[:,i,0,0] = hx[:,0,0,0]
            out[:,i,0,idx+1] = hx[:,0,0,1]
            out[:,i,idx+1,0] = out[:,i,0,idx+1]
            out[:,i,idx+1,idx+1] = hx[:,0,1,1]
        return out