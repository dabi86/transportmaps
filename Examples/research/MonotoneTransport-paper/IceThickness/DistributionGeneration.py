import matplotlib.pyplot as plt

import numpy as np
import numpy.random as npr
import scipy.stats as stats
from scipy.spatial.distance import pdist, squareform

import dill as pickle

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import src.TransportMaps.Maps as MAPS
import TransportMaps.Likelihoods as LKL
import TransportMaps.Distributions.Inference as DISTINF
import TransportMaps.Diagnostics as DIAG
import TransportMaps.FiniteDifference as FD

from aux import \
    GammaDistribution, GammaMixtureDistribution, \
    VerticalMeasurementMap, HorizontalMeasurementMap, \
    SpatiallyDistributedMeasurementMap

NPROCS = 10

TM.setLogLevel(10)
npr.seed(1)

STORE = False
PLOTTING = True

XNGRID = 5
YNGRID = 5
COV_TYPE = 'ou'
COV_CORR_LEN = 40.
MEAS_TYPE = 'horizontal'
# MEAS_LIST = [ (2,2), (2,5), (2,8),
#               (5,2), (5,5), (5,8),
#               (8,2), (8,5), (8,8) ]
MEAS_LIST = [ (1,1), (1,2), (1,3),
              (2,1), (2,2), (2,3),
              (3,1), (3,2), (3,3) ]

nloc = XNGRID * YNGRID
nobs = len(MEAS_LIST)

p = .2
k1 = 2.5
l1 = 0.25
t1 = .5
k2 = 14.
l2 = 0.25
t2 = .25
pi_HI1 = GammaMixtureDistribution(1., k1, l1, t1, k2, l2, t2)
pi_HI2 = GammaMixtureDistribution(0., k1, l1, t1, k2, l2, t2)
pi_HI = GammaMixtureDistribution(p, k1, l1, t1, k2, l2, t2)

# Start a pool of processes
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)
else:
    mpi_pool = None

try:
    if PLOTTING:
        x = np.linspace(0, 7, 100).reshape((100,1))
        plt.figure()
        plt.plot(x, p * pi_HI1.pdf(x), 'k')
        plt.plot(x, (1-p) * pi_HI2.pdf(x), 'b')
        plt.plot(x, pi_HI.pdf(x), 'r')
        plt.show(False)

    ######
    print('\nCreate map from samples T for (local) ice thickness distribution')
    # 1) Generate samples (on the real line)
    N = 1000
    E = Maps.FrozenExponentialDiagonalTransportMap(1)
    pull_E_pi = DIST.PullBackTransportMapDistribution(E, pi_HI)
    xx, ww = pull_E_pi.quadrature(0, N)
    # 2) Center and rescale
    xmean = np.mean(xx)
    xstd = np.std(xx)
    L = Maps.FrozenLinearDiagonalTransportMap(
        np.array([xmean]), np.array([xstd]) )
    xL = L.inverse(xx)
    EL = Maps.CompositeMap(E, L)
    pull_EL_pi = DIST.PullBackTransportMapDistribution(EL, pi_HI)
    # 3) Learning: density estimation / map from samples
    T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
        1, 9, btype='poly')
    push_T_pull_EL_pi = DIST.PushForwardTransportMapDistribution(T, pull_EL_pi)
    rho1d = DIST.StandardNormalDistribution(1)
    push_T_pull_EL_pi.minimize_kl_divergence(
        rho1d, x=xL, w=ww,
        regularization={'type':'L2', 'alpha':5e-2})
    # 4) Invert map T (kl-minimization)
    pull_T_rho1d = DIST.PullBackTransportMapDistribution(T, rho1d)
    Q1d = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
        1, 17, btype='poly')
    push_Q1d_rho1d = DIST.PushForwardTransportMapDistribution(Q1d, rho1d)
    log = push_Q1d_rho1d.minimize_kl_divergence(
        pull_T_rho1d,
        # qtype=3, qparams=[30],
        qtype=0, qparams=100000,
        regularization={'type':'L2', 'alpha':1e-3},
        mpi_pool=mpi_pool
    )
    # # 4) Invert map T (regression)
    # Q1d = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
    #     1, 50, btype='poly')
    # xr = np.linspace(-10,10,3000)[:,np.newaxis]
    # wr = np.ones(len(xr))/len(xr)
    # scatter_tuple = (['x'], [xr])
    # Tinv_xr = TM.mpi_map(
    #     'inverse', obj=T, scatter_tuple=scatter_tuple,
    #     mpi_pool=mpi_pool)
    # log = Q1d.regression(
    #     Tinv_xr, x=xr, w=wr, maxit=5000, tol=1e-6,
    #     # regularization={'type': 'L2', 'alpha': 5e-3}
    # )
    # 5) Assemble approximation
    LINK1d = Maps.ListCompositeMap([E, L, Q1d])
    push_LINK1d_rho1d = DIST.PushForwardTransportMapDistribution(LINK1d, rho1d)
    ######

    ######
    print('\nBuild spatial grids')
    # 1) Build estimation grid
    xgrid = np.linspace(0, 100, XNGRID)
    ygrid = np.linspace(0, 100, YNGRID)
    msh_grid = np.meshgrid(xgrid, ygrid)
    grid = np.vstack((msh_grid[0].flatten(), msh_grid[1].flatten())).T
    dim_grid = grid.shape[0]
    # 2) Build index grid
    xidxs = range(XNGRID)
    yidxs = range(YNGRID)
    msh_idxs = np.meshgrid(xidxs, yidxs)
    idxs = np.vstack((msh_idxs[0].flatten(), msh_idxs[1].flatten())).T
    # 3) Find measurement indexes
    idxs_list = [ tuple( idxs[i,:] ) for i in range(dim_grid) ]
    meas_idxs_list = [ i for i in range(dim_grid)
                       if idxs_list[i] in MEAS_LIST ]
    not_meas_idxs_list = [ i for i in range(dim_grid)
                           if idxs_list[i] not in MEAS_LIST ]
    # 4) Reorder grid with observation positions first
    srt_grid_list = meas_idxs_list + not_meas_idxs_list
    inv_srt_grid_list = [None for i in range(dim_grid)]
    for i, idx in enumerate(srt_grid_list):
        inv_srt_grid_list[idx] = i
    srt_grid = grid[srt_grid_list,:]
    ######    

    ######
    print('\nAssemble forward model')
    # 1) Define measurement map
    if MEAS_TYPE == 'vertical':
        F = VerticalMeasurementMap()
    elif MEAS_TYPE == 'horizontal':
        F = HorizontalMeasurementMap()
    # 2) Define spatially the distributed measurement map
    SF = SpatiallyDistributedMeasurementMap(
        dim_grid, F, range(nobs))
    # 3) Define measurement noise
    eps_sigma = 2 * 1e-1
    pi_noise = DIST.GaussianDistribution(
        np.zeros(nobs), eps_sigma**2 * np.eye(nobs) )
    ######

    ######
    print('\nConstruct Gaussian Process representing spatial correlation')
    # 1) Define covariance functions
    def ou_cov(var, dists, l):
        return var * np.exp( - dists / l )
    def sqexp_cov(var, dists, l):
        return var * np.exp( - dists**2 / 2 / l**2 )
    def id_cov(var, n):
        return var * np.eye(n)
    # 3) Compute the pairwise distances
    dists = squareform( pdist(srt_grid) )
    # 4) Build covariance
    if COV_TYPE == 'ou':
        cov = ou_cov(1, dists, COV_CORR_LEN)
    elif COV_TYPE == 'sqexp':
        cov = sqexp_cov(1, dists, COV_CORR_LEN)
    # 5) Build Gaussian Process
    gpdist = DIST.GaussianDistribution(
        np.zeros(dim_grid), cov)
    # 6) Build the Gaussian Process as the pushforward of N(0,1)
    G = Maps.LinearTransportMap.build_from_Gaussian(gpdist)
    rho_grid = DIST.StandardNormalDistribution(dim_grid)
    gpdist = DIST.PushForwardTransportMapDistribution(G, rho_grid)
    ######

    ######
    print('\nConstruct the spatially correlated prior')
    # 1) Build the diagonal maps to transform N(0,I)
    #    to the height distribution with no spatial correlation
    EG = Maps.FrozenExponentialDiagonalTransportMap(dim_grid)
    LG = Maps.FrozenLinearDiagonalTransportMap(
        np.array([xmean]*dim_grid), np.array([xstd]*dim_grid) )
    Q = Maps.DiagonalIsotropicTransportMap(dim_grid, Q1d.approx_list[0])
    # # Test Q derivatives
    # Qold = MAPS.TriangularTransportMap(
    #     [ [i] for i in range(dim_grid) ],
    #     [ Q1d.approx_list[0] ] * dim_grid )
    # flag = FD.check_grad_x(Q.evaluate, Q.grad_x, gpdist.rvs(1), 1e-4)
    # flag = FD.check_grad_x(Q.grad_x, Q.hess_x, gpdist.rvs(1), 1e-4)
    # flag = FD.check_grad_x(Q.log_det_grad_x, Q.grad_x_log_det_grad_x, gpdist.rvs(1), 1e-4)
    # flag = FD.check_grad_x(Q.grad_x_log_det_grad_x, Q.hess_x_log_det_grad_x,
    #                        gpdist.rvs(1), 1e-4)
    LINK = Maps.ListCompositeMap([EG, LG, Q])
    # 2) Build the spatially correlated prior distribution
    push_LINK_gpdist = DIST.PushForwardTransportMapDistribution(LINK, gpdist)
    pi_HI = push_LINK_gpdist
    ######

    if PLOTTING:
        print('\nGenerate some samples and plot')
        nsmpl = 5
        smpl = pi_HI.rvs(nsmpl)
        smpl = smpl[:,inv_srt_grid_list]
        for i in range(nsmpl):
            plt.figure()
            plt.imshow( smpl[i,:].reshape((XNGRID,YNGRID)) )
            plt.colorbar()
            plt.show(False)

    ######
    print('\nAssemble the overall prior including the prior on water conductivity')
    # 1) Build prior on water conductivity
    pi_sigma_w = GammaDistribution(5000, 0., 0.001)
    # 2) Assemble prior
    pi_prior = DIST.FactorizedDistribution(
        [(pi_sigma_w, [0], []),
         (pi_HI, list(range(1,pi_HI.dim+1)), [])] )
    ######


    ######
    print('\nGenerate synthetic data')
    # 1) Generate latent truth from prior
    synth_latent = pi_prior.rvs(1)
    # 2) Generate observation
    synth_obs = ( SF.evaluate(synth_latent) + pi_noise.rvs(1) ).flatten()
    ######

    ######
    print('\nSetup the Bayesian problem')
    logL = LKL.AdditiveLogLikelihood(synth_obs, pi_noise, SF)
    pi = DISTINF.BayesPosteriorDistribution(logL, pi_prior)
    ######

    ######
    print('\nTransform the domain to the real line and whiten')
    FIELDMAP = Maps.CompositeMap(LINK, G)
    WMAP = Maps.ListStackedMap(
        [E, FIELDMAP], [[0], list(range(1,dim_grid+1))] )
    pull_WMAP_pi_prior = DIST.FactorizedDistribution(
        [( DIST.PullBackTransportMapDistribution(E, pi_sigma_w), [0], []),
         ( rho_grid, list(range(1,pi_HI.dim+1)), [])] )
    pull_WMAP_logL = LKL.AdditiveLogLikelihood(
        synth_obs, pi_noise, Maps.CompositeMap(SF, WMAP) )
    pull_WMAP_pi = DISTINF.BayesPosteriorDistribution(
        pull_WMAP_logL, pull_WMAP_pi_prior)
    ######

    ######
    print('\nSet reference distribution')
    rho = DIST.StandardNormalDistribution(pull_WMAP_pi.dim)
    ######

    ######
    print('\nLaplace approximation')
    lap_approx = TM.laplace_approximation(pull_WMAP_pi)
    lap_map = Maps.LinearTransportMap.build_from_Gaussian(lap_approx)
    pull_lap_pull_WMAP_pi = DIST.PullBackTransportMapDistribution(lap_map, pull_WMAP_pi)
    print('\nComputing Laplace variance diagnostic')
    var_diag_lap = DIAG.variance_approx_kl(
        rho, pull_lap_pull_WMAP_pi, qtype=0, qparams=10000,
        mpi_pool_tuple=(None,mpi_pool))
    print("Laplace variance diagnostic: %e" % var_diag_lap)
    ######

    ######
    # Append settings to distribution
    settings = {
        'xngrid': XNGRID,
        'yngrid': YNGRID,
        'grid': grid,
        'idxs': idxs,
        'meas_list': MEAS_LIST,
        'srt_grid_list': srt_grid_list,
        'inv_srt_grid_list': inv_srt_grid_list,
        'synth_latent': synth_latent,
    }
    pull_WMAP_pi.settings = settings
    ######

    if STORE:
        # Store distribution
        with open('data/ice-nloc%d-nobs%d.dill' % (nloc,nobs), 'wb') as ostr:
            pickle.dump(pull_WMAP_pi, ostr)

finally:
    if mpi_pool is not None:
        mpi_pool.stop()