import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import scipy.stats as stats
import numpy as np
import numpy.random as npr
import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)

figfmt_list = ['svg', 'pdf', 'eps', 'png']

npr.seed(1)

alpha = 2.
beta = 10.
pi = DIST.BetaDistribution(alpha, beta)

# Support map L
L = Maps.FrozenGaussianToUniformDiagonalTransportMap(1)
# Exact map T^*
class BetaTransportMap(object):
    def __init__(self, alpha, beta):
        self.tar = stats.beta(alpha, beta)
        self.ref = stats.norm(0.,1.)
    def invsuppmap(self, x, *args, **kwargs):
        if isinstance(x,float):
            x = np.array([[x]])
        if x.ndim == 1:
            x = x[:,NAX]
        std = stats.norm()
        spd = stats.uniform()
        out = std.ppf( spd.cdf( x ) )
        return out
    def evaluate(self, x, *args, **kwargs):
        if isinstance(x,float):
            x = np.array([[x]])
        if x.ndim == 1:
            x = x[:,NAX]
        out = self.invsuppmap( self.tar.ppf( self.ref.cdf(x) ) )
        return out
    def __call__(self, x):
        return self.evaluate(x)
Tstar = BetaTransportMap(alpha, beta)

def plot_mapping(x, L, tar_star, Tstar, tar=None, T=None):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax_twx = ax.twinx()
    ax_twy = ax.twiny()
    ax.plot(x, L(Tstar(x)), 'k-', label=r"$T^\star$") # Map
    n01, = ax_twx.plot(x, stats.norm(0.,1.).pdf(x), '-b') # N(0,1)
    g, = ax_twy.plot(tar_star.pdf(L(Tstar(x))), L(Tstar(x)), '-r') # Gumbel
    if T is not None:
        ax.plot(x, L(T(x)), 'k--', label=r"$\hat{T}$") # Map
    if tar is not None:
        ax_twy.plot(tar.pdf(L(Tstar(x))), L(Tstar(x)), '--r') # Gumbel
    ax.set_ylabel(r"Map")
    ax_twx.set_ylabel('N(0,1)')
    ax_twx.yaxis.label.set_color(n01.get_color())
    ax_twy.set_xlabel('Beta')
    ax_twy.xaxis.label.set_color(g.get_color())
    ax.legend(loc = (0.1, 0.8))
    plt.show(False)
    return fig

def plot_summary(x_tm, L, tar_star, Tstar, tar_list, T_list, var_diag_list, order_list):
    fig = plt.figure(figsize=(14,6))
    nord = len(order_list)
    max_tar = np.max(tar_star.pdf(L(Tstar(x_tm))))
    max_tar *= 1.1
    for i, (order, tar, T, var_diag) in enumerate(
            zip(order_list, tar_list, T_list, var_diag_list)):
        ax = fig.add_subplot(2, nord, i+1)
        ax.set_title("Order %d\n" % order + \
                     "Var.diag. %.1e" % var_diag)
        # r"$\mathbb{V}[\rho / T^\sharp L^\sharp \pi]=%.1e$" % var_diag)
        # if not np.isnan(var_diag):
        ax.plot(L(Tstar(x_tm)), tar_star.pdf(L(Tstar(x_tm))), '-k', linewidth=3)
        ax.plot(L(Tstar(x_tm)), tar.pdf(L(Tstar(x_tm))), '--r', linewidth=3)
        ax.set_ylim(0,max_tar)
        ax.set_xlim(0,1)
        ax.get_xaxis().set_ticks([0,1])
        if i == 0:
            ax.set_ylabel(r"$\pi(x)$")
        else:
            ax.get_yaxis().set_visible(False)
            ax.get_yaxis().set_ticks([])
        ax = fig.add_subplot(2, nord, i+1+nord)
        ax.plot(x_tm, L(Tstar(x_tm)), '-k', linewidth=3)
        ax.plot(x_tm, L(T(x_tm)), '--r', linewidth=3)
        ax.set_ylim(0,1)
        ax.get_yaxis().set_ticks([0,1])
        if i == 0:
            ax.set_ylabel(r"$L\circ T(x)$")
        else:
            ax.get_yaxis().set_visible(False)
            ax.get_yaxis().set_ticks([])
        # ax.set_xlabel(r"$\mathbb{V}[\rho / T_\sharp L_\sharp \pi] = %.2e$" % var_diag)
    plt.show(False)
    return fig        

x_tm = np.linspace(-6,6,100).reshape((100,1))

fig = plot_mapping(x_tm, L, pi, Tstar)
for figfmt in figfmt_list:
    fig.savefig('mapping.' + figfmt)

rho = DIST.StandardNormalDistribution(1)
pull_L_pi = DIST.PullBackTransportMapDistribution(L, pi)

qtype = 0      # Gauss quadrature
qparams = 1000 # Quadrature order
tol = 1e-10    # Optimization tolerance
reg = {'type': 'L2', 'alpha': 1e-4}
maxit = 10000

orders = [ 2*n + 1 for n in range(0,5) ]

print("Linear Span approximation")
ders = 1       # Use gradient
T_list = []
tar_list = []
var_diag_list = []
for o in orders:
    print("Order: %d" % o)
    T = TM.Default_IsotropicMonotonicLinearSpanTriangularTransportMap(1, o)
    push_T_rho = DIST.PushForwardTransportMapDistribution(T, rho)

    log = push_T_rho.minimize_kl_divergence(
        pull_L_pi, qtype=qtype, qparams=qparams, regularization=reg,
        tol=tol, ders=ders, maxit=maxit)

    push_LT_rho = DIST.PushForwardTransportMapDistribution(L, push_T_rho)

    pull_TL_pi = DIST.PullBackTransportMapDistribution(T, pull_L_pi)
    var_diag = DIAG.variance_approx_kl(rho, pull_TL_pi, qtype=3, qparams=[20])

    T_list.append(T)
    tar_list.append( push_LT_rho )
    var_diag_list.append(var_diag)
    
    # plot_mapping(x_tm, L, pi, Tstar, push_LT_rho, T)

fig = plot_summary(x_tm, L, pi, Tstar, tar_list, T_list, var_diag_list, orders)
for figfmt in figfmt_list:
    fig.savefig('Beta-poly-conv.' + figfmt)    
    
print("Integrated Squared approximation")
ders = 2       # Use gradient and Hessian
T_list = []
tar_list = []
var_diag_list = []
for o in orders:
    print("Order: %d" % o)
    T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(1, o, btype='poly')
    push_T_rho = DIST.PushForwardTransportMapDistribution(T, rho)

    log = push_T_rho.minimize_kl_divergence(
        pull_L_pi, qtype=qtype, qparams=qparams, regularization=reg,
        tol=tol, ders=ders, maxit=maxit)

    push_LT_rho = DIST.PushForwardTransportMapDistribution(L, push_T_rho)

    pull_TL_pi = DIST.PullBackTransportMapDistribution(T, pull_L_pi)
    var_diag = DIAG.variance_approx_kl(rho, pull_TL_pi, qtype=3, qparams=[20])

    T_list.append(T)
    tar_list.append( push_LT_rho )
    var_diag_list.append(var_diag)

    # plot_mapping(x_tm, L, pi, Tstar, push_LT_rho, T)

fig = plot_summary(x_tm, L, pi, Tstar, tar_list, T_list, var_diag_list, orders)
for figfmt in figfmt_list:
    fig.savefig('Beta-intsq-conv.' + figfmt)    

    