#!/bin/bash

# $1 = data/prior/ndiscr20/runs/lap-PullPrior-chol

tmap-postprocess --input=$1.dill --output=$1-POST.dill \
                 --var-diag=exact-base --var-diag-qtype=0 --var-diag-qnum=10000 \
                 --nprocs=11 -v

tmap-postprocess --input=$1.dill --output=$1-POST.dill \
                 --quadrature=approx-target --quadrature-qtype=0 --quadrature-qnum=10000 \
                 --nprocs=11 -v
