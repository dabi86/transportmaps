#!/bin/bash

TMDIR=/home/dabi/VC-Projects/Software/Mine/Current/transportmaps-private/

if [ $# -eq 0 ]
then
    CMD=python
else
    if [ $1 = "debug" ]
    then
        CMD=pudb3
    else
        echo "Argument not recognized."
        exit 1
    fi
fi
     
$CMD $TMDIR/scripts/tmap-tm \
       --dist=data/prior/ndiscr20/distributions/dist-PullPrior-eig.dill \
       --output=data/prior/ndiscr20/runs/tm-pull-lap-PullPrior-eig.dill \
       --mtype=intsq --span=total --btype=poly --order=1 --sparsity=diag \
       --qtype=0 --qnum=100 \
       --tol=1e-3 --maxit=100 --reg=1e-1 --ders=2 --hessact \
       --validator=gradboot --val-eps=1e-3 --val-gradboot-eps-abs=1e-2 \
       --adapt=fv --adapt-tol=1e-1 --adapt-verbosity=1 \
       --adapt-regr-reg=1e-4 --adapt-regr-maxit=2000 \
       --adapt-fv-maxit=10 \
       --adapt-fv-interactive \
       --map-pull=data/prior/ndiscr20/runs/lap-PullPrior-eig.dill \
       --seed=0 --log=10 --nprocs=10 \
       # --reload

# python ../../../../scripts/tmap-tm \
#        --dist=data/prior/ndiscr20/distributions/dist-PullPrior-eig.dill \
#        --output=data/prior/ndiscr20/runs/tm-PullPrior-eig.dill \
#        --mtype=intsq --span=total --btype=poly --order=1 --sparsity=diag \
#        --qtype=0 --qnum=100 \
#        --tol=5e0 --maxit=100 --reg=1e-2 --ders=1 --hessact \
#        --validator=gradboot --val-eps=1e1 \
#        --adapt=fv --adapt-tol=1e-1 --adapt-verbosity=1 \
#        --adapt-regr-reg=1e-6 --adapt-regr-maxit=2000 \
#        --adapt-fv-maxit=10 \
#        --adapt-fv-interactive \
#        --log=20 --nprocs=10 \
#        # --reload

# pudb3 ../../../../scripts/tmap-tm \
#        --dist=data/prior/ndiscr20/distributions/dist-PullPrior-eig.dill \
#        --output=data/prior/ndiscr20/runs/tm-PullPrior-eig.dill \
#        --mtype=intsq --span=total --btype=poly --order=1 --sparsity=diag \
#        --qtype=0 --qnum=100 \
#        --tol=1e-3 --maxit=100 --reg=1e-2 --ders=2 --hessact \
#        --validator=saa --val-eps=1e-2 \
#        --val-max-nsamps=20000 \
#        --val-saa-eps-abs=1e-2 --val-saa-upper-mul=10 --val-saa-lower-n=5 \
#        --adapt=fv --adapt-tol=1e-1 --adapt-verbosity=1 \
#        --adapt-regr-reg=1e-6 --adapt-regr-maxit=2000 \
#        --adapt-fv-maxit=10 \
#        --adapt-fv-interactive \
#        --log=10 --nprocs=10 \
#        # --reload
