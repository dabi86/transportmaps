import sys
import os
import getopt
import dill
import h5py
import dolfin as dol
import matplotlib.pyplot as plt
import numpy as np

argv = sys.argv[1:]

IN_FNAME = None
POST_FNAME = None
opts, args = getopt.getopt(
    argv, "hI",
    [
        "input=", 'post='
    ])
for opt, arg in opts:
    if opt == '--input':
        IN_FNAME = arg
    elif opt == '--post':
        POST_FNAME = arg
    else:
        raise ValueError("Unrecognized option %s" % opt)

with open(IN_FNAME, 'rb') as istr:
    stg = dill.load(istr)

f = h5py.File(POST_FNAME + '.hdf5', 'r')

# Plot ture field
plt.figure()
p = dol.plot(stg.target_distribution.true_field)
plt.colorbar(p)
plt.title('Generating field')
plt.show(False)

x = f['quadrature/approx-target/0']
mn = np.mean(x, axis=0)
push_mn = stg.target_distribution.logL.T.t2.evaluate(
    mn[np.newaxis,:])[0,:]
push_mn_fun = stg.target_distribution.solver.dof_to_fun(push_mn)

plt.figure()
p = dol.plot(push_mn_fun)
plt.colorbar(p)
plt.title('Mean field')
plt.show(False)