import sys
import getopt
import dill
from tqdm import tqdm
import dolfin as dol
import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as npla
import numpy.random as npr

import TransportMaps as TM
import TransportMaps.Distributions as DIST

argv = sys.argv[1:]

def usage():
    print(
        "subspace-reduction.py --dist=STR --output=STR --nsamps=INT [--prop-dist=STR --nbstrap=INT --nprocs=INT --bsize=INT]"
    )

DIST_FNAME = None
PROP_FNAME = None
OUT_FNAME = None
NSAMPS = None
NBSTRAP = 20
NPROCS = 1
BSIZE = 100
opts, args = getopt.getopt(
    argv, 'h',
    [
        "dist=", 'prop-dist=', 'output=',
        'nsamps=', 'nbstrap=',
        "nprocs=", 'bsize=',
    ])
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--dist':
        DIST_FNAME = arg
    elif opt == '--prop-dist':
        PROP_FNAME = arg
    elif opt == '--output':
        OUT_FNAME = arg
    elif opt == '--nsamps':
        NSAMPS = int(arg)
    elif opt == '--nbstrap':
        NBSTRAP = int(arg)
    elif opt == '--nprocs':
        NPROCS = int(arg)
    elif opt == '--bsize':
        BSIZE = int(arg)
    else:
        raise ValueError("Unrecognized option %s" % opt)

if None in [DIST_FNAME, OUT_FNAME, NSAMPS]:
    usage()
    sys.exit(1)
        
with open(DIST_FNAME, 'rb') as istr:
    pi = dill.load(istr)

if PROP_FNAME is None:
    prop = DIST.StandardNormalDistribution(pi.dim)
else:
    with open(PROP_FNAME, 'rb') as istr:
        prop = dill.load(istr).approx_target_distribution

rho = DIST.StandardNormalDistribution(pi.dim)

def compute_eigenvalues(gx, **kwargs):
    n = kwargs.get('n', gx.shape[1])
    try:
        idxs = kwargs['idxs']
        if idxs.shape[0] == 0:
            return np.zeros((0,n))
        else:
            idxs = idxs[0,:]
    except KeyError:
        idxs = slice(None)
    H = np.dot(gx[idxs,:].T, gx[idxs,:])
    H /= gx.shape[0]
    val, _ = npla.eigh(H)
    val = val[::-1][np.newaxis,:n]
    return val

mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

try:
    TM.mpi_bcast_dmem(pi=pi, mpi_pool=mpi_pool)

    # Compute Zahm-subspace
    n = 0
    gx = np.zeros((NSAMPS,pi.dim))
    for n in tqdm(range(0,NSAMPS,BSIZE)):
        nnew = min(BSIZE, NSAMPS-n)
        x = prop.rvs(nnew)
        gx[n:n+nnew,:] = TM.mpi_map(
            'grad_x_log_pdf',
            scatter_tuple = (['x'], [x]),
            obj='pi',
            obj_val=pi,
            mpi_pool=mpi_pool
        )
        gx[n:n+nnew,:] -= rho.grad_x_log_pdf(x)
    mpi_pool.clear_dmem()

    # Bootstrap stability
    mpi_pool.mod_import([
        (None,'numpy','np'),
        ('numpy','linalg','npla')
    ])
    TM.mpi_bcast_dmem(gx=gx, mpi_pool=mpi_pool)
    vals = np.zeros((NBSTRAP,min(NSAMPS,pi.dim)))
    for n in tqdm(range(0,NBSTRAP,NPROCS)):
        nnew = min(NBSTRAP-n, NPROCS)
        idxs = np.array([ npr.randint(NSAMPS, size=NSAMPS) for i in range(nnew) ])
        vals[n:n+nnew,:] = TM.mpi_map(
            compute_eigenvalues,
            scatter_tuple = (['idxs'],[idxs]),
            bcast_tuple = (['n'], [NSAMPS]),
            dmem_key_in_list = ['gx'],
            dmem_arg_in_list = ['gx'],
            dmem_val_in_list = [gx],
            mpi_pool=mpi_pool
        )
    std = np.std(vals, axis=0)
    wstd = std / np.mean(vals, axis=0)    
    plt.figure()
    plt.title('Bootstrap weighted std. of eigenvalues')
    plt.semilogy(wstd, 'o-')
    plt.show(False)
    
finally:
    if mpi_pool is not None:
        mpi_pool.stop()

# # Bootstrap stability
# vals = np.zeros((NBSTRAP,NSAMPS))
# for i in tqdm(range(NBSTRAP)):
#     idxs = npr.randint(NSAMPS, size=NSAMPS)
#     H = np.dot(gx[idxs,:].T, gx[idxs,:])
#     H /= NSAMPS
#     val, _ = npla.eigh(H)
#     vals[i,:] = val[::-1][:NSAMPS]

H = np.dot(gx.T, gx)
H /= NSAMPS
val, vec = npla.eigh(H)
val = val[::-1]
vec = vec[:,::-1]

cmsm = np.cumsum(val)
cmsm /= cmsm[-1]
plt.figure()
# plt.plot(cmsm[:NSAMPS], 'o-')
plt.semilogy(1-cmsm[:NSAMPS], 'o-')
plt.show(False)

trunc = lambda tar, cmsm=cmsm: next(i for i,p in enumerate(cmsm) if p > tar)

print("Power 0.9 - dim: %d" % trunc(.9) )
print("Power 0.95 - dim: %d" % trunc(.95) )
print("Power 0.99 - dim: %d" % trunc(.99) )
print("Power 0.999 - dim: %d" % trunc(.999) )

tar, success = TM.read_and_cast_input(
    "percentage (0,1] of cumulative power to be retained",
    float
)

if not success:
    print("Terminating")
    sys.exit(0)

tidx = trunc(tar)

tval = val[:tidx]
tvec = vec[:,:tidx]

for i in range(min(tidx, 20)):
    efun = pi.whitening_map.t2.evaluate(vec[:,[i]].T)[0,:]
    efun = pi.solver.dof_to_fun(np.array(efun,order='C'))
    plt.figure("Eigenfunction %d" % i)
    plt.title("Eigenfunction %d" % i)
    p = dol.plot(efun)
    plt.colorbar(p)
    plt.show(False)
    
# Define the new target
tar_dim = tidx    
U = Maps.AffineMap(
    c = np.zeros(pi.dim),
    L = tvec
)
pi.logL.subspace_map = U
pi.logL.compose( U )
pi.prior = DIST.StandardNormalDistribution(tar_dim)
pi.dim = tar_dim
pi.store(OUT_FNAME)