import sys
import os
import getopt
import dill
import h5py
import dolfin as dol
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import numpy.linalg as npla

import src.TransportMaps.Maps as MAPS

argv = sys.argv[1:]

IN_FNAME = None
POST_FNAME = None
HDF5_PATH = 'quadrature/approx-target/0'
REDUCED = False
opts, args = getopt.getopt(
    argv, "hI",
    [
        "input=", 'post=', 'h5path=', 'reduced'
    ])
for opt, arg in opts:
    if opt == '--input':
        IN_FNAME = arg
    elif opt == '--post':
        POST_FNAME = arg
    elif opt == '--h5path':
        HDF5_PATH = arg
    elif opt == '--reduced':
        REDUCED = True
    else:
        raise ValueError("Unrecognized option %s" % opt)

with open(IN_FNAME, 'rb') as istr:
    stg = dill.load(istr)

f = h5py.File(POST_FNAME + '.hdf5', 'r')

# Create figures directory
FIG_DIR = '.'.join(IN_FNAME.split('.')[:-1]) + '/' + HDF5_PATH
os.makedirs(FIG_DIR, exist_ok=True)
def save_fig(fig, fig_name):
    for fmat in ['svg', 'pdf', 'png']:
        fig.savefig(FIG_DIR + '/' + fig_name + '.' + fmat, format=fmat)

tmap = stg.tmap
pi = stg.target_distribution

if REDUCED:
    toLIS_map = pi.logL.composite_map.map_list[2]
    toGP_map = Maps.CompositeMap(pi.logL.composite_map.map_list[1].t2, toLIS_map)
    toExp_map = pi.logL.composite_map.map_list[1].t1
else:
    toGP_map = pi.logL.composite_map.map_list[1].t2
    toExp_map = pi.logL.composite_map.map_list[1].t1

# Coordinates of the discretization
coord = pi.solver.coord
# Position of the sensors
sens_pos = np.asarray(pi.sens_pos_list)
# Position of marginals
marg_pos = np.array([
    [0.3, 0.3],
    [.2, .6],
    [.6, .7],
    [.7, .3]
])

# Plot ture field
dl = 1e-4
vmin = np.min(pi.field_vals)
vmax = np.max(pi.field_vals)
levels = np.linspace((1-dl)*vmin, (1+dl)*vmax, 50)
fig = plt.figure()
p = dol.plot(pi.true_field, levels=levels)
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.scatter(marg_pos[:,0], marg_pos[:,1], c='w', s=20, marker='s', edgecolors='k', linewidth=1)
plt.colorbar(p)
plt.title('Generating field')
plt.show(False)
save_fig(fig, 'true-field')

# # Plot maximum order in constant part
# mat_c = np.zeros( (tmap.dim_in, tmap.dim_in), dtype=int )
# mat_h = np.zeros( (tmap.dim_in, tmap.dim_in), dtype=int )
# for i, (avars, comp) in enumerate(zip(tmap.active_vars,tmap.approx_list)):
#     midxs = np.asarray(comp.c.multi_idxs)
#     for j, var in enumerate(avars):
#         mat_c[i,var] = np.max(midxs[:,j])
#     midxs = np.asarray(comp.h.multi_idxs)
#     for j, var in enumerate(avars):
#         mat_h[i,var] = np.max(midxs[:,j])
# plt.figure()
# plt.imshow(mat_c)
# plt.colorbar()
# plt.title("Maximum directional order -- constant")
# plt.show(False)

# plt.figure()
# plt.imshow(mat_h)
# plt.colorbar()
# plt.title("Maximum directional order -- integrated")
# plt.show(False)

# Load data
x = f[HDF5_PATH]

def plot(x):
    push_x = toGP_map.evaluate(x[:,:]) # Samples still in Normal space
    push_mn = np.mean(push_x, axis=0)
    push_std = np.std(push_x, axis=0)
    push_mn_fun = pi.solver.dof_to_fun(push_mn)
    push_std_fun = pi.solver.dof_to_fun(push_std)

    idxs = npr.choice(x.shape[0], size=3, replace=False)
    for i in idxs:
        push_x_fun = pi.solver.dof_to_fun(
            np.asarray(push_x[i,:],order='C'))
        fig = plt.figure()
        p = dol.plot(push_x_fun)
        plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
        plt.scatter(marg_pos[:,0], marg_pos[:,1], c='w', s=20, marker='s', edgecolors='k', linewidth=1)
        plt.colorbar(p)
        plt.title('Sample %d' % i)
        plt.show(False)
        save_fig(fig, 'sample-%d' % i)

    fig = plt.figure()
    p = dol.plot(push_mn_fun)
    plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
    plt.scatter(marg_pos[:,0], marg_pos[:,1], c='w', s=20, marker='s', edgecolors='k', linewidth=1)
    plt.colorbar(p)
    plt.title('Mean field')
    plt.show(False)
    save_fig(fig, 'mean-field')

    fig = plt.figure()
    p = dol.plot(push_std_fun)
    plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
    plt.scatter(marg_pos[:,0], marg_pos[:,1], c='w', s=20, marker='s', edgecolors='k', linewidth=1)
    plt.colorbar(p)
    plt.title('Standard deviation')
    plt.show(False)
    save_fig(fig, 'std-field')

    # Marginals
    # Select locations that are closest to prescribed points
    for i in range(marg_pos.shape[0]):
        pos = marg_pos[i,:]
        # Compute distance with repsect to coordinates
        dist = npla.norm(coord - pos, axis=1)
        # Find minimum index
        idx = np.argmin(dist)
        # Plot histogram
        fig = plt.figure()
        plt.hist(push_x[:,idx], bins=50, normed=True)
        plt.title("Posterior at x=" + str(pos))
        plt.show(False)
        save_fig(fig, 'marginal-x' + str(pos))

plot(x)