#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys
import getopt
import os
import os.path
import dill as pickle
import h5py
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

import TransportMaps as TM

def usage():
    usage_str = """
Usage: PlotPosteriorPredictive.py --fname=FNAME
"""
    print(usage_str)

argv = sys.argv[1:]
FNAME = None
NBINS = 50
PERCENTILES = [1, 5, 10, 90, 95, 99]
NSAMPS = -1
NPROCS = 1
BATCH_SIZE = 1
try:
    opts, args = getopt.getopt(
        argv,"",
        ["fname=",
         "nbins=",
         "percentiles=",
         "nsamps=",
         "nprocs=",
         "batch-size=",
     ])
except getopt.GetoptError:
    usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '--fname':
        FNAME = arg
    if opt == '--nbins':
        NBINS = int(arg)
    if opt == '--percentiles':
        PERCENTILES = [ int(s) for s in arg.split(',') ]
    if opt == '--nsamps':
        NSAMPS = int(arg)
    if opt == '--nprocs':
        NPROCS = int(arg)
    if opt == '--batch-size':
        BATCH_SIZE = int(arg)

with open( FNAME + '.dill', 'rb' ) as istr:
    stg = pickle.load(istr)

h5file = h5py.File(FNAME + '-post.dill.hdf5', 'r')

mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

try:
    # Load values
    smps = h5file['/quadrature/approx-target/0'][:NSAMPS,:]
    m = smps.shape[0]

    # Extract prior map and un-whiten the samples
    pmap = stg.target_distribution.prior_map
    smps = pmap.evaluate(smps)

    # Extract the solver and the boundary value conditions to be appended
    solver = stg.target_distribution.solver
    bvs = stg.target_distribution.logL.mismatch_map.bvs

    # Plot viscosity and initial values marginals
    nu_smps = smps[:,0]
    plt.figure()
    plt.hist(smps[:,0],
             bins=np.logspace(np.log10(min(nu_smps)),
                              np.log10(max(nu_smps)),
                              NBINS)
    )
    plt.gca().set_xscale('log')
    plt.show(False)

    u0_smps = np.hstack([ bvs[0]*np.ones((m,1)), smps[:,1:], bvs[1]*np.ones((m,1)) ])
    u0_qq = np.percentile( u0_smps, PERCENTILES, axis=0 )
    plt.figure()
    plt.plot(solver.coord, u0_qq.T, 'k', linewidth=.2)
    plt.show(False)

    # Solve Burgers equation for each sample
    def solve(nu=None, u0=None, solver=None):
        f = np.zeros((len(nu), solver.ndofs))
        for i in range(len(nu)):
            f[i,:] = solver.solve(nu[i], u0[i,:])
        return f
    
    bsize = BATCH_SIZE * NPROCS
    f = np.zeros((m, solver.ndofs))

    # Distribute solver
    TM.mpi_bcast_dmem(solver=solver, mpi_pool=mpi_pool)
    # Import numpy in children
    mpi_pool.mod_import([(None, 'numpy', 'np')])
    
    for i in tqdm(range(0,m,bsize)):
        end = min(m, i+bsize)
        
        u0 = np.zeros((end-i, solver.ndofs))
        u0[:,0] = bvs[0]
        u0[:,-1] = bvs[1]
        
        nu = smps[i:end,0]
        u0[:,1:-1] = smps[i:end,1:]

        # Solve
        f[i:end,:] = TM.mpi_map(
            solve,
            scatter_tuple=(['nu','u0'], [nu, u0]),
            dmem_key_in_list=['solver'],
            dmem_arg_in_list=['solver'],
            dmem_val_in_list=[solver],
            mpi_pool=mpi_pool
        )
        # f[i,:] = solver.solve(nu, u0)

    # Compute percentiles
    qq = np.percentile(f, PERCENTILES, axis=0)
    
    # Plot all the solutions
    plt.figure()
    plt.plot(solver.coord, qq.T, 'k', linewidth=.2)
    plt.show(False)
except Exception as e:
    raise e
finally:
    h5file.close()
    mpi_pool.stop()