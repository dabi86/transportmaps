import copy

import matplotlib.pyplot as plt
import matplotlib.pylab as pylab

import numpy as np
import numpy.random as npr

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Algorithms.Adaptivity as ALGADAPT
import TransportMaps.Diagnostics as DIAG
import src.TransportMaps.Maps as MAPS
import TransportMaps.Builders as BUILD
from TransportMaps.Distributions.Examples import FactorizedBananaDistribution

params = {'legend.fontsize': 'x-large',
         'axes.labelsize': 'x-large',
         'axes.titlesize':'x-large',
         'xtick.labelsize':'x-large',
         'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)
figfmt_list = ['svg', 'pdf', 'eps', 'png']

npr.seed(1)
TM.setLogLevel(20)
NPROCS = 2

mux = 0.5
sigma2x = 0.8
sigma2y = 0.2
pi = FactorizedBananaDistribution(
    mu0=mux, sigma0=np.sqrt(sigma2x), sigma1=np.sqrt(sigma2y))
pi_copy = copy.deepcopy(pi)

rho = DIST.StandardNormalDistribution(dim=2)
fig = DIAG.plotAlignedConditionals(
    pi_copy, do_diag=False, show_axis=False, numPointsXax=60)
for figfmt in figfmt_list:
    fig.savefig('figures/banana.' + figfmt)    

tm = Maps.assemble_IsotropicIntegratedSquaredDiagonalTransportMap(dim=2, order=1)

# solve_params = {
#     'qtype': 0,
#     'qparams': 1000
# }
# validator = DIAG.SampleAverageApproximationKLMinimizationValidator(
#     eps_sp_rel=1e-2, eps_sp_abs=1e-2)

solve_params = {
    'qtype': 0,
    'tol': 1e-3,
    'qparams': 1000# ,
    # 'batch_size': [1000,1000,1000]
}
validator = DIAG.GradientBootstrapKLMinimizationValidator(
    eps=1e-1, delta=5)

# solve_params = {
#     'qtype': 3,
#     'qparams': [6,6]
# }
# validator = None

builder = ALGADAPT.FirstVariationKullbackLeiblerBuilder(
    validator=validator,
    eps_bull=1e-2,
    regression_builder=BUILD.L2RegressionBuilder({'tol': 1e-5}),
    verbosity=2
)

mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

try:
    (tm, _) = builder.solve(
        transport_map = tm,
        base_distribution = rho,
        target_distribution = pi,
        solve_params = solve_params,
        mpi_pool=mpi_pool
    )
finally:
    if mpi_pool is not None:
        mpi_pool.stop()

n_coeffs = []
for i, tm in enumerate(builder.state.transport_map_list):
    n_coeffs.append(tm.n_coeffs)

    push_tm_rho = DIST.PushForwardTransportMapDistribution(tm, rho)
    fig = DIAG.plotAlignedConditionals(
        push_tm_rho, do_diag=False, show_axis=False, numPointsXax=60)
    for figfmt in figfmt_list:
        fig.savefig('figures/banana-push-step%d.' % i + figfmt)
    
    pull_tm_pi = DIST.PullBackTransportMapDistribution(tm, pi_copy)
    fig = DIAG.plotAlignedConditionals(
        pull_tm_pi, do_diag=False, show_axis=False, numPointsXax=60)
    for figfmt in figfmt_list:
        fig.savefig('figures/banana-pull-step%d.' % i + figfmt)

    fig = DIAG.plotGradXMap(tm, title=None, show_cbar=False, show_ticks=False)
    for figfmt in figfmt_list:
        fig.savefig('figures/banana-gradx-step%d.' % i + figfmt)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.semilogy(n_coeffs, builder.variance_diagnostic_list, 'o--k')
plt.show(False)
for figfmt in figfmt_list:
    fig.savefig('figures/banana-conv.' + figfmt)

# tm = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(dim=2, order=2)
# push_tm_rho = DIST.PushForwardTransportMapDistribution(tm, rho)
# solve_params = {'qtype': 3, 'qparams': [20,20], 'tol':1e-12}
# push_tm_rho.minimize_kl_divergence(pi, **solve_params)

# pull_tm_pi = DIST.PullBackTransportMapDistribution(tm, pi)
# (x, w) = rho.quadrature(**solve_params)
# gt = TM.grad_t_kl_divergence(x, rho, pull_tm_pi)

