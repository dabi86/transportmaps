PROBLEM SETTING
---------------

We want to compute \int h(x) f(x) dx over R^d where f(x) is an unnormalized
non-Gaussian density and h(x) is some function of interest that you can define.

We provide the evaluation of the following functions:
 x -> f(x)                  #Evaluation of f(x)
 x -> log_f(x)              #Evaluation of log(f(x))
 x -> grad_log_f(x)         #Evaluation of the gradient of log(f(x))
 x -> T(x)                  #Evaluation of the approximate transport map (which is monotone)
 x -> pull_f(x)             #Evaluation of the pullback of f under T

The approximate transport map T pushes forward a reference density, \eta, which is
a standard normal, to an approximation of the normalized version of f.

One possible transformation of the integral is then:
 \int h(x) f(x) dx = \int h(T(x)) ( \pull_f(x) / \eta(x) ) \eta(x) dx


USAGE
-----

The current folder contains data and loading script. In order to load the functions above execute,
for example, the following lines in python:

>>> import DataLoading as DL
>>> (d, f, log_f, grad_log_f, T, pull_f) = DL.load('data/TestCase-d15.dill')  # load 15 dimensional test case

In alternative you can load the 60 dimensional test case using:

>>> import DataLoading as DL
>>> (d, f, log_f, grad_log_f, T, pull_f) = DL.load('data/TestCase-d60.dill')  # load 60 dimensional test case

The inputs to the returned functions are described inside the script DataLoading.py, but also in the following.
Let d be the dimension of the density and m the number of points where to evaluate the functions.

  * f, log_f, pull_f: 
      input: (m,d) array
      output: (m) array
  * grad_log_f:
      input: (m,d) array 
      output: (m,d) array
  * T: 
      input: (m,d) array 
      output: (m,d) array

Note: if m==1, one MUST still provide a (1,d) array as input.

Example:

>>> import numpy.random as npr
>>> import DataLoading as DL
>>> (d, f, log_f, grad_log_f, T, pull_f) = DL.load('data/TestCase-d15.dill')
>>> x = npr.randn(10*d).reshape((d,10))
>>> Teval = T(x)
