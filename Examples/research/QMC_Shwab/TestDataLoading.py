import DataLoading as DL

fname_list = [ 'data/TestCase-d15.dill',
               'data/TestCase-d60.dill']

for fname in fname_list:
    print("Loading: %s" % fname)
    (d, f, log_f, grad_log_f, T, pull_f) = DL.load(fname)
    DL.test_load(d, f, log_f, grad_log_f, T, pull_f)