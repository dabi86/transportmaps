from __future__ import division
import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg
import math
import os.path
import dill
import TransportMaps.Densities as DENS
import TransportMaps as TM
import TransportMaps.Diagnostics as DIAG

class ConditionedDiffusionMarkovComponent(DENS.Density):
    def __init__(self, index_time, component_tmap, ref_density, dataObs, indexObs, deltat,
                 sigma):
        super(ConditionedDiffusionMarkovComponent,self).__init__(2)
        self.sigma  = sigma
        self.deltat = deltat
        self.dataObs = np.copy(dataObs)
        self.indexObs = np.copy(indexObs)
        self.index_time = index_time
        self.component_tmap = component_tmap
        self.ref_density = ref_density

    def pdf(self, X, params=None):
        return np.exp(self.log_pdf(X, params))
    def log_pdf(self, X, params=None):
        # X is a 2-d array of points. The first dimension corresponds to the number of points.
        index = self.index_time
        sigma = self.sigma
        dataObs = self.dataObs
        indexObs = self.indexObs
        deltat = self.deltat
        potential = self.potential
        X0 = X[:,0]
        X1 = X[:,1]
        if index == 0: #By convention the first  map is the identity
            T_X0 = X0
        else:
            T_X0 = self.component_tmap.evaluate(X0[:,np.newaxis])
        beta =  T_X0 + deltat*potential(T_X0)
        log_pdf = -.5*(X1-beta)**2/deltat
        if index == 0:
            log_pdf += -.5*X0**2/deltat
        else:
            log_pdf += self.ref_density.log_pdf(X0[:,np.newaxis])
        indexCurrentObs = np.where( indexObs == (index+1)  )[0]
        if len(indexCurrentObs)>0: #Add observation
            log_pdf += -.5*(dataObs[ indexCurrentObs[0] ] - X1)**2 /sigma**2
        return log_pdf

    def grad_x_log_pdf(self, X, params=None):
        index = self.index_time
        sigma = self.sigma
        dataObs = self.dataObs
        indexObs = self.indexObs
        deltat = self.deltat
        potential = self.potential
        d_potential = self.d_potential
        X0 = X[:,0]
        X1 = X[:,1]
        grad_x = np.zeros( X.shape )
        if index == 0: #By convention the first  map is the identity
            T_X0 = X0
            dT_X0 = 1.
        else:
            T_X0 = self.component_tmap.evaluate(X0[:,np.newaxis])
            dT_X0 = self.component_tmap.partial_xd(X0[:,np.newaxis])
        beta =  T_X0 + deltat*potential(T_X0)
        d_beta = dT_X0 + deltat*d_potential(T_X0)*dT_X0
        grad_x[:,0] = 1/deltat*(X1-beta)*d_beta
        grad_x[:,1] = - (X1 - beta)/deltat
        if index == 0:
            grad_x[:,0] += -X0/deltat
        else:
            grad_x[:,0] += self.ref_density.grad_x_log_pdf(X0[:,np.newaxis])[:,0]
        indexCurrentObs = np.where( indexObs == (index+1)  )[0]
        if len(indexCurrentObs)>0: #Add observation
            grad_x[:,1] +=  (dataObs[ indexCurrentObs[0] ] - X1) /sigma**2
        return grad_x

    def hess_x_log_pdf(self, X, params=None):
        index = self.index_time
        sigma = self.sigma
        dataObs = self.dataObs
        indexObs = self.indexObs
        deltat = self.deltat
        potential = self.potential
        d_potential = self.d_potential
        d2_potential = self.d2_potential
        X0 = X[:,0]
        X1 = X[:,1]
        hess_x = np.zeros( (X.shape[0], self.dim, self.dim) )
        if index == 0: #By convention the first  map is the identity
            T_X0 = X0
            dT_X0 = 1.
            d2T_X0 = 0.
        else:
            T_X0 = self.component_tmap.evaluate(X0[:,np.newaxis])
            dT_X0 = self.component_tmap.partial_xd(X0[:,np.newaxis])
            d2T_X0 = self.component_tmap.partial2_xd(X0[:,np.newaxis])
        beta =  T_X0 + deltat*potential(T_X0)
        d_beta = dT_X0 + deltat*d_potential(T_X0)*dT_X0
        d2_beta = d2T_X0 + deltat*d2_potential(T_X0)*(dT_X0**2) + \
                  deltat*d_potential(T_X0)*d2T_X0
        hess_x[:,0,0] =   ( -d_beta**2 + (X1-beta)*d2_beta )/deltat
        hess_x[:,0,1] = d_beta/deltat
        hess_x[:,1,0] = hess_x[:,0,1]
        hess_x[:,1,1] =  -1./deltat
        if index == 0:
            hess_x[:,0,0] += -1./deltat
        else:
            hess_x[:,0,0] += self.ref_density.hess_x_log_pdf(X0[:,np.newaxis])[:,0,0]
        indexCurrentObs = np.where( indexObs == (index+1)  )[0]
        if len(indexCurrentObs)>0: #Add observation
            hess_x[:,1,1] +=   -1. /sigma**2
        return hess_x
        
class ConditionedDiffusion(DENS.Density):
    def __init__(self, tvec_proc, Xt, nsteps, indexObs_proc, obs, sigma, c0):
        super(ConditionedDiffusion,self).__init__(nsteps)
        self.time_vec_proc = tvec_proc
        skip = len(tvec_proc) // nsteps
        self.idx_down_samp = range(skip-1,len(tvec_proc),skip)
        tmp = []
        for i in indexObs_proc:
            if i in self.idx_down_samp:
                tmp.append(True)
            else:
                tmp.append(False)
        if not all(tmp):
            raise ValueError("Observations are missed")
        self.time_vec = self.time_vec_proc[self.idx_down_samp]
        self.deltat = self.time_vec[1] - self.time_vec[0]
        self.Xt = Xt
        self.indexObs_proc = indexObs_proc
        self.indexObs = []
        for i,idx in enumerate(self.idx_down_samp):
            if idx in indexObs_proc:
                self.indexObs.append(i)
        self.dataObs = obs
        self.sigma = sigma
        self.c0 = c0
            
    def potential(self, x):
        return self.c0*x*(1-x**2)/(1+x**2)
    def d_potential(self, x):
        return -self.c0*(x**4+4*x**2-1)/(1+x**2)**2
    def d2_potential(self, x):
        return self.c0*4*x*(x**2-3)/(1+x**2)**3

    def get_MarkovFactor(self,index_time,component_tmap,ref_density):
        return ConditionedDiffusionMarkovComponent(
            index_time, component_tmap, ref_density,
            self.dataObs, self.indexObs, self.deltat,
            self.sigma)

    def pdf(self, X, params=None):
        return np.exp(self.log_pdf(X, params))
    
    def log_pdf(self, X, params=None):
        # X is a 2-d array of points.
        # The first dimension corresponds to the number of points.
        deltat = self.deltat
        sigma = self.sigma
        potential = self.potential
        dataObs = self.dataObs
        indexObs = self.indexObs
        logpdf  =  np.sum( -.5*( X[:,1:] - X[:,0:-1] -
                                 deltat*potential(X[:,0:-1]) )**2/deltat , axis = 1)
        logpdf +=  -.5*X[:,0]**2/deltat
        logpdf +=  -.5* np.sum( (dataObs - X[:,indexObs])**2 , axis = 1 )/sigma**2
        return logpdf

    def grad_x_log_pdf(self, X, params=None):
        deltat = self.deltat
        sigma = self.sigma
        potential = self.potential
        d_potential = self.d_potential
        dataObs = self.dataObs
        indexObs = self.indexObs
        gamma = X + deltat*potential(X)
        d_gamma = 1 + deltat*d_potential(X)
        grad = np.zeros( X.shape )
        grad[:,0] = ( d_gamma[:,0]*X[:,1]-X[:,0]-gamma[:,0]*d_gamma[:,0] )/deltat
        grad[:,1:-1] = ( d_gamma[:,1:-1]*X[:,2:] - X[:,1:-1] -
                         gamma[:,1:-1]*d_gamma[:,1:-1] + gamma[:,0:-2] )/deltat
        grad[:,-1] = ( gamma[:,-2] - X[:,-1] )/deltat
        grad[:,indexObs] += (dataObs-X[:,indexObs])/sigma**2
        return grad

    def hess_x_log_pdf(self, X, params=None):
        deltat = self.deltat
        sigma = self.sigma
        potential = self.potential
        d_potential = self.d_potential
        d2_potential = self.d2_potential
        dataObs = self.dataObs
        indexObs = self.indexObs
        gamma = X + deltat*potential(X)
        d_gamma = 1 + deltat*d_potential(X)
        d2_gamma =   deltat*d2_potential(X)

        npoints, dim = X.shape
        Hess_X = np.zeros( (npoints, dim,  dim) )

        for kk in np.arange(npoints):
            #Main diagonal
            diagView = np.diagonal( Hess_X[ kk , :, :] )
            diagView.setflags(write=True)
            diagView[:-1] = ( d2_gamma[kk,:-1]*X[kk,1:] - 1 -
                              d_gamma[kk,:-1]**2 -
                              d2_gamma[kk,:-1]*gamma[kk,:-1] )/deltat
            diagView[-1] = -1/deltat
            diagView[indexObs] += -1/sigma**2
            #Superdiagonal
            diagSup = np.diagonal( Hess_X[ kk , :, :] , 1)
            diagSup.setflags(write=True)
            diagSup[:] = d_gamma[kk,:-1]/deltat
            #Subdiagonal
            diagSub = np.diagonal( Hess_X[ kk , :, :] , -1)
            diagSub.setflags(write=True)
            diagSub[:] = d_gamma[kk,:-1]/deltat
        return Hess_X

    def store(self, fname):
        if os.path.exists(fname):
            if sys.version_info[0] == 3:
                sel = input("The file %s already exists. " + \
                            "Do you want to overwrite? [y/N] " % fname)
            else:
                sel = raw_input("The file %s already exists. " + \
                                "Do you want to overwrite? [y/N] " % fname)
            if sel != 'y' and sel != 'Y':
                print("Not storing")
                return
        with open(fname, 'wb') as out_stream:
            dill.dump(self, out_stream)

def generateData(nsteps=100, tfinal=10., sigma=0.1, c0=10., potential=None,
                 indexObs=None, nobs=20, x0=0.):
    if indexObs is None:
        skip = nsteps // nobs
        indexObs = np.arange(skip-1, nsteps, skip)
    elif np.max(indexObs) >= nsteps:
        raise ValueError("The maximum observation index exceed the number of steps.")
    if potential is None:
        potential = lambda x: c0 * x * (1-x**2)/(1+x**2)
    # Allocate variables
    tvec = np.linspace(0, tfinal, nsteps)
    dt = tvec[1] - tvec[0]
    sq_noise_dyn = np.sqrt(dt) * np.random.randn(nsteps)
    noise_obs = sigma * np.random.randn(nobs)
    Xt = np.zeros(nsteps+1)
    Xt[0] = x0
    # Simulate
    for i in range(nsteps):
        Xt[i+1] = Xt[i] + dt * potential(Xt[i]) + sq_noise_dyn[i]
    Xt = Xt[1:]
    obs = Xt[indexObs] + noise_obs
    return (tvec, Xt, indexObs, obs)



################ Finite differences routines #######################

def finiteDifference_gradient_X( fun , grad , X , delta = 1e-6, rtol=1e-6):
        # X is a 2-d array of points. The first dimension corresponds to the number of points.
        npoints = X.shape[0]
        ndim = X.shape[1]
        grad_X = grad(X)
        dF_X = np.zeros( (npoints, ndim) )
        for kk in np.arange(ndim):
            Xp = np.copy(X)
            Xp[:,kk] += delta
            Xm = np.copy(X)
            Xm[:,kk] -= delta
            Fp = fun(Xp)
            Fm = fun(Xm)
            dF_X[:,kk] = (Fp-Fm)/(2*delta)
        rerror_vec = np.linalg.norm(grad_X - dF_X, axis = 0)/np.linalg.norm(grad_X)
        aerror_vec = np.linalg.norm(grad_X - dF_X, axis = 0)
        maxrErr =  np.max(rerror_vec)
        maxraErr =  np.max(aerror_vec)
        iscorrect =  maxrErr <= rtol
        return iscorrect, maxrErr, maxraErr

def finiteDifference_hessian_X( fun , hess , X , delta = 1e-6, rtol=1e-6):
        # X is a 2-d array of points. The first dimension corresponds to the number of points.
        npoints = X.shape[0]
        ndim = X.shape[1]
        hess_X = hess(X)
        d2F_X = np.zeros( (npoints, ndim, ndim) )
        for kk in np.arange(ndim):
            Xp = np.copy(X)
            Xp[:,kk] += delta
            Xm = np.copy(X)
            Xm[:,kk] -= delta
            Fp = fun(Xp)
            Fm = fun(Xm)
            d2F_X[:,:,kk] = (Fp-Fm)/(2*delta)
        rerror_vec = np.zeros(npoints)
        for kk in np.arange(npoints):
            rerror_vec[kk] = np.linalg.norm( d2F_X[kk,:,:]-hess_X[kk,:,:])/np.linalg.norm(hess_X[kk,:,:])
            maxrErr =  np.max(rerror_vec)
        iscorrect =  maxrErr <= rtol
        return iscorrect, maxrErr

def finiteDifference1D_gradient_X( fun , grad , X , delta = 1e-6, rtol=1e-6):
        # X is a 1-d array of points.
        npoints = len(X)
        grad_X = grad(X)
        dF_X = np.zeros(  npoints   )
        Xp = np.copy(X)
        Xp += delta
        Xm = np.copy(X)
        Xm -= delta
        Fp = fun(Xp)
        Fm = fun(Xm)
        dF_X = (Fp-Fm)/(2*delta)
        rerror_vec = np.linalg.norm(grad_X - dF_X)/np.linalg.norm(grad_X)
        aerror_vec = np.linalg.norm(grad_X - dF_X)
        maxrErr =  np.max(rerror_vec)
        maxraErr =  np.max(aerror_vec)
        iscorrect =  maxrErr <= rtol
        return iscorrect, maxrErr, maxraErr

    # #Check gradient
    # fun = svDens.log_pdf
    # grad = svDens.grad_x_log_pdf
    # X = np.random.rand( 10, ndim  )
    # finiteDifference_gradient_X( fun , grad , X )

    # #Check hessian
    # fun = svDens.grad_x_log_pdf
    # hess = svDens.hess_x_log_pdf
    # X = np.random.rand( 8, ndim  )
    # finiteDifference_hessian_X( fun , hess , X )


###########################################
# numsteps = 1000
# conddiff = CondDiff(numsteps, numObs=43)

# fig = DIAG.nicePlot( conddiff.time_vec  , conddiff.dataDynamic, xlabel="Time", title="Conditioned diffusion", xtickslabel = 0, linewidth=2,  color = 'b',  show_flag=True, figname = 'testfig')
# ax = plt.gca()
# ax.plot(conddiff.time_vec[conddiff.indexObs] , conddiff.dataObs,'ro')
# plt.show()

# laplace_approx = TM.laplace_approximation( conddiff )
# #Check gradient
# fun = conddiff.log_pdf
# grad = conddiff.grad_x_log_pdf
# X = laplace_approx.mu +1e-2*np.random.rand( 50, numsteps  )
# iscorrect, maxrErr, maxraErr = finiteDifference_gradient_X( fun , grad , X )
# print 'Gradient'
# print iscorrect, maxrErr, maxraErr

# fun = conddiff.grad_x_log_pdf
# hess = conddiff.hess_x_log_pdf
# iscorrect, maxrErr = finiteDifference_hessian_X( fun , hess , X )
# print 'Hessian'
# print iscorrect, maxrErr
