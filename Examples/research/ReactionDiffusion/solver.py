import matplotlib.pyplot as plt
import numpy as np
import dolfin as dol

T      = 1.0        # final time
nsteps = 100        # number of time steps
nels   = 20
dt = T / nsteps

mesh = dol.UnitIntervalMesh(nels)

VE = dol.FiniteElement("Lagrange", dol.interval, 1)
MVE = dol.MixedElement([VE, VE])
MVEFS = dol.FunctionSpace(mesh, MVE)

# Test functions
ut, vt = dol.TestFunctions(MVEFS)

# Functions
uv = dol.Function(MVEFS)
uv_n = dol.Function(MVEFS)

# Split
u, v = dol.split(uv)
u_n, v_n = dol.split(uv_n)

# Define reaction terms
a = dol.Constant(.5)
b = dol.Constant(.5)
c = dol.Constant(.3)
d = dol.Constant(.7)

# Define the initial conditions
uv0 = dol.Expression(
    (
        'x[0]<=.5 ? - 16. * pow(x[0],2) + 8. * x[0] : 0.',
        'x[0]>=.5 ? - 16. * pow(x[0],2) + 24. * x[0] - 8. * x[0] : 0.'
    ),
    degree=1)

# Define boundary conditions
def boundary(x):
    return x[0] < dol.DOLFIN_EPS or x[0] > 1. - dol.DOLFIN_EPS
bcs = [
    dol.DirichletBC(MVEFS.sub(i), dol.Constant(0.), boundary)
    for i in range(2)
]

# Define the variational problem
k = dol.Constant(dt)
F = ( (u-u_n)/k * ut + dol.dot(dol.grad(u), dol.grad(ut)) ) * dol.dx + \
    ( (v-v_n)/k * vt + dol.dot(dol.grad(v), dol.grad(vt)) ) * dol.dx + \
    a * u * ut * dol.dx + b * v * ut * dol.dx + \
    c * v * vt * dol.dx + d * u * vt * dol.dx

# # Output file
# out_file = dol.File('output.pvd', 'compressed')

# Output arrays
uout = np.zeros((nsteps+1,nels+1))
vout = np.zeros((nsteps+1,nels+1))

# Time-stepping
uv.interpolate(uv0)
uout[0,:] = uv.vector()[::2]
vout[0,:] = uv.vector()[1::2]
t = 0
for n in range(nsteps):
    t += dt
    uv_n.vector()[:] = uv.vector()
    dol.solve( F == 0, uv, bcs )
    uout[n+1,:] = uv.vector()[::2]
    vout[n+1,:] = uv.vector()[1::2]
    # out_file << (uv, t)

plt.figure()
plt.imshow(uout)
plt.colorbar()

plt.figure()
plt.imshow(vout)
plt.colorbar()

plt.show(False)
