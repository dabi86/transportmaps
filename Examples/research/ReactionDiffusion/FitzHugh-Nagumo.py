import matplotlib.pyplot as plt
import numpy as np
import dolfin as dol

T      = 1.0        # final time
nsteps = 100        # number of time steps
nels   = 20
dt = T / nsteps

mesh = dol.UnitSquareMesh(nels,nels)

VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
# VEFS = dol.FunctionSpace(mesh, VE)
MVE = dol.MixedElement([VE, VE])
MVEFS = dol.FunctionSpace(mesh, MVE)

# Test functions
ut, vt = dol.TestFunctions(MVEFS)

# Functions
uv = dol.Function(MVEFS)
uv_p = dol.Function(MVEFS)

# Split
u, v = dol.split(uv)
u_p, v_p = dol.split(uv_p)

# Define constants
mu    = dol.Constant(1.)
alpha = dol.Constant(1.)
kappa = dol.Constant(.01)
eps   = dol.Constant(.01)
theta = dol.Constant(1.)
gamma = dol.Constant(1.)

# Define initial conditions
uv0 = dol.Constant((0.,0.))

# Define current
I = dol.Expression(
    'sin(atan2(x[0],x[1]))', degree=1
)

# Define form
k = dol.Constant(dt)
F = ( u - u_p ) / k * dol.dx \
    + dol.dot( mu * dol.grad(u), dol.grad(ut) ) * dol.dx \
    - u * (u - alpha) * (1 - u) * ut * dol.dx \
    + v * ut * dol.dx + \
    ( v - v_p ) / k * dol.dx \
    + dol.dot( kappa * dol.grad(v), dol.grad(vt) ) * dol.dx \
    - eps * (theta * u - gamma * v) * vt * dol.dx \
    - I * ut * dol.ds

# Output arrays
uout = np.zeros((nsteps+1, (nels+1)**2))
vout = np.zeros((nsteps+1, (nels+1)**2))

# Time-stepping
uv.interpolate(uv0)
# uout[0,:] = uv.vector()[:][::2]
# vout[0,:] = uv.vector()[:][1::2]
t = 0
for n in range(nsteps):
    t += dt
    uv_p.vector()[:] = uv.vector()
    dol.solve( F == 0, uv )
    # uout[n+1,:] = uv.vector()[:][::2]
    # vout[n+1,:] = uv.vector()[:][1::2]

