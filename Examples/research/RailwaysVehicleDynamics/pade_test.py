import numpy as np
import scipy.linalg as scila
import scipy.integrate as scint
import matplotlib.pyplot as plt

# Let \dot{y}(t) = A y(t) + G z(t)
# with intial conditions y(0) = 1 and final time T=0.2
# And z(t)=0.5 over the interval [0,T]
A = np.array([[-0.7, 0.4],[0.4, -0.5]])
G = np.array([[0.],[-0.2]])
y0 = np.ones((2,1))
T = 10.
z = 0.5

# Let us integrate with Euler
N = 1000
dt = T/N
y = [ y0.copy() ]
t = np.linspace(0.,T, N+1)
for i in range(N):
    y.append( y[i] + dt * (np.dot(A,y[i]) + np.dot(G, z)) )
y = np.asarray(y)
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(t, y[:,0,0])
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(t, y[:,1,0])
plt.show(False)

# Let us use the matrix exponential formulas
Phi = scila.expm(T * A)
def fgamma(t):
    return np.dot(scila.expm(t * A), G)
int_eval = np.zeros((N+1,)+G.shape)
for i in range(N+1):
    int_eval[i,:,:] = np.dot(scila.expm(t[i] * A), G)
int_eval = (int_eval[:-1,:,:] + int_eval[1:,:,:]) / 2.
Gamma = np.sum( dt * int_eval, axis=0 )
yT = np.dot(Phi, y0) + np.dot(Gamma, z)