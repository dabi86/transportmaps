import numpy as np

# Position of degrees of freedom
NDOFS  = 0
Yw1    = NDOFS; NDOFS += 1;
Yw1dot = NDOFS; NDOFS += 1;
Pw1    = NDOFS; NDOFS += 1;
Pw1dot = NDOFS; NDOFS += 1;
Yw2    = NDOFS; NDOFS += 1;
Yw2dot = NDOFS; NDOFS += 1;
Pw2    = NDOFS; NDOFS += 1;
Pw2dot = NDOFS; NDOFS += 1;
Yb     = NDOFS; NDOFS += 1;
Ybdot  = NDOFS; NDOFS += 1;
Pb     = NDOFS; NDOFS += 1;
Pbdot  = NDOFS; NDOFS += 1;
Yc     = NDOFS; NDOFS += 1;
Ycdot  = NDOFS; NDOFS += 1;
# Ysr    = NDOFS; NDOFS += 1;
D1     = NDOFS; NDOFS += 1;
D2     = NDOFS; NDOFS += 1;

class Coradia175Vehicle(object):
    r"""
    Args:
      v (float): Longitudinal vehicle velocity (m/s)
    """
    mw     = 1530.     # Wheel set mass (kg)
    Iw     = 1017.     # Wheel set yaw inertia (kg * m^2)
    mb     = 2698.     # Bogie mass (kg)
    Ib     = 2138.     # Bogie yaw inertia (kg * m^2)
    mc     = 37637./2  # Half vehicle mass (kg)
    a      = 1.25      # Semi wheel-wheel spacing (m)
    b      = 1.02      # Lateral semi-spacing of radial arm bushes (m)
    c      = 0.47      # Long. offset of lateral damper from bogie center (m)
    d      = 0.8       # Longitudinal semi-spacing of radial arm bushes (m)
    e      = 0.26      # Anti-roll bar longitudinal offset from bogie center (m)
    l      = 0.75      # Half gauge (m)
    lbw    = 1.20      # Lateral position of bogie end of anti-yaw damper from bogie center (m)
    lmb    = 0.15      # Conicity
    r0     = 0.42      # Wheelset radius (m)
    
    def __init__(self, v):
        self.L_Ky   = (2*554.)*10**3  # Leading primary spring lateral (kN/m)
        self.T_Ky   = (2*554.)*10**3  # Trailing primary spring lateral (kN/m)
        self.L_Kray = 2*11500.*10**3  # Leading primary radial bush lateral (kN/m)
        self.T_Kray = 2*11500.*10**3  # Trailing primary radial bush lateral (kN/m)
        self.L_Kx   = 2*23554.*10**3  # Leading primary longitudinal (kN/m)
        self.T_Kx   = 2*23554.*10**3  # Trailing primary longitudinal (kN/m)
        self.Ksy    = (2*88927.+197.)*10**3 # Secondary airbag + traction center
                                      # lateral stiffness (kN/m)
        self.Ksry   = 10000.*10**3    # Secondary lateral series stiffness (kN/m)
        self.Ksarb  = 83.*10**3       # Secondary anti-roll bar lateral stiff (kN/m)
        self.Ksyaw  = 10.*10**3       # Secondary anti-roll bar + traction center
                                      # yaw stiffness (kN * m/rad)
        self.Csy    = 40.*10**3       # Secondary lateral damping (kN * s/m)
        self.Csay   = 2*2057.*10**3   # Secondary anti-yaw damping (kN * s/m)
        self.f11    = 10.*10**6       # Longitudinal creep coeff (MN)
        self.f22    = 8.8*10**6       # Lateral creep coeff (MN)

        # Elaborate input
        self.v      = v         # Vehicle forward velocity (m/s)
        # if (self.a/track_spacing)%1 == 0.:
        #     skip = int(self.a / track_spacing)
        #     self.L_track_ydot = track_ydot[skip:]
        #     self.T_track_ydot = track_ydot[:-skip]

        # Set up stiffness/mass matrix A and control matrix G
        self.A = None
        self.G = None
        self.set_matrices()
            
    def set_matrices(self):
        ###################
        # System dynamics #
        ###################
        self.A = np.zeros((NDOFS,NDOFS))
        # Lateral displacement front wheelset
        self.A[Yw1,Yw1dot]    = 1.
        self.A[Yw1dot,Yw1]    = - (self.L_Ky + self.L_Kray)
        self.A[Yw1dot,Yw1dot] = - 2. * self.f22 / self.v
        self.A[Yw1dot,Pw1]    = 2. * self.f22 + self.L_Kray * (self.a - self.d)
        # self.A[Yw1dot,Yb]     = self.L_Ky + self.L_Kray
        # self.A[Yw1dot,Pb]     = self.a * self.L_Ky + self.d * self.L_Kray
        self.A[Yw1dot,:]     /= self.mw
        # Yaw front wheelset
        self.A[Pw1,Pw1dot]    = 1.
        self.A[Pw1dot,Yw1]    = self.L_Kray * (self.a - self.d)
        self.A[Pw1dot,Pw1dot] = - 2. * self.f11 * self.l**2 / self.v
        self.A[Pw1dot,Pw1]    = - self.L_Kx * self.b**2 - self.L_Kray * (self.a - self.d)**2
        # self.A[Pw1dot,Yb]     = - self.L_Kray * (self.a - self.d)
        # self.A[Pw1dot,Pb]     = self.L_Kx * self.b*2 \
        #                         - self.L_Kray * (self.a - self.d) * self.d
        self.A[Pw1dot,D1]     = - 2. * self.f11 * self.lmb * self.l / self.r0
        self.A[Pw1dot,:]     /= self.Iw
        # Lateral displacement trailing wheelset
        self.A[Yw2,Yw2dot]    = 1.
        self.A[Yw2dot,Yw2]    = - (self.T_Ky + self.T_Kray)
        self.A[Yw2dot,Yw2dot] = - 2. * self.f22 / self.v
        self.A[Yw2dot,Pw2]    = 2. * self.f22 - self.T_Kray * (self.a - self.d)
        # self.A[Yw2dot,Yb]     = self.T_Ky + self.T_Kray
        # self.A[Yw2dot,Pb]     = - (self.a * self.T_Ky + self.d * self.T_Kray)
        self.A[Yw2dot,:]     /= self.mw
        # Yaw trailing wheelset
        self.A[Pw2,Pw2dot]    = 1.
        self.A[Pw2dot,Yw2]    = - self.T_Kray * (self.a - self.d)
        self.A[Pw2dot,Pw2dot] = - 2. * self.f11 * self.l**2 / self.v
        self.A[Pw2dot,Pw2]    = - self.T_Kx * self.b**2 - self.T_Kray * (self.a - self.d)**2
        # self.A[Pw2dot,Yb]     = self.T_Kray * (self.a - self.d)
        # self.A[Pw2dot,Pb]     = self.T_Kx * self.b*2 \
        #                         - self.T_Kray * (self.a - self.d) * self.d
        self.A[Pw2dot,D2]     = - 2. * self.f11 * self.lmb * self.l / self.r0
        self.A[Pw2dot,:]     /= self.Iw
        # # Lateral displacement bogie frame
        # self.A[Yb,Ybdot]      = 1.
        # self.A[Ybdot,Yw1]     = self.L_Ky + self.L_Kray
        # self.A[Ybdot,Pw1]     = - self.L_Kray * (self.a - self.d)
        # self.A[Ybdot,Yw2]     = self.T_Ky + self.T_Kray
        # self.A[Ybdot,Pw2]     = self.T_Kray * (self.a - self.d)
        # self.A[Ybdot,Yb]      = - (self.L_Ky + self.L_Kray + self.T_Ky + self.T_Kray) \
        #                         - self.Ksarb - self.Ksy - self.Ksry
        # self.A[Ybdot,Pb]      = self.e * self.Ksarb - self.c * self.Ksry 
        # # self.A[Ybdot,Yc]      = self.Ksy + self.Ksarb
        # # self.A[Ybdot,Ysr]     = self.Ksry
        # self.A[Ybdot,:]      /= self.mb
        # # Yaw bogie frame
        # self.A[Pb,Pbdot]      = 1.
        # self.A[Pbdot,Yw1]     = self.a * self.L_Ky + self.d * self.L_Kray
        # self.A[Pbdot,Pw1]     = self.L_Kx * self.b**2 \
        #                         - self.L_Kray * (self.a - self.d) * self.d
        # self.A[Pbdot,Yw2]     = - (self.a * self.T_Ky + self.d * self.T_Kray)
        # self.A[Pbdot,Pw2]     = self.T_Kx * self.b**2 - self.T_Kray * (self.a - self.d) * self.d
        # self.A[Pbdot,Yb]      = self.e * self.Ksarb - self.c * self.Ksry
        # self.A[Pbdot,Pbdot]   = - self.Csay * self.lbw**2
        # self.A[Pbdot,Pb]      = - (self.L_Kx + self.T_Kx) * self.b**2 \
        #                         - (self.L_Ky + self.T_Ky) * self.a**2 \
        #                         - (self.L_Kray + self.T_Kray) * self.d**2 \
        #                         - (self.Ksry * self.c**2 + self.Ksarb * self.e**2) \
        #                         - self.Ksyaw
        # # self.A[Pbdot,Yc]      = - self.Ksarb * self.e
        # # self.A[Pbdot,Ysr]     = self.Ksry * self.c
        # self.A[Pbdot,:]      /= self.Ib
        # # Lateral displacement car body (MODIFIED!)
        # self.A[Yc,Ycdot]      = 1.
        # self.A[Ycdot,Yb]      = self.Ksy + self.Ksarb # self.Ksy + self.Ksry + self.Ksarb
        # self.A[Ycdot,Pb]      = - self.e * self.Ksarb # self.c * self.Ksry - self.e * self.Ksarb
        # self.A[Ycdot,Yc]      = - (self.Ksy + self.Ksarb)
        # self.A[Ycdot,Ysr]     = - self.Ksry
        # self.A[Ycdot,:]      /= self.mc
        # # Lateral displacement secondary lateral serial spring-dumper
        # self.A[Ysr,Yb]        = self.Ksry
        # self.A[Ysr,Pb]        = self.Ksry * self.c
        # self.A[Ysr,Ycdot]     = self.Csy
        # self.A[Ysr,Ysr]       = - self.Ksry
        # self.A[Ysr,:]        /= self.Csy
        # Lateral track displacement (leading wheelset)
        self.A[D1,Yw1dot]     = 1.
        self.A[D2,Yw2dot]     = 1.

        
        #################
        # Track control #
        #################
        self.G = np.zeros((NDOFS,2))
        self.G[D1,0] = -1.
        self.G[D2,1] = -1.

    def get_matrices(self):
        return (self.A, self.G)

if __name__ == "__main__":
    import matplotlib.pyplot as plt
    from matplotlib import animation
    import scipy.linalg as scila
    import scipy.integrate as scint
    # import scipy.integrate.
    
    v = 40.
    vehicle = Coradia175Vehicle(v)
    (A, G) = vehicle.get_matrices()

    plt.figure()
    plt.spy(A)
    plt.show(False)

    vals, vecs = scila.eig(A)
    stb = vals <= 0. # Stable modes
    plt.figure()
    plt.scatter(vals[stb].real, vals[stb].imag)
    plt.scatter(vals[np.logical_not(stb)].real, vals[np.logical_not(stb)].imag, c='r')
    plt.show(False)

    # Check stability on straight track
    nvs = 91
    vs = np.linspace(10., 200., nvs)
    fig = plt.figure()
    ax = plt.axes(xlim=(-100, 10), ylim=(-400, 400))
    ax.grid(True)
    sc1 = ax.scatter([], [], s=5)
    sc2 = ax.scatter([], [], c='r', s=5)
    ttl = ax.text(-95, 320, '', size=10)
    def init():
        empty = np.zeros((0,2))
        ttl.set_text('')
        sc1.set_offsets(empty)
        sc2.set_offsets(empty)
        return ttl, sc1, sc2
    def animate(i):
        vehicle = Coradia175Vehicle(vs[i])
        (A, G) = vehicle.get_matrices()
        vals, vecs = scila.eig(A)
        stb = vals.real < 1e-12 # Stable modes
        stb_vals = np.vstack((vals[stb].real, vals[stb].imag)).T
        ustb_vals = np.vstack((vals[np.logical_not(stb)].real,
                               vals[np.logical_not(stb)].imag)).T
        ttl.set_text('$v$=%.1f' % vs[i])
        sc1.set_offsets(stb_vals)
        sc2.set_offsets(ustb_vals)
        return ttl, sc1, sc2

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=nvs, interval=250, blit=True, repeat=False)
    plt.show(False)

    # Define right hand side function
    def f(y, t, A, G, z):
        return np.dot(A, y) + np.dot(G, z(t))
    
    # Solve for v=45. on straight track with initial disturbance
    # Straight track
    def z(t): 
        return np.zeros(2)
    # Initial conditions
    y0 = np.zeros(NDOFS)
    y0[Yw1] = 0.01
    y0[D1] = y0[Yw1]
    y0[Yw2] = 0.01
    y0[D2] = y0[Yw2]
    # Vehicle
    v = 10.
    vehicle = Coradia175Vehicle(v)
    (A, G) = vehicle.get_matrices()
    # Time discretization
    Nsteps = 101
    Tf = 5.
    T = np.linspace(0., Tf, Nsteps)
    dt = T[1]-T[0]
    # Solve
    sol = scint.odeint(f, y0, T, args=(A, G, z))
    # Plot
    plt.figure(figsize=(12,6))
    plt.subplot(241)
    plt.plot(T, sol[:,Yw1])
    plt.subplot(242)
    plt.plot(T, sol[:,Yw2])
    # plt.subplot(243)
    # plt.plot(T, sol[:,Yb])
    # plt.subplot(244)
    # plt.plot(T, sol[:,Yc])
    plt.subplot(245)
    plt.plot(T, sol[:,Pw1])
    plt.subplot(246)
    plt.plot(T, sol[:,Pw2])
    # plt.subplot(247)
    # plt.plot(T, sol[:,Pb])
    plt.show(False)