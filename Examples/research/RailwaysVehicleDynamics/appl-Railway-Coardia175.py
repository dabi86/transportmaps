
# coding: utf-8

# In[1]:

import logging
import numpy as np
import TransportMaps as TM
from TransportMaps.Distributions.Examples.RailwayVehicleDynamics import Coradia175 as C175
from TransportMaps.Algorithms import SequentialInference as ALGSI
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
import src.TransportMaps.Maps as MAPS
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams['font.size'] = 15

TM.setLogLevel(logging.DEBUG)

# # Coradia 175 vehicle

v = 42.5
nsteps = 100
observables = [C175.Ybdot, C175.Pb, C175.Ycdot]
vehicle = C175.Coradia175Vehicle(
    v, observables=observables)

# In[2]:

dofs_idxs = [0, 4, 8, 12, 2, 6, 10]
dofs_names = [r"$Y_{w_1}$", r"$Y_{w_2}$", r"$Y_{b}$", r"$Y_{c}$", r"$\Psi_{w_1}$", r"$\Psi_{w_2}$", r"$\Psi_{b}$"]
def plot_dynamic(T,Z,i,name):
    plt.subplot(2,4,i+1); plt.plot(T,Z); plt.ylabel(name); plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0));
def plot_dynamics(T,Z):
    plt.figure(figsize=(15,7))
    for i, (idx,name) in enumerate(zip(dofs_idxs, dofs_names)):
        plot_dynamic(T,Z[:,idx],i,name)
    plt.tight_layout()
# obs_names = [r"$\ddot{Y}_{w_1}$", r"$\ddot{Y}_{w_2}$", r"$\ddot{Y}_{b}$", r"$\dot{\Psi}_{b}$", r"$\ddot{Y}_{c}$"]
# obs_pos = [1, 2, 3, 7, 4]
obs_names = [r"$\ddot{Y}_{b}$", r"$\dot{\Psi}_{b}$", r"$\ddot{Y}_{c}$"]
obs_pos = [3, 7, 4]
def plot_data(T,Y):
    plt.figure(figsize=(15,5))
    yy = np.asarray(Y)
    for i, (pos,name) in enumerate(zip(obs_pos,obs_names)):
        plt.subplot(2,4,pos); 
        plt.plot(T,yy[:,i],'.'); 
        plt.ylabel(name);
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0));
    plt.tight_layout()


# In[3]:

T,Z,Y = C175.generate_data(nsteps, vehicle)
plot_dynamics(T,Z); plot_data(T,Y)

# ## Kalman filter

# In[4]:

par_name_list = []
par_val_list = np.zeros(0)
pi_prior = C175.StateSpacePrior(vehicle, par_name_list, par_val_list)
pi_trans = C175.StateSpaceTransition(vehicle, par_name_list, par_val_list)
FLT = ALGSI.LinearFilter()
for n in range(nsteps+1):
    # Define log-likelihood
    if Y[n] is None: # Missing data
        ll = None
    else: 
        ll = C175.StateSpaceLogLikelihood(
            Y[n], vehicle, par_name_list, par_val_list)
    # Define transition / prior
    if n > 0: 
        pin = pi_trans
    else:
        pin = pi_prior
    # Assimilation
    FLT.assimilate(pin, ll)


# In[5]:

def plot_dynamic_vs_track(T,Z,M,C,i,name):
    plt.subplot(2,4,i+1); 
    plt.plot(T,Z); 
    plt.plot(T,M,'k-');
    conf = 1.96 * np.sqrt(C)
    plt.fill_between(T,M-conf,M+conf,color='r',alpha=0.3)
    plt.ylabel(name); 
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0));
def plot_dynamics_vs_tracks(T,Z,M,C):
    plt.figure(figsize=(15,7))
    for i, (idx,name) in enumerate(zip(dofs_idxs, dofs_names)):
        mean = [m[idx] for m in M]
        var = [cov[idx,idx] for cov in C]
        plot_dynamic_vs_track(T,Z[:,idx],mean,var,i,name)
    plt.tight_layout()


# In[6]:
mean_list = FLT.filtering_mean_list
cov_list = FLT.filtering_covariance_list
plot_dynamics_vs_tracks(T,Z,mean_list,cov_list)
plt.show(False)

# # Check gradient system matrix
# L_Ky = (4000.)*10**3 - 100000
# par_name_list = ['L_Ky']
# dx = L_Ky * 1e-10
# g_sys = vehicle.get_grad_system_matrix(par_name_list, L_Ky=L_Ky)
# fd_g_sys_p = vehicle.get_system_matrix(L_Ky=L_Ky+dx)
# fd_g_sys_m = vehicle.get_system_matrix(L_Ky=L_Ky-dx)
# fd_g_sys = (fd_g_sys_p - fd_g_sys_m) / (2*dx)

# # Check gradient of DynamicMap
# dm = C175.DynamicsMap(vehicle, par_name_list)
# x = np.array([[L_Ky]])
# g_dm = dm.grad_x(x)
# fd_g_dm_p = dm.evaluate(x+dx)
# fd_g_dm_c = dm.evaluate(x)
# fd_g_dm = (fd_g_dm_p - fd_g_dm_c) / (dx)

# # Check gradient of NoiseDynamicMap
# ndm = C175.NoiseDynamicsMap(vehicle, par_name_list)
# x = np.array([[L_Ky]])
# g_ndm = ndm.grad_x(x)
# fd_g_ndm_p = ndm.evaluate(x+dx)
# fd_g_ndm_c = ndm.evaluate(x)
# fd_g_ndm = (fd_g_ndm_p - fd_g_ndm_c) / (dx)

# In[7]:

def f(L_Ky, ders):
    par_name_list = ['L_Ky']
    pi_hyper = C175.ParametersPrior(par_name_list)
    pi_prior = C175.StateSpacePrior(
        vehicle, par_name_list, init_coeffs=np.array([L_Ky]))
    pi_trans = C175.StateSpaceTransition(
        vehicle, par_name_list, init_coeffs=np.array([L_Ky]))
    FLT = ALGSI.LinearFilter(ders=ders, pi_hyper=pi_hyper)
    for n in range(nsteps):
        # Define log-likelihood
        if Y[n] is None: # Missing data
            ll = None
        else: 
            ll = C175.StateSpaceLogLikelihood(
                Y[n], vehicle, par_name_list, init_coeffs=np.array([L_Ky]))
        # Define transition / prior
        if n > 0: 
            pin = pi_trans
        else:
            pin = pi_prior
        # Assimilation
        FLT.assimilate(pin, ll)
    return FLT

nn = 21
L_Ky = C175.DEFAULTS['L_Ky']
dx = L_Ky * 0.05
xx = np.linspace(L_Ky-4*dx, L_Ky+4*dx, nn)
z = np.zeros(nn)
dz = np.zeros(nn)
for i in range(nn):
    flt = f(xx[i], 1)
    z[i] = flt.marginal_log_likelihood
    dz[i] = flt.grad_marginal_log_likelihood
plt.figure();
plt.plot(xx, z);
U = dx*np.ones(nn)
V = dx*dz
plt.quiver(xx, z, U, V, angles='xy')
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0));
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0));
plt.show(False);

gFLT = f(L_Ky, 1)
grad_mean_list = gFLT.filtering_grad_mean_list
grad_cov_list = gFLT.filtering_grad_covariance_list
grad_marg_logll = gFLT.grad_marginal_log_likelihood
# Finite difference
dx = L_Ky * 1e-8
FLTp = f(L_Ky + dx, 0)
FLTm = f(L_Ky - dx, 0)
fd_grad_mean_list = [ (p-m)/(2*dx) for p,m in zip(FLTp.filtering_mean_list,
                                                  FLTm.filtering_mean_list) ]
fd_grad_cov_list = [ (p-m)/(2*dx) for p,m in zip(FLTp.filtering_covariance_list,
                                                 FLTm.filtering_covariance_list) ]
fd_grad_marg_logll = ( FLTp.marginal_log_likelihood -  
                       FLTm.marginal_log_likelihood ) / (2*dx)

# ## Parameters estimation

# ### 2D Parameter estimation

# In[7]:

par_name_list = ['L_Ky']
pi = C175.ParametersPosterior(Y, vehicle, par_name_list)

P = Maps.LinearTransportMap(pi.prior.mu, pi.prior.sampling_mat)
# L = MAPS.FrozenLinearDiagonalTransportMap(np.array([L_Ky]),np.ones(len(par_name_list)))
pull_P_pi = DIST.PullBackTransportMapDistribution(P, pi)

# Laplace approximation
lap = TM.laplace_approximation(pull_P_pi, tol=1e-5, ders=1, fungrad=True)
L = Maps.LinearTransportMap.build_from_Gaussian(lap)
pull_LP_pi = DIST.PullBackTransportMapDistribution(L, pull_P_pi)

order = 1
T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
    1, order, 'total')
rho = DIST.StandardNormalDistribution(pi.dim)
push_rho = DIST.PushForwardTransportMapDistribution(T, rho)

# In[9]:

qtype = 3           # Gauss quadrature
qparams = [4] * 1   # Quadrature order
reg = None          # No regularization
tol = 1e-5          # Optimization tolerance
ders = 1            # Use gradient and Hessian
fungrad = True      # We provide a function that returns both function and gradient evaluation
log = push_rho.minimize_kl_divergence(
    pull_LP_pi, qtype=qtype, qparams=qparams, regularization=reg,
    tol=tol, ders=ders, fungrad=fungrad)


import TransportMaps.Diagnostics as DIAG
pull_TLP_pi = DIST.PullBackTransportMapDistribution(T, pull_LP_pi)
DIAG.plotAlignedConditionals(pull_TLP_pi)
var = DIAG.variance_approx_kl(rho, pull_TLP_pi, qtype=3, qparams=[4])
print("Variance diagnostic: %e" % var)

# # In[ ]:



