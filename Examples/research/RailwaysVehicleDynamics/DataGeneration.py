import sys, getopt
import logging
import numpy as np
import matplotlib.pyplot as plt

from TransportMaps.Distributions.Examples.RailwayVehicleDynamics import Coradia175 as C175

def usage():
    str_usage = """
DataGeneration.py --output=FNAME --nsteps=N --pars=PARLIST

PARLIST is a comma separated string conatining a subset of L_Ky,T_Ky,L_KPsi,T_KPsi,Kyb,Cyb,CPb,f11,f22
    """
    print(str_usage)

def full_usage():
    usage()

argv = sys.argv[1:]
nsteps = None
par_name_list = None
OUT_FNAME = None
try:
    opts, args = getopt.getopt(argv,"h", [
        "output=",
        "nsteps=", "pars="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--output"):
        OUT_FNAME = arg
    elif opt == ("--nsteps"):
        nsteps = int(arg)
    elif opt == ("--pars"):
        par_name_list = arg.split(',')
if None in [OUT_FNAME]:
    logging.warn("The density won't be stored")
if None in [nsteps, par_name_list]:
    print("ERROR: Missing inputs.")
    full_usage()
    sys.exit(1)

# Create prior distribution on the parameters
prior = C175.ParametersPrior(par_name_list)
par_val_list = prior.rvs(1).flatten()

# Instantiate vehicle
v = 42.5
vehicle = C175.Coradia175Vehicle(v)

# generate data
T, Z, Y = C175.generate_data(nsteps, vehicle, par_name_list, par_val_list)

# Plot generated data
dofs_idxs = [0, 4, 8, 12, 2, 6, 10]
dofs_names = [r"$Y_{w_1}$", r"$Y_{w_2}$", r"$Y_{b}$", r"$Y_{c}$", r"$\Psi_{w_1}$", r"$\Psi_{w_2}$", r"$\Psi_{b}$"]
def plot_dynamic(T,Z,i,name):
    plt.subplot(2,4,i+1); plt.plot(T,Z); plt.ylabel(name); plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0));
def plot_dynamics(T,Z):
    plt.figure(figsize=(15,7))
    for i, (idx,name) in enumerate(zip(dofs_idxs, dofs_names)):
        plot_dynamic(T,Z[:,idx],i,name)
    plt.tight_layout()
obs_names = [r"$\ddot{Y}_{b}$", r"$\dot{\Psi}_{b}$", r"$\ddot{Y}_{c}$",]
obs_pos = [3, 7, 4]
def plot_data(T,Y):
    plt.figure(figsize=(15,7))
    yy = np.asarray(Y)
    for i, (pos,name) in enumerate(zip(obs_pos,obs_names)):
        plt.subplot(2,4,pos); 
        plt.plot(T,yy[:,i],'.'); 
        plt.ylabel(name);
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0));
    plt.tight_layout()

plot_dynamics(T,Z); plot_data(T,Y);
plt.show(False)

# Create posterior distribution
pi = C175.ParametersPosterior(Y, vehicle, par_name_list, T=T, Z=Z)

if OUT_FNAME is not None:
    pi.store(OUT_FNAME)