import numpy as np

# Position of degrees of freedom
NDOFS  = 0
Yw1    = NDOFS; NDOFS += 1;
Yw1dot = NDOFS; NDOFS += 1;
Pw1    = NDOFS; NDOFS += 1;
Pw1dot = NDOFS; NDOFS += 1;
Yw2    = NDOFS; NDOFS += 1;
Yw2dot = NDOFS; NDOFS += 1;
Pw2    = NDOFS; NDOFS += 1;
Pw2dot = NDOFS; NDOFS += 1;
Yb     = NDOFS; NDOFS += 1;
Ybdot  = NDOFS; NDOFS += 1;
Pb     = NDOFS; NDOFS += 1;
Pbdot  = NDOFS; NDOFS += 1;
Yc     = NDOFS; NDOFS += 1;
Ycdot  = NDOFS; NDOFS += 1;
D1     = NDOFS; NDOFS += 1;
D2     = NDOFS; NDOFS += 1;

class Coradia175Vehicle(object):
    r"""
    Args:
      v (float): Longitudinal vehicle velocity (m/s)
    """
    mw     = 1000.     # Wheel set mass (kg)
    Iw     = 600.      # Wheel set yaw inertia (kg * m^2)
    mb     = 2000.     # Bogie mass (kg)
    Ib     = 2300.     # Bogie yaw inertia (kg * m^2)
    mc     = 8720.     # Half vehicle mass (kg)
    a      = 1.05      # Semi wheel-wheel spacing (m)
    l      = 0.75      # Half gauge (m)
    lmb    = 0.15      # Conicity
    r0     = 0.37      # Wheelset radius (m)
    
    def __init__(self, v):
        self.v      = v         # Vehicle forward velocity (m/s)
            
    def get_matrices(
            self,
            L_Ky   = (4000.)*10**3,   # Leading primary spring lateral (kN/m)
            T_Ky   = (4000.)*10**3,   # Trailing primary spring lateral (kN/m)
            L_KPsi = (4000.)*10**3,   # Primary yaw stiffness (kN/rad)
            T_KPsi = (4000.)*10**3,   # Primary yaw stiffness (kN/rad)
            Kyb    = (160.)*10**3,    # Secondary lateral stiffness (kN/m)
            Cyb    = (16.)*10**3,     # Secondary lateral damping (kN s/m)
            CPb    = (500.)*10**3,    # Secondary anti-yaw damping (kN s/rad)
            f11    = 7.4*10**6,       # Longitudinal creep coeff (MN)
            f22    = 6.2*10**6,       # Lateral creep coeff (MN)
    ):
        ###################
        # System dynamics #
        ###################
        A = np.zeros((NDOFS,NDOFS))
        # Lateral displacement front wheelset
        A[Yw1,Yw1dot]    = 1.
        A[Yw1dot,Yw1]    = - L_Ky
        A[Yw1dot,Yw1dot] = - 2. * f22 / self.v
        A[Yw1dot,Pw1]    = 2. * f22 
        A[Yw1dot,Yb]     = L_Ky 
        A[Yw1dot,Pb]     = self.a * L_Ky 
        A[Yw1dot,:]     /= self.mw
        # Yaw front wheelset
        A[Pw1,Pw1dot]    = 1.
        A[Pw1dot,Pw1dot] = - 2. * f11 * self.l**2 / self.v
        A[Pw1dot,Pw1]    = - L_KPsi
        A[Pw1dot,Pb]     = L_KPsi
        A[Pw1dot,D1]     = - 2. * f11 * self.lmb * self.l / self.r0
        A[Pw1dot,:]     /= self.Iw
        # Lateral displacement trailing wheelset
        A[Yw2,Yw2dot]    = 1.
        A[Yw2dot,Yw2]    = - T_Ky
        A[Yw2dot,Yw2dot] = - 2. * f22 / self.v
        A[Yw2dot,Pw2]    = 2. * f22 
        A[Yw2dot,Yb]     = T_Ky 
        A[Yw2dot,Pb]     = self.a * T_Ky 
        A[Yw2dot,:]     /= self.mw
        # Yaw front wheelset
        A[Pw2,Pw2dot]    = 1.
        A[Pw2dot,Pw2dot] = - 2. * f11 * self.l**2 / self.v
        A[Pw2dot,Pw2]    = - T_KPsi
        A[Pw2dot,Pb]     = T_KPsi
        A[Pw2dot,D2]     = - 2. * f11 * self.lmb * self.l / self.r0
        A[Pw2dot,:]     /= self.Iw
        # Lateral displacement bogie frame
        A[Yb,Ybdot]      = 1.
        A[Ybdot,Yw1]     = L_Ky
        A[Ybdot,Yw2]     = T_Ky
        A[Ybdot,Ybdot]   = - Cyb
        A[Ybdot,Yb]      = - (L_Ky + T_Ky + Kyb)
        A[Ybdot,Ycdot]   = Cyb
        A[Ybdot,Yc]      = Kyb
        A[Ybdot,:]      /= self.mb
        # Yaw bogie frame
        A[Pb,Pbdot]      = 1.
        A[Pbdot,Yw1]     = self.a * L_Ky
        A[Pbdot,Pw1]     = L_KPsi
        A[Pbdot,Yw2]     = - self.a * T_Ky 
        A[Pbdot,Pw2]     = T_KPsi
        A[Pbdot,Pbdot]   = - CPb
        A[Pbdot,Pb]      = - (L_Ky + T_Ky) * self.a**2 \
                                - (L_KPsi + T_KPsi)
        A[Pbdot,:]      /= self.Ib
        # Lateral displacement car body (MODIFIED!)
        A[Yc,Ycdot]      = 1.
        A[Ycdot,Ybdot]   = Cyb
        A[Ycdot,Yb]      = Kyb
        A[Ycdot,Ycdot]   = -Cyb
        A[Ycdot,Yc]      = - Kyb
        A[Ycdot,:]      /= self.mc
        # Lateral track displacement (leading wheelset)
        A[D1,Yw1dot]     = 1.
        A[D2,Yw2dot]     = 1.
        
        #################
        # Track control #
        #################
        G = np.zeros((NDOFS,2))
        G[D1,0] = -1.
        G[D2,1] = -1.

        ################
        # Observations #
        ################
        H = A[[Yw1dot, Yw2dot, Ybdot, Pbdot, Ycdot], :]

        return (A, G, H)

if __name__ == "__main__":
    import numpy.random as npr
    import scipy.linalg as scila
    import scipy.integrate as scint
    import matplotlib.pyplot as plt
    from matplotlib import animation

    def plot_dynamics(T, sol, Z, v):
        plt.figure(figsize=(12,6))
        plt.subplot(241); plt.grid(True);
        plt.plot(v*T, sol[:,Yw1])
        plt.subplot(242); plt.grid(True);
        plt.plot(v*T, sol[:,Yw2])
        plt.subplot(243); plt.grid(True);
        plt.plot(v*T, sol[:,Yb])
        plt.subplot(244); plt.grid(True);
        plt.plot(v*T, sol[:,Yc])
        plt.subplot(245); plt.grid(True);
        plt.plot(v*T, sol[:,Pw1])
        plt.subplot(246); plt.grid(True);
        plt.plot(v*T, sol[:,Pw2])
        plt.subplot(247); plt.grid(True);
        plt.plot(v*T, sol[:,Pb])
        plt.subplot(248); plt.grid(True);
        plt.plot(v*T, Z[:,0])
        plt.plot(v*T, Z[:,1])
        plt.show(False)

    
    def plot_observations(T, obs, Z, v):
        plt.figure(figsize=(12,6))
        plt.subplot(241); plt.grid(True);
        plt.plot(v*T, obs[:,0])
        plt.subplot(242); plt.grid(True);
        plt.plot(v*T, obs[:,1])
        plt.subplot(243); plt.grid(True);
        plt.plot(v*T, obs[:,2])
        plt.subplot(244); plt.grid(True);
        plt.plot(v*T, obs[:,4])
        plt.subplot(245); plt.grid(True);
        plt.plot(v*T, Z_sol[:,0])
        plt.subplot(246); plt.grid(True);
        plt.plot(v*T, Z_sol[:,1])
        plt.subplot(247); plt.grid(True);
        plt.plot(v*T, obs[:,3])
        plt.show(False)
    
    v = 40.
    vehicle = Coradia175Vehicle(v)
    (A, G, H) = vehicle.get_matrices()

    plt.figure()
    plt.spy(A)
    plt.show(False)

    vals, vecs = scila.eig(A)
    stb = vals <= 0. # Stable modes
    plt.figure()
    plt.scatter(vals[stb].real, vals[stb].imag)
    plt.scatter(vals[np.logical_not(stb)].real, vals[np.logical_not(stb)].imag, c='r')
    plt.show(False)

    # Check stability on straight track
    nvs = 91
    vs = np.linspace(10., 200., nvs)
    fig = plt.figure()
    ax = plt.axes(xlim=(-100, 10), ylim=(-400, 400))
    ax.grid(True)
    sc1 = ax.scatter([], [], s=5)
    sc2 = ax.scatter([], [], c='r', s=5)
    ttl = ax.text(-95, 320, '', size=10)
    def init():
        empty = np.zeros((0,2))
        ttl.set_text('')
        sc1.set_offsets(empty)
        sc2.set_offsets(empty)
        return ttl, sc1, sc2
    def animate(i):
        vehicle = Coradia175Vehicle(vs[i])
        (A, G, H) = vehicle.get_matrices()
        vals, vecs = scila.eig(A)
        stb = vals.real < 1e-12 # Stable modes
        stb_vals = np.vstack((vals[stb].real, vals[stb].imag)).T
        ustb_vals = np.vstack((vals[np.logical_not(stb)].real,
                               vals[np.logical_not(stb)].imag)).T
        ttl.set_text('$v$=%.1f' % vs[i])
        sc1.set_offsets(stb_vals)
        sc2.set_offsets(ustb_vals)
        return ttl, sc1, sc2

    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=nvs, interval=250, blit=True, repeat=False)
    plt.show(False)

    ##### ODE SOLVES ######
    # Define right hand side function
    def f(y, t, A, G, z, v, l):
        return np.dot(A, y) + np.dot(G, z(None, t, v, l))

    # Solve for v=30. on straight track with initial disturbance
    # Straight track
    def z(z, t, v, l): 
        return np.zeros(2)
    # Initial conditions
    y0 = np.zeros(NDOFS)
    y0[Yw1] = 0.01
    y0[D1] = y0[Yw1]
    y0[Yw2] = 0.01
    y0[D2] = y0[Yw2]
    # Vehicle
    v = 30.
    vehicle = Coradia175Vehicle(v)
    (A, G, H) = vehicle.get_matrices()
    # Time discretization
    Nsteps = 101
    Tf = 5.
    T = np.linspace(0., Tf, Nsteps)
    dt = T[1]-T[0]
    # Solve
    sol = scint.odeint(f, y0, T, args=(A, G, z, v, 2*vehicle.a))
    Z_sol = scint.odeint(z, np.zeros(2), T, args=(v, 2*vehicle.a))
    plot_dynamics(T, sol, Z_sol, v)

    # Solve for v=2. on sinusoidal track
    def z(z, t, v, l): 
        return 0.02 * np.array([ np.sin(t), np.sin(t - l / v) ])
    # Initial conditions
    y0 = np.zeros(NDOFS)
    # Vehicle
    v = 2.
    vehicle = Coradia175Vehicle(v)
    (A, G, H) = vehicle.get_matrices()
    # Time discretization
    Nsteps = 101
    Tf = 20.
    T = np.linspace(0., Tf, Nsteps)
    dt = T[1]-T[0]
    # Solve
    sol = scint.odeint(f, y0, T, args=(A, G, z, v, 2*vehicle.a))
    Z_sol = scint.odeint(z, np.zeros(2), T, args=(v, 2*vehicle.a))
    plot_dynamics(T, sol, Z_sol, v)

    # Solve for v=2. on piecewise linear track
    def z(z, t, v, l): 
        return 0.02 * np.array([ np.sin(np.round(t - 0.5)),
                                 np.sin(np.round(t - l / v - 0.5)) ])
    # Initial conditions
    y0 = np.zeros(NDOFS)
    # Vehicle
    v = 2.
    vehicle = Coradia175Vehicle(v)
    (A, G, H) = vehicle.get_matrices()
    # Time discretization
    Nsteps = 20
    dt = 2*vehicle.a / v
    T = np.arange(0, Nsteps, dt)
    # Solve
    sol = scint.odeint(f, y0, T, args=(A, G, z, v, 2*vehicle.a))
    Z_sol = scint.odeint(z, np.zeros(2), T, args=(v, 2*vehicle.a))
    plot_dynamics(T, sol, Z_sol, v)

    # Solve for v=2. on piecewise linear track with Pade approximation
    def z(i): 
        return 0.02 * np.array([ np.sin(i), np.sin(i-1) ])
    # Initial conditions
    y0 = np.zeros(NDOFS)
    # Vehicle
    v = 2.
    vehicle = Coradia175Vehicle(v)
    (A, G, H) = vehicle.get_matrices()
    # Time discretization
    Nsteps = 20
    dt = 2*vehicle.a / v
    # Pade approximations (Trapezoidal rule for inhomogeneous part)
    Phi = scila.expm(dt * A)
    Nint = 1000
    dt_int = np.linspace(0, dt, Nint+1)
    int_eval = np.zeros((Nint+1,) + G.shape)
    for i in range(Nint+1):
        int_eval[i,:,:] = np.dot( scila.expm(dt_int[i] * A), G )
    int_eval = (int_eval[:-1,:,:] + int_eval[1:,:,:]) / 2.
    Gamma = np.sum( dt/Nint * int_eval, axis=0 )    
    # Solve
    T = [ 0 ]
    sol = [ y0 ]
    Z_sol = [ np.zeros(2) ]
    for n in range(Nsteps):
        sol.append( np.dot(Phi, sol[-1]) + np.dot(Gamma, z(n)) )
        Z_sol.append( Z_sol[-1] + z(n) )
        T.append( T[-1] + dt )
    T = np.hstack(T)
    sol = np.vstack(sol)
    Z_sol = np.vstack(Z_sol)
    plot_dynamics(T, sol, Z_sol, v)

    # Solve for v=30. on piecewise linear GP track with Pade approximation
    # Vehicle
    v = 30.
    vehicle = Coradia175Vehicle(v)
    (A, G, H) = vehicle.get_matrices()
    # Time discretization
    Nsteps = 10**5
    dx = 2*vehicle.a
    dt = 1e-3
    lag = dx / v / dt
    if abs( lag % 1 ) > 1e-12:
        raise "dt is not a divisor of dx/v"
    lag = int(np.round(lag))
    # Track
    Ar = 2 * 0.33 * 10**-3
    sigma = np.sqrt( 4 * np.pi**2 * Ar * v**2 * dt )
    trk = sigma * np.hstack( (np.zeros(lag), npr.randn(Nsteps)) )
    def z(i): 
        return np.array([ trk[i+lag], trk[i] ])
    # Pade approximations (Trapezoidal rule for inhomogeneous part)
    Phi = scila.expm(dt * A)
    Nint = 100
    dt_int = np.linspace(0, dt, Nint+1)
    int_eval = np.zeros((Nint+1,) + G.shape)
    for i in range(Nint+1):
        int_eval[i,:,:] = np.dot( scila.expm(dt_int[i] * A), G )
    int_eval = (int_eval[:-1,:,:] + int_eval[1:,:,:]) / 2.
    Gamma = np.sum( dt/Nint * int_eval, axis=0 )
    # Initial conditions
    y0 = np.zeros(NDOFS)
    y0[D1] = np.sum(trk[:lag])
    # Solve
    T = [ 0 ]
    sol = [ y0 ]
    Z_sol = [ np.zeros(2) ]
    for n in range(Nsteps):
        sol.append( np.dot(Phi, sol[-1]) + np.dot(Gamma, z(n)) )
        Z_sol.append( Z_sol[-1] + z(n) )
        T.append( T[-1] + dt )
    T = np.hstack(T)
    sol = np.vstack(sol)
    Z_sol = np.vstack(Z_sol)
    plot_dynamics(T, sol, Z_sol, v)
    obs = np.dot(H, sol.T).T
    plot_observations(T, obs, Z_sol, v)