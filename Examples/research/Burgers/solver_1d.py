import time
import matplotlib.pyplot as plt
import numpy as np
import dolfin as dol
import dolfin_adjoint as doladj
import dill as dill

import TransportMaps.Distributions.Examples.BurgersPDE as BURG

SETTING = 1

if SETTING == 1:
    T      = 2         # final time
    xl     = -5.       # left end
    xr     = 5.        # right end
    ul     = 1.        # left boundary condition
    ur     = 0.        # right boundary condition
    nu = .05       # viscosity

    dt     = 0.05      # time step
    nels   = 100       # number of elements
    order  = 1         # order of elements
elif SETTING == 2:
    T      = 4         # final time
    xl     = -2.       # left end
    xr     = 2.        # right end
    ul     = 0.        # left boundary condition
    ur     = 0.        # right boundary condition
    nu = .01       # viscosity

    dt     = 0.05      # time step
    nels   = 100       # number of elements
    order  = 1         # order of elements

nsteps = int(round(T / dt))
    
solver = BURG.FinalL2MismatchBurgersProblem(
    T=T, xl=xl, xr=xr, ul=ul, ur=ur,
    nels=nels, order=order, nsteps=nsteps
)

# Define initial conditions
if SETTING == 1:
    u0 = dol.project(
        dol.Expression(
            '.5 * ( 1. - tanh(x[0]/(20.*nu)) / tanh(xr/(20.*nu)) )',
            nu=nu, xr=xr, degree=1
        ),
        solver.VEFS
    )
elif SETTING == 2:
    u0 = dol.project(
        dol.Expression(
            '2*nu*pi*sin(pi*x[0]) / (2+cos(pi*x[0]))',
            nu=nu, pi=np.pi, degree=1
        ),
        solver.VEFS
    )

plt.figure()
plt.plot(solver.coord, u0.vector()[:])
plt.show(False)

uf = solver.solve(nu, u0.vector()[:])

plt.figure()
plt.plot(solver.coord, uf)
plt.show(False)

# Set up the reduced functional
u0 = dol.project(
    dol.Expression(
        '(x[0]-xr)/(xl-xr)',
        xr=xr, xl=xl, degree=1
    ),
    solver.VEFS
)

adj_uf = solver.dof_to_fun(uf)

solver.ud = adj_uf.vector()[:]

# Calculate gradients
J, dJdnu, dJdu = solver.tuple_grad_x(nu, u0.vector()[:])

nrun = 10
tic = time.process_time()
for i in range(nrun):
    print(i)
    J, dJdnu, dJdu = solver.tuple_grad_x(nu, u0.vector()[:])
toc = time.process_time()
print("Function/gradient evaluation: %.2f" % ((toc-tic)/nrun))

solver2 = dill.loads(dill.dumps(solver))

