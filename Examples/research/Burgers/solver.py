import matplotlib.pyplot as plt
import numpy as np
import dolfin as dol

T      = 0.1        # final time
nsteps = 100        # number of time steps
nels   = 40
dt = T / nsteps

mesh = dol.UnitSquareMesh(nels,nels)

VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
VEFS = dol.FunctionSpace(mesh, VE)

# Test function
v = dol.TestFunction(VEFS)

# Function
u = dol.Function(VEFS)
u_p = dol.Function(VEFS)

# Define viscosity
# nu = dol.Constant(.01)
nu = dol.Expression(
    'x[0]<0.5 ? 0.01 : 0.1', degree=1
)

u0 = dol.Expression(
    ("sin(4*pi*x[0]) + sin(4*pi*x[1])"), degree=1
)

# Define boundary conditions
def boundary(x):
    return x[0] < dol.DOLFIN_EPS or x[0] > 1. - dol.DOLFIN_EPS or \
        x[1] < dol.DOLFIN_EPS or x[1] > 1. - dol.DOLFIN_EPS
bc = dol.DirichletBC(VEFS, dol.Constant(0.), boundary)

# Define variational problem
k = dol.Constant(dt)
F = (u - u_p) / k * v * dol.dx + \
    dol.dot(nu * dol.grad(u), dol.grad(v)) * dol.dx + \
    u * u.dx(0) * v * dol.dx + u * u.dx(1) * v * dol.dx

# Output array
uout = np.zeros((nsteps+1, (nels+1)**2))

# Time stepping
u.interpolate(u0)

plt.figure()
dol.plot(u)
plt.show(False)

uout[0,:] = u.vector()[:]
t = 0
for n in range(nsteps):
    t += dt
    u_p.assign(u)
    dol.solve(F==0, u, bc)
    uout[n+1,:] = u.vector()[:]

# plt.figure()
# plt.imshow(uout[:,:])
# plt.colorbar()
# plt.show(False)

plt.figure()
dol.plot(u)
plt.show(False)
