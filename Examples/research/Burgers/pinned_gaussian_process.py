import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern
# import dolfin as dol

npr.seed(0)

gpr = GaussianProcessRegressor(
    kernel=Matern(nu=.5)
)

X = np.array([[0.],[1.]])
y = np.array([0., 0.])

gpr.fit(X, y)

npnts = 100
xx = np.linspace(0, 1, npnts).reshape((npnts, 1))

nrun = 10
yy = gpr.sample_y(xx, n_samples=nrun)

plt.figure()
plt.plot(xx, yy)
plt.show(False)

mean, cov = gpr.predict(xx, return_cov=True)

# nels = 100

# mesh = dol.UnitIntervalMesh(nels)
# VE = dol.FiniteElement("Lagrange", dol.interval, 1)
# VEFS = dol.FunctionSpace(mesh, VE)
# coord = VEFS.tabulate_dof_coordinates().flatten()
# ndofs = coord.shape[0]

# # Test function
# v = dol.TestFunction(VEFS)

# # Trial Function
# u = dol.TrialFunction(VEFS)

# # Parametres
# k = dol.Constant(.01)

# # Boundary conditions
# bc = dol.DirichletBC(VEFS, dol.Constant(0.), lambda x: x[0] < dol.DOLFIN_EPS or x[0] > 1 - dol.DOLFIN_EPS)

# # Forcing term
# f = dol.Function(VEFS)

# # Form
# A = - dol.dot( k * dol.grad(u), dol.grad(v) ) * dol.dx
# b = f * v * dol.dx

# # Generate some random fields
# nrun = 10
# x_lst = []
# for i in range(nrun):
#     # Generate random forcing
#     f.vector().set_local(npr.randn(ndofs), np.arange(ndofs, dtype=np.intc))
#     f.vector().apply('insert')
#     # Solve
#     u = dol.Function(VEFS)
#     dol.solve(A == b, u, bc)
#     # Extract field
#     x_lst.append( np.copy( u.vector()[:] ) )

# for x in x_lst:
#     plt.figure()
#     plt.plot(coord, x)
#     plt.show(False)