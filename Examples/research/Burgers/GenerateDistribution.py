import time
import matplotlib.pyplot as plt
import numpy as np
import dolfin as dol
import dill

import TransportMaps as TM
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DIST
import TransportMaps.Distributions.Examples.BurgersPDE as BURG

SETTING = 1

if SETTING == 1:
    T      = 2         # final time
    xl     = -5.       # left end
    xr     = 5.        # right end
    ul     = 1.        # left boundary condition
    ur     = 0.        # right boundary condition
    nu     = .05       # viscosity

    u0_length_scale = 1. # Prior setting on u0
    nu_mean = -3.      # Prior setting on nu
    nu_std  = 3.       # Prior setting on nu
    obs_sigma = 1e-2   # Likelihood noise standard deviation

    dt     = 0.05       # time step
    nels   = 100       # number of elements
    order  = 1         # order of elements

    high_dt = 0.01    # time step (high-accuracy solution)
    high_nels = 1000   # number of elements (high-accuracy solution)
    
elif SETTING == 2:
    T      = 4         # final time
    xl     = -2.       # left end
    xr     = 2.        # right end
    ul     = 0.        # left boundary condition
    ur     = 0.        # right boundary condition
    nu     = .01       # viscosity

    dt     = 0.05      # time step
    nels   = 100       # number of elements
    order  = 1         # order of elements

    high_dt = 0.0001   # time step (high-accuracy solution)
    high_nels = 1000     # number of elements (high-accuracy solution)

##############################
# Generate sythetic solution #
##############################
# Set up high accuracy solver
high_nsteps = int(round(T / high_dt))
high_solver = BURG.FinalL2MismatchBurgersProblem(
    T=T, xl=xl, xr=xr, ul=ul, ur=ur,
    nels=high_nels, order=order, nsteps=high_nsteps
)

# Define initial conditions
if SETTING == 1:
    u0 = dol.Expression(
        '.5 * ( 1. - tanh(x[0]/(20.*nu)) / tanh(xr/(20.*nu)) )',
        nu=nu, xr=xr, degree=1
    )
elif SETTING == 2:
    u0 = dol.Expression(
        '2*nu*pi*sin(pi*x[0]) / (2+cos(pi*x[0]))',
        nu=nu, pi=np.pi, degree=1
    )
u0 = np.array( dol.project(u0, high_solver.VEFS).vector()[:] )

plt.figure()
plt.plot(high_solver.coord, u0)
plt.title('Initial conditions - u(x,0.0)')
plt.show(False)

high_uf = high_solver.solve(nu, u0)

plt.figure()
plt.plot(high_solver.coord, high_uf)
plt.title('Final solution - u(x,%.1f)' % T)
plt.show(False)

high_solver.gen_u0 = u0
high_solver.gen_uf = high_uf

# Set up low accuracy solver
nsteps = int(round(T / dt))
solver = BURG.FinalL2MismatchBurgersProblem(
    T=T, xl=xl, xr=xr, ul=ul, ur=ur,
    nels=nels, order=order, nsteps=nsteps
)

# Project high-accuracy solution on low-accuracy space
uf = np.array( dol.project( high_solver.dof_to_fun(high_uf), solver.VEFS ).vector()[:] )

# Corrupt solution with noise
pi_noise = DIST.NormalDistribution(np.zeros(solver.ndofs-2), obs_sigma**2 * np.eye(solver.ndofs-2))
obs = uf.copy()
obs[1:-1] += pi_noise.rvs(1).flatten()

plt.figure()
plt.plot(solver.coord, uf, label='data')
plt.plot(solver.coord, obs, label='data + noise')
plt.show(False)

###################################
# ASSEMBLE POSTERIOR DISTRIBUTION #
###################################

pi = BURG.ViscosityInitialConditionsBurgersPosteriorDistribution(
    solver=solver,
    # Prior settings
    u0_length_scale = u0_length_scale,
    nu_mean = nu_mean,
    nu_std  = nu_std,
    # Likelihood setting
    obs_sigma = obs_sigma,
    # Observations (must be defined on the FunctionSpace of the solver)
    obs = obs
)

# Attach to the distribution also the generative model
pi.high_solver = high_solver

# Plot some samples from the prior 
nsamps = 10
smps = pi.prior.rvs(nsamps)

plt.figure('Prior samples (initial conditions)')
plt.plot(pi.solver.coord[1:-1], smps[:,1:].T)
plt.show(False)

# Plot some samples from the prior predictive
plt.figure('Prior predictive samples')
for i in range(nsamps):
    f = pi.solver.solve( smps[i,0], smps[i,1:] )
    plt.plot(pi.solver.coord, f)
plt.show(False)

# Plot histogram of prior on nu
nu_smps = pi.prior.rvs(10000)[:,0]
plt.figure()
plt.hist(nu_smps, bins=np.logspace(np.log10(min(nu_smps)), np.log10(max(nu_smps)), 100) )
plt.gca().set_xscale('log')
plt.show(False)

# Store target distribution
pi.store('data/dist-T2.dill')

# Whiten target distriubtion
pi_white = dill.loads(dill.dumps(pi))
W = pi_white.prior_map
pi_white.prior = pi_white.prior.base_distribution
pi_white.logL.T = Maps.CompositeMap(pi_white.logL.T, W)

# Test speed of evaluation
N = 10
start = time.process_time()
pi_white.tuple_grad_x_log_pdf( pi_white.prior.rvs(N) )
stop = time.process_time()
print("Evaluation time: %.2f" % ((stop-start)/N))

# Check derivatives
x = pi.prior.rvs(1)
dx = pi.prior.rvs(1)
TM.taylor_test(
    x=x, dx=dx,
    f=pi_white.tuple_grad_x_log_pdf,
    # ahf=pi_white.action_hess_x_log_pdf,
    h=1e-4,
    fungrad=True
)

# Store whitened target distribution
pi_white.store('data/white-dist-T2.dill')
