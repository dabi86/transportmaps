#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2016 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# E-mail: tmteam@mit.edu
#

import sys, getopt
import os.path
import time, datetime
import dill
import h5py
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import scipy.stats as scistat
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.collections import PolyCollection
import codecs
import matplotlib.dates as mdates
import matplotlib.cbook as cbook

import TransportMaps as TM
import TransportMaps.Diagnostics as DIAG
from TransportMaps.Distributions.Examples.Lorenz96 import \
    Lorenz96ModalForwardEulerDistribution

def usage():
    print("postprocess-plotting.py --data=<file_name> " + \
          "--postprocess-data=<fname> " + \
          "[" + \
          "--rmse-burnin=BURNIN" + \
          "--do-smooth --do-smooth-rmse " + \
          "--do-unb-smooth " + \
          "--do-smooth-evol" + \
          "--do-smooth-evol-steps=STEPS" + \
          "--do-filt --do-smooth-filt" + \
          "--trim-ts=TIMES " + \
          "--do-post-pred --do-unb-post-pred " + \
          "--mcmc-skip=SKIP --mcmc-burnin=BURNIN --ntraj=4 --perc=90,50,20 " + \
          "--do-trim-var-diag --chunk-size=SIZE" + \
          "--true-dynamics --observations" + \
          "--store-fig-dir=None]")

def full_usage():
    usage()

def bytespdate2num(fmt, encoding='us-ascii'):
    strconverter = mdates.strpdate2num(fmt)
    def bytesconverter(b):
        s = b.decode(encoding)
        return strconverter(s)
    return bytesconverter

LW = 1
TITLES = False
    
argv = sys.argv[1:]
DATA = None
POST_DATA = None
RMSE_BURNIN = 0
DO_SMOOTH_RMSE = False
DO_FILT_RMSE = False
DO_SMOOTH = False
DO_SMOOTH_BURNIN = 0
DO_UNBIASED_SMOOTH = False
DO_SMOOTH_EVOL = False
DO_SMOOTH_EVOL_STEPS = None
DO_FILT = False
DO_FILT_BURNIN = 0
TRIM_TS = None
DO_POST_PRED = False
DO_UNB_POST_PRED = False
DO_TRIM_VAR_DIAG = False
CHUNK_SIZE = None
MCMC_SKIP = 0
MCMC_BURNIN = 0
NTRAJ = 4
TRUE_DYNAMICS = False
OBSERVATIONS = False
PERC_LIST = [90, 50, 20]
PERC_STYLES = [':', '-.', '--']
STORE_FIG_DIR = None
STORE_FIG_FMATS = ['svg', 'pdf']
PLOT = True
try:
    opts, args = getopt.getopt(argv,"h",["data=", "postprocess-data=",
                                         "rmse-burnin=",
                                         "do-smooth-rmse", "do-filt-rmse",
                                         "do-smooth", "do-unb-smooth",
                                         "do-smooth-burnin=",
                                         "do-smooth-evol", 
                                         "do-smooth-evol-steps=",
                                         "do-filt", "do-filt-burnin=",
                                         "trim-ts=",
                                         "mcmc-skip=", "mcmc-burnin=",
                                         "ntraj=", "perc=",
                                         "true-dynamics", "observations",
                                         'do-post-pred', "do-unb-post-pred",
                                         "do-trim-var-diag", "chunk-size=",
                                         "store-fig-dir=",
                                         "no-plotting"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt == "--data":
        DATA = arg
    elif opt == "--postprocess-data":
        POST_DATA = arg
    elif opt == "--rmse-burnin":
        RMSE_BURNIN = int(arg)
    elif opt == "--do-smooth-rmse":
        DO_SMOOTH_RMSE = True
    elif opt == "--do-filt-rmse":
        DO_FILT_RMSE = True
    elif opt == "--do-smooth":
        DO_SMOOTH = True
    elif opt == '--do-smooth-burnin':
        DO_SMOOTH_BURNIN = int(arg)
    elif opt == "--do-unb-smooth":
        DO_UNBIASED_SMOOTH = True
    elif opt == "--do-smooth-evol":
        DO_SMOOTH_EVOL = True
    elif opt == "--do-smooth-evol-steps":
        DO_SMOOTH_EVOL_STEPS = [ int(a) for a in arg.split(',') ]
    elif opt == "--do-filt":
        DO_FILT = True
    elif opt == '--do-filt-burnin':
        DO_FILT_BURNIN = int(arg)
    elif opt == "--trim-ts":
        TRIM_TS = [ int(t) for t in arg.split(',') ]
    elif opt == "--mcmc-skip":
        MCMC_SKIP = int(arg)
        if MCMC_SKIP < 1:
            raise ValueError("SKIP must be >0 in --mcmc-skip=SKIP")
    elif opt == "--ntraj":
        NTRAJ = int(arg)
    elif opt == '--true-dynamics':
        TRUE_DYNAMICS = True
    elif opt == '--observations':
        OBSERVATIONS = True
    elif opt == "--perc":
        PERC_LIST = [int(p) for p in arg.split(',')]
    elif opt == '--do-post-pred':
        DO_POST_PRED = True
    elif opt == '--do-unb-post-pred':
        DO_UNB_POST_PRED = True
    elif opt == '--do-trim-var-diag':
        DO_TRIM_VAR_DIAG = True
    elif opt == '--chunk-size':
        CHUNK_SIZE = int(arg)
    elif opt == "--store-fig-dir":
        STORE_FIG_DIR = arg
    elif opt == "--no-plotting":
        PLOT = False

def tstamp_print(msg, *args, **kwargs):
    tstamp = datetime.datetime.fromtimestamp(
        time.time()
    ).strftime('%Y-%m-%d %H:%M:%S')
    print(tstamp + " " + msg, *args, **kwargs)

def filter_tstamp_print(msg, *args, **kwargs):
    if VERBOSE:
        tstamp_print(msg, *args, **kwargs)

def filter_print(*args, **kwargs):
    if VERBOSE:
        print(*args, **kwargs)

def store_figure(fig, fname):
    for fmat in STORE_FIG_FMATS:
        fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight');
        
if None in [DATA, POST_DATA]:
    full_usage()
    tstamp_print("ERROR: Options --data and --postprocess-data must be specified")
    sys.exit(3)

# Load data
with open(DATA, 'rb') as in_stream:
    stg = dill.load(in_stream)

# Load postprocess data
with open(POST_DATA, 'rb') as istr:
    data = dill.load(istr)

# Load hdf5 file
h5data = h5py.File(POST_DATA + '.hdf5', 'r')

# Restore data
target_distribution = stg.target_distribution
base_distribution = stg.base_distribution

IS_MODAL = isinstance(target_distribution, Lorenz96ModalForwardEulerDistribution)

dim = target_distribution.dim
dim_state = target_distribution.prior.state_dim
nsteps = target_distribution.nsteps
tvec = target_distribution.t
xs = np.asarray(target_distribution.xs)
tvec_obs = target_distribution.t_obs
ys = np.asarray(target_distribution.ys)
obs_lin_term = target_distribution.obs_map.linearTerm
# obs_lin_term = target_distribution.logL.factors[0][0].T.linearTerm
obs_dims = [ np.where( obs_lin_term[i,:] == 1. )[0][0] \
             for i in range(obs_lin_term.shape[0]) ]

if CHUNK_SIZE is None:
    CHUNK_SIZE = nsteps

def nicePlot(ax):
    coloraxes = 'gray'
    ax.spines['right'].set_visible(False) # Remove the right axis boundary
    ax.spines['top'].set_visible(False)  # Remove the top axis boundary
    ax.xaxis.set_ticks_position('bottom') # Set the x-ticks to only the bottom
    ax.yaxis.set_ticks_position('left') # Set the y-ticks to only the left
    ax.spines['bottom'].set_position(('axes',-0.04)) # Offset the bottom scale from the axis
    ax.spines['left'].set_position(('axes',-0.04))  # Offset the left scale from the axis
    ax.spines['left'].set_linewidth(.5)
    ax.spines['bottom'].set_linewidth(.5)
    ax.spines['bottom'].set_color(coloraxes)
    ax.spines['left'].set_color(coloraxes)
    ax.xaxis.label.set_color(coloraxes)
    ax.yaxis.label.set_color(coloraxes)
    ax.tick_params(axis='x', colors=coloraxes)
    ax.tick_params(axis='y', colors=coloraxes)
    ax.yaxis.label.set_size(18)
    ax.xaxis.label.set_size(18)
    ax.title.set_color(coloraxes)
    ax.title.set_size(16)

def plot_sample(
        tt,
        truth,
        data,
        cmap='jet',
        title=None,
        data_title=None,
        single_cmap=True,
):
    fsize = (8.4, 4.8)
    fig = plt.figure(figsize=fsize)

    dd = np.arange(truth.shape[1])
    DD, TT = np.meshgrid(dd, tt)
    
    axs = []
    imgs = []
    
    ax = fig.add_subplot(211)
    imgs.append( ax.contourf(
        TT.T, DD.T, truth.T, cmap=plt.get_cmap(cmap)) )
    ax.set_ylabel("Truth")
    ax.get_xaxis().set_visible(False)
    axs.append(ax)

    ax = fig.add_subplot(212)
    imgs.append( ax.contourf(
        TT.T, DD.T, data.T, cmap=plt.get_cmap(cmap)) )
    ax.set_ylabel(data_title)
    ax.set_xlabel("Time")
    axs.append(ax)

    if single_cmap:
        # Normalize colors
        vmin = min(img.get_array().min() for img in imgs)
        vmax = max(img.get_array().max() for img in imgs)
        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
        for im in imgs:
                im.set_norm(norm)
        # Set colorbar
        fig.colorbar(imgs[0], ax=axs, orientation='vertical')
    else:
        # Plot two colorbars
        for img, ax in zip(imgs, axs):
            fig.colorbar(img, ax=ax, orientation='vertical')

    if TITLES:
        ax.set_title(title)

    return fig

def plot_rmse_vardiag_ncoeffs(tvec, rmse, var_diag, n_coeffs):
    fig = plt.figure()
    ax = fig.add_subplot(3,1,1)
    nicePlot(ax)
    ax.plot( tvec, rmse )
    ax.set_ylabel('RMSE')
    xlim = ax.get_xlim()
    ax = fig.add_subplot(3,1,2)
    nicePlot(ax)
    if len(tvec) == len(var_diag):
        ax.plot( tvec, var_diag )
    else:
        ax.plot( tvec[1:], var_diag )
    ax.set_ylabel('Var. diag.')
    ax.set_xlim( xlim )
    ax = fig.add_subplot(3,1,3)
    nicePlot(ax)
    ax.plot( tvec, n_coeffs )
    ax.set_ylabel('\#coeffs')
    ax.set_xlabel('time')
    ax.set_xlim( xlim )
    return fig
    
# def plot_mean_percentile_traj_boxplot(
#         data1, ts, data2=None, ntraj=0,
#         ylabel=None):
#     fig = plt.figure(figsize=FSIZE)
    
#     mean_1 = np.mean(data1, axis=0)
#     q5_1 = np.percentile(data1, 5, axis=0)
#     q25_1 = np.percentile(data1, 25, axis=0)
#     # q40_1 = np.percentile(data1, 40, axis=0)
#     # q60_1 = np.percentile(data1, 60, axis=0)
#     q75_1 = np.percentile(data1, 75, axis=0)
#     q95_1 = np.percentile(data1, 95, axis=0)

#     ax = fig.add_subplot(111)
#     nicePlot(ax)
#     # Mean and quantiles
#     ax.plot(tvec, mean_1, c='k')
#     # verts = []
#     # verts.append( list(zip(tvec, q40_1)) + list(zip(tvec[::-1], q60_1[::-1])) )
#     # poly = PolyCollection(verts, facecolor=['red'])
#     # poly.set_alpha(0.5)
#     # ax.add_collection3d(poly, zs=[0], zdir='z')
#     ax.plot(tvec, q25_1, 'k-.', linewidth=0.5)
#     ax.plot(tvec, q75_1, 'k-.', linewidth=0.5)
#     ax.plot(tvec, q5_1, 'k--', linewidth=0.5)
#     ax.plot(tvec, q95_1, 'k--', linewidth=0.5)
#     miny = np.min(q5_1)
#     maxy = np.max(q95_1)
#     span = maxy-miny
#     miny -= 0.1 * span
#     maxy += 0.1 * span
#     # ax.set_ylim([miny, maxy])
#     # Slices
#     if data2 is not None:
#         boxprops = dict(linestyle='-', linewidth=1.5, color='firebrick')
#         medianprops = dict(linestyle='-', linewidth=3, color='firebrick')
#         capprops = dict(linestyle='-', linewidth=1.5, color='firebrick')
#         whiskerprops = dict(linestyle='-', linewidth=1.5, color='firebrick')
#         meanprops = dict(marker='o',markerfacecolor='green',
#                          markersize=7, linestyle='none')
#         ax.boxplot(
#             np.asarray(data2).T, positions=ts, whis=[5,95],
#             boxprops=boxprops, medianprops=medianprops,
#             whiskerprops=whiskerprops, capprops=capprops,
#             showfliers=False, widths=15.,
#             showmeans=True, meanprops=meanprops
#         )
#     # rvec = tvec[-1] - tvec[0]
#     # ax.set_xlim([tvec[0]-0.005*rvec, tvec[-1]+0.005*rvec])
#     rvec = ax.xaxis.get_data_interval()
#     ax.set_xlim( rvec[0]-1, rvec[1]+1 )
#     if ylabel is not None:
#         ax.set_ylabel(ylabel)
#     plt.tight_layout()

#     if EXCHANGES_DATA is not None:
#         ax.xaxis_date()
#         ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
#         fig.autofmt_xdate()
#     else:
#         ax.set_xlabel('time')
    
#     return fig

#--0--###### Report time-averaged RMSE for the smoothing distribution #############
if DO_SMOOTH_RMSE:
    print()
    tar_samp_smooth = h5data['trim-%d' % nsteps]['quadrature']['approx-target']['0']
    m = tar_samp_smooth.shape[0]
    Xt = tar_samp_smooth[:,:].reshape( (m, dim//dim_state, dim_state) )
    if IS_MODAL:
        Xt_tmp = Xt
        nsteps = Xt.shape[1]
        Xt = np.zeros((m, nsteps, target_distribution.modal_map.dim_out))
        for i in range(Xt.shape[1]):
            Xt[:,i,:] = target_distribution.modal_map.evaluate( Xt_tmp[:,i,:] )
    mean_Xt = np.mean(Xt, axis=0)[RMSE_BURNIN:,:] # nstep x 3
    true_Xt = np.asarray(target_distribution.xs)[RMSE_BURNIN:,:] # nsteps x 3
    print("Reporting time-averaged RMSE of the smoothing distribution " + \
          "(complete time series)")
    diff_Xt = true_Xt - mean_Xt
    rmse = npla.norm( diff_Xt, axis=1 ) / np.sqrt(dim_state)
    avg_rmse = np.mean( rmse, axis=0 )
    med_rmse = np.median( rmse, axis=0 )
    var_rmse = np.var( rmse, axis=0 )
    print("    Average RMSE:   %e" % avg_rmse)
    print("    Median RMSE:    %e" % med_rmse)
    print("    Variance RMSE:  %e" % var_rmse)
    print()
    # Plot RMSE vs Var-diagnostic vs number of coefficients (in filtering map)
    if PLOT:
        var_diag = [ stg.integrator.var_diag_convergence[i-1] for i in range(stg.integrator.nsteps)
                     if i >= RMSE_BURNIN ]
        n_coeffs = [ stg.integrator.filtering_map_list[i].n_coeffs for i in range(stg.integrator.nsteps+1)
                     if i >= RMSE_BURNIN ]
        plot_rmse_vardiag_ncoeffs(
            [tvec[i] for i in range(stg.integrator.nsteps+1) if i >= RMSE_BURNIN],
            rmse, var_diag, n_coeffs)
        plt.show(False)
    print("Reporting time-averaged RMSE of the smoothing distribution " + \
          "(at observation times)")
    idxs = [ i - RMSE_BURNIN
             for i, (ll,_) in enumerate(target_distribution.logL.factors) \
             if ll is not None and i >= RMSE_BURNIN ]
    diff_Xt = true_Xt[idxs,:] - mean_Xt[idxs,:]
    rmse = npla.norm( diff_Xt, axis=1 ) / np.sqrt(dim_state)
    avg_rmse = np.mean( rmse, axis=0 )
    med_rmse = np.median( rmse, axis=0 )
    var_rmse = np.var( rmse, axis=0 )
    print("    Average RMSE:   %e" % avg_rmse)
    print("    Median RMSE:    %e" % med_rmse)
    print("    Variance RMSE:  %e" % var_rmse)
    print()
    # Plot RMSE vs Var-diagnostic vs number of coefficients (in filtering map)
    if PLOT:
        var_diag = [ stg.integrator.var_diag_convergence[i-1+RMSE_BURNIN]
                     for i in idxs if i > 0  ]
        n_coeffs = [ stg.integrator.filtering_map_list[i-1+RMSE_BURNIN].n_coeffs
                     for i in idxs ]
        plot_rmse_vardiag_ncoeffs(
            [tvec[i+RMSE_BURNIN] for i in idxs],
            rmse, var_diag, n_coeffs)
        plt.show(False)
    
#--1--###### Plot: smoothing/posterior marginals timesteps #############
if DO_SMOOTH:
    print("\nPlotting smoothing/posterior marginals \n")
    tar_samp_smooth = h5data['trim-%d' % nsteps]['quadrature']['approx-target']['0']
    m = tar_samp_smooth.shape[0]
    Xt_samp_smooth = tar_samp_smooth[:,:].reshape( (m, dim//dim_state, dim_state) )
    if IS_MODAL:
        Xt_tmp = Xt_samp_smooth
        nsteps = Xt_samp_smooth.shape[1]
        Xt_samp_smooth = np.zeros((m, nsteps, target_distribution.modal_map.dim_out))
        for i in range(Xt_samp_smooth.shape[1]):
            Xt_samp_smooth[:,i,:] = target_distribution.modal_map.evaluate( Xt_tmp[:,i,:] )
    start = DO_SMOOTH_BURNIN
    i = 0
    while start < nsteps:
        stop = min(start + CHUNK_SIZE, nsteps)
        smps = Xt_samp_smooth[:,start:stop,:]
        tt = tvec[start:stop]
        truth = xs[start:stop,:]

        # Plot mean
        mean = np.mean(smps,axis=0)
        fig = plot_sample( tt, truth, mean,
                           data_title="Mean smoothing")
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/'+ \
                         'smoothing-marginals-timesteps-mean-chunk-%d' % (i))

        # Plot mean error
        mean_err = np.abs(truth - mean)
        fig = plot_sample( tt, truth, mean_err,
                           data_title="Error in mean smoothing",
                           single_cmap=False)
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/'+ \
                         'smoothing-marginals-timesteps-error-mean-chunk-%d' % (i))

        # Plot median
        med = np.median(smps,axis=0)
        fig = plot_sample( tt, truth, med,
                           data_title="Median smoothing")
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/'+ \
                         'smoothing-marginals-timesteps-median-chunk-%d' % (i))

        # Plot median error
        med_err = np.abs(truth - med)
        fig = plot_sample( tt, truth, med_err,
                           data_title="Error in median smoothing",
                           single_cmap=False)
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/'+ \
                         'smoothing-marginals-timesteps-error-median-chunk-%d' % (i))

        # Plot standard deviation
        std = np.std(smps, axis=0)
        fig = plot_sample( tt, truth, std,
                           data_title="Stadard deviation smoothing",
                           single_cmap=False)
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/'+ \
                         'smoothing-marginals-timesteps-std-chunk-%d' % (i))

        # Plot trajectories
        for nt in range(NTRAJ):
            trj = smps[nt,:,:]
            fig = plot_sample( tt, truth, med,
                               data_title="Smoothing trajectory %d" % nt)
            if PLOT: plt.show(False)
            if STORE_FIG_DIR is not None:
                store_figure(fig, STORE_FIG_DIR+'/Smoothing-traj-%d-timesteps-chunk-%d' % (nt, i))
            
        start = stop
        i += 1

if DO_SMOOTH_EVOL:
    # Iterate from the end to get handle of xlim and ylim
    xlim = None
    ylim = None
    for trim in reversed(DO_SMOOTH_EVOL_STEPS):
        print("\nPlotting smoothing/posterior marginals - step %d \n" % trim)
        trim_tar_samp_smooth = h5data['trim-%d' % trim]['quadrature']['approx-target']['0']
        m = trim_tar_samp_smooth.shape[0]
        trim_Xt_samp_smooth = \
           trim_tar_samp_smooth[:,:].reshape( (
               m, trim_tar_samp_smooth.shape[1]//3, 3) )
        start = 0
        i = 0
        while start < trim:
            stop = min(start + CHUNK_SIZE, trim)
            smps = trim_Xt_samp_smooth[:,start:stop,:]
            tt = tvec[start:stop]
            fig = plot_mean_percentile_traj(
                tt, smps, ntraj=NTRAJ,
                title="Posterior/Smoothing marginals - step %d" % trim,
                xlim=xlim, ylim=ylim)
            if PLOT: plt.show(False)

            if xlim is None and ylim is None:
                axs_lst = fig.get_axes()
                xlim = [ ax.get_xlim() for ax in axs_lst ]
                ylim = [ ax.get_ylim() for ax in axs_lst ]
            
            if STORE_FIG_DIR is not None:
                store_figure(fig, STORE_FIG_DIR+'/'+ \
                             'smoothing-marginals-evol-step-%d-ntraj-%d-chunk-%d' % (trim, NTRAJ, i))
            start = stop
            i += 1
        
if DO_UNBIASED_SMOOTH: # Fast reading from h5 file
    print("\nLoading Metropolis samples \n")
    dset = h5data['trim-%d' % nsteps]\
           ['metropolis-independent-proposal-samples']['skip-%d' % MCMC_SKIP]['x']
    tar_unb_samp_smooth = np.zeros(dset.shape)
    for d in range(dset.shape[1]):
        print("Dimension %d/%d" % (d+1,dset.shape[1]))
        tar_unb_samp_smooth[:,d] = dset[:,d]
    munb = tar_unb_samp_smooth.shape[0]
    tar_unb_samp_smooth = tar_unb_samp_smooth.reshape( munb, dim//dim_state, dim_state )
        
if DO_SMOOTH and DO_UNBIASED_SMOOTH:
    Xt_unb_samp_smooth = tar_unb_samp_smooth
    if IS_MODAL:
        Xt_tmp = Xt_unb_samp_smooth
        nsteps = Xt_unb_samp_smooth.shape[1]
        Xt_unb_samp_smooth = np.zeros((m, nsteps, target_distribution.modal_map.dim_out))
        for i in range(Xt_unb_samp_smooth.shape[1]):
            Xt_unb_samp_smooth[:,i,:] = target_distribution.modal_map.evaluate( Xt_tmp[:,i,:] )
    start = 0
    i = 0
    while start < nsteps:
        stop = min(start + CHUNK_SIZE, nsteps)
        smps = Xt_samp_smooth[:,start:stop,:]
        unb_smps = Xt_unb_samp_smooth[:,start:stop,:]
        tt = tvec[start:stop]
        fig = plot_mean_percentile_traj(
            tt, smps, unb_smps, ntraj=NTRAJ,
            title="Posterior/Smoothing marginals - vs. unbiased")
        if PLOT: plt.show(False)

        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/' + \
                         'smoothing-marginals-vs-unbiased-timesteps-chunk-%d' % i)
        start = stop
        i += 1
        
if DO_FILT or DO_FILT_RMSE:
    fdata = h5data['filtering']
    nsamps = np.min(
        [ fdata['step-%i/quadrature/0' %i].shape[0]
          for i in range(nsteps) ])
    dimfilt = target_distribution.prior.state_dim
    samp_filt = np.zeros((nsamps,nsteps,dimfilt))
    for n in range(nsteps):
        samp_filt[:,n,:] = fdata['step-%d/quadrature/0' % n][:nsamps,:]

#-----###### Report time-averaged RMSE for the filtering distribution ##########
if DO_FILT_RMSE:
    print()
    print("Reporting time-averaged RMSE of the filtering distributions " + \
          "(complete time series)")
    m = samp_filt.shape[0]
    Xt = samp_filt
    if IS_MODAL:
        Xt_tmp = Xt
        nsteps = Xt.shape[1]
        Xt = np.zeros((m, nsteps, target_distribution.modal_map.dim_out))
        for i in range(Xt.shape[1]):
            Xt[:,i,:] = target_distribution.modal_map.evaluate( Xt_tmp[:,i,:] )
    mean_Xt = np.mean(Xt, axis=0)[RMSE_BURNIN:,:]  # nstep x dim_state
    true_Xt = np.asarray(target_distribution.xs)[RMSE_BURNIN:,:] # nsteps x dim_state
    diff_Xt = true_Xt - mean_Xt
    rmse = npla.norm( diff_Xt, axis=1 ) / np.sqrt(dim_state)
    avg_rmse = np.mean( rmse, axis=0 )
    med_rmse = np.median( rmse, axis=0 )
    var_rmse = np.var( rmse, axis=0 )
    print("    Average RMSE:   %e" % avg_rmse)
    print("    Median RMSE:    %e" % med_rmse)
    print("    Variance RMSE:  %e" % var_rmse)
    print()
    # # Plot RMSE vs Var-diagnostic vs number of coefficients (in filtering map)
    # var_diag = [ stg.integrator.var_diag_convergence[i-1] for i in range(stg.integrator.nsteps)
    #              if i >= RMSE_BURNIN ]
    # n_coeffs = [ stg.integrator.filtering_map_list[i].n_coeffs for i in range(stg.integrator.nsteps+1)
    #              if i >= RMSE_BURNIN ]
    # plot_rmse_vardiag_ncoeffs(
    #     [tvec[i] for i in range(stg.integrator.nsteps+1) if i >= RMSE_BURNIN],
    #     rmse, var_diag, n_coeffs)
    # if PLOT: plt.show(False)
    print("Reporting time-averaged RMSE of the filtering distribution " + \
          "(at observation times)")
    idxs = [ i - RMSE_BURNIN
             for i, (ll,_) in enumerate(target_distribution.logL.factors) 
             if ll is not None and i >= RMSE_BURNIN]
    diff_Xt = true_Xt[idxs,:] - mean_Xt[idxs,:]
    rmse = npla.norm( diff_Xt, axis=1 ) / np.sqrt(dim_state)
    avg_rmse = np.mean( rmse, axis=0 )
    med_rmse = np.median( rmse, axis=0 )
    var_rmse = np.var( rmse, axis=0 )
    print("    Average RMSE:   %e" % avg_rmse)
    print("    Median RMSE:    %e" % med_rmse)
    print("    Variance RMSE:  %e" % var_rmse)
    print()
    # # Plot RMSE vs Var-diagnostic vs number of coefficients (in filtering map)
    # var_diag = [ stg.integrator.var_diag_convergence[i-1+RMSE_BURNIN]
    #              for i in idxs if i > 0 ]
    # n_coeffs = [ stg.integrator.filtering_map_list[i-1+RMSE_BURNIN].n_coeffs
    #              for i in idxs ]
    # plot_rmse_vardiag_ncoeffs(
    #     [tvec[i+RMSE_BURNIN] for i in idxs],
    #     rmse, var_diag, n_coeffs)
    # if PLOT: plt.show(False)
        
#--2--###### Plot: filtering marginals timesteps #############
if DO_FILT:
    print("Plotting filtering marginals \n")
    Xt_samp_filt = samp_filt
    if IS_MODAL:
        Xt_tmp = Xt_samp_filt
        nsteps = Xt_samp_filt.shape[1]
        Xt_samp_filt = np.zeros((m, nsteps, target_distribution.modal_map.dim_out))
        for i in range(Xt.shape[1]):
            Xt_samp_filt[:,i,:] = target_distribution.modal_map.evaluate( Xt_tmp[:,i,:] )
    start = DO_FILT_BURNIN
    i = 0
    while start < nsteps:
        stop = min(start + CHUNK_SIZE, nsteps)
        smps = Xt_samp_filt[:,start:stop,:]
        tt = tvec[start:stop]
        truth = xs[start:stop,:]

        # Plot mean
        mean = np.mean(smps, axis=0)
        fig = plot_sample( tt, truth, mean,
                           data_title="Mean filtering")
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/filtering-marginals-timesteps-mean-chunk-%d' % i)

        # Plot mean error
        mean_err = np.abs( truth - mean )
        fig = plot_sample( tt, truth, mean_err,
                           data_title="Error in mean filtering",
                           single_cmap=False)
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/filtering-marginals-timesteps-error-mean-chunk-%d' % i)
        

        # Plot median
        med = np.mean(smps, axis=0)
        fig = plot_sample( tt, truth, med,
                           data_title="Median filtering")
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/filtering-marginals-timesteps-median-chunk-%d' % i)

        # Plot median error
        med_err = np.abs( truth - med )
        fig = plot_sample( tt, truth, med_err,
                           data_title="Error in median filtering",
                           single_cmap=False)
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/filtering-marginals-timesteps-error-median-chunk-%d' % i)

        # Plot standard deviation
        std = np.std(smps, axis=0)
        fig = plot_sample( tt, truth, std,
                           data_title="Standard deviation filtering")
        if PLOT: plt.show(False)
        if STORE_FIG_DIR is not None:
            store_figure(fig, STORE_FIG_DIR+'/filtering-marginals-timesteps-std-chunk-%d' % i)

        # Plot trajectories
        for nt in range(NTRAJ):
            trj = smps[nt,:,:]
            fig = plot_sample( tt, truth, med,
                               data_title="Filtering trajectory %d" % nt)
            if PLOT: plt.show(False)
            if STORE_FIG_DIR is not None:
                store_figure(fig, STORE_FIG_DIR+'/filtering-traj-%d-timesteps-chunk-%d' % (nt, i))
        
        start = stop
        i += 1

# #--3--###### Plot: smoothing+filtering marginals timesteps #############
# if DO_SMOOTH and DO_FILT:
#     print("Plotting smoothing + filtering marginals \n")
#     start = 0
#     i = 0
#     while start < nsteps:
#         stop = min(start + CHUNK_SIZE, nsteps)
#         smps1 = Xt_samp_smooth[:,start:stop]
#         smps2 = Xt_samp_filt[:,start:stop]
#         tt = tvec[start:stop]
#         et = [t for t in events_dates if tt[0] <= t <= tt[-1]]
#         fig = plot_mean_percentile_traj(
#             tt, smps1, smps2, edates=et,
#             ylabel=r"$" + STATE_VAR + "$",
#             title='Smoothing and filtering marginals', c2='b')
#         if PLOT: plt.show(False)

#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/smoothing-filtering-marginals-timesteps-chunk-%d' % i)
#         start = stop
#         i += 1

# #--4--###### Plot: filtering marginals hyper-parameters #############
# if DO_HYPER_FILT and nhyper>0:
#     hyper_samp_filt = samp_filt[:,:nhyper,:]
#     if target_distribution.is_sigma_hyper:
#         hyper_samp_filt[:,index_sigma,:] = \
#             target_distribution.sigma.evaluate( hyper_samp_filt[:,index_sigma,:] )
#     if target_distribution.is_phi_hyper:
#         hyper_samp_filt[:,index_phi,:] = \
#             target_distribution.phi.evaluate( hyper_samp_filt[:,index_phi,:] )
    
#     print("Plotting hyper-parameters filtering marginals \n")
#     if target_distribution.is_mu_hyper:
#         #Plot filtering marginals for mu
#         mu_samp_filt = hyper_samp_filt[:,index_mu,:]
#         start = 0
#         i = 0
#         while start < nsteps:
#             stop = min(start + CHUNK_SIZE, nsteps)
#             smps = mu_samp_filt[:,start:stop]
#             tt = tvec[start:stop]
#             et = [t for t in events_dates if tt[0] <= t <= tt[-1]]
#             fig = plot_mean_percentile_traj(
#                 tt, smps, ntraj=0, edates=et,
#                 ylabel="$\pi_{\mu|Y_{0:t}}$",
#                 title='Filtering marginals of $\mu$')
#             if PLOT: plt.show(False)
#             if STORE_FIG_DIR is not None:
#                 store_figure(
#                     fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_mu-chunk-%d' % i)
#             start = stop
#             i += 1

#     if target_distribution.is_sigma_hyper:
#         #Plot filtering marginals for sigma
#         sigma_samp_filt = hyper_samp_filt[:,index_sigma,:]
#         start = 0
#         i = 0
#         while start < nsteps:
#             stop = min(start + CHUNK_SIZE, nsteps)
#             smps = sigma_samp_filt[:,start:stop]
#             tt = tvec[start:stop]
#             et = [t for t in events_dates if tt[0] <= t <= tt[-1]]
#             fig = plot_mean_percentile_traj(
#                 tt, smps, ntraj=0, edates=et,
#                 ylabel=r"$\pi_{\sigma|Y_{0:t}}$",
#                 title=r'Filtering marginals of $\sigma$')
#             if PLOT: plt.show(False)
#             if STORE_FIG_DIR is not None:
#                 store_figure(
#                     fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_sigma-chunk-%d' % i)
#             start = stop
#             i += 1

#     if target_distribution.is_phi_hyper:
#         #Plot filtering marginals for phi
#         phi_samp_filt = hyper_samp_filt[:,index_phi,:]
#         start = 0
#         i = 0
#         while start < nsteps:
#             stop = min(start + CHUNK_SIZE, nsteps)
#             smps = phi_samp_filt[:,start:stop]
#             tt = tvec[start:stop]
#             et = [t for t in events_dates if tt[0] <= t <= tt[-1]]
#             fig = plot_mean_percentile_traj(
#                 tt, smps, ntraj=0, edates=et,
#                 ylabel=r"$\pi_{\phi|Y_{0:t}}$",
#                 title=r'Filtering marginals of $\sigma$')
#             if PLOT: plt.show(False)
#             if STORE_FIG_DIR is not None:
#                 store_figure(
#                     fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_phi-chunk-%d' % i)
#             start = stop
#             i += 1
        
# if DO_HYPER_UNB_FILT and nhyper>0:
#     tar_unb_samp_smooth_trim = []
#     for t in TRIM_TS:
#         h5root = h5data['trim-%d' % t]
#         tar_unb_samp_smooth_trim.append(
#             h5root['metropolis-independent-proposal-samples/skip-%d/x/' % MCMC_SKIP]
#         )
#     print("Plotting hyper-parameters filtering marginals vs. unbiased \n")
#     if target_distribution.is_mu_hyper:
#         # Plot filtering marginals for mu (3D)
#         index_mu = target_distribution.index_mu
#         mu_samp_filt = hyper_samp_filt[:,index_mu,:]
#         mu_unb_samp_smooth = [ samp[:,index_mu] for samp in tar_unb_samp_smooth_trim ]
#         ylabel = "" # r"$\mu\vert Y_{0:t}$"
#         zlabel = r"$\pi_{\mu\vert Y_{0:t}}$"
#         fig = plot_mean_percentile_traj_3d(
#             mu_samp_filt, TRIM_TS, mu_unb_samp_smooth,
#             ylabel=ylabel, zlabel=zlabel)
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_mu-3d')
#         fig = plot_mean_percentile_traj_boxplot(
#             mu_samp_filt, TRIM_TS, mu_unb_samp_smooth,
#             ylabel=ylabel)
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_mu-boxplot')
            
#     if target_distribution.is_sigma_hyper:
#         # Plot filtering marginals for mu (3D)
#         index_sigma = target_distribution.index_sigma
#         sigma_samp_filt = hyper_samp_filt[:,index_sigma,:]
#         sigma_unb_samp_smooth = [
#             target_distribution.sigma.evaluate( samp[:,index_sigma] )
#             for samp in tar_unb_samp_smooth_trim ]
#         ylabel = "" # r"$\sigma\vert Y_{0:t}$"
#         zlabel = r"$\pi_{\sigma\vert Y_{0:t}}$"
#         fig = plot_mean_percentile_traj_3d(
#             sigma_samp_filt, TRIM_TS, sigma_unb_samp_smooth,
#             ylabel=ylabel, zlabel=zlabel)
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_sigma-3d')
#         fig = plot_mean_percentile_traj_boxplot(
#             sigma_samp_filt, TRIM_TS, sigma_unb_samp_smooth,
#             ylabel=ylabel)
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_sigma-boxplot')
            
#     if target_distribution.is_phi_hyper:
#         # Plot filtering marginals for mu (3D)
#         index_phi = target_distribution.index_phi
#         phi_samp_filt = hyper_samp_filt[:,index_phi,:]
#         phi_unb_samp_smooth = [
#             target_distribution.phi.evaluate( samp[:,index_phi] )
#             for samp in tar_unb_samp_smooth_trim ]
#         ylabel = "" # r"$\phi\vert Y_{0:t}$"
#         zlabel = r"$\pi_{\phi\vert Y_{0:t}}$"
#         fig = plot_mean_percentile_traj_3d(
#             phi_samp_filt, TRIM_TS, phi_unb_samp_smooth,
#             ylabel=ylabel, zlabel=zlabel)
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_phi-3d')
#         fig = plot_mean_percentile_traj_boxplot(
#             phi_samp_filt, TRIM_TS, phi_unb_samp_smooth,
#             ylabel=ylabel)
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/filtering-marginals-timesteps_phi-boxplot')
            
# #--4--###### Plot: smoothing/posterior marginals hyper-parameters #############
# if DO_HYPER_SMOOTH and nhyper>0:
#     tar_samp_smooth = h5data['trim-%d' % nsteps]['quadrature']['approx-target']['0']
#     print("Plotting hyper-parameters smoothing marginals \n")
#     if target_distribution.is_mu_hyper:
#         mu_samp_smooth = tar_samp_smooth[:,index_mu]
#         mu_min = np.min(mu_samp_smooth)
#         mu_min = mu_min - .1*np.abs(mu_min)
#         mu_max = np.max(mu_samp_smooth)
#         mu_max = mu_max + .1*np.abs(mu_max)
#         mu_kde = scistat.gaussian_kde(mu_samp_smooth)
#         mu_xx = np.linspace(mu_min, mu_max, 10000)
#         pdf_mu_xx = mu_kde(mu_xx)
#         fig = plt.figure()
#         plt.xlabel("$\mu$")
#         plt.ylabel("$\pi_{\mu|Y_{0:N}}$")
#         if TITLES:
#             plt.title('Posterior/smoothing marginals of $\mu$')
#         plt.xlim(mu_min,mu_max)
#         plt.plot(mu_xx, pdf_mu_xx, '-k',linewidth=1.0)
#         if PLOT: plt.show(False)

#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/smoothing-marginals-mu') 

#     if target_distribution.is_sigma_hyper:
#         Xsigma_samp_smooth =  tar_samp_smooth[:,index_sigma]
#         # Do the kde on the reference variable and then push forward through F_sigma.
#         Xsigma_kde = scistat.gaussian_kde(Xsigma_samp_smooth) 
#         Xsigma_min = np.min(Xsigma_samp_smooth)
#         Xsigma_min = Xsigma_min - .1*np.abs(Xsigma_min)
#         Xsigma_max = np.max(Xsigma_samp_smooth)
#         Xsigma_max = Xsigma_max + .1*np.abs(Xsigma_max)
#         Xsigma_xx = np.linspace(Xsigma_min, Xsigma_max, 10000)
#         FXsigma_xx = target_distribution.sigma.evaluate( Xsigma_xx )
#         grad_FXsigma_xx = target_distribution.sigma.grad_x( Xsigma_xx )
#         pdf_sigma_FXsigma = Xsigma_kde(Xsigma_xx)/grad_FXsigma_xx
#         fig = plt.figure()
#         plt.xlabel("$\sigma$")
#         plt.ylabel("$\pi_{\sigma|Y_{0:N}}$")
#         if TITLES:
#             plt.title('Posterior/smoothing marginals of $\sigma$')
#         sigma_max = np.max(FXsigma_xx)
#         ax.set_xlim(0.,sigma_max)
#         ax.plot(FXsigma_xx, pdf_sigma_FXsigma, '-k',linewidth=1.0)
#         if PLOT: plt.show(False)

#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/smoothing-marginals-sigma') 

#     if target_distribution.is_phi_hyper:
#         Xphi_samp_smooth =  target_distribution.phi.evaluate(
#             tar_samp_smooth[:,index_phi])
#         Xphi_kde = scistat.gaussian_kde(Xphi_samp_smooth)
#         Xphi_min = np.min(Xphi_samp_smooth)
#         Xphi_max = np.max(Xphi_samp_smooth)
#         Xphi_xx = np.linspace(Xphi_min, Xphi_max, 1000)
#         pdf_phi = Xphi_kde(Xphi_xx)
#         fig = plt.figure()
#         plt.ylabel("$\pi_{\phi|Y_{0:N}}$")
#         if TITLES:
#             plt.title('Posterior/smoothing marginals of $\phi$')
#         plt.plot(Xphi_xx, pdf_phi, '-k',linewidth=1.0, label='Transport Map')
#         plt.legend(loc='best')
#         if PLOT: plt.show(False)
        
#         # Xphi_samp_smooth =  tar_samp_smooth[:,index_phi]
#         # Xphi_kde = scistat.gaussian_kde(Xphi_samp_smooth)
#         # #Do the kde on the reference variable and then push forward through F_phi.
#         # Xphi_min = np.min(Xphi_samp_smooth)
#         # Xphi_min = Xphi_min # - .1*np.abs(Xphi_min)
#         # Xphi_max = np.max(Xphi_samp_smooth)
#         # Xphi_max = Xphi_max # + .1*np.abs(Xphi_max)
#         # Xphi_xx = np.linspace(Xphi_min, Xphi_max, 10000)
#         # FXphi_xx = target_distribution.phi.evaluate( Xphi_xx )
#         # grad_FXphi_xx = target_distribution.phi.grad_x( Xphi_xx )
#         # pdf_phi_FXphi = Xphi_kde(Xphi_xx)/grad_FXphi_xx
#         # fig = plt.figure()
#         # plt.xlabel("$\phi$")
#         # plt.ylabel("$\pi_{\phi|Y_{0:N}}$")
#         # if TITLES:
#         #     plt.title('Posterior/smoothing marginals of $\phi$')
#         # phi_min = np.min(FXphi_xx)
#         # plt.xlim(phi_min,1.)
#         # plt.plot(FXphi_xx, pdf_phi_FXphi, '-k',linewidth=1.0)
#         # if PLOT: plt.show(False)
#         # #phi_samp_smooth = target_distribution.phi.evaluate( Xphi_samp_smooth )

#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/smoothing-marginals-phi')

#     print("Plotting posterior/smoothing marginals \n")
#     hyper_samp_smooth = tar_samp_smooth[:,:nhyper].copy()
#     if target_distribution.is_sigma_hyper:
#         hyper_samp_smooth[:,index_sigma] = target_distribution.sigma.evaluate(
#             hyper_samp_smooth[:,index_sigma] )
#     if target_distribution.is_phi_hyper:
#         hyper_samp_smooth[:,index_phi] = target_distribution.phi.evaluate(
#             hyper_samp_smooth[:,index_phi] )
#     (fig, handles,_,_,_) = DIAG.plotAlignedMarginals(
#         hyper_samp_smooth, show_axis=True )
#     if PLOT: plt.show(False)
#     if STORE_FIG_DIR is not None:
#         store_figure(
#             fig, STORE_FIG_DIR+ '/smoothing-marginals-hyper')

# if DO_HYPER_SMOOTH and DO_UNBIASED_SMOOTH and nhyper > 0:
#     print("Plotting hyper-parameters unbiased smoothing marginals \n")
#     hyper_unb_samp_smooth = tar_unb_samp_smooth[:,:nhyper].copy()
#     if target_distribution.is_sigma_hyper:
#         hyper_unb_samp_smooth[:,index_sigma] = target_distribution.sigma.evaluate(
#             hyper_unb_samp_smooth[:,index_sigma] )
#     if target_distribution.is_phi_hyper:
#         hyper_unb_samp_smooth[:,index_phi] = target_distribution.phi.evaluate(
#             hyper_unb_samp_smooth[:,index_phi] )
#     # (fig, handles,_,_,_) = DIAG.plotAlignedMarginals(
#     #     hyper_unb_samp_smooth, show_axis=True )
#     (fig, handles,_,_,_) = DIAG.plotAlignedMarginals(
#         hyper_samp_smooth, hyper_unb_samp_smooth, show_axis=True )
#     if PLOT: plt.show(False)
#     if STORE_FIG_DIR is not None:
#         store_figure(
#             fig, STORE_FIG_DIR+ '/unbiased-smoothing-marginals-hyper')

    
#     print("Plotting hyper-parameters smoothing marginals vs. unbiased \n")
#     if target_distribution.is_mu_hyper:
#         mu_samp_smooth = tar_samp_smooth[:,index_mu]
#         mu_unb_samp_smooth = tar_unb_samp_smooth[:,index_mu]
#         mu_min = min(np.min(mu_samp_smooth), np.min(mu_unb_samp_smooth))
#         mu_min = mu_min - .1*np.abs(mu_min)
#         mu_max = max(np.max(mu_samp_smooth), np.max(mu_unb_samp_smooth))
#         mu_max = mu_max + .1*np.abs(mu_max)
#         mu_kde = scistat.gaussian_kde(mu_samp_smooth)
#         mu_unb_kde = scistat.gaussian_kde(mu_unb_samp_smooth)
#         mu_xx = np.linspace(mu_min, mu_max, 10000)
#         pdf_mu_xx = mu_kde(mu_xx)
#         pdf_mu_unb_xx = mu_unb_kde(mu_xx)
#         fig = plt.figure()
#         if TITLES:
#             plt.title('Posterior/smoothing marginals of $\mu$')
#         plt.ylabel("$\pi_{\mu|Y_{0:N}}$")
#         plt.xlim(mu_min,mu_max)
#         plt.plot(mu_xx, pdf_mu_xx, '-k',linewidth=1.0, label='Transport map')
#         plt.plot(mu_xx, pdf_mu_unb_xx, '--k',linewidth=1.0, label='MCMC')
#         plt.legend(loc='best')
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/smoothing-marginals-mu-vs-unb') 

#     if target_distribution.is_sigma_hyper:
#         Xsigma_samp_smooth =  tar_samp_smooth[:,index_sigma]
#         Xsigma_unb_samp_smooth = tar_unb_samp_smooth[:,index_sigma]
#         # Do the kde on the reference variable and then push forward through F_sigma.
#         Xsigma_kde = scistat.gaussian_kde(Xsigma_samp_smooth)
#         Xsigma_unb_kde = scistat.gaussian_kde(Xsigma_unb_samp_smooth) 
#         Xsigma_min = min(np.min(Xsigma_samp_smooth), np.min(Xsigma_unb_samp_smooth))
#         Xsigma_min = Xsigma_min - .1*np.abs(Xsigma_min)
#         Xsigma_max = max(np.max(Xsigma_samp_smooth), np.max(Xsigma_unb_samp_smooth))
#         Xsigma_max = Xsigma_max + .1*np.abs(Xsigma_max)
#         Xsigma_xx = np.linspace(Xsigma_min, Xsigma_max, 10000)
#         FXsigma_xx = target_distribution.sigma.evaluate( Xsigma_xx )
#         grad_FXsigma_xx = target_distribution.sigma.grad_x( Xsigma_xx )
#         pdf_sigma_FXsigma = Xsigma_kde(Xsigma_xx)/grad_FXsigma_xx
#         pdf_unb_sigma_FXsigma = Xsigma_unb_kde(Xsigma_xx)/grad_FXsigma_xx
#         fig = plt.figure()
#         plt.ylabel("$\pi_{\sigma|Y_{0:N}}$")
#         if TITLES:
#             plt.title('Posterior/smoothing marginals of $\sigma$')
#         sigma_max = np.max(FXsigma_xx)
#         plt.xlim(0.,sigma_max)
#         plt.plot(FXsigma_xx, pdf_sigma_FXsigma, '-k',linewidth=1.0, label='Transport Map')
#         plt.plot(FXsigma_xx, pdf_unb_sigma_FXsigma, '--k',
#                  linewidth=1.0, label='MCMC')
#         plt.legend(loc='best')
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/smoothing-marginals-sigma-vs-unb') 

#     if target_distribution.is_phi_hyper:
#         Xphi_samp_smooth =  target_distribution.phi.evaluate(
#             tar_samp_smooth[:,index_phi])
#         Xphi_unb_samp_smooth =  target_distribution.phi.evaluate(
#             tar_unb_samp_smooth[:,index_phi])
#         Xphi_kde = scistat.gaussian_kde(Xphi_samp_smooth)
#         Xphi_unb_kde = scistat.gaussian_kde(Xphi_unb_samp_smooth)
#         Xphi_min = min(np.min(Xphi_samp_smooth), np.min(Xphi_unb_samp_smooth))
#         Xphi_min = Xphi_min # - .1*np.abs(Xphi_min)
#         Xphi_max = max(np.max(Xphi_samp_smooth), np.max(Xphi_unb_samp_smooth))
#         Xphi_max = Xphi_max # + .1*np.abs(Xphi_max)
#         Xphi_xx = np.linspace(Xphi_min, Xphi_max, 1000)
#         pdf_phi = Xphi_kde(Xphi_xx)
#         pdf_unb_phi = Xphi_unb_kde(Xphi_xx)
#         fig = plt.figure()
#         plt.ylabel("$\pi_{\phi|Y_{0:N}}$")
#         if TITLES:
#             plt.title('Posterior/smoothing marginals of $\phi$')
#         plt.plot(Xphi_xx, pdf_phi, '-k',linewidth=1.0, label='Transport Map')
#         plt.plot(Xphi_xx, pdf_unb_phi, '--k',linewidth=1.0, label='MCMC')
#         plt.legend(loc='best')
#         if PLOT: plt.show(False)
#         if STORE_FIG_DIR is not None:
#             store_figure(
#                 fig, STORE_FIG_DIR+ '/smoothing-marginals-phi-vs-unb')
        
#         # Xphi_samp_smooth =  tar_samp_smooth[:,index_phi]
#         # Xphi_unb_samp_smooth =  tar_unb_samp_smooth[:,index_phi]
#         # #Do the kde on the reference variable and then push forward through F_phi.
#         # Xphi_kde = scistat.gaussian_kde(Xphi_samp_smooth)
#         # Xphi_unb_kde = scistat.gaussian_kde(Xphi_unb_samp_smooth) 
#         # Xphi_min = min(np.min(Xphi_samp_smooth), np.min(Xphi_unb_samp_smooth))
#         # Xphi_min = Xphi_min # - .1*np.abs(Xphi_min)
#         # Xphi_max = max(np.max(Xphi_samp_smooth), np.max(Xphi_unb_samp_smooth))
#         # Xphi_max = Xphi_max # + .1*np.abs(Xphi_max)
#         # Xphi_xx = np.linspace(Xphi_min, Xphi_max, 1000)
#         # FXphi_xx = target_distribution.phi.evaluate( Xphi_xx )
#         # grad_FXphi_xx = target_distribution.phi.grad_x( Xphi_xx )
#         # pdf_phi_FXphi = Xphi_kde(Xphi_xx)/grad_FXphi_xx
#         # pdf_unb_phi_FXphi = Xphi_unb_kde(Xphi_xx)/grad_FXphi_xx
#         # fig = plt.figure()
#         # plt.ylabel("$\pi_{\phi|Y_{0:N}}$")
#         # if TITLES:
#         #     plt.title('Posterior/smoothing marginals of $\phi$')
#         # phi_min = np.min(FXphi_xx)
#         # plt.plot(FXphi_xx, pdf_phi_FXphi, '-k',linewidth=1.0, label='Transport Map')
#         # plt.plot(FXphi_xx, pdf_unb_phi_FXphi, '--k',linewidth=1.0, label='MCMC')
#         # plt.legend(loc='best')
#         # if PLOT: plt.show(False)
#         # #phi_samp_smooth = target_distribution.phi.evaluate( Xphi_samp_smooth )
#         # if STORE_FIG_DIR is not None:
#         #     store_figure(
#         #         fig, STORE_FIG_DIR+ '/smoothing-marginals-phi-vs-unb')

# #--7--###### Plot: posterior data predictive #############
# def plot_post_pred(obs, data1, data2=None, edates=None, ylabel=None, title=None):
#     fig = plt.figure(figsize=FSIZE)
#     ax = fig.add_subplot(111)
#     nicePlot(ax)
#     if ylabel is not None:
#         ax.set_ylabel(ylabel)
#     if TITLES:
#         plt.title(title)
    
#     q5_1 = np.percentile(data1, 5, axis=0)
#     q25_1 = np.percentile(data1, 25, axis=0)
#     q40_1 = np.percentile(data1, 40, axis=0)
#     q60_1 = np.percentile(data1, 60, axis=0)
#     q75_1 = np.percentile(data1, 75, axis=0)
#     q95_1 = np.percentile(data1, 95, axis=0)
#     ax.set_xlim(tvec[0],tvec[-1])
#     ax.scatter(tvec,obs,s=0.5,c='k',label="data")
#     ax.fill_between(tvec, q40_1, q60_1, color="r", alpha=0.6)
#     ax.fill_between(tvec, q25_1, q75_1, color="r", alpha=0.3)
#     ax.fill_between(tvec, q5_1, q95_1, color="r", alpha=0.1)

#     if data2 is not None:
#         q5_2 = np.percentile(data2, 5, axis=0)
#         q25_2 = np.percentile(data2, 25, axis=0)
#         q40_2 = np.percentile(data2, 40, axis=0)
#         q60_2 = np.percentile(data2, 60, axis=0)
#         q75_2 = np.percentile(data2, 75, axis=0)
#         q95_2 = np.percentile(data2, 95, axis=0)
#         ax.fill_between(tvec, q40_2, q60_2, color="b", alpha=0.6)
#         ax.fill_between(tvec, q25_2, q75_2, color="b", alpha=0.3)
#         ax.fill_between(tvec, q5_2, q95_2, color="b", alpha=0.1)

#     if EXCHANGES_DATA is not None:
#         ax.set_xlabel("day")
#         ax.xaxis_date()
#         ax.xaxis.set_major_formatter(mdates.DateFormatter('%m/%d/%y'))
#         fig.autofmt_xdate()
#     else:
#         ax.set_xlabel("time")
#     if EVENTS_DATA is not None:
#         for dt in edates:
#             ax.plot([dt,dt], YLIM, '--k')
    
#     ax.legend(loc='best')
#     return fig

def plot_qq_post_pred(obs, data, ylabel=None, xlabel=None, title=None):
    nprc = 21
    prclst = np.linspace(0,100,nprc)

    prc = np.zeros((nprc,data.shape[1]))
    for n, p in enumerate(prclst):
        prc[n,:] = np.percentile(data, p, axis=0)

    nobs_in_prc = np.zeros(nprc)
    for n, p in enumerate(prclst):
        nobs_in_prc[n] = sum( obs < prc[n,:] )
        
    # qq-plot and identity tendency line
    fig = plt.figure()
    plt.scatter(prclst/100.,
                nobs_in_prc/float(len(obs)), s=5, c='k')
    plt.plot([0,1],[0,1],'r')
    if ylabel is not None:
        plt.ylabel(ylabel)
    if xlabel is not None:
        plt.xlabel(xlabel)
    if TITLES:
        plt.title(title)

    # # Perform PCA
    # P = np.vstack((p1,p2)).T
    # mP = np.mean(P,axis=0)
    # plt.scatter(mP[0],mP[1], s=15, c='k', marker='s')
    # X = P - mP
    # V = np.dot(X.T,X)
    # w,v = npla.eig(V)
    # srtidxs = np.argsort(w)[::-1]
    # w = w[srtidxs]
    # v = v[:,srtidxs]
    # v0 = v[:,0]
    # v1 = v[:,1]
    # yy = mP[1] + v0[1]/v0[0] * (xx - mP[0])
    # prjv1 = np.outer(v1,v1)
    # res = np.dot(prjv1, X.T).T
    # stdres = np.std(npla.norm(res,axis=1))
    # xy = np.vstack( (xx,yy) )
    # xyp = xy + 1.96 * stdres * v1[:,np.newaxis]
    # xym = xy - 1.96 * stdres * v1[:,np.newaxis]
    # # Plot PCA and 95% confidence interval
    # plt.plot(xx, yy,'k')
    # plt.plot(xyp[0,:],xyp[1,:],'--k')
    # plt.plot(xym[0,:],xym[1,:],'--k')
    return fig
    
if DO_POST_PRED:
    tar_samp_smooth = h5data['trim-%d' % nsteps]['quadrature']['approx-target']['0']
    Xt_samp_smooth = tar_samp_smooth[:,nhyper:]
    noise_samples = npr.randn(Xt_samp_smooth.shape[0],Xt_samp_smooth.shape[1])
    Ysmooth_samples = noise_samples*np.exp(.5*Xt_samp_smooth)
    print("Plotting posterior predictive \n")
    fig = plot_post_pred(
        obs, Ysmooth_samples, ylabel="$\pi_{Y_y|Y_{0:N}}$",
        title="Data Posterior/Smoothing marginals (posterior predictive)")
    if PLOT: plt.show(False)
    if STORE_FIG_DIR is not None:
        store_figure(
            fig, STORE_FIG_DIR+ '/posterior-data-predictive')

    fig = plot_qq_post_pred(obs, Ysmooth_samples,
                            xlabel='quantile', ylabel='observations',
                            title='qq-plot')
    if PLOT: plt.show(False)
    if STORE_FIG_DIR is not None:
        store_figure(
            fig, STORE_FIG_DIR + '/qq-plot-posterior-data-predictive')

if DO_UNB_POST_PRED:
    Xt_samp_smooth = tar_unb_samp_smooth[:,nhyper:]
    noise_samples = npr.randn(Xt_samp_smooth.shape[0],Xt_samp_smooth.shape[1])
    unb_Ysmooth_samples = noise_samples*np.exp(.5*Xt_samp_smooth)
    print("Plotting unbiased posterior predictive \n")
    fig = plot_post_pred(
        obs, unb_Ysmooth_samples, ylabel="$\pi_{Y_y|Y_{0:N}}$",
        title="Data Posterior/Smoothing marginals (posterior predictive)")
    if PLOT: plt.show(False)
    if STORE_FIG_DIR is not None:
        store_figure(
            fig, STORE_FIG_DIR+ '/posterior-data-predictive-unbiased')

    fig = plot_qq_post_pred(obs, unb_Ysmooth_samples,
                            xlabel='quantile', ylabel='observations',
                            title='qq-plot')
    if PLOT: plt.show(False)
    if STORE_FIG_DIR is not None:
        store_figure(
            fig, STORE_FIG_DIR + '/unb-qq-plot-posterior-data-predictive')

if DO_POST_PRED and DO_UNB_POST_PRED:
    print("Plotting posterior predictive vs. unbiased posterior predictive\n")
    fig = plot_post_pred(
        obs, Ysmooth_samples, unb_Ysmooth_samples,
        ylabel="$\pi_{Y_y|Y_{0:N}}$",
        title="Data Posterior/Smoothing marginals (posterior predictive)")
    if PLOT: plt.show(False)
    if STORE_FIG_DIR is not None:
        store_figure(
            fig, STORE_FIG_DIR+ '/posterior-data-predictive-vs-unbiased')

if DO_TRIM_VAR_DIAG:
    var_diag_list = []
    for t in TRIM_TS:
        h5root = h5data['trim-%d' % t]
        vals = h5root['vals_var_diag']['exact-base']['0']
        vals_d1 = vals['vals_d1'][:]
        vals_d2 = vals['vals_d2'][:]
        N = len(vals_d1)
        w = np.ones(N)/float(N)
        var_diag_list.append(
            DIAG.variance_approx_kl(
                None, None, vals_d1=vals_d1, vals_d2=vals_d2, w=w) )
    fig = plt.figure()
    ax = fig.add_subplot(111)
    nicePlot(ax)
    ax.semilogy(TRIM_TS, var_diag_list, '-ok')
    ax.grid(True)
    ax.set_ylabel(r"$\mathcal{D}_{\rm KL}(\rho \Vert T^\sharp \pi)$")
    ax.set_xlabel("time")
    if PLOT: plt.show(False)
    if STORE_FIG_DIR is not None:
        store_figure(
            fig, STORE_FIG_DIR+ '/var-diag-evolution')

