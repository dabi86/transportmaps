#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import numpy as np
import matplotlib.pyplot as plt

from auxmod import FadingIncreasingOrderBandedSequentialInferenceMapListFactory

STORE = False

n_band = 2
max_order = 1
alpha = 1.00
btype = 'fun'
bmf = FadingIncreasingOrderBandedSequentialInferenceMapListFactory(
    n_band, max_order, btype=btype, alpha=alpha )

# Plot example
nsteps = 1
sdim = 10
tm_lst = bmf.generate( nsteps=nsteps, sdim=sdim )
for i, tm in enumerate(tm_lst):
    print("Map %d - n. coeffs: %d" % (i, tm.n_coeffs) )

sp_strct = np.zeros(((nsteps+1)*sdim, (nsteps+1)*sdim), dtype=bool)
tm0 = tm_lst[0]
for d, avars in enumerate(tm0.active_vars):
    for v in avars:
        sp_strct[d, v] = True
plt.figure()
plt.spy(sp_strct)
plt.tick_params(
    axis='both',       # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labeltop=False,
    labelleft=False) # labels along the bottom edge are off
plt.show(False)
        
if STORE:
    fname = 'data/' + \
            'band-map-factory' + \
            '-n_band-%d' % n_band + \
            '-o-%d' % max_order + \
            '-alpha-%.2f' % alpha + \
            '-btype-' + btype + \
            '.dill'
    print("Storing in " + fname)
    bmf.store(fname)
