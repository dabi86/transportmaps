#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import SpectralToolbox.Spectral1D as S1D

from TransportMaps.Functionals.MonotonicFunctionApproximations import \
    MonotonicLinearSpanApproximation, MonotonicIntegratedSquaredApproximation
from src.TransportMaps.Maps import \
    IntegratedSquaredTriangularTransportMap
from src.TransportMaps.Maps import \
    FadingIncreasingOrderSequentialInferenceMapListFactory

__all__ = [
    'FadingIncreasingOrderLowRankSequentialInferenceMapListFactory',
    'FadingIncreasingOrderBandedSequentialInferenceMapListFactory',
    'FadingIncreasingOrderLowRankBandedSequentialInferenceMapListFactory',
]

class FadingIncreasingOrderLowRankSequentialInferenceMapListFactory(
        FadingIncreasingOrderSequentialInferenceMapListFactory):
    r""" The maps generated have a lower-triangular head and diagonal tails
    """

    def generate(self, nsteps, sdim, rlst, **kwargs):
        if nsteps+1 < len(rlst):
            raise NotImplementedError(
                "The case with hyperparameters is not implemented yet")
            
        tm_list = []
        
        for order in range(1, self._max_order+1):

            components_list = []
            active_vars = []

            for s, r in enumerate(rlst):
            
                # map components related to step s
                for d in range(sdim):
                    bnavars = 0
                    if d < r:
                        avars = []
                        if s > 0:
                            avars.extend( range((s-1)*sdim,(s-1)*sdim+rlst[s-1]) )
                            bnavars = len(avars)
                        avars.extend( range(s*sdim,s*sdim+d+1) )
                        oo = int(round(order**(1./(1+self._alpha*d))))
                    else:
                        avars = [s*sdim + d]
                        oo = 1

                    navars = len(avars)

                    # Constant part
                    c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * navars
                    c_orders_list = [1] * bnavars + [oo] * (navars-bnavars-1) + [0]
                    # c_orders_list = [oo] * (navars-1) + [0]
                    c_approx = MonotonicLinearSpanApproximation(
                        c_basis_list, spantype='total', order_list=c_orders_list)

                    # Integrated squared part
                    e_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (navars-1) + \
                                   [S1D.ConstantExtendedHermiteProbabilistsFunction()]
                    e_orders_list = [0] * bnavars + [oo-1] * (navars-bnavars)
                    # e_orders_list = [oo-1] * (navars)
                    e_approx = MonotonicLinearSpanApproximation(
                        e_basis_list, spantype='total', order_list=e_orders_list)

                    Tk = MonotonicIntegratedSquaredApproximation(c_approx, e_approx)
                    components_list.append( Tk )
                    active_vars.append( avars )

            tm = IntegratedSquaredTriangularTransportMap(
                active_vars, components_list)
            tm_list.append( tm )

        return tm_list

class FadingIncreasingOrderBandedSequentialInferenceMapListFactory(
        FadingIncreasingOrderSequentialInferenceMapListFactory):
    r""" The maps generated are banded diagonal.
    """
    def __init__(self, n_band, *args, **kwargs):
        super(FadingIncreasingOrderBandedSequentialInferenceMapListFactory, self).__init__(
            *args, **kwargs)
        self._n_band = n_band

    @property
    def n_band(self):
        return self._n_band

    def generate(self, nsteps, sdim, hdim=0, **kwargs):
        if hdim > 0:
            raise NotImplementedError("Not implemented for hyper-parameters")
        
        tm_list = []
        
        for order in range(1, self._max_order+1):

            components_list = []
            active_vars = []

            for s in range(nsteps+1):
            
                # map components related to step s
                for d in range(sdim):

                    avars = []
                    if s > 0: # Add interaction block
                        avars.extend( range(
                            max( ((s-1)*sdim)+d-self._n_band, (s-1)*sdim ),
                            min( ((s-1)*sdim)+d+self._n_band, s*sdim ) + 1
                        ) )
                    avars.extend( range(
                        max( s*sdim+d-self._n_band, s*sdim ),
                        s*sdim+d+1
                    ) )
                    oo = int(round(order**(1./(1+self._alpha*d))))
                    
                    navars = len(avars)

                    # Constant part
                    c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * navars
                    c_orders_list = [oo] * (navars-1) + [0]
                    c_approx = MonotonicLinearSpanApproximation(
                        c_basis_list, spantype='total', order_list=c_orders_list)

                    # Integrated squared part
                    e_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (navars-1) + \
                                   [S1D.ConstantExtendedHermiteProbabilistsFunction()]
                    e_orders_list = [oo-1] * navars
                    e_approx = MonotonicLinearSpanApproximation(
                        e_basis_list, spantype='total', order_list=e_orders_list)

                    Tk = MonotonicIntegratedSquaredApproximation(c_approx, e_approx)
                    components_list.append( Tk )
                    active_vars.append( avars )

            tm = IntegratedSquaredTriangularTransportMap(
                active_vars, components_list)
            tm_list.append( tm )

        return tm_list


class FadingIncreasingOrderLowRankBandedSequentialInferenceMapListFactory(
        FadingIncreasingOrderSequentialInferenceMapListFactory):
    r""" The maps generated are banded diagonal.
    """
    def __init__(self, n_band, *args, **kwargs):
        super(FadingIncreasingOrderLowRankBandedSequentialInferenceMapListFactory,
              self).__init__(
            *args, **kwargs)
        self._n_band = n_band

    @property
    def n_band(self):
        return self._n_band

    def generate(self, nsteps, sdim, rlst, hdim=0, **kwargs):
        if hdim > 0:
            raise NotImplementedError("Not implemented for hyper-parameters")
        if nsteps+1 < len(rlst):
            raise NotImplementedError(
                "The case with hyperparameters is not implemented yet")
        
        tm_list = []
        
        for order in range(1, self._max_order+1):

            components_list = []
            active_vars = []

            for s, r in enumerate(rlst):
            
                # map components related to step s
                for d in range(sdim):

                    avars = []
                    if d < r:
                        if s > 0: # Add interaction block
                            avars.extend( range(
                                max( ((s-1)*sdim)+d-self._n_band, (s-1)*sdim ),
                                min( ((s-1)*sdim)+d+self._n_band, s*sdim ) + 1
                            ) )
                        avars.extend( range(
                            max( s*sdim+d-self._n_band, s*sdim ),
                            s*sdim + d + 1
                        ) )
                        oo = int(round(order**(1./(1+self._alpha*d))))
                    else:
                        avars = [s*sdim + d]
                        oo = 1
                    
                    navars = len(avars)

                    # Constant part
                    c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * navars
                    c_orders_list = [oo] * (navars-1) + [0]
                    c_approx = MonotonicLinearSpanApproximation(
                        c_basis_list, spantype='total', order_list=c_orders_list)

                    # Integrated squared part
                    e_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (navars-1) + \
                                   [S1D.ConstantExtendedHermiteProbabilistsFunction()]
                    e_orders_list = [oo-1] * navars
                    e_approx = MonotonicLinearSpanApproximation(
                        e_basis_list, spantype='total', order_list=e_orders_list)

                    Tk = MonotonicIntegratedSquaredApproximation(c_approx, e_approx)
                    components_list.append( Tk )
                    active_vars.append( avars )

            tm = IntegratedSquaredTriangularTransportMap(
                active_vars, components_list)
            tm_list.append( tm )

        return tm_list
