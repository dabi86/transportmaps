#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import numpy as np
import matplotlib.pyplot as plt

import SpectralToolbox.Spectral1D as S1D

import TransportMaps.Functionals as FUNC

state_dim = 40             # State dimension
order_diag = 1             # Polynomial order of the diagonal terms
order_off_diag = 1         # Polynomial order of the off-diagonal terms
order_inter_diag = 1       # Polynomial order of the diagonal terms in the lower-block
order_inter_off_diag = 1   # Polynomial order of the off-diagonal terms in the lower-block
nlen = 2                   # Number of neighbors on each side of a location
l1_nlen = 2                # Number of neighbors on each side of a location in a previous state
btype = 'fun'              # Basis type

components_list = []
active_vars = []

const_sparsity_matrix = np.nan * np.ones( (2*state_dim, 2*state_dim) )
integ_sparsity_matrix = np.nan * np.ones( (2*state_dim, 2*state_dim) )

# Top of the map
for d in range(state_dim):
    avars = list(range(d-nlen, d+nlen+1))
    avars = [ v for v in avars if v >= 0 ]   # Remove active variables < 0
    avars = [ v % state_dim for v in avars ] # Circulate
    avars = [ v for v in avars if v <= d ]   # Remove upper triangular
    avars.sort()

    navars = len(avars)

    # Constant part
    c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * navars
    c_orders_list = [order_off_diag] * (navars-1) + [0]
    c_approx = FUNC.MonotonicLinearSpanApproximation(
        c_basis_list, spantype='total', order_list=c_orders_list)

    # Integrated squared part
    e_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (navars-1) + \
                   [S1D.ConstantExtendedHermiteProbabilistsFunction()]
    e_orders_list = [order_off_diag-1] * (navars-1) + [order_diag-1]
    e_approx = FUNC.MonotonicLinearSpanApproximation(
        e_basis_list, spantype='total', order_list=e_orders_list)
    
    Tk = FUNC.MonotonicIntegratedSquaredApproximation(c_approx, e_approx)
    components_list.append( Tk )
    active_vars.append( avars )

    for v, co, eo in zip(avars, c_orders_list, e_orders_list):
        const_sparsity_matrix[d, v] = co
        integ_sparsity_matrix[d, v] = eo

# Bottom of the map
for d in range(state_dim):
    avars = list(range(d-nlen, d+nlen+1))
    avars = [ v for v in avars if v >= 0 ]   # Remove active variables < 0
    avars = [ v % state_dim for v in avars ] # Circulate
    avars = [ v for v in avars if v <= d ]   # Remove upper triangular
    avars = [ v+state_dim for v in avars ]   # Move to the bottom map
    l1_avars = list(range(d-l1_nlen, d+l1_nlen+1))
    l1_avars = [ v % state_dim for v in l1_avars ] # Circulate
    avars.extend( l1_avars )
    avars.sort()

    navars = len(avars)

    # Constant part
    c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * navars

    # Set up orders in interaction block
    c_orders_list = [order_inter_off_diag] * (2*l1_nlen+1)
    if d < l1_nlen:
        c_orders_list[d] = order_inter_diag
    elif d > state_dim-1-l1_nlen:
        c_orders_list[2*l1_nlen+1-(state_dim-d)] = order_inter_diag
    else:
        c_orders_list[l1_nlen] = order_inter_diag
    # Set up orders in triangular part
    c_orders_list.extend(
        [order_off_diag] * (
            min(nlen,d)
            if d < state_dim-nlen
            else nlen + (d-(state_dim-nlen)+1) )
    )
    c_orders_list.append( 0 )

    c_approx = FUNC.MonotonicLinearSpanApproximation(
        c_basis_list, spantype='total', order_list=c_orders_list)

    # Integrated squared part
    e_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (navars-1) + \
                   [S1D.ConstantExtendedHermiteProbabilistsFunction()]

    # Set up orders in interaction block
    e_orders_list = [max(0,order_inter_off_diag-1)] * (2*l1_nlen+1)
    if d < l1_nlen:
        e_orders_list[d] = max(0,order_inter_diag-1)
    elif d > state_dim-1-l1_nlen:
        e_orders_list[2*l1_nlen+1-(state_dim-d)] = max(0,order_inter_diag-1)
    else:
        e_orders_list[l1_nlen] = max(0,order_inter_diag-1)
    # Set up orders in triangular part
    e_orders_list.extend(
        [max(0,order_off_diag-1)] * (
            min(nlen,d)
            if d < state_dim-nlen
            else nlen + (d-(state_dim-nlen)+1) )
    )
    e_orders_list.append(max(0, order_diag-1))
    
    e_approx = FUNC.MonotonicLinearSpanApproximation(
        e_basis_list, spantype='total', order_list=e_orders_list)
    
    Tk = FUNC.MonotonicIntegratedSquaredApproximation(c_approx, e_approx)
    components_list.append( Tk )
    active_vars.append( avars )

    for v, co, eo in zip(avars, c_orders_list, e_orders_list):
        const_sparsity_matrix[d+state_dim, v] = co
        integ_sparsity_matrix[d+state_dim, v] = eo
        
tm = Maps.IntegratedSquaredTriangularTransportMap(
    active_vars, components_list)
        
plt.figure()
plt.imshow(const_sparsity_matrix)
plt.title("Orders of constant part")
plt.colorbar()
plt.show(False)

plt.figure()
plt.imshow(integ_sparsity_matrix)
plt.title("Orders of integrated part")
plt.colorbar()
plt.show(False)

plt.figure()
plt.spy(integ_sparsity_matrix+1)
plt.show(False)

fname = 'data/' + \
        'dim-%d/' % state_dim + \
        'tms' + \
        '-od-%d' % order_diag + \
        '-ood-%d' % order_off_diag + \
        '-oid-%d' % order_inter_diag + \
        '-oiod-%d' % order_inter_off_diag + \
        '-nlen-%d' % nlen + \
        '-l1nlen-%d' % l1_nlen + \
        '-btype-' + btype + \
        '.dill'
# print("Storing in " + fname)
# tm.store(fname)