#!/bin/bash

tmap-sequential-postprocess \
    --input=$1/tm.dill \
    --output=$1/post.dill \
    --nprocs=$2 \
    -v ${@:3} \
    --quadrature=approx-target \
    --quadrature-qtype=0 \
    --quadrature-qnum=10000 \
    --quadrature-bsize=800 \
    --filtering-quadrature \
    --filt-quad-qtype=0 \
    --filt-quad-qnum=1000 

    # --var-diag=exact-base \
    # --var-diag-qtype=0 \
    # --var-diag-qnum=10000 

