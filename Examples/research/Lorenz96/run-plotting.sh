#!/bin/bash

mkdir -p $1/figs

BURNIN=${2}

ipython -i postprocess-plotting.py -- \
        --data=$1/tm.dill \
       --postprocess-data=$1/post.dill \
       --rmse-burnin=$BURNIN \
       --do-smooth-rmse \
       --do-filt-rmse \
       --ntraj=0 \
       --store-fig-dir=$1/figs/ \
       --do-smooth \
       --do-smooth-burnin=$BURNIN \
       --do-filt \
       --do-filt-burnin=$BURNIN \
       ${@:3}

