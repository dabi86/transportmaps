#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import errno
import os
import timeit
from datetime import timedelta
from tqdm import tqdm # progress bar
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
from scipy.integrate import ode

from TransportMaps.Distributions import NormalDistribution
from TransportMaps.Distributions.Examples.Lorenz96 import \
    Lorenz96ForwardEulerDistribution, \
    Lorenz96ModalForwardEulerDistribution, \
    Lorenz96DynamicsMap
from src.TransportMaps.Maps import LinearMap, CompositeMap

npr.seed(0)

STORE = True
STORE_FIG = True
TEST_GRADIENTS = False
SKIP_PREDICTIONS = False
MODAL = True

K          = 33          # Smoothing term
F          = 14.         # Forcing term
dim        = 240         # State dimension
obs_skip   = 10          # Spatial observation gap
obs_dims   = list(range(0,dim,obs_skip))   # Observation locations
dt         = 0.025     # Time discretization (for observations and dynamics)
warm_nsteps= 1000      # Warm up steps to generate initial dynamics conditions
nsteps     = 800       # Total number of discretization steps
obs_dt     = 0.10      # Time step per observation
init_noise = .5        # Variance initial conditions 
dyn_noise  = 1e-1      # Variance in the dynamics
obs_noise  = 1e-2      # Variance in the observations

# MODAL options
modal_warm_nsteps = 1000  # Warm up steps
modal_nsteps = 100000      # Number of steps
modal_alpha = .999        # Truncation level of total energy

def f(t, x, l96map):
    return l96map.evaluate( x.reshape( (1,dim) ) )[0,:]

def generate_dynamics(
        x0, l96map, dt, nsteps, warm_nsteps):
    dim = x0.shape[0]
    r = ode(f)
    r.set_integrator('dopri5')
    r.set_initial_value(x0, 0.)
    r.set_f_params( l96map )
    xs = np.zeros( (nsteps+1, dim) )
    i = 1
    start_time = timeit.default_timer()
    for i in tqdm(range(1,warm_nsteps + nsteps + 1)):
        if i < warm_nsteps:
            r.integrate(r.t + dt)
        else:
            xs[i-warm_nsteps,:] = r.integrate(r.t + dt) 
        if not r.successful():
            break
    stop_time = timeit.default_timer()
    print("Dynamics generation time: %s" % str(timedelta(seconds=stop_time-start_time)))
    return xs

obs_dsteps = obs_dt/dt
if math.modf(obs_dsteps)[0] > 1e-13:
    raise ValueError("Observation dt must be a multiple of dt.")
obs_dsteps = int(np.round(obs_dsteps))
if SKIP_PREDICTIONS:
    dstep = obs_dsteps
    dyn_noise *= obs_dsteps
else:
    dstep = 1
T = np.arange(0, nsteps*dt+1e-10, dt)
T_obs = np.arange(0, nsteps*dt+1e-10, obs_dt)
n_obs = len(T_obs)
dim_obs = len(obs_dims)

# Right hand side dynamics
l96map = Lorenz96DynamicsMap(d=dim, K=K, F=F)

# If MODAL, construct linear projection operator
if MODAL:
    x0 = np.random.randn(dim)
    m_xs = generate_dynamics(
        x0, l96map, dt, modal_nsteps, modal_warm_nsteps)
    m_xs = np.asarray(m_xs)
    m_mean = np.mean(m_xs, axis=0)
    m_B = m_xs - m_mean[np.newaxis,:]
    m_S = np.dot(m_B.T, m_B) / (modal_nsteps-1)
    m_u, m_s, m_v = npla.svd(m_S)
    m_cmsm = np.cumsum(m_s)
    m_cmsm /= m_cmsm[-1]
    m_idx = next(
        (i+1 for i in range(len(m_s)) if m_cmsm[i] > modal_alpha),
        len(m_s) )
    plt.figure()
    plt.plot(m_cmsm[:(m_idx*3)], '-o')
    plt.grid(True)
    plt.show(False)
    print("Modal space dimension: %d" % m_idx)
    modal_sval = m_s[:m_idx]
    modal_sqrt_sval = np.sqrt(modal_sval)
    m_lin = m_u[:,:m_idx] * modal_sqrt_sval[np.newaxis,:]
    m_lin_inv = m_u[:,:m_idx].T / modal_sqrt_sval[:,np.newaxis]
    modal_map = LinearMap(m_mean, m_lin, Linv=m_lin_inv)
    plt.figure()
    for i in range(5):
        plt.plot(m_lin[:,i])
    plt.show(False)

# Generation of true dynamics (integration via RK4)
x0 = np.random.randn(dim)
xs = generate_dynamics(x0, l96map, dt, nsteps, warm_nsteps)
x0 = xs[0,:]

if MODAL: # Compute max error from modal truncation
    xs_mod = modal_map.evaluate( modal_map.inverse( xs ) )
    max_err = np.max(np.abs(xs - xs_mod))
    avg_rmse = np.mean( npla.norm(xs - xs_mod, axis=1) / np.sqrt(dim) )
    print("Max error from modal truncation: %.2e" % max_err)
    print("Avg. RMSE from modal truncation: %.2e" % avg_rmse)

# Plotting
dd = np.arange(dim)
DD, TT = np.meshgrid(dd, T)
fig = plt.figure(figsize=(8.4,2.4))
plt.contourf(TT.T, DD.T, xs.T, cmap=plt.get_cmap('jet'))
plt.colorbar(orientation='vertical')
plt.xlabel('time')
plt.ylabel('dimension')
plt.show(False)

# Define distribution initial condition
if not MODAL:    
    pi_init = NormalDistribution(
        x0, init_noise * np.eye(dim) )
else:
    pi_init = NormalDistribution(
        modal_map.inverse(x0[np.newaxis,:])[0,:],
        np.diag( init_noise / modal_sval) )

# Define the transition distribution
if not MODAL:
    pi_dyn = NormalDistribution(
        np.zeros(dim), dyn_noise * np.eye(dim) )
else:
    modal_dim = modal_map.dim_in
    pi_dyn = NormalDistribution(
        np.zeros(modal_dim),
        np.diag( dyn_noise / modal_sval ) )

# Define the observation distribution
obs_map = LinearMap(np.zeros(dim_obs), np.eye(dim)[obs_dims,:] )
pi_noise = NormalDistribution(
    np.zeros(dim_obs), obs_noise * np.eye(dim_obs) )
obs = obs_map.evaluate(xs[::obs_dsteps,:]) + pi_noise.rvs(n_obs)
    
# Assemble full posterior distribution
if not MODAL:
    pi = Lorenz96ForwardEulerDistribution(
        n        = dstep,
        dt       = dt,
        d        = dim,
        K        = K,
        F        = F,
        pi_init  = pi_init,
        pi_dyn   = pi_dyn,
        obs_map  = obs_map,
        pi_noise = pi_noise,
    )
else:
    pi = Lorenz96ModalForwardEulerDistribution(
        n         = dstep,
        dt        = dt,
        d         = dim,
        K         = K,
        F         = F,
        modal_map = modal_map,
        pi_init   = pi_init,
        pi_dyn    = pi_dyn,
        obs_map   = obs_map,
        pi_noise  = pi_noise,
    )
obs_i = 0
n = 0
while n <= nsteps:
    if n % obs_dsteps == 0:
        y = obs[obs_i,:]
        obs_i += 1
    else:
        y = None
    pi.step( y, x=xs[n,:] )
    n += dstep    

if TEST_GRADIENTS:
    print("Testing gradients of the right hand side of the forward map")
    rhs = pi.l96map.l96femap.rhs
    x = np.random.randn( 1, rhs.dim )
    v = np.random.randn( 1, rhs.dim )
    rhs.test_gradients(x, v=v)
    print("Testing gradients of distribution")
    x = np.random.randn( 1, pi.dim )
    v = np.random.randn( 1, pi.dim )
    pi.test_gradients(x, v=v)
    print("Testing gradients of first Markov component")
    mc = pi.get_MarkovComponent(0, 1)
    x = np.random.randn( 1, mc.dim )
    v = np.random.randn( 1, mc.dim )
    mc.test_gradients(x, v=v)

dir_name = 'data/' + \
           'dim-%d/' % dim + \
           'L96' + \
           ('-modal' if MODAL else '') + \
           ('-mod-alpha-%.4f' % modal_alpha if MODAL else '') + \
           '-K-%.1f' % K + \
           '-F-%.1f' % F + \
           '-nsteps-%d' % nsteps + \
           '-dt%.3f' % dt + \
           ('-skippred' if SKIP_PREDICTIONS else '') + \
           '-obsdt%.2f' % obs_dt + \
           '-obs%1.e' % obs_noise + \
           '-dyn%1.e' % dyn_noise
if STORE:
    try:
        os.makedirs(dir_name)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dir_name):
            pass
        else:
            raise
    # Save distribution
    fname = dir_name + '/dist.dill'
    print("Storing in " + fname)
    pi.store( fname )

if STORE_FIG:
    try:
        os.makedirs(dir_name + '/figs/')
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dir_name):
            pass
        else:
            raise
    fmts_list = ['svg', 'pdf', 'eps', 'png']
    fname = dir_name + '/figs/l96-trajectorty'
    for fmt in fmts_list:
        fig.savefig(fname+'.'+fmt, format=fmt, bbox_inches='tight');
