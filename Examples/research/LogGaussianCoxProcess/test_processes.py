import numpy as np
import matplotlib.pyplot as plt

import TransportMaps.Densities as DENS

import LogGaussianCoxProcessDensities as LGCPD

# Generate 2d mesh grid
N = 64
nobs = 30
x = np.linspace(0,1,N)
xx, yy = np.meshgrid(x, x)
X = np.vstack( (xx.flatten(), yy.flatten()) ).T

# Generating observation points
all_idxs = np.arange(N**2)
obs_idxs = np.random.choice(all_idxs, size=nobs)

# Re-order discretization
mask_obs = np.zeros(N**2, np.bool)
mask_obs[obs_idxs] = 1
mask_other = np.ones(N**2, np.bool)
mask_other[obs_idxs] = 0
shu_idxs = np.hstack( (all_idxs[mask_obs], all_idxs[mask_other]) )
inv_shu_idxs = np.argsort(shu_idxs)
X_shu = X[shu_idxs,:]

# Init the squared exponential kernel
kernel = LGCPD.SquaredExponentialKernel(l=0.1)

# Init the Gaussian process
gp = LGCPD.GaussianProcess(X_shu, kernel)

# # Generate one sample of the process
# samp_shu = gp.rvs(1)
# samp = samp_shu[:,inv_shu_idxs]

# # Plot
# plt.figure()
# plt.contourf(xx, yy, samp.reshape((N,N)))
# plt.colorbar()
# plt.show(False)

# Init the log-Gaussian process as a pushforward density
exp_map = Maps.FrozenExponentialDiagonalTransportMap(gp.dim)
log_gp = DENS.PushForwardTransportMapDensity(exp_map, gp)

# Generate one sample from the process
samp_lmb_shu = log_gp.rvs(1)
samp_lmb = samp_lmb_shu[:,inv_shu_idxs]

# Plot
levels = np.linspace(np.min(samp_lmb),np.max(samp_lmb),20)
plt.figure()
plt.contour(xx, yy, samp_lmb.reshape((N,N)), levels=levels)
plt.colorbar()
plt.show(False)

# Generate a Possion process at nobs=30 observation points
obs_lmb = samp_lmb[0, mask_obs]
ppp = LGCPD.PoissonPointProcessDensity(obs_lmb)
obs = ppp.rvs(1).flatten()
# Plot observation positions
X_obs = X[mask_obs,:]
plt.scatter(X_obs[:,0], X_obs[:,1], s=30, c='k')
plt.draw()

# Build posterior
Lk = gp.sampling_mat[:nobs,:nobs]
post = LGCPD.LogGaussianCoxProcessPosterior(Lk, obs)