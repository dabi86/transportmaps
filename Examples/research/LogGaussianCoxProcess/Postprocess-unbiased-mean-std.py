#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import logging
import time
from datetime import timedelta
import numpy as np
import scipy.stats as stats
import dill
import h5py

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Samplers as SAMP

def usage():
    print("Postprocess-unbiased-mean-var.py --data=<filename> " + \
          "[--ess=1000 --burnin=100 --lags=50 " + \
          "--no-title --store-dir=None --no-plotting " + \
          "--log=30 --batch=1000 --nprocs=1]")

def description():
    print("Description \n" + \
          "Load base and target densities along with the transport map " + \
          "which pushes the base to the target (or that pulls the base to " + \
          "the target if the --inverse option is provided)")

def full_usage():
    usage()
    description()

def compute_ess(samps, quantile=0.95):
    nsamps = samps.shape[0]
    if nsamps <= 1:
        return nsamps
    mean = np.mean(samps, axis=0)
    std = np.std(samps, axis=0)
    cent_samps = samps - mean
    corr = np.zeros((LAGS+1, samps.shape[1]))
    for lag in range(0,LAGS+1):
        corr[lag,:] = np.sum(cent_samps[:nsamps-lag,:] * cent_samps[lag:,:], axis=0) / \
                      float(nsamps-lag) / std**2.
    # Confidence interval
    var = 1. / np.arange(nsamps, nsamps-LAGS-1, -1)
    alpha = 1 - (1 - quantile)/2.
    confint = stats.norm.ppf(alpha) * np.sqrt(var)
    # ESS
    kappa = np.zeros(dim)
    abs_corr = np.abs(corr[1:,:])
    for d in range(dim):
        sig_corr = abs_corr[abs_corr[:,d] >= confint[1:],d] # 95% significant factors
        kappa[d] = 1 + 2 * np.sum(sig_corr, axis=0)
    ess = float(nsamps) / kappa
    arg_min_ess = np.argmin(ess)
    min_ess = int(ess[arg_min_ess])
    if PLOTTING:
        plt.figure()
        plt.stem(range(0,LAGS+1), corr[:,arg_min_ess], markerfmt='b.')
        plt.plot(range(0,LAGS+1), confint, 'r--')
        plt.plot(range(0,LAGS+1), -confint, 'r--')
        plt.grid(True)
        plt.show(False)
    return min_ess

argv = sys.argv[1:]
DATA_FNAME = None
ESS = 1000
BURNIN = 100
LAGS = 50
MIN_NBATCH = 100
# Plotting
PLOTTING = True
TITLE = True
STORE_DIR = None
STORE_FMT = ['png', 'eps', 'svg', 'pdf']
# Logging
LOGGING_LEVEL = 30 # Warnings
# Parallelization
BATCH_SIZE = 1000
NPROCS = 1
try:
    opts, args = getopt.getopt(argv, 'h', ['data=', 'ess=', 'burnin=', "lags=",
                                           'no-title', 'store-dir=', 'no-plotting',
                                           'log=', 'batch=', 'nprocs='])
except getopt.GetoptError:
    full_usage()
    raise
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ('--data'):
        DATA_FNAME = arg
    elif opt in ('--ess'):
        ESS = int(arg)
    elif opt in ('--burnin'):
        BURNIN = int(arg)
    elif opt in ('--lags'):
        LAGS = int(arg)
    elif opt in ('--no-plotting'):
        PLOTTING = False
    elif opt in ('--no-title'):
        TITLE = False
    elif opt in ('--store-dir'):
        STORE_DIR = arg
    elif opt in ('--log'):
        LOGGING_LEVEL = int(arg)
    elif opt in ('--batch'):
        BATCH_SIZE = int(arg)
    elif opt in ('--nprocs'):
        NPROCS = int(arg)
if None in [DATA_FNAME]:
    full_usage()
    sys.exit(2)

logging.basicConfig(level=LOGGING_LEVEL)

if PLOTTING:
    import matplotlib.pyplot as plt
    from matplotlib.ticker import MaxNLocator

    def store_figure(fig, fname, fig_formats):
        for fmat in fig_formats:
            fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight')
    
# Load data
with open(DATA_FNAME, 'rb') as istr:
    stg = dill.load(istr)

# Load target distribution
target_distribution = stg.target_distribution
dim = target_distribution.dim
obs = target_distribution.obs
N = target_distribution.full_N
full_lmb = target_distribution.full_lmb
full_gp = target_distribution.full_gp
full_dim = full_gp.dim

# Shuffling masks
obs_idxs = target_distribution.full_obs_idxs
mask_obs = np.zeros(full_dim, np.bool)
mask_obs[obs_idxs] = 1
mask_nobs = np.ones(full_dim, np.bool)
mask_nobs[obs_idxs] = 0

# Construct Gaussian process map
gp_map =  Maps.LinearTransportMap(full_gp.mu, full_gp.sampling_mat)
    
# Prepare distribution for Likelihood informed directions
base_distribution = stg.base_distribution
tmap = stg.tmap
approx_distribution = DIST.PushForwardTransportMapDistribution(tmap, base_distribution)

# Prepare pull target for sampling with Metropolis-Hastings
pull_target = DIST.PullBackTransportMapDistribution(tmap, target_distribution)

# Prepare distribution for remaining directions
prior_distribution = DIST.StandardNormalDistribution(full_dim - dim)

# Define stacking distribution
class StackDistribution(DIST.Distribution):
    def __init__(self, d1, d2):
        super(StackDistribution, self).__init__(d1.dim + d2.dim)
        self.d1 = d1
        self.d2 = d2
    def rvs(self, n, mpi_pool=None):
        x1 = self.d1.rvs(n, mpi_pool)
        x2 = self.d2.rvs(n)
        return np.hstack((x1,x2))

approx_pull_tar = StackDistribution(approx_distribution, prior_distribution)

# Define approximate target
approx_target = DIST.PushForwardTransportMapDistribution(gp_map, approx_pull_tar)

# Prepare 2d mesh grid
x = np.linspace(0,1,N)
xx, yy = np.meshgrid(x, x)
X = np.vstack( (xx.flatten(), yy.flatten()) ).T
X_obs = X[mask_obs,:]

## Load/Create samples file ##
samp_file = h5py.File(DATA_FNAME + '.hdf5','a')
grp_name = "/metropolis_hasting_indep"
if grp_name not in samp_file:
    samp_file.create_group(grp_name)
samp_grp = samp_file[grp_name]
if 'samples' in samp_grp or 'samples_pull' in samp_grp:
    if sys.version_info[0] == 3:
        sel = input("The data contain already samples. Do you want to overwrite them? [y/N] ")
    else:
        sel = raw_input("The data contain already samples. Do you want to overwrite them? [y/N] ")
    if sel == 'y' or sel == 'Y':
        samp_grp['samples'].resize(0, axis=0)
        samp_grp['samples_pull'].resize(0, axis=0)
else:
    samp_grp.create_dataset("samples", (0, approx_target.dim),
                            maxshape=(None, approx_target.dim), dtype='d')
    samp_grp.create_dataset("samples_pull", (0, pull_target.dim),
                            maxshape=(None, pull_target.dim), dtype='d')
samps = samp_grp['samples']
samps_pull = samp_grp['samples_pull']
nsamps_loaded = samps.shape[0]

# Start MPI
mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

try:
    ####### Generate samples with Metropolis-Hastings #######
    # Sample target using Metropolis-Hastings with independent porposals
    # from the approx_distribution
    start = time.clock()
    sampler = SAMP.MetropolisHastingsIndependentProposalsSampler(
        pull_target, base_distribution)
    ess = compute_ess(samps_pull)
    print("N samples %d - ESS %d" % (nsamps_loaded, ess))
    it_ess = -1 if nsamps_loaded == 0 else 0
    while ess < ESS:
        it_ess += 1
        nsamps_old = samps.shape[0]
        if it_ess != 0:
            nbatch = max((ESS - ess) * 2, MIN_NBATCH)
            (new_samps_pull, weights) = sampler.rvs(
                nbatch, x0=samps_pull[-1,:], mpi_pool_tuple=(mpi_pool,None))
        else:
            (new_samps_pull, weights) = sampler.rvs(
                ESS, BURNIN, mpi_pool_tuple=(mpi_pool,None))
        all_samps_pull = np.vstack( (samps_pull, new_samps_pull) )
        # Compute effective sample size
        nsamps_pull = all_samps_pull.shape[0]
        ess = compute_ess(all_samps_pull)
        print("N samples %d - ESS %d" % (nsamps_pull, ess))
        # Pushforward new samples
        scatter_tuple = (['x'], [new_samps_pull])
        batch = TM.mpi_eval("evaluate", scatter_tuple=scatter_tuple,
                            obj=tmap, mpi_pool=mpi_pool)
        nsamps_batch = batch.shape[0]
        # Append prior samples
        batch = np.hstack((batch, prior_distribution.rvs(nsamps_batch)))
        # Push forward throug the Gaussian process map (batching)
        if BATCH_SIZE is None:
            BATCH_SIZE = nsamps_batch
        new_samps = np.zeros(batch.shape)
        niter = nsamps_batch // BATCH_SIZE + (1 if nsamps_batch % BATCH_SIZE > 0 else 0)
        for it in range(niter):
            nnew = min(nsamps_batch-it*BATCH_SIZE, BATCH_SIZE)
            samp_start = it * BATCH_SIZE
            samp_stop = samp_start + nnew
            # Evaluate
            scatter_tuple = (['x'], [batch[samp_start:samp_stop,:]])
            new_samps[samp_start:samp_stop,:] = TM.mpi_eval(
                "evaluate", scatter_tuple=scatter_tuple,
                obj=gp_map, mpi_pool=mpi_pool )
        samps = np.vstack( (samps, new_samps) )
        nsamps = samps.shape[0]
        ##### Store samples #####
        samp_grp['samples'].resize(nsamps, axis=0)
        samp_grp['samples_pull'].resize(nsamps, axis=0)
        samp_grp['samples'][nsamps_old:nsamps,:] = new_samps
        samp_grp['samples_pull'][nsamps_old:nsamps,:] = new_samps_pull
    stop = time.clock()
    print("Sampling time: %s" % str(timedelta(seconds=stop-start)))
    ### Done with sampling ###

    if PLOTTING:
        # Reshuffle
        samp_log_lmb_shu = samps
        samp_log_lmb = np.zeros(samp_log_lmb_shu.shape)
        samp_log_lmb[:,mask_obs] = samp_log_lmb_shu[:,:dim]
        samp_log_lmb[:,mask_nobs] = samp_log_lmb_shu[:,dim:]
        # Compute mean and standard deviation
        samp_lmb = np.exp(samp_log_lmb)
        mean_lmb = np.mean(samp_lmb, axis=0)
        std_lmb = np.std(samp_lmb, axis=0)

        # Plot
        levels = np.linspace(np.min(mean_lmb),np.max(mean_lmb),20)
        fig = plt.figure(figsize=(6,5))
        plt.contourf(xx, yy, mean_lmb.reshape((N,N)), levels=levels)
        plt.gca().xaxis.set_ticklabels([])
        plt.gca().yaxis.set_ticklabels([])
        plt.colorbar(ticks=MaxNLocator(integer=True))
        plt.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w')
        plt.xlim([0,1])
        plt.ylim([0,1])
        if TITLE:
            plt.title("Unbiased - Mean of $\lambda$")
        plt.show(False)
        if STORE_DIR is not None:
            store_figure(
                fig, STORE_DIR + '/LogGaussianCox-unbiased-mean-map-ess-%i' % ess,
                STORE_FMT)

        levels = np.linspace(np.min(std_lmb),np.max(std_lmb),20)
        fig = plt.figure(figsize=(6,5))
        plt.contourf(xx, yy, std_lmb.reshape((N,N)), levels=levels)
        plt.gca().xaxis.set_ticklabels([])
        plt.gca().yaxis.set_ticklabels([])
        plt.colorbar(ticks=MaxNLocator(integer=True))
        plt.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w')
        plt.xlim([0,1])
        plt.ylim([0,1])
        if TITLE:
            plt.title("Unbiased - Standard deviation of $\lambda$")
        plt.show(False)
        if STORE_DIR is not None:
            store_figure(
                fig, STORE_DIR + '/LogGaussianCox-unbiased-std-map-ess-%i' % ess,
                STORE_FMT)

finally:
    samp_file.close()
    if mpi_pool is not None:
        mpi_pool.stop()
