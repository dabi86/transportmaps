#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import numpy as np
import matplotlib.pyplot as plt
import dill as pickle

import TransportMaps.Densities as DENS
import src.TransportMaps.Maps as MAPS

import LogGaussianCoxProcessDensities as LGCPD

AVAIL_COVARIANCES = {'ou': 'Ornstein-Uhlenbeck',
                     'sqexp': 'Squared exponential'}

def usage():
    print("LogGaussianCox-DataGeneration.py --output=<filename> [--n-grid=64 " +\
          "--n-obs=30 --cov=ou --cor-len=1.0]")

def print_avail_cov():
    print('Available covariances:')
    for qtypen, qtypename in AVAIL_COVARIANCES.items():
        print('  %s: %s' % (qtypen, qtypename))        

def full_usage():
    usage()
    print_avail_cov()

argv = sys.argv[1:]
N = 64
nobs = 30
cov = 'ou'
corr_length = 1.
OUT_FNAME = None
try:
    opts, args = getopt.getopt(argv,"h",["output=", "n-grid=", "n-obs=", "cov=", "cor-len="])
except getopt.GetoptError:
    full_usage()
    raise
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--output"):
        OUT_FNAME = arg
    elif opt in ("--n-grid"):
        N = int(arg)
    elif opt in ("--n-obs"):
        nobs = int(arg)
    elif opt in ("--cov"):
        cov = arg
    elif opt in ("--cor-len"):
        corr_length = float(arg)
if None in [OUT_FNAME]:
    full_usage()
    sys.exit(3)

# Generate 2d mesh grid
x = np.linspace(0,1,N)
xx, yy = np.meshgrid(x, x)
X = np.vstack( (xx.flatten(), yy.flatten()) ).T

fig_fld = plt.figure()
fig_lkl = plt.figure()
accept = False
while not accept:
    plt.figure(fig_fld.number)
    plt.clf()
    plt.figure(fig_lkl.number)
    plt.clf()
    # Generating observation points
    all_idxs = np.arange(N**2)
    obs_idxs = np.random.choice(all_idxs, size=nobs, replace=False)

    # Re-order discretization
    mask_obs = np.zeros(N**2, np.bool)
    mask_obs[obs_idxs] = 1
    mask_other = np.ones(N**2, np.bool)
    mask_other[obs_idxs] = 0
    shu_idxs = np.hstack( (all_idxs[mask_obs], all_idxs[mask_other]) )
    inv_shu_idxs = np.argsort(shu_idxs)
    X_shu = X[shu_idxs,:]

    # Init the squared exponential kernel
    if cov == 'ou':
        kernel = LGCPD.OrnsteinUhlenbeck(l=corr_length)
    elif cov == 'sqexp':
        kernel = LGCPD.SquaredExponentialKernel(l=corr_length)
    else: raise ValueError("Unrecognized covariance type")

    # Init the Gaussian process
    gp = LGCPD.GaussianProcess(X_shu, kernel)

    # Init the log-Gaussian process as a pushforward density
    exp_map = Maps.FrozenExponentialDiagonalTransportMap(gp.dim)
    log_gp = DENS.PushForwardTransportMapDensity(exp_map, gp)

    # Generate one sample from the process
    samp_lmb_shu = log_gp.rvs(1)
    samp_lmb = samp_lmb_shu[:,inv_shu_idxs]

    # Plot
    plt.figure(fig_fld.number)
    levels = np.linspace(np.min(samp_lmb),np.max(samp_lmb),20)
    plt.contourf(xx, yy, samp_lmb.reshape((N,N)), levels=levels)
    plt.colorbar()

    # Generate a Possion process at nobs=30 observation points
    obs_lmb = samp_lmb[0, mask_obs]
    ppp = LGCPD.PoissonPointProcessDensity(obs_lmb)
    obs = ppp.rvs(1).flatten()
    # Plot observation positions
    X_obs = X[mask_obs,:]
    plt.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w')
    # plt.scatter(X_obs[:,0], X_obs[:,1], s=80, c=obs, cmap=plt.get_cmap('gray'))
    plt.xlim([0,1])
    plt.ylim([0,1])

    # Plot likelihood of the observations (lmb vs obs)
    plt.figure(fig_lkl.number)
    plt.scatter(obs_lmb, obs)
    mmax = max(np.max(obs_lmb), np.max(obs))
    pmax = mmax + 1
    plt.plot([0,pmax],[0,pmax])
    plt.xlim([0,pmax])
    plt.ylim([0,pmax])
    plt.grid()

    plt.show(False)
    if sys.version_info[0] == 3:
        sel = input("Accept the current data? [y/N/q] ")
    else:
        sel = raw_input("Accept the current data? [y/N/q] ")
    if (sel == 'y' or sel == 'Y'):
        accept = True
    elif sel == 'q': sys.exit(0)

# Build posterior
Lk = gp.sampling_mat[:nobs,:nobs]
post = LGCPD.LogGaussianCoxProcessPosterior(
    Lk, obs,
    full_N=N, full_obs_idxs=obs_idxs,
    full_gp=gp, full_lmb=samp_lmb)

with open(OUT_FNAME, 'wb') as out_stream:
    pickle.dump(post, out_stream)
