#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import logging
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import dill

import TransportMaps as TM
import TransportMaps.Distributions as DIST


def usage():
    print("Postprocess-realizations.py --data=<filename> " + \
          "[--nsamps=1 --no-title --store-dir=None --log=30 --nprocs=1]")

def description():
    print("Description \n" + \
          "Load base and target densities along with the transport map " + \
          "which pushes the base to the target (or that pulls the base to " + \
          "the target if the --inverse option is provided)")

def full_usage():
    usage()
    description()

def store_figure(fig, fname, fig_formats):
    for fmat in fig_formats:
        fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight')

argv = sys.argv[1:]
DATA_FNAME = None
NSAMPS = 1
TITLE = True
STORE_DIR = None
STORE_FMT = ['png', 'eps', 'svg', 'pdf']
# Logging
LOGGING_LEVEL = 30 # Warnings
# Parallelization
NPROCS = 1
try:
    opts, args = getopt.getopt(argv, 'h', ['data=', 'nsamps=', 'no-title', 'store-dir=',
                                           'log=', 'nprocs='])
except getopt.GetoptError:
    full_usage()
    raise
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ('--data'):
        DATA_FNAME = arg
    elif opt in ('--nsamps'):
        NSAMPS = int(arg)
    elif opt in ('--no-title'):
        TITLE = False
    elif opt in ('--store-dir'):
        STORE_DIR = arg
    elif opt in ('--log'):
        LOGGING_LEVEL = int(arg)
    elif opt in ('--nprocs'):
        NPROCS = int(arg)
if None in [DATA_FNAME]:
    full_usage()
    sys.exit(2)

logging.basicConfig(level=LOGGING_LEVEL)
    
# Load data
with open(DATA_FNAME, 'rb') as istr:
    stg = dill.load(istr)

# Load target distribution
target_distribution = stg.target_distribution
dim = target_distribution.dim
obs = target_distribution.obs
N = target_distribution.full_N
full_lmb = target_distribution.full_lmb
full_gp = target_distribution.full_gp
full_dim = full_gp.dim

# Shuffling masks
obs_idxs = target_distribution.full_obs_idxs
mask_obs = np.zeros(full_dim, np.bool)
mask_obs[obs_idxs] = 1
mask_nobs = np.ones(full_dim, np.bool)
mask_nobs[obs_idxs] = 0

# Construct Gaussian process map
gp_map =  Maps.LinearTransportMap(full_gp.mu, full_gp.sampling_mat)
    
# Prepare distribution for Likelihood informed directions
base_distribution = stg.base_distribution
tmap = stg.tmap
approx_distribution = DIST.PushForwardTransportMapDistribution(tmap, base_distribution)

# Prepare distribution for remaining directions
prior_distribution = DIST.StandardNormalDistribution(full_dim - dim)

# Define stacking distribution
class StackDistribution(DIST.Distribution):
    def __init__(self, d1, d2):
        super(StackDistribution, self).__init__(d1.dim + d2.dim)
        self.d1 = d1
        self.d2 = d2
    def rvs(self, n, mpi_pool=None):
        x1 = self.d1.rvs(n, mpi_pool)
        x2 = self.d2.rvs(n)
        return np.hstack((x1,x2))

approx_pull_tar = StackDistribution(approx_distribution, prior_distribution)

# Define approximate target
approx_target = DIST.PushForwardTransportMapDistribution(gp_map, approx_pull_tar)

# Prepare 2d mesh grid
x = np.linspace(0,1,N)
xx, yy = np.meshgrid(x, x)
X = np.vstack( (xx.flatten(), yy.flatten()) ).T
X_obs = X[mask_obs,:]

# Start MPI
mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(min(NPROCS,NSAMPS))

try:
    ######## Generate NSAMPS samples ########
    samp_log_lmb_shu = approx_target.rvs(NSAMPS, mpi_pool=mpi_pool)
    samp_log_lmb = np.zeros(samp_log_lmb_shu.shape)
    samp_log_lmb[:,mask_obs] = samp_log_lmb_shu[:,:dim]
    samp_log_lmb[:,mask_nobs] = samp_log_lmb_shu[:,dim:]
    samp_lmb = np.exp(samp_log_lmb)
    for i in range(NSAMPS):
        levels = np.linspace(0, np.ceil(np.max(samp_lmb[i,:])), 20)
        fig = plt.figure(figsize=(6,5))
        plt.contourf(xx, yy, samp_lmb[i,:].reshape((N,N)), levels=levels)
        plt.gca().xaxis.set_ticklabels([])
        plt.gca().yaxis.set_ticklabels([])
        plt.colorbar(ticks=MaxNLocator(integer=True))
        plt.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w')
        plt.xlim([0,1])
        plt.ylim([0,1])
        if TITLE:
            plt.title("Realization of $\lambda$")
        plt.tight_layout()
        plt.show(False)
        if STORE_DIR is not None:
            store_figure(fig, STORE_DIR + '/LogGaussianCox-realization-%i' % i, STORE_FMT)

finally:
    if mpi_pool is not None:
        mpi_pool.stop()
