#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import dill as pickle

import TransportMaps.Densities as DENS
import src.TransportMaps.Maps as MAPS

import LogGaussianCoxProcessDensities as LGCPD

def usage():
    print("LogGaussianCox-PlotData.py --data=<filename>")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
try:
    opts, args = getopt.getopt(argv,"h",["data="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--data"):
        IN_FNAME = arg
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME,'rb') as in_stream:
    dens = pickle.load(in_stream)

# Generate 2d mesh grid
N = dens.full_N
x = np.linspace(0,1,N)
xx, yy = np.meshgrid(x, x)
X = np.vstack( (xx.flatten(), yy.flatten()) ).T

fig = plt.figure(figsize=(6,5))
ax = fig.add_axes([0.2, 0.1, 0.8, 0.8])
# ax = fig.add_subplot(111)
# Plot field
samp_lmb = dens.full_lmb
levels = np.linspace(0,np.ceil(np.max(samp_lmb)), 20)
cax = ax.contourf(xx, yy, samp_lmb.reshape((N,N)), levels=levels)
cb = plt.colorbar(cax, ticks=MaxNLocator(integer=True))

# Load observation points
obs = dens.obs
obs_idxs = dens.full_obs_idxs
# Mask discretization
mask_obs = np.zeros(N**2, np.bool)
mask_obs[obs_idxs] = 1
# Plot observation locations
X_obs = X[mask_obs,:]
ax.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w')
ax.xaxis.set_ticklabels([])
ax.yaxis.set_ticklabels([])
ax.set_xlim([0,1])
ax.set_ylim([0,1])
# Add axes for observation bar
rect = [0.075, 0.1, 0.09, 0.8]
ax1 = fig.add_axes(rect, axisbg='lightgray')
ax1.axes.get_xaxis().set_visible(False)
nobs_ls = 8
obs_ls = np.linspace(np.min(obs), np.max(obs), nobs_ls)
ax1.scatter( np.zeros(nobs_ls), obs_ls, s=(obs_ls+1)*60, c='w')
fig.canvas.draw()
labels = ["%s" % l.get_text() for l in ax1.yaxis.get_ticklabels()]
labels[0] = ""
labels[-1] = ""
ax1.yaxis.set_ticklabels(labels)

# Plot likelihood (lmb vs obs)
obs_lmb = samp_lmb[0,mask_obs]
plt.figure()
plt.scatter(obs_lmb, obs)
mmax = max(np.max(obs_lmb), np.max(obs))
pmax = mmax + 1
plt.plot([0,pmax],[0,pmax])
plt.xlim([0,pmax])
plt.ylim([0,pmax])
plt.grid()

plt.show(False)