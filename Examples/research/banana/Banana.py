#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Alessio Spantini & Daniele Bigoni
# E-mail: spantini@mit.edu & dabi@limitcycle.it
#

import sys, getopt
import logging
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import scipy.linalg as scila
from tabulate import tabulate
import TransportMaps as TM
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG

import MapGeneration as MG

logging.basicConfig(level=logging.INFO)

def usage():
    print('Banana.py -o <order> -n <quad_n_points> [-b <batch_size> --no-laplace --no-plotting]')

def full_usage():
    usage()
    print("Example: Banana.py -o 10 -n 30")

argv = sys.argv[1:]
ORDER = None
QNUM = None
BATCH_SIZE = None
DO_LAPLACE = True
DO_PLOT = True
try:
    opts, args = getopt.getopt(argv,"ho:n:b:",["order=", "qnum=", "bsize=",
                                               "no-laplace", "no-plotting"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-o", "--order"):
        ORDER = int(arg)
    elif opt in ("-n", "--qnum"):
        QNUM = int(arg)
    elif opt in ("-b", "--bsize"):
        BATCH_SIZE = int(arg)
    elif opt in "--no-laplace":
        DO_LAPLACE = False
    elif opt in "--no-plotting":
        DO_PLOT = False
if None in [ORDER, QNUM]:
    full_usage()
    sys.exit(3)

a = 1.
b = 1.
mu = np.zeros(2)
sigma2 = np.array([[1., 0.5],[0.5, 1.]])
pre_target_density = DENS.BananaDensity(a, b, mu, sigma2)

# Define rotation linear map
theta = - np.pi/2
linearTerm = np.array([[ np.cos(theta), np.sin(theta)],[-np.sin(theta), np.cos(theta)]])
constantTerm = np.zeros(2)
linMap = Maps.LinearTransportMap(constantTerm, linearTerm)

# # Define analytic composed map
# invLinMap = MAPS.LinearTransportMap(constantTerm, linearTerm.T)
# class MapComposition(TransportMap):
#     def __init__(self,map1,map2):
#         self.map1 = map1
#         self.map2 = map2
#     def evaluate(self, x, precomp=None, idxs_slice=slice(None)):
#         return self.map1( self.map2( x ) )
# comp_map = MapComposition(invLinMap, DENS.BananaDensity
# DIAG.plotAlignedSliceMap(, range_vec=[-4,4], numPointsXax = 30,
#                          numCont = 30,  tickslabelsize = 10 )

# Pullback to create new target
target_density = DENS.PullBackTransportMapDensity( linMap, pre_target_density )

# Plot target
DIAG.plot2density( target_density, xlim=[-8.,2.], ylim=[-3.,4.],
                   colormap = 'jet', contourf=True )

if DO_LAPLACE:
    ################ Laplace approximation #############
    laplace_approx = TM.laplace_approximation( target_density )
    laplace_tm = Maps.LinearTransportMap.build_from_Gaussian(laplace_approx,
                                                             typeMap = "Tri")
    laplace_pullback = DENS.PullBackTransportMapDensity( laplace_tm, target_density )
    if DO_PLOT:
        DIAG.plotAlignedConditionals( laplace_pullback, range_vec=[-5,5])

order = ORDER

#Define map Integrated Exponential
tm_approx = MG.Default_IsotropicIntegratedExponentialRBFTriangularTransportMap(2, order)
# tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
#     2, order, 'total')
# #Define map Linear span
# tm_approx = TM.Default_IsotropicLinearSpanTriangularTransportMap(2, order)

print("Number of coefficients: %d" % tm_approx.get_n_coeffs())

#Reference density
base_density = DENS.StandardNormalDensity(2)
tm_density = DENS.PushForwardTransportMapDensity(tm_approx,
                                                 base_density)

#Set up optimization
qtype = 3           # Gauss quadrature
qparams = [QNUM] * 2  # Quadrature order
reg = None          # No regularization
# reg = {'type': 'L2', # CHANGED: L2 regularization
#        'alpha': 1e-3}
batch_size = [None,None,BATCH_SIZE]
tol = 1e-5          # Optimization tolerance
ders = 2            # Use gradient and Hessian
tm_density.minimize_kl_divergence(target_density,
                                  qtype=qtype,
                                  qparams=qparams,
                                  regularization=reg,
                                  tol=tol, ders=ders,
                                  batch_size=batch_size)

#Plot results
# DIAG.plot2density( tm_density, xlim=[-0.8,5.5], ylim=[-9,1.5],
#                    colormap = 'jet', contourf=True, numPointsXax = 50, figname="Pushforward" )
# pullback_density = DENS.PullBackTransportMapDensity( tm_approx, target_density )
# DIAG.plotAlignedSliceMap(tm_approx, range_vec=[-4,4], numPointsXax = 30,
#                          numCont = 30,  tickslabelsize = 10 )
# DIAG.plotAlignedConditionals(pullback_density, figname="Pullback")
# DIAG.plotRandomConditionals(pullback_density, figname="PullbackRandom")
# DIAG.varianceDiagnostic(base_density, pullback_density,
#                         numMCsamples=[1e3, 5e3, 1e4], ntrialMC=10)
# DIAG.accuracyIntegratedExponentialDiagnostic(base_density, pullback_density, figname="Integral")



pullback_density = DENS.PullBackTransportMapDensity( tm_approx, target_density )
DIAG.plotAlignedSliceMap(tm_approx, range_vec=[-4,4], numPointsXax = 30,
                         numCont = 30,  tickslabelsize = 10 )
DIAG.plotAlignedConditionals(pullback_density )
DIAG.plot2density( tm_density, xlim=[-8.,2.], ylim=[-3.,4.],
                   colormap = 'jet', contourf=True, numPointsXax = 50  )
#DIAG.plotRandomConditionals(pullback_density )
# DIAG.varianceDiagnostic(base_density, pullback_density,
#                         numMCsamples=[1e3, 5e3, 1e4], ntrialMC=10)
#DIAG.accuracyIntegratedExponentialDiagnostic(base_density, pullback_density )
