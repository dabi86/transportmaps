import SpectralToolbox.Spectral1D as S1D
import TransportMaps.Functionals as FUNC


def Default_IsotropicIntegratedExponentialRBFTriangularTransportMap(dim, order):
    r""" Generate a triangular transport map with default settings.

    The polynomials used are the Hermite Probabilists' polynomials and the
    Hermite Functions. The approximation order is isotropic.

    Args:
      dim (int): dimension of the map
      order (int): isotropic order of the map
      span (str): 'full' for full order approximations, 'total' for total order
        approximations

    Returns:
      (:class:`CommonBasisIntegratedExponentialTriangularTransportMap<CommonBasisIntegratedExponentialTriangularTransportMap>`) -- the constructed transport map
    """
    approx_list = []
    active_vars = []
    c_basis_list = [S1D.LinearExtendedHermiteProbabilistsRadialBasisFunction(order, 0.9)
                    for i in range(dim)]
    e_basis_list = [S1D.ConstantExtendedHermiteProbabilistsRadialBasisFunction(order, 0.9)
                    for i in range(dim)]
    for ii in range(dim):
        c_orders_list = [order]*ii+[0]
        c_approx = FUNC.MonotonicFullOrderLinearSpanApproximation(c_basis_list[:ii+1],
                                                                  c_orders_list)
        e_orders_list = [order]*(ii+1)
        e_approx = FUNC.MonotonicFullOrderLinearSpanApproximation(e_basis_list[:ii+1],
                                                                  e_orders_list)
        approx = FUNC.MonotonicIntegratedExponentialApproximation(c_approx, e_approx)
        approx_list.append( approx )
        active_vars.append( list(range(ii+1)) )
    tm_approx = Maps.CommonBasisIntegratedExponentialTriangularTransportMap(active_vars, approx_list)
    return tm_approx
