import subprocess
import dolfin as dol
import mshr

import matplotlib.pyplot as plt
import numpy as np

from GmshGeoGenerator import generate_geo
from solver import solve, adjoint

#
# Cheng, K. S., Isaacson, D., Newell, J. C., & Gisser, D. G. (1989).
# Electrode Models for Electric Current Computed Tomography.
# IEEE Transactions on Biomedical Engineering, 36(9), 918–924.
# https://doi.org/10.1109/10.35300
#
# In this setting the cylindrical bath is completely surrounded by
# electrodes all around it (not on the top and bottom)
# The model is described by
#
#   \nabla \cdot \sigma(x) \nabla U = 0                     for x\in B
#   \sigma(x) \frac{\partial U(x)}{\partial n} = g^k(x)     for x\in \partial B
#
# where the current pattern g^k(x) := j^k(\theta), for \theta = \arctan2(x_1,x_2),
# is given by
#
#   j^k(\theta) = \frac{5 \cos(k\theta)}{A}
#   A = \frac{f 2 \pi h r_0 }{L}   (area of one electrode)
#
# with 
#
#   f   = 1          (fraction of circumference covered by electrodes)
#   h   = 4.24 cm    (depth of the bath)
#   r_0 = 15 cm      (radius of the bath)
#   L   = 32         (number of electrodes)
#

# parameters['allow_extrapolation'] = True

rho = 284.0 # Bath resistivities

PLOTTING = True
k = 8  # Frequencies k=1,...,16

# Parameters
h  = 4.24
r0 = 15.
L  = 32

A = 2 * np.pi * h * r0 / L

# Sensor std
std = 0.1

ndiscr=40

# Current pattern
class Current(dol.Expression):
    def __init__(self, k, A, element):
        self.k = k
        self.A = A
        self._element = element
    def eval(self, values, x):
        theta = np.arctan2(x[1], x[0])
        values[0] = 5. * np.cos(self.k * theta) / self.A
    def value_shape(self):
        return (1,)

# Pointwise sensor
boundary_sensor_str = '1./sqrt(2.*pi*pow(std,2)) * ' + \
                      'exp(-.5 * pow((atan2(c1, c0)-atan2(x[1], x[0]))*r0,2) / pow(std,2))'
        
# Geometry
geometry = mshr.Circle(dol.Point(0, 0), r0)
mesh = mshr.generate_mesh(geometry, ndiscr)

# Finite element space
VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
VEFS = dol.FunctionSpace(mesh, VE)
RE = dol.FiniteElement("R", dol.triangle, 0)
REFS = dol.FunctionSpace(mesh, RE)
VERE = dol.MixedElement([VE, RE])
W = dol.FunctionSpace(mesh, VERE)

N = 1000 # Interpolation points

current_expr = Current(k, A, element=VE)
sigma = dol.Constant(1./rho)

# plt.figure()
# plot(sigma_fun)
# plt.show(False)

u = solve(current_expr, sigma, W)

plt.figure()
dol.plot(u)
plt.show(False)

# Sensor
sensor_expr = dol.Expression(boundary_sensor_str, c0=0., c1=r0, r0=r0, std=std, pi=np.pi,
                             element=VE)

def observe(sigma, sensor_expr, current_expr, W):
    sigma = dol.Constant(sigma)
    u = solve(current_expr, sigma, W)
    vlt = dol.assemble(sensor_expr * u * dol.ds)
    return vlt, u

def grad_observe(sigma, u, sensor_expr, current_expr, W):
    sigma_cnst = dol.Constant(sigma)
    adj = solve(sensor_expr, sigma_cnst, W)
    grd = - dol.inner(dol.grad(adj), dol.grad(u))
    grd = dol.assemble( grd * dol.dx )
    return grd

# Taylor test
sigma = 1/284.
h = 1e-4
dx = 1.
vlt, u = observe(sigma, sensor_expr, current_expr, W)
grad_vlt = grad_observe(sigma, u, sensor_expr, current_expr, W)
vlt_shdx, u = observe(sigma + h * dx, sensor_expr, current_expr, W)
err1 = np.abs( vlt_shdx - vlt - h * grad_vlt * dx )
h /= 2
vlt_shdx, u = observe(sigma + h * dx, sensor_expr, current_expr, W)
err2 = np.abs( vlt_shdx - vlt - h * grad_vlt * dx )
rate = np.log2(err1/err2)
print("Rate: %.2f" % rate)

