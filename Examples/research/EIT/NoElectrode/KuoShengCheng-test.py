import subprocess
import dolfin as dol

import matplotlib.pyplot as plt
import numpy as np

from GmshGeoGenerator import generate_geo
from solver import solve

#
# Cheng, K. S., Isaacson, D., Newell, J. C., & Gisser, D. G. (1989).
# Electrode Models for Electric Current Computed Tomography.
# IEEE Transactions on Biomedical Engineering, 36(9), 918–924.
# https://doi.org/10.1109/10.35300
#
# In this setting the cylindrical bath is completely surrounded by
# electrodes all around it (not on the top and bottom)
# The model is described by
#
#   \nabla \cdot \sigma(x) \nabla U = 0                     for x\in B
#   \sigma(x) \frac{\partial U(x)}{\partial n} = g^k(x)     for x\in \partial B
#
# where the current pattern g^k(x) := j^k(\theta), for \theta = \arctan2(x_1,x_2),
# is given by
#
#   j^k(\theta) = \frac{5 \cos(k\theta)}{A}
#   A = \frac{f 2 \pi h r_0 }{L}   (area of one electrode)
#
# with 
#
#   f   = 1          (fraction of circumference covered by electrodes)
#   h   = 4.24 cm    (depth of the bath)
#   r_0 = 15 cm      (radius of the bath)
#   L   = 32         (number of electrodes)
#

dol.parameters['allow_extrapolation'] = True

rho_list = [284.0, 139.7, 62.3, 29.5] # Bath resistivities
test_idx = 0  # Bath resistivity index

# Experimental data
rho_k_exp = np.array(
    [[312.0, 150.0, 96.0, 69.5, 54.0, 44.1, 37.3, 32.4,
      28.9, 26.2, 24.3, 22.8, 21.8, 21.0, 20.6, 20.5],
     [156.8, 75.0, 47.9, 34.6, 27.0, 22.0, 18.6, 16.2,
      14.4, 13.1, 12.2, 11.4, 10.9, 10.6, 10.3, 10.3],
     [70.7, 33.8, 21.7, 15.7, 12.2, 10.0, 8.5, 7.4,
      6.6, 6.0, 5.6, 5.2, 5.0, 4.9, 4.8, 4.7],
     [33.9, 16.3, 10.1, 7.6, 5.9, 4.8, 4.1, 3.6,
      3.2, 2.9, 2.7, 2.6, 2.5, 2.4, 2.3, 2.3]])

rho = rho_list[test_idx]

PLOTTING = False
k_list = range(1,17)  # Frequencies k=1,...,16

# Parameters
h  = 4.24
r0 = 15.
L  = 32

A = 2 * np.pi * h * r0 / L

# Mesh size multiplier
mesh_scale = 0.025

# Current pattern
class Current(dol.Expression):
    def __init__(self, k, A, element):
        self.k = k
        self.A = A
        self._element = element
    def eval(self, values, x):
        theta = np.arctan2(x[1], x[0])
        values[0] = 5. * np.cos(self.k * theta) / self.A
    def value_shape(self):
        return (1,)

# Conductivity field \sigma
class ConductivityField(dol.Expression):
    def __init__(self, rho, element):
        self.rho = rho
        self._element = element
    def eval(self, value, x):
        value[0] = 1./rho
    def value_shape(self):
        return (1,)

# Geometry
fname = "circle"
generate_geo(mesh_scale, r0, L, fname + ".geo")
subprocess.check_call("gmsh -2 " + fname + ".geo", shell=True)
subprocess.check_call("dolfin-convert " + fname + ".msh " + fname + ".xml", shell=True)
mesh = dol.Mesh(fname + ".xml")
surface = dol.BoundaryMesh(mesh, "exterior")

# Finite element space
VE = dol.FiniteElement("Lagrange", dol.triangle, 1)

N = 1000 # Interpolation points
krho = np.zeros(len(k_list))
for i, k in enumerate(k_list):  # Current frequencies k=1,...,16
    g = Current(k, A, element=VE)
    sigma = ConductivityField(rho, element=VE)
    
    U = solve(g, sigma, mesh, VE)

    if PLOTTING:
        plt.figure()
        dol.plot(U)
        plt.show(False)

    # Extract boundary current
    WS = dol.FunctionSpace(surface, "Lagrange", 1)
    I = dol.Function(WS)
    I.interpolate(g)

    # Extract boundary voltage (i.e. electrostatic potential)
    V = dol.Function(WS)
    V.interpolate(U)

    # Iterate over angle to get currents and voltages
    thetas = np.linspace(0, 2*np.pi, N)
    crt = np.zeros(N)
    vlt = np.zeros(N)
    for j,t in enumerate(thetas):
        pnt = [r0*np.cos(t), r0*np.sin(t)]
        crt[j] = I(pnt)
        vlt[j] = V(pnt)

    if PLOTTING:
        fig = plt.figure()
        axI = fig.add_subplot(111)
        axV = axI.twinx()
        axI.plot(thetas, crt, '-k', label='Current')
        axV.plot(thetas, vlt, '-r', label='Voltage')
        plt.show(False)

    rho_approx = np.mean(vlt/crt/A)
    krho[i] = rho_approx * k
    print("Characteristic resistance (rho): %.2f" % rho_approx)
    print("k*rho: %.2f" % (krho[i]))

krho_exp = np.array(k_list) * rho_k_exp[test_idx,[i-1 for i in k_list]]
plt.figure()
plt.plot(k_list, krho)
plt.plot(k_list, krho_exp, 'xr')
plt.ylabel(r"$k \rho_k$")
plt.show(False)