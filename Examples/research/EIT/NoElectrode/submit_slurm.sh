#!/bin/bash
#SBATCH --job-name=tm
#SBATCH --workdir=/master/home/dabi/VC-Projects/Software/Mine/Current/transportmaps-private/Examples/research/EIT/NoElectrode/
#SBATCH --output=job.%J.out
#SBATCH --error=job.%J.err
#SBATCH --nodes=6
#SBATCH --ntasks-per-node=16
#SBATCH --mail-user=dabi@mit.edu

OPENBLAS_NUM_THREADS=1 $*
