#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import time
import dill
import numpy as np
import numpy.random as npr
import dolfin as dol

try:
    import matplotlib.pyplot as plt
    PLOTTING_SUPPORTED = True
except ImportError:
    PLOTTING_SUPPORTED = False

import TransportMaps as TM
from NoElectrodeEITDistributions import NoElectrodeEITSolver

def usage():
    print('test_distribution.py --input=<filename> --no-cbar --no-axis --vmin=VMIN --vmax=VMAX --taylor=n --fd --time=n --mpi --serial --x=<val> --nprocs=N')

argv = sys.argv[1:]
IN_FNAME = None
CBAR = True
AXIS = True
TITLE = True
VMIN = None
VMAX = None
DO_TAYLOR = False
N_TAYLOR = 1
DO_FD = False
DO_TIMING = False
DO_TIMING_N = 10
DO_MPI = False
DO_SERIAL = False
X = 1.
NPROCS = 1
try:
    opts, args = getopt.getopt(argv, "h", ['input=',
                                           'no-title', 'no-cbar', 'no-axis',
                                           'vmin=', 'vmax=',
                                           'taylor=', 'fd',
                                           'time=', 'mpi', 'serial',
                                           'x=', 'nprocs='])
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
    elif opt == '--no-title':
        TITLE = False
    elif opt == '--no-cbar':
        CBAR = False
    elif opt == '--no-axis':
        AXIS = False
    elif opt == '--vmin':
        VMIN = float(arg)
    elif opt == '--vmax':
        VMAX = float(arg)
    elif opt == '--taylor':
        DO_TAYLOR = True
        N_TAYLOR = int(arg)
    elif opt == '--fd':
        DO_FD = True
    elif opt == '--time':
        DO_TIMING = True
        DO_TIMING_N = int(arg)
    elif opt == '--mpi':
        DO_MPI = True
    elif opt == '--serial':
        DO_SERIAL = True
    elif opt == '--x':
        X = float(arg)
    elif opt == '--nprocs':
        NPROCS = int(arg)
if None in [IN_FNAME]:
    usage()
    sys.exit(2)

with open(IN_FNAME, 'rb') as istr:
    pi = dill.load(istr)


if PLOTTING_SUPPORTED and pi.real_observations is None:
    senspos = np.asarray(pi.sens_pos_list)
    
    plt.figure()
    dol.plot(pi.mesh)
    plt.show(False)

    # Plot field
    dl = 1e-4
    vmin = (1-dl) * np.min(pi.field_vals) if VMIN is None else VMIN
    vmax = (1+dl) * np.max(pi.field_vals) if VMAX is None else VMAX
    levels = np.linspace(vmin, vmax, 50)
    plt.figure()
    p = dol.plot(pi.true_field, levels=levels)
    plt.scatter(senspos[:,0], senspos[:,1], c='r')
    if CBAR: plt.colorbar(p)
    if not AXIS: plt.axis('off')
    if TITLE: plt.title('Data generating field')
    plt.show(False)

    # Plot observations
    Y = [ ll.y for ll,_ in pi.logL.factors ]
    theta = np.linspace(0, 2*np.pi, len(Y[0]))
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i, y in enumerate(Y):
        ax.plot(theta, y, '-o', label='freq: %d' % (i+1))
    ax.legend(loc='lower left', fontsize=12)
    if not AXIS:
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.get_yaxis().set_tick_params(direction='in')
        ax.get_xaxis().set_tick_params(direction='in')
    plt.show(False)

    for ftdm in pi.field_to_data_maps_list:
        u = NoElectrodeEITSolver.solve(ftdm.current, pi.true_field, pi.W)
        plt.figure()
        p = dol.plot(u)
        for c in p.collections:
            c.set_edgecolor("face")
        plt.scatter(senspos[:,0], senspos[:,1], c='r')
        if CBAR: plt.colorbar(p)
        if not AXIS: plt.axis('off')
        if TITLE: plt.title('Solution - frequency %d' % ftdm.freq)
        plt.show(False)

if DO_TAYLOR:
    # Taylor test for derivatives
    x = X * np.ones((N_TAYLOR,pi.dim))
    dx = npr.randn(N_TAYLOR,pi.dim)
    TM.taylor_test(x, dx, h=1e-4,
                   f = pi.tuple_grad_x_log_pdf,
                   ahf = pi.action_hess_x_log_pdf,
                   fungrad=True, caching=True)

if DO_FD:
    # Finite difference testing (uses MPI)
    mpi_pool = None
    if NPROCS > 1:
        mpi_pool = TM.get_mpi_pool()
        mpi_pool.start(NPROCS)

    try:
        # Load distribution in memory
        pi.reset_counters()
        TM.mpi_alloc_dmem(pi=pi, mpi_pool=mpi_pool)

        # Compute Hessian from action of Hessian
        x = X * np.ones((pi.dim,pi.dim))
        dx = np.eye(pi.dim)
        scatter_tuple = (['x', 'dx'], [x, dx])
        hess = TM.mpi_map(
            'action_hess_x_log_pdf', scatter_tuple=scatter_tuple,
            obj='pi', obj_val=pi, mpi_pool=mpi_pool)
        plt.figure()
        plt.imshow(np.log10(np.abs(hess)))
        plt.colorbar()
        if TITLE: plt.title("Hessian from action")
        plt.show(False)

        # Compute Hessian using finite difference
        h = 1e-6
        xp = X * np.ones((pi.dim,pi.dim)) + h/2 * np.eye(pi.dim)
        xm = X * np.ones((pi.dim,pi.dim)) - h/2 * np.eye(pi.dim)
        fp = TM.mpi_map(
            'grad_x_log_pdf', scatter_tuple=(['x'],[xp]),
            obj='pi', obj_val=pi, mpi_pool=mpi_pool)
        fm = TM.mpi_map(
            'grad_x_log_pdf', scatter_tuple=(['x'],[xm]),
            obj='pi', obj_val=pi, mpi_pool=mpi_pool)
        fdhess = (fp-fm)/h
        plt.figure()
        plt.imshow(np.log10(np.abs(fdhess)))
        plt.colorbar()
        if TITLE: plt.title("Hessian from finite difference")
        plt.show(False)

        plt.figure()
        plt.imshow(np.log10(np.abs(hess-fdhess)))
        plt.colorbar()
        if TITLE: plt.title(
                "Hessian discrepancy - max err.: %e" % np.max(np.abs(hess-fdhess)))
        plt.show(False)
    finally:
        if mpi_pool is not None:
            mpi_pool.stop()

# Timing
if DO_TIMING:
    print("")
    print("Timing")

    N = DO_TIMING_N
    x = pi.prior.rvs(N)
    dx = npr.randn(N, pi.dim)

    if DO_MPI:
        mpi_pool = TM.get_mpi_pool()
        mpi_pool.start(NPROCS)

    try:
        pi.reset_counters()        

        if DO_MPI:
            # Load distribution in memory
            start = time.process_time()
            TM.mpi_alloc_dmem(pi=pi, mpi_pool=mpi_pool)
            stop = time.process_time()
            print("MPI allocation time: %.2f s" % (stop-start))

        # Function evaluation
        if DO_MPI:
            mpi_start = time.process_time()
            scatter_tuple= (['x'], [x])
            mpi_lpdf = TM.mpi_map(
                'log_pdf', scatter_tuple=scatter_tuple,
                obj='pi', mpi_pool=mpi_pool)
            mpi_stop = time.process_time()
            print("Function evaluation: MPI done.")
        if DO_SERIAL:
            start = time.process_time()
            lpdf = pi.log_pdf(x)
            stop = time.process_time()
            print("Function evaluation: Serial done.")
        if DO_MPI and DO_SERIAL:
            assert np.all(np.allclose(mpi_lpdf, lpdf))
            print("Function evaluation success. " + \
                  "Serial timing %.2f s. " % ((stop-start)/N) + \
                  "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))
        elif DO_SERIAL:
            print("Function evaluation success. " + \
                  "Serial timing %.2f s. " % ((stop-start)/N))
        elif DO_MPI:
            print("Function evaluation success. " + \
                  "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))

        # Distribute cache
        cache = {'tot_size': N}
        if DO_MPI: 
            def f(x=x, *args, **kwargs):
                cache = {'tot_size': x.shape[0]}
                return (cache,)
            scatter_tuple= (['x'], [x])
            TM.mpi_map_alloc_dmem(
                f, scatter_tuple=scatter_tuple,
                dmem_key_out_list=['cache'],
                mpi_pool=mpi_pool,
                concatenate=False)

        # Function/gradient evaluation
        if DO_MPI:
            mpi_start = time.process_time()
            scatter_tuple= (['x'], [x])
            dmem_key_in_list = ['cache']
            dmem_arg_in_list = ['cache']
            mpi_lpdf, mpi_gxlpdf = TM.mpi_map(
                'tuple_grad_x_log_pdf', scatter_tuple=scatter_tuple,
                dmem_key_in_list=dmem_key_in_list,
                dmem_arg_in_list=dmem_arg_in_list,
                obj='pi', mpi_pool=mpi_pool)
            mpi_stop = time.process_time()
            print("Function/gradient evaluation: MPI done.")
        if DO_SERIAL:
            start = time.process_time()
            lpdf, gxlpdf = pi.tuple_grad_x_log_pdf(x, cache=cache)
            stop = time.process_time()
            print("Function/gradient evaluation: Serial done.")
        if DO_MPI and DO_SERIAL:
            assert np.all(np.allclose(mpi_lpdf, lpdf))
            assert np.all(np.allclose(mpi_gxlpdf, gxlpdf))
            print("Function/gradient evaluation success. " + \
                  "Serial timing %.2f s. " % ((stop-start)/N) + \
                  "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))
        elif DO_SERIAL:
            print("Function/gradient evaluation success. " + \
                  "Serial timing %.2f s. " % ((stop-start)/N))
        elif DO_MPI:
            print("Function/gradient evaluation success. " + \
                  "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))
            
        # Action of Hessian
        if DO_MPI:
            mpi_start = time.process_time()
            scatter_tuple= (['x', 'dx'], [x, dx])
            dmem_key_in_list = ['cache']
            dmem_arg_in_list = ['cache']
            mpi_ahxlpdf = TM.mpi_map(
                'action_hess_x_log_pdf', scatter_tuple=scatter_tuple,
                dmem_key_in_list=dmem_key_in_list,
                dmem_arg_in_list=dmem_arg_in_list,
                obj='pi', mpi_pool=mpi_pool)
            mpi_stop = time.process_time()
            print("Action Hessian evaluation: MPI done.")
        if DO_SERIAL:
            start = time.process_time()
            ahxlpdf = pi.action_hess_x_log_pdf(x, dx, cache=cache)
            stop = time.process_time()
            print("Action Hessian evaluation: Serial done.")
        if DO_MPI and DO_SERIAL:
            assert np.all(np.allclose(mpi_ahxlpdf, ahxlpdf))
            print("Action Hessian evaluation success. " + \
                  "Serial timing %.2f s. " % ((stop-start)/N) + \
                  "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))
        elif DO_SERIAL:
            print("Action Hessian evaluation success. " + \
                  "Serial timing %.2f s. " % ((stop-start)/N))
        elif DO_MPI:
            print("Action Hessian evaluation success. " + \
                  "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))
    finally:
        if DO_MPI:
            mpi_pool.stop()
