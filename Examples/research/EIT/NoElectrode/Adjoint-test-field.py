import subprocess
import dolfin as dol
import mshr

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr

from GmshGeoGenerator import generate_geo
from NoElectrodeEITDistributions import \
    NoElectrodeEITSolver, CirclePhantom, FieldExpression

import TransportMaps as TM
import TransportMaps.FiniteDifference as FD

dol.set_log_level(30)

#
# Cheng, K. S., Isaacson, D., Newell, J. C., & Gisser, D. G. (1989).
# Electrode Models for Electric Current Computed Tomography.
# IEEE Transactions on Biomedical Engineering, 36(9), 918–924.
# https://doi.org/10.1109/10.35300
#
# In this setting the cylindrical bath is completely surrounded by
# electrodes all around it (not on the top and bottom)
# The model is described by
#
#   \nabla \cdot \sigma(x) \nabla U = 0                     for x\in B
#   \sigma(x) \frac{\partial U(x)}{\partial n} = g^k(x)     for x\in \partial B
#
# where the current pattern g^k(x) := j^k(\theta), for \theta = \arctan2(x_1,x_2),
# is given by
#
#   j^k(\theta) = \frac{5 \cos(k\theta)}{A}
#   A = \frac{f 2 \pi h r_0 }{L}   (area of one electrode)
#
# with 
#
#   f   = 1          (fraction of circumference covered by electrodes)
#   h   = 4.24 cm    (depth of the bath)
#   r_0 = 15 cm      (radius of the bath)
#   L   = 32         (number of electrodes)
#

# parameters['allow_extrapolation'] = True

rho = 284.0 # Bath resistivities

PLOTTING = True
k = 8 # Frequencies k=1,...,16

# Parameters
h  = 4.24
r0 = 15.
L  = 32

A = 2 * np.pi * h * r0 / L

# Sensor std
std = 0.1

ndiscr = 25

# Pointwise sensor
# boundary_sensor_str = '1./sqrt(2.*pi*pow(std,2)) * ' + \
#                       'exp(-.5 * (pow(c0-x[0],2) + pow(c1-x[1],2)) / pow(std,2))'
boundary_sensor_str = \
    '1./sqrt(2.*pi*pow(std,2)) * ' + \
    'exp(-.5 * pow((atan2(c1, c0)-atan2(x[1], x[0]))*r0,2) / pow(std,2))'

        
# Geometry
geometry = mshr.Circle(dol.Point(0, 0), r0)
mesh = mshr.generate_mesh(geometry, ndiscr)

# Finite element space
VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
VEFS = dol.FunctionSpace(mesh, VE)
RE = dol.FiniteElement("R", dol.triangle, 0)
REFS = dol.FunctionSpace(mesh, RE)
VERE = dol.MixedElement([VE, RE])
W = dol.FunctionSpace(mesh, VERE)

coord = VEFS.tabulate_dof_coordinates().reshape((-1,2))
ndofs = coord.shape[0]

N = 1000 # Interpolation points

current_expr = dol.Expression(
    '0.5 * cos( k * atan2(x[1], x[0]) ) / A', k=k, A=A, element=VE)

# Constant field
sigma = 1/284. * np.ones(ndofs)
sigma_fun = NoElectrodeEITSolver.dof_to_fun(sigma, VEFS)
# # Phantom field
# cp = CirclePhantom(c=np.array([-7,6]), r=4, v=6, b=0.1)
# # cp = CirclePhantom(c=np.array([7,0]), r=4, v=6, b=0.1)
# sigma = FieldExpression(cp, element=VE)
# sigma_fun = dol.project(sigma, VEFS)
# sigma = sigma_fun.vector().get_local()

plt.figure()
p = dol.plot(sigma_fun)
plt.colorbar(p)
plt.show(False)

u = NoElectrodeEITSolver.solve(current_expr, sigma_fun, W)

plt.figure()
p = dol.plot(u)
plt.colorbar(p)
plt.show(False)

# Sensor
sensor_expr = dol.Expression(
    boundary_sensor_str, c0=0., c1=r0, std=std, pi=np.pi,
    r0=r0, element=VE)

def observe(sigma, sensor_expr, current_expr, W, FS):
    vlt = np.zeros(sigma.shape[0])
    for i in range(sigma.shape[0]):
        sigma_fun = NoElectrodeEITSolver.dof_to_fun(sigma[i,:], FS)
        u = NoElectrodeEITSolver.solve(current_expr, sigma_fun, W)
        vlt[i] = dol.assemble(sensor_expr * u * dol.ds)
    return vlt

def grad_observe(sigma, sensor_expr, current_expr, W, FS):
    out = np.zeros(sigma.shape)
    for i in range(sigma.shape[0]):
        v = dol.TestFunction(FS)
        sigma_fun = NoElectrodeEITSolver.dof_to_fun(sigma[i,:], FS)
        # Solve
        u = NoElectrodeEITSolver.solve(current_expr, sigma_fun, W)
        adj = NoElectrodeEITSolver.solve(sensor_expr, sigma_fun, W)
        grd = - dol.inner( dol.grad(adj), dol.grad(u) )
        out[i,:] = dol.assemble( grd * v * dol.dx ).get_local()
    return out

def action_hess_observe(sigma, dx, sensor_expr, current_expr, W, FS):
    out = np.zeros(sigma.shape)
    for i in range(sigma.shape[0]):
        print("Adjoint action: %d/%d" % (i+1, sigma.shape[0]))
        v = dol.TestFunction(FS)
        sigma_fun = NoElectrodeEITSolver.dof_to_fun(sigma[i,:], FS)
        dx_fun = NoElectrodeEITSolver.dof_to_fun(dx[i,:], FS)
        # Solve
        u = NoElectrodeEITSolver.solve(current_expr, sigma_fun, W)    
        adj = NoElectrodeEITSolver.solve(sensor_expr, sigma_fun, W)
        A1Mu = NoElectrodeEITSolver.solve_action_hess_adjoint(dx_fun, u, sigma_fun, W)
        A1Madj = NoElectrodeEITSolver.solve_action_hess_adjoint(dx_fun, adj, sigma_fun, W)
        ahess = dol.inner( dol.grad(adj), dol.grad(A1Mu) ) + \
                dol.inner( dol.grad(A1Madj), dol.grad(u) )
        out[i,:] = dol.assemble( ahess * v * dol.dx ).get_local()
    return out

# Finite difference and Taylor test
ntest = 10
h = 1e-4
x = np.tile(sigma[np.newaxis,:], (ntest,1))
dx = npr.randn(ntest,ndofs)

args = {'sensor_expr': sensor_expr,
        'current_expr': current_expr,
        'W': W,
        'FS': VEFS}
TM.taylor_test(x, dx, h=h,
               f=observe,
               gf=grad_observe,
               ahf=action_hess_observe,
               args=args)

print("Checking gradient with finite difference")
fdgrad = FD.fd(observe, x[[0],:], 1e-4, args)[0,:]
grad = grad_observe(x[[0],:], **args)
print("Gradient max error: %e" % np.max(np.abs(fdgrad-grad)))

print("Checking Hessian with finite difference")
# Compute Hessian by applying it to the Identiy matrix
x = np.tile(sigma[np.newaxis,:], (ndofs,1))
dx = np.eye(ndofs)
hess = action_hess_observe(x, dx, sensor_expr, current_expr, W, VEFS)
plt.figure()
plt.imshow(np.log10(np.abs(hess)))
plt.colorbar()
plt.title("Hessian from action")
plt.show(False)

# Finite difference Hessian
fdhess = FD.fd(grad_observe, x[[0],:], 1e-6, args)[0,:,:]
plt.figure()
plt.imshow(np.log10(np.abs(fdhess)))
plt.colorbar()
plt.title("Hessian from finite difference")
plt.show(False)

plt.figure()
plt.imshow(np.log10(np.abs(hess-fdhess)))
plt.colorbar()
plt.title("Error Hessian action to difference - max: %e" % np.max(np.abs(hess-fdhess)))
plt.show(False)
