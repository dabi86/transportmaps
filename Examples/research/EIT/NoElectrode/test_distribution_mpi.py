#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import time
import dill
import numpy as np
import numpy.random as npr
import dolfin as dol

import TransportMaps as TM
import NoElectrodeEITDistributions as EITDIST

def usage():
    print('test_distribution.py --input=<filename>')

argv = sys.argv[1:]
IN_FNAME = None
try:
    opts, args = getopt.getopt(argv, "h", ['input='])
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
if None in [IN_FNAME]:
    usage()
    sys.exit(2)

with open(IN_FNAME, 'rb') as istr:
    pi = dill.load(istr)

print("")
print("TEST MPI")
print("")

N = 60
x = pi.prior.rvs(N)
dx = npr.randn(N, pi.dim)

mpi_pool = TM.get_mpi_pool()
mpi_pool.start(32)
# mpi_pool.set_log_level(10)
print("Started MPI pool")

try:
    # Load distribution in memory
    pi.reset_counters()
    start = time.process_time()
    TM.mpi_alloc_dmem(pi=pi, mpi_pool=mpi_pool)
    stop = time.process_time()
    print("MPI allocation time: %.2f s" % (stop-start))

    # Function evaluation
    print("Function evaluation: MPI start")
    mpi_start = time.process_time()
    scatter_tuple= (['x'], [x])
    mpi_lpdf = TM.mpi_map(
        'log_pdf', scatter_tuple=scatter_tuple,
        obj='pi', mpi_pool=mpi_pool)
    mpi_stop = time.process_time()
    print("Function evaluation: MPI done.")
    start = time.process_time()
    lpdf = pi.log_pdf(x)
    stop = time.process_time()
    print("Function evaluation: Serial done.")
    assert np.all(np.allclose(mpi_lpdf, lpdf))
    print("Function evaluation success. " + \
          "Serial timing %.2f s. " % ((stop-start)/N) + \
          "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))

    # Distribute cache
    print("Distributing cache: start")
    cache = {'tot_size': N}
    def f(x=x, *args, **kwargs):
        cache = {'tot_size': x.shape[0]}
        return (cache,)
    scatter_tuple= (['x'], [x])
    TM.mpi_map_alloc_dmem(
        f, scatter_tuple=scatter_tuple,
        dmem_key_out_list=['cache'],
        mpi_pool=mpi_pool,
        concatenate=False)
    print("Distributing cache: done")
    
    # Function/gradient evaluation
    print("Function/gradient evaluation: MPI start.")
    mpi_start = time.process_time()
    scatter_tuple= (['x'], [x])
    dmem_key_in_list = ['cache']
    dmem_arg_in_list = ['cache']
    mpi_lpdf, mpi_gxlpdf = TM.mpi_map(
        'tuple_grad_x_log_pdf', scatter_tuple=scatter_tuple,
        dmem_key_in_list=dmem_key_in_list,
        dmem_arg_in_list=dmem_arg_in_list,
        obj='pi', mpi_pool=mpi_pool)
    mpi_stop = time.process_time()
    print("Function/gradient evaluation: MPI done.")
    start = time.process_time()
    lpdf, gxlpdf = pi.tuple_grad_x_log_pdf(x, cache=cache)
    stop = time.process_time()
    print("Function/gradient evaluation: Serial done.")
    assert np.all(np.allclose(mpi_lpdf, lpdf))
    assert np.all(np.allclose(mpi_gxlpdf, gxlpdf))
    print("Function/gradient evaluation success. " + \
          "Serial timing %.2f s. " % ((stop-start)/N) + \
          "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))

    # Action of Hessian
    print("Action Hessian evaluation: MPI start.")
    mpi_start = time.process_time()
    scatter_tuple= (['x', 'dx'], [x, dx])
    dmem_key_in_list = ['cache']
    dmem_arg_in_list = ['cache']
    mpi_ahxlpdf = TM.mpi_map(
        'action_hess_x_log_pdf', scatter_tuple=scatter_tuple,
        dmem_key_in_list=dmem_key_in_list,
        dmem_arg_in_list=dmem_arg_in_list,
        obj='pi', mpi_pool=mpi_pool)
    mpi_stop = time.process_time()
    print("Action Hessian evaluation: MPI done.")
    start = time.process_time()
    ahxlpdf = pi.action_hess_x_log_pdf(x, dx, cache=cache)
    stop = time.process_time()
    print("Action Hessian evaluation: Serial done.")
    assert np.all(np.allclose(mpi_ahxlpdf, ahxlpdf))
    print("Action Hessian evaluation success. " + \
          "Serial timing %.2f s. " % ((stop-start)/N) + \
          "MPI timing %.2f s." % ((mpi_stop-mpi_start)/N))
finally:
    mpi_pool.stop()
