import numpy as np

def generate_geo(mesh_size=0.5, r0=15., L=8):

    outstr = ""
    
    Delta = 2 * np.pi / float(L)

    tls = np.arange(1,L+1) * 2 * np.pi / float(L)
    npts = len(tls)

    # center points of electrodes
    pnts = np.vstack( (r0 * np.cos(tls), r0 * np.sin(tls)) ).T 

    # Writing points
    outstr += "Point(0) = {0,0,0,%.10f};\n" % (mesh_size)
    for i in range(npts):
        outstr += "Point(%d) = " % (i+1) + \
                  "{%.10f,%.10f,0,%.10f};\n" % (
                      pnts[i,0], pnts[i,1],mesh_size)
    # Defining arcs
    for i in range(npts-1):
        outstr += "Circle(%d) = {%d, %d, %d};\n" % (i, i+1, 0, i+2)
    outstr += "Circle(%d) = {%d, %d, %d};\n" % (npts-1, npts, 0, 1)
    # Define line loop (circle)
    outstr += "Line Loop(%d) = {" % npts + \
            ",".join([str(i) for i in range(npts)]) + "};\n"
    # Define surface
    outstr += "Plane Surface(0) = {%d};\n" % npts

    # Define physical points
    outstr += "Physical Point(\"electrodes_bounds\") = {" + \
            ",".join([str(i) for i in range(1,npts+1)]) + "};\n"
    # Physical lines
    outstr += "Physical Line(\"electrodes\") = {" + \
            ",".join([str(i) for i in range(0,npts,2)]) + "};\n"
    outstr += "Physical Line(\"gaps\") = {" + \
            ",".join([str(i) for i in range(1,npts,2)]) + "};\n"
    # Physical surface
    outstr += "Physical Surface(\"disk\") = {0};\n"

    return outstr