#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import dill
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import dolfin as dol

import TransportMaps as TM
import NoElectrodeEITDistributions as EITDIST

def usage():
    print('ConstantFieldDataGeneration.py --output=<filename> [ ' + \
          '--ndiscr=200 --n-sens=8 --r0=15. --h=4.24 --sens-geo-std=0.1 ' + \
          '--prior-mean=0. --prior-var=1e-1 ' + \
          '--lkl-var=1e-2 --lkl-cov=id --lkl-corr-len=1e-1')
    print(
"""Available covariances:
 - id      Gaussian noise (identity)
 - ou      Ornstein–Uhlenbeck (exponential)
 - sqexp   Squared exponential
""")

argv = sys.argv[1:]
OUT_FNAME = None
# Physics settings
NDISCR = 50
N_SENS = 8
SENS_GEO_STD = 0.1
R0 = 15.
H = 4.24
# Prior settings
PRIOR_MEAN = 0.
PRIOR_VAR = 1e-1
# Likelihood settings
LKL_VAR = 1e-2
LKL_COV = 'id'
LKL_CORR_LEN = 1e-1
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "output=",
            # Physics settings
            "ndiscr=", "n-sens=", "sens-geo-std=", "r0=", "h="
            # Prior settings
            "prior-mean=", "prior-var=", 
            # Likelihood settings
            "lkl-var=", "lkl-cov=", "lkl-corr-len="
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--output':
        OUT_FNAME = arg

    # Physics settings
    elif opt == '--ndiscr':
        NDISCR = int(arg)
    elif opt == '--n-sens':
        N_SENS = int(arg)
    elif opt == '--sens-geo-std':
        SENS_GEO_STD = float(arg)
    elif opt == '--r0':
        RO = float(arg)
    elif opt == '--h':
        H = float(arg)

    # Prior settings
    elif opt == '--prior-mean':
        PRIOR_MEAN = float(arg)
    elif opt == '--prior-var':
        PRIOR_VAR = float(arg)

    # Likelihood settings
    elif opt == '--lkl-var':
        LKL_VAR = float(arg)
    elif opt == '--lkl-cov':
        LKL_COV = arg
    elif opt == '--lkl-corr-len':
        LKL_CORR_LEN = float(arg)
if None in [OUT_FNAME]:
    usage()
    sys.exit(2)

pi = EITDIST.ConstantFieldNoElectrodeEITDistribution(
    # Physiscs settings
    ndiscr=NDISCR, n_sens=N_SENS, sens_geo_std=SENS_GEO_STD, r0=R0, h=H,
    # Prior settings
    prior_mean=PRIOR_MEAN,
    prior_var=PRIOR_VAR, 
    # Likelihood settings
    lkl_var=LKL_VAR, lkl_cov=LKL_COV,
    lkl_corr_len=LKL_CORR_LEN
)

for ftdm in pi.field_to_data_maps_list:
    u = EITDIST.solve(ftdm.current, pi.true_field, pi.W)
    plt.figure()
    p = dol.plot(u)
    plt.colorbar(p)
    plt.title('Solution - frequency %d' % ftdm.freq)
    plt.show(False)

# Taylor test for derivatives
npoints = 1
x = np.ones((npoints,pi.dim))
dx = np.tile(npr.randn(1,pi.dim), (npoints, 1))
TM.taylor_test(x, dx, f=pi.tuple_grad_x_log_pdf, fungrad=True)
        
# Store Distributions
with open(OUT_FNAME, 'wb') as ostr:
    dill.dump(pi, ostr)