import dolfin as dol

def solve(current, sigma, W):
    # Trial an test functions
    (u, c) = dol.TrialFunction(W)
    (v, d) = dol.TestFunction(W)
    # Forms
    LagMult = (c*v + u*d) * dol.ds
    lhs = dol.inner(dol.nabla_grad(u), sigma * dol.nabla_grad(v)) * dol.dx + LagMult
    rhs = current * v * dol.ds
    # Solve
    w = dol.Function(W)
    dol.solve(lhs == rhs, w)
    (u, c) = w.split()
    return u

def solve_action_hess_adjoint(dx, usol, sigma, W):
    # Trial an test functions
    (u, c) = dol.TrialFunction(W)
    (v, d) = dol.TestFunction(W)
    # Forms
    LagMult = (c*v + u*d) * dol.ds
    lhs = dol.inner(dol.nabla_grad(u), sigma * dol.nabla_grad(v)) * dol.dx + LagMult
    rhs = dol.inner(dol.nabla_grad(usol), dx * dol.nabla_grad(v)) * dol.dx
    # Solve
    w = dol.Function(W)
    dol.solve(lhs == rhs, w)
    (u, c) = w.split()
    return u
    