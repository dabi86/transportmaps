#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import print_function
import sys, getopt
import time
import dill
import h5py
import numpy as np
import numpy.random as npr
import dolfin as dol
import matplotlib.pyplot as plt

import TransportMaps as TM
from NoElectrodeEITDistributions import NoElectrodeEITSolver

nax = np.newaxis

def shell_input():
    try:
        cmd = raw_input()
    except NameError:
        cmd = input()
    return cmd

def usage():
    print('postprocess-plotting.py --fname=<filename> --no-cbar --no-axis --no-title')

params = {}
    
argv = sys.argv[1:]
IN_FNAME = None
try:
    opts, args = getopt.getopt(argv, "h", ['fname=', 'no-cbar', 'no-axis', 'no-title'])
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--fname':
        IN_FNAME = arg
    elif opt == '--no-cbar':
        params['colorbar'] = False
    elif opt == '--no-axis':
        params['axis'] = False
    elif opt == '--no-title':
        params['title'] = False
if None in [IN_FNAME]:
    usage()
    sys.exit(2)

# Open dill file and hdf5 data set
with open(IN_FNAME, 'rb') as istr:
    stg = dill.load(istr)
    senspos = np.asarray(stg.target_distribution.sens_pos_list)
try:
    h5fname = '.'.join(IN_FNAME.split('.')[:-1])
    h5file = h5py.File(h5fname + '-POST.dill.hdf5', 'r')

    # Provide list of commands to explore hdf5 and plotting
    print("Use ? for help.")
    pwd = '/'
    cmd = ''
    while cmd != 'q':
        print(pwd + "> ", end='')
        cmd = shell_input()
        cmd_lst = cmd.split(' ')
        if cmd_lst[0] == '?':
            if len(cmd_lst) == 1:
                print("List of commands:")
                print("  ?      help")
                print("  ls     list groups/datasets")
                print("  cd     enter group")
                print("  q      quit")
        elif cmd_lst[0] == 'ls':
            if len(cmd_lst) == 1:
                ls_pwd = pwd
            else:
                ls_pwd = pwd + '/' + '/'.join(cmd_lst[1:])
            try:
                print( list(h5file[ls_pwd].keys()) )
            except KeyError:
                print("No such group or dataset")
            except AttributeError:
                print( h5file[ls_pwd] )
        elif cmd_lst[0] == 'cd':
            if len(cmd_lst) == 1:
                pwd = '/'
            else:
                cd_pwd_split = pwd.split('/')[:-1] + cmd_lst[1].split('/')
                cd_pwd_split_cmpr = []
                skip = 0
                for s in reversed(cd_pwd_split):
                    if s == '.':
                        pass
                    elif s == '..':
                        skip += 1
                    else:
                        if skip > 0:
                            skip -= 1
                        else:
                            cd_pwd_split_cmpr.append(s)
                cd_pwd = '/'.join(reversed( cd_pwd_split_cmpr )) + '/'
                if cd_pwd in h5file:
                    pwd = cd_pwd
                else:
                    print("Group/dataset " + cd_pwd + " does not exist.")
        elif cmd_lst[0] == 'set':
            try:
                set_cmd = cmd_lst[1:]
                if set_cmd[0] == 'float':
                    params[set_cmd[1]] = float(set_cmd[2])
                elif set_cmd[0] == 'int':
                    params[set_cmd[1]] = int(set_cmd[2])
                elif set_cmd[0] == 'str':
                    params[set_cmd[1]] = set_cmd[2]
                elif set_cmd[0] == 'bool':
                    params[set_cmd[1]] = (set_cmd[2]=='True')
                else:
                    raise AttributeError("Unrecognized type %s" % set_cmd[0])
            except Exception as e:
                print("Error: %s" % e)
        elif cmd_lst[0] == 'unset':
            try:
                unset_cmd = cmd_lst[1:]
                del params[unset_cmd[0]]
            except Exception as e:
                print("Error: %s" % e)
        elif cmd_lst[0] == 'get':
            try:
                get_cmd = cmd_lst[1:]
                print(params.get(get_cmd[0]))
            except Exception as e:
                print("Error: %s" % e)
        elif cmd_lst[0] == 'plot':
            try:
                plt_cmd = cmd_lst[1:]
                if plt_cmd[0] in ['exp(mean)', 'mean', 'var', 'std', 'log(std)']:
                    if len(plt_cmd) > 1:
                        x = h5file[pwd + plt_cmd[1]]
                    else:
                        x = h5file[pwd]
                    if len(plt_cmd) > 2:
                        w = h5file[pwd + plt_cmd[2]]
                    else:
                        nx = x.shape[0]
                        w = np.ones(nx)/float(nx)
                elif plt_cmd[0] in ['exp(sample)', 'sample', 'hist']:
                    x = h5file[pwd]
                else:
                    raise AttributeError("plot command " + plt_cmd[0] + "not valid")
                if plt_cmd[0] in ['exp(mean)', 'mean', 'var', 'std', 'log10(std)']:
                    if plt_cmd[0] in ['mean','exp(mean)']:
                        fld = np.dot(w, x)
                        if plt_cmd[0] == 'exp(mean)':
                            fld = np.exp(fld)
                    elif plt_cmd[0] in ['var', 'std']:
                        mn = np.dot(w, x)
                        fld = np.dot(w, (x - mn[nax,:])**2)
                        if plt_cmd[0] in ['std', 'log10(std)']:
                            fld = np.sqrt(fld)
                        if plt_cmd[0] == 'log10(std)':
                            fld = np.log(fld)
                    vmin = params.get('vmin', np.min(fld))
                    vmax = params.get('vmax', np.max(fld))
                    levels = np.linspace(vmin, vmax, 50)
                    fld_fun = NoElectrodeEITSolver.dof_to_fun(
                        fld, stg.target_distribution.VEFS)
                    fig = plt.figure()
                    p = dol.plot(fld_fun, levels=levels)
                    for c in p.collections:
                        c.set_edgecolor("face")
                    plt.scatter(senspos[:,0], senspos[:,1], c='r')
                    if params.get('colorbar',True): plt.colorbar(p)
                    if not params.get('axis',True): plt.axis('off')
                    if params.get('title',True): plt.title(plt_cmd[0])
                    plt.show(False)
                elif plt_cmd[0] in ['sample', 'exp(sample)']:
                    if len(plt_cmd) == 1:
                        raise AttributeError(
                            "The \"plot sample\" " + \
                            "command must be followed by integers.")
                    for sint in plt_cmd[1:]:
                        fld = x[int(sint),:]
                        if plt_cmd[0] == 'exp(sample)':
                            fld = np.exp(fld)
                        fld_fun = NoElectrodeEITSolver.dof_to_fun(
                            fld, stg.target_distribution.VEFS)
                        fig = plt.figure()
                        p = dol.plot(fld_fun)
                        for c in p.collections:
                            c.set_edgecolor("face")
                        plt.scatter(senspos[:,0], senspos[:,1], c='r')
                        if params.get('colorbar',True): plt.colorbar(p)
                        if not params.get('axis',True): plt.axis('off')
                        if params.get('title',True): plt.title("Sample %d" % int(sint))
                        plt.show(False)
                elif plt_cmd[0] == 'hist':
                    fig = plt.figure()
                    plt.hist(x, bins=50, normed=True)
                    plt.show(False)
            except Exception as e:
                print("Error: %s" % e)
        elif cmd_lst[0] == 'q':
            pass
        else:
            print("Command not found. Use ? for help.")
finally:
    h5file.close()

