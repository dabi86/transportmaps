#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import subprocess
import dill
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import dolfin as dol

import TransportMaps as TM
import NoElectrodeEITDistributions as EITDIST
import GmshGeoGenerator as GMSH

def usage():
    print("""python ConstantFieldDataGeneration.py --output=<filename> [ 
   --mesh-size=0.5 --r0=15. --h=4.24 --sens-geo-std=0.1
   --n-sens=8 --n-freq=4
   --field-file=<fname> 
   --field-circle-phantom
      --circle-phantom-center=0,0 --circle-phantom-radius=5
      --circle-phantom-value=.01 --circle-phantom-bkground=1/284
   --prior-mean-field-file=<fname> --prior-mean=ln(1/284) --prior-var=1.
   --prior-cov=ou --prior-corr-len=5
   --lkl-var=5e-2 --lkl-cov=id --lkl-corr-len=1e-1 """)
    print(
"""Available covariances:
 - id      Gaussian noise (identity)
 - ou      Ornstein–Uhlenbeck (exponential)
 - sqexp   Squared exponential
""")

argv = sys.argv[1:]
OUT_FNAME = None
# Physics settings
MESH_SIZE = 0.5
N_SENS = 8
N_FREQ = 4
SENS_GEO_STD = 0.1
R0 = 15.
H = 4.24
# Generating field
FIELD_FNAME = None
FIELD_CIRCLE_PHANTOM = False
CIRCLE_PHANTOM_CENTER = (-7.,6.)
CIRCLE_PHANTOM_RADIUS = 5.
CIRCLE_PHANTOM_VALUE = .03
CIRCLE_PHANTOM_BKGROUND = 1./284.
FIELD_DOUBLE_CIRCLE_PHANTOM = False
CIRCLE2_PHANTOM_CENTER = (1.,5.)
CIRCLE2_PHANTOM_RADIUS = 8.
CIRCLE2_PHANTOM_VALUE = .015
CIRCLE12_PHANTOM_VALUE = .03
# Prior settings
PRIOR_MEAN_FIELD_FNAME = None
PRIOR_MEAN = np.log(1./284.)
PRIOR_VAR = 1.
PRIOR_COV = 'ou'
PRIOR_CORR_LEN = 5.
# Likelihood settings
LKL_VAR = 5e-2
LKL_COV = 'id'
LKL_CORR_LEN = 1e-1
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "output=",
            # Physics settings
            "mesh-size=", "r0=", "h=",
            "n-sens=", "sens-geo-std=",
            "n-freq=",
            # Generating field
            "field-file=",
            "field-circle-phantom",
            "circle-phantom-center=", "circle-phantom-radius=",
            "circle-phantom-value=", "circle-phantom-bkground=",
            "field-double-circle-phantom",
            "circle2-phantom-center=", "circle2-phantom-radius=",
            "circle2-phantom-value=", "circle12-phantom-value=",
            # Prior settings
            "prior-mean-field-file=", "prior-mean=",
            "prior-var=", "prior-cov=", "prior-corr-len=",
            # Likelihood settings
            "lkl-var=", "lkl-cov=", "lkl-corr-len="
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--output':
        OUT_FNAME = arg

    # Physics settings
    elif opt == '--mesh-size':
        MESH_SIZE = float(arg)
    elif opt == '--r0':
        RO = float(arg)
    elif opt == '--h':
        H = float(arg)
    elif opt == '--n-sens':
        N_SENS = int(arg)
    elif opt == '--sens-geo-std':
        SENS_GEO_STD = float(arg)
    elif opt == '--n-freq':
        N_FREQ = int(arg)

    # Generating field
    elif opt == '--field-fname':
        FIELD_FNAME = arg
    elif opt == '--field-circle-phantom':
        FIELD_CIRCLE_PHANTOM = True
    elif opt == '--circle-phantom-center':
        CIRCLE_PHANTOM_CENTER = tuple([float(c) for c in arg.split(',')])
    elif opt == '--circle-phantom-radius':
        CIRCLE_PHANTOM_RADIUS = float(arg)
    elif opt == '--circle-phantom-value':
        CIRCLE_PHANTOM_VALUE = float(arg)
    elif opt == '--circle-phantom-bkground':
        CIRCLE_PHANTOM_BKGROUND = float(arg)
    elif opt == '--field-double-circle-phantom':
        FIELD_DOUBLE_CIRCLE_PHANTOM = True
    elif opt == '--circle2-phantom-center':
        CIRCLE2_PHANTOM_CENTER = tuple([float(c) for c in arg.split(',')])
    elif opt == '--circle2-phantom-radius':
        CIRCLE2_PHANTOM_RADIUS = float(arg)
    elif opt == '--circle2-phantom-value':
        CIRCLE2_PHANTOM_VALUE = float(arg)
    elif opt == '--circle12-phantom-value':
        CIRCLE12_PHANTOM_VALUE = float(arg)

    # Prior settings
    elif opt == '--prior-mean-field-file':
        PRIOR_MEAN_FIELD_FNAME = arg
    elif opt == '--prior-mean':
        PRIOR_MEAN = float(arg)
    elif opt == '--prior-var':
        PRIOR_VAR = float(arg)
    elif opt == '--prior-cov':
        PRIOR_COV = arg
        if PRIOR_COV not in ['sqexp', 'ou']:
            raise NotImplementedError("The selected covariance is not implemented")
    elif opt == '--prior-corr-len':
        PRIOR_CORR_LEN = float(arg)
        
    # Likelihood settings
    elif opt == '--lkl-var':
        LKL_VAR = float(arg)
    elif opt == '--lkl-cov':
        LKL_COV = arg
    elif opt == '--lkl-corr-len':
        LKL_CORR_LEN = float(arg)

    else:
        raise AttributeError("Unknown option %s" % opt)
if None in [OUT_FNAME]:
    usage()
    sys.exit(2)

fname = 'mesh'
with open(fname + '.geo','w') as ostr:
    ostr.write( GMSH.generate_geo(MESH_SIZE, R0, N_SENS) )
subprocess.check_call("gmsh -2 " + fname + ".geo", shell=True)
subprocess.check_call("dolfin-convert " + fname + ".msh " + fname + ".xml", shell=True)
with open(fname + '.xml','r') as istr:
    mesh_xml = istr.readlines()

field_expr = None
if FIELD_CIRCLE_PHANTOM:
    field_expr = EITDIST.CirclePhantom(
        CIRCLE_PHANTOM_CENTER, CIRCLE_PHANTOM_RADIUS,
        CIRCLE_PHANTOM_VALUE, CIRCLE_PHANTOM_BKGROUND)
if FIELD_DOUBLE_CIRCLE_PHANTOM:
    field_expr = EITDIST.DoubleCirclePhantom(
        CIRCLE_PHANTOM_BKGROUND,
        CIRCLE_PHANTOM_CENTER, CIRCLE_PHANTOM_RADIUS, CIRCLE_PHANTOM_VALUE,
        CIRCLE2_PHANTOM_CENTER, CIRCLE2_PHANTOM_RADIUS, CIRCLE2_PHANTOM_VALUE,
        CIRCLE12_PHANTOM_VALUE)
    
observations=None
field_vals=None
prior_mean_field_vals=None
pi = EITDIST.GaussianFieldNoElectrodeEITDistribution(
    # Physiscs settings
    mesh_xml=mesh_xml, r0=R0, h=H,
    n_sens=N_SENS, sens_geo_std=SENS_GEO_STD,
    n_freq=N_FREQ,
    real_observations=observations,
    # Generating field
    field_vals=field_vals,
    field_expr=field_expr,
    # Prior settings
    prior_mean_field_vals=prior_mean_field_vals,
    prior_mean=PRIOR_MEAN, prior_var=PRIOR_VAR, prior_cov=PRIOR_COV,
    prior_corr_len=PRIOR_CORR_LEN,
    # Likelihood settings
    lkl_var=LKL_VAR, lkl_cov=LKL_COV,
    lkl_corr_len=LKL_CORR_LEN
)

if pi.real_observations is None:
    # Plot mesh
    plt.figure()
    dol.plot(pi.mesh)
    plt.show(False)
    
    # Plot field
    dl = 1e-4
    vmin = np.min(pi.field_vals)
    vmax = np.max(pi.field_vals)
    levels = np.linspace((1-dl)*vmin, (1+dl)*vmax, 50)
    plt.figure()
    p = dol.plot(pi.true_field, levels=levels)
    plt.colorbar(p)
    plt.title('True field')
    plt.show(False)

    # Plot observations
    Y = [ ll.y for ll,_ in pi.logL.factors ]
    theta = np.linspace(0, 2*np.pi, len(Y[0]))
    plt.figure()
    for i, y in enumerate(Y):
        plt.plot(theta, y, label='freq: %d' % (i+1))
    plt.legend(loc='best')
    plt.show(False)

    solver = EITDIST.NoElectrodeEITSolver()
    for ftdm in pi.field_to_data_maps_list:
        u = solver.solve(ftdm.current, pi.true_field, pi.W)
        plt.figure()
        p = dol.plot(u)
        plt.colorbar(p)
        plt.title('Solution - frequency %d' % ftdm.freq)
        plt.show(False)

# Taylor test for derivatives
npoints = 1
x = np.ones((npoints,pi.dim))
dx = np.tile(npr.randn(1,pi.dim), (npoints, 1))
TM.taylor_test(x, dx,
               f = pi.tuple_grad_x_log_pdf,
               ahf = pi.action_hess_x_log_pdf,
               fungrad=True, caching=True)
        
# Store Distributions
with open(OUT_FNAME, 'wb') as ostr:
    dill.dump(pi, ostr)