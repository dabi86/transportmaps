#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import mpi4py.MPI as MPI
import numpy as np
from scipy.spatial.distance import pdist, squareform

from petsc4py import PETSc
import dolfin as dol
import mshr

import TransportMaps.Distributions as DIST
import TransportMaps.Distributions.Inference as DISTINF
import TransportMaps.Likelihoods as LKL
import src.TransportMaps.Maps as MAPS
from TransportMaps import counted, cached, cached_tuple, get_sub_cache

from GmshGeoGenerator import generate_geo

dol.set_log_level(30)
# dol.parameters["reorder_dofs_serial"] = False

class NoElectrodeEITSolver(object):
    @staticmethod
    def solve(current, sigma, W):
        # Trial an test functions
        (u, c) = dol.TrialFunction(W)
        (v, d) = dol.TestFunction(W)
        # Forms
        LagMult = (c*v + u*d) * dol.ds
        lhs = dol.inner(dol.nabla_grad(u), sigma * dol.nabla_grad(v)) * dol.dx + LagMult
        rhs = current * v * dol.ds
        # Solve
        w = dol.Function(W)
        dol.solve(lhs == rhs, w)
        (u, c) = w.split(deepcopy=True)
        return u

    @staticmethod
    def solve_action_hess_adjoint(dx, usol, sigma, W):
        # Trial an test functions
        (u, c) = dol.TrialFunction(W)
        (v, d) = dol.TestFunction(W)
        # Forms
        LagMult = (c*v + u*d) * dol.ds
        lhs = dol.inner(dol.nabla_grad(u), sigma * dol.nabla_grad(v)) * dol.dx + LagMult
        rhs = dol.inner(dol.nabla_grad(usol), dx * dol.nabla_grad(v)) * dol.dx
        # Solve
        w = dol.Function(W)
        dol.solve(lhs == rhs, w)
        (u, c) = w.split(deepcopy=True)
        return u

    @staticmethod
    def dof_to_fun(x, FS):
        ndofs = len(x)
        fun = dol.Function(FS)
        fun.vector().set_local( x, np.arange(ndofs, dtype=np.intc) )
        fun.vector().apply('insert')
        return fun
    
def ou_cov(var, dists, l):
    return var * np.exp( - dists / l )

def sqexp_cov(var, dists, l):
    return var * np.exp( - dists**2 / 2 / l**2 )

def id_cov(var, n):
    return var * np.eye(n)
    
class ConstantFieldNoElectrodeEITDistribution(DISTINF.BayesPosteriorDistribution):

    required_init_args = set([
        # Physics settings
        'r0', 'h', 'ndiscr', 'n_sens', 'sens_geo_std',
        # Prior settings
        'prior_mean', 'prior_var',
        # Likelihood settings
        'lkl_var', 'lkl_cov', 'lkl_corr_len'
    ])

    def __init__(self, **kwargs):
        # Check all the input arguments have been provided
        if not ConstantFieldNoElectrodeEITDistribution.required_init_args <= set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(ConstantFieldNoElectrodeEITDistribution.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # List of attributes to be excluded in get/set params
        self.exclude_state_param_list = set()

        self.build_mesh_and_function_space()

        # Build the field to data maps
        self.field_to_data_maps_list = []
        for i in range(self.n_sens):
            self.field_to_data_maps_list.append(
                ConstantFieldNoElectrodeEITFieldToDataMap(
                    W=self.W, VE=self.VE, freq=i+1, r0=self.r0, h=self.h,
                    n_sens=self.n_sens, sens_geo_std=self.sens_geo_std ) )

        # Build prior (log-Gaussian)
        prior = DIST.PushForwardTransportMapDistribution(
            Maps.FrozenExponentialDiagonalTransportMap(1),
            DIST.GaussianDistribution(
                self.prior_mean * np.ones(1),
                self.prior_var * np.eye(1)) )

        # Build the slowness field if no-observation was provided
        self.field_vals = prior.rvs(1)
        self.set_true_field()

        # Generate the observations
        y_list = [ f.evaluate(self.field_vals)[0,:]
                   for f in self.field_to_data_maps_list ]

        # Generate sensor position list
        self.sens_angle_list = np.arange(self.n_sens) * 2 * np.pi / float(self.n_sens)
        self.sens_pos_list = [
            (self.r0 * np.cos(angle), self.r0 * np.sin(angle))
            for angle in self.sens_angle_list ]
        
        # Generate likelihoods
        if self.lkl_cov in ['ou','sqexp']:
            sens_dists = squareform( pdist(self.sens_pos_list) )
        lkl = LKL.IndependentLogLikelihood([])
        for y, f in zip(y_list, self.field_to_data_maps_list):
            # Create observational noise distribution (Gaussian)
            if self.lkl_cov == 'id':
                cov = id_cov(self.lkl_var, self.n_sens)
            if self.lkl_cov == 'ou':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            if self.lkl_cov == 'sqexp':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            pi_noise = DIST.GaussianDistribution(np.zeros(self.n_sens), cov)
            # If observations are synthetic, corrupt them with noise
            y += pi_noise.rvs(1)[0,:]
            # Build likelihood
            add_lkl = LKL.AdditiveLogLikelihood(y, pi_noise, f)
            # Add the factor to the general likelihood
            lkl.append( (add_lkl, range(1)) )

        # Call super to assemble posterior distribution
        super(ConstantFieldNoElectrodeEITDistribution, self).__init__(lkl, prior)

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)
        self.build_mesh_and_function_space()
        # Reset fields in fields to data map list
        for ftdm in self.field_to_data_maps_list:
            ftdm.W = self.W
            ftdm.VE = self.VE
            ftdm.set_up()
        self.set_true_field()

    def build_mesh_and_function_space(self):
        geometry = mshr.Circle(dol.Point(0, 0), self.r0)
        self.mesh = mshr.generate_mesh(geometry, self.ndiscr)
        self.VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
        self.VEFS = dol.FunctionSpace(self.mesh, self.VE)
        self.RE = dol.FiniteElement("R", dol.triangle, 0)
        self.REFS = dol.FunctionSpace(self.mesh, self.RE)
        self.VERE = dol.MixedElement([self.VE, self.RE])
        self.W = dol.FunctionSpace(self.mesh, self.VERE)
        self.exclude_state_param_list |= set([
            'mesh',
            'VE', 'VEFS', 'RE', 'REFS', 'VERE', 'W'])

    def set_true_field(self):
        self.true_field = dol.Constant(self.field_vals[0,0])
        self.exclude_state_param_list.add( 'true_field' )

class ConstantFieldNoElectrodeEITFieldToDataMap(Maps.Map, NoElectrodeEITSolver):

    required_init_args = set(['W', 'VE', 'freq', 'h', 'r0', 'n_sens', 'sens_geo_std'])

    def __init__(self, **kwargs):
        if not ConstantFieldNoElectrodeEITFieldToDataMap.required_init_args <= set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(ConstantFieldNoElectrodeEITFieldToDataMap.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # List of exluded parameters
        self.exclude_state_param_list = set(['W', 'VE'])
        # Set up experiment
        self.A = 2 * np.pi * self.h * self.r0 / self.n_sens
        self.set_up()
        # Init super
        super(ConstantFieldNoElectrodeEITFieldToDataMap, self).__init__(
            1, self.n_sens)

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)

    def set_up(self):
        self.current = dol.Expression(
            '0.5 * cos( k * atan2(x[1], x[0]) ) / A',
            k=self.freq, A=self.A, element=self.VE)
        self.sens_angle_list = np.arange(self.n_sens) * 2 * np.pi / float(self.n_sens)
        self.sensors_list = [
            dol.Expression(
                '1./sqrt(2.*pi*pow(std,2)) * ' + \
                'exp(-.5 * pow((atan2(c1, c0)-atan2(x[1], x[0]))*r0,2) / pow(std,2))',
                pi=np.pi, std=self.sens_geo_std, r0=self.r0,
                c0=self.r0*np.cos(angle), c1=self.r0*np.sin(angle),
                element=self.VE)
            for angle in self.sens_angle_list ]
        self.exclude_state_param_list |= set(['current', 'sensors_list'])

    @cached()
    @counted
    def evaluate(self, x, *args, **kwargs):
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        for i in range(m):
            sigma = dol.Constant(x[i,0])
            u = NoElectrodeEITSolver.solve(self.current, sigma, self.W)
            for j, sensor in enumerate(self.sensors_list):
                fx[i,j] = dol.assemble(sensor * u * dol.ds)
        return fx

    @cached_tuple(['evaluate', 'grad_x'])
    @counted
    def tuple_grad_x(self, x, *args, **kwargs):
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        gfx = np.zeros((m,self.dim_out,self.dim_in))
        for i in range(m):
            sigma = x[i,0]
            sigma_cnst = dol.Constant(sigma)
            u = NoElectrodeEITSolver.solve(self.current, sigma_cnst, self.W)
            for j, sensor in enumerate(self.sensors_list):
                fx[i,j] = dol.assemble(sensor * u * dol.ds)
                adj = NoElectrodeEITSolver.solve(sensor, sigma_cnst, self.W)
                grd = - dol.inner(dol.grad(adj), dol.grad(u))
                gfx[i,j,0] = dol.assemble( grd * dol.dx )
        return fx, gfx

class GaussianFieldNoElectrodeEITDistribution(DISTINF.BayesPosteriorDistribution):

    required_init_args = set([
        # Physics settings
        'r0', 'h', 'mesh_xml',
        'n_sens', 'sens_geo_std',
        'n_freq',
        'real_observations',
        # Generating field
        'field_vals', 'field_expr',
        # Prior settings
        'prior_mean_field_vals',
        'prior_mean', 'prior_var',
        'prior_cov', 'prior_corr_len',
        # Likelihood settings
        'lkl_var', 'lkl_cov', 'lkl_corr_len'
    ])

    def __init__(self, **kwargs):
        # Check all the input arguments have been provided
        if not GaussianFieldNoElectrodeEITDistribution.required_init_args <= \
           set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(GaussianFieldNoElectrodeEITDistribution.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # List of attributes to be excluded in get/set params
        self.exclude_state_param_list = set()

        self.build_mesh_and_function_space()

        # Build the field to data maps
        self.field_to_data_maps_list = []
        for i in range(self.n_freq):
            self.field_to_data_maps_list.append(
                GaussianFieldNoElectrodeEITFieldToDataMap(
                    W=self.W, VE=self.VE, VEFS=self.VEFS, ndofs=self.ndofs,
                    freq=i+1, r0=self.r0, h=self.h,
                    n_sens=self.n_sens, sens_geo_std=self.sens_geo_std ) )

        # Build prior (log-GP)
        if self.prior_mean_field_vals is not None:
            raise NotImplementedError()
        else:
            mean = self.prior_mean * np.ones(self.ndofs)
        if self.prior_cov in ['ou', 'sqexp']:
            dists = squareform( pdist(self.coord) )
        if self.prior_cov == 'id':
            cov = id_cov(self.prior_var, self.ndofs)
        if self.prior_cov == 'ou':
            cov = ou_cov(self.prior_var, dists, self.prior_corr_len)
        if self.prior_cov == 'sqexp':
            cov = sqexp_cov(self.prior_var, dists, self.prior_corr_len)
        prior = DIST.PushForwardTransportMapDistribution(
            Maps.FrozenExponentialDiagonalTransportMap(self.ndofs),
            DIST.GaussianDistribution(mean, cov) )

        # Build the slowness field if no-observation was provided
        if self.real_observations is None:
            if self.field_vals is not None:
                pass
            elif self.field_expr is not None:
                field_expr = FieldExpression(self.field_expr, element=self.VE)
                field_fun = dol.project(field_expr, self.VEFS)
                self.field_vals = field_fun.vector().get_local()[np.newaxis,:]
            else:
                self.field_vals = prior.rvs(1)
            self.set_true_field()

        # Generate the observations
        if self.real_observations is None:
            y_list = [ f.evaluate(self.field_vals)[0,:]
                       for f in self.field_to_data_maps_list ]
        else:
            y_list = self.real_observations

        # Generate sensor position list
        self.sens_angle_list = np.arange(self.n_sens) * 2 * np.pi / float(self.n_sens)
        self.sens_pos_list = [
            (self.r0 * np.cos(angle), self.r0 * np.sin(angle))
            for angle in self.sens_angle_list ]
            
        # Generate likelihoods
        if self.lkl_cov in ['ou','sqexp']:
            sens_dists = squareform( pdist(self.sens_pos_list) )
        lkl = LKL.IndependentLogLikelihood([])
        for y, f in zip(y_list, self.field_to_data_maps_list):
            # Create observational noise distribution (Gaussian)
            if self.lkl_cov == 'id':
                cov = id_cov(self.lkl_var, self.n_sens)
            if self.lkl_cov == 'ou':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            if self.lkl_cov == 'sqexp':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            pi_noise = DIST.GaussianDistribution(np.zeros(self.n_sens), cov)
            if self.real_observations is None:    
                # If observations are synthetic, corrupt them with noise
                y += pi_noise.rvs(1)[0,:]
            # Build likelihood
            add_lkl = LKL.AdditiveLogLikelihood(y, pi_noise, f)
            # Add the factor to the general likelihood
            lkl.append( (add_lkl, range(self.ndofs)) )

        # Call super to assemble posterior distribution
        super(GaussianFieldNoElectrodeEITDistribution, self).__init__(lkl, prior)

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)
        self.build_mesh_and_function_space()
        # Reset fields in fields to data map list
        for ftdm in self.field_to_data_maps_list:
            ftdm.W = self.W
            ftdm.VE = self.VE
            ftdm.VEFS = self.VEFS
            ftdm.set_up()
        if self.real_observations is None:
            self.set_true_field()

    def build_mesh_and_function_space(self):
        # geometry = mshr.Circle(dol.Point(0, 0), self.r0)
        # self.mesh = mshr.generate_mesh(geometry, self.ndiscr)

        wcomm = MPI.COMM_WORLD
        rank = wcomm.Get_rank()
        scomm = PETSc.Comm(MPI.COMM_SELF)
        
        fname = 'msh-%d.xml' % rank
        with open(fname,'w') as ostr:
            ostr.writelines( self.mesh_xml )
        self.mesh = dol.Mesh(scomm, fname)
        # self.mesh = dol.Mesh(fname)
        
        self.VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
        self.VEFS = dol.FunctionSpace(self.mesh, self.VE)
        self.RE = dol.FiniteElement("R", dol.triangle, 0)
        self.REFS = dol.FunctionSpace(self.mesh, self.RE)
        self.VERE = dol.MixedElement([self.VE, self.RE])
        self.W = dol.FunctionSpace(self.mesh, self.VERE)
        self.coord = self.VEFS.tabulate_dof_coordinates().reshape((-1,2))
        self.ndofs = self.coord.shape[0]
        self.exclude_state_param_list |= set([
            'mesh',
            'VE', 'VEFS', 'RE', 'REFS', 'VERE', 'W',
            'coord', 'ndofs'])
        # print("Number of degrees of freedom: %d" % self.ndofs)

    def set_true_field(self):
        self.true_field = NoElectrodeEITSolver.dof_to_fun(
            self.field_vals[0,:], self.VEFS)
        self.exclude_state_param_list.add( 'true_field' )

class GaussianFieldNoElectrodeEITFieldToDataMap(Maps.Map, NoElectrodeEITSolver):

    required_init_args = set([
        'W', 'VE', 'VEFS', 'ndofs', 'freq', 'h', 'r0', 'n_sens', 'sens_geo_std'])

    def __init__(self, **kwargs):
        if not GaussianFieldNoElectrodeEITFieldToDataMap.required_init_args <= set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(GaussianFieldNoElectrodeEITFieldToDataMap.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # List of exluded parameters
        self.exclude_state_param_list = set(['W', 'VE', 'VEFS'])
        # Set up experiment
        self.A = 2 * np.pi * self.h * self.r0 / self.n_sens
        self.set_up()
        # Init super
        super(GaussianFieldNoElectrodeEITFieldToDataMap, self).__init__(
            self.ndofs, self.n_sens)

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)

    def set_up(self):
        self.current = dol.Expression(
            '0.5 * cos( k * atan2(x[1], x[0]) ) / A',
            k=self.freq, A=self.A, element=self.VE)
        self.sens_angle_list = np.arange(self.n_sens) * 2 * np.pi / float(self.n_sens)
        self.sensors_list = [
            dol.Expression(
                '1./sqrt(2.*pi*pow(std,2)) * ' + \
                'exp(-.5 * pow((atan2(c1, c0)-atan2(x[1], x[0]))*r0,2) / pow(std,2))',
                pi=np.pi, std=self.sens_geo_std, r0=self.r0,
                c0=self.r0*np.cos(angle), c1=self.r0*np.sin(angle),
                element=self.VE)
            for angle in self.sens_angle_list ]
        self.exclude_state_param_list |= set(['current', 'sensors_list'])

    @cached([('solve', None)])
    @counted
    def evaluate(self, x, *args, **kwargs):
        # rank = MPI.COMM_WORLD.Get_rank()
        # print("rank %d: evaluate" % rank)
        x = np.asarray(x, order='C')
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        if 'cache' in kwargs and kwargs['cache'] is not None:
            sol_cache = get_sub_cache( kwargs['cache'], ('solve', None) )
            sol_cache['solve_list'] = []
            sol_cache = sol_cache['solve_list']
        for i in range(m):
            sigma = NoElectrodeEITSolver.dof_to_fun(x[i,:], self.VEFS)
            # Solve
            # print("rank %d: solving" % rank)
            u = NoElectrodeEITSolver.solve(self.current, sigma, self.W)
            if 'cache' in kwargs and kwargs['cache'] is not None:
                sol_cache.append( u.vector().get_local() )
            for j, sensor in enumerate(self.sensors_list):
                fx[i,j] = dol.assemble(sensor * u * dol.ds)
        return fx

    @cached([('solve',None),('adjoints',None)])
    @counted
    def grad_x(self, x, *args, **kwargs):
        x = np.asarray(x, order='C')
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        gfx = np.zeros((m,self.dim_out,self.dim_in))
        if 'cache' in kwargs and kwargs['cache'] is not None:
            sol_cache, adj_cache = get_sub_cache(
                kwargs['cache'], ('solve', None), ('adjoints',None) )
            sol_cache['solve_list'] = []
            adj_cache['adjoints_list'] = [[] for i in range(m)]
            sol_cache = sol_cache['solve_list']
            adj_cache = adj_cache['adjoints_list']
        for i in range(m):
            sigma = NoElectrodeEITSolver.dof_to_fun(x[i,:], self.VEFS)
            # Solve
            if 'cache' in kwargs and kwargs['cache'] is not None:
                u = NoElectrodeEITSolver.dof_to_fun(sol_cache[i], self.VEFS)
            else:
                u = NoElectrodeEITSolver.solve(self.current, sigma, self.W)
            v = dol.TestFunction(self.VEFS)
            for j, sensor in enumerate(self.sensors_list):
                # Gradient
                adj = NoElectrodeEITSolver.solve(sensor, sigma, self.W)
                if 'cache' in kwargs and kwargs['cache'] is not None:
                    adj_cache[i].append( adj.vector().get_local() )
                grd = - dol.inner(dol.grad(adj), dol.grad(u))
                gfx[i,j,:] = dol.assemble( grd * v * dol.dx )
        return gfx

    @cached_tuple(['evaluate', 'grad_x'],
                  [('solve',None),('adjoints',None)])
    @counted
    def tuple_grad_x(self, x, *args, **kwargs):
        x = np.asarray(x, order='C')
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        gfx = np.zeros((m,self.dim_out,self.dim_in))
        if 'cache' in kwargs and kwargs['cache'] is not None:
            sol_cache, adj_cache = get_sub_cache(
                kwargs['cache'], ('solve', None), ('adjoints',None) )
            sol_cache['solve_list'] = []
            adj_cache['adjoints_list'] = [[] for i in range(m)]
            sol_cache = sol_cache['solve_list']
            adj_cache = adj_cache['adjoints_list']
        for i in range(m):
            sigma = NoElectrodeEITSolver.dof_to_fun(x[i,:], self.VEFS)
            # Solve
            u = NoElectrodeEITSolver.solve(self.current, sigma, self.W)
            if 'cache' in kwargs and kwargs['cache'] is not None:
                sol_cache.append( u.vector().get_local() )
            v = dol.TestFunction(self.VEFS)
            for j, sensor in enumerate(self.sensors_list):
                fx[i,j] = dol.assemble(sensor * u * dol.ds)
                # Gradient
                adj = NoElectrodeEITSolver.solve(sensor, sigma, self.W)
                if 'cache' in kwargs and kwargs['cache'] is not None:
                    adj_cache[i].append( adj.vector().get_local() )
                grd = - dol.inner(dol.grad(adj), dol.grad(u))
                gfx[i,j,:] = dol.assemble( grd * v * dol.dx )
        return fx, gfx

    @cached(caching=False)
    @counted
    def action_hess_x(self, x, dx, *args, **kwargs):
        x = np.asarray(x, order='C')
        dx = np.asarray(dx, order='C')
        m = x.shape[0]
        ahx = np.zeros((m,self.dim_out,self.dim_in))
        if 'cache' in kwargs and kwargs['cache'] is not None:
            sol_cache, adj_cache = get_sub_cache(
                kwargs['cache'], ('solve', None), ('adjoints',None) )
            sol_cache = sol_cache['solve_list']
            adj_cache = adj_cache['adjoints_list']
        for i in range(m):
            sigma_fun = NoElectrodeEITSolver.dof_to_fun(x[i,:], self.VEFS)
            dx_fun = NoElectrodeEITSolver.dof_to_fun(dx[i,:], self.VEFS)
            # Solve
            if 'cache' in kwargs and kwargs['cache'] is not None:
                u = NoElectrodeEITSolver.dof_to_fun(sol_cache[i], self.VEFS)
            else:
                u = NoElectrodeEITSolver.solve(self.current, sigma_fun, self.W)
            v = dol.TestFunction(self.VEFS)
            for j, sensor in enumerate(self.sensors_list):
                if 'cache' in kwargs and kwargs['cache'] is not None:
                    adj = NoElectrodeEITSolver.dof_to_fun(adj_cache[i][j], self.VEFS)
                else:
                    adj = NoElectrodeEITSolver.solve(sensor, sigma_fun, self.W)
                A1Mu = NoElectrodeEITSolver.solve_action_hess_adjoint(
                    dx_fun, u, sigma_fun, self.W)
                A1Madj = NoElectrodeEITSolver.solve_action_hess_adjoint(
                    dx_fun, adj, sigma_fun, self.W)
                ahess = dol.inner( dol.grad(adj), dol.grad(A1Mu) ) + \
                        dol.inner( dol.grad(A1Madj), dol.grad(u) )
                ahx[i,j,:] = dol.assemble( ahess * v * dol.dx )
        return ahx

class FieldExpression(dol.Expression):
    def __init__(self, expr, element):
        self.expr = expr
        self._element = element
    def eval(self, value, x):
        self.expr.eval(value, x)
    def value_shape(self):
        return (1,)
        
class CirclePhantom(object):
    def __init__(self, c, r, v, b):
        self.c = c
        self.r = r
        self.v = v
        self.b = b
    def eval(self, value, x):
        if np.sqrt(np.sum((x-self.c)**2)) < self.r:
            value[0] = self.v
        else:
            value[0] = self.b

class DoubleCirclePhantom(object):
    def __init__(self, b, c1, r1, v1, c2, r2, v2, v12):
        self.b = b
        self.c1 = c1
        self.r1 = r1
        self.v1 = v1
        self.c2 = c2
        self.r2 = r2
        self.v2 = v2
        self.v12 = v12
    def eval(self, value, x):
        in1 = np.sqrt(np.sum((x-self.c1)**2)) < self.r1
        in2 = np.sqrt(np.sum((x-self.c2)**2)) < self.r2
        if in1 and in2:
            value[0] = self.v12
        elif in1:
            value[0] = self.v1
        elif in2:
            value[0] = self.v2
        else:
            value[0] = self.b