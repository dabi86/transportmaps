import dolfin as dol
import mshr

import matplotlib.pyplot as plt
import numpy as np

import subprocess
from GmshGeoGenerator import *

#
# ** This requires gmsh **
#
# In this setting the cylindrical bath is completely surrounded by
# electrodes all around it (not on the top and bottom)
# The model is described by
#
#   \nabla \cdot \sigma(x) \nabla U = 0                     for x\in B
#   \sigma(x) \frac{\partial U(x)}{\partial n} = g^k(x)     for x\in \partial B
#
# where the current pattern g^k(x) := j^k(\theta), for \theta = \arctan2(x_1,x_2),
# is given by
#
#   j^k(\theta) = \frac{5 \cos(k\theta_\ell)}{A}  for
#                    \theta_\ell-f\Delta/2 \leq \theta \theta_\ell+f\Delta/2
#   j^k(\theta) = 0    otherwise
#
# with
#
#   \Delta = 2 \pi / L
#   \theta_\ell = l \Delta ,   \ell=1,...,L
#   f = s / r_0 / \Delta       (fraction of circumference covered by electrodes)
#   A = f h r_0 \Delta         (area of one electrode)
#
# with 
#
#   s   = 2.84 cm    (arc-length of electrode)
#   h   = 4.24 cm    (depth of the bath)
#   r_0 = 15 cm      (radius of the bath)
#   L   = 32         (number of electrodes)

dol.parameters['allow_extrapolation'] = True

rho_list = [284.0, 139.7, 62.3, 29.5] # Bath resistivities
test_idx = 0  # Bath resistivity index

# Experimental data
rho_k_exp = np.array(
    [[312.0, 150.0, 96.0, 69.5, 54.0, 44.1, 37.3, 32.4,
      28.9, 26.2, 24.3, 22.8, 21.8, 21.0, 20.6, 20.5],
     [156.8, 75.0, 47.9, 34.6, 27.0, 22.0, 18.6, 16.2,
      14.4, 13.1, 12.2, 11.4, 10.9, 10.6, 10.3, 10.3],
     [70.7, 33.8, 21.7, 15.7, 12.2, 10.0, 8.5, 7.4,
      6.6, 6.0, 5.6, 5.2, 5.0, 4.9, 4.8, 4.7],
     [33.9, 16.3, 10.1, 7.6, 5.9, 4.8, 4.1, 3.6,
      3.2, 2.9, 2.7, 2.6, 2.5, 2.4, 2.3, 2.3]])

rho = rho_list[test_idx]

PLOTTING = False
k_list = range(1,17)  # Frequencies k=1,...,16

# Parameters
s  = 2.84
h  = 4.24
r0 = 15.
L  = 32

Delta = 2 * np.pi / float(L)
f = s / r0 / Delta
A = f * h * r0 * Delta

# Mesh size multiplier
mesh_scale = 1.

# Current pattern
class Current(dol.Expression):
    def __init__(self, k, L, Delta, f, A, element):
        self.k = k
        self.L = L
        self.dlt = Delta
        self.f = f
        self.A = A
        self._element = element
    def eval(self, values, x):
        theta = np.arctan2(x[1], x[0])
        l = round(theta * self.L / 2. / np.pi)
        tl = l * 2. * np.pi / self.L
        if np.abs(theta-tl) < self.f * self.dlt / 2.: # + dol.DOLFIN_EPS:
            values[0] = 5. * np.cos(self.k * tl) / self.A
        else:
            values[0] = 0.
    def value_shape(self):
        return (1,)

# Conductivity field \sigma
class ConductivityField(dol.Expression):
    def __init__(self, rho, element):
        self.rho = rho
        self._element = element
    def eval(self, value, x):
        value[0] = 1./rho
    def value_shape(self):
        return (1,)

def solve(g, sigma, mesh, VE):

    # Finite element spaces
    RE = dol.FiniteElement("R", dol.triangle, 0)
    VERE = dol.MixedElement([VE, RE])
    W = dol.FunctionSpace(mesh, VERE)

    # Trial an test functions
    (u, c) = dol.TrialFunction(W)
    (v, d) = dol.TestFunction(W)

    # Forms
    LagMult = (c*v + u*d) * dol.ds
    lhs = dol.inner(dol.nabla_grad(u), sigma * dol.nabla_grad(v)) * dol.dx + LagMult
    rhs = g * v * dol.ds

    # Solve
    w = dol.Function(W)
    dol.solve(lhs == rhs, w)
    (U, C) = w.split()

    return U

# Geometry
fname = "DiscretizedCEM"
generate_geo(mesh_scale, s, h, r0, L, fname + ".geo")
subprocess.check_call("gmsh -2 " + fname + ".geo", shell=True)
subprocess.check_call("dolfin-convert " + fname + ".msh " + fname + ".xml", shell=True)
mesh = dol.Mesh(fname + ".xml")

# geometry = mshr.Circle(dol.Point(0, 0), r0)
# mesh = mshr.generate_mesh(geometry, nel)



surface = dol.BoundaryMesh(mesh, "exterior")

# Finite element space
VE = dol.FiniteElement("Lagrange", dol.triangle, 1)

N = 10000 # Interpolation points
krho = np.zeros(len(k_list))
for i, k in enumerate(k_list):  # Current frequencies k=1,...,16
    g = Current(k, L, Delta, f, A, element=VE)
    sigma = ConductivityField(rho, element=VE)
    
    U = solve(g, sigma, mesh, VE)

    if PLOTTING:
        plt.figure()
        dol.plot(U)
        plt.show(False)

    # Extract boundary current
    WSI = dol.FunctionSpace(surface, "DP", 0)
    I = dol.Function(WSI)
    I.interpolate(g)

    # Extract boundary voltage (i.e. electrostatic potential)
    WSV = dol.FunctionSpace(surface, "P", 1)
    V = dol.Function(WSV)
    V.interpolate(U)

    # Iterate over angle to get currents and voltages
    thetas = np.linspace(
        1e-7, 2*np.pi-1e-7, N)
    # thetas = np.zeros(N*L)
    # thetas[:N//2] = np.linspace(0.001, f*Delta/2., N//2)
    # thetas[-(N//2):] = np.linspace(2*np.pi-f*Delta/2., 2*np.pi-0.001, N//2)
    # for l in range(1,L):
    #     tl = l * 2 * np.pi / float(L)
    #     thetas[N//2+(l-1)*N:N//2+l*N] = np.linspace(tl-f*Delta/2., tl+f*Delta/2., N)
    crt = np.zeros(N)
    vlt = np.zeros(N)
    pnts = np.vstack( (r0*np.cos(thetas), r0*np.sin(thetas)) ).T
    for j,t in enumerate(thetas):
        crt[j] = I(pnts[j,:])
        vlt[j] = V(pnts[j,:])

    if PLOTTING:
        fig = plt.figure()
        axI = fig.add_subplot(111)
        axV = axI.twinx()
        axI.plot(thetas, crt, '.k', label='Current', markersize=1)
        axV.plot(thetas, vlt, '-r', label='Voltage')
        plt.show(False)

    mvlt = np.max(vlt)
    mcrt = np.max(crt)
    krho[i] = mvlt/mcrt/A * k
    char_res = mvlt/mcrt/A
    print("Characteristic resistance (rho): %.2f" % (char_res))
    print("k*rho: %.2f" % (char_res * k))

krho_exp = np.array(k_list) * rho_k_exp[test_idx,[i-1 for i in k_list]]
plt.figure()
plt.plot(k_list, krho, '-ok')
plt.plot(k_list, krho_exp, 'xr')
plt.ylabel(r"$k \rho_k$")
plt.show(False)

plt.figure()
plt.plot(k_list, np.abs(krho - krho_exp), '-ok')
plt.ylabel(r"$\vert k \rho_k - k \rho_k^{\rm exp}\vert$")
plt.show(False)