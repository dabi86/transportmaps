import subprocess
import matplotlib.pyplot as plt
import numpy as np
from  fenics  import *
from GmshGeoGenerator import generate_geo

parameters['allow_extrapolation'] = True

rho = 284.0 # Bath resistivities

PLOTTING = True
k = 16  # Frequencies k=1,...,16

# Parameters
h  = 4.24
r0 = 15.
L  = 32

A = 2 * np.pi * h * r0 / L

# Sensor std
std = 0.2

# Mesh size multiplier
mesh_size = 0.2

# Geometry
fname = "circle"
generate_geo(mesh_size, r0, L, fname + ".geo")
subprocess.check_call("gmsh -2 " + fname + ".geo", shell=True)
subprocess.check_call("dolfin-convert " + fname + ".msh " + fname + ".xml", shell=True)
mesh = Mesh(fname + ".xml")
surface = BoundaryMesh(mesh, "exterior")

# Voltage pattern
class Voltage(Expression):
    def __init__(self, k, A, element):
        self.k = k
        self.A = A
        self._element = element
    def eval(self, values, x):
        theta = np.arctan2(x[1], x[0])
        values[0] = 5. * np.cos(self.k * theta) / self.A
    def value_shape(self):
        return (1,)

# Conductivity field \sigma
conductivity_field_str = '1./rho'

# Pointwise sensor
# boundary_sensor_str = '2./(2.*pi*pow(std,2)) * ' + \
#                       'exp(-.5 * (pow(c0-x[0],2) + pow(c1-x[1],2)) / pow(std,2))'
boundary_sensor_str = '1./sqrt(2.*pi)/std * ' + \
                      'exp(-.5 * pow(' + \
                      'pi - fabs(fabs(atan2(c1,c0)-atan2(x[1],x[0]))-pi)' + \
                      ',2) / pow(std,2))'
# boundary_sensor_str = 'isless(fabs(atan2(c1,c0)-atan2(x[1],x[0])),1e-10)'

# Define  mesh  and  finite  element  space
E = FiniteElement("Lagrange", triangle, 1)
FS = FunctionSpace(mesh , E)

voltage_expr = Voltage(k, A, element=E)
sigma_expr = Expression(conductivity_field_str, rho=rho, element=E, name='sigma')
sigma_fun = project(sigma_expr, FS)#, annotate=False)

# Define  basis  functions  and  parameters
u = TrialFunction(FS)
v = TestFunction(FS)

# Define  variational  problem
lhs = sigma_fun*inner(grad(u), grad(v))*dx
rhs = Constant(0.) * v * dx
bc = DirichletBC(FS, voltage_expr, "on_boundary")

# Solve  variational  problem
u = Function(FS)
solve(lhs == rhs, u, bc)
plt.figure()
plot(u, title="u")
plt.show(False)

# # Extract boundary current
# WS = FunctionSpace(surface, "Lagrange", 1)
# I = Function(WS)
# n = FacetNormal(surface)
# I.interpolate( sigma_expr * dot( grad(u), n ) )

# # Extract boundary voltage (i.e. electrostatic potential)
# V = Function(WS)
# V.interpolate(voltage_expr)

# # Iterate over angle to get currents and voltages
# N = 1000
# thetas = np.linspace(0, 2*np.pi, N)
# crt = np.zeros(N)
# vlt = np.zeros(N)
# for j,t in enumerate(thetas):
#     pnt = [r0*np.cos(t), r0*np.sin(t)]
#     crt[j] = I(pnt)
#     vlt[j] = V(pnt)

# # # Get sensor measuraments
# # vlt_sens = [ assemble(s * u * dx)
# #              for s in sensors_expr_list ]

# fig = plt.figure()
# axI = fig.add_subplot(111)
# axV = axI.twinx()
# axI.plot(thetas, crt, '-k', label='Current')
# axV.plot(thetas, vlt, '-r', label='Voltage')
# # axV.plot(tls, vlt_sens, 'or')
# plt.show(False)

# rho_approx = np.mean(vlt/crt/A)
# krho = rho_approx * k
# print("Characteristic resistance (rho): %.2f" % rho_approx)
# print("k*rho: %.2f" % (krho))

# Sensors
tls = np.arange(1,L+1) * 2 * np.pi / float(L)
pnts = np.vstack( (r0 * np.cos(tls), r0 * np.sin(tls)) ).T
sensors_expr_list = [
    Expression(boundary_sensor_str,
               c0=pnts[i,0], c1=pnts[i,1], std=std, pi=np.pi,
               degree=2)
    for i in range(L) ]
# sensors_expr_list = [
#     Expression(boundary_sensor_str,
#                c0=pnts[i,0], c1=pnts[i,1],
#                degree=2)
#     for i in range(L) ]

# Measurement functionals
n = FacetNormal(mesh)
# # Jform_list = [ inner(s, sigma_expr * dot(grad(u), n)) * ds
# #                for s in sensors_expr_list ]
# Jform_list = [ inner(s, dot(grad(u), n)) * ds
#                for s in sensors_expr_list ]
# J_list = [ Functional( Jform )
#            for Jform in Jform_list ]

# sigmac = Control(sigma_fun)

# dJ_dsigma_list = [ compute_gradient(J, sigmac, project=True, forget=False)
#                    for J in J_list ]

# for dJ_dsigma in dJ_dsigma_list:
#     plt.figure()
#     plot(dJ_dsigma)
#     plt.show(False)

#########################
# Observational currents
# crt_obs = [ assemble(Jform) for Jform in Jform_list ]

#################
# Current
VFS = VectorFunctionSpace(mesh, "Lagrange", 1)
SURF_FS = FunctionSpace(surface, "Lagrange", 1)
SURF_VFS = VectorFunctionSpace(surface, "Lagrange", 1)
# Normal component on surface
n_expr = Expression(('x[0]/r0','x[1]/r0'), r0=r0, element=SURF_VFS.ufl_element())
n_fun = Function(SURF_VFS)
n_fun.interpolate(n_expr)
plt.figure()
plot(n_fun)
plt.show(False)
# Gradient on surface
gu = project( nabla_grad(u), VFS )
surf_gu = Function(SURF_VFS)
surf_gu.interpolate(gu)
plt.figure()
plot(surf_gu)
plt.show(False)
# Sigma on surface
surf_sigma_fun = Function(SURF_FS)
surf_sigma_fun.interpolate(sigma_expr)
# # Current \sigma \frac{\partial u}{\partial n}
# srf_crt = project( surf_sigma_fun * inner(surf_gu, n_fun), SURF_FS )
# Current \frac{\partial u}{\partial n}
srf_crt = project( inner(surf_gu, n_fun), SURF_FS )
# Extract current
N = 1000
thetas = np.linspace(0, 2*np.pi, N)
crt = np.zeros(N)
for j,t in enumerate(thetas):
    pnt = [r0*np.cos(t), r0*np.sin(t)]
    crt[j] = srf_crt(pnt)


plt.figure()
plt.plot(tls, crt_obs, 'or')
plt.plot(thetas, crt, 'r')
plt.show(False)