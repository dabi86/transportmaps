import numpy as np

def generate_geo(mesh_size=0.5, r0=15., L=32, fname='circle.geo'):

    Delta = 2 * np.pi / float(L)

    tls = np.arange(1,L+1) * 2 * np.pi / float(L)
    npts = len(tls)

    # center points of electrodes
    pnts = np.vstack( (r0 * np.cos(tls), r0 * np.sin(tls)) ).T

    mesh_size_center = 5*mesh_size
    mesh_size_boundary = mesh_size

    # Writing .geo file
    with open(fname,'w') as geo:
        # Writing points
        geo.write("Point(0) = {0,0,0,%.10f};\n" % (mesh_size_center))
        for i in range(npts):
            geo.write("Point(%d) = " % (i+1) + \
                      "{%.10f,%.10f,0,%.10f};\n" % (
                          pnts[i,0], pnts[i,1],mesh_size_boundary))
        # Defining arcs
        for i in range(npts-1):
            geo.write(
                "Circle(%d) = {%d, %d, %d};\n" % (i, i+1, 0, i+2))
        geo.write("Circle(%d) = {%d, %d, %d};\n" % (npts-1, npts, 0, 1))
        # Define line loop (circle)
        geo.write("Line Loop(%d) = {" % npts + \
                ",".join([str(i) for i in range(npts)]) + "};\n")
        # Define surface
        geo.write("Plane Surface(0) = {%d};\n" % npts)

        # Define physical points
        geo.write("Physical Point(\"electrodes_bounds\") = {" + \
                ",".join([str(i) for i in range(1,npts+1)]) + "};\n")
        # Physical lines
        geo.write("Physical Line(\"electrodes\") = {" + \
                ",".join([str(i) for i in range(0,npts,2)]) + "};\n")
        geo.write("Physical Line(\"gaps\") = {" + \
                ",".join([str(i) for i in range(1,npts,2)]) + "};\n")
        # Physical surface
        geo.write("Physical Surface(\"disk\") = {0};\n")
