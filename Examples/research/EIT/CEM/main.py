import dolfin as dol

import matplotlib.pyplot as plt
import numpy as np

import subprocess
from GmshGeoGenerator import generate_geo

dol.parameters['allow_extrapolation'] = True

rho_list = [284.0, 139.7, 62.3, 29.5] # Bath resistivities
zl_list = [58.0, 30.5, 15.0, 7.5] # Contact impedances
test_idx = 0  # Bath resistivity index

# Experimental data
rho_k_exp = np.array(
    [[312.0, 150.0, 96.0, 69.5, 54.0, 44.1, 37.3, 32.4,
      28.9, 26.2, 24.3, 22.8, 21.8, 21.0, 20.6, 20.5],
     [156.8, 75.0, 47.9, 34.6, 27.0, 22.0, 18.6, 16.2,
      14.4, 13.1, 12.2, 11.4, 10.9, 10.6, 10.3, 10.3],
     [70.7, 33.8, 21.7, 15.7, 12.2, 10.0, 8.5, 7.4,
      6.6, 6.0, 5.6, 5.2, 5.0, 4.9, 4.8, 4.7],
     [33.9, 16.3, 10.1, 7.6, 5.9, 4.8, 4.1, 3.6,
      3.2, 2.9, 2.7, 2.6, 2.5, 2.4, 2.3, 2.3]])

rho = rho_list[test_idx]
zl = zl_list[test_idx]

PLOTTING = False
k_list = range(1,17)  # Frequencies k=1,...,16
k = [1]

# Parameters
s  = 2.84
h  = 4.24
r0 = 15.
L  = 32

Delta = 2 * np.pi / float(L)
f = s / r0 / Delta
A = f * h * r0 * Delta

# Mesh size multiplier
mesh_scale = 1.

# Geometry
fname = "DiscretizedCEM"
# generate_geo(mesh_scale, s, h, r0, L, fname + ".geo")
# subprocess.check_call("gmsh -2 " + fname + ".geo", shell=True)
# subprocess.check_call("dolfin-convert " + fname + ".msh " + fname + ".xml", shell=True)
mesh = dol.Mesh(fname + ".xml")
surface = dol.BoundaryMesh(mesh, "exterior")

VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
VEFS = dol.FunctionSpace(mesh, VE)
REIMP = [ dol.FiniteElement("R", dol.triangle, 0)
           for i in range(L) ]
REIMPFS = [ dol.FunctionSpace(mesh, E) for E in REIMP ]
RE = dol.FiniteElement("R", dol.triangle, 0)
VE_REIMP_RE = dol.MixedElement([VE] + REIMP + [RE])
W = dol.FunctionSpace(mesh, VE_REIMP_RE)

conductivity_field_str = '1./rho'
sigma_expr = dol.Expression(conductivity_field_str, rho=rho, element=VE, name='sigma')
sigma_fun = dol.project(sigma_expr, VEFS)

# Split boundary condtions into each electrodes and everything else
class BoundaryNoElectrode(dol.SubDomain):
    def inside(self, x, on_boundary):
        # get angle of x
        theta = np.arctan2(x[1], x[0])
        # get angle of closest center of electrode
        l = int(np.round(theta / Delta))
        theta_center = l * np.pi / L
        # x is in between electrodes if distance from center is more than f*Delta/2
        theta_dist = np.pi - np.abs( np.abs(theta-theta_center) - np.pi)
        return on_boundary and theta_dist >= f*Delta/2

class BoundaryElectrode(dol.SubDomain):
    def __init__(self, n):
        self.n = n
        super(BoundaryElectrode, self).__init__()
    def inside(self, x, on_boundary):
        # get angle of x
        theta = np.arctan2(x[1], x[0])
        # get angle of closest center of electrode
        l = int(np.round(theta / Delta))
        if l != n:
            return False
        theta_center = l * np.pi / L
        # x is in electrode n if distance from center is less than f*Delta/2
        theta_dist = np.pi - np.abs( np.abs(theta-theta_center) - np.pi)
        return on_boundary and theta_dist < f*Delta/2
        
boundary_markers = dol.FacetFunction('size_t', mesh)
BoundaryNoElectrode().mark( boundary_markers, 0 )
for n in range(L):
    BoundaryElectrode(n).mark( boundary_markers, n+1)
ds = dol.Measure('ds', domain=mesh, subdomain_data=boundary_markers)


krho = np.zeros(len(k_list))
for i, k in enumerate(k_list):
    # Current pattern
    l_list = np.arange(1,L+1)
    tl_list = l_list * 2 * np.pi / L
    Il_list = 5 * np.cos( k * tl_list )
    Il_fun_list = [ dol.project(dol.Constant(Il), FS)
                    for Il, FS in zip(Il_list, REIMPFS) ]

    # Trial and test functions
    vprime = dol.TrialFunction(W)
    wprime = dol.TestFunctions(W)

    v = vprime[0]
    Vl_list = [ vprime[i] for i in range(1,L+1) ]
    c = vprime[-1]
    w = wprime[0]
    Wl_list = [ wprime[i] for i in range(1,L+1) ]
    d = wprime[-1]

    # Lagrange multiplier and forms
    invImp = dol.Constant(1./zl)
    LagMult_imp_list = [ invImp * (v - Vl_list[i-1]) * (w - Wl_list[i-1]) * ds(i)
                         for i in range(1,L+1) ]
    LagMult_gv = (c*w + v*d) * dol.ds  # ground voltage
    lhs =  dol.inner(sigma_fun * dol.nabla_grad(v), dol.nabla_grad(w)) * dol.dx \
           + sum(LagMult_imp_list) + LagMult_gv
    rhs = sum( [ Il * Wl for Il,Wl in zip(Il_fun_list, Wl_list) ] )

    # Solve
    vprime = dol.Function(W)
    dol.solve(lhs == rhs, vprime)
    v = vprime.split()[0]

    plt.figure()
    dol.plot(v)
    plt.show(False)
    
# ### OLD ###
# V = dol.FunctionSpace(mesh, "Lagrange", 2)

# E = dol.TrialFunction(V)
# v = dol.TestFunction(V)

# class ConductivityField(dol.Expression):
#     def eval(self, value, x):
#         if -0.5 < x[0] < 0.5  and -0.5 < x[1] < 0.5:
#             value[0] = 1.
#         else:
#             value[0] = 10.
#     def value_shape(self):
#         return (1,)
# sigma = ConductivityField(element=V.ufl_element())

# # Neumann boundary conditions
# class ElectrodesBoundaryConditions(dol.Expression):
#     def eval(self, value, x):
#         l = int(np.round(np.arctan2(x[1], x[0]) / dTheta))
#         if l % 2 == 0:
#             value[0] = Il[l//2]
#         else:
#             value[0] = 0.
#     def value_shape(self):
#         return (1,)
# g = ElectrodesBoundaryConditions(element=V.ufl_element())

# # Poisson Equation
# A = dol.inner(
#     dol.nabla_grad(E), sigma * dol.nabla_grad(v)) * dol.dx
# L = g * v * dol.ds

# # # Assemble the system
# # print("Assembly: ...")
# # A, rhs = dol.assemble_system(A, L)
# # print("Assembly: DONE")

# # # Solve system
# # E = dol.Function(V)
# # E_vec = E.vector()
# # print("Solve: ...")
# # dol.solve(A, E_vec, rhs)
# # print("Solve: DONE")

# for i in range(NE):
#     I = 0.1 * np.array([0] * i + [1., -1.] + [0., 0.] * (NE-i-2))
#     Il = I / dTheta * 1. # Arc on the unit circle
    
#     # Solve system
#     E = dol.Function(V)
#     print("Solve: ...")
#     dol.solve(A == L, E)
#     print("Solve: DONE")

#     plt.figure()
#     dol.plot(E, interactive=True)
#     plt.title("Solution")
#     plt.show(False)

# print("Storage: ...")
# file = dol.File("data/EIT-0.pvd")
# file << E
# print("Storage: DONE")

# #### COMPUTE NORMAL GRADIENT ####
# vector_SV = dol.VectorFunctionSpace(surface, "Lagrange", 2)

# plt.figure()
# # Normal components on surface
# n_expr = dol.Expression(('x[0]','x[1]'), element=vector_SV.ufl_element())
# surf_n = dol.Function(vector_SV)
# surf_n.interpolate(n_expr)
# dol.plot(surf_n)

# # Gradient on surface
# vector_V = dol.VectorFunctionSpace(mesh, "Lagrange", 2)
# grad_E = dol.project( dol.nabla_grad(E), vector_V )
# surf_grad_E = dol.Function(vector_SV)
# surf_grad_E.interpolate(grad_E)
# dol.plot(surf_grad_E)
# plt.show(False)

# # Normal gradient at center of electrodes
# inner = dol.inner( surf_grad_E, surf_n )
# V = np.zeros(NE)
# theta = 2*np.pi / NE * np.arange(NE)
# for i in range(NE):
#     x = [  np.cos(theta[i]), np.sin(theta[i]) ]
#     V[i] = np.dot( surf_grad_E(x), x )
# plt.figure()
# plt.plot(theta, V)
# plt.show(False)



# # scalar_SV = dol.FunctionSpace(surface, "Lagrange", 2)
# # norm_grad_E = dol.Function(scalar_SV)
# # norm_grad_E.interpolate( dol.dot( surf_grad_E, surf_n ) )

# # trial_G = dol.TrialFunction(scalar_SV)
# # test_G = dol.TestFunction(scalar_SV)
# # A = trial_G * test_G * dol.ds
# # L =  * test_G * dol.ds

# # A, rhs = dol.assemble_system(A, L)



# # G = dol.Function(V)

# # dol.solve(A, G.vector(), rhs)