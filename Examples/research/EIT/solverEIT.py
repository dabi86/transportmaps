import dolfin as dol
import mshr

import matplotlib.pyplot as plt
import numpy as np

dol.parameters['allow_extrapolation'] = True

geometry = mshr.Circle(dol.Point(0, 0), 1)
mesh = mshr.generate_mesh(geometry, 128)
surface = dol.BoundaryMesh(mesh, "exterior")
rtilde = 0.8
C = 5.
N = 3

VE = dol.FiniteElement("Lagrange", dol.triangle, 2)
RE = dol.FiniteElement("R", dol.triangle, 0)
VERE = dol.MixedElement([VE, RE])
W = dol.FunctionSpace(mesh, VERE)

# V = dol.FunctionSpace(mesh, "Lagrange", 2)
# R = dol.FunctionSpace(mesh, "R", 0)
# W = V * R

class ConductivityField(dol.Expression):
    def eval(self, value, x):
        if np.sqrt(x[0]**2 + x[1]**2) < rtilde:
            value[0] = 1. + C
        else:
            value[0] = 1.
    def value_shape(self):
        return (1,)
sigma = ConductivityField(element=VE)

class Current(dol.Expression):
    def eval(self, values, x):
        theta = np.arctan2(x[1], x[0])
        values[0] = np.cos(N * theta)
    def value_shape(self):
        return (1,)
g = Current(element=VE)

# Trial and test functions
(u, c) = dol.TrialFunction(W)
(v, d) = dol.TestFunctions(W)

# Forms and Lagrange multiplier
LagMult = (c*v + u*d) * dol.ds
B = dol.inner(dol.nabla_grad(u), sigma * dol.nabla_grad(v)) * dol.dx + LagMult
L = g * v * dol.ds

# Solve
w = dol.Function(W)
dol.solve(B == L, w)
(u, c) = w.split()

plt.figure()
dol.plot(u)
plt.show(False)

