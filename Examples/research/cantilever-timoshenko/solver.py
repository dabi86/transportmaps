import time
import dolfin as dol
import dolfin_adjoint as doladj
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr

from TransportMaps.Distributions.Examples.Timoshenko import \
    TimoshenkoSolver, AdjointTimoshenkoSolver, ClampedTimoshenkoProblem

npr.seed(0)

# Define parameters (in meters)
thickness = 0.03
width = 0.01
length = 10.
kappa = 5./6.

# Discretization parameters
N = 100

# Define material properties (~steel) in GPa
young = 200e9
shear = young/2

# Define the load (kg) point end
mass = 1.
load = mass * 9.81

# Instantiate the solver
solver = ClampedTimoshenkoProblem(
    # Physical settings
    q=load,# *np.ones(N+1)
    thickness=thickness, width=width,
    length=length, kappa=kappa,
    # Discretization settings
    nels=N
)

# Solve test case
E = young*np.ones(N+1)
G = shear*np.ones(N+1)
u = solver.solve(E, G)

# Test solver against analytic solution
I = width * thickness**3. / 12.
A = width * thickness
def displ(x):
    return load * x / (kappa * A * shear) \
        - load * (length - x) / (2 * young * I) * (length**2 - (length - x)**2/3) \
        + load * length**3 / (3 * young * I)
# displ = load * length**4 / 8 / young / I
dd = displ(length)
print("Mismatch: %e" % (np.abs(dd - u([length]))/np.abs(dd)))

# Speed
NSAMPS = 100
start = time.process_time()
for i in range(NSAMPS):
    u = solver.solve(E, G)
stop = time.process_time()
print("Solve: %.2f f/s" % (NSAMPS/(stop-start)))

# Define sensors
sens_pos_list = np.linspace(1, 10, 10)
sens_eps = 1e-3
nrm_list = [
    dol.assemble(
        doladj.Expression(
            'exp(-.5 * pow(p-x[0],2) / pow(eps,2))',
            pi=np.pi, p=p, eps=sens_eps,
            element=solver.VEFS.sub(0).ufl_element()
        ) * dol.project(dol.Constant(1.), solver.UEFS) * dol.dx
    )
    for p in sens_pos_list
]
sens_expr = '1./nrm * exp(-.5 * pow(p-x[0],2) / pow(eps,2))'
sensors_list = [
    doladj.Expression(
        sens_expr, nrm=nrm, pi=np.pi, p=p, eps=sens_eps,
        element=solver.VEFS.sub(0).ufl_element()
    )
    for p, nrm in zip(sens_pos_list,nrm_list)
]

pointwise_obs = [ u([p]) for p in sens_pos_list ]

solver.set_data(pointwise_obs, sens_pos_list, sens_eps)

# Test sensors
J = solver.evaluate(E, G)

# Test correctness adjoint
Et = solver.dof_to_fun(E/2)
Gt = solver.dof_to_fun(G/2)
dE = solver.dof_to_fun(E/2 * npr.randn(N+1))
dG = solver.dof_to_fun(G/2 * npr.randn(N+1))
conv_rate = doladj.taylor_test(
    solver.Jhat, [Et, Gt], [dE, dG] 
)

# Test speed of adjoint
NSAMPS = 100
start = time.process_time()
for i in range(NSAMPS):
    # J = solver.evaluate(young*np.ones(N+1), shear*np.ones(N+1))
    J, dJdE, dJdG = solver.tuple_grad_x(E, G)
stop = time.process_time()
print("Adjoint: %.2f f/s" % (NSAMPS/(stop-start)))
