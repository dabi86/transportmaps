#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import time
import sys, getopt
import dill
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import dolfin as dol
import itertools

import TransportMaps as TM
import TransportMaps.FiniteDifference as FD
import src.TransportMaps.Maps as MAPS

import TransportMaps.Distributions.Examples.Timoshenko as TIMO

def usage():
    print("""python GaussianFieldDataGeneration.py --output=<filename> [ 
    --ndiscr=100
    --nsens=10
    --sens-geo-eps=1e-4
    --field=<field>
    --piecewise=INT
    --young-prior-mean=200 --young-prior-std=5
    --young-prior-cov=ou --young-prior-corr-len=1
    --lkl-std=1e-3""")
    print(
"""Available covariances:
 - id      Gaussian noise (identity)
 - ou      Ornstein-Uhlenbeck (exponential)
 - sqexp   Squared exponential""")
    print(
        "Available fields: \n" + \
        " - prior           Generate from prior [default] \n" + \
        " - four-steels-10  Setting with four different kinds of steel (10 pieces)\n" + \
        " - four-steels-4   Setting with four different kinds of steel (4 pieces)\n" + \
        " - five-steels-5   Setting with five different kinds of steel (5 pieces)"
    )

stg = TM.DataStorageObject()

argv = sys.argv[1:]
OUT_FNAME = None
# Physics settings
stg.NDISCR = 200
stg.SENS_POS_LIST = [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.]
stg.SENS_GEO_EPS = 1e-4
stg.FIELD = 'prior'
# Prior settings
stg.PIECEWISE            = None # Split bar in N elements
stg.YOUNG_PRIOR_MEAN     = 200  # ~[190,210] GPa
stg.YOUNG_PRIOR_STD      = 5    # 2.5%
stg.YOUNG_PRIOR_COV      = 'sqexp'
stg.YOUNG_PRIOR_CORR_LEN = .1
# Likelihood settings
stg.LKL_STD = 1e-3
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "output=",
            # Physics settings
            "ndiscr=",
            "nsens=", "sens-geo-eps=",
            "field=",
            # Prior settings
            "piecewise=",
            "young-prior-mean=", "young-prior-std=", "young-prior-cov=", "young-prior-corr-len=",
            # Likelihood settings
            "lkl-std=", 
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--output':
        OUT_FNAME = arg

    # Physics settings
    elif opt == '--ndiscr':
        stg.NDISCR = int(arg)
    elif opt == '--nsens':
        stg.SENS_POS_LIST = list(np.linspace(0, 10, int(arg)+1)[1:])
        # stg.SENS_POS_LIST = [float(s) for s in arg.split(',')]
    elif opt == '--sens-geo-eps':
        stg.SENS_GEO_EPS = float(arg)
    elif opt == '--field':
        stg.FIELD = arg

    # Prior settings
    elif opt == '--piecewise':
        stg.PIECEWISE = int(arg)
    elif opt == '--young-prior-mean':
        stg.YOUNG_PRIOR_MEAN = float(arg)
    elif opt == '--young-prior-std':
        stg.YOUNG_PRIOR_STD = float(arg)
    elif opt == '--young-prior-cov':
        stg.YOUNG_PRIOR_COV = arg
        if stg.YOUNG_PRIOR_COV not in ['sqexp', 'ou', 'id']:
            raise NotImplementedError("The selected covariance is not implemented")
    elif opt == '--young-prior-corr-len':
        stg.YOUNG_PRIOR_CORR_LEN = float(arg)

    # Likelihood settings
    elif opt == '--lkl-std':
        stg.LKL_STD = float(arg)

    else:
        raise AttributeError("Unknown option %s" % opt)
if None in [OUT_FNAME]:
    usage()
    sys.exit(2)

# Set up solver
thickness = 0.03
width = 0.01
length = 10.
kappa = 5./6.
poisson = 0.28
# Define the load (kg) point end
mass = 5.
load = - mass * 9.81

solver = TIMO.ClampedTimoshenkoProblem(
    q = load,
    thickness = thickness,
    width = width,
    length = length,
    kappa = kappa,
    poisson=poisson,
    nels = stg.NDISCR
)

if stg.PIECEWISE is not None:
    dim = stg.PIECEWISE
else:
    dim = solver.aux_ndofs

# Set up prior
young_prior_mean_vals = stg.YOUNG_PRIOR_MEAN * np.ones(dim)
if stg.YOUNG_PRIOR_COV == 'ou':
    young_prior_cov = TIMO.OrnsteinUhlenbeckCovariance(
        stg.YOUNG_PRIOR_STD**2, stg.YOUNG_PRIOR_CORR_LEN)
elif stg.YOUNG_PRIOR_COV == 'sqexp':
    young_prior_cov = TIMO.SquaredExponentialCovariance(
        stg.YOUNG_PRIOR_STD**2, stg.YOUNG_PRIOR_CORR_LEN)
elif stg.YOUNG_PRIOR_COV == 'id':
    young_prior_cov = TIMO.IdentityCovariance(
        stg.YOUNG_PRIOR_STD**2
    )

# Set up generating fields
if stg.FIELD == 'prior':
    young_field_vals = None
elif stg.FIELD == 'four-steels-10':
    # The beam is composed by four different kinds of steel
    # E = 190  [ x < 3 ]
    # E = 213  [ 3 < x < 4 ]
    # E = 195  [ 4 < x < 8 ]
    # E = 208  [ 8 < x < 10]
    class YoungModulusFieldExpression(dol.UserExpression):
        def eval(self, value, x):
            if 0 <= x[0] <= 3.:
                value[0] = 190
            elif 3 < x[0] <= 4:
                value[0] = 213
            elif 4 < x[0] <= 8:
                value[0] = 195
            else:
                value[0] = 208
        def value_shape(self):
            return (1,)
    young_field_expr = YoungModulusFieldExpression(element=solver.AUXFS.ufl_element())
    young_field_vals = dol.project(young_field_expr, solver.AUXFS).vector().get_local()
elif stg.FIELD == 'four-steels-4':
    # The beam is composed by four different kinds of steel
    # E = 190  [ x < 2.5 ]
    # E = 213  [ 2.5 < x < 5 ]
    # E = 195  [ 5 < x < 7.5 ]
    # E = 208  [ 7.5 < x < 10]
    class YoungModulusFieldExpression(dol.UserExpression):
        def eval(self, value, x):
            if 0 <= x[0] <= 2.5:
                value[0] = 190
            elif 2.5 < x[0] <= 5.:
                value[0] = 213
            elif 5. < x[0] <= 7.5:
                value[0] = 195
            else:
                value[0] = 208
        def value_shape(self):
            return (1,)
    young_field_expr = YoungModulusFieldExpression(element=solver.AUXFS.ufl_element())
    young_field_vals = dol.project(young_field_expr, solver.AUXFS).vector().get_local()
elif stg.FIELD == 'five-steels-5':
    # The beam is composed by four different kinds of steel
    # E = 190  [ x < 2. ]
    # E = 213  [ 2. < x < 4. ]
    # E = 195  [ 4. < x < 6. ]
    # E = 208  [ 6. < x < 8. ]
    # E = 200  [ 8. < x < 10.]
    class YoungModulusFieldExpression(dol.UserExpression):
        def eval(self, value, x):
            if 0 <= x[0] <= 2.:
                value[0] = 190
            elif 2. < x[0] <= 4.:
                value[0] = 213
            elif 4. < x[0] <= 6.:
                value[0] = 195
            elif 6. < x[0] <= 8.:
                value[0] = 208
            else:
                value[0] = 200
        def value_shape(self):
            return (1,)
    young_field_expr = YoungModulusFieldExpression(element=solver.AUXFS.ufl_element())
    young_field_vals = dol.project(young_field_expr, solver.AUXFS).vector().get_local()
else:
    raise ValueError("Field %s not recognized." % stg.FIELD)

# Assemble posterior distribution
pi = TIMO.TimoshenkoYoungModulusPosteriorDistribution(
    solver,
    sens_pos_list               = stg.SENS_POS_LIST,
    sens_geo_eps                = stg.SENS_GEO_EPS,
    piecewise                   = stg.PIECEWISE,
    young_prior_mean_field_vals = young_prior_mean_vals,
    young_prior_cov             = young_prior_cov,
    lkl_std                     = stg.LKL_STD,
    young_field_vals            = young_field_vals,
)

# Append generation options to the object
pi.options = stg
    
if pi.real_observations is None:

    sens_pos = np.asarray(pi.sens_pos_list)
    
    # Plot fields
    plt.figure()
    plt.plot(pi.solver.aux_coord.flatten(), pi.young_field_vals)
    plt.scatter(pi.sens_pos_list, [np.mean(pi.young_field_vals)]*len(pi.sens_pos_list), marker='x', color='r')
    plt.title("Young modulus")
    plt.show(False)

    # Plot synthetic solution
    u = solver.solve(pi.young_field_vals)
    plt.figure()
    p = dol.plot(u)
    for pos in pi.sens_pos_list:
        plt.plot([pos]*2, [0, u([pos])], 'x-r')
    plt.axis('equal')
    plt.title('Synthetic solution')
    plt.show(False)

    # Plot observations
    pointwise_obs = [u([pos]) for pos in pi.sens_pos_list]
    noise_free_obs = pi.obs_without_noise
    obs = pi.logL.y
    plt.figure()
    plt.plot(pointwise_obs, 'go', label='pointwise')
    plt.plot(noise_free_obs, 'ko', label='w/o noise')
    plt.plot(obs, 'ro', label='with noise')
    # plt.ylim(0., 1.)
    plt.legend(loc='best')
    plt.show(False)

    # Samples from the prior (GPa)
    x = pi.piecewise_map.evaluate( pi.prior.rvs(1) )[0,:]
    plt.figure()
    plt.plot(pi.solver.aux_coord.flatten(), x)
    plt.plot(pi.solver.aux_coord.flatten(), pi.young_field_vals, 'r')
    plt.title("Young modulus - prior sample (GPa)")
    plt.show(False)

    print("signal/noise: " + str(np.abs(noise_free_obs/stg.LKL_STD)))
    
# Store
pi.store(OUT_FNAME + '.dill')
    
# Build up whitened distribution
pi.logL = Maps.CompositeMap(pi.logL, pi.prior_map)
pi.prior = pi.prior.base_distribution
    
# Taylor test for derivatives
x = np.zeros((1,pi.dim))
dx = 1e11 * (solver.aux_ndofs / pi.dim) * np.tile(npr.randn(1,pi.dim), (1, 1))
TM.taylor_test(
    x, dx,
    f = pi.tuple_grad_x_log_pdf,
    fungrad=True
)

# # Finite difference for derivatives
# pi.test_gradients(x, fd_dx=1e-1)

# Test speed of adjoint
NSAMPS = 100
start = time.process_time()
for i in range(NSAMPS):
    _, _ = pi.tuple_grad_x_log_pdf(x)
stop = time.process_time()
print("Adjoint: %.2f f/s" % (NSAMPS/(stop-start)))

# Store Distributions
pi.store(OUT_FNAME + '-white.dill')
