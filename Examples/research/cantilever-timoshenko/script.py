from dolfin import *
from dolfin_adjoint import *
import numpy as np
import numpy.random as npr

npr.seed(0)

# Define parameters (in meters)
thickness = 0.03
width = 0.01
length = 10.
kappa = 5./6.
load = - 1.
A = width * thickness
I = width * thickness**3. / 12.

# Discretization parameters
nels = 100

# Define material properties (~steel) in GPa
young = 200e9
shear = young/2
E = Constant(young)
G = Constant(shear)

# Define discretization and function spaces
mesh = IntervalMesh(nels, 0, length)
UE = FiniteElement("CG", mesh.ufl_cell(), 2)
PE = FiniteElement("CG", mesh.ufl_cell(), 1)
VEFS = FunctionSpace(mesh, UE * PE)
UEFS, PEFS = VEFS.split()

# Set boundary conditions (clamped)
bcs = [
    DirichletBC(
        UEFS, Constant(0.), lambda x: near(x[0], 0)),
    DirichletBC(
        PEFS, Constant(0.), lambda x: near(x[0], 0))
]

# Set sensor position
sens_pos = 8.0
sens_eps = 1e-3
nrm = assemble(
        Expression(
            'exp(-.5 * pow(p-x[0],2) / pow(eps,2))',
            pi=np.pi, p=sens_pos, eps=sens_eps,
            element=VEFS.sub(0).ufl_element(), annotate=False
        ) * project(Constant(1.), UEFS.collapse(), annotate=False) * dx,
    annotate=False
)
sens_expr = '1./nrm * exp(-.5 * pow(p-x[0],2) / pow(eps,2))'
sensor = Expression(
    sens_expr, nrm=nrm, pi=np.pi, p=sens_pos, eps=sens_eps,
    element=UEFS.ufl_element()
)

# Other parameters
A = Constant(A)
I = Constant(I)
kappa = Constant(kappa)
q = Constant(load)
# Test and trial functions
u_ = TestFunction(VEFS)
ub = TrialFunction(VEFS)
(w_, phi_) = split(u_)
(wb, phib) = split(ub)
# Define variational problem
a = E * I * inner(grad(phi_), grad(phib)) * dx + \
    kappa * A * G * dot(grad(w_)[0]-phi_, grad(wb)[0]-phib) * dx
L = q * w_ * dx
# Solve
u = Function(VEFS)
solve(a == L, u, bcs)
# w,_ = split(u)
J = assemble(u[0] * sensor * dx)
# Create reduced functional
Jhat = ReducedFunctional(
    J, [Control(E), Control(G)]
)

# Test displacement
I = width * thickness**3. / 12.
displ = load * length**4 / 8 / young / I
print("Mismatch: %e" % (np.abs(displ - u[0]([length]))/np.abs(displ)))

# Taylor test
Et = Constant(young/2)
Gt = Constant(shear/2)
dE = Constant(1e3 * npr.randn())
dG = Constant(1e3 * npr.randn())
conv_rate = taylor_test(
    Jhat, [Et, Gt], [dE, dG]
)
