#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team and Alessio Spantini
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/ and spantini@mit.edu
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import logging
import sys, os, getopt
import warnings
import copy
import dill
import numpy as np
import scipy.stats as stats
from tabulate import tabulate
import time
from datetime import timedelta

# np.random.seed(0)

import AdaptivityRoutines as AR

import TransportMaps as TM
import TransportMaps.FiniteDifference as FD
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG
import src.TransportMaps.Maps as MAPS

# Data storage object
stg = type('', (), {})()

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

def usage():
    print('python FirstVariationAdaptivity.py --dens=<file_name> ' + \
          '-q <qtype> -n <qnum> --output=<file_name> [--eps-var-diag=1e-2 ' + \
          '--coeff-trunc=.7 --avar-trunc=.2, --with-reg=None --ders=2 ' + \
          '--tol-kl=1e-4 --tol-reg=1e-4 --nmc-max=5000 ' + \
          '--nmc-incr=.2 --bsize=<batch_size> ' + \
          '--nprocs=<num_processes>]')

def print_avail_qtype():
    print('Available <quad_type>:')
    for qtypen, qtypename in AVAIL_QUADRATURE.items():
        print('  %d: %s' % (qtypen, qtypename))        

def full_usage():
    usage()
    print_avail_qtype()

argv = sys.argv[1:]
# Initial quadrature parameters    
stg.QTYPE = None
stg.QNUM = None
# Solver's parameters
stg.REG = None
stg.DERS = 2
stg.TOL_KL = 1e-4
stg.TOL_REG = 1e-4
# Refinement parameters
stg.EPS_VAR_DIAG = 1e-2
stg.IT_MAX_VAR_DIAG = 5
stg.NMC_VAR_DIAG_INCR = 1.5
stg.COEFF_TRUNC = .3
stg.AVAR_TRUNC = .4
stg.IT_MAX_COEFF_REFINE = 3
stg.TOL_STAB_VAR_DIAG = 5e-2
stg.NMC_MAX = 5000
stg.NMC_INCR = 2.
stg.TOL_VAR_COEFF_REL = 1e-1
stg.TOL_VAR_COEFF_ABS = 1e-2
# Laplace preprocess parameters
stg.PULL_LAPLACE = False
# Direct or inverse transport
stg.IS_DIRECT = True
# I/O parameters
DENS_FNAME = None
OUT_FNAME = None
# Parallelization and batching parameters
BATCH_SIZE = None
NPROCS = 1
WARMSTART = False
try:
    opts, args = getopt.getopt(argv,"hq:n:",[
        # Solver's parameters
        "with-reg=", "tol-kl=", "tol-reg=", "ders=",
        # Refinement parameters
        "eps-var-diag=", "it-max-var-diag=", "nmc-var-diag-incr=",
        "coeff-trunc=", "avar-trunc=",
        "it-max-coeff-refine=", "tol-stab-var-diag", "nmc-max=", "nmc-incr=",
        "tol-var-coeff-rel=", "tol-var-coeff-abs=",
        # Laplace pre-process
        "pull-laplace",
        # Inverse transport
        "inverse-transport",
        # I/O Parameters
        "dens=", "output=",
        # Warm starting, parallelization and batching parameters
        "force-warm", "nprocs=", "bsize="])
except getopt.GetoptError as e:
    full_usage()
    print(e)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-q"):
        stg.QTYPE = int(arg)
    elif opt in ("-n"):
        stg.QNUM = int(arg)

    # Solver's parameters
    elif opt in ("--with-reg"):
        stg.REG = {'type': 'L2',
                    'alpha': float(arg)}
    elif opt in ("--tol-kl"):
        stg.TOL_KL = float(arg)
    elif opt in ("--tol-reg"):
        stg.TOL_REG = float(arg)
    elif opt in ("--ders"):
        stg.DERS = int(arg)

    # Refinement parameters
    elif opt in ("--eps-var-diag"):
        stg.EPS_VAR_DIAG = float(arg)
    elif opt in ("--it-max-var-diag"):
        stg.IT_MAX_VAR_DIAG = int(arg)
    elif opt in ("--nmc-var-diag-incr"):
        stg.NMC_VAR_DIAG_INCR = float(arg)
    elif opt in ("--coeff-trunc"):
        stg.COEFF_TRUNC = float(arg)
    elif opt in ("--avar-trunc"):
        stg.AVAR_TRUNC = float(arg)
    elif opt in ("--it-max-coeff-refine"):
        stg.IT_MAX_COEFF_REFINE = int(arg)
    elif opt in ("--tol-stab-var-diag"):
        stg.TOL_STAB_VAR_DIAG = float(arg)
    elif opt in ("--nmc-max"):
        stg.NMC_MAX = int(arg)
    elif opt in ("--nmc-incr"):
        stg.NMC_INCR = float(arg)
    elif opt in ("--tol-var-coeff-rel"):
        stg.TOL_VAR_COEFF_REL = float(arg)
    elif opt in ("--tol-var-coeff-abs"):
        stg.TOL_VAR_COEFF_ABS = float(arg)

    # Laplace pre-process
    elif opt in ("--pull-laplace"):
        stg.PULL_LAPLACE = True

    # Inverse transport
    elif opt in ("--inverse-transport"):
        stg.IS_DIRECT = False
        
    # I/O parameters
    elif opt in ("--dens"):
        DENS_FNAME = arg
    elif opt in ("--output"):
        OUT_FNAME = arg

    # Warm starting, parallelization and batching parameters
    elif opt in "--force-warm":
        WARMSTART = True
    elif opt in ("--nprocs="):
        NPROCS = int(arg)
    elif opt in ("-b", "--bsize"):
        BATCH_SIZE = int(arg)
    else:
        raise RuntimeError("Input option %s not recognized." % opt)

if not WARMSTART and None in [DENS_FNAME, OUT_FNAME]:
    full_usage()
    sys.exit(1)

if stg.COEFF_TRUNC > 1. or stg.COEFF_TRUNC < 0.:
    full_usage()
    sys.exit(2)

if stg.AVAR_TRUNC > 1. or stg.AVAR_TRUNC < 0.:
    full_usage()
    sys.exit(3)

if OUT_FNAME is not None and os.path.exists(OUT_FNAME) and not WARMSTART:
    if sys.version_info[0] == 3:
        sel = input("The file %s already exists. " % OUT_FNAME + \
                    "Do you want to use it to warm start? [y/N] ")
    else:
        sel = raw_input("The file %s already exists. " % OUT_FNAME + \
                        "Do you want to use it to warm start? [y/N] ")
    if sel == 'y' or sel == 'Y':
        WARMSTART = True
    else:
        if sys.version_info[0] == 3:
            sel = input("Do you want to overwrite? [y/N] ")
        else:
            sel = raw_input("Do you want to overwrite? [y/N] ")
        if sel == 'y' or sel == 'Y':
            pass
        else:
            print("Terminating.")
            sys.exit(0)

refine = False
if WARMSTART:
    print("All arguments will be ignored on warm-starting")
    # Restore state
    with open(OUT_FNAME, 'rb') as in_stream:
        stg = dill.load(in_stream)
        var_diag = stg.var_diag_list[-1]
        it = len(stg.tm_approx_list) - 1
        print("Iteration %d -- var-diag: %e" % (it+1, var_diag))
        if var_diag > stg.EPS_VAR_DIAG:
            refine = True
        tm_approx = stg.tm_approx_list[-1]
        stg.base_density = DENS.StandardNormalDensity(stg.target_density.dim)
        qparams = stg.qparams_list[-1]
        if stg.QTYPE == 0:
            qparams_var_diag = int(qparams * stg.NMC_VAR_DIAG_INCR)
        elif qtype == 3:
            qparams_var_diag = [qparams + 1] * stg.target_density.dim

else:
    with open(DENS_FNAME,'rb') as in_stream:
        stg.target_density = dill.load(in_stream)
    it = -1
    if stg.QTYPE == 0:
        qparams = stg.QNUM
        qparams_var_diag = int(stg.QNUM * stg.NMC_VAR_DIAG_INCR)
    elif stg.QTYPE == 3:
        qparams = [stg.QNUM] * stg.target_density.dim
        qparams_var_diag = [stg.QNUM+1] * stg.target_density.dim
    stg.tm_approx_list = []
    stg.var_diag_list = []
    stg.qparams_list = []

    # Init base density
    stg.base_density = DENS.StandardNormalDensity(stg.target_density.dim)
    # Initialize the linear map
    # tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
    #     stg.target_density.dim, order=3)
    tm_approx = TM.Default_IsotropicIntegratedExponentialDiagonalTransportMap(
         stg.target_density.dim, order=1)
    if stg.PULL_LAPLACE:
        laplace_approx = TM.laplace_approximation( stg.target_density )
        stg.laplace_map = Maps.LinearTransportMap.build_from_Gaussian(laplace_approx)

# Initialize pullback and pushforward
if not stg.PULL_LAPLACE:
    target_density = stg.target_density
else:
    target_density = DENS.PullBackTransportMapDensity(stg.laplace_map, stg.target_density)

# Initial variance diagnostic
var_diag = DIAG.variance_approx_kl(stg.base_density, target_density,
                                   qtype=stg.QTYPE,
                                   qparams=qparams_var_diag)
print("Iteration -1 -- var-diag: %e" % var_diag)

if stg.IS_DIRECT:
    tm_density = DENS.PushForwardTransportMapDensity(tm_approx, stg.base_density)
    pb_density = DENS.PullBackTransportMapDensity(tm_approx, target_density)
else:
    stg.DERS = min(stg.DERS, 1)
    tm_density = DENS.PullBackTransportMapDensity(tm_approx, stg.base_density)
    pb_density = DENS.PushForwardTransportMapDensity(tm_approx, target_density)

mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

try:
    #################################
    ####  Initial approximation  ####
    #################################
    abort = False
    if not WARMSTART:
        it += 1
        print("Iteration %d" % it)

        var_coeff_refine = True
        it_var_coeff_refine = 0
        while var_coeff_refine and it_var_coeff_refine < stg.IT_MAX_COEFF_REFINE:
            if stg.QTYPE == 0:
                print ("\tIteration %d -- Variance iteration %d -- N points: %d" % (
                    it, it_var_coeff_refine, qparams))
            it_var_coeff_refine += 1
            # SOLVE
            nc = tm_approx.get_n_coeffs()
            bsize = None if BATCH_SIZE is None else BATCH_SIZE//nc
            print("\t\t[Start] Solve -- N coeffs: %d" % nc)
            start = time.clock()
            log_entry_solve = tm_density.minimize_kl_divergence(
                target_density, qtype=stg.QTYPE, qparams=qparams,
                x0=tm_approx.get_coeffs(), regularization=stg.REG,
                tol=stg.TOL_KL, ders=stg.DERS,
                batch_size=[None,None,bsize], disp=False,
                mpi_pool=mpi_pool
            )
            stop = time.clock()
            print("\t\tSolve time: %s -- N coeffs: %d" % (timedelta(seconds=(stop-start)),
                                                          tm_approx.get_n_coeffs()))
            # Variance diagnostic
            var_diag = DIAG.variance_approx_kl(stg.base_density, pb_density,
                                               qtype=stg.QTYPE,
                                               qparams=qparams_var_diag,
                                               mpi_pool_tuple=(None,mpi_pool))
            if stg.QTYPE != 0:
                var_coeff_refine = False
            else:
                # Check whether the variance diagnostic is affected
                # by the variance of the coefficients (MC points)
                coeffs1 = tm_approx.get_coeffs()
                var_diag1 = var_diag
                # SOLVE
                nc = tm_approx.get_n_coeffs()
                bsize = None if BATCH_SIZE is None else BATCH_SIZE//nc
                print("\t\t[Start] Solve -- N coeffs: %d" % nc)
                start = time.clock()
                log_entry_solve = tm_density.minimize_kl_divergence(
                    target_density, qtype=stg.QTYPE, qparams=qparams,
                    x0=tm_approx.get_coeffs(), regularization=stg.REG,
                    tol=stg.TOL_KL, ders=stg.DERS,
                    batch_size=[None,None,bsize], disp=False,
                    mpi_pool=mpi_pool
                )
                stop = time.clock()
                print("\t\tSolve time: %s -- N coeffs: %d" % (
                    timedelta(seconds=(stop-start)), tm_approx.get_n_coeffs()))
                coeffs2 = tm_approx.get_coeffs()
                var_diag2 = DIAG.variance_approx_kl(stg.base_density, pb_density,
                                                    qtype=stg.QTYPE,
                                                    qparams=qparams_var_diag,
                                                    mpi_pool_tuple=(None,mpi_pool))
                # Check sensitivity of variance diagnostic to the coefficients
                if np.abs(var_diag1 - var_diag2) < stg.TOL_STAB_VAR_DIAG * var_diag1:
                    var_coeff_refine = False
                else:
                    # Find coefficients with high variance
                    tol_var_idxs = np.where(
                        np.abs(coeffs1 - coeffs2) > stg.TOL_VAR_COEFF_REL * \
                        np.abs(coeffs1) + stg.TOL_VAR_COEFF_ABS )[0]
                    if len(tol_var_idxs) == 0:
                        var_coeff_refine = False
                    else:
                        if qparams < stg.NMC_MAX and \
                           it_var_coeff_refine < stg.IT_MAX_COEFF_REFINE:
                            qparams = min( int(qparams * stg.NMC_INCR), stg.NMC_MAX )
                            qparams_var_diag = int(qparams * stg.NMC_VAR_DIAG_INCR)
                        else:
                            (abort, rm_avar, rm_const_midxs,
                             rm_exp_midxs) = AR.remove_coefficients(tm_approx, tol_var_idxs)
                            if abort:
                                var_coeff_refine = False
                                # SOLVE
                                nc = tm_approx.get_n_coeffs()
                                bsize = None if BATCH_SIZE is None else BATCH_SIZE//nc
                                print("\t\t[Start] Solve -- " + \
                                      "N coeffs: %d" % nc)
                                start = time.clock()
                                log_entry_solve = tm_density.minimize_kl_divergence(
                                    target_density, qtype=stg.QTYPE, qparams=qparams,
                                    x0=tm_approx.get_coeffs(), regularization=stg.REG,
                                    tol=stg.TOL_KL, ders=stg.DERS,
                                    batch_size=[None,None,bsize], disp=False,
                                    mpi_pool=mpi_pool
                                )
                                stop = time.clock()
                                print("\t\tSolve time: %s -- " % timedelta(
                                    seconds=(stop-start)) + \
                                      "N coeffs: %d" % tm_approx.get_n_coeffs())
                                # Variance diagnostic
                                var_diag = DIAG.variance_approx_kl(
                                    stg.base_density, pb_density, qtype=stg.QTYPE,
                                    qparams=qparams_var_diag, mpi_pool_tuple=(None,mpi_pool))
                            if it_var_coeff_refine == stg.IT_MAX_COEFF_REFINE:
                                it_var_coeff_refine -= 1

        print("Iteration %d -- var-diag: %e" % (it, var_diag))
        if var_diag > stg.EPS_VAR_DIAG:
            refine = True
        else:
            refine = False

        # Store
        stg.tm_approx_list.append( copy.deepcopy(tm_approx) )
        stg.var_diag_list.append( var_diag )
        stg.qparams_list.append( qparams )
        with open(OUT_FNAME, 'wb') as out_stream:
            dill.dump(stg, out_stream)

        print("")

    ###############################
    #### Refinement iterations ####
    ###############################
    while refine and it < stg.IT_MAX_VAR_DIAG:
        it += 1
        print("Iteration %d" % it)
        ######################################
        ####  First variation refinement  ####
        ######################################
        # # Check derivatives of the grad_x first deviation
        # x = base_density.rvs(10)
        # def f(x, params):
        #     d1 = params['d1']
        #     d2 = params['d2']
        #     return TM.grad_t_kl_divergence(x, d1, d2)
        # def gx_f(x, params):
        #     d1 = params['d1']
        #     d2 = params['d2']
        #     (_,out) = TM.grad_x_grad_t_kl_divergence(x, d1, d2)
        #     return out
        # params = {'d1': base_density,
        #           'd2': pb_density}
        # if not FD.check_grad_x(f, gx_f, x, 1e-4, params):
        #     raise RuntimeError("Wrong gradient")

        (x,w) = stg.base_density.quadrature(stg.QTYPE, qparams)
        # gt_kl_div = TM.grad_t_kl_divergence(x, base_density, pb_density)
        gt_kl_div, gx_gt_kl_div = TM.grad_x_grad_t_kl_divergence(
            x, stg.base_density, pb_density, mpi_pool_tuple=(None,mpi_pool))
        exp_gx_gt_kl_div = np.tensordot(np.abs(gx_gt_kl_div), w, (0,0))

        # Construct the approximation of first variation
        fv_approx = AR.first_variation_candidate_map(
            tm_approx, gt_kl_div, exp_gx_gt_kl_div, stg.AVAR_TRUNC)

        # Regress on the first variation
        fv_approx.regression(gt_kl_div, x=x, w=w, tol=stg.TOL_REG, disp=False)
        # Enrich the original approximation
        AR.first_variation_refinement(tm_approx, fv_approx, stg.COEFF_TRUNC)

        var_coeff_refine = True
        it_var_coeff_refine = 0
        while var_coeff_refine and it_var_coeff_refine < stg.IT_MAX_COEFF_REFINE:
            if stg.QTYPE == 0:
                print ("\tIteration %d -- Variance iteration %d -- N points: %d" % (
                    it, it_var_coeff_refine, qparams))
            it_var_coeff_refine += 1
            # SOLVE
            nc = tm_approx.get_n_coeffs()
            bsize = None if BATCH_SIZE is None else BATCH_SIZE//nc
            print("\t\t[Start] Solve -- N coeffs: %d" % nc)
            start = time.clock()
            log_entry_solve = tm_density.minimize_kl_divergence(
                target_density, qtype=stg.QTYPE, qparams=qparams,
                x0=tm_approx.get_coeffs(), regularization=stg.REG,
                tol=stg.TOL_KL, ders=stg.DERS,
                batch_size=[None,None,bsize], disp=False,
                mpi_pool=mpi_pool
            )
            stop = time.clock()
            print("\t\tSolve time: %s -- N coeffs: %d" % (timedelta(seconds=(stop-start)),
                                                          tm_approx.get_n_coeffs()))
            # Variance diagnostic
            var_diag = DIAG.variance_approx_kl(stg.base_density, pb_density,
                                               qtype=stg.QTYPE,
                                               qparams=qparams_var_diag,
                                               mpi_pool_tuple=(None,mpi_pool))
            if stg.QTYPE != 0:
                var_coeff_refine = False
            else: # Control variance of coefficients (MC points)
                coeffs1 = tm_approx.get_coeffs()
                var_diag1 = var_diag
                # SOLVE
                nc = tm_approx.get_n_coeffs()
                bsize = None if BATCH_SIZE is None else BATCH_SIZE//nc
                print("\t\t[Start] Solve -- N coeffs: %d" % nc)
                start = time.clock()
                log_entry_solve = tm_density.minimize_kl_divergence(
                    target_density, qtype=stg.QTYPE, qparams=qparams,
                    x0=tm_approx.get_coeffs(), regularization=stg.REG,
                    tol=stg.TOL_KL, ders=stg.DERS,
                    batch_size=[None,None,bsize], disp=False,
                    mpi_pool=mpi_pool
                )
                stop = time.clock()
                print("\t\tSolve time: %s -- N coeffs: %d" % (
                    timedelta(seconds=(stop-start)), tm_approx.get_n_coeffs()))
                coeffs2 = tm_approx.get_coeffs()
                var_diag2 = DIAG.variance_approx_kl(stg.base_density, pb_density,
                                                    qtype=stg.QTYPE,
                                                    qparams=qparams_var_diag,
                                                    mpi_pool_tuple=(None,mpi_pool))
                # Check sensitivity of variance diagnostic to the coefficients
                if np.abs(var_diag1 - var_diag2) < stg.TOL_STAB_VAR_DIAG * var_diag1:
                    var_coeff_refine = False
                else:
                    # Find coefficients with high variance
                    tol_var_idxs = np.where(
                        np.abs(coeffs1 - coeffs2) > stg.TOL_VAR_COEFF_REL * \
                        np.abs(coeffs1) + stg.TOL_VAR_COEFF_ABS )[0]
                    if len(tol_var_idxs) == 0:
                        var_coeff_refine = False
                    else:
                        if qparams < stg.NMC_MAX and \
                           it_var_coeff_refine < stg.IT_MAX_COEFF_REFINE:
                            qparams = min( int(qparams * stg.NMC_INCR), stg.NMC_MAX )
                            qparams_var_diag = int(qparams * stg.NMC_VAR_DIAG_INCR)
                        else:
                            (abort, rm_avar, rm_const_midxs,
                             rm_exp_midxs) = AR.remove_coefficients(tm_approx, tol_var_idxs)
                            if abort:
                                var_coeff_refine = False
                                # SOLVE
                                nc = tm_approx.get_n_coeffs()
                                bsize = None if BATCH_SIZE is None else BATCH_SIZE//nc
                                print("\t\t[Start] Solve -- " + \
                                      "N coeffs: %d" % nc)
                                start = time.clock()
                                log_entry_solve = tm_density.minimize_kl_divergence(
                                    target_density, qtype=stg.QTYPE, qparams=qparams,
                                    x0=tm_approx.get_coeffs(), regularization=stg.REG,
                                    tol=stg.TOL_KL, ders=stg.DERS,
                                    batch_size=[None,None,bsize], disp=False,
                                    mpi_pool=mpi_pool
                                )
                                stop = time.clock()
                                print("\t\tSolve time: %s -- " % timedelta(seconds=(stop-start) + \
                                      "N coeffs: %d" % tm_approx.get_n_coeffs()))
                                # Variance diagnostic
                                var_diag = DIAG.variance_approx_kl(
                                    stg.base_density, pb_density, qtype=stg.QTYPE,
                                    qparams=qparams_var_diag, mpi_pool_tuple=(None,mpi_pool))
                            if it_var_coeff_refine == stg.IT_MAX_COEFF_REFINE:
                                it_var_coeff_refine -= 1

        print("Iteration %d -- var-diag: %e" % (it, var_diag))
        if var_diag < stg.EPS_VAR_DIAG:
            refine = False

        # Store
        stg.tm_approx_list.append( copy.deepcopy(tm_approx) )
        stg.var_diag_list.append( var_diag )
        stg.qparams_list.append( qparams )
        with open(OUT_FNAME, 'wb') as out_stream:
            dill.dump(stg, out_stream)

        print("")
finally:
    if mpi_pool is not None:
        mpi_pool.stop()