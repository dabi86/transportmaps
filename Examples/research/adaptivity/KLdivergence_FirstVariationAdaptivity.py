#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import TransportMaps as TM
import sys, getopt
import warnings
import numpy as np
import time
from datetime import timedelta

import AdaptivityRoutines as AR

import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
import TransportMaps.tests.TestFunctions as TF

# # DEBUG MPI
# import mpi_map
# mpi_map.logger.setLevel(logging.DEBUG)

# # DEBUG everything
# logging.basicConfig(level=logging.INFO)

# TESTS
#  0: 1D - N(mu,sigma)
#  NOT WORKING 1: 1D - Mixture X~N(-1,1), Y~N(1,0.5), p=0.3
#  2: 1D - T(x) = a + b*x + c * arctan(d + e*x)
#  3: 1D - Y = T(x) = exp(a*x) -- Corresponding to Y~logN(0,a^2)
#  4: 1D - Y~Logistic(\mu,s)
#  5: 1D - Y~Gamma(loc,scale)
#  6: 1D - Y~Beta(a,b)
#  7: 1D - Gumbel distribution
#  8: 1D - Weibull distribution
#  9: 2D - N(mu,sigma)
# 10: 2D - Banana
AVAIL_TESTN = {0: '1D Std. Normal',
               2: '1D ArcTan',
               3: '1D LogNormal',
               4: '1D Logistic',
               5: '1D Gamma',
               6: '1D Beta',
               7: '1D Gumbel',
               8: '1D Weibull',
               9: '2D Std. Normal',
               10: '2D Banana'}

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

def usage():
    print('python KLdivergence.py -t <test_number> -q <quad_type> -n <quad_n_points> [--tol=<tolerance> --with-reg=<alpha> --no-plotting --no-l2]')

def mpi_usage():
    print('mpirun -n <nprocs> python KLdivergence.py -t <test_number> -q <quad_type> -n <quad_n_points> [--tol=<tolerance> --with-reg=<alpha> --no-plotting --no-l2]')

def print_avail_qtype():
    print('Available <quad_type>:')
    for qtypen, qtypename in AVAIL_QUADRATURE.items():
        print('  %d: %s' % (qtypen, qtypename))

def print_avail_testn():
    print('Available <test_number>:')
    for testn, testname in AVAIL_TESTN.items():
        print('  %d: %s' % (testn, testname))

def print_example():
    print('python KLdivergence.py -t 2 -m intexp -s total -q 3 -n 30 --tol=1e-4 --with-reg=1e-3')

def full_usage():
    usage()
    mpi_usage()
    print_avail_testn()
    print_avail_qtype()
    print_example()

argv = sys.argv[1:]

PLOTTING = True
TESTN = None
QTYPE = None
QNUM = None
REG = None
# Tolerance
TOL = 1e-4
NPROCS = 1
STORE_FIG = False
try:
    opts, args = getopt.getopt(argv,"ht:m:s:q:n:",["testn=", 
                                                   "qtype=", "qnum=",
                                                   "tol=", "with-reg=",
                                                   "no-plotting",
                                                   "store-fig"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-t", "--testn"):
        TESTN = int(arg)
        if TESTN not in AVAIL_TESTN:
            usage()
            print_avail_testn()
            sys.exit(1)
    elif opt in ("-q", "--qtype"):
        QTYPE = int(arg)
        if QTYPE not in AVAIL_QUADRATURE:
            usage()
            print_avail_qtype()
            sys.exit(1)
    elif opt in ("-n", "--qnum"):
        QNUM = int(arg)
    elif opt in ("--tol"):
        TOL = float(arg)
    elif opt in ("--with-reg"):
        REG = {'type': 'L2',
               'alpha': float(arg)}
    elif opt in ("--no-plotting"):
        PLOTTING = False
    elif opt in ("--store-fig"):
        STORE_FIG = True
if None in [TESTN, QTYPE, QNUM]:
    full_usage()
    sys.exit(3)

if PLOTTING:
    try:
        import matplotlib.pyplot as plt
    except ImportError:
        PLOT_SUPPORT = False
        warnings.warn("No plotting support")
    else:
        PLOT_SUPPORT = True
    fsize = (6,5)
    discr = 80
    fig_folder = 'data/Figures/UnitTests'
    fig_formats = ['eps', 'pdf', 'svg', 'png']
    def store_figure(fig, fname, fig_formats):
        for fmat in fig_formats:
            fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight')

# Get test title and parameters
title, setup, test = TF.get(TESTN)

# REGULARIZATION
# None: No regularization
# L2: L2 regularization
if REG is None:
    tit_reg = 'None'
elif REG['type'] == 'L2':
    tit_reg = REG['type'] + '-' + str(REG['alpha'])

# L2 error estimation
# 3: Gauss quadrature of order n
# 0: Monte Carlo quadrature with n point
qtype = QTYPE
if qtype == 0:
    qparams = QNUM
    tit_intest = "MC" + '-' + str(qparams)
elif qtype == 3:
    qparams = [QNUM] * setup['dim']
    tit_intest = "Quad" + '-' + str(qparams[0])

# Gradient information
# 0: derivative free
# 1: gradient information
# 2: Hessian information
ders = 2

# Construct distribution
# Target distribution to be approximated L^\sharp \pi_{\rm tar}
target_distribution = DIST.PullBackTransportMapDistribution(test['support_map'],
                                                  test['target_distribution'])

################################
####  Pre-porcessing plots  ####
################################

# Start plotting of the 1d map
if PLOTTING and PLOT_SUPPORT:
    pspan = setup['plotspan']
    rspan = setup['refplotspan']
    fig = DIAG.plotAlignedConditionals(target_distribution, range_vec=pspan,
                                       numPointsXax=discr,
                                       show_flag=(not STORE_FIG))
    if STORE_FIG: store_figure(
            fig, fig_folder+'/'+title+'-target-aligned-conditionals', fig_formats)
    fig = DIAG.plotAlignedSliceMap(test['target_map'], range_vec=pspan,
                                   numPointsXax=discr,
                                   show_flag=(not STORE_FIG))
    if STORE_FIG: store_figure(
            fig, fig_folder+'/'+title+'-target-aligned-slice-map', fig_formats)

#################################
####  Initial approximation  ####
#################################

# Initialize the linear map
tm_approx = TM.Default_IsotropicIntegratedExponentialDiagonalTransportMap(
    setup['dim'], order=1)

# Distribution T_\sharp \pi
tm_distribution = DIST.PushForwardTransportMapDistribution(tm_approx, test['base_distribution'])

# SOLVE
start = time.clock()
log_entry_solve = tm_distribution.minimize_kl_divergence(target_distribution, qtype=qtype,
                                                    qparams=qparams,
                                                    regularization=REG,
                                                    tol=TOL, ders=ders,
                                                    batch_size=[None,None,None])
stop = time.clock()
print("Solve time: %s -- N coeffs: %d" % (timedelta(seconds=(stop-start)),
                                          tm_approx.get_n_coeffs()))

# Construct approximate pushforward distribution L_\sharp T_\sharp \pi
approx_distribution = DIST.PushForwardTransportMapDistribution(test['support_map'], tm_distribution)

# Construct the approximate pushforward map T \circ L
approx_map = Maps.CompositeTransportMap(tm_approx, test['support_map'])

# Construct approximate pullback distribution T^\sharp L^\sharp \pi_tar
pull_back = DIST.PullBackTransportMapDistribution(tm_approx, target_distribution)

# PLOT
if PLOTTING and PLOT_SUPPORT:
    fig = DIAG.plotAlignedConditionals(approx_distribution, range_vec=pspan,
                                       numPointsXax=discr, show_flag=(not STORE_FIG))
    if STORE_FIG: store_figure(
            fig, fig_folder+'/'+title+'-it0-aligned-conditionals', fig_formats)
    fig = DIAG.plotAlignedSliceMap(approx_map, range_vec=pspan, numPointsXax=discr,
                             show_flag=(not STORE_FIG))
    if STORE_FIG: store_figure(
            fig, fig_folder+'/'+title+'-it0-aligned-slice-map', fig_formats)
    fig = DIAG.plotAlignedConditionals(pull_back, range_vec=rspan, numPointsXax=discr,
                                 show_flag=(not STORE_FIG))
    if STORE_FIG: store_figure(
            fig, fig_folder+'/'+title+'-it0-aligned-pull-back', fig_formats)

refine_flag = True
threshold = 1e-5    # Threshold for the size of the added coefficients
it = 0
while refine_flag:
    it += 1
    ##############################################
    ####  Calculation of the first variation  ####
    ##############################################
    if PLOTTING and PLOT_SUPPORT:
        x = np.linspace(-7,7,30)
        XX,YY = np.meshgrid(x,x)
        xx = np.vstack((XX.flatten(), YY.flatten())).T
        gt_kl_div = TM.grad_t_kl_divergence(xx, test['base_distribution'], pull_back)
        for d in range(setup['dim']):
            plt.figure()
            plt.contourf(XX, YY, gt_kl_div[:,d].reshape(XX.shape), 20, cmap=plt.cm.bone)
            plt.colorbar()
            plt.title("it %d - First variation - dim %d" % (it,d))

    (x,w) = test['base_distribution'].quadrature(qtype, qparams)
    gt_kl_div = TM.grad_t_kl_divergence(x, test['base_distribution'], pull_back)

    # Construct the approximation of first variation
    fv_approx = AR.first_variation_candidate_map(tm_approx, gt_kl_div, threshold, w)

    # Check whether the number of discretization points are sufficient for the
    # regression
    if fv_approx.get_n_coeffs() < x.shape[0]:
        # Regress on the first variation
        fv_approx.regression(gt_kl_div, x=x, w=w, tol=1e-8)

        # Enrich the original approximation
        refine_flag = AR.first_variation_refinement(tm_approx, fv_approx, threshold)
    else:
        refine_flag = False

    if refine_flag:
        # Refine approximation minimizing kl-divergence
        x0 = tm_approx.get_coeffs()
        start = time.clock()
        log_entry_solve = tm_distribution.minimize_kl_divergence(
            target_distribution, qtype=qtype, qparams=qparams, x0=x0, regularization=REG,
            tol=TOL, ders=ders, batch_size=[None,None,None])
        stop = time.clock()
        print("Solve time: %s -- N coeffs: %d" % (timedelta(seconds=(stop-start)),
                                                  tm_approx.get_n_coeffs()))

        # PLOT
        if PLOTTING and PLOT_SUPPORT:
            fig = DIAG.plotAlignedConditionals(approx_distribution,
                                               range_vec=pspan,
                                               numPointsXax=discr,
                                               show_flag=(not STORE_FIG))
            if STORE_FIG: store_figure(
                    fig, fig_folder+'/'+title+'-it%d-aligned-conditionals' % it,
                    fig_formats)
            fig = DIAG.plotAlignedSliceMap(approx_map, range_vec=pspan,
                                           numPointsXax=discr,
                                           show_flag=(not STORE_FIG))
            if STORE_FIG: store_figure(
                    fig, fig_folder+'/'+title+'-it%d-aligned-slice-map' % it,
                    fig_formats)
            fig = DIAG.plotAlignedConditionals(pull_back, range_vec=rspan,
                                               numPointsXax=discr,
                                               show_flag=(not STORE_FIG))
            if STORE_FIG: store_figure(
                    fig, fig_folder+'/'+title+'-it%d-aligned-pull-back' % it,
                    fig_formats)