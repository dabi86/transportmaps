#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team and Alessio Spantini
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/ and spantini@mit.edu
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, os, getopt
import dill
import json
import numpy as np

import TransportMaps as TM
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

def usage():
    print('python FirstVariationAdaptivity-Postprocess.py --data=<file_name> ' + \
          '[--no-plotting --store-fig ' + \
          '--no-pullback --no-pushforward --no-map --no-var-diag --no-lin-plot ' + \
          '--no-gx-plot ' + \
          '--bsize=<batch_size> --nprocs=<num_processes> --mpi-lag=<lag>]')

def print_avail_qtype():
    print('Available <quad_type>:')
    for qtypen, qtypename in AVAIL_QUADRATURE.items():
        print('  %d: %s' % (qtypen, qtypename))        

def full_usage():
    usage()
    print_avail_qtype()

argv = sys.argv[1:]
BATCH_SIZE = None
DATA_FNAME = None
DISCR = 10
INIT_MAP = 0
PLOTTING = True
PLOT_TARGET = True
RANGE_TARGET = [-8,8]
PLOT_PULLBACK = True
RANGE_PULLBACK = [-5,5]
PLOT_PUSHFORWARD = True
RANGE_PUSHFORWARD = [-8,8]
PLOT_MARGINALS = True
NMC_MARGINALS = 10000
PLOT_MAP = True
N_TRI_PLOTS = 0
VAR_DIAG = True
LIN_PLOT = True
GX_PLOT = True
NLEV_LIN_PLOT = 2
EPS_LIN_PLOT = 1e-1
STORE_FIG = False
EXTRA_TIT = ''
NPROCS = 1
try:
    opts, args = getopt.getopt(argv,"h",["bsize=", "nprocs=",
                                         "data=", "discr=", "init-map=",
                                         "no-plotting", "store-fig",
                                         "no-target", "range-target=",
                                         "no-pullback", "range-pullback=",
                                         "no-map", "no-lin-plot",
                                         "no-gx-plot", "no-pushforward",
                                         "range-pushforward=",
                                         "no-marginals", "no-var-diag",
                                         "nmc-marginals=", "n-tri-plots=",
                                         "nlev-lin-plot=", "eps_lin_plot=",
                                         "extra-tit="])
except getopt.GetoptError as e:
    full_usage()
    print(e)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--data"):
        DATA_FNAME = arg
    elif opt in ("--bsize"):
        BATCH_SIZE = int(arg)
    elif opt in ("--discr"):
        DISCR = int(arg)
    elif opt in ("--init-map"):
        INIT_MAP = int(arg)
    elif opt in "--no-plotting":
        PLOTTING = False
    elif opt in ("--store-fig"):
        STORE_FIG = True
    elif opt in ("--no-target"):
        PLOT_TARGET = False
    elif opt in ("--range-target"):
        RANGE_TARGET = json.loads(arg)
    elif opt in ("--no-pullback"):
        PLOT_PULLBACK = False
    elif opt in ("--range-pullback"):
        RANGE_PULLBACK = json.loads(arg)
    elif opt in ("--no-pushforward"):
        PLOT_PUSHFORWARD = False
    elif opt in ("--range-pushforward"):
        RANGE_PUSHFORWARD = json.loads(arg)
    elif opt in ("--no-marginals"):
        PLOT_MARGINALS = False
    elif opt in ("--no-map"):
        PLOT_MAP = False
    elif opt in ("--n-tri-plots"):
        N_TRI_PLOTS = int(arg)
    elif opt in ("--no-var-diag"):
        VAR_DIAG = False
    elif opt in ("--no-lin-plot"):
        LIN_PLOT = False
    elif opt in ("--no-gx-plot"):
        GX_PLOT = False
    elif opt in ("--nmc-marginals"):
        NMC_MARGINALS = int(arg)
    elif opt in ("--nlev-lin-plot"):
        NLEV_LIN_PLOT = int(arg)
    elif opt in ("--eps-lin-plot"):
        EPS_LIN_PLOT = float(arg)
    elif opt in ("--extra-tit"):
        EXTRA_TIT = arg
    elif opt in ("--nprocs="):
        NPROCS = int(arg)
    else:
        raise RuntimeError("Input option %s not recognized." % opt)

if None in [DATA_FNAME]:
    full_usage()
    sys.exit(1)

if not PLOTTING and STORE_FIG:
    full_usage()
    sys.exit(3)

if N_TRI_PLOTS != 0:
    N_TRI_PLOTS = list(range(N_TRI_PLOTS))

with open(DATA_FNAME, 'rb') as in_stream:
    stg = dill.load(in_stream)
    print("Number of maps: %d" % len(stg.tm_approx_list))

if PLOTTING:
    try:
        import matplotlib.pyplot as plt
    except ImportError:
        PLOT_SUPPORT = False
        warnings.warn("No plotting support")
    else:
        PLOT_SUPPORT = True
if PLOTTING and PLOT_SUPPORT:
    fsize = (6,5)
    if STORE_FIG:
        fig_folder = 'data/Figures'
        fig_formats = ['eps', 'pdf', 'svg', 'png']
        tit_no_path = str.split(DATA_FNAME,"/")[-1]
        title = '.'.join(str.split(tit_no_path,".")[:-1])
        def store_figure(fig, fname, fig_formats):
            for fmat in fig_formats:
                fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight')

mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

try:
    ncoeffs_list = []

    if PLOTTING and PLOT_SUPPORT and PLOT_TARGET:
        # Find MAP point
        laplace_approx = TM.laplace_approximation( stg.target_density )
        # PLOT
        fig = DIAG.plotAlignedConditionals(stg.target_density,
                                           pointEval=laplace_approx.mu,
                                           range_vec=RANGE_TARGET,
                                           numPointsXax=DISCR,
                                           show_flag=(not STORE_FIG))
        if STORE_FIG: store_figure(
                fig, fig_folder+'/'+title+'-target-aligned-conditionals', fig_formats)

    for it, tm_approx in enumerate(stg.tm_approx_list[INIT_MAP:]):
        ncoeffs_list.append( tm_approx.get_n_coeffs() )
        print("Iteration %d/%d -- N coeffs: %d" % (it, len(stg.tm_approx_list),
                                                   ncoeffs_list[-1]))

        # Define densities
        if not stg.PULL_LAPLACE:
            push_density = DENS.PushForwardTransportMapDensity(tm_approx, stg.base_density)
            pull_density = DENS.PullBackTransportMapDensity(tm_approx, stg.target_density)
        else:
            push_density = DENS.PushForwardTransportMapDensity(
                stg.laplace_map,
                DENS.PushForwardTransportMapDensity(
                    tm_approx, stg.base_density) )
            pull_density = DENS.PullBackTransportMapDensity(
                tm_approx,
                DENS.PullBackTransportMapDensity(
                    stg.laplace_map, stg.target_density) )

        # PLOTS
        if PLOTTING and PLOT_SUPPORT:
            # Push forward
            if PLOT_PUSHFORWARD:
                print("Pushforward")
                fig = DIAG.plotAlignedConditionals(
                    push_density, pointEval=laplace_approx.mu, numPointsXax=DISCR,
                    dimensions_vec=N_TRI_PLOTS,
                    range_vec=RANGE_PUSHFORWARD, show_flag=(not STORE_FIG),
                    mpi_pool=mpi_pool)
                if STORE_FIG:
                    store_figure(
                        fig, fig_folder+'/'+title+ \
                        '-it%d-aligned-pushforward-conditionals' % it + \
                        EXTRA_TIT,
                        fig_formats)
            # Map
            if PLOT_MAP:
                print("Map")
                fig = DIAG.plotAlignedSliceMap(tm_approx, numPointsXax=DISCR,
                                               dimensions_vec=N_TRI_PLOTS,
                                               show_flag=(not STORE_FIG),
                                               mpi_pool=mpi_pool)
                if STORE_FIG: store_figure(
                        fig, fig_folder+'/'+title+'-it%d-aligned-slice-map' % it,
                        fig_formats)
            # Pullback
            if PLOT_PULLBACK:
                print("Pullback")
                fig = DIAG.plotAlignedConditionals(
                    pull_density, numPointsXax=DISCR, dimensions_vec=N_TRI_PLOTS,
                    range_vec=RANGE_PULLBACK, show_flag=(not STORE_FIG),
                    mpi_pool=mpi_pool)
                if STORE_FIG: store_figure(
                        fig, fig_folder+'/'+title+ \
                        '-it%d-aligned-pull-back-conditionals' % it,
                        fig_formats)

            # Marginals
            if PLOT_MARGINALS:
                print("Marginals")
                samples = push_density.rvs(NMC_MARGINALS, mpi_pool=mpi_pool)
                fig = DIAG.plotAlignedMarginals(samples, dimensions_vec=N_TRI_PLOTS,
                                                show_flag=(not STORE_FIG))
                if STORE_FIG: store_figure(
                        fig,
                        fig_folder+'/'+title+ \
                        '-it%d-aligned-pushforward-marginals' % it + \
                        '-' + EXTRA_TIT,
                        fig_formats)

            # Linearity plot
            if LIN_PLOT:
                print("Linearity plot")
                fig = DIAG.plotLinearityMap(tm_approx, nlevels=NLEV_LIN_PLOT,
                                            threshold=EPS_LIN_PLOT,
                                            show_flag=(not STORE_FIG))
                if STORE_FIG:
                    store_figure(fig, fig_folder+'/'+title+'-it%d-linearity' % it,
                                 fig_formats)

            # Gradient plot
            if GX_PLOT:
                print("Grad_x plot")
                fig = DIAG.plotGradXMap(tm_approx, show_flag=(not STORE_FIG),
                                        mpi_pool=mpi_pool)
                if STORE_FIG:
                    store_figure(fig, fig_folder+'/'+title+'-it%d-gx' % it,
                                 fig_formats)

    if VAR_DIAG and PLOTTING:
        print("Variance diagnostic")
        fig = DIAG.nicePlot(ncoeffs_list, stg.var_diag_list,
                            xlabel="Number of coefficients",
                            ylabel=r"$V[\log \rho / T^\sharp \pi]$",
                            logyscale=True,
                            linestyle='w.', show_flag=(not STORE_FIG))
        ax = fig.gca()
        ax.semilogy(ncoeffs_list, stg.var_diag_list, '--', color='gray')
        ax.semilogy(ncoeffs_list, stg.var_diag_list, 'ko', markersize=12)
        if STORE_FIG:
            store_figure(fig, fig_folder+'/'+title+'-variance-diagnostic',
                         fig_formats)

finally:
    if mpi_pool is not None:
        mpi_pool.stop()

    