#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Authors: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import time
import numpy as np
import matplotlib.pyplot as plt

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

NSOLVE = 100

# Banana distribution
a = 1.
b = 1.
mu = np.zeros(2)
sigma2 = np.array([[1., 0.9],[0.9, 1.]])
pi = DIST.BananaDistribution(a, b, mu, sigma2)

# Linear map
tm = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(2, order=1)

# Reference distribution
rho = DIST.StandardNormalDistribution(2)

val_err_list = []
val_time_list = []
val_nsamps_list = []
saa_err_list = []
for niter in range(NSOLVE):
    # # Construct SAA the validator
    # eps_sp = 1e-2    # error stopping criterium
    # cost_limit = 100 # cpu seconds
    # upper_mult = 10  # quadrature multiplier for upper bound
    # lower_n = 3      # number of solves for lower bound
    # alpha = 0.05     # bound quantile
    # lmb_def = 5      # default multiplier for new quadrature
    # lmb_max = 10     # maximum multiplier for new quadrature
    # validator = DIAG.SampleAverageApproximationKLMinimizationValidator(
    #     eps_sp, DIAG.total_time_cost_function, cost_limit, stop_on_fcast=False,
    #     upper_mult=upper_mult, lower_n=lower_n,
    #     alpha=alpha, lmb_def=lmb_def, lmb_max=lmb_max)


    # Construct gradient chi2 validator
    eps_sp = 1e-1          # error stopping criterium
    cost_limit = 100       # cpu seconds
    upper_mult = 10        # quadrature multiplier for upper bound
    n_grad_samps = 5
    n_bootstrap = 100
    alpha = 0.95
    lmb_def = 5
    lmb_max = 10
    fungrad = False
    validator = DIAG.GradientChi2KLMinimizationValidator(
        eps_sp, DIAG.total_time_cost_function, cost_limit, stop_on_fcast=False,
        n_grad_samps=n_grad_samps, n_bootstrap=n_bootstrap,
        alpha=alpha,
        lmb_def=lmb_def, lmb_max=lmb_max,
        fungrad=fungrad)

    # # Construct gradient validator
    # eps_sp = 1e-2          # error stopping criterium
    # cost_limit = 100       # cpu seconds
    # upper_mult = 10        # quadrature multiplier for upper bound
    # n_grad_samps = 10
    # n_gap_resampling = 10
    # beta = 0.95
    # gamma = 0.05
    # lmb_def = 2
    # lmb_max = 10
    # fungrad = False
    # validator = DIAG.GradientStabilityKLMinimizationValidator(
    #     eps_sp, DIAG.total_time_cost_function, cost_limit, stop_on_fcast=False,
    #     n_grad_samps=n_grad_samps, n_gap_resampling=n_gap_resampling,
    #     beta=beta, gamma=gamma, lmb_def=lmb_def, lmb_max=lmb_max,
    #     fungrad=fungrad)


    # Solve
    solve_params = {
        'qtype': 0,
        'qparams': 100,
        'ders': 2,
        'tol': 1e-2,
        'regularization': {'type': 'L2', 'alpha': 1e-1},
    }
    start = time.process_time()
    log = validator.solve_to_tolerance(tm, rho, pi, solve_params)
    stop = time.process_time()
    print("Elapsed time: %.3f s" % (stop-start))
    val_err_list.append( log['validator_error'] )
    val_time_list.append( (stop-start) )
    val_nsamps_list.append( solve_params['qparams'] )

    # Construct SAA the validator
    eps_sp = 1e-1    # error stopping criterium
    cost_limit = 100 # cpu seconds
    upper_mult = 10  # quadrature multiplier for upper bound
    lower_n = 3      # number of solves for lower bound
    alpha = 0.05     # bound quantile
    lmb_def = 5      # default multiplier for new quadrature
    lmb_max = 10     # maximum multiplier for new quadrature
    saa_validator = DIAG.SampleAverageApproximationKLMinimizationValidator(
        eps_sp, DIAG.total_time_cost_function, cost_limit, stop_on_fcast=False,
        upper_mult=upper_mult, lower_n=lower_n,
        alpha=alpha, lmb_def=lmb_def, lmb_max=lmb_max)
    saa_err,_ = saa_validator.error_estimation(
        rho, DIST.PullBackTransportMapDistribution(tm, pi), solve_params)
    print("SAA error: %.3e" % saa_err)
    saa_err_list.append( saa_err )

plt.figure()
plt.scatter(saa_err_list, val_err_list)
plt.show(False)

plt.figure()
plt.hist(val_nsamps_list)
plt.show(False)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.scatter(val_nsamps_list, val_err_list)
ax.set_yscale('log')
plt.show(False)