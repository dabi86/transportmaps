import numpy as np
import itertools
import TransportMaps as TM
import time

dim = 10
MAX_ORD = 10

max_ord_list = np.random.choice(list(range(MAX_ORD+1)), size=dim, replace=True)
print("Maximum orders: %s" % str(max_ord_list))

# Construct with itertools
start = time.clock()
max_ord = max(max_ord_list)
ord_iter = itertools.product(*[range(o+1) for o in max_ord_list])
it_tools_midxs = [ otuple for otuple in ord_iter if sum(otuple) <= max_ord ]
stop = time.clock()
print("Time itertools: %f" % (stop-start))

# Construct with TM implementation
start = time.clock()
tm_midxs = TM.generate_total_order_midxs(max_ord_list)
stop = time.clock()
print("Time TM: %f" % (stop-start))

# Test that the two sets contain the same elements
import collections
compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
print("Passed: %s" % str(compare(it_tools_midxs, tm_midxs)) )