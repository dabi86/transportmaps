#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team and Alessio Spantini
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/ and spantini@mit.edu
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import TransportMaps as TM

if not TM.MPI_SUPPORT or TM.get_mpi_rank() == 0: # Root process

    import sys, os, getopt, time
    from datetime import timedelta
    import dill
    import numpy as np
    import matplotlib.pyplot as plt

    import TransportMaps.Diagnostics as DIAG
    
    def usage():
        print('python FirstVariationAdaptivity-SensitivityTest-Postprocess.py ' + \
              '--data=<file_name> [--store-fig]')

    def full_usage():
        usage()

    argv = sys.argv[1:]

    DATA_FNAME = None
    STORE_FIG = False
    
    try:
        opts, args = getopt.getopt(argv,"h",["data=", "store-fig"])
    except getopt.GetoptError as e:
        full_usage()
        print(e)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            full_usage()
            sys.exit()
        elif opt in ("--data="):
            DATA_FNAME = arg
        elif opt in ("--store-fig="):
            STORE_FIG = True
        else:
            raise RuntimeError("Input option %s not recognized." % opt)

    if None in [DATA_FNAME]:
        full_usage()
        sys.exit(1)

    if STORE_FIG:
        fig_folder = 'data/Figures/SV'
        fig_formats = ['eps', 'pdf', 'svg', 'png']
        tit_no_path = str.split(DATA_FNAME,"/")[-1]
        title = '.'.join(str.split(tit_no_path,".")[:-1])
        def store_figure(fig, fname, fig_formats):
            for fmat in fig_formats:
                fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight')

    with open(DATA_FNAME, 'rb') as in_stream:
        data = dill.load(in_stream)
        coeffs = data['coeffs']

    idx_coeffs = list(range(coeffs.shape[1]))
    mean_coeffs = np.mean(coeffs, axis=0)
    q5 = np.percentile(coeffs, 5, axis=0)
    q25 = np.percentile(coeffs, 25, axis=0)
    q40 = np.percentile(coeffs, 40, axis=0)
    q60 = np.percentile(coeffs, 60, axis=0)
    q75 = np.percentile(coeffs, 75, axis=0)
    q95 = np.percentile(coeffs, 95, axis=0)
    fig1 = DIAG.nicePlot([],[], xlabel="Index coefficients", ylabel="$a_i$",
                                      title="Distribution coefficients")
    ax1 = fig1.gca()
    #ax1.set_aspect('equal')
    #ax.set_ylim(-3,3)
    ax1.set_xlim(0,idx_coeffs[-1])
    #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
    #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
    #ax.plot(target_density.tobs, target_density.dataObs, 'or')
    ax1.plot(idx_coeffs, mean_coeffs, '-r')
    ax1.fill_between(idx_coeffs, q40, q60, facecolor='r', alpha=0.35, linewidth = 0.0)
    ax1.fill_between(idx_coeffs, q25, q75, facecolor='r', alpha=0.25, linewidth = 0.0)
    ax1.fill_between(idx_coeffs, q5, q95, facecolor='r', alpha=0.15, linewidth = 0.0)
    #plt.legend(loc='best')
    # Split components
    # ylim = ax1.get_ylim()
    # counter = 0
    # for tm_comp in tm_approx.approx_list[:-1]:
    #     counter += tm_comp.get_n_coeffs()
    #     ax1.plot([counter]*2, ylim, '--k')
    plt.show(False)