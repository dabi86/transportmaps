#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team and Alessio Spantini
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/ and spantini@mit.edu
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import TransportMaps as TM

if not TM.MPI_SUPPORT or TM.get_mpi_rank() == 0: # Root process

    import sys, os, getopt, time
    from datetime import timedelta
    import dill
    import numpy as np

    import TransportMaps.Densities as DENS
    import TransportMaps.Diagnostics as DIAG
    
    def usage():
        print('python FirstVariationAdaptivity-SensitivityTest.py --data=<file_name> ' + \
              '--nmc=<qnum> --ntests=<ntests> --output=<file_name>' + \
              '[--with-reg=None --ders=2 --tol-kl=1e-4 --bsize=<batch_size> ' + \
              '--nprocs=<num_processes> --mpi-lag=<lag>]')

    def full_usage():
        usage()
        
    argv = sys.argv[1:]
    BATCH_SIZE = None
    DATA_FNAME = None
    QNUM = None
    NTESTS = None
    OUT_FNAME = None
    TOL_KL = 1e-4
    REG = None
    DERS = 2
    NPROCS = None
    MPI_LAG = 0.0
    try:
        opts, args = getopt.getopt(argv,"h",["bsize=", "nprocs=", "mpi-lag=",
                                             "data=", "nmc=", "ntests=", "output=",
                                             "with-reg=", "tol-kl=", "ders="])
    except getopt.GetoptError as e:
        full_usage()
        print(e)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            full_usage()
            sys.exit()
        elif opt in ("--data"):
            DATA_FNAME = arg
        elif opt in ("--bsize"):
            BATCH_SIZE = int(arg)
        elif opt in ("--nprocs="):
            NPROCS = int(arg)
        elif opt in ("--mpi-lag="):
            MPI_LAG = float(arg)
        elif opt in ("--nmc="):
            QNUM = int(arg)
        elif opt in ("--ntests="):
            NTESTS = int(arg)
        elif opt in ("--output="):
            OUT_FNAME = arg
        elif opt in ("--with-reg"):
            REG = {'type': 'L2',
                   'alpha': float(arg)}
        elif opt in ("--tol-kl"):
            TOL_KL = float(arg)
        elif opt in ("--ders"):
            DERS = int(arg)
        else:
            raise RuntimeError("Input option %s not recognized." % opt)

    if None in [DATA_FNAME, QNUM, NTESTS, OUT_FNAME]:
        full_usage()
        sys.exit(1)

    with open(DATA_FNAME, 'rb') as in_stream:
        data = dill.load(in_stream)
        target_density = data['target_density']
        tm_approx_list = data['tm_approx_list']
        var_diag_list = data['var_diag_list']

    print("Number of maps: %d" % len(tm_approx_list))
    tm_approx = tm_approx_list[-1]    
    base_density = DENS.StandardNormalDensity(target_density.dim)
    pull_density = DENS.PullBackTransportMapDensity(tm_approx, target_density)
    push_density = DENS.PushForwardTransportMapDensity(tm_approx, base_density)
    print("Number of coeffs: %d" % tm_approx.get_n_coeffs())

    coeffs = np.zeros((NTESTS, tm_approx.get_n_coeffs()))
    for it in range(NTESTS):
        print("[Start] Test %i -- N coeffs: %d" % (it, tm_approx.get_n_coeffs()))
        start = time.clock()
        log_entry_solve = push_density.minimize_kl_divergence(
            target_density, qtype=0, qparams=QNUM, regularization=REG,
            tol=TOL_KL, ders=DERS, batch_size=[None,None,BATCH_SIZE]# ,
            # nprocs=NPROCS
        )
        stop = time.clock()
        print("Solve time: %s -- N coeffs: %d" % (timedelta(seconds=(stop-start)),
                                                  tm_approx.get_n_coeffs()))
        # Save coeffs
        coeffs[it,:] = tm_approx.get_coeffs()
        # Store
        data = {'coeffs': coeffs,
                'it': it}
        with open(OUT_FNAME, 'wb') as out_stream:
            dill.dump(data, out_stream)