#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import logging
import numpy as np
import TransportMaps as TM
import src.TransportMaps.Maps as MAPS

def first_variation_refinement(tm_approx, fv_approx, coeff_trunc=.7):
    r""" Update (inplace) the transport map approximation, adding the coefficients deemed important from the analysis of the first variation
    """
    refine_flag = False
    tot_add_coeffs = 0
    for d, (tm_comp, tm_comp_avar,
            fv_comp, fv_comp_avar) in enumerate(zip(
                tm_approx.approx_list, tm_approx.active_vars,
                fv_approx.approx_list, fv_approx.active_vars)):
        # Transform multi-indices into matrices
        tm_const_midxs_mat = np.asarray(tm_comp.c.multi_idxs)
        tm_exp_midxs_mat = np.asarray(tm_comp.h.multi_idxs)
        fv_midxs_mat = np.asarray(fv_comp.multi_idxs)
        # Threshold the matrix of multi-indices
        scaled_coeffs = np.abs(fv_comp.get_coeffs())
        scaled_coeffs /= np.max(scaled_coeffs)
        threshold_idxs = list(np.where(scaled_coeffs > coeff_trunc)[0])
        if len(threshold_idxs) == 0:
            n_add_coeffs = 0
        else:
            fv_midxs_mat = fv_midxs_mat[threshold_idxs,:]
            # Find active variables of fv and trim fv_midxs_mat
            fv_avar_idxs = [i for i in range(fv_comp.dim)
                            if np.where(fv_midxs_mat[:,i]>0)[0].shape[0] > 0]
            fv_avar = [fv_comp_avar[i] for i in fv_avar_idxs]
            if not d in fv_avar:
                fv_avar_idxs.append(fv_midxs_mat.shape[1]-1)
                fv_avar.append(d)
            fv_midxs_mat = fv_midxs_mat[:,fv_avar_idxs]
            # Find active variables to be added
            add_var = [var for var in fv_avar if not var in tm_comp_avar]
            fv_add_var = [var for var in tm_comp_avar if not var in fv_avar]
            fv_avar = sorted( fv_avar + fv_add_var )
            tm_comp_avar = sorted( tm_comp_avar + add_var )
            # Add zero columns to the multi-indices of fv_comp
            for var in fv_add_var:
                ivar = fv_avar.index(var)
                nmidxs = fv_midxs_mat.shape[0]
                fv_midxs_mat = np.hstack( (fv_midxs_mat[:,:ivar],
                                           np.zeros((nmidxs,1), dtype=int),
                                           fv_midxs_mat[:,ivar:]) )
            # Add zero columns to the multi-indices of tm_comp
            for var in add_var:
                ivar = tm_comp_avar.index(var)
                # Constant multi-index
                nmidxs_const = tm_const_midxs_mat.shape[0]
                tm_const_midxs_mat = np.hstack( (tm_const_midxs_mat[:,:ivar],
                                                 np.zeros((nmidxs_const,1), dtype=int),
                                                 tm_const_midxs_mat[:,ivar:]) )
                # Exponential multi-index
                nmidxs_exp = tm_exp_midxs_mat.shape[0]
                tm_exp_midxs_mat = np.hstack( (tm_exp_midxs_mat[:,:ivar],
                                               np.zeros((nmidxs_exp,1), dtype=int),
                                               tm_exp_midxs_mat[:,ivar:]) )
            # Split multi-index into constant and exponential parts
            const_rows = list(np.where(fv_midxs_mat[:,-1] == 0)[0])
            exp_rows = list(set(range(fv_midxs_mat.shape[0])).difference(set(const_rows)))
            fv_const_midxs_mat = fv_midxs_mat[const_rows,:]
            fv_exp_midxs_mat = fv_midxs_mat[exp_rows,:]
            # Adjust multi-index of the exponential part to map to the linear span approx
            fv_exp_midxs_mat[:,-1] -= 1
            # Transform multi-indices into lists
            tm_const_midxs = [tuple(idx) for idx in list(tm_const_midxs_mat)]
            tm_exp_midxs = [tuple(idx) for idx in list(tm_exp_midxs_mat)]
            fv_const_midxs = [tuple(idx) for idx in list(fv_const_midxs_mat)]
            fv_exp_midxs = [tuple(idx) for idx in list(fv_exp_midxs_mat)]
            # Enrich the constant and exponential approximations
            add_const_midxs = [midx for midx in fv_const_midxs if not midx in tm_const_midxs]
            add_exp_midxs = [midx for midx in fv_exp_midxs if not midx in tm_exp_midxs]
            if len(add_const_midxs) > 0 or len(add_exp_midxs) > 0:
                refine_flag = True
            tm_const_midxs += add_const_midxs
            tm_exp_midxs += add_exp_midxs
            # Get the maximum order
            if len(tm_const_midxs) == 0:
                max_ord_const = [0] * len(tm_comp_avar)
            else:
                max_ord_const = list(np.max(np.asarray(tm_const_midxs), axis=0))
            max_ord_exp = list(np.max(np.asarray(tm_exp_midxs), axis=0))        
            # Update the tm_approx
            tm_approx.active_vars[d] = tm_comp_avar
            tm_comp.dim = len(tm_comp_avar)
            tm_comp.c.dim = len(tm_comp_avar)
            tm_comp.c.multi_idxs = tm_const_midxs
            tm_comp.c.max_order_list = max_ord_const
            tm_comp.h.dim = len(tm_comp_avar)
            tm_comp.h.multi_idxs = tm_exp_midxs
            tm_comp.h.max_order_list = max_ord_exp
            for var in add_var: # Assuming isotropic basis
                ivar = tm_comp_avar.index(var)
                tm_comp.c.basis_list.insert(ivar, tm_comp.c.basis_list[-1])
                tm_comp.h.basis_list.insert(ivar, tm_comp.h.basis_list[-1])
            # Append new zero coefficients
            tm_comp.c.set_coeffs( np.hstack( (tm_comp.c.get_coeffs(),
                                              np.zeros(len(add_const_midxs))) ) )
            tm_comp.h.set_coeffs( np.hstack( (tm_comp.h.get_coeffs(),
                                              np.zeros(len(add_exp_midxs))) ) )
            n_add_coeffs = len(add_const_midxs)+len(add_exp_midxs)
        lin_max_ord_exp = max_ord_exp.copy()
        lin_max_ord_exp[-1] += 1
        print("Component %i: " % d + \
              "active vars %d - " % len(tm_comp_avar) + \
              "added %d coefficients - " % n_add_coeffs + \
              "max ord: %d" % max(np.max(max_ord_const),np.max(lin_max_ord_exp)))
        tot_add_coeffs += n_add_coeffs
    print("Total number of added coefficients: %d" % tot_add_coeffs)
    return refine_flag

def first_variation_candidate_map(tm_approx, gt_kl_div, exp_gx_gt_kl_div,
                                  avar_trunc=.2):
    r""" Construct the candidate map to be used in the regression of the first variation.

    It takes the multi-indices in tm_approx and increase them by one, adding also active variables if needed. The active variable to add are detected using the second moment of the first variation.
    """
    active_vars = []
    midxs_list = []
    for d, (tm_comp, tm_comp_avar) in enumerate(zip(tm_approx.approx_list,
                                                    tm_approx.active_vars)):
        # Update active variables using second order information
        tmp = np.abs(exp_gx_gt_kl_div[d,:d+1])
        scaled_avar_diag = tmp / np.max(tmp)
        fv_avar = list(np.where(scaled_avar_diag > avar_trunc)[0])
        add_avar = [ var for var in fv_avar if not var in tm_comp_avar ]
        avar = sorted( tm_comp_avar + add_avar )
        # Adjust multi-index of the exponential part to map to the linear span approx
        exp_midxs_mat = np.asarray(tm_comp.h.multi_idxs)
        exp_midxs_mat[:,-1] += 1
        exp_midxs = mat_idxs_to_list_idxs(exp_midxs_mat)
        # Merge constant and exponential multi-indices
        tm_approx_midxs = tm_comp.c.multi_idxs + exp_midxs
        # Transform multi-indices into matrix
        tm_approx_midxs_mat = np.asarray(tm_approx_midxs)
        # Add zero columns to the multi-indices
        for var in add_avar:
            ivar = avar.index(var)
            nmidxs = tm_approx_midxs_mat.shape[0]
            tm_approx_midxs_mat = np.hstack( (tm_approx_midxs_mat[:,:ivar],
                                              np.zeros((nmidxs,1), dtype=int),
                                              tm_approx_midxs_mat[:,ivar:]) )
        # Extract maximum order per direction
        max_ord_vec = np.max(tm_approx_midxs_mat, axis=0)
        max_ord = np.max(np.sum(tm_approx_midxs_mat, axis=1))    
        # Generate new multi-index (GREAT BOTTLENECK)
        max_ord_list = list( max_ord_vec + 1 )
        max_ord += 1
        midxs = TM.generate_total_order_midxs(max_ord_list)
        # Update
        active_vars.append(avar)
        midxs_list.append(midxs)
    fv_approx = TM.Default_LinearSpanTriangularTransportMap(
        tm_approx.dim, midxs_list, active_vars)
    return fv_approx

def remove_coefficients(tm_approx, idxs_list_in):
    r""" Remove coefficients from the approximation and, if necessary, remove active variables.

    .. note:: works only for integrated exponential approximations
    """
    rm_avar = []
    rm_midxs_const = []
    rm_midxs_exp = []
    idxs_list = list(idxs_list_in)
    comp_update = []
    abort = False
    cstart = 0
    for d, (tm_comp, tm_comp_avar) in enumerate(zip(tm_approx.approx_list,
                                                    tm_approx.active_vars)):
        # CONSTANT PART
        cstop = cstart + tm_comp.c.get_n_coeffs()
        rm_idxs_list = []
        while len(idxs_list) > 0  and idxs_list[0] < cstop:
            rm_idxs_list.append( idxs_list.pop(0) - cstart )
        keep_idxs_list = [ i for i in range(tm_comp.c.get_n_coeffs())
                           if not i in rm_idxs_list ]
        # if len(keep_idxs_list) == 0:
        #     logging.warning("Comp %d - Constant: Removing all coefficients" % d + \
        #                     " -- aborting the removal of coefficients")
        #     abort = True
        #     break
        # Remove indices from constant part
        if len(tm_comp.c.multi_idxs) == 0:
            const_midxs_mat = np.zeros((0,len(tm_comp_avar)), dtype=int)
        else:
            const_midxs_mat = np.asarray( tm_comp.c.multi_idxs )
        rm_midxs_const.append( mat_idxs_to_list_idxs( const_midxs_mat[rm_idxs_list,:] ) )
        const_midxs_mat = const_midxs_mat[keep_idxs_list,:]
        const_coeffs = tm_comp.c.get_coeffs()[keep_idxs_list]
        # Update start pointer
        cstart = cstop
        # EXPONENTIAL PART
        cstop = cstart + tm_comp.h.get_n_coeffs()
        rm_idxs_list = []
        while len(idxs_list) > 0  and idxs_list[0] < cstop:
            rm_idxs_list.append( idxs_list.pop(0) - cstart )
        keep_idxs_list = [ i for i in range(tm_comp.h.get_n_coeffs())
                           if not i in rm_idxs_list ]
        if len(keep_idxs_list) == 0:
            logging.warning("Comp %d - Exponential: Removing all coefficients" % d + \
                            " -- reverting to linear approximation")
            exp_midxs_mat = np.asarray( tm_comp.h.multi_idxs )
            rm_midxs_exp.append( mat_idxs_to_list_idxs( exp_midxs_mat[rm_idxs_list,:] ) )
            exp_midxs_mat = np.zeros((1,tm_comp.h.dim), dtype=int)
            exp_coeffs = np.zeros(1)
            abort = True
        else:
            # Remove indices from exponential part
            exp_midxs_mat = np.asarray( tm_comp.h.multi_idxs )
            rm_midxs_exp.append( mat_idxs_to_list_idxs( exp_midxs_mat[rm_idxs_list,:] ) )    
            exp_midxs_mat = exp_midxs_mat[keep_idxs_list,:]
            exp_coeffs = tm_comp.h.get_coeffs()[keep_idxs_list]
        # Update start pointer
        cstart = cstop
        # ACTIVE VARIABLES (to be done over const and exp parts together)
        inc_exp_midxs_mat = exp_midxs_mat.copy()
        inc_exp_midxs_mat[:,-1] += 1
        midxs_mat = np.vstack((const_midxs_mat, inc_exp_midxs_mat))
        idxs_avar = list(np.where(np.max(midxs_mat,axis=0)>0)[0])
        # if midxs_mat.shape[1]-1 not in idxs_avar: # Keep at least diagonal terms
        #     idxs_avar.append( midxs_mat.shape[1]-1 )
        avar = [ tm_comp_avar[i] for i in idxs_avar ]
        # Remove inactive variable from matrices of coefficients
        const_midxs_mat = const_midxs_mat[:,idxs_avar]
        exp_midxs_mat = exp_midxs_mat[:,idxs_avar]
        # Get max orders
        if const_midxs_mat.shape[0] == 0:
            max_ord_const = [0] * len(avar)
        else:
            max_ord_const = list(np.max(const_midxs_mat, axis=0))
        max_ord_exp = list(np.max(exp_midxs_mat, axis=0))
        # OUTPUT: store removed active variables
        iavar = list( set(tm_comp_avar) - set(avar) )
        rm_avar.append( iavar )
        # APPEND update values
        const_midxs_list = mat_idxs_to_list_idxs(const_midxs_mat)
        exp_midxs_list = mat_idxs_to_list_idxs(exp_midxs_mat)
        if tuple([0]*len(avar)) not in exp_midxs_list:
            exp_midxs_list.append( tuple([0]*len(avar)) )
        comp_update.append( ( const_midxs_list, const_coeffs, max_ord_const,
                              exp_midxs_list, exp_coeffs, max_ord_exp,
                              avar, idxs_avar ) )
    # Update map
    tot_rm_coeffs = 0
    for d, (tm_comp, (const_midxs, const_coeffs, max_ord_const,
                      exp_midxs, exp_coeffs, max_ord_exp, avar, idxs_avar),
            rm_const, rm_exp) in enumerate(
                zip(tm_approx.approx_list, comp_update,
                    rm_midxs_const, rm_midxs_exp)):
        # Active variables
        tm_approx.active_vars[d] = avar
        tm_comp.dim = len(avar)
        # Constant part
        tm_comp.c.dim = len(avar)
        tm_comp.c.multi_idxs = const_midxs
        tm_comp.c.max_orer_list = max_ord_const
        tm_comp.c.basis_list = [ tm_comp.c.basis_list[i] for i in idxs_avar ]
        tm_comp.c.set_coeffs(const_coeffs)
        # Exponential part
        tm_comp.h.dim = len(avar)
        tm_comp.h.multi_idxs = exp_midxs
        tm_comp.h.max_orer_list = max_ord_exp
        tm_comp.h.basis_list = [ tm_comp.h.basis_list[i] for i in idxs_avar ]
        tm_comp.h.set_coeffs(exp_coeffs)
        # Output
        rm_coeffs = len(rm_const) + len(rm_exp)
        tot_rm_coeffs += rm_coeffs
        print("Component %i: " % d + \
              "removed %d coefficients" % rm_coeffs)
    print("Total number of removed coefficients: %d" % tot_rm_coeffs)

    return (abort, rm_avar, rm_midxs_const, rm_midxs_exp)

def mat_idxs_to_list_idxs(mat_idxs):
    return [tuple(idxs) for idxs in list(mat_idxs)]
