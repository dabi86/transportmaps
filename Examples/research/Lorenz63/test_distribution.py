#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import numpy as np

from TransportMaps.Distributions.Examples.Lorenz63 import \
    Lorenz63DynamicsMap, \
    Lorenz63ForwardEulerMap

l63map = Lorenz63DynamicsMap()
l63femap = Lorenz63ForwardEulerMap(0.01)

# Test point
x = np.array([[ 1.5089, -1.5313, 25.4609 ]])
v = np.random.randn(1,3)

# Test gradients
l63map.test_gradients(x, v=v)
l63femap.test_gradients(x, v=v)