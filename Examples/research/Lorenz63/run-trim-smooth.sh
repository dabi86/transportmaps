#!/bin/bash

tmap-sequential-postprocess \
    --input=$1/tm.dill \
    --output=$1/post.dill \
    --trim=$2 \
    --quadrature=approx-target \
    --quadrature-qtype=0 \
    --quadrature-qnum=2000 \
    --nprocs=$3 
