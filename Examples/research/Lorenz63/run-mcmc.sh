#!/bin/bash

tmap-sequential-postprocess \
    --input=$1/tm.dill \
    --output=$1/post.dill \
    --mcmc=mhind \
    --mcmc-samples=20000 \
    --mcmc-burnin=1000 \
    --mcmc-skip=0 \
    --nprocs=$2 \
    -v --log=20 ${@:3}
