#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import numpy as np
import matplotlib.pyplot as plt

import SpectralToolbox.Spectral1D as S1D

import TransportMaps.Functionals as FUNC

state_dim = 3              # State dimension
order = 3                  # Polynomial order
rank = 2                   # Rank of sub-maps
btype = 'fun'              # Basis type

components_list = []
active_vars = []

const_sparsity_matrix = np.nan * np.ones( (2*state_dim, 2*state_dim) )
integ_sparsity_matrix = np.nan * np.ones( (2*state_dim, 2*state_dim) )

# Top of the map
for d in range(state_dim):
    if d < rank:
        avars = list(range(d+1))
        oo = order
    else:
        avars = [d]
        oo = 1

    navars = len(avars)

    # Constant part
    c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * navars
    c_orders_list = [oo] * (navars-1) + [0]
    c_approx = FUNC.MonotonicLinearSpanApproximation(
        c_basis_list, spantype='total', order_list=c_orders_list)

    # Integrated squared part
    e_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (navars-1) + \
                   [S1D.ConstantExtendedHermiteProbabilistsFunction()]
    e_orders_list = [oo-1] * (navars)
    e_approx = FUNC.MonotonicLinearSpanApproximation(
        e_basis_list, spantype='total', order_list=e_orders_list)
    
    Tk = FUNC.MonotonicIntegratedSquaredApproximation(c_approx, e_approx)
    components_list.append( Tk )
    active_vars.append( avars )

    for v, co, eo in zip(avars, c_orders_list, e_orders_list):
        const_sparsity_matrix[d, v] = co
        integ_sparsity_matrix[d, v] = eo

# Bottom of the map
for d in range(state_dim):
    if d < rank:
        avars = list(range(rank))
        avars.extend( range(state_dim,state_dim+d+1) )
        oo = order
    else:
        avars = [state_dim+d]
        oo = 1

    navars = len(avars)

    # Constant part
    c_basis_list = [S1D.HermiteProbabilistsPolynomial()] * navars

    # Set up orders in interaction block
    c_orders_list = [oo] * (navars-1) + [0]

    c_approx = FUNC.MonotonicLinearSpanApproximation(
        c_basis_list, spantype='total', order_list=c_orders_list)

    # Integrated squared part
    e_basis_list = [S1D.HermiteProbabilistsPolynomial()] * (navars-1) + \
                   [S1D.ConstantExtendedHermiteProbabilistsFunction()]

    # Set up orders in interaction block
    e_orders_list = [oo-1] * navars
    
    e_approx = FUNC.MonotonicLinearSpanApproximation(
        e_basis_list, spantype='total', order_list=e_orders_list)
    
    Tk = FUNC.MonotonicIntegratedSquaredApproximation(c_approx, e_approx)
    components_list.append( Tk )
    active_vars.append( avars )

    for v, co, eo in zip(avars, c_orders_list, e_orders_list):
        const_sparsity_matrix[d+state_dim, v] = co
        integ_sparsity_matrix[d+state_dim, v] = eo
        
tm = Maps.IntegratedSquaredTriangularTransportMap(
    active_vars, components_list)
        
plt.figure()
plt.imshow(const_sparsity_matrix)
plt.title("Orders of constant part")
plt.colorbar()
plt.show(False)

plt.figure()
plt.imshow(integ_sparsity_matrix)
plt.title("Orders of integrated part")
plt.colorbar()
plt.show(False)

plt.figure()
plt.spy(integ_sparsity_matrix+1)
plt.show(False)

fname = 'data/' + \
        'tms' + \
        '-o-%d' % order + \
        '-rank-%d' % rank + \
        '-btype-' + btype + \
        '.dill'
print("Storing in " + fname)
tm.store(fname)