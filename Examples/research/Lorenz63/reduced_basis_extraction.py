#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the LGNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# LGNU Lesser General Public License for more details.
#
# You should have received a copy of the LGNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2019 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# E-mail: tmteam@mit.edu
#

import sys, getopt
import os.path
import dill

import numpy as np
import numpy.linalg as npla

import matplotlib.pyplot as plt

import TransportMaps as TM
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Distributions as DIST

def usage():
    print("reduced_basis_extraction.py --data=<file_name>")

argv = sys.argv[1:]
DATA = None

try:
    opts, args = getopt.getopt(argv, "h", ['data='])
except:
    usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt == "--data":
        DATA = arg
    else:
        raise ValueError("Unexpected option " + opt)

# Load data
with open(DATA, 'rb') as in_stream:
    stg = dill.load(in_stream)

N = 100

overlap = []
overlap_pca = []
overlap_lap = []
sval1 = []
sval2 = []
sval1lap = []
sval2lap = []


for N in range(1,20):
    print("Step %i" % N)
    
    tar = stg.target_distribution
    fmap = stg.integrator.filtering_map_list[N]
    mc = tar.get_MarkovComponent(N, n=1, state_map=fmap)
    rho = DIST.StandardNormalDistribution(mc.dim)

    rhok = DIST.StandardNormalDistribution(fmap.dim)
    push_fmap_rhok = DIST.PushForwardTransportMapDistribution(fmap,rhok)

    # PCA on samples from push_fmap_rhok
    m = 1000
    X = push_fmap_rhok.rvs(m)
    M = np.mean(X, axis=0)
    B = X - M[np.newaxis,:]
    BTB = np.dot(B.T, B) / (m-1)
    pca_vals, pca_vecs = npla.eigh(BTB)
    pca_vals = pca_vals[::-1]
    pca_vecs = pca_vecs[:,::-1]
    
    # Find basis for the Markov component (active subspace)
    X = rho.rvs(m)
    gxlpdf_mc = mc.grad_x_log_pdf(X)
    H = np.dot(gxlpdf_mc.T, gxlpdf_mc) / (m-1)
    as_vals, as_vecs = npla.eigh(BTB)
    as_vals = as_vals[::-1]
    as_vecs = as_vecs[:,::-1]

    # Assemble the PCA basis
    R1pca = pca_vecs 
    Rp = np.vstack( (R1pca, np.zeros((fmap.dim, fmap.dim))) )

    # Remove the PCA basis from the AS
    RR = H - np.dot(Rp, np.dot(Rp.T, H)) 

    # Extract the non-zero eigendirections
    u,s,v = npla.svd(RR)

    # Extract basis for z_k+1
    R2pca = u[3:,:3]

    o0 = np.abs(npla.det(np.dot(R1pca[:,:2].T, R2pca[:,:2])))
    overlap_pca.append( o0 )

    # Second route 
    # Compute H
    m = 1000
    X = rho.rvs(m)
    X[:,3:] = fmap.evaluate(X[:,3:])
    # X[:,3:] = stg.integrator.filtering_map_list[N].evaluate(X[:,3:])
    gxlpdf_mc = mc.grad_x_log_pdf(X) - rho.grad_x_log_pdf(X)
    H = np.dot(gxlpdf_mc.T, gxlpdf_mc) / (m-1)

    # Compute basis for first component
    # B = np.vstack( (np.zeros((3,3)), np.eye(3)) )
    # H1 = H - np.dot(B, np.dot(B.T, H))
    # u1,s1,v1 = npla.svd(H1)
    u1,s1,v1 = npla.svd(H[:3,:3])
    sval1.append( s1[:3] )
    R1 = u1[:,:3]

    # Compute basis for second component
    # B = np.vstack( (np.eye(3), np.zeros((3,3))) )
    # H2 = H - np.dot(B, np.dot(B.T, H))
    # u2,s2,v2 = npla.svd(H2)
    u2,s2,v2 = npla.svd(H[3:,3:])
    sval2.append( s2[:3] )
    R2 = u2[:,:3]

    # Compute the overlap of the first two eigendirections
    o1 = np.abs(npla.det(np.dot(R1[:,:2].T, R2[:,:2]))) 
    overlap.append( o1 )


plt.figure()
plt.plot(overlap_pca, label='PCA')    
plt.plot(overlap, label='LIS')
# plt.plot(overlap_lap, label='LIS Laplace')
plt.legend()
plt.show(False)

fig = plt.figure()
ax = fig.add_subplot(2,1,1)
sval1 = np.asarray(sval1)
for i in range(3):
    ax.semilogy(sval1[:,i])
ax = fig.add_subplot(2,1,2)
sval2 = np.asarray(sval2)
for i in range(3):
    ax.semilogy(sval2[:,i])
plt.show(False)

# vals, vecs = npla.eigh(Rp)
# vals = vals[::-1]
# vecs = vecs[:,::-1]