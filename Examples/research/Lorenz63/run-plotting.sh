#!/bin/bash

mkdir -p $1/figs

# pudb3 postprocess-plotting.py \

ipython -i postprocess-plotting.py -- \
       --data=$1/tm.dill \
       --postprocess-data=$1/post.dill \
       --rmse-burnin=100 \
       --do-smooth \
       --do-smooth-rmse \
       --do-filt \
       --do-filt-rmse \
       --ntraj=0 \
       --perc=99 \
       --true-dynamics \
       --observations \
       --titles \
       --store-fig-dir=$1/figs/ \
       ${@:2}



       # --do-filt-evol-3d \
       # --do-filt-evol-3d-start=100 \
       # --do-filt-evol-3d-stop=1001 \
       # --do-filt-evol-3d-edir \
       # --do-filt-evol-edir \
