#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.integrate import ode

from TransportMaps.Distributions import NormalDistribution
from TransportMaps.Distributions.Examples.Lorenz63 import \
    Lorenz63ForwardEulerDistribution, \
    Lorenz63DynamicsMap
from src.TransportMaps.Maps import LinearMap

STORE = True
STORE_FIG = True
TEST_GRADIENTS = False
SKIP_PREDICTIONS = False

sigma      = 10.       # Lortenz 63 parameter
beta       = 8./3.     # Lortenz 63 parameter
rho        = 28.       # Lortenz 63 parameter
dim        = 3         # State dimension
obs_dims   = [0,1,2]   # Dimensions observed
init_noise = 2.        # Variance initial conditions
dyn_noise  = 0.1       # Variance in the dynamics
obs_noise  = 2.        # Variance in the observations
dt         = 0.01      # Time discretization (for observations and dynamics)
nsteps     = 1         # Total number of discretization steps
obs_dt     = 0.08      # Time step per observation

obs_dsteps = obs_dt/dt
if math.modf(obs_dsteps)[0] > 1e-13:
    raise ValueError("Observation dt must be a multiple of dt.")
obs_dsteps = int(np.round(obs_dsteps))
if SKIP_PREDICTIONS:
    dstep = obs_dsteps
    dyn_noise *= obs_dsteps
else:
    dstep = 1
T = np.arange(0, (nsteps+1)*dt, dt)
T_obs = np.arange(0, (nsteps+1)*dt, obs_dt)
n_obs = len(T_obs)
dim_obs = len(obs_dims)

# Define distribution initial condition
init_mu = np.array([ 1.5089, -1.5313, 25.4609 ])
pi_init = NormalDistribution(
    init_mu, init_noise * np.eye(dim) )

# Define noise of the dynamics
pi_dyn = NormalDistribution(
    np.zeros(dim), dyn_noise * np.eye(dim) )

# Generation of true dynamics (integration via RK4)
l63map = Lorenz63DynamicsMap(
    sigma=sigma, beta=beta, rho=rho)
def f(t, x, l63map):
    return l63map.evaluate( x.reshape( (1,3) ) )[0,:]
def jac(t, x, l63map):
    return l63map.grad_x( x.reshape( (1,3) ) )[0,:,:]
r = ode(f, jac)
r.set_integrator('dopri5')
r.set_f_params( l63map )
r.set_jac_params( l63map )
xs = np.zeros( (nsteps+1, 3) )
xs[0,:] = init_mu
r.set_initial_value(xs[0,:], 0.)
i = 1
while r.successful() and i <= nsteps:
    xs[i,:] = r.integrate(r.t + dt)
    i += 1

# Generation of the observations
obs_map = LinearMap(np.zeros(dim_obs), np.eye(dim)[obs_dims,:] )
pi_noise = NormalDistribution(
    np.zeros(dim_obs), obs_noise * np.eye(dim_obs) )
obs = obs_map.evaluate(xs[::obs_dsteps,:]) + pi_noise.rvs(n_obs)

# Plotting
fig_stsp = plt.figure()
ax = fig_stsp.add_subplot(111, projection='3d')
ax.plot( xs[:,0], xs[:,1], xs[:,2] )
plt.show(False)

ax_lst = []
fig_sig = plt.figure(figsize=(8.4,4.8))
ax = fig_sig.add_subplot(311)
ax_lst.append(ax)
ax.plot(T, xs[:,0])
ax.set_ylabel(r'$x$')
ax.get_xaxis().set_visible(False)
ax = fig_sig.add_subplot(312)
ax_lst.append(ax)
ax.plot(T, xs[:,1])
ax.set_ylabel(r'$y$')
ax.get_xaxis().set_visible(False)
ax = fig_sig.add_subplot(313)
ax_lst.append(ax)
ax.plot(T, xs[:,2])
ax.set_ylabel(r'$z$')
ax.set_xlabel('time')
for cntr, d in enumerate(obs_dims):
    ax_lst[d].plot(T_obs, obs[:,cntr], 'o', markersize=2)
plt.show(False)

# Assemble full posterior distribution
pi = Lorenz63ForwardEulerDistribution(
    n        = dstep,
    dt       = dt,
    sigma    = sigma,
    beta     = beta,
    rho      = rho,
    pi_init  = pi_init,
    pi_dyn   = pi_dyn,
    obs_map  = obs_map,
    pi_noise = pi_noise,
)
obs_i = 0
n = 0
while n <= nsteps:
    if n % obs_dsteps == 0:
        y = obs[obs_i,:]
        obs_i += 1
    else:
        y = None
    pi.step( y, x=xs[n,:] )
    n += dstep    

if TEST_GRADIENTS:
    print("Testing gradients of distribution")
    x = np.random.randn( 1, pi.dim )
    v = np.random.randn( 1, pi.dim )
    pi.test_gradients(x, v=v)
    print("Testing gradients of first Markov component")
    mc = pi.get_MarkovComponent(0, 1)
    x = np.random.randn( 1, mc.dim )
    v = np.random.randn( 1, mc.dim )
    mc.test_gradients(x, v=v)

dir_name = 'data/' + \
           'L63' + \
           '-nsteps-%d' % nsteps + \
           ('-skippred' if SKIP_PREDICTIONS else '') + \
           '-obs-' + ''.join([str(d) for d in obs_dims]) + \
           '-obsdt%.2f' % obs_dt + \
           '-dyn%1.1e' % dyn_noise 
if STORE:
    # Save distribution
    fname = dir_name + '/dist.dill'
    print("Storing in " + fname)
    pi.store( fname )

if STORE_FIG:
    fmts_list = ['svg', 'pdf', 'eps', 'png']
    # Save state space plot
    fname = dir_name + '/figs/l63-state-space'
    for fmt in fmts_list:
        fig_stsp.savefig(fname+'.'+fmt, format=fmt, bbox_inches='tight');
    # Save trajectories plot
    fname = dir_name + '/figs/l63-signals'
    for fmt in fmts_list:
        fig_sig.savefig(fname+'.'+fmt, format=fmt, bbox_inches='tight');