import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
from TransportMaps.Distributions.Examples.BiochemicalOxygenDemand import \
    JointDistributionLogNormalPrior

from TransportMaps.Algorithms.DeepLazyMaps import *

npr.seed(1)

NPROCS = 1
TM.setLogLevel(20)

nobs = 4
sig2 = 1e-3
times = np.arange(1,nobs+1)
pi = JointDistributionLogNormalPrior(times, sig2)

# Visualize some posteriors to see get an idea of the particular setting
fig = DIAG.plotAlignedConditionals(
    pi, do_diag=False, range_vec=[[-4,4]]*nobs + [[0.,2.],[0.001,0.4]],
    numPointsXax=100)

####################
# Whiten the prior #
####################
A = pi.factors[1][1]
B = pi.factors[2][1]
L = Maps.FrozenLinearDiagonalTransportMap(
    np.array([np.log(A.scale), np.log(B.scale)]), np.array([A.s, B.s]))
exp = Maps.FrozenExponentialDiagonalTransportMap(2)
exp_L = Maps.CompositeTransportMap(exp, L)
P = Maps.IdentityEmbeddedTransportMap(exp_L, [nobs, nobs + 1], nobs + 2)

# pull_exp_L_prior = DIST.PullBackTransportMapDistribution(
#     exp_L, pi.prior)
# fig = DIAG.plotAlignedConditionals(
#     pull_exp_L_prior, do_diag=False,
#     range_vec=[[-5,5],[-5,5]],
#     numPointsXax=100)

pull_P_pi = DIST.PullBackTransportMapDistribution(P, pi)

fig = DIAG.plotAlignedConditionals(
    pull_P_pi, do_diag=False,
    range_vec=[[-5,5]]*(nobs+2),
    numPointsXax=100)

# Laplace approximation
lap = TM.laplace_approximation(pull_P_pi)
lap_map = lap.transport_map

pull_lap_P_pi = DIST.PullBackTransportMapDistribution(
    lap_map, pull_P_pi)

fig = DIAG.plotAlignedConditionals(
    pull_lap_P_pi, do_diag=False,
    range_vec=[[-5,5]]*(nobs+2),
    numPointsXax=100)

fig = DIAG.plotRandomConditionals(
    pull_lap_P_pi)


# ############################
# # Try to nail with one map #
# ############################

# map_ord = 3
# map_qtype = 0
# map_qparams = 1000
# map_reg = {'type':'L2','alpha':1e-2}

# var_qtype=0
# var_qparams=1000

# T1 = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
#     pull_lap_P_pi.dim, map_ord, btype='fun')
# rho = DIST.StandardNormalDistribution(pull_lap_P_pi.dim)
# push_T1_rho = DIST.PushForwardTransportMapDistribution(T1, rho)
# push_T1_rho.minimize_kl_divergence(
#     pull_lap_P_pi, qtype=map_qtype, qparams=map_qparams, ders=1, regularization=map_reg)
# pull_T1_lap_P_pi = DIST.PullBackTransportMapDistribution(T1, pull_lap_P_pi)

# var_diag = DIAG.variance_approx_kl(
#     rho, pull_T1_lap_P_pi, qtype=var_qtype, qparams=var_qparams)
# TM.logger.info("Variance diagnostic one shot: %e" % var_diag)

#############################
# Approximation by LazyMaps #
#############################

plt.pause(0.1)

pi_tar = pull_lap_P_pi

eps = 1e-2
maxit = 20

k_eps = 1e-2 #1e-3
k_max_rank = 4
k_over_samp_mul = 5
k_over_samp_add = 10

ld_qtype=0
ld_qparams=0

hard_truncation = False
map_ord = 3
map_btype = 'poly'
map_qtype = 3
map_qparams = [4] # 1000
map_reg = {
    'type': 'L2',
    'alpha': 1e-3
}
map_maxit = 1000

var_qtype=0
var_qparams=10000

if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)
else:
    mpi_pool = None

try:
    T, var_diag = deep_lazy_map_construction(
        pi_tar,
        # Overall convergence criterion
        eps=eps, maxit=maxit,
        # Low-rank approximation parameters
        k_max_rank=k_max_rank, k_eps=k_eps,
        k_qtype=0,
        k_qparams=k_over_samp_add + k_over_samp_mul * k_max_rank,
        # Low-dimensional target parameters
        ld_qtype=ld_qtype, ld_qparams=ld_qparams,
        # Map approximation parameters
        hard_truncation=hard_truncation,
        map_ord=map_ord, map_btype=map_btype,
        map_qtype=map_qtype, map_qparams=map_qparams,
        map_reg=map_reg, map_maxit=1000,
        # Variance diagnostic parameters
        var_qtype=var_qtype, var_qparams=var_qparams,
        # Parallelization
        mpi_pool=mpi_pool,
        # Plotting options
        plotting=True)
finally:
    if mpi_pool:
        mpi_pool.stop()
