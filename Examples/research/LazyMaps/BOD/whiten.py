import sys
import dill
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import matplotlib as mpl
import matplotlib.pyplot as plt

import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DIST

nobs = 4

with open('data/BODJoint.dill', 'rb') as istr:
    pi = dill.load(istr)

A = pi.factors[1][1]
B = pi.factors[2][1]
L = Maps.FrozenLinearDiagonalTransportMap(
    np.array([np.log(A.scale), np.log(B.scale)]), np.array([A.s, B.s]))
exp = Maps.FrozenExponentialDiagonalTransportMap(2)
exp_L = Maps.CompositeTransportMap(exp, L)
P = Maps.IdentityEmbeddedTransportMap(exp_L, [nobs, nobs + 1], nobs + 2)

pull_P_pi = DIST.PullBackTransportMapDistribution(P, pi)

with open('data/BODJoint-whiten.dill', 'wb') as ostr:
    dill.dump(pull_P_pi, ostr)