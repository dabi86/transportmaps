import sys
import numpy as np
import matplotlib.pyplot as plt
from TransportMaps.Misc import cmdinput
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
from TransportMaps.Distributions.Examples.BiochemicalOxygenDemand import \
    JointDistributionLogNormalPrior, PosteriorDistributionLogNormalPrior

nobs = 6
sig2 = 1e-2
times = np.arange(1,nobs+1)
pi = JointDistributionLogNormalPrior(times, sig2)

# Visualize some posteriors to see get an idea of the particular setting
instr = 'n'
while instr == 'n':
    smpl = pi.rvs(1)
    obs = smpl[0,:nobs]
    par = smpl[0,nobs:]
    pipos = PosteriorDistributionLogNormalPrior(obs, times, sig2)
    fig = DIAG.plotAlignedConditionals(
        pipos, do_diag=False, range_vec=[[0.,2.],[0.001,0.4]],
        numPointsXax=100)

    ####################
    # Whiten the prior #
    ####################
    A = pipos.prior.factors[0][1]
    B = pipos.prior.factors[1][1]
    L = Maps.FrozenLinearDiagonalTransportMap(
        np.array([np.log(A.scale), np.log(B.scale)]), np.array([A.s, B.s]))
    exp = Maps.FrozenExponentialDiagonalTransportMap(2)
    exp_L = Maps.CompositeMap(exp, L)
    
    # pull_exp_L_prior = DIST.PullBackTransportMapDistribution(
    #     exp_L, pipos.prior)
    # fig = DIAG.plotAlignedConditionals(
    #     pull_exp_L_prior, do_diag=False,
    #     range_vec=[[-5,5],[-5,5]],
    #     numPointsXax=100)

    pull_exp_L_pos = DIST.PullBackTransportMapDistribution(
        exp_L, pipos)

    fig = DIAG.plotAlignedConditionals(
        pull_exp_L_pos, do_diag=False,
        range_vec=[[-5,5],[-5,5]],
        numPointsXax=100)

    instr = None
    while instr not in ['n','q','c']:
        instr = cmdinput("Select between (n)ew realization, (q)uit or (c)continue: ")
    if instr == 'q':
        sys.exit(0)
    if instr == 'c':
        break