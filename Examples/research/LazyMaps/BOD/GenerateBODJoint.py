import sys
import dill
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import matplotlib as mpl
import matplotlib.pyplot as plt

from TransportMaps.Distributions.Examples.BiochemicalOxygenDemand import \
    JointDistributionLogNormalPrior

npr.seed(1)

nobs = 4
sig2 = 1e-3
times = np.arange(1,nobs+1)
pi = JointDistributionLogNormalPrior(times, sig2)

with open('data/BODJoint.dill', 'wb') as ostr:
    dill.dump(pi, ostr)