import sys
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import matplotlib as mpl
import matplotlib.pyplot as plt
import TransportMaps as TM
from TransportMaps.Misc import cmdinput
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Builders as BUILD
import TransportMaps.Algorithms.DeepLazyMaps as ALGDEEP
import TransportMaps.Samplers as SAMPS
from TransportMaps.Distributions import BananaDistribution
from TransportMaps.Distributions.Examples import FactorizedBananaDistribution

from TransportMaps.Algorithms.DeepLazyMaps import *

plt.rcParams.update({'font.size': 14})

npr.seed(1)

NPROCS = 1
TM.setLogLevel(20)
FIG_DIR = 'figs'
STORE_FIGS = True

def save_fig(fig, fname):
    for fmat in ['svg', 'pdf', 'png']:
        fig.savefig(
            FIG_DIR + '/' + fname + '.' + fmat,
            format=fmat, bbox_inches='tight'
        )

mux = 0.5
sigma2x = 0.8
sigma2y = 0.2
pi = FactorizedBananaDistribution(
    mu0=mux, sigma0=np.sqrt(sigma2x), sigma1=np.sqrt(sigma2y))

# Randomly rotate pi
A = npr.randn(4).reshape((2,2))
Q, R = npla.qr(A)
rot_map = Maps.LinearTransportMap(np.zeros(2), Q)
rot_pi = DIST.PushForwardTransportMapDistribution(rot_map, pi)

# Visualize some posteriors to see get an idea of the particular setting
fig = DIAG.plotAlignedConditionals(
    rot_pi, do_diag=False,
    range_vec=[[-6,6],[-6,6]],
    numPointsXax=100)
if STORE_FIGS:
    save_fig(fig, 'banana-target')

pi_tar = rot_pi

#############################
# Approximation by LazyMaps #
#############################

plt.pause(0.1)

eps = 1e-3
maxit = 20

k_eps = 1e-3 #1e-3
k_max_rank = 1 #4 #3
k_qtype=3
k_qparams=[10,10]

hard_truncation = False
ht_qtype=3
ht_qparams=[0]

map_ord = 3
if hard_truncation:
    map_factory = ALGDEEP.FixedOrderMapFactory(map_ord)
else:
    map_factory = ALGDEEP.FixedOrderDeepLazyMapFactory(map_ord)

map_qtype = 3
map_qparams = [10,10]
map_reg = None # {
#     'type': 'L2',
#     'alpha': 5e-2
# }
map_maxit = 1000
map_ders = 1

var_qtype=3
var_qparams=[20,20]

assembler_state = TM.DataStorageObject()
callback_kwargs = {
    'state': assembler_state,
    'hard_truncation': hard_truncation,
    'ht_qparams': ht_qparams
}

def callback(tmap, state, hard_truncation, ht_qparams):
    fig = DIAG.plotAlignedConditionals(
        state.pull_pi, do_diag=False,
        range_vec=[[-6,6],[-6,6]],
        numPointsXax=100
    )
    if STORE_FIGS:
        prefix = 'ht-' + str(ht_qparams[0]) + '-' if hard_truncation else '' 
        save_fig(fig, prefix + 'banana-pullback-%i' % state.iter_counter)

builder = BUILD.KullbackLeiblerBuilder(None)
assembler = ALGDEEP.DeepLazyMapsAssembler(
    builder = builder,
    map_factory = map_factory,
    eps = eps,
    maxit = maxit,
    rank_max = k_max_rank,
    rank_eps = k_eps,
    rank_qtype = k_qtype,
    rank_qparams = k_qparams,
    hard_truncation = hard_truncation,
    ht_qtype = ht_qtype,
    ht_qparams = ht_qparams,
    var_diag_qtype = var_qtype,
    var_diag_qparams = var_qparams,
    callback=callback,
    callback_kwargs=callback_kwargs
)

if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)
else:
    mpi_pool = None

try:
    tm, vdiag = assembler.assemble(
        state = assembler_state,
        mpi_pool = mpi_pool,
        plotting = True,
        target_distribution = pi_tar,
        builder_solve_params = {
            'qtype': map_qtype,
            'qparams': map_qparams,
            'regularization': map_reg,
            'maxit': map_maxit,
            'ders': map_ders,
        }
    )
    # T, var_diag = deep_lazy_map_construction(
    #     pi_tar,
    #     # Overall convergence criterion
    #     eps=eps, maxit=maxit,
    #     # Low-rank approximation parameters
    #     k_max_rank=k_max_rank, k_eps=k_eps,
    #     k_qtype=k_qtype,
    #     k_qparams=k_qparams,
    #     # Low-dimensional target parameters
    #     ld_qtype=ld_qtype, ld_qparams=ld_qparams,
    #     # Map approximation parameters
    #     hard_truncation=False,
    #     map_ord=map_ord, map_btype='poly',
    #     map_qtype=map_qtype, map_qparams=map_qparams,
    #     map_reg=map_reg, map_maxit=1000,
    #     # Variance diagnostic parameters
    #     var_qtype=var_qtype, var_qparams=var_qparams,
    #     # Parallelization
    #     mpi_pool=mpi_pool,
    #     # Plotting options
    #     plotting=True)
finally:
    if mpi_pool:
        mpi_pool.stop()

# Perform Metropolis with independent proposal on the pullback
pull_pi = DIST.PullBackTransportMapDistribution(tm, pi_tar)
sampler = SAMPS.MetropolisHastingsIndependentProposalsSampler(
    pull_pi, DIST.StandardNormalDistribution(pi_tar.dim))
N = 10000
s, w = sampler.rvs(N)

ess = SAMPS.uwerr(s)
print("ESS: %d/%d (%.3f%%)" % (ess,N, float(ess)/float(N)*100.))
