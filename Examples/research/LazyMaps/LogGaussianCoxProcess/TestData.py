#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import time
import sys, getopt
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import dill as pickle

import TransportMaps as TM

npr.seed(0)

def usage():
    print("LogGaussianCox-PlotData.py --data=<filename>")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
try:
    opts, args = getopt.getopt(argv,"h",["data="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--data"):
        IN_FNAME = arg
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME,'rb') as in_stream:
    d = pickle.load(in_stream)

x = d.prior.rvs(1)
dx = d.prior.rvs(1)

TM.taylor_test(x, dx, d.log_pdf, gf=d.grad_x_log_pdf)

N = 1000
x = d.prior.rvs(N)
# Speed log_pdf
start = time.process_time()
d.log_pdf(x)
stop = time.process_time()
print("log_pdf evaluation time: %.2f [it/s]" % (N/(stop-start)))
# Speed grad_x_log_pdf
start = time.process_time()
d.grad_x_log_pdf(x)
stop = time.process_time()
print("grad_x_log_pdf evaluation time: %.2f [it/s]" % (N/(stop-start)))
