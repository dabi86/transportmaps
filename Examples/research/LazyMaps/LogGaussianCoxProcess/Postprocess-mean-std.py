#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import logging
import time
from datetime import timedelta
import numpy as np
import dill
import h5py

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import src.TransportMaps.Maps as MAPS
import TransportMaps.Samplers as SAMP

def usage():
    print("Postprocess-mean-var.py --data=<filename> " + \
          "[--nmc=1000 --no-title --store-dir=None --no-plotting " + \
          "--log=30 --batch=1000 --nprocs=1]")

def description():
    print("Description \n" + \
          "Load base and target densities along with the transport map " + \
          "which pushes the base to the target (or that pulls the base to " + \
          "the target if the --inverse option is provided)")

def full_usage():
    usage()
    description()

argv = sys.argv[1:]
DATA_FNAME = None
NMC = 1000
# Plotting
PLOTTING = True
TITLE = True
STORE_DIR = None
STORE_FMT = ['png', 'eps', 'svg', 'pdf']
# Logging
LOGGING_LEVEL = 30 # Warnings
# Parallelization
BATCH_SIZE = 1000
NPROCS = 1
try:
    opts, args = getopt.getopt(argv, 'h', ['data=', 'nmc=', 
                                           'no-title', 'store-dir=', 'no-plotting',
                                           'log=', 'batch=', 'nprocs='])
except getopt.GetoptError:
    full_usage()
    raise
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ('--data'):
        DATA_FNAME = arg
    elif opt in ('--nmc'):
        NMC = int(arg)
    elif opt in ('--no-plotting'):
        PLOTTING = False
    elif opt in ('--no-title'):
        TITLE = False
    elif opt in ('--store-dir'):
        STORE_DIR = arg
    elif opt in ('--log'):
        LOGGING_LEVEL = int(arg)
    elif opt in ('--batch'):
        BATCH_SIZE = int(arg)
    elif opt in ('--nprocs'):
        NPROCS = int(arg)
if None in [DATA_FNAME]:
    full_usage()
    sys.exit(2)

logging.basicConfig(level=LOGGING_LEVEL)

if PLOTTING:
    import matplotlib.pyplot as plt
    from matplotlib.ticker import MaxNLocator

    def store_figure(fig, fname, fig_formats):
        for fmat in fig_formats:
            fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight')

# Load data
with open(DATA_FNAME, 'rb') as istr:
    stg = dill.load(istr)

# Load target distribution
target_distribution = stg.target_distribution
dim = target_distribution.dim
obs = target_distribution.obs
N = target_distribution.full_N
full_lmb = target_distribution.full_lmb
full_gp = target_distribution.full_gp
full_dim = full_gp.dim

# Shuffling masks
obs_idxs = target_distribution.full_obs_idxs
mask_obs = np.zeros(full_dim, np.bool)
mask_obs[obs_idxs] = 1
mask_nobs = np.ones(full_dim, np.bool)
mask_nobs[obs_idxs] = 0

# Construct Gaussian process map
gp_map =  Maps.LinearTransportMap(full_gp.mu, full_gp.sampling_mat)
    
# Prepare distribution for Likelihood informed directions
base_distribution = stg.base_distribution
tmap = stg.tmap
approx_distribution = DIST.PushForwardTransportMapDistribution(tmap, base_distribution)

# Prepare distribution for remaining directions
prior_distribution = DIST.StandardNormalDistribution(full_dim - dim)

# Define stacking distribution
class StackDistribution(DIST.Distribution):
    def __init__(self, d1, d2):
        super(StackDistribution, self).__init__(d1.dim + d2.dim)
        self.d1 = d1
        self.d2 = d2
    def rvs(self, n, mpi_pool=None):
        x1 = self.d1.rvs(n, mpi_pool)
        x2 = self.d2.rvs(n)
        return np.hstack((x1,x2))

approx_pull_tar = StackDistribution(approx_distribution, prior_distribution)

# Define approximate target
approx_target = DIST.PushForwardTransportMapDistribution(gp_map, approx_pull_tar)

# Prepare 2d mesh grid
x = np.linspace(0,1,N)
xx, yy = np.meshgrid(x, x)
X = np.vstack( (xx.flatten(), yy.flatten()) ).T
X_obs = X[mask_obs,:]

## Load/Create samples file ##
samp_file = h5py.File(DATA_FNAME + '.hdf5','a')
grp_name = "/tmap"
if grp_name not in samp_file:
    samp_file.create_group(grp_name)
samp_grp = samp_file[grp_name]
if 'samples' in samp_grp:
    if sys.version_info[0] == 3:
        sel = input("The data contain already samples. Do you want to overwrite them? [y/N] ")
    else:
        sel = raw_input("The data contain already samples. Do you want to overwrite them? [y/N] ")
    if sel == 'y' or sel == 'Y':
        samp_grp['samples'].resize(0, axis=0)
else:
    samp_grp.create_dataset("samples", (0, approx_target.dim),
                            maxshape=(None, approx_target.dim), dtype='d')
nsamps_loaded = samp_grp['samples'].shape[0]
print("%i samples loaded." % nsamps_loaded)

# Start MPI
mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

try:
    ####### Generate new samples ######
    start = time.clock()
    nnew = max(0, NMC - nsamps_loaded)
    new_samps = approx_target.rvs(nnew, mpi_pool=mpi_pool, batch_size=BATCH_SIZE)
    stop = time.clock()
    print("Sampling time: %s" % str(timedelta(seconds=stop-start)))

    ##### Store samples #####
    if nnew > 0:
        nsamps = nsamps_loaded + nnew
        samp_grp['samples'].resize(nsamps, axis=0)
        samp_grp['samples'][nsamps_loaded:nsamps,:] = new_samps

    if PLOTTING:
        samp_log_lmb_shu = samp_grp['samples']
        
        ##### Compute mean and standard deviation ######
        samp_log_lmb = np.zeros(samp_log_lmb_shu.shape)
        samp_log_lmb[:,mask_obs] = samp_log_lmb_shu[:,:dim]
        samp_log_lmb[:,mask_nobs] = samp_log_lmb_shu[:,dim:]
        samp_lmb = np.exp(samp_log_lmb)
        mean_lmb = np.mean(samp_lmb, axis=0)
        std_lmb = np.std(samp_lmb, axis=0)
        levels = np.linspace(np.min(mean_lmb),np.max(mean_lmb),20)
        fig = plt.figure(figsize=(6,5))
        plt.contourf(xx, yy, mean_lmb.reshape((N,N)), levels=levels)
        plt.gca().xaxis.set_ticklabels([])
        plt.gca().yaxis.set_ticklabels([])
        plt.colorbar(ticks=MaxNLocator(integer=True))
        plt.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w')
        plt.xlim([0,1])
        plt.ylim([0,1])
        if TITLE:
            plt.title("Mean of $\lambda$")
        plt.show(False)
        if STORE_DIR is not None:
            store_figure(fig, STORE_DIR + '/LogGaussianCox-mean-map-nmc-%i' % NMC, STORE_FMT)

        levels = np.linspace(np.min(std_lmb),np.max(std_lmb),20)
        fig = plt.figure(figsize=(6,5))
        plt.contourf(xx, yy, std_lmb.reshape((N,N)), levels=levels)
        plt.gca().xaxis.set_ticklabels([])
        plt.gca().yaxis.set_ticklabels([])
        plt.colorbar(ticks=MaxNLocator(integer=True))
        plt.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w')
        plt.xlim([0,1])
        plt.ylim([0,1])
        if TITLE:
            plt.title("Standard deviation of $\lambda$")
        plt.show(False)
        if STORE_DIR is not None:
            store_figure(fig, STORE_DIR + '/LogGaussianCox-std-map-nmc-%i' % NMC, STORE_FMT)

finally:
    samp_file.close()
    if mpi_pool is not None:
        mpi_pool.stop()
