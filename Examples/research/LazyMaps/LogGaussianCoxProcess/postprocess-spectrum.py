import dill
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

plt.rcParams.update({'font.size': 14})

ranks_list = [1, 3, 5]
fname = 'data/dist-ng64-no30/tm-r5-o1-extra.dill'
fname_out = 'data/dist-ng64-no30/figs/spectrum-r5-o1-extra'

fig = plt.figure(figsize=(5,4))
ax = fig.add_subplot(111)
ax.set_xlabel("Eigenvalues")
with open(fname,'rb') as istr:
    stg = dill.load(istr)
eig_val_list = stg.assembler_state.eig_val_list
for i, val in enumerate(eig_val_list):
    ax.semilogy(val[:30], 'o-', label=r'$\operatorname{Eig}(H_{%d})$' % i)
ax.legend()
ax.grid()
plt.show(False)

for fmat in ['svg', 'pdf', 'png']:
    fig.savefig(
        fname_out + '.' + fmat,
        format=fmat, bbox_inches='tight'
    )
