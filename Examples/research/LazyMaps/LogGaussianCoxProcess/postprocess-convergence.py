import dill
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

plt.rcParams.update({'font.size': 14})

ranks_list = [1, 3, 5]
fname_list = [
    'data/dist-ng64-no30/tm-r1-o1.dill',
    'data/dist-ng64-no30/tm-r3-o1-trim1.dill',
    'data/dist-ng64-no30/tm-r5-o1-trim1.dill',
]
fname_out = 'data/dist-ng64-no30/figs/convergence-rate'

fig1 = plt.figure(figsize=(5,4))
fig2 = plt.figure(figsize=(5,4))
ax1 = fig1.add_subplot(111)
ax2 = fig2.add_subplot(111)
ax1.set_xlabel(r"Lazy iteration $\ell$")
ax1.set_ylabel(r"$\frac{1}{2}\operatorname{Tr}(H_\ell)$")
ax2.set_xlabel(r"Number of $\pi,\nabla\pi$ evaluations")
ax2.set_ylabel(r"$\frac{1}{2}\operatorname{Tr}(H_\ell)$")
for r, fname in zip(ranks_list, fname_list):
    with open(fname, 'rb') as istr:
        stg = dill.load(istr)
    trH12 = [.5 * p for p in stg.assembler_state.power_list]
    ax1.semilogy(
        trH12, 'o-', label=r'Lazy rank: %d' % r
    )
    # Get the distribution and the total gradient/function
    pi = stg.assembler_state.pull_pi
    for i in range(stg.assembler_state.iter_counter-1):
        pi = pi.base_distribution
    tot_cost = pi.nevals['log_pdf'] + pi.nevals['grad_x_log_pdf']
    cost_per_iter = tot_cost / float(len(trH12))
    cmsm_cost = [ i * cost_per_iter for i in range(len(trH12)) ]
    ax2.semilogy(cmsm_cost, trH12, 'o-', label=r'Lazy rank: %d' % r)
    # Plot vs 
ax1.legend()
ax1.grid()
ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
ax2.legend()
ax2.grid()
ax2.ticklabel_format(
    axis='x',
    style='sci',
    scilimits=(0,0)
)
plt.show(False)

for fmat in ['svg', 'pdf', 'png']:
    fig1.savefig(
        fname_out + '.' + fmat,
        format=fmat, bbox_inches='tight'
    )
    fig2.savefig(
        fname_out + '-cost.' + fmat,
        format=fmat, bbox_inches='tight'
    )
