import dill
import h5py
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import numpy.random as npr

npr.seed(0)

scale_lmb = True
max_lmb = 10

stg_fname = 'data/dist-ng64-no30/tm-r5-o1-trim1.dill'
h5_fname = 'data/dist-ng64-no30/tm-r5-o1-trim1-post.dill.hdf5'
h5_dset = 'quadrature/approx-target/0'
fig_dir = 'data/dist-ng64-no30/tm-r5-o1-trim1/'

def savefig(fig, fname):
    for fmat in ['svg', 'pdf', 'png']:
        fig.savefig(
            fig_dir + '/' + fname + '.' + fmat,
            format=fmat
        )

with open(stg_fname, 'rb') as istr:
    stg = dill.load(istr)
    
pi = stg.target_distribution
gp_tmap = pi.log_likelihood.map_list[-1]
exp_tmap = pi.log_likelihood.map_list[-2]
nobs = pi.nobs
dim = pi.dim
obs = pi.obs

# Shuffling masks
obs_idxs = pi.full_obs_idxs
mask_obs = np.zeros(dim, np.bool)
mask_obs[obs_idxs] = 1
mask_nobs = np.ones(dim, np.bool)
mask_nobs[obs_idxs] = 0

# Load data
f = h5py.File(h5_fname)
data = f[h5_dset][:,:]
f.close()

# Apply the Gaussian process transformation
data_gp = gp_tmap.evaluate(data)

def plot_field(xx, yy, field, nobs, mask_nobs, mask_obs, obs, levels=None, colorbar=True):
    if levels is None:
        levels = 20
    N = xx.shape[0]
    X = np.vstack( (xx.flatten(), yy.flatten()) ).T
    # Reorder field
    ff = np.zeros(field.shape)
    ff[mask_obs] = field[:nobs]
    ff[mask_nobs] = field[nobs:]
    # Plot
    fig = plt.figure(figsize=(6,5))
    ax = fig.add_subplot(111)
    cax = ax.contourf(xx, yy, ff.reshape((N,N)), levels=levels, cmap='jet')
    for c in cax.collections:
        c.set_edgecolor("face")
    if colorbar:
        cb = plt.colorbar(cax, ticks=MaxNLocator(integer=True))
    X_obs = X[mask_obs,:]
    ax.scatter(X_obs[:,0], X_obs[:,1], s=(obs+1)*60, c='w', edgecolors='k', alpha=.7)
    # ax.xaxis.set_ticklabels([])
    # ax.yaxis.set_ticklabels([])
    ax.set_xlim([0,1])
    ax.set_ylim([0,1])
    plt.tick_params(
        axis='both',        # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        left=False,
        right=False,
        labelbottom=False,
        labelleft=False,
    ) 
    # plt.show(False)
    return fig

# Prepare grid
x = np.linspace(0,1,pi.full_N)
xx, yy = np.meshgrid(x, x)

# Select random samples
nrand = 20
idxs = npr.choice(data.shape[0], size=nrand, replace=False)

# # Compute mean and standard deviation (in GP space)
# mean = np.mean(data_gp, axis=0)
# std = np.std(data_gp, axis=0)

# fig = plot_field(xx, yy, mean, nobs, mask_nobs, mask_obs, obs)
# savefig(fig, 'mean-gp-space')
# fig = plot_field(xx, yy, std, nobs, mask_nobs, mask_obs, obs)
# savefig(fig, 'std-gp-space')

# # Plot random samples
# for ii,i in enumerate(idxs):
#     fig = plot_field(xx, yy, data_gp[i,:], nobs, mask_nobs, mask_obs, obs)
#     savefig(fig, 'realization-%d-gp-space' % ii)

# Transform data in exp space
data_exp = exp_tmap.evaluate(data_gp)

# Set up levels
if scale_lmb:
    levels = np.linspace(0, max_lmb, 20)
else:
    levels = 20

# Compute mean and standard deviation (in exp space)
mean = np.mean(data_exp, axis=0)
std = np.std(data_exp, axis=0)

fig = plot_field(xx, yy, mean, nobs, mask_nobs, mask_obs, obs)
savefig(fig, 'mean-exp-space')
fig = plot_field(xx, yy, std, nobs, mask_nobs, mask_obs, obs)
savefig(fig, 'std-exp-space')

# Select random samples
for ii,i in enumerate(idxs):
    fig = plot_field(xx, yy, data_exp[i,:], nobs, mask_nobs, mask_obs, obs,
                     levels=levels, colorbar=(not scale_lmb))
    savefig(fig, 'realization-%d-exp-space' % ii)
