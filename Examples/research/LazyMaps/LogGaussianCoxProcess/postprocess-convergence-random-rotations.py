import dill
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

plt.rcParams.update({'font.size': 14})

ranks_list = [1, 3, 5]
fname_list = [
    'data/dist-ng64-no30/tm-r1-o1.dill',
    'data/dist-ng64-no30/tm-r3-o1-trim1.dill',
    'data/dist-ng64-no30/tm-r5-o1-trim1.dill',
]

rrot_ranks_list = [3]
rrot_fname_list = [
    'data/dist-ng64-no30/tm-rand-r3-o1.dill',
]


fname_out = 'data/dist-ng64-no30/figs/convergence-rate-rand'

fig1 = plt.figure(figsize=(5,4))
fig2 = plt.figure(figsize=(5,4))
ax1 = fig1.add_subplot(111)
ax2 = fig2.add_subplot(111)
ax1.set_xlabel(r"Lazy iteration $\ell$")
ax1.set_ylabel(r"$\mathbb{V}[\log \rho / T^\sharp \pi]$")
ax2.set_xlabel(r"Number of $\pi,\nabla\pi$ evaluations")
ax2.set_ylabel(r"$\mathbb{V}[\log \rho / T^\sharp \pi]$")
for r, fname in zip(ranks_list, fname_list):
    with open(fname, 'rb') as istr:
        stg = dill.load(istr)
    var_diag = stg.assembler_state.var_diag_list
    ax1.semilogy(
        var_diag, 'o-', label=r'Lazy rank: %d' % r
    )
    # Get the distribution and the total gradient/function
    pi = stg.assembler_state.pull_pi
    for i in range(stg.assembler_state.iter_counter-1):
        pi = pi.base_distribution
    tot_cost = pi.nevals['log_pdf'] + pi.nevals['grad_x_log_pdf']
    cost_per_iter = tot_cost / float(len(var_diag))
    cmsm_cost = [ i * cost_per_iter for i in range(len(var_diag)) ]
    ax2.semilogy(cmsm_cost, var_diag, 'o-', label=r'Lazy rank: %d' % r)
for r, fname in zip(rrot_ranks_list, rrot_fname_list):
    with open(fname, 'rb') as istr:
        stg = dill.load(istr)
    var_diag = stg.assembler_state.var_diag_list
    ax1.semilogy(
        var_diag, 'o-', label=r'Rand. rank: %d' % r
    )
    # Get the distribution and the total gradient/function
    pi = stg.assembler_state.pull_pi
    for i in range(stg.assembler_state.iter_counter-1):
        pi = pi.base_distribution
    tot_cost = pi.nevals['log_pdf'] + pi.nevals['grad_x_log_pdf']
    cost_per_iter = tot_cost / float(len(var_diag))
    cmsm_cost = [ i * cost_per_iter for i in range(len(var_diag)) ]
    ax2.semilogy(cmsm_cost, var_diag, 'o-', label=r'Lazy random: %d' % r)

ax1.legend(loc='best')
ax1.grid()
ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
ax2.legend()
ax2.grid()
ax2.ticklabel_format(
    axis='x',
    style='sci',
    scilimits=(0,0)
)
plt.show(False)

for fmat in ['svg', 'pdf', 'png']:
    fig1.savefig(
        fname_out + '.' + fmat,
        format=fmat, bbox_inches='tight'
    )
    fig2.savefig(
        fname_out + '-cost.' + fmat,
        format=fmat, bbox_inches='tight'
    )
