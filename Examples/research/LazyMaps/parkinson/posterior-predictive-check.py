import sys, getopt
import dill
import h5py
import numpy as np

def usage():
    print(
        "python posterior-predictive-check.py --test-dist=<fname> " + \
        "--h5fname=<fname> --h5path=<path>"
    )

argv = sys.argv[1:]
TEST_DIST_FNAME = None
H5_FNAME = None
H5_PATH = None

try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "test-dist=",
            "h5fname=",
            "h5path="
        ]
    )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)

for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--test-dist':
        TEST_DIST_FNAME = arg
    elif opt == '--h5fname':
        H5_FNAME = arg
    elif opt == '--h5path':
        H5_PATH = arg
    else:
        raise AttributeError("Unknown option %s" % opt)

if None in [TEST_DIST_FNAME, H5_PATH, H5_FNAME]:
    print("Error: missing arguments")
    usage()
    sys.exit(2)

# Load distribution with test data
with open(TEST_DIST_FNAME, 'rb') as istr:
    test_dist = dill.load(istr)
Ntest = len(test_dist.logL.y)

# Load h5 file and extract samples from the posterior
h5file = h5py.File(H5_FNAME, 'r')
x = h5file[H5_PATH][:,:]
h5file.close()

# Compute the inverse logit function with respect to the test data
xx = np.einsum('ij,kj->ik', x, test_dist.logL.F)
z = test_dist.logL.inv_logit(xx)

# Compute the likelihood for each of the test observations
y = test_dist.logL.y
ll = y * np.log(z) + (1 - y) * np.log(1 - z)
lik = np.exp(ll)

# Average likelihood for each of test observations
avg_lik = np.mean(lik, axis=0)

# Log posterior predictive check
lppc = np.sum( avg_lik ) / Ntest
print("Held-out-data posterior predictive: %.2f" % lppc)

# Classify
pred = avg_lik > .5
accuracy = np.sum(pred == test_dist.logL.y) / Ntest
print("Accuracy: %.2f" % accuracy)
