import numpy as np
import numpy.random as npr
import pandas as pd

import TransportMaps as TM
import TransportMaps.Distributions as DIST
import TransportMaps.Distributions.Inference as DISTINF
import TransportMaps.Likelihoods as LKL

data_fname = 'data/pd_speech_features.csv'
dist_fname = 'data/dist'
train_size = .8

# subset = 'mfcc'
# ind_vars = slice('mean_Log_energy', 'std_12th_delta_delta')

# subset = 'wavelet'
# ind_vars = slice('Ea', 'app_LT_TKEO_std_10_coef')

subset = 'all'
ind_vars = slice('gender', 'tqwt_kurtosisValue_dec_36')

# Read data (normalize)
df = pd.read_csv(data_fname, skiprows=1)
# Group by id (3 recordings per patient)
df_grp = df.groupby('id')
# Split data in training and test sets
ntrain = int(np.floor(len(df_grp) * train_size))
ids = npr.permutation(len(df_grp))
ids_train = ids[:ntrain]
ids_test = ids[ntrain:]
idxs_train = np.hstack( df_grp.indices[ii] for ii in ids_train )
idxs_test = np.hstack( df_grp.indices[ii] for ii in ids_test )
df_train = df.loc[idxs_train,:]
df_test = df.loc[idxs_test,:]

df_F_train = df_train.loc[:,ind_vars]
F_train = df_F_train.values
y_train = df_train.values[:,-1]

df_F_test = df_test.loc[:,ind_vars]
F_test = df_F_test.values
y_test = df_test.values[:,-1]

# Normalize indipendent variables [0,1]
F = df.loc[:,ind_vars].values
min_F = np.min(F, axis=0)
max_F = np.max(F, axis=0)

nrm_F_train = (F_train - min_F)/(max_F - min_F)
nrm_F_test = (F_test - min_F)/(max_F - min_F)

# Build Distribution (training and test)
dim = F.shape[1] + 1 # number of factors + intercept
ll_train = LKL.LogisticLogLikelihood(y_train, nrm_F_train)
ll_test = LKL.LogisticLogLikelihood(y_test, nrm_F_test)
prior = DIST.StandardNormalDistribution(dim)
pi_train = DISTINF.BayesPosteriorDistribution(ll_train, prior)
pi_test = DISTINF.BayesPosteriorDistribution(ll_test, prior)

# Store distribution
pi_train.store(dist_fname + '-' + subset + '-train.dill')
pi_test.store(dist_fname + '-' + subset +  '-test.dill')
