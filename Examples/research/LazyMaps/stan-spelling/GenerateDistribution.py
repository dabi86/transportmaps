import numpy as np
import pandas as pd

import TransportMaps.Distributions as DIST
import TransportMaps.Distributions.Inference as DISTINF
import TransportMaps.Likelihoods as LKL

data_fname = 'data/stan-spelling.csv'
dist_fname = 'data/dist'

# Read data
df = pd.read_csv(data_fname)
y = df.values[:,2:].T
I = y.shape[0]
J = y.shape[1]

# Assemble the log-likelihood
ll = LKL.TwoParametersLogisticLogLikelihood(y)
dim = ll.dim_in

# Set up prior (standard normal)
prior = DIST.StandardNormalDistribution(dim)

# alpha = DIST.NormalDistribution(.5 * np.ones(I), np.eye(I))
# beta = DIST.NormalDistribution(np.zeros(I), 10**2 * np.eye(I))
# theta = DIST.StandardNormalDistribution(J)

# # Assemble prior
# prior = DIST.FactorizedDistribution(
#     factors = [
#         (alpha, list(range(I)), []),
#         (beta, list(range(I,2*I)), []),
#         (theta, list(range(2*I,2*I+J)), [])
#     ]
# )

# Prepare whitening map
exp_map = Maps.FrozenExponentialDiagonalTransportMap(I)
lin_alpha = Maps.AffineTransportMap(c=.5 * np.ones(I), L=np.eye(I))
alpha_map = Maps.CompositeMap(exp_map, lin_alpha)
beta_map = Maps.AffineTransportMap(c=np.zeros(I), L=10 * np.eye(I))
theta_map = Maps.IdentityTransportMap(J)
whitening_map = Maps.ListStackedTransportMap(
    map_list = [ alpha_map, beta_map, theta_map ],
    active_vars = [ list(range(I)), list(range(I,2*I)), list(range(2*I,2*I+J)) ]
)
# whitening_map = MAPS.IdentityEmbeddedTransportMap(
#     tm   = exp_map,
#     idxs = list(range(I)),
#     dim  = dim
# )

# Whiten the likelihood
white_ll = Maps.CompositeMap(ll, whitening_map)

# # Assemble posterior
# post = DISTINF.BayesPosteriorDistribution(ll, prior)

# Assemble whitened posterior
white_post = DISTINF.BayesPosteriorDistribution(white_ll, prior)

# Store
# post.store(dist_fname + '.dill')
white_post.store(dist_fname + '-white.dill')