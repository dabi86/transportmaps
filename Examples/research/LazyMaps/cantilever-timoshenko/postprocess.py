#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import os.path
import sys, getopt
import dill
import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import dolfin as dol
from tqdm import tqdm

import TransportMaps as TM
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

plt.rcParams.update({'font.size': 14})

nax = np.newaxis

def usage():
    print("""Usage: python postprocess.py -I --input=<filename> --h5path=<path>""")

argv = sys.argv[1:]
IN_FNAME = None
H5PATH = None
INTERACTIVE = False
TITLE = False
try:
    opts, args = getopt.getopt(
        argv, 'hI',
        [
            "input=",
            'h5path='
        ]
    )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)

for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '-I':
        INTERACTIVE = True
    elif opt == '--input':
        IN_FNAME = arg
    elif opt == '--h5path':
        H5PATH = arg
if None in [IN_FNAME, H5PATH]:
    usage()
    sys.exit(2)

with open(IN_FNAME + '.dill', 'rb') as istr:
    stg = dill.load(istr)

f = h5py.File(IN_FNAME + '-post.dill.hdf5', 'r')
try:
    x = f[H5PATH][:,:]
finally:
    f.close()

FIG_DIR = IN_FNAME + '/' + H5PATH
os.makedirs(FIG_DIR, exist_ok=True)
def save_fig(fig, fig_name):
    for fmat in ['svg', 'pdf', 'png']:
        fig.savefig(
            FIG_DIR + '/' + fig_name + '.' + fmat,
            format=fmat, bbox_inches='tight')
    
m = x.shape[0]
pi = stg.target_distribution
solver = pi.solver
coord = solver.aux_coord.flatten()
young = pi.prior_map(x)

if pi.real_observations is None:
    sens_pos = np.asarray(pi.sens_pos_list)
    true_young = np.dot(pi.piecewise_map.L.T, pi.young_field_vals) / np.sum(pi.piecewise_map.L,axis=0)
    # Plot synthetic solution
    u = solver.solve(pi.young_field_vals)
    fig = plt.figure(figsize=(5,4))
    ax = fig.add_subplot(111)
    p = dol.plot(u, linewidth=4)
    for pos in pi.sens_pos_list:
        ax.plot([pos]*2, [0, u([pos])], '-r', linewidth=.5)
        ax.scatter(pos, 0, marker='v', color='r')
    ax.axis('equal')
    ax.apply_aspect()
    ylim = ax.get_ylim()
    ax.plot([0,0], ylim, '--k', linewidth=.25)
    ends = np.linspace(0, 10, pi.piecewise+1)
    for ii, (end, yy) in enumerate(zip(ends[1:],true_young)):
        ax.plot([end]*2, ylim, '--k', linewidth=.25)
        ax.text((ends[ii]+end)/2-(end-ends[ii])/4, 1, '%.0f' % yy)
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    if TITLE:
        plt.title('Synthetic solution')
    plt.xlabel("Position [m]")
    plt.ylabel("Displacement [m]")
    plt.show(False)
    save_fig(fig, "solution-sensors")
    
# Plot map progression with order, rank, variance diagnostic and bound
trH12 = [.5 * p for p in stg.assembler_state.power_list]
fig = plt.figure(figsize=(5,4))
ax = fig.add_subplot(111)
ax.semilogy(trH12, 'ko-', label=r'$\frac{1}{2}\operatorname{Tr}(H_\ell)$')
ax.semilogy(stg.assembler_state.var_diag_list, 'rv-', label=r'$\frac{1}{2}V[\log \rho/T^\sharp\pi]$')
ax.legend()
ax.grid()
ax.set_xlabel(r"Lazy iteration $\ell$")
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
plt.show(False)
save_fig(fig, "convergence")

if pi.piecewise_map.dim_in != solver.aux_ndofs:
    # Piecewise problem
    for i in range(young.shape[1]):
        fig = plt.figure(figsize=(5,4))
        plt.hist(young[:,i], bins=100)
        plt.xlabel("Young modulus [GPa]")
        plt.title("Piece %i" % i)
        plt.show(False)
        save_fig(fig, 'piece-hist-%i' % i)

    # Compute quantiles
    q5 = np.quantile(young, .05, axis=0)
    q10 = np.quantile(young, .1, axis=0)
    q90 = np.quantile(young, .9, axis=0)
    q95 = np.quantile(young, .95, axis=0)
    mean = np.mean(young, axis=0)

    # Span quantile among pieces
    q5 = pi.piecewise_map.evaluate(q5[nax,:])[0,:]
    q10 = pi.piecewise_map.evaluate(q10[nax,:])[0,:]
    q90= pi.piecewise_map.evaluate(q90[nax,:])[0,:]
    q95 = pi.piecewise_map.evaluate(q95[nax,:])[0,:]
    mean = pi.piecewise_map.evaluate(mean[nax,:])[0,:]

    # Plot against synthetic
    fig = plt.figure(figsize=(5,4))
    plt.plot(coord, pi.young_field_vals, 'r', linewidth=2, label="Synthetic")
    plt.plot(coord, mean, 'k--', linewidth=2, label="Posterior mean")
    plt.plot(coord, q5, 'k', linewidth=.5, label="5,10,90,95 percentiles")
    plt.plot(coord, q10, 'k', linewidth=.5)
    plt.plot(coord, q90, 'k', linewidth=.5)
    plt.plot(coord, q95, 'k', linewidth=.5)
    plt.ylabel("Young modulus [GPa]")
    plt.xlabel("Position [m]")
    # plt.legend()
    plt.show(False)
    save_fig(fig, 'field-distr-vs-synthetic')
    
    # Plot the 2d-marginals
    SKIP = 10
    plt.rcParams.update({'font.size': 20})
    fig, handles = DIAG.plotAlignedMarginals(
        young[::SKIP,:], scatter=True, title=None,
        vartitles=[r'$E_{%i}$' % (i+1) for i in range(pi.piecewise)],
        ret_handles=True, show_flag=False
    )
    # Add true values
    axarr = handles['axarr']
    nplots = axarr.shape[0]
    for ii in range(nplots): # 2d-plots
        for jj in range(ii+1):
            if jj < ii:
                ax = axarr[ii,jj]
                ax.scatter(true_young[jj], true_young[ii],
                           s=20, marker='s', color='r')
    for ii in range(nplots): # 1d-plots
        ax = axarr[ii,ii]
        ylim = ax.get_ylim()
        ax.plot([true_young[ii]]*2, ylim, 'r-', linewidth=2)
    plt.show(False)
    save_fig(fig, 'aligned-marginals')
    SKIP = 1
    plt.rcParams.update({'font.size': 14})
    
    ################################################
    # Compute posterior predictive (whole field)
    young_fields = pi.piecewise_map.evaluate(young[::SKIP,:])
    young_fields = np.asarray(young_fields, order='C')
    displ = np.zeros((young_fields.shape[0], solver.aux_ndofs))
    print('Compute posterior predictive displacement')
    for i in tqdm(range(young_fields.shape[0])):
        u = solver.solve( young_fields[i,:] )
        for j in range(solver.aux_ndofs):
            displ[i,j] = u( [coord[j]] )

    # Compute displacement for true field
    ustar = solver.solve( pi.young_field_vals )
    displ_star = np.zeros(solver.aux_ndofs)
    for j in range(solver.aux_ndofs):
        displ_star[j] = ustar( [coord[j]] )

    # Plot the mismatch between posterior predicitve and sythetic
    mismatch = displ_star - displ
    q5 = np.quantile(mismatch, .05, axis=0)
    q10 = np.quantile(mismatch, .1, axis=0)
    q90 = np.quantile(mismatch, .9, axis=0)
    q95 = np.quantile(mismatch, .95, axis=0)
    mean = np.mean(mismatch, axis=0)

    fig = plt.figure(figsize=(5,4))
    plt.plot(coord, np.zeros(coord.shape), 'r', linewidth=2)
    plt.plot(coord, mean, 'k--', linewidth=2)
    plt.plot(coord, q5, 'k', linewidth=.5)
    plt.plot(coord, q10, 'k', linewidth=.5)
    plt.plot(coord, q90, 'k', linewidth=.5)
    plt.plot(coord, q95, 'k', linewidth=.5)
    plt.ylabel("Displacement mismatch [m]")
    plt.xlabel("Position [m]")
    plt.show(False)
    save_fig(fig, 'posterior-predictive-mismatch')

    ################################################
    # Compute posterior predictive (observations)
    young_fields = pi.piecewise_map.evaluate(young[::SKIP,:])
    young_fields = np.asarray(young_fields, order='C')
    displ = np.zeros((young_fields.shape[0], len(solver.obs_list)))
    print('Compute posterior predictive observations')
    for i in tqdm(range(young_fields.shape[0])):
        displ[i,:] = solver.observe( young_fields[i,:] )

    # Compute displacement for true field
    displ_star = solver.observe( pi.young_field_vals )

    # Plot the mismatch between posterior predicitve and sythetic
    mismatch = displ_star - displ
    fig = plt.figure(figsize=(5,4))
    ax = fig.add_subplot(111)
    ax.boxplot(
        mismatch,
        positions=pi.sens_pos_list,
        widths=0.25,
        flierprops=dict(
            marker='o', markerfacecolor='k', markersize=1.5,
            linestyle='none', markeredgecolor='k'
        )
    )
    for label in ax.get_xticklabels():
        label.set_visible(False)
    ax.get_xticklabels()[0].set_visible(True)
    for label in ax.get_xticklabels()[3::4]:
        label.set_visible(True)
    ax.ticklabel_format(
        axis='y',
        style='sci',
        scilimits=(0,0)
    )
    ax.set_ylabel("Displacement mismatch [m]")
    ax.set_xlabel("Position [m]")
    plt.show(False)
    save_fig(fig, 'posterior-predictive-obs-mismatch')

    # Plot the mismatch ratio between posterior predicitve and sythetic
    mismatch = (displ_star - displ) / np.abs(displ_star)
    fig = plt.figure(figsize=(5,4))
    ax = fig.add_subplot(111)
    ax.boxplot(
        mismatch,
        positions=pi.sens_pos_list,
        widths=0.25,
        flierprops=dict(
            marker='o', markerfacecolor='k', markersize=1.5,
            linestyle='none', markeredgecolor='k'
        )
    )
    for label in ax.get_xticklabels():
        label.set_visible(False)
    ax.get_xticklabels()[0].set_visible(True)
    for label in ax.get_xticklabels()[3::4]:
        label.set_visible(True)
    ax.ticklabel_format(
        axis='y',
        style='sci',
        scilimits=(0,0)
    )
    ax.set_ylabel("Displacement mismatch ratio")
    ax.set_xlabel("Position [m]")
    plt.show(False)
    save_fig(fig, 'posterior-predictive-obs-mismatch-ratio')
    
if INTERACTIVE:
    from IPython import embed
    embed()
