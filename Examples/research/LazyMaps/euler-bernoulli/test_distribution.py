import numpy as np

import TransportMaps as TM
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Distributions as DIST
import src.TransportMaps.Maps as MAPS
import TransportMaps.FiniteDifference as FD

import EulerBernoulliDistributions as EBD

TM.setLogLevel(20)

nInt = 3
length = 1
radius = 0.1

inputs = np.array([
    -6.27806575, -6.2230957 , -6.28984623, -6.31550654, -6.30096829,
    -6.24224779, -6.15124141, -6.05080126, -5.97933971, -5.93969198,
    -5.90410886, -5.79876914, -5.67852756, -5.50306108, -5.42762107,
    -5.41595502, -5.3843819 , -5.36153521, -5.28782507, -5.20536196,
    -5.18802121, -5.26312799, -5.3033814 , -5.34768516, -5.32256947,
    -5.22803202, -5.14520256, -5.14551224, -5.06832866, -4.98474208,
    -4.85633211])
nNodes = 31

pi = EBD.EulerBernoulliDistribution(
    nNodes, nInt, length, radius, inputs)

# DIAG.plotAlignedConditionals( pi, range_vec=[[1000, 10000]]*nInt )

# # Normalize the prior
# exp = pi.prior.transport_map
# pi.prior = pi.prior.base_distribution
# pi.logL.T = MAPS.CompositeMap( pi.logL.T, exp )

# Test gradient with finite difference
x = pi.prior.rvs(1)
dx = 1e-4
FD.check_grad_x( pi.log_pdf, pi.grad_x_log_pdf, x, dx)

# Laplace approximation
lap = TM.laplace_approximation(pi, ders=1, hess_approx='fd')
