#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import numpy as np
import numpy.linalg as npla
from scipy.spatial.distance import pdist, squareform

import TransportMaps.Distributions as DIST
import TransportMaps.Distributions.Inference as DISTINF
import TransportMaps.Likelihoods as LKL
from TransportMaps import counted

__all__ = [
    'Covariance',
    'OrnsteinUhlenbeckCovariance',
    'SquaredExponentialCovariance',
    'IdentityCovariance',
    'EulerBernoulliDistribution'
]           

nax = np.newaxis

class Covariance(object):
    def __call__(self, coord):
        dists = squareform( pdist(coord) )
        return self._evaluate( dists )
    def _evaluate(self, dists):
        raise NotImplementedError("To be implemented in subclasses")

class OrnsteinUhlenbeckCovariance(Covariance):
    def __init__(self, var, l):
        self.var = var
        self.l = l
    def _evaluate(self, dists):
        return self.var * np.exp( - dists / self.l )

class SquaredExponentialCovariance(Covariance):
    def __init__(self, var, l):
        self.var = var
        self.l = l
    def _evaluate(self, dists):
        d0 = np.isclose(dists, 0.)
        dn0 = np.logical_not(d0)
        d0 = np.where(d0)
        dn0 = np.where(dn0)
        out = np.zeros(dists.shape)
        out[d0] = self.var + 1e-13
        out[dn0] = self.var * np.exp( - dists[dn0]**2 / self.l**2 / 2 )
        return out

class IdentityCovariance(Covariance):
    def __init__(self, var):
        self.var = var
    def _evaluate(self, dists):
        return self.var * np.eye( len(dists) )

class EulerBernoulliDistribution(DISTINF.BayesPosteriorDistribution):
    def __init__(
            self,
            # Problem setting
            nNodes, nInt, length, radius, inputs,
            # Prior setting
            prior_mu=10., prior_sig2=4.,
            prior_cov=IdentityCovariance,
            prior_cov_kwargs = {},
            # Likelihood settings
            lkl_sig2=1e-4,
            lkl_cov = IdentityCovariance,
            lkl_cov_kwargs = {},
            # Observations
            observations=None):

        # Build the field to data map
        self.field_to_data_map = EulerBernoulliFieldToDataMap(
            nNodes, nInt, length, radius, inputs)

        # Build the prior
        mean = prior_mu * np.ones( nInt )
        cov_fun = prior_cov( prior_sig2, **prior_cov_kwargs )
        cov = cov_fun( self.field_to_data_map.centPtsInt[:,nax] )
        prior = DIST.PushForwardTransportMapDistribution(
            Maps.FrozenExponentialDiagonalTransportMap(nInt),
            DIST.NormalDistribution(mean, cov) )

        # Create the observations if missing
        if observations:
            self.real_observations = True
        else:
            self.real_observations = False
            self.generating_modulus = prior.rvs(1)[0,:]
            observations = self.field_to_data_map(self.generating_modulus[nax,:])[0,:]

        # Build the likelkihoods
        cov_fun = lkl_cov( lkl_sig2, **lkl_cov_kwargs )
        cov = cov_fun( self.field_to_data_map.endPts[1:,nax] )
        pi_noise = DIST.NormalDistribution(
            np.zeros( nNodes-1 ), cov )
        if not self.real_observations:
            observations += pi_noise.rvs(1)[0,:]
        lkl = LKL.AdditiveLogLikelihood(observations, pi_noise, self.field_to_data_map)

        super(EulerBernoulliDistribution, self).__init__(lkl, prior)

class EulerBernoulliFieldToDataMap(Maps.Map):
    def __init__(self, nNodes, nInt, length, radius, inputs):
        if len(inputs) != nNodes:
            raise ValueError(
                "The inputs must contain values for each discretization node")
        self.nNodes = nNodes
        self.length = length
        self.nInt = nInt
        self.radius = radius
        self.dx = length/nNodes
        self.I = np.pi/4.0*radius**4
        self.inputs = inputs
        self.endPts = np.linspace(0, length, nNodes)
        self.endPtsInt = np.linspace(0, length, nInt+1)
        self.centPtsInt = (self.endPtsInt[:-1] + self.endPtsInt[1:]) / 2.

        super(EulerBernoulliFieldToDataMap, self).__init__(
            self.nInt, self.nNodes-1)
        
    def _build_K(self, m):
        # Number of nodes in the finite difference stencil
        n = self.nNodes

        # Map modulus to map discretization
        modulus = np.zeros(n)
        jInt = 0
        for i in range(n-1):
            if self.endPts[i] >= self.endPtsInt[jInt]:
                jInt += 1
            modulus[i] = m[jInt-1]
        modulus[-1] = m[-1]
        
        # Create stiffness matrix
        K = np.zeros((n, n))

        # Build stiffness matrix (center)
        for i in range(2, n-2):
            K[i,i+2] = modulus[i]
            K[i,i+1] = modulus[i+1] - 6.0*modulus[i] + modulus[i-1]
            K[i,i]   = -2.0*modulus[i+1] + 10.0*modulus[i] - 2.0*modulus[i-1]
            K[i,i-1] = modulus[i+1] - 6.0*modulus[i] + modulus[i-1]
            K[i,i-2] = modulus[i]

        # Set row i == 1
        K[1,3] = modulus[1]
        K[1,2] = modulus[2] - 6.0*modulus[1] + modulus[0]
        K[1,1] = -2.0*modulus[2] + 11.0*modulus[1] - 2.0*modulus[0]

        # Set row i == n - 2
        K[n-2,n-1] = modulus[n-1] - 4.0*modulus[n-2] + modulus[n-3]
        K[n-2,n-2] = -2.0*modulus[n-1] + 9.0*modulus[n-2] - 2.0*modulus[n-3]
        K[n-2,n-3] = modulus[n-1] - 6.0*modulus[n-2] + modulus[n-3]
        K[n-2,n-4] = modulus[n-2]

        # Set row i == n - 1 (last row)
        K[n-1,n-1] = 2.0*modulus[n-1]
        K[n-1,n-2] = -4.0*modulus[n-1]
        K[n-1,n-3] = 2.0*modulus[n-1]

        # Apply dirichlet BC (w=0 at x=0)
        K[0,:] = 0.0; K[:,0] = 0.0
        K[0,0] = 1

        return K/self.dx**4

    @counted
    def evaluate(self, x, *args, **kwargs):
        m = x.shape[0]
        displacement = np.zeros( (m, self.dim_out) )
        # Prepare load and apply bc
        load = self.inputs / self.I
        load[0] = 0.
        for i in range(m):
            K = self._build_K( x[i,:] )
            # Solve
            displacement[i,:] = npla.solve(K, load)[1:]
        return displacement

    @counted
    def grad_x(self, x, *args, **kwargs):
        m = x.shape[0]
        gx_disp = np.zeros( (m, self.dim_out, self.dim_in) )
        # Prepare load and apply bc
        load = self.inputs / self.I
        load[0] = 0.
        for i in range(m):
            # Solve for displacement
            K = self._build_K( x[i,:] )
            disp = npla.solve(K, load)
            # Solve for adjoint
            rhs = np.zeros( (self.nNodes, self.dim_in) )
            for j in range(self.dim_in):
                xj = np.zeros(x.shape[1])
                xj[j] = 1. # x[i,j]
                Kj = self._build_K( xj )
                rhs[:,j] = np.dot(Kj, disp)
            gx_disp[i,:,:] = - npla.solve(K, rhs)[1:,:]
        return gx_disp
        