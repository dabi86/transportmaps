import sys, getopt
import dill
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import numpy.random as npr
import dolfin as dol
import itertools
import h5py

import src.TransportMaps.Maps as MAPS

plt.rcParams.update({'font.size': 14})

def store_figure(fig, fname):
    for fmat in STORE_FIG_FMATS:
        fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight');

def usage():
    print("""python visualize_distribution.py
  --input=<filename> --postprocess-data=<filename>
  --store-fig-dir=<dir> --no-plotting --no-title""")
    
argv = sys.argv[1:]
IN_FNAME = None
POST_FNAME = None
STORE_FIG_DIR = None
STORE_FIG_FMATS = ['svg', 'pdf', 'png']
PLOT = True
TITLE = True
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "input=",
            "postprocess-data=",
            "store-fig-dir=",
            "no-plotting",
            "no-title"
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
    elif opt == '--postprocess-data':
        POST_FNAME = arg
    elif opt == "--store-fig-dir":
        STORE_FIG_DIR = arg
    elif opt == "--no-plotting":
        PLOT = False
    elif opt == "--no-title":
        TITLE = False
    else:
        raise AttributeError("Unknown option %s" % opt)

with open(IN_FNAME, 'rb') as istr:
    stg = dill.load(istr)

# Extract distribution and whitening map
pi = stg.target_distribution
if hasattr(pi, 'whitening_map'):
    W = pi.whitening_map
    EXP = W.t1
    EIG = W.t2
elif hasattr(pi.logL, 'T') and isinstance(pi.logL.T, Maps.CompositeMap):
    W = pi.logL.T.tm_list[1]
else:
    W = Maps.IdentityTransportMap(pi.dim)

# Plot convergence rate
var_diag_list = stg.assembler_state.var_diag_list
power_list = [.5 * p for p in stg.assembler_state.power_list ]
fig = plt.figure(figsize=(5,4))
ax = fig.add_subplot(111)
ax.semilogy( power_list, 'o-k',
              label=r'$\frac{1}{2}\operatorname{Tr}(H)$' )
ax.semilogy( var_diag_list, 'o-r',
              label=r'$\frac{1}{2}V[\log \rho/T^\sharp\pi]$')
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.grid()
ax.legend()
ax.set_xlabel("Lazy iteration")
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/convergence' )
    
# Retrieve sensor locations
sens_pos = np.asarray(pi.sens_pos_list)

# Load postprocess data
f = h5py.File(POST_FNAME + '.hdf5', 'r')
samps = f['/quadrature/approx-target/0'][:,:]

# Transform samples
eig_samps = EIG.evaluate(samps)
exp_samps = EXP.evaluate(eig_samps)

# Plot a couple of samples from MC quadratures
NSAMPS = 4
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(5,4))
for i, ax in enumerate(axes.ravel().tolist()):
    plt.sca(ax)
    p = dol.plot( pi.solver.dof_to_fun( np.asarray(exp_samps[i,:],order='C') ) )
    for c in p.collections:
        c.set_edgecolor("face")
    ax.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=1)
    ax.tick_params(
        axis='both',        # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        left=False,
        right=False,
        labelbottom=False,
        labelleft=False,
    )    
fig.colorbar(p, ax=axes.ravel().tolist())
# if TITLE:
#     plt.title("Sample %d" % i)
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/exp-samples' )

# Plot a couple of samples from MC quadratures
NSAMPS = 4
fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(5,4))
for i, ax in enumerate(axes.ravel().tolist()):
    plt.sca(ax)
    p = dol.plot( pi.solver.dof_to_fun( np.asarray(eig_samps[i,:],order='C') ) )
    for c in p.collections:
        c.set_edgecolor("face")
    ax.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=1)
    ax.tick_params(
        axis='both',        # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        left=False,
        right=False,
        labelbottom=False,
        labelleft=False,
    )    
fig.colorbar(p, ax=axes.ravel().tolist(), ticks=MaxNLocator(integer=True))
# if TITLE:
#     plt.title("Sample %d" % i)
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/eig-samples' )
                    
# Plot GP mean
mean = np.asarray( np.mean(eig_samps, axis=0), order='C')        
# mean = np.ascontiguousarray( np.mean( samps, axis=0 ) )
fig = plt.figure(figsize=(5,4))
p = dol.plot( pi.solver.dof_to_fun(mean) )
for c in p.collections:
    c.set_edgecolor("face")
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.colorbar(p, ticks=MaxNLocator(integer=True))
plt.tick_params(
    axis='both',        # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False,
) 
if TITLE:
    plt.title("GP Mean")
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/eig-mean' )

# Plot standard deviation
std = np.std( eig_samps, axis=0 )
fig = plt.figure(figsize=(5,4))
p = dol.plot( pi.solver.dof_to_fun( std ) )
for c in p.collections:
    c.set_edgecolor("face")
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.colorbar(p, ticks=MaxNLocator(steps=[1,2,10]))
plt.tick_params(
    axis='both',        # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False,
) 
if TITLE:
    plt.title("Standard deviation")
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/eig-std' )
    
# Plot transformed mean
fig = plt.figure(figsize=(5,4))
mean = np.asarray( np.mean(exp_samps, axis=0), order='C')
p = dol.plot( pi.solver.dof_to_fun(mean), levels=50 )
for c in p.collections:
    c.set_edgecolor("face")
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.colorbar(p, ticks=MaxNLocator(integer=True))
plt.tick_params(
    axis='both',        # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False,
) 
if TITLE:
    plt.title("Mean")
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/exp-mean' )

# Plot transformed standard deviation
fig = plt.figure(figsize=(5,4))
std = np.asarray( np.std(exp_samps, axis=0), order='C')
p = dol.plot( pi.solver.dof_to_fun(std), levels=50 )
for c in p.collections:
    c.set_edgecolor("face")
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.colorbar(p, ticks=MaxNLocator(integer=True))
plt.tick_params(
    axis='both',        # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False,
) 
if TITLE:
    plt.title("Standard deviation")
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/exp-std' )

