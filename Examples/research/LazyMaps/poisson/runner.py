import sys
import dill
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import matplotlib as mpl
import matplotlib.pyplot as plt
import TransportMaps as TM
from TransportMaps.Misc import cmdinput
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DIST
import TransportMaps.Diagnostics as DIAG

from TransportMaps.Algorithms.DeepLazyMaps import *

npr.seed(1)

NPROCS = 8
TM.setLogLevel(20)

fname = 'data/square/ndiscr20/dist-PullPrior-eig.dill'
with open(fname,'rb') as istr:
    pi = dill.load(istr)

# # Visualize some posteriors to see get an idea of the particular setting
# fig = DIAG.plotRandomConditionals(
#     pi, numPointsXax=50)

#############################
# Approximation by LazyMaps #
#############################

plt.pause(0.1)

eps = 1e-3
maxit = 20

k_eps = 1e-3 #1e-3
k_max_rank = 4 #4 #3
k_qtype=0
k_qparams=20

ld_qtype=0#3
ld_qparams=0#[30]

map_ord = 2
map_qtype = 0
map_qparams = 1000
map_reg = {
    'type': 'L2',
    'alpha': 1e-2
}
map_maxit = 1000

var_qtype=0
var_qparams=1000

if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)
else:
    mpi_pool = None

try:
    T, var_diag = deep_lazy_map_construction(
        pi,
        # Overall convergence criterion
        eps=eps, maxit=maxit,
        # Low-rank approximation parameters
        k_max_rank=k_max_rank, k_eps=k_eps,
        k_qtype=k_qtype,
        k_qparams=k_qparams,
        # Low-dimensional target parameters
        ld_qtype=ld_qtype, ld_qparams=ld_qparams,
        # Map approximation parameters
        hard_truncation=False,
        map_ord=map_ord, map_btype='poly',
        map_qtype=map_qtype, map_qparams=map_qparams,
        map_reg=map_reg, map_maxit=1000,
        # Variance diagnostic parameters
        var_qtype=var_qtype, var_qparams=var_qparams,
        # KL min tolerance
        tol=1.,
        # Parallelization
        mpi_pool=mpi_pool,
        # Plotting options
        plotting=True)
finally:
    if mpi_pool:
        mpi_pool.stop()

