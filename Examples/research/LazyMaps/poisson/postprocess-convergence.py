import sys, getopt
import dill
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import numpy.random as npr
import dolfin as dol
import itertools
import h5py

import src.TransportMaps.Maps as MAPS

plt.rcParams.update({'font.size': 14})

def store_figure(fig, fname):
    for fmat in STORE_FIG_FMATS:
        fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight');

def usage():
    print("""python visualize_distribution.py
  --input=<filename> --postprocess-data=<filename>
  --store-fig-dir=<dir> --no-plotting --no-title""")
    
argv = sys.argv[1:]
IN_FNAME = None
POST_FNAME = None
STORE_FIG_DIR = None
STORE_FIG_FMATS = ['svg', 'pdf', 'png']
PLOT = True
TITLE = True
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "input=",
            "postprocess-data=",
            "store-fig-dir=",
            "no-plotting",
            "no-title"
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
    elif opt == '--postprocess-data':
        POST_FNAME = arg
    elif opt == "--store-fig-dir":
        STORE_FIG_DIR = arg
    elif opt == "--no-plotting":
        PLOT = False
    elif opt == "--no-title":
        TITLE = False
    else:
        raise AttributeError("Unknown option %s" % opt)

with open(IN_FNAME, 'rb') as istr:
    stg = dill.load(istr)

# Plot convergence rate
var_diag_list = stg.assembler_state.var_diag_list
power_list = [.5 * p for p in stg.assembler_state.power_list ]
fig = plt.figure(figsize=(5,4))
ax = fig.add_subplot(111)
ax.semilogy( power_list, 'o-k',
             label=r'$\frac{1}{2}\operatorname{Tr}(H_\ell)$' )
ax.semilogy( var_diag_list, 'v-r',
             label=r'$\frac{1}{2}V[\log \rho/T^\sharp\pi]$')
ax.xaxis.set_major_locator(MaxNLocator(integer=True))
ax.grid()
ax.legend()
ax.set_xlabel(r"Lazy iteration $\ell$")
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/convergence' )
