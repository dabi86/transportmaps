import sys, getopt
import dill
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
import numpy.random as npr
import dolfin as dol
import itertools

plt.rcParams.update({'font.size': 14})

def usage():
    print(
        """python visualize_distribution.py
  --input=<filename>
  --store-fig-dir=<dirname> --no-plotting""")

argv = sys.argv[1:]
IN_FNAME = None
STORE_FIG_DIR = None
STORE_FIG_FMATS = ['svg', 'pdf','png']
PLOT = True
TITLES = False
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "input=",
            "store-fig-dir=",
            "no-plotting"
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
    elif opt == "--store-fig-dir":
        STORE_FIG_DIR = arg
    elif opt == "--no-plotting":
        PLOT = False
    else:
        raise AttributeError("Unknown option %s" % opt)

def store_figure(fig, fname):
    for fmat in STORE_FIG_FMATS:
        fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight');
        
with open(IN_FNAME, 'rb') as istr:
    pi = dill.load(istr)

# Plot mesh
fig = plt.figure(figsize=(5,4))
dol.plot(pi.solver.mesh)
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/mesh' )

# Retrieve sensor locations
sens_pos = np.asarray(pi.sens_pos_list)

# Plot generating field and sensors
dl = 0 # 1e-4
# vmin = np.min(pi.field_vals)
vmin = 0
vmax = np.max(pi.field_vals)
levels = np.linspace((1-dl)*vmin, (1+dl)*vmax, 50)
fig = plt.figure(figsize=(5,4))
p = dol.plot(pi.true_field, levels=levels)
for c in p.collections:
    c.set_edgecolor("face")
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.tick_params(
    axis='both',        # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False,
) 
cbar = plt.colorbar(p, ticks=[0,1])
if TITLES:
    plt.title('Synthetic field')
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/synthetic-field' )

# Plot log-generating field and sensors
dl = 0 # 1e-4
# vmin = np.min(pi.field_vals)
log_field = np.log(pi.field_vals)
vmin = np.min(log_field)
vmax = np.max(log_field)
levels = np.linspace((1-dl)*vmin, (1+dl)*vmax, 50)
fig = plt.figure(figsize=(5,4))
p = dol.plot(pi.solver.dof_to_fun(log_field), levels=levels)
for c in p.collections:
    c.set_edgecolor("face")
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.tick_params(
    axis='both',        # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False,
) 
cbar = plt.colorbar(p, ticks=MaxNLocator(integer=True))
if TITLES:
    plt.title(r'Synthetic field $\log\kappa$')
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/synthetic-field-log-kappa' )


# Plot Reference solution and sensors
u = pi.solver.solve(pi.true_field)
fig = plt.figure(figsize=(5,4))
p = dol.plot(u)
for c in p.collections:
    c.set_edgecolor("face")
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.tick_params(
    axis='both',        # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False,
)
cbar = plt.colorbar(p, ticks=MaxNLocator(integer=True))
if TITLES:
    plt.title('Synthetic solution')
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/synthetic-solution' )


# Plot observations
pointwise_obs = [u(pos) for pos in sens_pos]
noise_free_obs = pi.field_to_data_map.evaluate(
    pi.field_vals[np.newaxis,:])[0,:]
obs = pi.logL.y
fig = plt.figure(figsize=(5,4))
plt.plot(pointwise_obs, 'go', label='pointwise')
plt.plot(noise_free_obs, 'ko', label='w/o noise')
plt.plot(obs, 'ro', label='with noise')
# plt.ylim(0., 1.)
plt.legend(loc='best')
if PLOT:
    plt.show(False)
if STORE_FIG_DIR is not None:
    store_figure(
        fig, STORE_FIG_DIR + '/observations' )
