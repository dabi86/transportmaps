#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

#
# USAGE
# 
#

import dill
import TransportMaps.Densities as DENS
import MapGeneration as MG

def load(fname):
    """
    Load data and returns:
      * ``d``: the dimension of the density
      * ``f``: \pi
      * ``log_f``: \log\pi
      * ``grad_log_f``: \nabla\log\pi
      * ``T``: approximate transport map
      * ``pull_f``: pullback of \pi under the transport map (T_\sharp^{-1} \pi)

    In particular:
      * ``f``, ``log_f``, ``pull_f`` take an (m,d) array and return an (m) array
      * ``grad_log_f`` takes an (m,d) array and returns an (m,d) array
      * ``T`` takes an (m,d) array and returns an (m,d) array
    Args:
      fname (str): filename

    Returns:
      (d, f, log_f, grad_log_f, T, pull_f) (functions): functions described above.
    """
    with open(fname,'rb') as in_stream:
        data = dill.load(in_stream)

    tm_approx_comp_list = data['tm_approx_comp_list']
    tar_dens = data['target_density']
    ref_dens = DENS.StandardNormalDensity(tar_dens.dim)
    T = MG.compose_Markovian_tm(tm_approx_comp_list)
    pull_tar = MG.compose_Markovian_pullback(tm_approx_comp_list, tar_dens)

    # Functions returned
    d = tar_dens.dim
    f = tar_dens.pdf
    log_f = tar_dens.log_pdf
    grad_log_f = tar_dens.grad_x_log_pdf
    pull_f = pull_tar.pdf
    
    return (d, f, log_f, grad_log_f, T, pull_f)

def test_load(d, f, log_f, grad_log_f, T, pull_f):
    import numpy as np
    import scipy.stats as stats
    ref_samps = stats.norm().rvs(10*d).reshape((10,d))
    approx_tar_samps = T(ref_samps)
    tar_pdf = f(approx_tar_samps)
    tar_log_pdf = log_f(approx_tar_samps)
    grad_log_f = grad_log_f(approx_tar_samps)
    pull_pdf = pull_f(ref_samps)
