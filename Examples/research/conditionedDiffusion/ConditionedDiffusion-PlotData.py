#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import numpy as np
import matplotlib.pyplot as plt
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS

def usage():
    print("ConditionedDiffusion-PlotData.py -i <file_name>")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
try:
    opts, args = getopt.getopt(argv,"hi:",["input="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-i", "--input"):
        IN_FNAME = arg
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME, 'rb') as in_stream:
    d = dill.load(in_stream)

fig = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{t}$",
                    title='Observations')
ax = fig.gca()
ax.set_aspect('equal')
ax.set_ylim(-2,2)
ax.set_xlim(0,d.time_vec[-1])
ax.plot(d.time_vec_proc, d.Xt, '--k')
ax.plot(d.time_vec[d.indexObs], d.dataObs, 'ro')
plt.show(False)