#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import numpy as np
import matplotlib.pyplot as plt
import TransportMaps.Diagnostics as DIAG

import ConditionedDiffusionDensities as CDDENS


# Conditioned diffusion model
#
# COMMENTS
# Inputs are:
#  * num_steps: the number of steps at which to discretize (60)
#  * sigma: the noise
#  * final_time: the final time
#

def usage():
    print('ConditionedDiffusion-Preprocessing.py -d <num_steps> ')

def full_usage():
    usage()
    print("Example: ConditionedDiffusion-Preprocessing.py -d 50")

argv = sys.argv[1:]
NSTEPS = None
SIGMA = 0.1
TFINAL = 10.
try:
    opts, args = getopt.getopt(argv,"hd:",["nsteps=","sigma=","tfinal="])
except getopt.GetoptError as e:
    full_usage()
    print(e)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-d", "--nsteps"):
        NSTEPS = int(arg)
    elif opt in ("--sigma"):
        SIGMA = float(arg)
    elif opt in ("--tfinal"):
        TFINAL = float(arg)
    else:
        raise RuntimeError("Input option %s not recognized." % opt)

if  None in [NSTEPS]:
    full_usage()
    sys.exit(3)
    
################ Define density ####################
# # TC DATA
# dataObs = np.array([.8, 0., -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
#                     -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20  ])
# Modified TC DATA
dataObs = np.array([ -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
                    -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20, 1.1, 1.05  ])
# dataObs = np.array([1., 1.21, 1.025, 0.98, 1.05, .907 , 1.023,  1.05, .99, .90,
#                     1.05 , .956, 1.067,  .9, .850,  .65,  -.9, -1., -0.92 , -1.05  ])
# # Early transition
# dataObs = np.array([1., 1.05, -.85, -1.0,  -.95,  -.9, -1., -0.92 , -1.05, -1.025,
#                     -0.98, -1.05, -.907 , -1.023,  -1.05, -.99, -.90,
#                     -1.05 , -.956, -1.067    ])
# # NO TRANSITION
# dataObs = np.array([1.,    1.21,  1.025, 0.98, 1.05,  
#                      .907, 1.023, 1.05,   .99,  .90,
#                     1.05 ,  .956, 1.067,  .95, 1.02,
#                     1.1,   1.0,    .98,   .92, 1.05  ])
target_density = CDDENS.ConditionedDiffusion(
    NSTEPS, sigma=SIGMA, tfinal=TFINAL, dataObs = dataObs)

fig = DIAG.nicePlot([],[], xlabel="t", ylabel="$\pi_{t}$",
                    title='Distribution')
ax = fig.gca()
ax.set_aspect('equal')
ax.set_ylim(-2,2)
ax.set_xlim(0,10)
ax.plot(target_density.timeObs, target_density.dataObs, 'or')
plt.show(False)

DIAG.plotAlignedConditionals(target_density, numPointsXax=100,
                             dimensions_vec=range(38,47), range_vec=[-2,2])