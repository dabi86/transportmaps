#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import numpy as np
import matplotlib.pyplot as plt
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS

import MapGeneration as MG

def usage():
    print("ConditionedDiffusion-Composition-Postprocess.py -i <file_name>")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
NMC = 10000
try:
    opts, args = getopt.getopt(argv,"hi:",["input=", "nmc="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-i", "--input"):
        IN_FNAME = arg
    elif opt in "--nmc":
        NMC = int(arg)
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME, 'rb') as in_stream:
    data = dill.load(in_stream)

# Resetore data
target_density = data['target_density']
laplace_id = data['laplace_id']
laplace_tm = data['laplace_tm']
base_density = DENS.StandardNormalDensity(target_density.dim)

# # Construct T^\sharp \pi_{tar}
# pull_tar_lap_id = DENS.PullBackTransportMapDensity(laplace_id, target_density)

# print("Conditional Random Id")
# DIAG.plotRandomConditionals(pull_tar_lap_id, num_conditionalsXax=10, numPointsXax = 10,
#                             range_vec=[-10,10], title='Laplace Id')
# DIAG.plotAlignedConditionals(pull_tar_lap_id, numPointsXax = 10, range_vec=[-10,10], 
#                              dimensions_vec=range(20), title='Laplace Id - First 20')
# DIAG.plotAlignedConditionals(pull_tar_lap_id, numPointsXax = 10, range_vec=[-10,10], 
#                              dimensions_vec=range(40,60), title='Laplace Id - Last 20')
# var_diag_id = DIAG.variance_approx_kl(base_density, pull_tar_lap_id,
#                                       qtype=0, qparams=NMC)
# print("Variance Diagnostic Laplace Id: %e" % var_diag_id)

# Construct T^\sharp \pi_{tar}
pull_tar_lap_tm = DENS.PullBackTransportMapDensity(laplace_tm, target_density)

print("Conditional Random Laplace TM")
DIAG.plotRandomConditionals(pull_tar_lap_tm, num_conditionalsXax=10, numPointsXax = 10,
                            range_vec=[-10,10], title='Laplace TM')
# fig = plt.figure(figsize=(60,60))
DIAG.plotAlignedConditionals(pull_tar_lap_tm, numPointsXax = 40, range_vec=[-8,8],
                             dimensions_vec=[1,21,22,24,25,42,43],
                             title='Conditionals pullback linear map') #, fig=fig)
# DIAG.plotAlignedConditionals(pull_tar_lap_tm, numPointsXax = 10,
#                              dimensions_vec=range(40,60), title='Laplace TM - Last 20')
var_diag_tm = DIAG.variance_approx_kl(base_density, pull_tar_lap_tm,
                                      qtype=0, qparams=NMC)
print("Variance Diagnostic Laplace TM: %e" % var_diag_tm)

# Mean
plt.figure()
plt.plot(target_density.time_vec_proc, target_density.Xt, '--k')
plt.plot(target_density.time_vec[target_density.indexObs], target_density.dataObs,
         'ro')
plt.plot(target_density.time_vec, laplace_tm.constantTerm,'-')
plt.show(False)

# # Plot conditionals target at transition
# DIAG.plotAlignedConditionals(target_density, numPointsXax=200,
#                              dimensions_vec=range(38,47), range_vec=[-6,6],
#                              pointEval=laplace_id.constantTerm)