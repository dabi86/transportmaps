#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import warnings
import numpy as np
import scipy.stats as stats
import TransportMaps as TM
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG

import FiniteDifferenceDiagnostics as FDD
import ConditionedDiffusionDensities as CDDENS
import MapGeneration as MG

# Conditioned diffusion model
#
# COMMENTS
# Inputs are:
#  * num_steps: the number of steps at which to discretize (60)
#  * sigma: the noise
#  * final_time: the final time
#

# MONOTONE APPROXIMATION
# 'intexp': Integrated Exponential
# 'polyconst': Constrained Polynomial
AVAIL_MONOTONE = ['intexp', 'polyconst']

# BASIS TYPES
# 'poly': Hermite polynomials
# 'rbf': Radial basis functions
AVAIL_BTYPE = ['poly', 'rbf']

# SPAN APPROXIMATION
# 'full': Full order approximation
# 'total': Total order approximation
AVAIL_SPAN = ['full', 'total']

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

def usage():
    print('ConditionedDiffusion-Composition-Adapted.py --dens=<file_name> ' + \
          '-m <monotone_type> -s <span_type> ' + \
          ' [-b <batch_size> --output=<file_name> ' + \
          '--btype=<basis_type> --sigma=<sigma> --tfinal=<final_time> ' + \
          '--no-plotting --nprocs=<num_processors>]')

def print_avail_span():
    print('Available <span_type>: %s' % AVAIL_SPAN)

def print_avail_monotone():
    print('Available <monotone_type>: %s' % AVAIL_MONOTONE)

def print_avail_btype():
    print('Available <btype>: %s' % AVAIL_BTYPE)

def full_usage():
    usage()
    print_avail_span()
    print_avail_monotone()
    print_avail_btype()
    print("Example: ConditionedDiffusion-Composition-Adapted.py -d 50 " + \
          "-m intexp -s full")

qtype = 3
argv = sys.argv[1:]
MONOTONE = None
BTYPE = 'rbf'
SPAN = 'full'
BATCH_SIZE = None
DENS_FNAME = None
OUT_FNAME = None
DO_PLOT = True
CHECK_DERS = False
EPS_VAR_DIAG = 1e-2
ITMAX_VAR_DIAG = 15
NPROCS = 1
try:
    opts, args = getopt.getopt(argv,"hm:s:q:n:b:",["nsteps=","monoton_type=",
                                                   "span=", "bsize=",
                                                   "dens=",
                                                   "btype=", "output=",
                                                   "no-plotting",
                                                   "check-ders", "nprocs=",
                                                   "eps-var-diag=", "itmax-var-diag="])
except getopt.GetoptError as e:
    full_usage()
    print(e)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--dens"):
        DENS_FNAME = arg
    elif opt in ("-m", "--monotone_type"):
        MONOTONE = arg
        if MONOTONE not in AVAIL_MONOTONE:
            usage()
            print_avail_monotone()
            sys.exit(1)
    elif opt in ("-t", "--btype"):
        BTYPE = arg
        if BTYPE not in AVAIL_BTYPE:
            usage()
            print_avail_btype()
            sys.exit(1)
    elif opt in ("-s", "--span"):
        SPAN = arg
        if SPAN not in AVAIL_SPAN:
            usage()
            print_avail_span()
            sys.exit(1)
    elif opt in ("-b", "--bsize"):
        BATCH_SIZE = int(arg)
    elif opt in ("--output"):
        OUT_FNAME = arg
    elif opt in "--no-plotting":
        DO_PLOT = False
    elif opt in "--nprocs":
        NPROCS = int(arg)
    elif opt in "--check-ders":
        CHECK_DERS = True
    elif opt in ("--eps-var-diag"):
        EPS_VAR_DIAG = float(arg)
    elif opt in ("--itmax-var-diag"):
        ITMAX_VAR_DIAG = int(arg)
    else:
        raise RuntimeError("Input option %s not recognized." % opt)

WARMSTART = False
if OUT_FNAME is not None and os.path.exists(OUT_FNAME):
    if sys.version_info[0] == 3:
        sel = input("The file %s already exists. Do you want to use it to warm start? [y/N] " % OUT_FNAME)
    else:
        sel = raw_input("The file %s already exists. Do you want to use it to warm start? [y/N] " % OUT_FNAME)
    if sel == 'y' or sel == 'Y':
        WARMSTART = True
        # Restore state
        with open(OUT_FNAME, 'rb') as in_stream:
            data = dill.load(in_stream)
            target_density = data['target_density']
            tm_approx_comp_list = data['tm_approx_comp_list']
            ord_var_diag_list = data['ord_var_diag_list']
            MONOTONE = data['MONOTONE']
            BTYPE = data['BTYPE']
            SPAN = data['SPAN']
    else:
        if sys.version_info[0] == 3:
            sel = input("Do you want to overwrite? [y/N] ")
        else:
            sel = raw_input("Do you want to overwrite? [y/N] ")
        if sel == 'y' or sel == 'Y':
            pass
        else:
            print("Terminating.")
            sys.exit(0)

if not WARMSTART and None in [DENS_FNAME, OUT_FNAME, MONOTONE, SPAN]:
    full_usage()
    sys.exit(3)

# reg = None
reg = {'type': 'L2', # CHANGED: L2 regularization
       'alpha': 1e-2}
tol = 1e-5     # Optimization tolerance
    
if DO_PLOT:
    import matplotlib.pyplot as plt
    from matplotlib.patches import Rectangle
    plt.ion()

if not WARMSTART:
    ################ Define density ####################
    # TC DATA
    # dataObs = np.array([.8, 0., -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
    #                     -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20  ])
    # # Modified TC DATA
    # dataObs = np.array([ -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
    #                     -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20, 1.1, 1.05  ])
    # dataObs = np.array([1., 1.21, 1.025, 0.98, 1.05, .907 , 1.023,  1.05, .99, .90,
    #                     1.05 , .956, 1.067,  .9, .850,  .65,  -.9, -1., -0.92 , -1.05  ])
    # # Early transition
    # dataObs = np.array([1., 1.05, -.85, -1.0,  -.95,  -.9, -1., -0.92 , -1.05, -1.025,
    #                     -0.98, -1.05, -.907 , -1.023,  -1.05, -.99, -.90,
    #                     -1.05 , -.956, -1.067    ])
    # # NO TRANSITION
    # dataObs = np.array([1.,    1.21,  1.025, 0.98, 1.05,  
    #                      .907, 1.023, 1.05,   .99,  .90,
    #                     1.05 ,  .956, 1.067,  .95, 1.02,
    #                     1.1,   1.0,    .98,   .92, 1.05  ])
    with open(DENS_FNAME,'rb') as in_stream:
        target_density = dill.load(in_stream)

    # target_density = CDDENS.ConditionedDiffusion(
    #     NSTEPS, sigma=SIGMA, tfinal=TFINAL, dataObs = dataObs)

dim = target_density.dim
        
if DO_PLOT:
    fig_obs = plt.figure()
    ax_obs = fig_obs.add_subplot(111)
    ax_obs.plot(target_density.time_vec[target_density.indexObs], target_density.dataObs, 'o-')
    ax_obs.axis('equal')
    plt.show(False)

################ Compute Map list #######################
permutation_map = Maps.LinearTransportMap(np.array([0, 0]), np.array([[0, 1], [1, 0]]))
base_density_comp = DENS.StandardNormalDensity(2)
if not WARMSTART:
    tm_approx_comp_list = [] # List of 2d component maps
    ord_var_diag_list = []
if DO_PLOT:
    fig_slice_map = None
    fig_pullback = None
    fig_perm_target = None

# Solve for Map 0
if DO_PLOT:
    # Plot where we are
    ax_obs.clear()
    ax_obs.plot(target_density.time_vec[target_density.indexObs], target_density.dataObs, 'o-')
    ax_obs.plot([target_density.time_vec[1]]*2, [-2.,2.], 'k--')
    ax_obs.add_patch(Rectangle((0., -2.), target_density.time_vec[1], 4.,
                               facecolor="grey", alpha=0.4))
    ax_obs.axis('equal')
    fig_obs.canvas.draw()

if WARMSTART and len(tm_approx_comp_list) >= 1:
    ord_var_diag = ord_var_diag_list[0]
    it = ord_var_diag - 1
    mquad = ord_var_diag + 5
    tm_approx_comp = tm_approx_comp_list[0]
    tm_density_comp = DENS.PushForwardTransportMapDensity(
        tm_approx_comp, base_density_comp)
    sub_base_density_comp = DENS.StandardNormalDensity(1)
    target_comp = target_density.get_MarkovFactor(0, None, sub_base_density_comp)
    permuted_target_density_comp = DENS.PullBackTransportMapDensity(
        permutation_map, target_comp)
    # Construct T^\sharp tar
    pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                                 permuted_target_density_comp )
    var_diag = DIAG.variance_approx_kl(base_density_comp, pull_tar,
                                       qtype=3, qparams=[mquad+2]*2)
    print("Map 0 - it %d - Number of coefficients: %d" % (it, tm_density_comp.get_n_coeffs()))
    print("Map 0 - it %d - Variance diagnostic: %e" % (it,var_diag))
else:
    var_diag = 1.
    ord_var_diag = 0
    it = -1
    tm_approx_comp = None
while var_diag >= EPS_VAR_DIAG and it < ITMAX_VAR_DIAG:
    it += 1
    ord_var_diag += 1
    mquad = ord_var_diag + 5
    tm_approx_comp_old = tm_approx_comp
    if MONOTONE == 'polyconst':
        tm_approx_comp = TM.Default_IsotropicLinearSpanTriangularTransportMap(
            2, ord_var_diag, span=SPAN, btype=BTYPE)
        ders = 1
        batch_size = [None, None]
    elif MONOTONE == 'intexp':
        tm_approx_comp = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
            2, ord_var_diag, span=SPAN, btype=BTYPE)
        ders = 2
        batch_size = [None, None, BATCH_SIZE]
    if tm_approx_comp_old is not None:
        tm_approx_comp.regression(tm_approx_comp_old, d=base_density_comp,
                                  qtype=3, qparams=[mquad]*2, tol=1e-4)
    tm_density_comp = DENS.PushForwardTransportMapDensity(
        tm_approx_comp, base_density_comp)
    sub_base_density_comp = DENS.StandardNormalDensity(1)
    target_comp = target_density.get_MarkovFactor(0, None, sub_base_density_comp)
    # X = np.random.rand( 10, 2 )
    # (success, maxerr) = FDD.finiteDifference_hessian_X(
    #   target_comp.grad_x_log_pdf, target_comp.hess_x_log_pdf, X)
    permuted_target_density_comp = DENS.PullBackTransportMapDensity(
        permutation_map, target_comp)
    if CHECK_DERS:
        X = np.random.rand( 10, 2 )
        (success, maxerr) = FDD.finiteDifference_gradient_X(
            permuted_target_density_comp.log_pdf,
            permuted_target_density_comp.grad_x_log_pdf, X )
        if not success:
            raise RuntimeError("Wrong gradient. Maxerr: %e" % maxerr)
        (success, maxerr) = FDD.finiteDifference_hessian_X(
            permuted_target_density_comp.grad_x_log_pdf,
            permuted_target_density_comp.hess_x_log_pdf, X )
        if not success:
            raise RuntimeError("Wrong Hessian. Maxerr: %e" % maxerr)
    print("Map 0 - it %d - Number of coefficients: %d" % (it, tm_density_comp.get_n_coeffs()))
    qparams = [mquad]*2 # Quadrature
    # Solve
    x0 = tm_approx_comp.get_coeffs()
    log_entry_solve = tm_density_comp.minimize_kl_divergence(permuted_target_density_comp,
                                                             qtype=qtype,
                                                             qparams=qparams,
                                                             x0=x0,
                                                             regularization=reg,
                                                             tol=tol, ders=ders,
                                                             batch_size=batch_size)
    # Construct T^\sharp tar
    pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                                 permuted_target_density_comp )
    if DO_PLOT:
        # Run diagnostics
        print("Conditional Standard")
        fig_perm_target = DIAG.plotAlignedConditionals(permuted_target_density_comp,
                                                       range_vec=[-10,10],
                                                       title='Permuted target %d' % 0,
                                                       numPointsXax = 100,
                                                       fig = fig_perm_target,
                                                       nprocs=NPROCS)
        fig_slice_map = DIAG.plotAlignedSliceMap(tm_approx_comp, range_vec=[-4,4],
                                                 numPointsXax = 30,
                                                 numCont = 30,  tickslabelsize = 10,
                                                 fig = fig_slice_map,
                                                 nprocs=NPROCS)
        fig_pullback = DIAG.plotAlignedConditionals(pull_tar, title='Pullback %d' % 0,
                                                    range_vec=[-10,10],
                                                    numPointsXax = 100,
                                                    fig = fig_pullback,
                                                    nprocs=NPROCS)
    var_diag = DIAG.variance_approx_kl(base_density_comp, pull_tar,
                                       qtype=3, qparams=[mquad+2]*2)
    print("Map 0 - it %d - Variance diagnostic: %e" % (it, var_diag))
if len(tm_approx_comp_list) >= 1:
    tm_approx_comp_list[0] = tm_approx_comp
    ord_var_diag_list[0] = ord_var_diag
else:
    tm_approx_comp_list.append( tm_approx_comp )
    ord_var_diag_list.append( ord_var_diag )
if it >= ITMAX_VAR_DIAG:
    warnings.warn("Map not accurate enough", RuntimeWarning)

# Store transport maps:
with open(OUT_FNAME, 'wb') as out_stream:
    data = {'target_density': target_density,
            'tm_approx_comp_list': tm_approx_comp_list,
            'ord_var_diag_list': ord_var_diag_list,
            'MONOTONE': MONOTONE,
            'BTYPE': BTYPE,
            'SPAN': SPAN }
    dill.dump(data, out_stream)
    
# Interate over Map n
for n in range(1,dim-1):
    if DO_PLOT:
        # Plot where we are
        ax_obs.clear()
        ax_obs.plot(target_density.time_vec[target_density.indexObs],
                    target_density.dataObs, 'o-')
        ax_obs.plot([target_density.time_vec[n+1]]*2, [-2.,2.], 'k--')
        ax_obs.add_patch(Rectangle((0., -2.), target_density.time_vec[n+1], 4.,
                                   facecolor="grey", alpha=0.4))
        ax_obs.axis('equal')
        fig_obs.canvas.draw()
    if WARMSTART and len(tm_approx_comp_list) >= n+1:
        ord_var_diag = ord_var_diag_list[n]
        it = ord_var_diag - 1
        if n not in []:
            mquad = ord_var_diag + 5
        else:
            mquad = ord_var_diag + 50        
        tm_approx_comp = tm_approx_comp_list[n]
        tm_density_comp = DENS.PushForwardTransportMapDensity(
            tm_approx_comp, base_density_comp)
        sub_base_density_comp = DENS.StandardNormalDensity(1)
        target_comp = target_density.get_MarkovFactor(
            n, tm_approx_comp_list[n-1].approx_list[0], sub_base_density_comp)
        permuted_target_density_comp = DENS.PullBackTransportMapDensity(
            permutation_map, target_comp)
        # Construct T^\sharp tar
        pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                                     permuted_target_density_comp )
        var_diag = DIAG.variance_approx_kl(base_density_comp, pull_tar,
                                           qtype=3, qparams=[mquad+2]*2)
        print("Map %d - it %d - Number of coefficients: %d" %
              (n,it,tm_density_comp.get_n_coeffs()))
        print("Map %d - it %d - Variance diagnostic: %e" % (n,it,var_diag))
    else:
        var_diag = 1.
        if n not in []:
            ord_var_diag = 0
        else:
            ord_var_diag = 8
        it = -1
        tm_approx_comp = None
    while var_diag >= EPS_VAR_DIAG and it < ITMAX_VAR_DIAG:
        it += 1
        ord_var_diag += 1
        if n not in []:
            mquad = ord_var_diag + 5
        else:
            mquad = ord_var_diag + 50
        tm_approx_comp_old = tm_approx_comp
        if MONOTONE == 'polyconst':
            tm_approx_comp = TM.Default_IsotropicLinearSpanTriangularTransportMap(
                2, ord_var_diag, span=SPAN, btype=BTYPE)
            ders = 1
            batch_size = [None, None]
        elif MONOTONE == 'intexp':
            tm_approx_comp = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
                2, ord_var_diag, span=SPAN, btype=BTYPE)
            ders = 2
            batch_size = [None, None, BATCH_SIZE]
        if tm_approx_comp_old is not None:
            tm_approx_comp.regression(tm_approx_comp_old, d=base_density_comp,
                                      qtype=3, qparams=[mquad]*2, tol=1e-4)
        tm_density_comp = DENS.PushForwardTransportMapDensity(
            tm_approx_comp, base_density_comp)
        sub_base_density_comp = DENS.StandardNormalDensity(1)
        target_comp = target_density.get_MarkovFactor(
            n, tm_approx_comp_list[n-1].approx_list[0], sub_base_density_comp)
        permuted_target_density_comp = DENS.PullBackTransportMapDensity(
            permutation_map, target_comp)
        if CHECK_DERS:
            X = np.random.rand( 10, 2 )
            (success, maxerr) = FDD.finiteDifference_gradient_X(
                permuted_target_density_comp.log_pdf,
                permuted_target_density_comp.grad_x_log_pdf, X )
            if not success:
                raise RuntimeError("Wrong gradient. Maxerr: %e" % maxerr)
            (success, maxerr) = FDD.finiteDifference_hessian_X(
                permuted_target_density_comp.grad_x_log_pdf,
                permuted_target_density_comp.hess_x_log_pdf, X )
            if not success:
                raise RuntimeError("Wrong Hessian. Maxerr: %e" % maxerr)
        print("Map %d - it %d - Number of coefficients: %d" %
              (n,it,tm_density_comp.get_n_coeffs()))
        qparams = [mquad]*2 # Quadrature
        # Solve
        x0 = tm_approx_comp.get_coeffs()
        log_entry_solve = tm_density_comp.minimize_kl_divergence(
            permuted_target_density_comp,
            qtype=qtype,
            qparams=qparams,
            x0=x0,
            regularization=reg,
            tol=tol, ders=ders,
            batch_size=batch_size)
        # Construct T^\sharp tar
        pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                                     permuted_target_density_comp )
        if DO_PLOT:
            # Run diagnostics
            print("Conditional Standard")
            fig_perm_target = DIAG.plotAlignedConditionals(permuted_target_density_comp,
                                                           range_vec=[-10,10],
                                                           title='Permuted target %d' % n,
                                                           numPointsXax = 100,
                                                           fig = fig_perm_target,
                                                           nprocs=NPROCS)
            fig_slice_map = DIAG.plotAlignedSliceMap(tm_approx_comp, range_vec=[-4,4],
                                                     numPointsXax = 30, numCont = 30,
                                                     tickslabelsize = 10,
                                                     fig = fig_slice_map,
                                                     nprocs=NPROCS)
            fig_pullback = DIAG.plotAlignedConditionals(pull_tar, title='Pullback %d' % n,
                                                        range_vec=[-10,10],
                                                        numPointsXax = 100,
                                                        fig = fig_pullback,
                                                        nprocs=NPROCS)
        var_diag = DIAG.variance_approx_kl(base_density_comp, pull_tar,
                                           qtype=3, qparams=[mquad+2]*2)
        print("Map %d - it %d - Variance diagnostic: %e" % (n,it,var_diag))
    if len(tm_approx_comp_list) >= n+1:
        tm_approx_comp_list[n] = tm_approx_comp
        ord_var_diag_list[n] = ord_var_diag
    else:
        tm_approx_comp_list.append( tm_approx_comp )
        ord_var_diag_list.append( ord_var_diag )
    if it >= ITMAX_VAR_DIAG:
        warnings.warn("Map not accurate enough", RuntimeWarning)

    # Store transport maps:
    with open(OUT_FNAME, 'wb') as out_stream:
        data = {'target_density': target_density,
                'tm_approx_comp_list': tm_approx_comp_list,
                'ord_var_diag_list': ord_var_diag_list,
                'MONOTONE': MONOTONE,
                'BTYPE': BTYPE,
                'SPAN': SPAN }
        dill.dump(data, out_stream)

# #Check gradient
# fun = svDens.log_pdf
# grad = svDens.grad_x_log_pdf
# X = np.random.rand( 10, ndim  )
# finiteDifference_gradient_X( fun , grad , X )

# #Check hessian
# fun = svDens.grad_x_log_pdf
# hess = svDens.hess_x_log_pdf
# X = np.random.rand( 8, ndim  )
# finiteDifference_hessian_X( fun , hess , X )
