#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import warnings
import numpy as np
import scipy.stats as stats
import TransportMaps as TM
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG

import FiniteDifferenceDiagnostics as FDD
import ConditionedDiffusionDensities as CDDENS
import MapGeneration as MG

# Conditioned diffusion model
#
# COMMENTS
# Inputs are:
#  * num_steps: the number of steps at which to discretize (60)
#  * sigma: the noise
#  * final_time: the final time
#

def usage():
    print('ConditionedDiffusion-Laplace.py ' + \
          ' [--output=<file_name> --dens=<file_name>]')

def full_usage():
    usage()
    print("Example: ConditionedDiffusion-Laplace.py")

argv = sys.argv[1:]
OUT_FNAME = None
DENS_FNAME = None
try:
    opts, args = getopt.getopt(argv,"h",["output=", "dens="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--output"):
        OUT_FNAME = arg
    elif opt in ("--dens"):
        DENS_FNAME = arg
    else:
        raise RuntimeError("Input option %s not recognized." % opt)

if None in [OUT_FNAME, DENS_FNAME]:
    full_usage()
    sys.exit(3)
        
if os.path.exists(OUT_FNAME):
    if sys.version_info[0] == 3:
        sel = input("The file %s already exists. Do you want to overwrite? [y/N] " % OUT_FNAME)
    else:
        sel = raw_input("The file %s already exists. Do you want to overwrite? [y/N] " % OUT_FNAME)
    if sel == 'y' or sel == 'Y':
        pass
    else:
        print("Terminating.")
        sys.exit(0)

################ Define density ####################
# # TC DATA
# dataObs = np.array([.8, 0., -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
#                     -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20  ])
# # Modified TC DATA
# dataObs = np.array([ -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
#                     -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20, 1.1, 1.05  ])
# dataObs = np.array([1., 1.21, 1.025, 0.98, 1.05, .907 , 1.023,  1.05, .99, .90,
#                     1.05 , .956, 1.067,  .9, .850,  .65,  -.9, -1., -0.92 , -1.05  ])
# # Early transition
# dataObs = np.array([1., 1.05, -.85, -1.0,  -.95,  -.9, -1., -0.92 , -1.05, -1.025,
#                     -0.98, -1.05, -.907 , -1.023,  -1.05, -.99, -.90,
#                     -1.05 , -.956, -1.067    ])
# # NO TRANSITION
# dataObs = np.array([1.,    1.21,  1.025, 0.98, 1.05,  
#                      .907, 1.023, 1.05,   .99,  .90,
#                     1.05 ,  .956, 1.067,  .95, 1.02,
#                     1.1,   1.0,    .98,   .92, 1.05  ])
with open(DENS_FNAME,'rb') as in_stream:
    target_density = dill.load(in_stream)

dim = target_density.dim

################ Identity Laplace ##################
laplace_approx = TM.laplace_approximation( target_density )
laplace_id = Maps.LinearTransportMap(laplace_approx.mu, np.eye(dim))

# Store Laplace approximations:
with open(OUT_FNAME, 'wb') as out_stream:
    data = {'target_density': target_density,
            'laplace_id': laplace_id,
            'NSTEPS': dim }
    dill.dump(data, out_stream)

################ Laplace approximation #############
laplace_approx = TM.laplace_approximation( target_density )
laplace_tm = Maps.LinearTransportMap.build_from_Gaussian(laplace_approx)

# Store Laplace approximations:
with open(OUT_FNAME, 'wb') as out_stream:
    data = {'target_density': target_density,
            'laplace_id': laplace_id,
            'laplace_tm': laplace_tm,
            'NSTEPS': dim }
    dill.dump(data, out_stream)
