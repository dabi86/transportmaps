#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import time
# import seaborn as sns
# sns.set(color_codes=True)
# import pandas as pd
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS

import MapGeneration as MG

def usage():
    print("ConditionedDiffusion-Composition-Postprocess.py -i <file_name> [--nprocs=<num_processors> --no-aligned --no-random]")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
NMC = 10000
NPROCS = 1
DO_ALIGNED = True
N_ALIGNED = 8
DO_RANDOM = True
N_RANDOM = 6
DO_VAR_DIAG = True
DO_SMOOTH = True
DO_FILT = True
# DO_MARG_TAR = True
try:
    opts, args = getopt.getopt(argv,"hi:",["input=", "nprocs=", "nmc=",
                                           "no-aligned", "naligned=",
                                           "no-random", "nrandom=",
                                           "no-var-diag",
                                           "no-smooth", "no-filt"]) #, "no-marg-tar"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-i", "--input"):
        IN_FNAME = arg
    elif opt in "--nprocs":
        NPROCS = int(arg)
    elif opt in "--nmc":
        NMC = int(arg)
    elif opt in "--no-aligned":
        DO_ALIGNED = False
    elif opt in "--naligned":
        N_ALIGNED = int(arg)
    elif opt in "--no-random":
        DO_RANDOM = False
    elif opt in "--nrandom":
        N_RANDOM = int(arg)
    elif opt in "--no-var-diag":
        DO_VAR_DIAG = False
    elif opt in "--no-smooth":
        DO_SMOOTH = False
    elif opt in "--no-filt":
        DO_FILT = False
    # elif opt in "--no-marg-tar":
    #     DO_MARG_TAR = False
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME, 'rb') as in_stream:
    data = dill.load(in_stream)

# Resetore data
tm_approx_comp_list = data['tm_approx_comp_list']
target_density = data['target_density']
base_density = DENS.StandardNormalDensity(target_density.dim)

# Construct T^\sharp \pi_{tar}
pull_tar = MG.compose_Markovian_pullback(tm_approx_comp_list, target_density)

# Construct T
tm = MG.compose_Markovian_tm(tm_approx_comp_list)

# Construct T_\sharp \rho
push_base = DENS.PushForwardTransportMapDensity(tm, base_density)

# Diagnostics
if DO_ALIGNED:
    start = time.clock()
    dvec = list(npr.choice(np.arange(target_density.dim),
                           min(target_density.dim, N_ALIGNED),
                           replace=False))
    # Run diagnostics
    print("Aligned conditionals: %s" % dvec)
    DIAG.plotAlignedConditionals(pull_tar, numPointsXax = 40, range_vec=[-8,8],
                                 dimensions_vec=dvec, nprocs=NPROCS,
                                 title='Conditionals pullback nonlinear map')
    stop = time.clock()
    print("Aligned conditionals - Time: %.2f" % (stop - start))

if DO_RANDOM:
    start = time.clock()
    print("Random conditionals")
    DIAG.plotRandomConditionals(pull_tar, num_conditionalsXax=N_RANDOM,
                                numPointsXax = 10, nprocs=NPROCS)
    stop = time.clock()
    print("Random conditionals - Time: %.2f" % (stop - start))

if DO_VAR_DIAG:
    start = time.clock()
    var_diag = DIAG.variance_approx_kl(base_density, pull_tar, qtype=0, qparams=NMC,
                                       nprocs=NPROCS)
    print("Variance Diagnostic: %e" % var_diag)
    stop = time.clock()
    print("Variance Diagnostic - Time: %.2f" % (stop - start))

# if DO_MARG_TAR:
#     tar_samp = push_base.rvs(NMC)
#     tar_samp_marg = tar_samp[:,40:45]
#     df = pd.DataFrame(tar_samp_marg, columns=[ str(a) for a in range(40,45) ])
#     g = sns.PairGrid(df)
#     g.map_diag(sns.kdeplot)
#     g.map_offdiag(sns.kdeplot, cmap="Blues_d", n_levels=6)
#     plt.show(False)
    
if DO_SMOOTH:
    tar_samp = push_base.rvs(NMC)
    tvec = np.zeros(target_density.dim + 1)
    tvec[1:] = target_density.time_vec
    mean_smooth = np.zeros(tvec.shape)
    mean_smooth[1:] = np.mean(tar_samp, axis=0)
    q10_smooth = np.zeros(tvec.shape)
    q10_smooth[1:] = np.percentile(tar_samp, 10, axis=0)
    q90_smooth = np.zeros(tvec.shape)
    q90_smooth[1:] = np.percentile(tar_samp, 90, axis=0)

    fig = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{t}$",
                        title='Smoothing')
    ax = fig.gca()
    ax.set_aspect('equal')
    ax.set_ylim(-2,2)
    ax.set_xlim(0,target_density.time_vec[-1])
    ax.plot(target_density.time_vec_proc, target_density.Xt, '--k', label="truth")
    ax.plot(target_density.time_vec[target_density.indexObs], target_density.dataObs, 'or')
    ax.plot(tvec, mean_smooth, '-k')
    ax.fill_between(tvec, q10_smooth, q90_smooth, facecolor='grey', alpha=0.2)
    plt.legend(loc='best')
    plt.show(False)

if DO_FILT:
    n1d = DENS.StandardNormalDensity(1)
    n2d = DENS.StandardNormalDensity(2)
    tvec = np.zeros(target_density.dim + 1)
    tvec[1:] = target_density.time_vec

    tar_samp = np.zeros((NMC,target_density.dim))
    n2d_samp = n2d.rvs(NMC)
    tar_samp[:,0] = tm_approx_comp_list[0].evaluate(n2d_samp)[:,1]
    for i,tm in enumerate(tm_approx_comp_list):
        n1d_samp = n1d.rvs(NMC)
        tar_samp[:,i+1] = tm.approx_list[0].evaluate(n1d_samp).flatten()
    
    mean_filt = np.zeros(tvec.shape)
    mean_filt[1:] = np.mean(tar_samp, axis=0)
    q10_filt = np.zeros(tvec.shape)
    q10_filt[1:] = np.percentile(tar_samp, 10, axis=0)
    q90_filt = np.zeros(tvec.shape)
    q90_filt[1:] = np.percentile(tar_samp, 90, axis=0)

    fig = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{t}$",
                        title='Filtering')
    ax = fig.gca()
    ax.set_aspect('equal')
    ax.set_ylim(-2,2)
    ax.set_xlim(0,target_density.time_vec[-1])
    ax.plot(target_density.time_vec_proc, target_density.Xt, '--k', label="truth")
    ax.plot(target_density.time_vec[target_density.indexObs], target_density.dataObs, 'or')
    ax.plot(tvec, mean_filt, '-k')
    ax.fill_between(tvec, q10_filt, q90_filt, facecolor='grey', alpha=0.2)
    plt.legend(loc='best')
    plt.show(False)

if DO_SMOOTH and DO_FILT:
    fig = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{t}$",
                        title='')
    ax = fig.gca()
    ax.set_aspect('equal')
    ax.set_ylim(-2,2)
    ax.set_xlim(0,target_density.time_vec[-1])
    ax.plot(target_density.time_vec_proc, target_density.Xt, '--k', label="truth")
    ax.plot(target_density.time_vec[target_density.indexObs], target_density.dataObs, 'or')
    ax.plot(tvec, mean_filt, '-r', label="filtering")
    ax.fill_between(tvec, q10_filt, q90_filt, facecolor='red', alpha=0.1)
    ax.plot(tvec, mean_smooth, '-k', label="smoothing")
    ax.fill_between(tvec, q10_smooth, q90_smooth, facecolor='grey', alpha=0.3)
    ax.legend(loc='best')
    plt.show(False)