#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys
import matplotlib.pyplot as plt
import numpy as np

import ConditionedDiffusionDensities as CDDENS

nsteps_proc = 600
nsteps = 60
tfinal = 10.
sigma = 0.1
x0 = 0.
c0 = 7.
nobs = 20

fig = plt.figure()

accept = False
while not accept:
    (tvec, Xt, indexObs, obs) = CDDENS.generateData(
        nsteps=nsteps_proc, tfinal=tfinal, sigma=sigma,
        c0=c0, nobs=nobs, x0=x0)

    ax = fig.gca()
    ax.clear()
    ax.set_aspect('equal')
    ax.set_ylim(-2,2)
    ax.set_xlim(0,tfinal)
    ax.plot(tvec, Xt, '-k')
    ax.plot(tvec[indexObs], obs, 'ro')
    plt.show(False)

    if (np.min(Xt) < -1. and np.max(Xt) > 1.):
        if sys.version_info[0] == 3:
            sel = input("Accept the current data? [y/N/q] ")
        else:
            sel = raw_input("Accept the current data? [y/N/q] ")
        if (sel == 'y' or sel == 'Y'):
            accept = True
        elif sel == 'q': sys.exit(0)

dens = CDDENS.ConditionedDiffusion(tvec, Xt, nsteps, indexObs, obs, sigma, c0)