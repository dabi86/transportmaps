#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import numpy as np
import TransportMaps as TM
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG

import FiniteDifferenceDiagnostics as FDD
import ConditionedDiffusionDensities as CDDENS

# Conditioned diffusion model
#
# COMMENTS
# Inputs are:
#  * num_steps: the number of steps at which to discretize (60)
#  * sigma: the noise
#  * final_time: the final time
#

# MONOTONE APPROXIMATION
# 'intexp': Integrated Exponential
# 'polyconst': Constrained Polynomial
AVAIL_MONOTONE = ['intexp', 'polyconst']

# BASIS TYPES
# 'poly': Hermite polynomials
# 'rbf': Radial basis functions
AVAIL_BTYPE = ['poly', 'rbf']

# SPAN APPROXIMATION
# 'full': Full order approximation
# 'total': Total order approximation
AVAIL_SPAN = ['full', 'total']

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

def usage():
    print('ConditionedDiffusion-Composition-Construction.py -d <num_steps> ' + \
          '-m <monotone_type> -o <order> -s <span_type> -q <quad_type> ' + \
          '-n <quad_n_points> [-b <batch_size> --output=<file_name>  ' + \
          '--btype=<basis_type> --sigma=<sigma> --tfinal=<final_time> ' + \
          '--no-laplace --no-plotting]')

def print_avail_span():
    print('Available <span_type>: %s' % AVAIL_SPAN)

def print_avail_qtype():
    print('Available <quad_type>:')
    for qtypen, qtypename in AVAIL_QUADRATURE.items():
        print('  %d: %s' % (qtypen, qtypename))

def print_avail_monotone():
    print('Available <monotone_type>: %s' % AVAIL_MONOTONE)

def print_avail_btype():
    print('Available <btype>: %s' % AVAIL_BTYPE)

def full_usage():
    usage()
    print_avail_span()
    print_avail_qtype()
    print_avail_monotone()
    print_avail_btype()
    print("Example: ConditionedDiffusion-Composition-Construction.py -d 50 " + \
          "-m intexp -o 10 -s full -q 3 -n 20")


argv = sys.argv[1:]
NSTEPS = None
MONOTONE = None
BTYPE = 'rbf'
ORDER = None
SPAN = 'full'
QTYPE = None
QNUM = None
BATCH_SIZE = None
SIGMA = 0.1
TFINAL = 10.
OUT_FNAME = 'ConditionedDiffusion-Composition.dill'
DO_LAPLACE = True
DO_PLOT = True
CHECK_DERS = False
try:
    opts, args = getopt.getopt(argv,"hd:m:o:s:q:n:b:",["nsteps=","monoton_type=",
                                                       "order=","span=",
                                                       "qtype=", "qnum=", "bsize=",
                                                       "btype=", "output=",
                                                       "sigma=","tfinal=",
                                                       "no-laplace","no-plotting",
                                                       "check-ders"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-d", "--nsteps"):
        NSTEPS = int(arg)
    elif opt in ("-m", "--monotone_type"):
        MONOTONE = arg
        if MONOTONE not in AVAIL_MONOTONE:
            usage()
            print_avail_monotone()
            sys.exit(1)
    elif opt in ("-t", "--btype"):
        BTYPE = arg
        if BTYPE not in AVAIL_BTYPE:
            usage()
            print_avail_btype()
            sys.exit(1)
    elif opt in ("-o", "--order"):
        ORDER = int(arg)
    elif opt in ("-s", "--span"):
        SPAN = arg
        if SPAN not in AVAIL_SPAN:
            usage()
            print_avail_span()
            sys.exit(1)
    elif opt in ("-q", "--qtype"):
        QTYPE = int(arg)
        if QTYPE not in AVAIL_QUADRATURE:
            usage()
            print_avail_qtype()
            sys.exit(1)
    elif opt in ("-n", "--qnum"):
        QNUM = int(arg)
    elif opt in ("-b", "--bsize"):
        BATCH_SIZE = int(arg)
    elif opt in ("--sigma"):
        SIGMA = float(arg)
    elif opt in ("--tfinal"):
        TFINAL = float(arg)
    elif opt in ("--output"):
        OUT_FNAME = arg
    elif opt in "--no-laplace":
        DO_LAPLACE = False
    elif opt in "--no-plotting":
        DO_PLOT = False
    elif opt in "--check-ders":
        CHECK_DERS = True
if None in [NSTEPS, MONOTONE, ORDER, SPAN, QTYPE, QNUM]:
    full_usage()
    sys.exit(3)

if os.path.exists(OUT_FNAME):
    if sys.version_info[0] == 3:
        sel = input("The file %s already exists. Overwrite? [y/N] " % OUT_FNAME)
    else:
        sel = raw_input("The file %s already exists. Overwrite? [y/N] " % OUT_FNAME)
    if sel == 'y' or sel == 'Y':
        pass
    else:
        sys.exit(0)
    
if DO_PLOT:
    import matplotlib.pyplot as plt
    from matplotlib.patches import Rectangle
    plt.ion()
    
################ Define density ####################
# TC DATA
dataObs = np.array([.8, 0., -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
                    -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20  ])
# # Modified TC DATA
# dataObs = np.array([ -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
#                     -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20, 1.1, 1.05  ])
# dataObs = np.array([1., 1.21, 1.025, 0.98, 1.05, .907 , 1.023,  1.05, .99, .90,
#                     1.05 , .956, 1.067,  .9, .850,  .65,  -.9, -1., -0.92 , -1.05  ])
# # Early transition
# dataObs = np.array([1., 1.05, -.85, -1.0,  -.95,  -.9, -1., -0.92 , -1.05, -1.025,
#                     -0.98, -1.05, -.907 , -1.023,  -1.05, -.99, -.90,
#                     -1.05 , -.956, -1.067    ])
# # NO TRANSITION
# dataObs = np.array([1.,    1.21,  1.025, 0.98, 1.05,  
#                      .907, 1.023, 1.05,   .99,  .90,
#                     1.05 ,  .956, 1.067,  .95, 1.02,
#                     1.1,   1.0,    .98,   .92, 1.05  ])

target_density = CDDENS.ConditionedDiffusion(
    NSTEPS, sigma=SIGMA, tfinal=TFINAL, dataObs = dataObs)

if DO_PLOT:
    fig_obs = plt.figure()
    ax_obs = fig_obs.add_subplot(111)
    ax_obs.plot(target_density.timeObs, target_density.dataObs, 'o-')
    plt.show(False)

################ Plot density ######################

if DO_LAPLACE:
    ################ Identity Laplace ##################
    laplace_approx = TM.laplace_approximation( target_density )
    laplace_id = Maps.LinearTransportMap(laplace_approx.mu, np.eye(NSTEPS))
    laplace_id_pullback = DENS.PullBackTransportMapDensity( laplace_id, target_density )
    if DO_PLOT:
        DIAG.plotAlignedConditionals( laplace_id_pullback, numPointsXax=100 )

    ################ Laplace approximation #############
    laplace_approx = TM.laplace_approximation( target_density )
    laplace_tm = Maps.LinearTransportMap.build_from_Gaussian(laplace_approx)
    laplace_pullback = DENS.PullBackTransportMapDensity( laplace_tm, target_density )
    if DO_PLOT:
        DIAG.plotAlignedConditionals( laplace_pullback )

################ Compute Map list #######################
permutation_map = Maps.LinearTransportMap(np.array([0, 0]), np.array([[0, 1], [1, 0]]))
tm_approx_comp_list = [] # List of 2d component maps

# Solve for Map 0
if MONOTONE == 'polyconst':
    tm_approx_comp = TM.Default_IsotropicLinearSpanTriangularTransportMap(
        2, ORDER, span=SPAN, btype=BTYPE)
    ders = 1
    batch_size = [None, None]
elif MONOTONE == 'intexp':
    tm_approx_comp = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
        2, ORDER, span=SPAN, btype=BTYPE)
    ders = 2
    batch_size = [None, None, BATCH_SIZE]
tm_approx_comp_list.append( tm_approx_comp )
base_density_comp = DENS.StandardNormalDensity(2)
tm_density_comp = DENS.PushForwardTransportMapDensity(tm_approx_comp, base_density_comp)
sub_base_density_comp = DENS.StandardNormalDensity(1)
target_comp = target_density.get_MarkovFactor(0, None, sub_base_density_comp)
X = np.random.rand( 10, 2 )
(success, maxerr) = FDD.finiteDifference_hessian_X(
  target_comp.grad_x_log_pdf, target_comp.hess_x_log_pdf, X)

permuted_target_density_comp = DENS.PullBackTransportMapDensity(permutation_map, target_comp)
if CHECK_DERS:
    X = np.random.rand( 10, 2 )
    (success, maxerr) = FDD.finiteDifference_gradient_X(
        permuted_target_density_comp.log_pdf,
        permuted_target_density_comp.grad_x_log_pdf, X )
    if not success:
        raise RuntimeError("Wrong gradient. Maxerr: %e" % maxerr)
    (success, maxerr) = FDD.finiteDifference_hessian_X(
        permuted_target_density_comp.grad_x_log_pdf,
        permuted_target_density_comp.hess_x_log_pdf, X )
    if not success:
        raise RuntimeError("Wrong Hessian. Maxerr: %e" % maxerr)
print("Map 0 - Number of coefficients: %d" % tm_density_comp.get_n_coeffs())
qtype = QTYPE
if qtype == 0:
    qparams = QNUM # Monte Carlo
elif qtype == 3:
    qparams = [QNUM]*2 # Quadrature
# reg = None     # No regularization
reg = {'type': 'L2', # CHANGED: L2 regularization
       'alpha': 1e-2}
tol = 1e-4     # Optimization tolerance
if DO_PLOT:
    # Plot where we are
    ax_obs.clear()
    ax_obs.plot(target_density.timeObs, target_density.dataObs, 'o-')
    ax_obs.plot([target_density.time_vec[1]]*2, [-2.,2.], 'k--')
    ax_obs.add_patch(Rectangle((0., -2.), target_density.time_vec[1], 4.,
                               facecolor="grey", alpha=0.4))
    fig_obs.canvas.draw()
# Solve
log_entry_solve = tm_density_comp.minimize_kl_divergence(permuted_target_density_comp,
                                                         qtype=qtype,
                                                         qparams=qparams,
                                                         regularization=reg,
                                                         tol=tol, ders=ders,
                                                         batch_size=batch_size)
# Construct T^\sharp tar
pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                             permuted_target_density_comp )
if DO_PLOT:
    # Run diagnostics
    print("Conditional Standard")
    fig_slice_map = DIAG.plotAlignedSliceMap(tm_approx_comp, range_vec=[-4,4],
                                             numPointsXax = 30,
                                             numCont = 30,  tickslabelsize = 10 )
    fig_pullback = DIAG.plotAlignedConditionals(pull_tar, title='Pullback %d' % 0,
                                                numPointsXax = 100)
    fig_perm_target = DIAG.plotAlignedConditionals(permuted_target_density_comp,
                                                   title='Permuted target %d' % 0,
                                                   numPointsXax = 100)

# Interate over Map n
for n in range(1,NSTEPS-1):
    if MONOTONE == 'polyconst':
        tm_approx_comp = TM.Default_IsotropicLinearSpanTriangularTransportMap(
            2, ORDER, span=SPAN, btype=BTYPE)
        ders = 1
        batch_size = [None, None]
    elif MONOTONE == 'intexp':
        tm_approx_comp = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
            2, ORDER, span=SPAN, btype=BTYPE)
        ders = 2
        batch_size = [None, None, BATCH_SIZE]
    tm_approx_comp_list.append( tm_approx_comp )
    base_density_comp = DENS.StandardNormalDensity(2)
    tm_density_comp = DENS.PushForwardTransportMapDensity(tm_approx_comp, base_density_comp)
    sub_base_density_comp = DENS.StandardNormalDensity(1)
    target_comp = target_density.get_MarkovFactor(n, tm_approx_comp_list[n-1].approx_list[0],
                                                  sub_base_density_comp)
    permuted_target_density_comp = DENS.PullBackTransportMapDensity(permutation_map, target_comp)
    if CHECK_DERS:
        X = np.random.rand( 10, 2 )
        (success, maxerr) = FDD.finiteDifference_gradient_X(
            permuted_target_density_comp.log_pdf,
            permuted_target_density_comp.grad_x_log_pdf, X )
        if not success:
            raise RuntimeError("Wrong gradient. Maxerr: %e" % maxerr)
        (success, maxerr) = FDD.finiteDifference_hessian_X(
            permuted_target_density_comp.grad_x_log_pdf,
            permuted_target_density_comp.hess_x_log_pdf, X )
        if not success:
            raise RuntimeError("Wrong Hessian. Maxerr: %e" % maxerr)
    print("Map %d - Number of coefficients: %d" % (n, tm_density_comp.get_n_coeffs()))
    qtype = QTYPE
    if qtype == 0:
        qparams = QNUM # Monte Carlo
    elif qtype == 3:
        qparams = [QNUM]*2 # Quadrature
    tol = 1e-4    # Optimization tolerance
    if DO_PLOT:
        # Plot where we are
        ax_obs.clear()
        ax_obs.plot(target_density.timeObs, target_density.dataObs, 'o-')
        ax_obs.plot([target_density.time_vec[n+1]]*2, [-2.,2.], 'k--')
        ax_obs.add_patch(Rectangle((0., -2.), target_density.time_vec[n+1], 4.,
                                   facecolor="grey", alpha=0.4))
        fig_obs.canvas.draw()
    # Solve
    log_entry_solve = tm_density_comp.minimize_kl_divergence(permuted_target_density_comp,
                                                             qtype=qtype,
                                                             qparams=qparams,
                                                             regularization=reg,
                                                             tol=tol, ders=ders,
                                                             batch_size=batch_size)
    # Construct T^\sharp tar
    pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                                 permuted_target_density_comp )
    if DO_PLOT:
        # Run diagnostics
        print("Conditional Standard")
        fig_slice_map = DIAG.plotAlignedSliceMap(tm_approx_comp, range_vec=[-4,4],
                                                 numPointsXax = 30, numCont = 30,
                                                 tickslabelsize = 10, fig = fig_slice_map)
        fig_pullback = DIAG.plotAlignedConditionals(pull_tar, title='Pullback %d' % n,
                                                    numPointsXax = 100,
                                                    fig = fig_pullback)
        fig_perm_target = DIAG.plotAlignedConditionals(permuted_target_density_comp,
                                                       title='Permuted target %d' % n,
                                                       numPointsXax = 100,
                                                       fig = fig_perm_target)

# Store transport maps:
with open(OUT_FNAME, 'wb') as out_stream:
    data = {'tm_approx_comp_list': tm_approx_comp_list,
            'target_density': target_density}
    dill.dump(data, out_stream)

# #Check gradient
# fun = svDens.log_pdf
# grad = svDens.grad_x_log_pdf
# X = np.random.rand( 10, ndim  )
# finiteDifference_gradient_X( fun , grad , X )

# #Check hessian
# fun = svDens.grad_x_log_pdf
# hess = svDens.hess_x_log_pdf
# X = np.random.rand( 8, ndim  )
# finiteDifference_hessian_X( fun , hess , X )
