from __future__ import division #Get rid of integer division
import numpy as np
import numpy.linalg as npla

################ Finite differences routines #######################
def finiteDifference_gradient_X( fun , grad , X , delta = 1e-6, rtol=1e-6):
    # X is a 2-d array of points. The first dimension corresponds to the number of points.
    npoints = X.shape[0]
    ndim = X.shape[1]
    grad_X = grad(X)
    dF_X = np.zeros( (npoints, ndim) )
    for kk in np.arange(ndim):
        Xp = np.copy(X)
        Xp[:,kk] += delta
        Xm = np.copy(X)
        Xm[:,kk] -= delta
        Fp = fun(Xp)
        Fm = fun(Xm)
        dF_X[:,kk] = (Fp-Fm)/(2*delta)
    rerror_vec = npla.norm(grad_X - dF_X, axis = 0)/npla.norm(grad_X)
    maxrErr =  np.max(rerror_vec)
    iscorrect =  maxrErr <= rtol
    return iscorrect, maxrErr
def finiteDifference_hessian_X( fun , hess , X , delta = 1e-6, rtol=1e-6):
    # X is a 2-d array of points. The first dimension corresponds to the number of points.
    npoints = X.shape[0]
    ndim = X.shape[1]
    hess_X = hess(X)
    d2F_X = np.zeros( (npoints, ndim, ndim) )
    for kk in np.arange(ndim):
        Xp = np.copy(X)
        Xp[:,kk] += delta
        Xm = np.copy(X)
        Xm[:,kk] -= delta
        Fp = fun(Xp)
        Fm = fun(Xm)
        d2F_X[:,:,kk] = (Fp-Fm)/(2*delta)
    rerror_vec = np.zeros(npoints)
    for kk in np.arange(npoints):
        rerror_vec[kk] = npla.norm( d2F_X[kk,:,:]-hess_X[kk,:,:])/npla.norm(hess_X[kk,:,:])
        maxrErr =  np.max(rerror_vec)
    iscorrect =  maxrErr <= rtol
    return iscorrect, maxrErr