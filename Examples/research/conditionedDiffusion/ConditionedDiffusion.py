#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

# Running example
# python StochasticVolatility.py -d 3 -o 4 -s total -q 3 -n 8

from __future__ import division #Get rid of integer division
import sys, getopt
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from scipy import linalg
import SpectralToolbox.Spectral1D as S1D
import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG

import condDiffusionDensities as CDDENS

# Conditioned diffusion model
#
# COMMENTS
# Inputs are:
#  * num_steps: the number of steps at which to discretize
#  * sigma: the noise
#  * final_time: the final time
#

# SPAN APPROXIMATION
# 'full': Full order approximation
# 'total': Total order approximation
AVAIL_SPAN = ['full', 'total']

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

def usage():
    print('ConditionedDiffusion.py -d <num_steps> -o <order> -s <span_type> -q <quad_type> -n <quad_n_points> [--sigma=<sigma> --tfinal=<final_time> --no-laplace --no-plotting]')

def print_avail_span():
    print('Available <span_type>: %s' % AVAIL_SPAN)

def print_avail_qtype():
    print('Available <quad_type>:')
    for qtypen, qtypename in AVAIL_QUADRATURE.items():
        print('  %d: %s' % (qtypen, qtypename))

def full_usage():
    usage()
    print_avail_span()
    print_avail_qtype()
    print("Example: ConditionedDiffusion.py -d 3 -o 4 -s total -q 3 -n 8")

argv = sys.argv[1:]
NSTEPS = None
ORDER = None
SPAN = None
QTYPE = None
QNUM = None
SIGMA = 0.1
TFINAL = 10.
DO_LAPLACE = True
DO_PLOT = True
try:
    opts, args = getopt.getopt(argv,"hd:o:s:q:n:",["nsteps=","order=","span=",
                                                   "qtype=", "qnum=",
                                                   "sigma=", "tfinal=",
                                                   "no-laplace","no-plotting"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-d", "--nsteps"):
        NSTEPS = int(arg)
    elif opt in ("-o", "--order"):
        ORDER = int(arg)
    elif opt in ("-s", "--span"):
        SPAN = arg
        if SPAN not in AVAIL_SPAN:
            usage()
            print_avail_span()
            sys.exit(1)
    elif opt in ("-q", "--qtype"):
        QTYPE = int(arg)
        if QTYPE not in AVAIL_QUADRATURE:
            usage()
            print_avail_qtype()
            sys.exit(1)
    elif opt in ("-n", "--qnum"):
        QNUM = int(arg)
    elif opt in ("--sigma"):
        SIGMA = float(arg)
    elif opt in ("--tfinal"):
        TFINAL = float(arg)
    elif opt in "--no-laplace":
        DO_LAPLACE = False
    elif opt in "--no-plotting":
        DO_PLOT = False
if None in [NSTEPS, ORDER, SPAN, QTYPE, QNUM]:
    full_usage()
    sys.exit(3)

################ Define density ####################
dataObs = [.8, 0., -1.1, -1.2, -.75, -1.19, -1.23,  -0.95, -0.88, -.55,
           -1.7, -1.35,  -0.9,  -1.26, -.5,  -.73, 1., .95, 1.22, 1.20  ]
cond_diff_density = CDDENS.CondDiff(NSTEPS, sigma=SIGMA, tfinal=TFINAL, dataObs = dataObs)
target_density = cond_diff_density

################ Plot density ######################

if DO_LAPLACE:
    ################ Identity Laplace ##################
    laplace_approx = TM.laplace_approximation( target_density )
    laplace_id = Maps.LinearTransportMap(laplace_approx.mu, np.eye(NSTEPS))
    laplace_id_pullback = DENS.PullBackTransportMapDensity( laplace_id, target_density )
    if DO_PLOT:
        DIAG.plotAlignedConditionals( laplace_id_pullback, numPointsXax=100 )

    ################ Laplace approximation #############
    laplace_approx = TM.laplace_approximation( target_density )
    laplace_tm = Maps.LinearTransportMap.build_from_Gaussian(laplace_approx)
    laplace_pullback = DENS.PullBackTransportMapDensity( laplace_tm, target_density )
    if DO_PLOT:
        DIAG.plotAlignedConditionals( laplace_pullback )

################ Compute Map #######################
tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
    NSTEPS, ORDER, SPAN)
base_density = DENS.StandardNormalDensity(NSTEPS)
tm_density = DENS.PushForwardTransportMapDensity(tm_approx, base_density)

print("Number of coefficients: %d" % tm_density.get_n_coeffs())

qtype = QTYPE
if qtype == 0:
    qparams = QNUM # Monte Carlo
elif qtype == 3:
    qparams = [QNUM]*NSTEPS # Quadrature
reg = None     # No regularization
tol = 1e-4     # Optimization tolerance
ders = 2       # Use gradient and Hessian
log_entry_solve = tm_density.minimize_kl_divergence(target_density,
                                                    qtype=qtype,
                                                    qparams=qparams,
                                                    regularization=reg,
                                                    tol=tol, ders=ders)

# Construct T^\sharp tar
pull_tar = DENS.PullBackTransportMapDensity( tm_density.transport_map, target_density )

if DO_PLOT:
    # Run diagnostics
    print("Conditional Standard")
    DIAG.plotAlignedConditionals(pull_tar)
    print("Conditional Random")
    DIAG.plotRandomConditionals(pull_tar, num_conditionalsXax=7)
# print("Variance Diagnostic")
# DIAG.varianceDiagnosic( base_density, pull_tar,
#                         numMCsamples =[1e3, 5e3, 1e4], ntrialMC=20)

################ Finite differences routines #######################

def finiteDifference_gradient_X( fun , grad , X , delta = 1e-6, rtol=1e-6):
    # X is a 2-d array of points. The first dimension corresponds to the number of points.
    npoints = X.shape[0]
    ndim = X.shape[1]
    grad_X = grad(X)
    dF_X = np.zeros( (npoints, ndim) )
    for kk in np.arange(ndim):
        Xp = np.copy(X)
        Xp[:,kk] += delta
        Xm = np.copy(X)
        Xm[:,kk] -= delta
        Fp = fun(Xp)
        Fm = fun(Xm)
        dF_X[:,kk] = (Fp-Fm)/(2*delta)
    rerror_vec = np.linalg.norm(grad_X - dF_X, axis = 0)/np.linalg.norm(grad_X)
    maxrErr =  np.max(rerror_vec)
    iscorrect =  maxrErr <= rtol
    return iscorrect, maxrErr

def finiteDifference_hessian_X( fun , hess , X , delta = 1e-6, rtol=1e-6):
    # X is a 2-d array of points. The first dimension corresponds to the number of points.
    npoints = X.shape[0]
    ndim = X.shape[1]
    hess_X = hess(X)
    d2F_X = np.zeros( (npoints, ndim, ndim) )
    for kk in np.arange(ndim):
        Xp = np.copy(X)
        Xp[:,kk] += delta
        Xm = np.copy(X)
        Xm[:,kk] -= delta
        Fp = fun(Xp)
        Fm = fun(Xm)
        d2F_X[:,:,kk] = (Fp-Fm)/(2*delta)
    rerror_vec = np.zeros(npoints)
    for kk in np.arange(npoints):
        rerror_vec[kk] = np.linalg.norm( d2F_X[kk,:,:]-hess_X[kk,:,:])/np.linalg.norm(hess_X[kk,:,:])
        maxrErr =  np.max(rerror_vec)
    iscorrect =  maxrErr <= rtol
    return iscorrect, maxrErr

# #Check gradient
# fun = svDens.log_pdf
# grad = svDens.grad_x_log_pdf
# X = np.random.rand( 10, ndim  )
# finiteDifference_gradient_X( fun , grad , X )

# #Check hessian
# fun = svDens.grad_x_log_pdf
# hess = svDens.hess_x_log_pdf
# X = np.random.rand( 8, ndim  )
# finiteDifference_hessian_X( fun , hess , X )
