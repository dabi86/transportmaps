import sys, getopt
import os.path
import numpy as np
import dill
import matplotlib.pyplot as plt

import ConditionedDiffusionDensities as CDDENS

def usage():
    print('ConditionedDiffusion-TrimProblem.py -i <file_name> ' + \
          '-n <trim_idx> '+ \
          ' [--output=<file_name>]')

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FILE = None
TRIM_IDX = None
OUT_FILE = None
try:
    opts, args = getopt.getopt(argv,"hi:n:",["output="])
except getopt.GetoptError as e:
    full_usage()
    print(e)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-i"):
        IN_FILE = arg
    elif opt in ("-n"):
        TRIM_IDX = int(arg)
    elif opt in ("--output"):
        OUT_FILE = arg
if None in (IN_FILE, TRIM_IDX):
    full_usage()
    sys.exit(3)

with open(IN_FILE,'rb') as in_stream:
    dens = dill.load(in_stream)

# Get data
tvec_proc = dens.time_vec_proc.copy()
Xt = dens.Xt.copy()
indexObs_proc = dens.indexObs_proc.copy()
obs = dens.dataObs.copy()
sigma = dens.sigma
c0 = dens.c0
tvec = dens.time_vec.copy()

# Find index in process
proc_idx = np.where(tvec[TRIM_IDX-1] == tvec_proc)[0][0]

# Trim process
tvec_proc = tvec_proc[:proc_idx+1]
Xt = Xt[:proc_idx+1]
indexObs_proc = indexObs_proc[ indexObs_proc <= proc_idx+1 ]
obs = obs[:len(indexObs_proc)]

new_dens = CDDENS.ConditionedDiffusion(tvec_proc, Xt, TRIM_IDX, indexObs_proc, obs,
                                       sigma, c0)

# Plot original dynamics and observations
plt.figure()
plt.ylim(-2,2)
plt.xlim(0,dens.time_vec[-1])
plt.plot(dens.time_vec_proc, dens.Xt, '--k')
plt.plot(dens.time_vec[dens.indexObs], dens.dataObs, 'or')
plt.show(False)

# Plot new dynamics and observations
plt.figure()
plt.ylim(-2,2)
plt.xlim(0,new_dens.time_vec[-1])
plt.plot(new_dens.time_vec_proc, new_dens.Xt, '--k')
plt.plot(new_dens.time_vec[new_dens.indexObs], new_dens.dataObs, 'or')
plt.show(False)

if OUT_FILE is not None:
    if sys.version_info[0] == 3:
        sel = input("Store? [y/N]")
    else:
        sel = raw_input("Store? [y/N]")
    if sel == 'y' or sel == 'Y':
        new_dens.store(OUT_FILE)