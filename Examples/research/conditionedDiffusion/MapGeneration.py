import numpy as np
import SpectralToolbox.Spectral1D as S1D
import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
    
def generate_lift_2d(dim, tm2d, idx):
    approx_list = []
    active_vars = []
    id_comp = FUNC.FrozenLinear(1,0.,1.)
    for d in range(dim):
        if d == idx:
            approx_list.append( tm2d.approx_list[0] )
            active_vars.append( [d] )
        elif d == idx+1:
            approx_list.append( tm2d.approx_list[1] )
            active_vars.append( [d-1, d] )
        else:
            approx_list.append( id_comp )
            active_vars.append( [d] )
    tm_lift = Maps.TriangularTransportMap(active_vars, approx_list)
    return tm_lift

def compose_Markovian_pullback(tm_list, target):
    r""" Takes list of Markovian lower triangular matrices and the target density and returns the corresponding pullback density.
    """
    composed_density = target
    for d, tm in enumerate(tm_list):
        # Build permutation map
        perm_lin_term = np.eye(target.dim)
        perm_lin_term[d,d] = 0.
        perm_lin_term[d+1,d+1] = 0.
        perm_lin_term[d,d+1] = 1.
        perm_lin_term[d+1,d] = 1.
        perm_const_term = np.zeros(target.dim)
        permutation_map = Maps.LinearTransportMap(perm_const_term, perm_lin_term)
        # Lift Transport Map
        tm_lift = generate_lift_2d(target.dim, tm, d)
        # Compositions
        composed_density = DENS.PullBackTransportMapDensity(permutation_map, composed_density)
        composed_density = DENS.PullBackTransportMapDensity(tm_lift, composed_density)
        composed_density = DENS.PullBackTransportMapDensity(permutation_map, composed_density)
    return composed_density

def compose_Markovian_tm(tm_list):
    r""" Takes list of Markovian lower triangular matrices and the target density and returns the corresponding composed transport map.
    """
    dim = len(tm_list) + 1
    for d, tm in enumerate(tm_list):
        # Build permutation map
        perm_lin_term = np.eye(dim)
        perm_lin_term[d,d] = 0.
        perm_lin_term[d+1,d+1] = 0.
        perm_lin_term[d,d+1] = 1.
        perm_lin_term[d+1,d] = 1.
        perm_const_term = np.zeros(dim)
        permutation_map = Maps.LinearTransportMap(perm_const_term, perm_lin_term)
        # Lift Transport Map
        tm_lift = generate_lift_2d(dim, tm, d)
        if d == 0:
            # Compositions
            composed_map = permutation_map
            composed_map = Maps.CompositeTransportMap(composed_map, tm_lift)
            composed_map = Maps.CompositeTransportMap(composed_map, permutation_map)
        else:
            composed_map = Maps.CompositeTransportMap(composed_map, permutation_map)
            composed_map = Maps.CompositeTransportMap(composed_map, tm_lift)
            composed_map = Maps.CompositeTransportMap(composed_map, permutation_map)
    return composed_map