#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import dill
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import dolfin as dol
import itertools

import TransportMaps as TM
import TransportMaps.FiniteDifference as FD

import TransportMaps.Distributions.Examples.PoissonProblem as PP

def usage():
    print("""python GaussianFieldDataGeneration.py --output=<filename> [ 
   --ndiscr=20 
   --n-sens=4 --sens-geo-std=0.1
   --field=<field> --problem=<problem>
   --prior-mean-field-file=<fname> --prior-mean=0. --prior-var=1.
   --prior-cov=ou --prior-corr-len=1
   --lkl-var=1e-3 --lkl-cov=id --lkl-corr-len=1e-1
   --no-plotting""")
    print(
"""Available covariances:
 - id      Gaussian noise (identity)
 - ou      Ornstein-Uhlenbeck (exponential)
 - sqexp   Squared exponential""")
    print(
        "Available fields: \n" + \
        " - prior      Generate from prior [default] \n" + \
        " - square     Square feature on top left corner\n" + \
        " - tc         Setting from TC (obstruction)"
    )
    print(
        "Available problems: \n" + \
        " - 1: (x[0]=0 -> u[x]=0), (x[0]=1 -> u[x]=1), Newmann otherwise"
    )

stg = TM.DataStorageObject()

argv = sys.argv[1:]
OUT_FNAME = None
# Physics settings
stg.NDISCR = 20
stg.N_SENS = 4
stg.SENS_GEO_STD = 0.1
stg.FIELD = 'prior'
stg.PROBLEM = 1
# Prior settings
stg.PRIOR_MEAN = 0.
stg.PRIOR_VAR = 1.
stg.PRIOR_COV = 'ou'
stg.PRIOR_CORR_LEN = 1.
# Likelihood settings
stg.LKL_VAR = 1e-3
stg.LKL_COV = 'id'
stg.LKL_CORR_LEN = 1e-1
PLOT=True
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "output=",
            # Physics settings
            "ndiscr=",
            "n-sens=", "sens-geo-std=",
            "field=", "problem=",
            # Prior settings
            "prior-mean=", "prior-var=", "prior-cov=", "prior-corr-len=",
            # Likelihood settings
            "lkl-var=", "lkl-cov=", "lkl-corr-len=",
            "no-plotting"
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--output':
        OUT_FNAME = arg

    # Physics settings
    elif opt == '--ndiscr':
        stg.NDISCR = int(arg)
    elif opt == '--n-sens':
        stg.N_SENS = int(arg)
    elif opt == '--sens-geo-std':
        stg.SENS_GEO_STD = float(arg)
    elif opt == '--field':
        stg.FIELD = arg
    elif opt == '--problem':
        stg.PROBLEM = int(arg)

    # Prior settings
    elif opt == '--prior-mean':
        stg.PRIOR_MEAN = float(arg)
    elif opt == '--prior-var':
        stg.PRIOR_VAR = float(arg)
    elif opt == '--prior-cov':
        stg.PRIOR_COV = arg
        if stg.PRIOR_COV not in ['sqexp', 'ou']:
            raise NotImplementedError("The selected covariance is not implemented")
    elif opt == '--prior-corr-len':
        stg.PRIOR_CORR_LEN = float(arg)
        
    # Likelihood settings
    elif opt == '--lkl-var':
        stg.LKL_VAR = float(arg)
    elif opt == '--lkl-cov':
        stg.LKL_COV = arg
    elif opt == '--lkl-corr-len':
        stg.LKL_CORR_LEN = float(arg)

    else:
        raise AttributeError("Unknown option %s" % opt)
if None in [OUT_FNAME]:
    usage()
    sys.exit(2)

solver = PP.get_Poisson_problem_solver(stg.PROBLEM, stg.NDISCR)
prior_mean_field_vals = stg.PRIOR_MEAN * np.ones(solver.ndofs)
if stg.PRIOR_COV == 'ou':
    prior_cov = PP.OrnsteinUhlenbeckCovariance(stg.PRIOR_VAR, stg.PRIOR_CORR_LEN)
elif stg.PRIOR_COV == 'sqexp':
    prior_cov = PP.SquaredExponentialCovariance(stg.PRIOR_VAR, stg.PRIOR_CORR_LEN)
if stg.LKL_COV == 'ou':
    lkl_cov = PP.OrnsteinUhlenbeckCovariance(stg.LKL_VAR, stg.LKL_CORR_LEN)
elif stg.LKL_COV == 'sqexp':
    lkl_cov = PP.SquaredExponentialCovariance(stg.LKL_VAR, stg.LKL_CORR_LEN)
elif stg.LKL_COV == 'id':
    lkl_cov = PP.IdentityCovariance(stg.LKL_VAR)

if stg.FIELD != 'tc':
    # Sensors are positioned equally distant on the upper left corner [0.3,1]^2
    sens_sq = [0.3, 1.]
else:
    # Sensors are equally spaced
    sens_sq = [0., 1.]
sens_x_pos = np.linspace(sens_sq[0], sens_sq[1], stg.N_SENS+2)[1:-1]
sens_pos_list = list(itertools.product(sens_x_pos, sens_x_pos))

print("Sensors positions: " + str(sens_x_pos))
    
if stg.FIELD == 'prior':
    field_vals = None
elif stg.FIELD == 'square':
    # The generating field is
    # f(x) = 10 for x \in [0.2, 0.5] x [0.6, 0.8]
    # f(x) = 1 otherwise
    class FieldExpression(dol.UserExpression):
        def eval(self, value, x):
            if 0.2 <= x[0] <= 0.5 and 0.6 <= x[1] <= 0.8:
                value[0] = 5.
            else:
                value[0] = 1.
        def value_shape(self):
            return (1,)
    field_expr = FieldExpression(element=solver.VE)
    field_vals = dol.project(field_expr, solver.VEFS).vector().get_local()
elif stg.FIELD == 'tc':
    # Field suggested by TC
    class FieldExpression(dol.UserExpression):
        def eval(self, value, x):
            circ_cntr = np.array([.8, .2]) # Semi-circle center
            circ_rad = 0.6                 # Radius of the centerline of the obstruction
            circ_width = 0.03+1e-10        # Width of the obstruction
            sq_cntr = np.array([.7, .3])   # Center of the square
            sq_width = 0.05+1e-10          # Half-side of the square

            in_circ = x[0] < circ_cntr[0] + 1e-10 and x[1] > circ_cntr[1] - 1e-10 and \
                      (circ_rad - circ_width) < npla.norm(circ_cntr - x) < (circ_rad + circ_width)
            in_sq = sq_cntr[0] - sq_width < x[0] < sq_cntr[0] + sq_width and \
                    sq_cntr[1] - sq_width < x[1] < sq_cntr[1] + sq_width
            if in_circ or in_sq:
                value[0] = 1e-4
            else:
                value[0] = 1.
        def value_shape(self):
            return (1,)
    field_expr = FieldExpression(element=solver.VE)
    field_vals = dol.project(field_expr, solver.VEFS).vector().get_local()
else:
    raise ValueError("Field %s not recognized." % FIELD)

if stg.LKL_COV == 'id':
    pi = PP.GaussianFieldIndependentLikelihoodPoissonDistribution(
        # Physiscs settings
        solver,
        # Sensors definition
        sens_pos_list, stg.SENS_GEO_STD,
        # Prior settings
        prior_mean_field_vals=prior_mean_field_vals,
        prior_cov=prior_cov,
        # Likelihood settings
        lkl_std=np.sqrt(stg.LKL_VAR),
        # Field
        field_vals=field_vals
    )
else:    
    pi = PP.GaussianFieldPoissonDistribution(
        # Physiscs settings
        solver,
        # Sensors definition
        sens_pos_list, stg.SENS_GEO_STD,
        # Prior settings
        prior_mean_field_vals=prior_mean_field_vals,
        prior_cov=prior_cov,
        # Likelihood settings
        lkl_cov=lkl_cov,
        # Field
        field_vals=field_vals
    )

# Append generation options to the object
pi.options = stg
    
if pi.real_observations is None and PLOT:
    # Plot mesh
    plt.figure()
    dol.plot(solver.mesh)
    plt.show(False)

    sens_pos = np.asarray(pi.sens_pos_list)
    
    # Plot field
    dl = 1e-4
    vmin = np.min(pi.field_vals)
    vmax = np.max(pi.field_vals)
    levels = np.linspace((1-dl)*vmin, (1+dl)*vmax, 50)
    plt.figure()
    p = dol.plot(pi.true_field, levels=levels)
    plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
    plt.colorbar(p)
    plt.title('True field')
    plt.show(False)
    
    u = solver.solve(pi.true_field)
    plt.figure()
    p = dol.plot(u)
    plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
    plt.colorbar(p)
    plt.title('Solution')
    plt.show(False)

    # Plot observations
    pointwise_obs = [u(pos) for pos in sens_pos]
    noise_free_obs = pi.field_to_data_map.evaluate(
        pi.field_vals[np.newaxis,:])[0,:]
    obs = pi.logL.y
    plt.figure()
    plt.plot(pointwise_obs, 'go', label='pointwise')
    plt.plot(noise_free_obs, 'ko', label='w/o noise')
    plt.plot(obs, 'ro', label='with noise')
    plt.ylim(0., 1.)
    plt.legend(loc='best')
    plt.show(False)

    print("signal/noise: %.1f" % np.mean(np.abs(noise_free_obs/(obs - noise_free_obs))))

# Taylor test for derivatives
x = np.ones((1,pi.dim))

dx = np.tile(npr.randn(1,pi.dim), (1, 1))
TM.taylor_test(
    x, dx,
    # f=pi.log_pdf,
    # gf=pi.grad_x_log_pdf,
    # ahf=pi.action_hess_x_log_pdf
    f = pi.tuple_grad_x_log_pdf,
    fungrad=True
)

# pi.test_gradients(x)

# Store Distributions
pi.store(OUT_FNAME)
