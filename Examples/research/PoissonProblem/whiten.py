#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import time
import dill
import numpy as np
import numpy.random as npr
import numpy.linalg as npla
import matplotlib.pyplot as plt

import TransportMaps as TM
import src.TransportMaps.Maps as MAPS
import TransportMaps.Distributions as DISTS

def usage():
    print('whiten.py --input=<filename> --output=<filename> --pull-prior --factorization=<chol|eig> --truncate')

argv = sys.argv[1:]
IN_FNAME = None
OUT_FNAME = None
PULL_PRIOR = False
FACTORIZATION = 'chol' # 'eig'
TRUNCATE = False
try:
    opts, args = getopt.getopt(argv, "h", [
        'input=', 'output=',
        'pull-prior', 'factorization=', 'truncate'])
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
    elif opt == '--output':
        OUT_FNAME = arg
    elif opt == '--pull-prior':
        PULL_PRIOR = True
    elif opt == '--factorization':
        if arg not in ['chol', 'eig']:
            raise ValueError('factorization can be either of the following values: chol, eig')
        FACTORIZATION = arg
    elif opt == '--truncate':
        TRUNCATE = True
    else:
        raise ValueError('Unrecognized option ' + opt)
if None in [IN_FNAME]:
    usage()
    sys.exit(2)

if TRUNCATE and (FACTORIZATION != 'eig' or not PULL_PRIOR):
    print("The truncate option is only available for the eig factorization.")
    sys.exit(3)
    
with open(IN_FNAME, 'rb') as istr:
    pi = dill.load(istr)

M = pi.prior.transport_map
pi.prior = pi.prior.base_distribution

if PULL_PRIOR:
    mu = pi.prior.mu
    cov = pi.prior.covariance
    if FACTORIZATION == 'chol':
        lin = np.tril(npla.cholesky(cov))
        dim = pi.prior.dim
    elif FACTORIZATION == 'eig':
        w, v = npla.eigh(cov)
        srt_idxs = np.argsort(w)[::-1]
        w = w[srt_idxs]
        v = v[:,srt_idxs]
        if TRUNCATE:
            plt.figure()
            cmsm = np.cumsum(w)
            cmpr = cmsm/cmsm[-1]
            plt.plot(cmpr, 'o-')
            plt.grid(True)
            plt.title("Cumulative power")
            plt.show(False)
            p, success = TM.read_and_cast_input(
                "percentage (0,1] of cumulative power to be retained",
                float
            )
            if not success:
                print("Terminating")
                sys.exit(4)
            dim = next( i for i in range(len(cmpr)) if cmpr[i] >= p )
            print("Truncation dimension: %d" % dim)    
            lin = v[:,:dim] * np.sqrt(w)[np.newaxis,:dim]
        else:
            dim = pi.prior.dim
            lin = v * np.sqrt(w)[np.newaxis,:]
    L = Maps.AffineMap(c=mu, L=lin)
    M = Maps.CompositeMap(M, L)
    pi.prior = DISTS.StandardNormalDistribution( dim )

pi.whitening_map = M
pi.logL.compose( M )
pi.prior = DISTS.StandardNormalDistribution(dim)
pi.dim = dim    

pi.store(OUT_FNAME)
