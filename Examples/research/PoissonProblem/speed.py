#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import time
import dill

import TransportMaps as TM

def usage():
    print('speed.py --input=<filename> --nsamps=INT --nprocs=INT')

argv = sys.argv[1:]
IN_FNAME = None
NSAMPS = 10
NPROCS = 1
try:
    opts, args = getopt.getopt(argv, "h", [
        'input=', 'nsamps=', 'nprocs='
    ])
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
    elif opt == '--nsamps':
        NSAMPS = int(arg)
    elif opt == '--nprocs':
        NPROCS = int(arg)
    else:
        raise ValueError('Unrecognized option ' + opt)
if None in [IN_FNAME]:
    usage()
    sys.exit(2)

with open(IN_FNAME, 'rb') as istr:
    pi = dill.load(istr)

# Generate random sample
smpl = pi.prior.rvs( NSAMPS )

# Start MPI if necessary
mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start( NPROCS )

# Distribute pi across processes
TM.mpi_bcast_dmem(pi=pi, mpi_pool=mpi_pool)

# Distribute samples
def allocate_smpl(x):
    return x
TM.mpi_map_alloc_dmem(
    allocate_smpl,
    scatter_tuple=(['x'],[smpl]),
    dmem_key_out_list=['x'],
    mpi_pool=mpi_pool,
    concatenate=False
)

# # Distribute cache across processes
# def allocate_cache(x):
#     cache = {'tot_size': x.shape[0]}
#     return cache
# (cache,) = TM.mpi_map_alloc_dmem(
#     allocate_cache,
#     dmem_key_in_list=['x'],
#     dmem_arg_in_list=['x'],
#     dmem_val_in_list=[smpl],
#     dmem_key_out_list=['cache'],
#     mpi_pool=mpi_pool,
#     concatenate=False
# )

# # Gradient evaluation
# start = time.process_time()
# TM.mpi_map(
#     'grad_x_log_pdf',
#     dmem_key_in_list=['x'],
#     dmem_arg_in_list=['x'],
#     dmem_val_in_list=[smpl],
#     obj='pi',
#     obj_val=pi,
#     mpi_pool=mpi_pool
# )
# stop = time.process_time()
# dt = stop - start
# print("Evaluation time grad_x_log_pdf (w/o cache): %.2f f/s" % (NSAMPS/dt))

# Function evaluation
start = time.process_time()
TM.mpi_map(
    'log_pdf',
    dmem_key_in_list=['x'],
    dmem_arg_in_list=['x'],
    dmem_val_in_list=[smpl],
    # dmem_key_in_list=['cache','x'],
    # dmem_arg_in_list=['cache','x'],
    # dmem_val_in_list=[cache,smpl],
    obj='pi',
    obj_val=pi,
    mpi_pool=mpi_pool
)
stop = time.process_time()
dt = stop - start
print("Evaluation time log_pdf: %.2f f/s" % (NSAMPS/dt))

# Function/gradient evaluation
start = time.process_time()
TM.mpi_map(
    'tuple_grad_x_log_pdf',
    dmem_key_in_list=['x'],
    dmem_arg_in_list=['x'],
    dmem_val_in_list=[smpl],
    # dmem_key_in_list=['cache','x'],
    # dmem_arg_in_list=['cache','x'],
    # dmem_val_in_list=[cache,smpl],
    obj='pi',
    obj_val=pi,
    mpi_pool=mpi_pool
)
stop = time.process_time()
dt = stop - start
print("Evaluation time tuple_grad_x_log_pdf: %.2f f/s" % (NSAMPS/dt))
