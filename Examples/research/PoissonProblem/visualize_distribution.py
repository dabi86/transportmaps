import sys, getopt
import dill
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import dolfin as dol
import itertools

def usage():
    print("""python visualize_distribution.py --input=<filename>""")

argv = sys.argv[1:]
IN_FNAME = None
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "input=",
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
    else:
        raise AttributeError("Unknown option %s" % opt)

with open(IN_FNAME, 'rb') as istr:
    pi = dill.load(istr)

# Plot mesh
plt.figure()
dol.plot(pi.solver.mesh)
plt.show(False)

# Retrieve sensor locations
sens_pos = np.asarray(pi.sens_pos_list)

# Plot generating field and sensors
dl = 1e-4
vmin = np.min(pi.field_vals)
vmax = np.max(pi.field_vals)
levels = np.linspace((1-dl)*vmin, (1+dl)*vmax, 50)
plt.figure()
p = dol.plot(pi.true_field, levels=levels)
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.colorbar(p)
plt.title('Synthetic field')
plt.show(False)

# Plot Reference solution and sensors
u = pi.solver.solve(pi.true_field)
plt.figure()
p = dol.plot(u)
plt.scatter(sens_pos[:,0], sens_pos[:,1], c='r', s=10)
plt.colorbar(p)
plt.title('Synthetic solution')
plt.show(False)