import numpy as np
import matplotlib.pyplot as plt
import TransportMaps as TM
import TransportMaps.Densities as DENS

# Declare target density
mu = 3.
beta = 4.
target_density = DENS.GumbelDensity(mu, beta)

# Plot original PDF
x = np.linspace(-10., 40., 100).reshape((100,1))
pdf_exact = target_density.pdf(x)
fig = plt.figure()
ax_pdf = fig.gca()
ax_pdf.plot(x, pdf_exact)
plt.show(False)		

# Setup base density
base_density = DENS.StandardNormalDensity(1)

# Construct transport map
tm = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
	dim=1, order=5, span='total', active_vars=[[0]], btype='poly')

# Construct push forward object 
push_base = DENS.PushForwardTransportMapDensity(tm, base_density)

# Minimize KL Divergence with regularization
reg = {'type': 'L2', 'alpha': 1e-3}
log = push_base.minimize_kl_divergence(target_density, qtype=3, qparams=[10],
										regularization=reg, tol=1e-4, ders=2, 
										batch_size=[None, None, 5])

# Plot push forward density
ax_pdf.plot(x, push_base.pdf(x), '--')
plt.show(False)

# Construct pullback
pull_tar = DENS.PullBackTransportMapDensity(tm, target_density)

# Plotting the pullback (should look Gaussian)
x_std = np.linspace(-4,4,100).reshape((100,1))
plt.figure()
plt.plot(x_std, base_density.pdf(x_std))
plt.plot(x_std, pull_tar.pdf(x_std), '--')
plt.show()

# Plotting the transport map
plt.figure()
plt.plot(x_std, tm(x_std))
plt.show(False)

# Plotting the inverse map
plt.figure()
plt.plot(x, tm.inverse(x))
plt.show(False)


