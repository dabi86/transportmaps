import matplotlib.pyplot as plt
import numpy as np
import dolfin as dol
import HelmholtzModelDistributions as HMD

NE = 100
k = 2 * np.pi # 2.4 GHz / c
mesh = dol.UnitSquareMesh(NE,NE)
VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
VFS = dol.FunctionSpace(mesh, VE)

# One source (0.5,0), constant field
field = dol.Expression('10.', degree=2)
field_fun = dol.project(field, VFS)

src_pos_list = [(.4,.0)]

u = HMD.solve(k, VFS, field_fun, src_pos_list)

plt.figure()
dol.plot(u)
plt.show(False)

# One source (0.4, 0), field Gaussian+1 centered at (0.5,0.5)
field = dol.Expression('10*exp(-.5 * (pow(x[0]-.5,2)+pow(x[1]-.5,2)) / 10) + 1.', degree=2)
field_fun = dol.project(field, VFS)

src_pos_list = [(.4,.0)]

u = HMD.solve(k, VFS, field_fun, src_pos_list)

plt.figure()
dol.plot(u)
plt.show(False)

# Extract values on sensors (.25,1.), (.5,1.), (.75,1.)
sensors_pos_list = [(0.,1.), (.25,1.), (.5,1.), (.75,1.), (1.,1.)]
eps_sens = 1/float(NE)
# Compute normalization constants
unnorm_sensor_expr_str = '1./(2*pi)/pow(std,2) * ' + \
                         'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
unnorm_sensors_expr_list = [ dol.Expression( unnorm_sensor_expr_str,
                                             c0=p[0], c1=p[1], std=eps_sens, pi=np.pi,
                                             degree=2)
                             for p in sensors_pos_list ]
unnorm_sensor_fun_list = [ dol.project(s, VFS)
                           for s in unnorm_sensors_expr_list ]
nrm_const_list = [ dol.assemble(s * dol.dx) for s in unnorm_sensor_fun_list ]
# Construct normalized sensors
sensor_expr_str = '1./(2*pi)/pow(std,2)/nrm * ' + \
                  'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
sensors_expr_list = [ dol.Expression( sensor_expr_str,
                                      c0=p[0], c1=p[1], std=eps_sens,
                                      pi=np.pi, nrm=nrm,
                                      degree=2)
                      for p,nrm in zip(sensors_pos_list, nrm_const_list) ]

# Extract values
N = 100
xx = np.linspace(0,1,N)
vals = np.array([ u([x,1.]) for x in xx ])
# Compute with sensors
sens_vals = [ dol.assemble(u * s * dol.dx) for s in sensors_expr_list]

plt.figure()
plt.plot(xx, vals)
plt.plot([p[0] for p in sensors_pos_list], sens_vals, 'or')
plt.show(False)

# class SensorSubDomain(dol.SubDomain):
#     def __init__(self, c):
#         self.c = c
#         dol.SubDomain.__init__(self)
#     def inside(self, x, on_boundary):
#         return np.sqrt(sum((x-self.c)**2)) < 2e-2 # dol.DOLFIN_EPS_LARGE 
# sensors_markers = dol.MeshFunction('size_t', mesh, 2)
# sensors_markers.set_all(0)
# for i, pos in enumerate(sensors_pos_list):
#     ssd = SensorSubDomain(pos)
#     ssd.mark(sensors_markers, i+1)
# dx = dol.Measure('dx', domain=mesh, subdomain_data=sensors_markers)
# plt.figure(); dol.plot(sensors_markers); plt.show(False);