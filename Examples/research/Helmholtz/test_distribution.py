#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import time
import dill
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import dolfin as dol

import TransportMaps as TM
import HelmholtzModelDistributions as HMD

def usage():
    print('test_distribution.py --input=<filename>')

argv = sys.argv[1:]
IN_FNAME = None
try:
    opts, args = getopt.getopt(argv, "h", ['input='])
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--input':
        IN_FNAME = arg
if None in [IN_FNAME]:
    usage()
    sys.exit(2)

with open(IN_FNAME, 'rb') as istr:
    pi = dill.load(istr)

if pi.real_observations is None:
    # # Plot field
    # plt.figure()
    # dol.plot(pi.true_field)
    # plt.title('True field')
    # plt.show(False)

    for src_pos in pi.src_pos_list:
        u = HMD.solve(pi.k, pi.VFS, pi.true_field, src_pos)
        plt.figure()
        dol.plot(u)
        plt.title('Solution - source %s' % str(src_pos))
        plt.show(False)

# Taylor test for derivatives
npoints = 1
x = np.ones((npoints,pi.dim))
dx = np.tile(npr.randn(1,pi.dim), (npoints, 1))

TM.taylor_test(x, dx, h=1e-4,
               f = pi.tuple_grad_x_log_pdf,
               fungrad=True)

# # Timing
# pi.reset_counters()
# npoints = 10
# x = np.ones((npoints,pi.dim))
# dx = np.tile(npr.randn(1,pi.dim), (npoints, 1))

# cache = {'tot_size': npoints}
# print("Timing function/gradient evaluation")
# start = time.process_time()
# lpdf = pi.tuple_grad_x_log_pdf(x, cache=cache)
# lpdf_time = time.process_time() - start
# print("Function/gradient evaluation %.2f s" % (lpdf_time/npoints))
# print("Timing action Hessian")
# start = time.process_time()
# lpdf = pi.action_hess_x_log_pdf(x, dx, cache=cache)
# lpdf_time = time.process_time() - start
# print("Action Hessian evaluation %.2f s" % (lpdf_time/npoints))

print("")
print("TEST MPI")
print("")

x = pi.prior.rvs(2)
dx = npr.randn(pi.dim)

mpi_pool = TM.get_mpi_pool()
mpi_pool.start(2)

try:
    scatter_tuple= (['x'], [x])
    mpi_lpdf, mpi_gxlpdf = TM.mpi_map(
        'tuple_grad_x_log_pdf', scatter_tuple=scatter_tuple,
        obj=pi, mpi_pool=mpi_pool)
    lpdf, gxlpdf = pi.tuple_grad_x_log_pdf(x)
    assert np.all(np.allclose(mpi_lpdf, lpdf))
    assert np.all(np.allclose(mpi_gxlpdf, gxlpdf))
    mpi_lpdf = TM.mpi_map(
        'log_pdf', scatter_tuple=scatter_tuple,
        obj=pi, mpi_pool=mpi_pool)
    assert np.all(np.allclose(mpi_lpdf, lpdf))
    print("Success!")
finally:
    mpi_pool.stop()