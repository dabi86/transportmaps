import numpy as np
import numpy.linalg as npla
from scipy.spatial.distance import pdist, squareform
from petsc4py import PETSc
from mpi4py import MPI
import dolfin as dol
import dolfin_adjoint as doladj
import TransportMaps.Distributions as DIST
import TransportMaps.Distributions.Inference as DISTINF
import TransportMaps.Likelihoods as LKL
import src.TransportMaps.Maps as MAPS
from TransportMaps import counted, cached, cached_tuple

def solve(k, VFS, sf, src_pos_list):
# def solve(k, mesh, VE, sf, src_pos_list, eps_src=1e-2):
    if not isinstance(src_pos_list, list):
        src_pos_list = [src_pos_list]
    
    k = doladj.Constant(k)

    # VFS = dol.FunctionSpace(mesh, VE)
    # RE = dol.FiniteElement("R", dol.triangle, 0)
    # REFS = dol.FunctionSpace(mesh, RE)
    # VERE = dol.MixedElement([VE, RE])
    # W = dol.FunctionSpace(mesh, VERE)

    # Trial and test functions
    u = dol.TrialFunction(VFS)
    v = dol.TestFunction(VFS)

    # (u, c) = dol.TrialFunction(W)
    # (v, d) = dol.TestFunction(W)


    # Sources
    def source_bc_factory(pos):
        def source_bc(x, on_boundary):
            return dol.near(x[0], pos[0], dol.DOLFIN_EPS_LARGE) and \
                dol.near(x[1], pos[1], dol.DOLFIN_EPS_LARGE)
        return source_bc
    src_bc_list = [ dol.DirichletBC(
        VFS, doladj.Constant(1.0),
        source_bc_factory(pos), method='pointwise')
                    for pos in src_pos_list ]

    # ################
    # # Sources
    # # eps_src = 1./float(ndiscr)
    # # Compute normalization constants
    # unnorm_src_expr_str = '1./(2*pi)/pow(std,2) * ' + \
    #                       'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
    # unnorm_src_expr_list = [ dol.Expression(
    #     unnorm_src_expr_str, c0=p[0], c1=p[1], std=eps_src, pi=np.pi, degree=2)
    #                          for p in src_pos_list ]
    # unnorm_src_fun_list = [ dol.project(s, VFS)
    #                         for s in unnorm_src_expr_list ]
    # nrm_const_list = [ dol.assemble(s * dol.dx) for s in unnorm_src_fun_list ]
    # # Construct normalized sources
    # src_expr_str = '1./(2*pi)/pow(std,2)/nrm * ' + \
    #                'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
    # src_expr_list = [ dol.Expression(
    #     src_expr_str, c0=p[0], c1=p[1], std=eps_src,
    #     pi=np.pi, nrm=nrm, degree=2)
    #                   for p,nrm in zip(src_pos_list, nrm_const_list) ]
    # ################
        
    # Equation
    # LagMult = (c*v + u*d) * dol.ds
    lhs = ( k**2 * dol.inner(u, v * sf) - \
            dol.inner(dol.nabla_grad(u), dol.nabla_grad(v))) * dol.dx # + \
        # LagMult
    rhs = doladj.Constant(0.0) * v * dol.dx
    # rhs = - sum([ src_expr * v * dol.dx for src_expr in src_expr_list ])
    
    # Assemble system
    LHS, RHS = doladj.assemble_system(lhs, rhs, src_bc_list)
    # LHS, RHS = doladj.assemble_system(lhs, rhs)

    # Solve system
    u = doladj.Function(VFS)
    doladj.solve(LHS, u.vector(), RHS)

    # w = doladj.Function(W)
    # doladj.solve(LHS, w.vector(), RHS)
    # (u, _) = w.split()
    
    return u

# def build_sensors_expr_list(sensors_pos_list, ndiscr, VFS):
def build_sensors_expr_list(sensors_pos_list, VFS, eps_sens=1e-2):
    # eps_sens = 1./float(ndiscr)
    # Compute normalization constants
    unnorm_sensor_expr_str = '1./(2*pi)/pow(std,2) * ' + \
                             'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
    unnorm_sensors_expr_list = [ dol.Expression( unnorm_sensor_expr_str,
                                                 c0=p[0], c1=p[1], std=eps_sens, pi=np.pi,
                                                 degree=2)
                                 for p in sensors_pos_list ]
    unnorm_sensor_fun_list = [ dol.project(s, VFS)
                               for s in unnorm_sensors_expr_list ]
    nrm_const_list = [ dol.assemble(s * dol.dx) for s in unnorm_sensor_fun_list ]
    # Construct normalized sensors
    sensor_expr_str = '1./(2*pi)/pow(std,2)/nrm * ' + \
                      'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
    sensors_expr_list = [ dol.Expression( sensor_expr_str,
                                          c0=p[0], c1=p[1], std=eps_sens,
                                          pi=np.pi, nrm=nrm,
                                          degree=2)
                          for p,nrm in zip(sensors_pos_list, nrm_const_list) ]
    return sensors_expr_list
    
def build_reduced_functionals(u, sensors_expr_list, fname='slowness', isconst=True):
    J_list = [ doladj.Functional(s * u * dol.dx)
               for s in sensors_expr_list ]
    if isconst:
        cntr = doladj.ConstantControl(fname)
    else:
        cntr = doladj.FunctionControl(fname)
    rf_list = [
        doladj.ReducedFunctional(J, cntr)
        for J in J_list ]
    return rf_list

def ou_cov(var, dists, l):
    return var * np.exp( - dists / l )

def sqexp_cov(var, dists, l):
    return var * np.exp( - dists**2 / 2 / l**2 )

def id_cov(var, n):
    return var * np.eye(n)
    
class HelmholtzFieldPosteriorDistribution(DISTINF.BayesPosteriorDistribution):

    required_init_args = set([
        # Physics settings
        'ndiscr', 'src_pos_list', 'sens_pos_list', 'k',
        'real_observations', 'field_vals',
        # Prior settings
        'prior_mean_field_vals', 'prior_var',
        'prior_cov', 'prior_corr_len',
        # Likelihood settings
        'lkl_var', 'lkl_cov', 'lkl_corr_len'
    ])

    def __init__(self, **kwargs):
        # Check all the input arguments have been provided
        if not HelmholtzFieldPosteriorDistribution.required_init_args <= set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(HelmholtzFieldPosteriorDistribution.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # List of attributes to be excluded in get/set params
        self.exclude_state_param_list = set()
        
        self.build_mesh_and_function_space()

        # Build the field to data maps
        self.field_to_data_maps_list = []
        for src_pos in self.src_pos_list:
            self.field_to_data_maps_list.append(
                HelmholtzFieldToDataMap(
                    VFS=self.VFS, ndiscr=self.ndiscr, ndofs=self.ndofs,
                    k=self.k, src_pos=src_pos, sens_pos_list=self.sens_pos_list
                ) )

        # Build prior (log-GP)
        if self.prior_cov in ['ou', 'sqexp']:
            dists = squareform( pdist(self.coord) )
        if self.prior_cov == 'id':
            cov = id_cov(self.prior_var, self.ndofs)
        if self.prior_cov == 'ou':
            cov = ou_cov(self.prior_var, dists, self.prior_corr_len)
        if self.prior_cov == 'sqexp':
            cov = sqexp_cov(self.prior_var, dists, self.prior_corr_len)
        prior = DIST.PushForwardTransportMapDistribution(
            Maps.FrozenExponentialDiagonalTransportMap(self.ndofs),
            DIST.GaussianDistribution(np.zeros(self.ndofs), cov) )
        if self.prior_mean_field_vals is not None:
            raise NotImplementedError()

        # Build the slowness field if no-observation was provided
        if self.real_observations is None:
            if self.field_vals is None:
                self.field_vals = prior.rvs(1)
            self.set_true_field()

        # Generate the observations
        if self.real_observations is None:
            # Synthetic observations
            y_list = [ f.evaluate(self.field_vals)[0,:]
                       for f in self.field_to_data_maps_list ]
        else:
            y_list = self.real_observations

        # Generate likelihoods
        nsens = len(self.sens_pos_list)
        if self.lkl_cov in ['ou','sqexp']:
            sens_dists = squareform( pdist(self.sens_pos_list) )
        lkl = LKL.IndependentLogLikelihood([])
        for y, f in zip(y_list, self.field_to_data_maps_list):
            # Create observational noise distribution (Gaussian)
            if self.lkl_cov == 'id':
                cov = id_cov(self.lkl_var, nsens)
            if self.lkl_cov == 'ou':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            if self.lkl_cov == 'sqexp':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            pi_noise = DIST.GaussianDistribution(np.zeros(nsens), cov)
            # If observations are synthetic, corrupt them with noise
            if self.real_observations is None:
                y += pi_noise.rvs(1)[0,:]
            # Build likelihood
            add_lkl = LKL.AdditiveLogLikelihood(y, pi_noise, f)
            # Add the factor to the general likelihood
            lkl.append( (add_lkl, range(self.ndofs)) )

        # Call super to assemble posterior distribution
        super(HelmholtzFieldPosteriorDistribution, self).__init__(lkl, prior)

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)
        self.build_mesh_and_function_space()
        # Reset fields in fields to data map list
        for ftdm in self.field_to_data_maps_list:
            ftdm.VFS = self.VFS
        if self.real_observations is None:
            self.set_true_field()
                
    def build_mesh_and_function_space(self):
        # comm = PETSc.Comm(MPI.COMM_SELF)
        # self.mesh = dol.UnitSquareMesh(comm, self.ndiscr, self.ndiscr)
        self.mesh = dol.UnitSquareMesh(dol.mpi_comm_self(), self.ndiscr, self.ndiscr)
        self.VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
        self.VFS = dol.FunctionSpace(self.mesh, self.VE)
        self.coord = self.VFS.tabulate_dof_coordinates().reshape((-1,2))
        self.ndofs = self.coord.shape[0]
        self.exclude_state_param_list |= set(['mesh', 'VE', 'VFS', 'coord', 'ndofs'])

    def set_true_field(self):
        self.true_field = doladj.Function(self.VFS)
        self.true_field.vector().set_local(
            self.field_vals[0,:], np.arange(self.ndofs, dtype=np.intc))
        self.true_field.vector().apply('insert')
        self.exclude_state_param_list.add( 'true_field' )

class HelmholtzFieldToDataMap(Maps.Map):

    required_init_args = set([
        'VFS', 'ndiscr', 'ndofs', 'k', 'src_pos', 'sens_pos_list' ])

    def __init__(self, **kwargs):
        if not HelmholtzFieldToDataMap.required_init_args <= set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(HelmholtzFieldToDataMap.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # List of exluded parameters
        self.exclude_state_param_list = set(['VFS'])
        # Init super
        super(HelmholtzFieldToDataMap, self).__init__(
            self.ndofs, len(self.sens_pos_list))

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)
        self.rfs = None
        
    def new_reduced_functionals(self, field, fname):
        # Solve system once to get data and set up reduced functionals
        sensors_expr_list = build_sensors_expr_list(
            self.sens_pos_list, self.ndiscr, self.VFS)
        u = solve(self.k, self.VFS, field, self.src_pos)
        return build_reduced_functionals(u, sensors_expr_list, fname=fname, isconst=False)

    @cached()
    @counted
    def evaluate(self, x, *args, **kwargs):
        doladj.adj_reset()
        x = np.asarray( x, order='C' )
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        for i in range(m):
            # Define dolfin field
            if 'rfs' not in locals():
                fname = 'slowness'
                field = doladj.Function(self.VFS, name=fname)
            else:
                field = dol.Function(self.VFS)
            field.vector().set_local(
                x[i,:], np.arange(self.ndofs, dtype=np.intc))
            field.vector().apply('insert')
            # Generate new reduced functional if necessary
            if 'rfs' not in locals():
                rfs = self.new_reduced_functionals(field, fname)
            # Evaluate functional
            for j, rf in enumerate( rfs ):
                fx[i,j] = rf(field)
        return fx
        
    @cached_tuple(['evaluate', 'grad_x'])
    @counted
    def tuple_grad_x(self, x, *args, **kwargs):
        doladj.adj_reset()
        x = np.asarray( x, order='C' )
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        gfx = np.zeros((m,self.dim_out,self.dim_in))
        for i in range(m):
            # Define dolfin field
            if 'rfs' not in locals():
                fname = 'slowness'
                field = doladj.Function(self.VFS, name=fname)
            else:
                field = dol.Function(self.VFS)
            field.vector().set_local(
                x[i,:], np.arange(self.ndofs, dtype=np.intc))
            field.vector().apply('insert')
            # Generate new reduced functional if necessary
            if 'rfs' not in locals():
                rfs = self.new_reduced_functionals(field, fname)
            # Evaluate functional
            for j, rf in enumerate( rfs ):
                fx[i,j] = rf(field)
                dJds = rf.derivative()[0]
                gfx[i,j,:] = dJds.vector()[:]
        return fx, gfx
        
    @counted
    def action_hess_x(self, x, dx, *args, **kwargs):
        doladj.adj_reset()
        x = np.asarray( x, order='C' )
        dx = np.asarray( dx, order='C' )
        m = x.shape[0]
        out = np.zeros((m,self.dim_out,self.dim_in))
        for i in range(m):
            # Define dolfin field
            if 'rfs' not in locals():
                fname = 'slowness'
                field = doladj.Function(self.VFS, name=fname)
            else:
                field = dol.Function(self.VFS)
            field.vector().set_local(
                x[i,:], np.arange(self.ndofs, dtype=np.intc))
            field.vector().apply('insert')
            # Generate new reduced functional if necessary
            if 'rfs' not in locals():
                rfs = self.new_reduced_functionals(field, fname)
            # Define dolfin perturbation
            dxfield = doladj.Function(self.VFS)
            dxfield.vector().set_local(
                dx[i,:], np.arange(self.ndofs, dtype=np.intc))
            dxfield.vector().apply('insert')
            # Evaluate functional
            for j, rf in enumerate( rfs ):
                rf(field)
                d2Jd2s = rf.hessian(dxfield)
                out[i,j,:] = d2Jd2s.vector()[:]
        return out

class HelmholtzConstantFieldPosteriorDistribution(DISTINF.BayesPosteriorDistribution):

    required_init_args = set([
        # Physics settings
        'ndiscr', 'layer_size', 'src_pos_list', 'sens_pos_list', 'k',
        'real_observations', 'field_vals',
        # Prior settings
        'prior_mean', 'prior_var',
        # Likelihood settings
        'lkl_var', 'lkl_cov', 'lkl_corr_len'
    ])

    def __init__(self, **kwargs):
        # Check all the input arguments have been provided
        if not HelmholtzConstantFieldPosteriorDistribution.required_init_args <= set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(HelmholtzConstantFieldPosteriorDistribution.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # List of attributes to be excluded in get/set params
        self.exclude_state_param_list = set()
        
        self.build_mesh_and_function_space()

        # Build the field to data maps
        self.field_to_data_maps_list = []
        for src_pos in self.src_pos_list:
            self.field_to_data_maps_list.append(
                HelmholtzConstantFieldToDataMap(
                    VFS=self.VFS, ndiscr=self.ndiscr, layer_size=self.layer_size,
                    k=self.k, src_pos=src_pos, sens_pos_list=self.sens_pos_list
                ) )

        # Build prior (log-Gaussian)
        prior = DIST.PushForwardTransportMapDistribution(
            Maps.FrozenExponentialDiagonalTransportMap(1),
            DIST.GaussianDistribution(
                self.prior_mean * np.ones(1),
                self.prior_var * np.eye(1)) )

        # Build the slowness field if no-observation was provided
        if self.real_observations is None:
            if self.field_vals is None:
                self.field_vals = prior.rvs(1)
            self.set_true_field()

        # Generate the observations
        if self.real_observations is None:
            # Synthetic observations
            y_list = [ f.evaluate(self.field_vals)[0,:]
                       for f in self.field_to_data_maps_list ]
        else:
            y_list = self.real_observations

        # Generate likelihoods
        nsens = len(self.sens_pos_list)
        if self.lkl_cov in ['ou','sqexp']:
            sens_dists = squareform( pdist(self.sens_pos_list) )
        lkl = LKL.IndependentLogLikelihood([])
        for y, f in zip(y_list, self.field_to_data_maps_list):
            # Create observational noise distribution (Gaussian)
            if self.lkl_cov == 'id':
                cov = id_cov(self.lkl_var, nsens)
            if self.lkl_cov == 'ou':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            if self.lkl_cov == 'sqexp':
                cov = ou_cov(self.lkl_var, dists, self.lkl_corr_len)
            pi_noise = DIST.GaussianDistribution(np.zeros(nsens), cov)
            # If observations are synthetic, corrupt them with noise
            if self.real_observations is None:
                y += pi_noise.rvs(1)[0,:]
            # Build likelihood
            add_lkl = LKL.AdditiveLogLikelihood(y, pi_noise, f)
            # Add the factor to the general likelihood
            lkl.append( (add_lkl, range(1)) )

        # Call super to assemble posterior distribution
        super(HelmholtzConstantFieldPosteriorDistribution, self).__init__(lkl, prior)

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)
        self.build_mesh_and_function_space()
        # Reset fields in fields to data map list
        for ftdm in self.field_to_data_maps_list:
            ftdm.VFS = self.VFS
        if self.real_observations is None:
            self.set_true_field()
                
    def build_mesh_and_function_space(self):
        # comm = PETSc.Comm(MPI.COMM_SELF)
        # self.mesh = dol.UnitSquareMesh(comm, self.ndiscr, self.ndiscr)
        self.mesh = dol.UnitSquareMesh(dol.mpi_comm_self(), self.ndiscr, self.ndiscr)
        self.VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
        self.VFS = dol.FunctionSpace(self.mesh, self.VE)
        self.coord = self.VFS.tabulate_dof_coordinates().reshape((-1,2))
        self.ndofs = self.coord.shape[0]
        self.exclude_state_param_list |= set(['mesh', 'VE', 'VFS', 'coord', 'ndofs'])

    def set_true_field(self):
        self.true_field = doladj.Constant(self.field_vals[0,0])
        self.exclude_state_param_list.add( 'true_field' )
        
class HelmholtzConstantFieldToDataMap(Maps.Map):

    required_init_args = set([
        'VFS', 'ndiscr', 'layer_size', 'k', 'src_pos', 'sens_pos_list' ])

    fields_counter = 0

    def __init__(self, **kwargs):
        if not HelmholtzConstantFieldToDataMap.required_init_args <= set(kwargs.keys()):
            raise ValueError(
                "The required arguments " + \
                str(HelmholtzConstantFieldToDataMap.required_init_args - \
                    set(kwargs.keys())) + \
                " are missing.")
        # Store all input arguments
        vars(self).update(kwargs)
        # Start with an empty list of reduced functionals
        self.rf_list = []
        # List of exluded parameters
        self.exclude_state_param_list = set(['VFS', 'rf_list'])
        # Init super
        super(HelmholtzConstantFieldToDataMap, self).__init__(
            1, len(self.sens_pos_list))

    def __getstate__(self):
        out = {}
        for key, val in vars(self).items():
            if key not in self.exclude_state_param_list:
                out[key] = val
        return out

    def __setstate__(self, state):
        vars(self).update(state)
        self.rf_list = []
        
    def new_reduced_functionals(self, field, fname):
        # Solve system once to get data and set up reduced functionals
        sensors_expr_list = build_sensors_expr_list(
            self.sens_pos_list, self.ndiscr, self.VFS)
        u = solve(self.k, self.VFS, field, self.src_pos)
        return build_reduced_functionals(u, sensors_expr_list, fname=fname, isconst=True)

    @cached()
    @counted
    def evaluate(self, x, *args, **kwargs):
        # print("entered evaluate")
        # print("x.shape: %s" % str(x.shape))
        doladj.adj_reset()
        x = np.asarray( x, order='C' )
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        # print("Entering loop - x.shape: %s" % str(x.shape))
        for i in range(m):
            # Define dolfin field
            if 'rfs' not in locals():
                fname = 'slowness'
                field = doladj.Constant(x[i,0], name=fname)
            else:
                field = dol.Constant(x[i,0])
            # Generate new reduced functional if necessary
            if 'rfs' not in locals():
                rfs = self.new_reduced_functionals(field, fname)
            # Evaluate functional
            # print("Evaluating functional %i - x.shape: %s" % (i,str(x.shape)))
            for j, rf in enumerate( rfs ):
                fx[i,j] = rf(field)
        # print("Exiting evaluate")
        return fx
        
    @cached_tuple(['evaluate', 'grad_x'])
    @counted
    def tuple_grad_x(self, x, *args, **kwargs):
        doladj.adj_reset()
        x = np.asarray( x, order='C' )
        m = x.shape[0]
        fx = np.zeros((m,self.dim_out))
        gfx = np.zeros((m,self.dim_out,self.dim_in))
        for i in range(m):
            # Define dolfin field
            if 'rfs' not in locals():
                fname = 'slowness'
                field = doladj.Constant(x[i,0], name=fname)
            else:
                field = dol.Constant(x[i,0])
            # Generate new reduced functional if necessary
            if 'rfs' not in locals():
                rfs = self.new_reduced_functionals(field, fname)
            # Evaluate functional
            for j, rf in enumerate( rfs ):
                fx[i,j] = rf(field)
                dJds = rf.derivative()[0]
                gfx[i,j,0] = dJds(0)
        return fx, gfx

    @counted
    def action_hess_x(self, x, dx, *args, **kwargs):
        doladj.adj_reset()
        x = np.asarray( x, order='C' )
        dx = np.asarray( dx, order='C' )
        m = x.shape[0]
        out = np.zeros((m,self.dim_out,self.dim_in))
        for i in range(m):
            # Define dolfin field
            if 'rfs' not in locals():
                fname = 'slowness'
                field = doladj.Constant(x[i,0], name=fname)
            else:
                field = dol.Constant(x[i,0])
            # Generate new reduced functional if necessary
            if 'rfs' not in locals():
                rfs = self.new_reduced_functionals(field, fname)
            # Define dolfin perturbation
            dxfield = doladj.Constant(dx[i,0])
            # Evaluate functional
            for j, rf in enumerate( rfs ):
                rf(field)
                d2Jd2s = rf.hessian(dxfield)
                out[i,j,0] = d2Jd2s.vector()[:]
        return out
        