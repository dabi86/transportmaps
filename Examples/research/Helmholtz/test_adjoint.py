import matplotlib.pyplot as plt
import numpy as np
import dolfin as dol
import dolfin_adjoint as doladj
import HelmholtzModelDistributions as HMD

NE = 100
k = 2 * np.pi # 2.4 GHz / c
mesh = dol.UnitSquareMesh(NE,NE)
VE = dol.FiniteElement("Lagrange", dol.triangle, 1)
VFS = dol.FunctionSpace(mesh, VE)

# One source (0.4, 0), field Gaussian+1 centered at (0.5,0.5)
field = dol.Expression('10*exp(-.5 * (pow(x[0]-.5,2)+pow(x[1]-.5,2)) / 10) + 1.', degree=2)
field_fun = doladj.project(field, VFS, name='slowness')

src_pos_list = [(.4,.0)] # Make sure sources are in discretization

u = HMD.solve(k, VFS, field_fun, src_pos_list)

plt.figure()
dol.plot(u)
plt.show(False)

####
# Define sensors
####
sensors_pos_list = [(0.,1.), (.25,1.), (.5,1.), (.75,1.), (1.,1.)]
eps_sens = 1./float(NE)
# Compute normalization constants
unnorm_sensor_expr_str = '1./(2*pi)/pow(std,2) * ' + \
                         'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
unnorm_sensors_expr_list = [ dol.Expression( unnorm_sensor_expr_str,
                                             c0=p[0], c1=p[1], std=eps_sens, pi=np.pi,
                                             degree=2)
                             for p in sensors_pos_list ]
unnorm_sensor_fun_list = [ dol.project(s, VFS)
                           for s in unnorm_sensors_expr_list ]
nrm_const_list = [ dol.assemble(s * dol.dx) for s in unnorm_sensor_fun_list ]
# Construct normalized sensors
sensor_expr_str = '1./(2*pi)/pow(std,2)/nrm * ' + \
                  'exp(-.5 * (pow(x[0]-c0,2)+pow(x[1]-c1,2)) / pow(std,2))'
sensors_expr_list = [ dol.Expression( sensor_expr_str,
                                      c0=p[0], c1=p[1], std=eps_sens,
                                      pi=np.pi, nrm=nrm,
                                      degree=2)
                      for p,nrm in zip(sensors_pos_list, nrm_const_list) ]


# # Compute gradient at sensors 
# dJ_dslow_list = HMD.grad_slowness(u, sensors_pos_list)

# for dJ_dslow in dJ_dslow_list:
#     plt.figure()
#     dol.plot(dJ_dslow)
#     plt.show(False)

# Build reduced functionals
rf_list = HMD.build_reduced_functionals(u, sensors_expr_list, field_fun)

taylor_list = [ rf.taylor_test(test_hessian=True)
                for rf in rf_list ]

# # Check whether updating field in forward solve,
# # cause an update in gradients and hessian
# field = dol.Expression('9*floor(4*fabs(x[0]-.5))*floor(4*fabs(x[1]-.5))+1', degree=2)
# field_fun = doladj.project(field, VFS)

# u = HMD.solve(k, VFS, field, src_pos_list)

# plt.figure()
# dol.plot(u)
# plt.show(False)

# dJ_dslow_list = [ rf.derivative(forget=False)[0]
#                   for rf in rf_list ]
# for dJ_dslow in dJ_dslow_list:
#     plt.figure()
#     dol.plot(dJ_dslow)
#     plt.show(False)

# hJ_dslow_list = [ rf.hessian(dJ)
#                   for rf, dJ in zip(rf_list, dJ_dslow_list) ]
# for hJ_dslow in hJ_dslow_list:
#     plt.figure()
#     dol.plot(hJ_dslow)
#     plt.show(False)
