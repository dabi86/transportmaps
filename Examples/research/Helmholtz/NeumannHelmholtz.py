import numpy as np
import numpy.linalg as npla
from dolfin import *

k = 2 * np.pi

## Problem data
k = Constant(k)  # 2.4 GHz / c

## Formulation
mesh = UnitSquareMesh(100, 100)
V = FunctionSpace(mesh, "Lagrange", 1)

E = TrialFunction(V)
v = TestFunction(V)

# # Boundary conditions
# point = Point(0.5, 0.5)
# f = PointSource(V, point)

class Mollifier(Expression):
    K = 0.074247753387961
    def __init__(self, p, eps, element):
        self.eps = eps
        self.eps2 = eps**2
        self.log_alpha = np.log(2) + np.log(np.pi) + np.log(self.K) + \
                         2*np.log(self.eps) - 1/self.eps2
        self.p = p
        self._element = element
    def eval(self, value, x):
        dst2 = np.sum((x - self.p)**2.)
        if dst2 < self.eps2:
            lv = - self.log_alpha - 1./(self.eps2 - dst2)
            value[0] = np.exp(lv)
        else:
            value[0] = 0.
    def value_shape(self):
        return (1,)
p = np.array([0.5,0.])
eps = 1e-6
psi = Mollifier(p, eps, element=V.ufl_element())

class SlownessField(Expression):
    def eval(self, value, x):
        if x[1] < 0.5:
            value[0] = 10.
        else:
            value[0] = 1.
    def value_shape(self):
        return (1,)
m = SlownessField(element=V.ufl_element())

# Equation
a = ( - k**2 * inner(E, v * m) + inner(nabla_grad(E), nabla_grad(v))) * dx
L = psi * v * dx

# Assemble system
print("Assembly: ...")
A, rhs = assemble_system(a, L)
print("Assembly: DONE")

# Solve system
E = Function(V)
E_vec = E.vector()
print("Solve: ...")
solve(A, E_vec, rhs)
print("Solve: DONE")

# Plot and export solution
plot(E, interactive=True)

print("Storage: ...")
file = File("data/NeumannHelmholtz-0.pvd")
file << E
print("Storage: DONE")