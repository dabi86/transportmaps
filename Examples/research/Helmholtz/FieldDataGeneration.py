#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import dill
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import dolfin as dol

import HelmholtzModelDistributions as HMD

def usage():
    print('FieldDataGeneration.py --output=<filename> [ ' + \
          '--ndiscr=100 --src-pos=0.4,0;0.,0.2 --sens-pos=0.25,1;0.5,1;0.75,1 ' + \
          '--wave-number=2pi --field-file=<fname> ' + \
          '--prior-mean-field-file=<fname> --prior-var=1e-1 ' + \
          '--prior-cov=ou --prior-corr-len=1e-1' + \
          '--lkl-var=1e-2 --lkl-cov=id --lkl-corr-len=1e-1')
    print(
"""Available covariances:
 - id      Gaussian noise (identity)
 - ou      Ornstein–Uhlenbeck (exponential)
 - sqexp   Squared exponential
""")

argv = sys.argv[1:]
OUT_FNAME = None
# Physics settings
NDISCR = 100
SRC_POS_LIST = [(0.4, 0.),(0.,0.2)]
SENS_POS_LIST = [(0.25,1),(0.5,1),(0.75,1)]
K = 2 * np.pi
FIELD_FNAME = None
# Prior settings
PRIOR_MEAN_FIELD_FNAME = None
PRIOR_VAR = 1e-1
PRIOR_COV = 'ou'
PRIOR_CORR_LEN = 1e-1
# Likelihood settings
LKL_VAR = 1e-2
LKL_COV = 'id'
LKL_CORR_LEN = 1e-1
try:
    opts, args = getopt.getopt(
        argv, "h",
        [
            "output=",
            # Physics settings
            "ndiscr=", "src-pos=", "sens-pos=",
            "wave-number=", "field-file=",
            # Prior settings
            "prior-mean-field-file=",
            "prior-var=", "prior-cov=", "prior-corr-len=",
            # Likelihood settings
            "lkl-var=", "lkl-cov=", "lkl-corr-len="
        ] )
except getopt.GetoptError as e:
    print(e)
    usage()
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        usage()
        sys.exit(0)
    elif opt == '--output':
        OUT_FNAME = arg

    # Physics settings
    elif opt == '--ndiscr':
        NDISCR = int(arg)
    elif opt == '--src-pos':
        SRC_POS_LIST = [eval(s) for s in arg.split(';')]
    elif opt == '--sens-pos':
        SENS_POS_LIST = [eval(s) for s in arg.split(';')]
    elif opt == '--wave-number':
        K = float(arg)
    elif opt == '--field-fname':
        FIELD_FNAME = arg

    # Prior settings
    elif opt == '--prior-mean-field-file':
        PRIOR_MEAN_FIELD_FNAME = arg
    elif opt == '--prior-var':
        PRIOR_VAR = float(arg)
    elif opt == '--prior-cov':
        PRIOR_COV = arg
        if PRIOR_COV not in ['sqexp', 'ou']:
            raise NotImplementedError("The selected covariance is not implemented")
    elif opt == '--prior-corr-len':
        PRIOR_CORR_LEN = float(arg)

    # Likelihood settings
    elif opt == '--lkl-var':
        LKL_VAR = float(arg)
    elif opt == '--lkl-cov':
        LKL_COV = arg
    elif opt == '--lkl-corr-len':
        LKL_CORR_LEN = float(arg)
if None in [OUT_FNAME]:
    usage()
    sys.exit(2)

observations=None
field_vals=None
prior_mean_field_vals=None
pi = HMD.HelmholtzFieldPosteriorDistribution(
    # Physiscs settings
    ndiscr=NDISCR, src_pos_list=SRC_POS_LIST,
    sens_pos_list=SENS_POS_LIST, k=K,
    real_observations=observations,
    field_vals=field_vals,
    # Prior settings
    prior_mean_field_vals=prior_mean_field_vals,
    prior_var=PRIOR_VAR, prior_cov=PRIOR_COV,
    prior_corr_len=PRIOR_CORR_LEN,
    # Likelihood settings
    lkl_var=LKL_VAR, lkl_cov=LKL_COV,
    lkl_corr_len=LKL_CORR_LEN
)


if pi.real_observations is None:
    # Plot field
    plt.figure()
    dol.plot(pi.true_field)
    plt.title('True field')
    plt.show(False)

    for src_pos in SRC_POS_LIST:
        u = HMD.solve(pi.k, pi.VFS, pi.true_field, src_pos)
        plt.figure()
        dol.plot(u)
        plt.title('Solution - source %s' % str(src_pos))
        plt.show(False)

# Store Distributions
with open(OUT_FNAME, 'wb') as ostr:
    dill.dump(pi, ostr)