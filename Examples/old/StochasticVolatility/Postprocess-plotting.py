#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import TransportMaps as TM
import sys, getopt
import os.path
import dill
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import scipy.stats as scistat
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS

def usage():
    print("Postprocess-plotting.py --input=<file_name> " + \
          "[--do-smooth --do-unb-smooth --do-filt " + \
          "--do-hyper-smooth --do-hyper-filt " + \
          "--do-aligned --do-random --do-marginals" + \
          "--do-orders --do-reg-orders --do-post-pred "  + \
          "--var-diag " + \
          "--store-fig-dir=None " + \
          "--nprocs=<num_processors>]")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
DO_SMOOTH = False
DO_UNBIASED_SMOOTH = False
DO_FILT = False
DO_HYPER_SMOOTH = False
DO_HYPER_FILT = False
DO_ALIGNED = False
DO_RANDOM = False
DO_MARGINALS = False
DO_ORDERS = False
DO_REG_ORDERS = False
DO_POST_PRED = False
DO_VAR_DIAG = False
STORE_FIG_DIR = None
try:
    opts, args = getopt.getopt(argv,"h",["input=",
                                         "do-aligned", "do-random", "do-marginals",
                                         "do-smooth", "do-unb-smooth", "do-filt",
                                         "do-hyper-smooth", "do-hyper-filt",
                                         "do-orders", 'do-reg-orders', 'do-post-pred',
                                         "do-var-diag",
                                         "store-fig-dir="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--input"):
        IN_FNAME = arg
    elif opt in "--do-smooth":
        DO_SMOOTH = True
    elif opt in "--do-unb-smooth":
        DO_UNBIASED_SMOOTH = True
    elif opt in "--do-filt":
        DO_FILT = True
    elif opt in "--do-hyper-smooth":
        DO_HYPER_SMOOTH = True
    elif opt in "--do-hyper-filt":
        DO_HYPER_FILT = True
    elif opt in "--do-marginals":
        DO_MARGINALS = True
    elif opt in "--do-aligned":
        DO_ALIGNED = True
    elif opt in "--do-random":
        DO_RANDOM = True
    elif opt in '--do-orders':
        DO_ORDERS = True
    elif opt in '--do-reg-orders':
        DO_REG_ORDERS = True
    elif opt in '--do-post-pred':
        DO_POST_PRED = True
    elif opt in '--do-var-diag':
        DO_VAR_DIAG = True
    elif opt in ("--store-fig-dir"):
        STORE_FIG_DIR = arg
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME, 'rb') as in_stream:
    data = dill.load(in_stream)

# Restore data
tm_approx_comp_list = data['tm_approx_comp_list']
target_density = data['target_density']
tm_approx_hyper_list = data['tm_approx_hyper_list']
regression_order_list = data['regression_order_list']
map_order_list = data['map_order_list']
base_density = data['base_density']
# push_base = data['push_base']
# tar_samp_smooth = data['tar_samp_smooth']
# Xt_samp_filt = data['Xt_samp_filt']
# hyper_samp_filt = data['hyper_samp_filt']
# Ysmooth_samples = data['Ysmooth_samples']
# var_diag = data['variance_diagnostic']

#Retrieve remaining variables
dim = target_density.dim
nsteps = target_density.nsteps
nhyper =sum(target_density.which_hyper)
index_X0 = target_density.index_X0
base_density = DENS.StandardNormalDensity(dim)

#Store plots
if STORE_FIG_DIR is not None:
    data2save = {}
    fig_folder = STORE_FIG_DIR
    fig_formats = ['eps', 'pdf', 'svg', 'png']
    tit_no_path = str.split(IN_FNAME,"/")[-1]
    title = '.'.join(str.split(tit_no_path,".")[:-1])
    def store_figure(fig, fname, fig_formats):
        for fmat in fig_formats:
            fig.savefig(fname+'.'+fmat, format=fmat, bbox_inches='tight')

if DO_VAR_DIAG:
    print("Variance diagnostic: %e" % data['variance_diagnostic'])
            
#--1--###### Plot: smoothing/posterior marginals timesteps #############
print("\nPlotting smoothing/posterior marginals \n")
if DO_SMOOTH:
    NTRAJ = 4
    tar_samp_smooth = data['tar_samp_smooth']
    Xt_samp_smooth = tar_samp_smooth[:,index_X0:]
    tvec = target_density.tvec
    meanXt_smooth = np.mean(Xt_samp_smooth, axis=0)
    q5_smooth = np.percentile(Xt_samp_smooth, 5, axis=0)
    q25_smooth = np.percentile(Xt_samp_smooth, 25, axis=0)
    q40_smooth = np.percentile(Xt_samp_smooth, 40, axis=0)
    q60_smooth = np.percentile(Xt_samp_smooth, 60, axis=0)
    q75_smooth = np.percentile(Xt_samp_smooth, 75, axis=0)
    q95_smooth = np.percentile(Xt_samp_smooth, 95, axis=0)
    fig1 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{X_t|Y_{0:N}}$",
                         title="Posterior/Smoothing marginals")
    ax1 = fig1.gca()
    #ax1.set_aspect('equal')
    #ax.set_ylim(-3,3)
    ax1.set_xlim(0,tvec[-1])
    #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
    #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
    #ax.plot(target_density.tobs, target_density.dataObs, 'or')
    ax1.plot(tvec, meanXt_smooth, '-r', linewidth=2)
    if NTRAJ == 0:
        ax1.fill_between(tvec, q40_smooth, q60_smooth, facecolor='r',
                         alpha=0.35, linewidth = 0.0)
    ax1.fill_between(tvec, q25_smooth, q75_smooth, facecolor='r',
                     alpha=0.25, linewidth = 0.0)
    ax1.fill_between(tvec, q5_smooth, q95_smooth, facecolor='r',
                     alpha=0.15, linewidth = 0.0)
    #plt.legend(loc='best')
    # Trajectories
    for i in range(NTRAJ):
        ax1.plot(tvec, Xt_samp_smooth[i,:], '-k', linewidth=0.5, alpha=0.6)
    plt.show(False)

    if STORE_FIG_DIR is not None:
        data2save.update({'fig:smoothing_marginals_timesteps_ntraj%d' % NTRAJ: fig1})
        store_figure(fig1, fig_folder+'/'+title + \
                     '-smoothing-marginals-timesteps-ntraj-%d' % NTRAJ,
                     fig_formats)

if DO_SMOOTH and DO_UNBIASED_SMOOTH:
    unb_tar_samp_smooth = data['unb_tar_samp_smooth']
    Xt_unb_samp_smooth = unb_tar_samp_smooth[:,index_X0:]
    meanXt_unb_smooth = np.mean(Xt_unb_samp_smooth, axis=0)
    q5_unb_smooth = np.percentile(Xt_unb_samp_smooth, 5, axis=0)
    q25_unb_smooth = np.percentile(Xt_unb_samp_smooth, 25, axis=0)
    q40_unb_smooth = np.percentile(Xt_unb_samp_smooth, 40, axis=0)
    q60_unb_smooth = np.percentile(Xt_unb_samp_smooth, 60, axis=0)
    q75_unb_smooth = np.percentile(Xt_unb_samp_smooth, 75, axis=0)
    q95_unb_smooth = np.percentile(Xt_unb_samp_smooth, 95, axis=0)
    # Plot vs. the approximated distribution
    fig = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{X_t|Y_{0:N}}$",
                        title="Posterior/Smoothing marginals - " + \
                        "vs. unbiased")
    ax1 = fig.gca()
    #ax1.set_aspect('equal')
    #ax.set_ylim(-3,3)
    ax1.set_xlim(0,tvec[-1])
    #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
    #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
    #ax.plot(target_density.tobs, target_density.dataObs, 'or')

    LW = 2
    # Unbiased distribution quantiles
    ax1.plot(tvec, meanXt_unb_smooth, '-k', linewidth=LW)
    ax1.plot(tvec, q25_unb_smooth, '--k', linewidth=LW)
    ax1.plot(tvec, q75_unb_smooth, '--k', linewidth=LW)
    ax1.plot(tvec, q5_unb_smooth, '-.k', linewidth=LW)
    ax1.plot(tvec, q95_unb_smooth, '-.k', linewidth=LW)
    # Approximated distribution quantiles
    ax1.plot(tvec, meanXt_smooth, '-r', linewidth=LW)
    ax1.plot(tvec, q25_smooth, '--r', linewidth=LW)
    ax1.plot(tvec, q75_smooth, '--r', linewidth=LW)
    ax1.plot(tvec, q5_smooth, '-.r', linewidth=LW)
    ax1.plot(tvec, q95_smooth, '-.r', linewidth=LW)
    #plt.legend(loc='best')
    plt.show(False)

    if STORE_FIG_DIR is not None:
        data2save.update({'fig:smoothing_marginals_vs_unbiased_timesteps':fig})
        store_figure(fig, fig_folder+'/'+title + \
                     '-smoothing-marginals-vs-unbiased-timesteps',
                     fig_formats)

#--2--###### Plot: filtering marginals timesteps #############
if DO_FILT:
    print("Plotting filtering marginals \n")
    Xt_samp_filt = data['Xt_samp_filt']
    meanXt_filt = np.mean(Xt_samp_filt, axis=0)
    q5_filt = np.percentile(Xt_samp_filt, 5, axis=0)
    q25_filt = np.percentile(Xt_samp_filt, 25, axis=0)
    q40_filt = np.percentile(Xt_samp_filt, 40, axis=0)
    q60_filt = np.percentile(Xt_samp_filt, 60, axis=0)
    q75_filt = np.percentile(Xt_samp_filt, 75, axis=0)
    q95_filt = np.percentile(Xt_samp_filt, 95, axis=0)
    fig2 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{X_t|Y_{0:t}}$",
                                      title='Filtering marginals')
    ax2 = fig2.gca()
    #ax2.set_aspect('equal')
    #ax.set_ylim(-3,3)
    ax2.set_xlim(0,tvec[-1])
    #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
    #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
    #ax.plot(target_density.tobs, target_density.dataObs, 'or')
    ax2.plot(tvec, meanXt_filt, '-b')
    ax2.fill_between(tvec, q40_filt, q60_filt, facecolor='b',
                     alpha=0.35, linewidth = 0.0)
    ax2.fill_between(tvec, q25_filt, q75_filt, facecolor='b',
                     alpha=0.25, linewidth = 0.0)
    ax2.fill_between(tvec, q5_filt, q95_filt, facecolor='b',
                     alpha=0.15, linewidth = 0.0)
    #plt.legend(loc='best')
    plt.show(False)

    if STORE_FIG_DIR is not None:
        data2save.update({'fig:filtering_marginals_timesteps':fig2})
        store_figure(fig2, fig_folder+'/'+title+'-filtering-marginals-timesteps',
                     fig_formats)

#--3--###### Plot: smoothing+filtering marginals timesteps #############
if DO_SMOOTH and DO_FILT:
    print("Plotting smoothing + filtering marginals \n")
    fig3 = DIAG.nicePlot([],[], xlabel="time", ylabel=" ",
                         title='Smoothing and filtering marginals')
    ax3 = fig3.gca()
    #ax3.set_aspect('equal')
    #ax.set_ylim(-3,3)
    ax3.set_xlim(0,tvec[-1])
    #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
    #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
    #ax.plot(target_density.tobs, target_density.dataObs, 'or')
    ax3.plot(tvec, meanXt_filt, '-b', label = "Filtering")
    ax3.fill_between(tvec, q25_filt, q75_filt, facecolor='b',
                     alpha=0.15, linewidth = 0.0)
    ax3.fill_between(tvec, q5_filt, q95_filt, facecolor='b',
                     alpha=0.05, linewidth = 0.0)

    ax3.plot(tvec, meanXt_smooth, '-r', label = "Smoothing")
    ax3.fill_between(tvec, q25_smooth, q75_smooth, facecolor='r',
                     alpha=0.15, linewidth = 0.0)
    ax3.fill_between(tvec, q5_smooth, q95_smooth, facecolor='r',
                     alpha=0.05, linewidth = 0.0)
    plt.legend(loc='best')
    plt.show(False)

    if STORE_FIG_DIR is not None:
        data2save.update({'fig:smoothing-filtering_marginals_timesteps':fig3})
        store_figure(
            fig3, fig_folder+ '/' + title + '-' + \
            'smoothing-filtering-marginals-timesteps',
            fig_formats)

#--4--###### Plot: filtering marginals hyper-parameters #############
if DO_HYPER_FILT and nhyper>0:
    hyper_samp_filt = data['hyper_samp_filt']
    print("Plotting hyper-parameters filtering marginals \n")
    if target_density.is_mu_hyper:
        #Plot filtering marginals for mu
        index_mu = target_density.index_mu
        mu_samp_filt = hyper_samp_filt[:,index_mu,:]
        meanMu_filt = np.mean(mu_samp_filt, axis=0)
        q5mu_filt = np.percentile(mu_samp_filt, 5, axis=0)
        q25mu_filt = np.percentile(mu_samp_filt, 25, axis=0)
        q40mu_filt = np.percentile(mu_samp_filt, 40, axis=0)
        q60mu_filt = np.percentile(mu_samp_filt, 60, axis=0)
        q75mu_filt = np.percentile(mu_samp_filt, 75, axis=0)
        q95mu_filt = np.percentile(mu_samp_filt, 95, axis=0)

        fig4 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{\mu|Y_{0:t}}$",
                                  title='Filtering marginals of $\mu$')
        ax4 = fig4.gca()
        #ax4.set_aspect('equal')
        #ax.set_ylim(-3,3)
        ax4.set_xlim(0,tvec[-1])
        #If target_density.Xt is not None:  ######### FIX THIS LINE ###########
        #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
        #ax.plot(target_density.tobs, target_density.dataObs, 'or')
        ax4.plot(tvec, meanMu_filt, '-k')
        ax4.fill_between(tvec, q40mu_filt, q60mu_filt, facecolor='grey',
                         alpha=0.35, linewidth = 0.0)
        ax4.fill_between(tvec, q25mu_filt, q75mu_filt, facecolor='grey',
                         alpha=0.25, linewidth = 0.0)
        ax4.fill_between(tvec, q5mu_filt, q95mu_filt, facecolor='grey',
                         alpha=0.15, linewidth = 0.0)
        #plt.legend(loc='best')
        plt.show(False)

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:filtering_marginals_mu':fig4})
            store_figure(
                fig4, fig_folder+ '/' + title + '-' + \
                'filtering-marginals-timesteps_mu',
                fig_formats)

    if target_density.is_sigma_hyper:
        #Plot filtering marginals for sigma
        index_sigma = target_density.index_sigma
        sigma_samp_filt = target_density.sigma.evaluate(
            hyper_samp_filt[:,index_sigma,:] )
        meanSigma_filt = np.mean(sigma_samp_filt, axis=0)
        q5sigma_filt = np.percentile(sigma_samp_filt, 5, axis=0)
        q25sigma_filt = np.percentile(sigma_samp_filt, 25, axis=0)
        q40sigma_filt = np.percentile(sigma_samp_filt, 40, axis=0)
        q60sigma_filt = np.percentile(sigma_samp_filt, 60, axis=0)
        q75sigma_filt = np.percentile(sigma_samp_filt, 75, axis=0)
        q95sigma_filt = np.percentile(sigma_samp_filt, 95, axis=0)

        fig5 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{\sigma|Y_{0:t}}$",
                                  title='Filtering marginals of $\sigma$')
        ax5 = fig5.gca()
        #ax5.set_aspect('equal')
        #ax.set_ylim(-3,3)
        ax5.set_xlim(0,tvec[-1])
        #If target_density.Xt is not None:  ######### FIX THIS LINE ###########
        #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
        #ax.plot(target_density.tobs, target_density.dataObs, 'or')
        ax5.plot(tvec, meanSigma_filt, '-k')
        ax5.fill_between(tvec, q40sigma_filt, q60sigma_filt, facecolor='grey',
                         alpha=0.35, linewidth = 0.0)
        ax5.fill_between(tvec, q25sigma_filt, q75sigma_filt, facecolor='grey',
                         alpha=0.25, linewidth = 0.0)
        ax5.fill_between(tvec, q5sigma_filt, q95sigma_filt, facecolor='grey',
                         alpha=0.15, linewidth = 0.0)
        #plt.legend(loc='best')
        plt.show(False)

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:filtering_marginals_sigma':fig5})
            store_figure(
                fig5, fig_folder+ '/' + title + '-' + \
                'filtering-marginals-timesteps_sigma',
                fig_formats)

    if target_density.is_phi_hyper:
        #Plot filtering marginals for sigma
        index_phi = target_density.index_phi
        phi_samp_filt = target_density.phi.evaluate( hyper_samp_filt[:,index_phi,:] )
        meanPhi_filt = np.mean(phi_samp_filt, axis=0)
        q5phi_filt = np.percentile(phi_samp_filt, 5, axis=0)
        q25phi_filt = np.percentile(phi_samp_filt, 25, axis=0)
        q40phi_filt = np.percentile(phi_samp_filt, 40, axis=0)
        q60phi_filt = np.percentile(phi_samp_filt, 60, axis=0)
        q75phi_filt = np.percentile(phi_samp_filt, 75, axis=0)
        q95phi_filt = np.percentile(phi_samp_filt, 95, axis=0)

        fig6 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{\phi|Y_{0:t}}$",
                                  title='Filtering marginals of $\phi$')
        ax6 = fig6.gca()
        #ax6.set_aspect('equal')
        #ax.set_ylim(-3,3)
        ax6.set_xlim(0,tvec[-1])
        #If target_density.Xt is not None:  ######### FIX THIS LINE ###########
        #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
        #ax.plot(target_density.tobs, target_density.dataObs, 'or')
        ax6.plot(tvec, meanPhi_filt, '-k')
        ax6.fill_between(tvec, q40phi_filt, q60phi_filt, facecolor='grey',
                         alpha=0.35, linewidth = 0.0)
        ax6.fill_between(tvec, q25phi_filt, q75phi_filt, facecolor='grey',
                         alpha=0.25, linewidth = 0.0)
        ax6.fill_between(tvec, q5phi_filt, q95phi_filt, facecolor='grey',
                         alpha=0.15, linewidth = 0.0)
        #plt.legend(loc='best')
        plt.show(False)

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:filtering_marginals_phi':fig6})
            store_figure(
                fig6, fig_folder+ '/' + title + '-' + \
                'filtering-marginals-timesteps_phi',
                fig_formats)


#--4--###### Plot: smoothing/posterior marginals hyper-parameters #############
if DO_HYPER_SMOOTH and nhyper>0:
    tar_samp_smooth = data['tar_samp_smooth'].copy()
    print("Plotting hyper-parameters smoothing marginals \n")
    if target_density.is_mu_hyper:
        index_mu = target_density.index_mu
        mu_samp_smooth = tar_samp_smooth[:,index_mu]
        mu_min = np.min(mu_samp_smooth)
        mu_min = mu_min - .1*np.abs(mu_min)
        mu_max = np.max(mu_samp_smooth)
        mu_max = mu_max + .1*np.abs(mu_max)
        mu_kde = scistat.gaussian_kde(mu_samp_smooth)
        mu_xx = np.linspace(mu_min, mu_max, 10000)
        pdf_mu_xx = mu_kde(mu_xx)
        fig7 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\mu|Y_{0:N}}$",
                                  title='Posterior/smoothing marginals of $\mu$')
        ax7 = fig7.gca()
        ax7.set_xlim(mu_min,mu_max)
        ax7.plot(mu_xx, pdf_mu_xx, '-k',linewidth=1.0)
        plt.show(False)

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:smoothing_marginals_mu':fig7})
            store_figure(
                fig7, fig_folder+ '/' + title + '-' + \
                'smoothing-marginals-mu',
                fig_formats) 

    if target_density.is_sigma_hyper:
        index_sigma = target_density.index_sigma
        Xsigma_samp_smooth =  tar_samp_smooth[:,index_sigma]
        # Do the kde on the reference variable and then push forward through F_sigma.
        Xsigma_kde = scistat.gaussian_kde(Xsigma_samp_smooth) 
        Xsigma_min = np.min(Xsigma_samp_smooth)
        Xsigma_min = Xsigma_min - .1*np.abs(Xsigma_min)
        Xsigma_max = np.max(Xsigma_samp_smooth)
        Xsigma_max = Xsigma_max + .1*np.abs(Xsigma_max)
        Xsigma_xx = np.linspace(Xsigma_min, Xsigma_max, 10000)
        FXsigma_xx = target_density.sigma.evaluate( Xsigma_xx )
        grad_FXsigma_xx = target_density.sigma.grad_x( Xsigma_xx )
        pdf_sigma_FXsigma = Xsigma_kde(Xsigma_xx)/grad_FXsigma_xx
        fig8 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\sigma|Y_{0:N}}$",
                             title='Posterior/smoothing marginals of $\sigma$')
        ax8 = fig8.gca()
        sigma_max = np.max(FXsigma_xx)
        ax8.set_xlim(0.,sigma_max)
        ax8.plot(FXsigma_xx, pdf_sigma_FXsigma, '-k',linewidth=1.0)
        plt.show(False)

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:smoothing_marginals_sigma':fig8})
            store_figure(
                fig8, fig_folder+ '/' + title + '-' + \
                'smoothing-marginals-sigma',
                fig_formats) 

    if target_density.is_phi_hyper:
        index_phi = target_density.index_phi
        Xphi_samp_smooth =  tar_samp_smooth[:,index_phi]
        Xphi_kde = scistat.gaussian_kde(Xphi_samp_smooth) #Do the kde on the reference variable and then push forward through F_phi.
        Xphi_min = np.min(Xphi_samp_smooth)
        Xphi_min = Xphi_min - .1*np.abs(Xphi_min)
        Xphi_max = np.max(Xphi_samp_smooth)
        Xphi_max = Xphi_max + .1*np.abs(Xphi_max)
        Xphi_xx = np.linspace(Xphi_min, Xphi_max, 10000)
        FXphi_xx = target_density.phi.evaluate( Xphi_xx )
        grad_FXphi_xx = target_density.phi.grad_x( Xphi_xx )
        pdf_phi_FXphi = Xphi_kde(Xphi_xx)/grad_FXphi_xx
        fig9 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\phi|Y_{0:N}}$",
                                  title='Posterior/smoothing marginals of $\phi$')
        ax9 = fig9.gca()
        phi_min = np.min(FXphi_xx)
        ax9.set_xlim(phi_min,1.)
        ax9.plot(FXphi_xx, pdf_phi_FXphi, '-k',linewidth=1.0)
        plt.show(False)
        #phi_samp_smooth = target_density.phi.evaluate( Xphi_samp_smooth )

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:smoothing_marginals_phi':fig9})
            store_figure(
                fig9, fig_folder+ '/' + title + '-' + \
                'smoothing-marginals-phi',
                fig_formats)

    print("Plotting posterior/smoothing marginals \n")
    hyper_samp_smooth = tar_samp_smooth[:,:nhyper].copy()
    if target_density.is_sigma_hyper:
        index_sigma = target_density.index_sigma
        hyper_samp_smooth[:,index_sigma] = target_density.sigma.evaluate(
            hyper_samp_smooth[:,index_sigma] )
    if target_density.is_phi_hyper:
        index_phi = target_density.index_phi
        hyper_samp_smooth[:,index_phi] = target_density.phi.evaluate(
            hyper_samp_smooth[:,index_phi] )
    fig = DIAG.plotAlignedMarginals(
        hyper_samp_smooth, range_vec=[(-2.,1.), (0.2,1.05)],
        show_axis=True )
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:smoothing_marginals_hyper':fig})
        store_figure(
            fig, fig_folder+ '/' + title + '-' + \
            'smoothing-marginals-hyper',
            fig_formats)

    print("Plotting posterior/smoothing 2d marginals \n")
    hyper_samp_smooth = tar_samp_smooth[:,:nhyper].copy()
    if target_density.is_sigma_hyper:
        index_sigma = target_density.index_sigma
        hyper_samp_smooth[:,index_sigma] = target_density.sigma.evaluate(
            hyper_samp_smooth[:,index_sigma] )
    if target_density.is_phi_hyper:
        index_phi = target_density.index_phi
        hyper_samp_smooth[:,index_phi] = target_density.phi.evaluate(
            hyper_samp_smooth[:,index_phi] )
    fig = DIAG.plotAlignedMarginals(
        hyper_samp_smooth, range_vec=[(-2.,1.), (0.2,1.)],
        white_background=False, do_diag=False,
        show_axis=True )
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:smoothing_marginals_hyper_2d':fig})
        store_figure(
            fig, fig_folder+ '/' + title + '-' + \
            'smoothing-marginals-hyper-2d',
            fig_formats)

if DO_HYPER_SMOOTH and DO_UNBIASED_SMOOTH and nhyper > 0:
    print("Plotting hyper-parameters unbiased smoothing marginals \n")
    tar_unb_samp_smooth = data['unb_tar_samp_smooth'].copy()
    hyper_unb_samp_smooth = tar_unb_samp_smooth[:,:nhyper]
    if target_density.is_sigma_hyper:
        index_sigma = target_density.index_sigma
        hyper_unb_samp_smooth[:,index_sigma] = target_density.sigma.evaluate(
            hyper_unb_samp_smooth[:,index_sigma] )
    if target_density.is_phi_hyper:
        index_phi = target_density.index_phi
        hyper_unb_samp_smooth[:,index_phi] = target_density.phi.evaluate(
            hyper_unb_samp_smooth[:,index_phi] )
    fig = DIAG.plotAlignedMarginals(
        hyper_unb_samp_smooth, range_vec=[(-2.,1.), (0.2,1.05)],
        show_axis=True )
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:unbiased_smoothing_marginals_hyper':fig})
        store_figure(
            fig, fig_folder+ '/' + title + '-' + \
            'unbiased-smoothing-marginals-hyper',
            fig_formats)

    print("Plotting hyper-parameters unbiased smoothing 2d marginals \n")
    tar_unb_samp_smooth = data['unb_tar_samp_smooth'].copy()
    hyper_unb_samp_smooth = tar_unb_samp_smooth[:,:nhyper]
    if target_density.is_sigma_hyper:
        index_sigma = target_density.index_sigma
        hyper_unb_samp_smooth[:,index_sigma] = target_density.sigma.evaluate(
            hyper_unb_samp_smooth[:,index_sigma] )
    if target_density.is_phi_hyper:
        index_phi = target_density.index_phi
        hyper_unb_samp_smooth[:,index_phi] = target_density.phi.evaluate(
            hyper_unb_samp_smooth[:,index_phi] )
    fig = DIAG.plotAlignedMarginals(
        hyper_unb_samp_smooth, range_vec=[(-2.,1.), (0.2,1.)],
        white_background=False, do_diag=False,
        show_axis=True )
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:unbiased_smoothing_marginals_hyper_2d':fig})
        store_figure(
            fig, fig_folder+ '/' + title + '-' + \
            'unbiased-smoothing-marginals-hyper-2d',
            fig_formats)

    tar_samp_smooth = data['tar_samp_smooth'].copy()
    tar_unb_samp_smooth = data['unb_tar_samp_smooth'].copy()
    print("Plotting hyper-parameters smoothing marginals vs. unbiased \n")
    if target_density.is_mu_hyper:
        index_mu = target_density.index_mu
        mu_samp_smooth = tar_samp_smooth[:,index_mu]
        mu_unb_samp_smooth = tar_unb_samp_smooth[:,index_mu]
        mu_min = min(np.min(mu_samp_smooth), np.min(mu_unb_samp_smooth))
        mu_min = mu_min - .1*np.abs(mu_min)
        mu_max = max(np.max(mu_samp_smooth), np.max(mu_unb_samp_smooth))
        mu_max = mu_max + .1*np.abs(mu_max)
        mu_kde = scistat.gaussian_kde(mu_samp_smooth)
        mu_unb_kde = scistat.gaussian_kde(mu_unb_samp_smooth)
        mu_xx = np.linspace(mu_min, mu_max, 10000)
        pdf_mu_xx = mu_kde(mu_xx)
        pdf_mu_unb_xx = mu_unb_kde(mu_xx)
        fig7 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\mu|Y_{0:N}}$",
                             title='Posterior/smoothing marginals of $\mu$')
        ax7 = fig7.gca()
        ax7.set_xlim(mu_min,mu_max)
        ax7.plot(mu_xx, pdf_mu_xx, '-k',linewidth=1.0, label='Transport map')
        ax7.plot(mu_xx, pdf_mu_unb_xx, '--k',linewidth=1.0, label='MCMC')
        ax7.legend(loc='best')
        plt.show(False)

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:smoothing_marginals_mu_vs_unb':fig7})
            store_figure(
                fig7, fig_folder+ '/' + title + '-' + \
                'smoothing-marginals-mu-vs-unb',
                fig_formats) 

    if target_density.is_sigma_hyper:
        index_sigma = target_density.index_sigma
        Xsigma_samp_smooth =  tar_samp_smooth[:,index_sigma]
        Xsigma_unb_samp_smooth = tar_unb_samp_smooth[:,index_sigma]
        # Do the kde on the reference variable and then push forward through F_sigma.
        Xsigma_kde = scistat.gaussian_kde(Xsigma_samp_smooth)
        Xsigma_unb_kde = scistat.gaussian_kde(Xsigma_unb_samp_smooth) 
        Xsigma_min = min(np.min(Xsigma_samp_smooth), np.min(Xsigma_unb_samp_smooth))
        Xsigma_min = Xsigma_min - .1*np.abs(Xsigma_min)
        Xsigma_max = max(np.max(Xsigma_samp_smooth), np.max(Xsigma_unb_samp_smooth))
        Xsigma_max = Xsigma_max + .1*np.abs(Xsigma_max)
        Xsigma_xx = np.linspace(Xsigma_min, Xsigma_max, 10000)
        FXsigma_xx = target_density.sigma.evaluate( Xsigma_xx )
        grad_FXsigma_xx = target_density.sigma.grad_x( Xsigma_xx )
        pdf_sigma_FXsigma = Xsigma_kde(Xsigma_xx)/grad_FXsigma_xx
        pdf_unb_sigma_FXsigma = Xsigma_unb_kde(Xsigma_xx)/grad_FXsigma_xx
        fig8 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\sigma|Y_{0:N}}$",
                             title='Posterior/smoothing marginals of $\sigma$')
        ax8 = fig8.gca()
        sigma_max = np.max(FXsigma_xx)
        ax8.set_xlim(0.,sigma_max)
        ax8.plot(FXsigma_xx, pdf_sigma_FXsigma, '-k',linewidth=1.0, label='Transport Map')
        ax8.plot(FXsigma_xx, pdf_unb_sigma_FXsigma, '--k',
                 linewidth=1.0, label='MCMC')
        ax8.legend(loc='best')
        plt.show(False)

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:smoothing_marginals_sigma_vs_unb':fig8})
            store_figure(
                fig8, fig_folder+ '/' + title + '-' + \
                'smoothing-marginals-sigma-vs-unb',
                fig_formats) 

    if target_density.is_phi_hyper:
        index_phi = target_density.index_phi
        Xphi_samp_smooth =  tar_samp_smooth[:,index_phi]
        Xphi_unb_samp_smooth =  tar_unb_samp_smooth[:,index_phi]
        #Do the kde on the reference variable and then push forward through F_phi.
        Xphi_kde = scistat.gaussian_kde(Xphi_samp_smooth)
        Xphi_unb_kde = scistat.gaussian_kde(Xphi_unb_samp_smooth) 
        Xphi_min = min(np.min(Xphi_samp_smooth), np.min(Xphi_unb_samp_smooth))
        Xphi_min = Xphi_min - .1*np.abs(Xphi_min)
        Xphi_max = max(np.max(Xphi_samp_smooth), np.max(Xphi_unb_samp_smooth))
        Xphi_max = Xphi_max + .1*np.abs(Xphi_max)
        Xphi_xx = np.linspace(Xphi_min, Xphi_max, 10000)
        FXphi_xx = target_density.phi.evaluate( Xphi_xx )
        grad_FXphi_xx = target_density.phi.grad_x( Xphi_xx )
        pdf_phi_FXphi = Xphi_kde(Xphi_xx)/grad_FXphi_xx
        pdf_unb_phi_FXphi = Xphi_unb_kde(Xphi_xx)/grad_FXphi_xx
        fig9 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\phi|Y_{0:N}}$",
                                  title='Posterior/smoothing marginals of $\phi$')
        ax9 = fig9.gca()
        phi_min = np.min(FXphi_xx)
        ax9.set_xlim(phi_min,1.)
        ax9.plot(FXphi_xx, pdf_phi_FXphi, '-k',linewidth=1.0, label='Transport Map')
        ax9.plot(FXphi_xx, pdf_unb_phi_FXphi, '--k',linewidth=1.0, label='MCMC')
        ax9.legend(loc='best')
        plt.show(False)
        #phi_samp_smooth = target_density.phi.evaluate( Xphi_samp_smooth )

        if STORE_FIG_DIR is not None:
            data2save.update({'fig:smoothing_marginals_phi_vs_unb':fig9})
            store_figure(
                fig9, fig_folder+ '/' + title + '-' + \
                'smoothing-marginals-phi-vs-unb',
                fig_formats)

#--5--###### Plot: order maps in the composition #############
if DO_ORDERS:
    fig10 = DIAG.nicePlot([],[], xlabel="iteration", ylabel="order map",
                          title='Order maps in the composition')
    ax10 = fig10.gca()
    ax10.set_xlim(0,len(map_order_list))
    ax10.set_ylim(0,1+np.max(map_order_list))
    ax10.plot(np.arange(len(map_order_list)), map_order_list, '--ko',linewidth=1.0)
    plt.show(False)

    if STORE_FIG_DIR is not None:
        data2save.update({'fig:order_maps_composition':fig10})
        store_figure(
            fig10, fig_folder+ '/' + title + '-' + \
            'order-maps-composition',
            fig_formats) 

#--6--###### Plot: regression order hyper-maps #############
if DO_REG_ORDERS:
    fig11 = DIAG.nicePlot([],[], xlabel="iteration", ylabel="order map",
                          title='Order regression hyper-maps')
    ax11 = fig11.gca()
    ax11.set_xlim(0,len(regression_order_list))
    ax11.set_ylim(0,1+np.max(regression_order_list))
    ax11.plot(np.arange(len(regression_order_list)), regression_order_list, '--ko',linewidth=1.0)
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:order_regression_hypermaps':fig11})
        store_figure(
            fig11, fig_folder+ '/' + title + '-' + \
            'order-regression-hypermaps',
            fig_formats) 

#--7--###### Plot: posterior data predictive #############
if DO_POST_PRED:
    Ysmooth_samples = data['Ysmooth_samples']
    print("Plotting posterior predictive \n")
    q5y_smooth = np.percentile(Ysmooth_samples, 5, axis=0)
    q25y_smooth = np.percentile(Ysmooth_samples, 25, axis=0)
    q40y_smooth = np.percentile(Ysmooth_samples, 40, axis=0)
    q60y_smooth = np.percentile(Ysmooth_samples, 60, axis=0)
    q75y_smooth = np.percentile(Ysmooth_samples, 75, axis=0)
    q95y_smooth = np.percentile(Ysmooth_samples, 95, axis=0)
    fig12 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{Y_y|Y_{0:N}}$",
                          title="Data Posterior/Smoothing marginals (posterior predictive)")
    ax12 = fig12.gca()
    #ax12.set_aspect('equal')
    #ax.set_ylim(-3,3)
    ax12.set_xlim(0,tvec[-1])
    ax12.plot(target_density.tobs,target_density.dataObs,'.k', label="data")
    ax12.fill_between(tvec, q40y_smooth, q60y_smooth, facecolor='r',
                      alpha=0.35, linewidth = 0.0)
    ax12.fill_between(tvec, q25y_smooth, q75y_smooth, facecolor='r',
                      alpha=0.25, linewidth = 0.0)
    ax12.fill_between(tvec, q5y_smooth, q95y_smooth, facecolor='r',
                      alpha=0.15, linewidth = 0.0)
    plt.legend(loc='best')
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:posterior_data_predictive':fig12})
        store_figure(
            fig12, fig_folder+ '/' + title + '-' + \
            'posterior-data-predictive',
            fig_formats) 

#--8--###### Plot: marginals posterior/smoothing distribution #############
if DO_MARGINALS:
    tar_samp_smooth = data['tar_samp_smooth']
    print("Plotting posterior/smoothing marginals \n")
    fig13 = DIAG.plotAlignedMarginals(
        tar_samp_smooth, dimensions_vec = list(range( min(dim,15) )),
        mpi_pool=mpi_pool)
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:smoothing_marginals_triangularPlot':fig13})
        store_figure(
            fig13, fig_folder+ '/' + title + '-' + \
            'smoothing-marginals-triangular',
            fig_formats) 

if DO_RANDOM:
    data_random_conditionals = data['data_random_conditionals']
    print("Plotting random conditionals")
    fig14 = DIAG.plotRandomConditionals(data=data_random_conditionals)
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:random_conditionals':fig14})
        store_figure(
            fig14, fig_folder+ '/' + title + '-' + \
            'random-conditionals',
            fig_formats) 

if DO_ALIGNED:
    data_aligned_conditionals = data['data_aligned_conditionals']
    print("Plotting aligned conditionals: %s" % dvec)
    fig15 = DIAG.plotAlignedConditionals(data=data_aligned_conditionals)
    plt.show(False)
    if STORE_FIG_DIR is not None:
        data2save.update({'fig:aligned_conditionals':fig15})
        store_figure(
            fig15, fig_folder+ '/' + title + '-' + \
            'aligned-conditionals',
            fig_formats) 

#Save data to file!
if STORE_FIG_DIR is not None:
    with open(fig_folder + '/' + title + '-plots.dill', 'wb') as out_stream:
        dill.dump(data2save, out_stream)

