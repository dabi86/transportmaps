#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import sys, getopt
import os.path
import dill
import numpy as np
import scipy.stats as stats

import TransportMaps as TM
import TransportMaps.Samplers as SAMP
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS
import src.TransportMaps.Maps.Decomposable as DECMAPS

import StocVolHyperDensitiesDurbin as SVHDENS

def usage():
    print("Postprocess-analysis.py --input=<file_name> " + \
          "[--trim=<trim> " + \
          "--output=<file_name> --nmc=<nmc> " + \
          "--do-random --do-aligned --do-var-diag " + \
          "--do-filt --do-smooth --do-post-pred " + \
          "--do-unbiased-smooth " + \
          "--nprocs=<num_processors> --mpi-lag=<lag>]")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
NMC = 5000
TRIM = None
OUT_FNAME = None
NPROCS = 1
DO_RANDOM = False
DO_ALIGNED = False
DO_VAR_DIAG = False
DO_FILT = False
DO_SMOOTH = False
DO_POST_PRED = False
DO_UNBIASED_SMOOTH = False
BURNIN = 1000
LAGS = 50
try:
    opts, args = getopt.getopt(argv,"h",["input=", "nmc=", "trim=",
                                         "nprocs=", "output=",
                                         "do-aligned", "do-random", "do-var-diag",
                                         "do-smooth", "do-filt",
                                         "do-post-pred",
                                         "do-unbiased-smooth", "burnin=", "lags="])

except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--input"):
        IN_FNAME = arg
    elif opt in "--nmc":
        NMC = int(arg)
    elif opt in "--trim":
        TRIM = int(arg)
    elif opt in "--do-random":
        DO_RANDOM = True
    elif opt in '--do-aligned':
        DO_ALIGNED = True
    elif opt in "--do-var-diag":
        DO_VAR_DIAG = True
    elif opt in "--do-smooth":
        DO_SMOOTH = True
    elif opt in "--do-filt":
        DO_FILT = True
    elif opt in "--do-postpred":
        DO_POST_PRED = True
    elif opt in "--do-unbiased-smooth":
        DO_UNBIASED_SMOOTH = True
    elif opt in "--burnin":
        BURNIN = int(arg)
    elif opt in "--lags":
        LAGS = int(arg)
    elif opt in ("--output"):
        OUT_FNAME = arg
    elif opt in ("--nprocs="):
        NPROCS = int(arg)

if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME, 'rb') as in_stream:
    data = dill.load(in_stream)

# Restore data
tm_approx_comp_list = data['tm_approx_comp_list']
target_density = data['target_density']
tm_approx_hyper_list = data['tm_approx_hyper_list']
regression_order_list = data['regression_order_list']
map_order_list = data['map_order_list']

#Trim == effective number of timesteps considered
if  len(tm_approx_comp_list) == 0:
    raise ValueError("No maps have been computed")
nsteps_computed = len(tm_approx_comp_list)+1
if TRIM is None: #Check that the desired maps have been computed
    if target_density.nsteps > nsteps_computed:
        #In this case, reduce the target density and compute what is possible
        TRIM = nsteps_computed
        warnings.warn("The composition of maps is NOT complete. " + \
                      "We will trim the target density accordingly", RuntimeWarning)


if TRIM is not None:
    if TRIM > nsteps_computed:
        warnings.warn("You are asking for more steps that you have actually computed. " + \
                      "We will trim the target density accordingly", RuntimeWarning)
        TRIM = nsteps_computed
        #raise ValueError("You only computed %d time steps" %(nsteps_computed))
    #Number of maps to consider
    n_maps2consider = TRIM  - 1
    #Trim maps
    tm_approx_comp_list = tm_approx_comp_list[:n_maps2consider]
    tm_approx_hyper_list = tm_approx_hyper_list[:n_maps2consider+1]
    regression_order_list = regression_order_list[:n_maps2consider]
    map_order_list = map_order_list[:n_maps2consider]
    #Trim densities
    target_density = SVHDENS.trim_density(target_density, TRIM)

#Retrieve remaining variables
dim = target_density.dim
nsteps = target_density.nsteps
nhyper = sum(target_density.which_hyper)
index_X0 = target_density.index_X0
base_density = DENS.StandardNormalDensity(dim)

#Store data if needed
RELOAD = False
if OUT_FNAME is not None:
    if os.path.exists(OUT_FNAME):
        sel = '-'
        while sel not in ['y', 'Y', 'n', 'N']:
            sel = raw_input("The output file already exists. Do you want to load it? [y/N] ")
            if sel == 'y' or sel == 'Y':
                RELOAD = True
if RELOAD:
    with open(OUT_FNAME, 'rb') as istr:
        data2save = dill.load(istr)
else:
    data2save = {'tm_approx_comp_list': tm_approx_comp_list,
                 'target_density': target_density,
                 'tm_approx_hyper_list': tm_approx_hyper_list,
                 'regression_order_list': regression_order_list,
                 'map_order_list': map_order_list,
                 'base_density': base_density}

# Construct T
tm_comp = DECMAPS.SequentialMarkovianTransportMap(tm_approx_comp_list, nhyper )

# Construct T^\sharp \pi_{tar}
pull_tar = DENS.PullBackTransportMapDensity(tm_comp, target_density)

# Construct T_\sharp \rho
push_base = DENS.PushForwardTransportMapDensity(tm_comp, base_density)

#--0--###### Start pool of mpi processes
mpi_pool = None
if NPROCS > 1:
    import_set = set()
    import_set.add( ('numpy', 'np') )
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS, import_set)
try:
    #--1--###### Computing: smoothing/posterior marginals timesteps #############
    if DO_SMOOTH:
        print("\nComputing smoothing/posterior marginals \n")
        tar_samp_smooth = push_base.rvs(NMC, mpi_pool=mpi_pool)
        if OUT_FNAME is not None:
            data2save.update({'push_base':push_base,
                              'tar_samp_smooth':tar_samp_smooth})

    if DO_UNBIASED_SMOOTH:
        print("Computing unbiased smoothing/posterior marginals \n")
        def compute_ess(samps, quantile=0.95):
            nsamps = samps.shape[0]
            if nsamps <= 1:
                return nsamps
            mean = np.mean(samps, axis=0)
            std = np.std(samps, axis=0)
            cent_samps = samps - mean
            corr = np.zeros((LAGS+1, samps.shape[1]))
            for lag in range(0,LAGS+1):
                corr[lag,:] = np.sum(cent_samps[:nsamps-lag,:] * \
                                     cent_samps[lag:,:], axis=0) / \
                              float(nsamps-lag) / std**2.
            # Confidence interval
            var = 1. / np.arange(nsamps, nsamps-LAGS-1, -1)
            alpha = 1 - (1 - quantile)/2.
            confint = stats.norm.ppf(alpha) * np.sqrt(var)
            # ESS
            kappa = np.zeros(dim)
            abs_corr = np.abs(corr[1:,:])
            for d in range(dim):
                sig_corr = abs_corr[abs_corr[:,d] >= confint[1:],d] # 95% significant factors
                kappa[d] = 1 + 2 * np.sum(sig_corr, axis=0)
            ess = float(nsamps) / kappa
            arg_min_ess = np.argmin(ess)
            min_ess = int(ess[arg_min_ess])
            return min_ess
        sampler = SAMP.MetropolisHastingsIndependentProposalsSampler(
            pull_tar, base_density)
        ess = 0
        it_ess = -1
        unb_pull_tar_samp_smooth = np.zeros((0,pull_tar.dim))
        MIN_NBATCH = 100
        while ess < NMC:
            it_ess += 1
            nsamps_old = unb_pull_tar_samp_smooth.shape[0]
            if it_ess != 0:
                nbatch = max((NMC - ess) * 2, MIN_NBATCH)
                (new_samps, weights) = sampler.rvs(
                    nbatch, x0=unb_pull_tar_samp_smooth[-1,:], mpi_pool_tuple=(mpi_pool,None))
            else:
                (new_samps, weights) = sampler.rvs(
                    NMC, BURNIN, mpi_pool_tuple=(mpi_pool,None))
            unb_pull_tar_samp_smooth = np.vstack( (unb_pull_tar_samp_smooth, new_samps) )
            # Compute effective sample size
            nsamps = unb_pull_tar_samp_smooth.shape[0]
            ess = compute_ess(unb_pull_tar_samp_smooth)
            print("N samples %d - ESS %d" % (nsamps, ess))
        unb_tar_samp_smooth = pull_tar.map_samples_target_to_base(
            unb_pull_tar_samp_smooth, mpi_pool=mpi_pool)
        if OUT_FNAME is not None:
            data2save.update({'unb_tar_samp_smooth': unb_tar_samp_smooth})

    #--2--###### Computing: filtering marginals timesteps #############
    if DO_FILT:
        print("Computing: filtering marginals \n")
        Xt_samp_filt = np.zeros((NMC,nsteps))
        dim_Markov = 2+nhyper
        nXd = DENS.StandardNormalDensity(dim_Markov)
        nXm1d = DENS.StandardNormalDensity(dim_Markov-1)
        nXd_samp = nXd.rvs(NMC)
        scatter_tuple = (['x'], [nXd_samp])
        Xt_samp_filt[:,0] = TM.mpi_eval("evaluate", scatter_tuple=scatter_tuple,
                                        obj=tm_approx_comp_list[0].approx_list[-1],
                                        mpi_pool=mpi_pool).flatten()
        for ii,tm in enumerate(tm_approx_comp_list):
            nXm1d_samp = nXm1d.rvs(NMC)
            scatter_tuple = (['x'], [nXm1d_samp])
            Xt_samp_filt[:,ii+1] = TM.mpi_eval("evaluate", scatter_tuple=scatter_tuple,
                                               obj=tm.approx_list[-2],
                                               mpi_pool=mpi_pool).flatten()
        if OUT_FNAME is not None:
            data2save.update({'Xt_samp_filt':Xt_samp_filt})

    #--4--###### Computing: filtering marginals hyper-parameters #############
    if nhyper>0 and DO_FILT:
        print("Computing hyper-parameters filtering marginals \n")
        hyper_samp_filt = np.zeros((NMC,nhyper,nsteps))
        nHd = DENS.StandardNormalDensity(nhyper)
        for ii,tm in enumerate(tm_approx_hyper_list[1:]):
            nHd_samp = nHd.rvs(NMC)
            hyper_samp_filt[:,:,ii+1] = tm.evaluate(nHd_samp)
        # We assimilate the two steps at the same time
        hyper_samp_filt[:,:,0] = hyper_samp_filt[:,:,1].copy() 
        if OUT_FNAME is not None:
            data2save.update({'hyper_samp_filt':hyper_samp_filt})

    #--7--###### Computing: posterior data predictive #############
    #Xt_samp_smooth
    if DO_POST_PRED and DO_SMOOTH:
        print("Computing posterior predictive \n")
        Xt_samp_smooth = tar_samp_smooth[:,index_X0:]
        noise_samples = np.random.randn(Xt_samp_smooth.shape[0],Xt_samp_smooth.shape[1])
        Ysmooth_samples = noise_samples*np.exp(.5*Xt_samp_smooth)
        if OUT_FNAME is not None:
            data2save.update({'Ysmooth_samples':Ysmooth_samples})

    # #--9--###### Plot: intensity coefficients map #############
    # print("Plotting intensity coefficients map \n")
    # fig14 = DIAG.plotCoefficientsMap(tm_comp)
    # if OUT_FNAME is not None:
    #     data2save.update({'fig:intensity_coefficients_map':fig14})

    if DO_RANDOM:
        print("Computing random conditionals")
        data = DIAG.computeRandomConditionals(pull_tar, num_conditionalsXax=6,
                                               numPointsXax = 15,
                                               mpi_pool=mpi_pool)
        if OUT_FNAME is not None:
            data2save.update({'data_random_conditionals':data})

    if DO_ALIGNED:
        # dvec = list(npr.choice(np.arange(target_density.dim), min(target_density.dim,8),
        #                        replace=False))
        dvec = list(range( min(target_density.dim,10) ))
        print("Computing aligned conditionals: %s" % dvec)
        data = DIAG.computerAlignedConditionals(pull_tar, numPointsXax = 15,
                                             range_vec=[-3,3],
                                             dimensions_vec=dvec, mpi_pool=mpi_pool)
        if OUT_FNAME is not None:
            data2save.update({'data_aligned_conditionals':data})

    if DO_VAR_DIAG:
        print("Calculating variance diagnostic")
        var_diag = DIAG.variance_approx_kl(base_density, pull_tar, qtype=0, qparams=NMC,
                                           mpi_pool_tuple=(None, mpi_pool))
        if OUT_FNAME is not None:
            data2save.update({'variance_diagnostic':var_diag})
finally:
    if mpi_pool is not None:
        mpi_pool.stop()
        mpi_pool = None

#Save data to file!
if OUT_FNAME is not None:
    with open(OUT_FNAME, 'wb') as out_stream:
        dill.dump(data2save, out_stream)
