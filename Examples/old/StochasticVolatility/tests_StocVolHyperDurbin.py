######### STOCHASTIC VOLATILITY MODEL FROM DURBIN(1998) AND RUE(2009) #############
from __future__ import division
import numpy as np
from scipy import linalg
import scipy.stats as stats
import math
import os.path
import dill
import TransportMaps.Densities as DENS
import TransportMaps as TM
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.FiniteDifference as FD
import copy as cp

import StocVolHyperDensitiesDurbin as SVHD
import TransportMaps as TM
import TransportMaps.Diagnostics as DIAG
import matplotlib.pyplot as plt

which_hyper = (True, True, True)
#which_hyper = (False, True, False)
#which_hyper = (True, False, True)
#which_hyper = (True, True, True)


load_data = "NO"

######## TEST JOINT DENSITY ##################

if load_data == "YES":
  #Load data
  trim = None
  dataObs = np.loadtxt( "sv.dat", skiprows = 1 )
  if trim is not None:
    dataObs=dataObs[:trim]
  nsteps = len(dataObs)
  indexObs = np.arange(nsteps)
  stocDens = SVHD.StocVolHyper(nsteps, which_hyper= which_hyper, indexObs = indexObs, dataObs=dataObs)

elif load_data == "NO":
  #Generate random data
  phi_data = .95
  mu_data = -.5
  sigma_data = 0.25
  nsteps = 945
  stocDens = SVHD.StocVolHyper(nsteps, which_hyper= which_hyper,
                               mu_data = mu_data, sigma_data = sigma_data, phi_data = phi_data)
else:
    raise ValueError("Choose between YES and NO")
phi_data = .95
mu_data = -.5
sigma_data = 0.25
nsteps = 945
stocDens = SVHD.StocVolHyper(nsteps, which_hyper= which_hyper, mu_data = mu_data, sigma_data = sigma_data, phi_data = phi_data)



#Prior on mu ~ N(0,1)

#Prior on sigma
# plt.figure()
# plt.title("Prior on sigma")
# n_sam = 3e4
# normalSamples = np.random.randn(n_sam)
# sig_samples = ( stocDens.sigma.evaluate(normalSamples) )
# plt.hist(sig_samples, bins=1800, normed=True)
# plt.axis([0,40,0,0.3])
# plt.show(False)

# #Prior on phi
# plt.figure()
# plt.title("Prior on phi")
# n_sam = 3e4
# normalSamples = np.random.randn(n_sam)
# phi_samples = ( stocDens.phi.evaluate(normalSamples) )
# plt.hist(phi_samples, bins=120, normed=True)
# plt.show(False)

# # plt.figure()
# # n_sam = 3e4
# # normalSamples = np.random.randn(n_sam)
# # sig_samples = ( stocDens.phi.evaluate(normalSamples) )
# # plt.hist(sig_samples, bins=1800, normed=True)
# # #plt.axis([0,40,0,3])
# # plt.show(False)

# fun = stocDens.log_pdf
# grad = stocDens.grad_x_log_pdf
# hess = stocDens.hess_x_log_pdf
# X = np.random.randn(3,stocDens.dim)
# fun(X)

# grad(X)
# hess(X)

# SVHD.finiteDifference_gradient_X( fun , grad , X , delta = 1e-6, rtol=1e-6)
# SVHD.finiteDifference_hessian_X( grad , hess , X , delta = 1e-6, rtol=1e-6)


# # ################ Laplace approximation #############
# laplace_approx = TM.laplace_approximation( stocDens , ders = 2)

# variances = np.diag(np.linalg.inv(laplace_approx.inv_sigma))
# alpha = 3

# #Plot laplace approximation
# fig1 = plt.figure()
# plt.title("Laplace approximation")
# #fig1 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{t}$", title='Smoothing trajectory')
# ax1 = fig1.gca()
# index_vec = np.arange(stocDens.nsteps)
# mean_laplace_Xt = laplace_approx.mu[stocDens.index_X0:]
# ax1.plot(index_vec, mean_laplace_Xt , '-r', label="mean laplace")
# #ax1.plot(index_vec, stocDens.Xt, '--k', label="truth")
# std_minus = mean_laplace_Xt - alpha*variances[stocDens.index_X0:]
# std_plus = mean_laplace_Xt + alpha*variances[stocDens.index_X0:]
# ax1.fill_between(index_vec, std_minus , std_plus, facecolor='grey', alpha=0.2)
# if load_data == "NO":
#   ax1.plot(index_vec, stocDens.Xt , '--k', label="mean laplace")
# plt.legend(loc='best')
# plt.show(False)


# #Plot posterior data predictive
# fig2 = plt.figure()
# plt.title("Posterior data predictive")
# ax2 = fig2.gca()
# ax2.plot(index_vec,stocDens.dataObs,'.k')
# num_samples = 3e4
# dynamic_samples = laplace_approx.rvs(num_samples)
# dynamic_samples = dynamic_samples[:,stocDens.index_X0:]
# noise_samples = np.random.randn(num_samples,stocDens.nsteps)
# Ypost_samples = noise_samples*np.exp(.5*dynamic_samples)

# q10 = np.percentile(Ypost_samples, 10, axis=0)
# q90 = np.percentile(Ypost_samples, 90, axis=0)
# ax2.fill_between(index_vec, q10 , q90, facecolor='green', alpha=0.25)

# q1 = np.percentile(Ypost_samples, 1, axis=0)
# q99 = np.percentile(Ypost_samples, 99, axis=0)
# ax2.fill_between(index_vec, q1 , q99, facecolor='green', alpha=0.15)

# plt.legend(loc='best')
# plt.show(False)


# #Plot hyper-parameters
# fig3 = plt.figure()
# #fig2 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{t}$", title='Smoothing hyper-parameters')
# ax3 = fig3.gca()
# index_vec = np.arange( sum(stocDens.which_hyper) )
# mean_laplace_hypers = laplace_approx.mu[0:stocDens.index_X0]
# std_minus = mean_laplace_hypers - alpha*variances[0:stocDens.index_X0]
# std_plus = mean_laplace_hypers + alpha*variances[0:stocDens.index_X0]

# print mean_laplace_hypers
# mean_laplace_hypers[stocDens.index_sigma] = stocDens.sigma.evaluate(mean_laplace_hypers[stocDens.index_sigma])
# mean_laplace_hypers[stocDens.index_phi] = stocDens.phi.evaluate(mean_laplace_hypers[stocDens.index_phi])
# std_minus[stocDens.index_sigma] = stocDens.sigma.evaluate(std_minus[stocDens.index_sigma])
# std_minus[stocDens.index_phi] = stocDens.phi.evaluate(std_minus[stocDens.index_phi])
# std_plus[stocDens.index_sigma] = stocDens.sigma.evaluate(std_plus[stocDens.index_sigma])
# std_plus[stocDens.index_phi] = stocDens.phi.evaluate(std_plus[stocDens.index_phi])
# print mean_laplace_hypers

# ax3.plot(index_vec, mean_laplace_hypers , '-go', label="mean laplace")
# ax3.fill_between(index_vec, std_minus , std_plus, facecolor='grey', alpha=0.2)
# plt.legend(loc='best')
# plt.show(False)


#########Test Markov Component############

######## Initial conditions ########
# index_time = 0
# markov_comp = stocDens.get_MarkovFactor(index_time)
# fun = markov_comp.log_pdf
# grad = markov_comp.grad_x_log_pdf
# hess = markov_comp.hess_x_log_pdf

# X = np.random.randn(5,markov_comp.dim)

# print X
# hess(X)
# print X

#print fun(X)
#print grad(X)
#print hess(X)

#print SVHD.finiteDifference_gradient_X( fun , grad, X , delta = 1e-6, rtol=1e-6)
#print SVHD.finiteDifference_hessian_X( grad , hess, X , delta = 1e-6, rtol=1e-6)

######## k>0 ##########
# index_time = 3

# #Build random map
# nhyper = sum(which_hyper)
# dim =  2 + nhyper
# order = 3
# btype = 'rbf'
# tmap = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(dim, order, span='full', btype='rbf')
# temp_coeff = tmap.get_coeffs()
# temp_coeff = temp_coeff + np.random.randn(len(temp_coeff))/10
# tmap.set_coeffs(temp_coeff)

#Select head of the map
# ind_head = 3
# head_map = MAPS.TransportMap(tmap.active_vars[:ind_head], tmap.approx_list[:ind_head])

# component_map = tmap.approx_list[nhyper]
# markov_comp = stocDens.get_MarkovFactor(index_time,component_map=component_map, hyper_map=head_map)

# fun = markov_comp.log_pdf
# grad = markov_comp.grad_x_log_pdf
# hess = markov_comp.hess_x_log_pdf

# X = np.random.randn(5,markov_comp.dim)

# print X
# hess(X)
# print X

#print fun(X)
#print grad(X)
# print hess(X)

#print SVHD.finiteDifference_gradient_X( fun , grad, X , delta = 1e-6, rtol=1e-6)
#print SVHD.finiteDifference_hessian_X( grad , hess, X , delta = 1e-6, rtol=1e-6)

