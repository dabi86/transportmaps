######### STOCHASTIC VOLATILITY MODEL FROM DURBIN(1998) AND RUE(2009) #############

from __future__ import division
import numpy as np
import scipy.stats as stats
import TransportMaps.Densities as DENS
import TransportMaps as TM
import copy as cp

class composition_logdensity_with_mvf(object):
    # Composition logdensity with multivariate function mvf
    def __init__(self, pi, mvf):
        dim = pi.dim
        self.dim=dim
        self.pi = pi
        self.mvf = cp.deepcopy(mvf)
    def log_pdf(self, x, *args, **kwargs):
        if x.shape[1] != self.dim:
            raise ValueError("dimension mismatch")
        return self.pi.log_pdf( self.mvf.evaluate(x) )
    def grad_x_log_pdf(self, x, *args, **kwargs):
        if x.shape[1] != self.dim:
            raise ValueError("dimension mismatch")
        return np.einsum( '...i,...ij->...j',
                          self.pi.grad_x_log_pdf(self.mvf.evaluate(x)),
                          self.mvf.grad_x(x) )
    def hess_x_log_pdf(self, x, *args, **kwargs):
        nax = np.newaxis
        dim = self.dim
        pi = self.pi
        mvf = self.mvf
        if x.shape[1] != dim:
            raise ValueError("dimension mismatch")
        n = x.shape[0]
        dx2logpi = pi.hess_x_log_pdf( mvf.evaluate(x) ).reshape((n,dim**2)) # n x 2d
        dxT = mvf.grad_x( x ) # n x d x d
        dxT2 = (dxT[:,:,:,nax,nax] * dxT[:,nax,nax,:,:]).transpose(
            (0,1,3,2,4)).reshape((n, dim**2, dim, dim))
        A = np.einsum('...i,...ikl->...kl', dx2logpi, dxT2) # n x d x d
        dxlogpi = pi.grad_x_log_pdf( mvf.evaluate(x) ) # n x d
        dx2T = mvf.hess_x(x) # n x d x d x d
        B = np.einsum('...i,...ijk->...jk', dxlogpi, dx2T)
        return A + B

class StocVolHyperMarkovComponent(DENS.Density):
    def __init__(self, index_time, priorDynamic_t, priorDynamic_ic,
                 dataObs, indexObs, which_hyper, mu_sigma=None,
                 component_map = None, hyper_map=None):
        #If \pi^k is the Markov component at time k, this routine implements
        # \pi^k \circ Qhat, where Qhat is a local permuation that swaps Xk with Xkp1
        #in order to compute always triangular maps
        dim =  2 + sum(which_hyper) 
        super(StocVolHyperMarkovComponent,self).__init__(dim)
        self.dataObs  = dataObs.copy()
        self.indexObs = np.array(indexObs) 
        self.priorDynamic_ic = priorDynamic_ic
        self.priorDynamic_t = priorDynamic_t
        self.index_time = index_time

        # Check which hyper-parameters are present and
        # assign local ordering \pi^k: mu,sigma,phi,Xk,Xkp1
        self.is_mu_hyper = which_hyper[0]
        self.is_sigma_hyper = which_hyper[1]
        self.is_phi_hyper = which_hyper[2]
        self.which_hyper = which_hyper
        counter = 0
        if self.is_mu_hyper:
            self.index_mu = counter
            counter += 1
            # Centered normal prior on mu
            self.mu_sigma = mu_sigma 
        if self.is_sigma_hyper:
            self.index_sigma = counter
            counter += 1
        if self.is_phi_hyper:
            self.index_phi = counter
            counter += 1
        self.index_Xt = counter
        counter += 1
        self.index_Xtp1 = counter

        if index_time > 0:
            if component_map is None:
                raise ValueError("You must prescribe a component map")
            if dim>2 and hyper_map is None:
                raise ValueError("You must prescribe a hyper-map")
                
            # First need to build the frakF function for
            # the local ordering: mu,sigma,phi,Xk,Xkp1
            active_vars = [] 
            approx_list = []
            # Append the component from the previous transport map
            active_vars.append( list(range(self.index_Xtp1)) )
            approx_list.append( cp.deepcopy(component_map) )

            # Append the identity component for Xkp1
            temp_map_1d = \
                TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(1, 1)
            id_comp = temp_map_1d.approx_list[0]
            active_vars.append(  [self.index_Xtp1] )
            approx_list.append(  id_comp )

            # Add the hyper map (if present)
            if dim>2:  
                for jj in np.arange(hyper_map.dim):
                    active_vars.append(
                        cp.deepcopy( hyper_map.active_vars[jj] ) )
                    approx_list.append(
                        cp.deepcopy( hyper_map.approx_list[jj]  ) )

            # Create the frakF function
            frakF = Maps.TransportMap(active_vars, approx_list)
            self.priorDynamic_t_composed_frakF = \
                composition_logdensity_with_mvf(priorDynamic_t, frakF)

    def pdf(self, x, params=None):
        return np.exp(self.log_pdf(x, params))

    def log_pdf(self, x, params=None):
        Xc = x.copy() # Will need to modify x within the routine
        index_time = self.index_time
        indexObs = self.indexObs
        dataObs = self.dataObs
        index_Xt = self.index_Xt
        index_Xtp1 = self.index_Xtp1

        # Action permuatation Qhat: swap Xk with Xkp1
        temp = Xc[:,index_Xt].copy()
        Xc[:,index_Xt] = Xc[:,index_Xtp1]
        Xc[:,index_Xtp1] = temp

        if index_time == 0: # FIRST STEP (Initial conditions)
            logpdf = 0.
            # Initial conditions-->local ordering:  X0, Xmu, Xphi, Xsigma
            indexToExtract = [index_Xt]  + list(range(index_Xt))
            logpdf += self.priorDynamic_ic.log_pdf( Xc[:, indexToExtract] )

            # Prior dynamic-->local ordering: Xt, Xtp1, Xmu, Xsigma, Xphi
            indexToExtract = [index_Xt , index_Xtp1] + list(range(index_Xt))
            logpdf += self.priorDynamic_t.log_pdf( Xc[:, indexToExtract] )

            # Prior hyper-parameters
            if self.is_mu_hyper:
                logpdf += -.5*Xc[:,self.index_mu]**2/self.mu_sigma**2
            if self.is_sigma_hyper:
                logpdf += -.5*Xc[:,self.index_sigma]**2
            if self.is_phi_hyper:
                logpdf += -.5*Xc[:,self.index_phi]**2

            # Likelihood(?)
            obsX0 = np.where( indexObs == index_time)[0]
            if len(obsX0)>0: #There is an observation at the first time step
                ii = obsX0[0]
                logpdf += -.5*dataObs[ii]**2*np.exp(-Xc[:,index_Xt]) \
                          - .5*Xc[:,index_Xt]
            obsX1 = np.where( indexObs == (index_time+1) )[0]
            if len(obsX1)>0: #There is an observation at the second time step
                jj = obsX1[0]
                logpdf += -.5*dataObs[jj]**2*np.exp(-Xc[:,index_Xtp1]) \
                          - .5*Xc[:,index_Xtp1]

        else: # NOT the first time step
            logpdf = 0.

            # Prior dynamics-->local ordering: Xt, Xtp1, Xmu, Xsigma, Xphi
            # frakF already accounts for the right local order
            logpdf += self.priorDynamic_t_composed_frakF.log_pdf(Xc)

            # Reference hyper-parameters
            logpdf += -.5*Xc[:,index_Xt]**2
            if self.is_mu_hyper:
                logpdf += -.5*Xc[:,self.index_mu]**2
            if self.is_sigma_hyper:
                logpdf += -.5*Xc[:,self.index_sigma]**2
            if self.is_phi_hyper:
                logpdf += -.5*Xc[:,self.index_phi]**2

            # Likelihood(?)
            obsXtp1 = np.where( indexObs == (index_time+1))[0]
            if len(obsXtp1)>0: #There is an observation at Xtp1
               ii = obsXtp1[0]
               logpdf += -.5*dataObs[ii]**2*np.exp(-Xc[:,index_Xtp1]) - .5*Xc[:,index_Xtp1]

        return logpdf

    def grad_x_log_pdf(self, x, params=None):
        Xc = x.copy() #Will need to modify X within the routine
        index_time = self.index_time
        indexObs = self.indexObs
        dataObs = self.dataObs
        index_Xt = self.index_Xt
        index_Xtp1 = self.index_Xtp1

        grad = np.zeros( Xc.shape )

        # Action permuatation Qhat: swap Xk with Xkp1
        temp = Xc[:,index_Xt].copy()
        Xc[:,index_Xt] = Xc[:,index_Xtp1]
        Xc[:,index_Xtp1] = temp

        if index_time == 0: # FIRST STEP (Initial conditions)
            # Initial conditions-->local ordering:  X0, Xmu, Xphi, Xsigma
            indexToExtract = [index_Xt]  + list(range(index_Xt))
            grad[:,indexToExtract] += \
                self.priorDynamic_ic.grad_x_log_pdf( Xc[:, indexToExtract] )

            # Prior dynamic-->local ordering: Xt, Xtp1, Xmu, Xsigma, Xphi
            indexToExtract = [index_Xt , index_Xtp1] + list(range(index_Xt))
            grad[:,indexToExtract] += \
                self.priorDynamic_t.grad_x_log_pdf( Xc[:, indexToExtract] )

            # Prior hyper-parameters
            if self.is_mu_hyper:
                grad[:,self.index_mu] += -Xc[:,self.index_mu]/self.mu_sigma**2
            if self.is_sigma_hyper:
                grad[:,self.index_sigma] += -Xc[:,self.index_sigma]
            if self.is_phi_hyper:
                grad[:,self.index_phi] += -Xc[:,self.index_phi]

            # Likelihood(?)
            obsX0 = np.where( indexObs == index_time)[0]
            if len(obsX0)>0: #There is an observation at the first time step
                ii = obsX0[0]
                grad[:,index_Xt] += .5*dataObs[ii]**2*np.exp(-Xc[:,index_Xt]) - .5

            obsX1 = np.where( indexObs == (index_time+1) )[0]
            if len(obsX1)>0: #There is an observation at the second time step
                jj = obsX1[0]
                grad[:,index_Xtp1] += .5*dataObs[jj]**2*np.exp(-Xc[:,index_Xtp1]) - .5

        else: # NOT the first time step
            # Prior dynamics-->local ordering: Xt, Xtp1, Xmu, Xsigma, Xphi
            # frakF already accounts for the right local order
            grad += self.priorDynamic_t_composed_frakF.grad_x_log_pdf(Xc)

            # Reference hyper-parameters
            grad[:,index_Xt] += -Xc[:,index_Xt]
            if self.is_mu_hyper:
                grad[:,self.index_mu] += -Xc[:,self.index_mu]
            if self.is_sigma_hyper:
                grad[:,self.index_sigma] += -Xc[:,self.index_sigma]
            if self.is_phi_hyper:
                grad[:,self.index_phi] += -Xc[:,self.index_phi]

            # Likelihood(?)
            obsXtp1 = np.where( indexObs == (index_time+1))[0]
            if len(obsXtp1)>0: #There is an observation at Xtp1
                ii = obsXtp1[0]
                grad[:,index_Xtp1] += .5*dataObs[ii]**2*np.exp(-Xc[:,index_Xtp1]) - .5

        # Action permuatation Qhat: swap Xk with Xkp1
        temp = grad[:,index_Xt].copy()
        grad[:,index_Xt] = grad[:,index_Xtp1]
        grad[:,index_Xtp1] = temp

        return grad

    def hess_x_log_pdf(self, x, params=None):
        Xc = x.copy() #Will need to modify x within the routine
        index_time = self.index_time
        indexObs = self.indexObs
        dataObs = self.dataObs
        index_Xt = self.index_Xt
        index_Xtp1 = self.index_Xtp1

        npoints, dim = Xc.shape
        hess = np.zeros( (npoints, dim,  dim) )

        # Action permuatation Qhat: swap Xk with Xkp1
        temp = Xc[:,index_Xt].copy()
        Xc[:,index_Xt] = Xc[:,index_Xtp1]
        Xc[:,index_Xtp1] = temp

        if index_time == 0: # FIRST STEP (Initial conditions)
            # Initial conditions-->local ordering:  X0, Xmu, Xphi, Xsigma
            indexToExtract = [index_Xt]  + list(range(index_Xt))
            hess_ic = self.priorDynamic_ic.hess_x_log_pdf( Xc[:, indexToExtract] )
            ii=(slice(None),)+np.ix_(indexToExtract,indexToExtract)
            hess[ii] += hess_ic

            # Prior dynamic-->local ordering: Xt, Xtp1, Xmu, Xsigma, Xphi
            indexToExtract = [index_Xt , index_Xtp1] + list(range(index_Xt))
            hess_t = self.priorDynamic_t.hess_x_log_pdf( Xc[:, indexToExtract] )
            ii=(slice(None),)+np.ix_(indexToExtract,indexToExtract)
            hess[ii] += hess_t

            # Prior hyper-parameters
            if self.is_mu_hyper:
              hess[:,self.index_mu,self.index_mu] += -1./self.mu_sigma**2
            if self.is_sigma_hyper:
              hess[:,self.index_sigma,self.index_sigma] += -1.
            if self.is_phi_hyper:
              hess[:,self.index_phi,self.index_phi] += -1.

            # Likelihood(?)
            obsX0 = np.where( indexObs == index_time)[0]
            if len(obsX0)>0: #There is an observation at the first time step
                ii = obsX0[0]
                hess[:,index_Xt,index_Xt] += -.5*dataObs[ii]**2*np.exp(-Xc[:,index_Xt])
            obsX1 = np.where( indexObs == (index_time+1) )[0]
            if len(obsX1)>0: #There is an observation at the second time step
                jj = obsX1[0]
                hess[:,index_Xtp1,index_Xtp1] += -.5*dataObs[jj]**2*np.exp(-Xc[:,index_Xtp1])

        else: # NOT the first time step
            # Prior dynamics-->local ordering: Xt, Xtp1, Xmu, Xsigma, Xphi
            # frakF already accounts for the right local order
            hess += self.priorDynamic_t_composed_frakF.hess_x_log_pdf(Xc)

            # Reference hyper-parameters
            hess[:,index_Xt,index_Xt] += -1.
            if self.is_mu_hyper:
                hess[:,self.index_mu,self.index_mu] += -1.
            if self.is_sigma_hyper:
                hess[:,self.index_sigma,self.index_sigma] += -1.
            if self.is_phi_hyper:
                hess[:,self.index_phi,self.index_phi] += -1.

            # Likelihood(?)
            obsXtp1 = np.where( indexObs == (index_time+1))[0]
            if len(obsXtp1)>0: #There is an observation at Xtp1
                ii = obsXtp1[0]
                hess[:,index_Xtp1,index_Xtp1] += \
                    -.5*dataObs[ii]**2*np.exp(-Xc[:,index_Xtp1])

        # Action left permuatation Qhat: swap rows Xk with Xkp1
        temp = hess[:,index_Xt,:].copy()
        hess[:,index_Xt,:] = hess[:,index_Xtp1,:]
        hess[:,index_Xtp1,:] = temp

        # Action right permuatation Qhat: swap columns Xk with Xkp1
        temp = hess[:,:,index_Xt].copy()
        hess[:,:,index_Xt] = hess[:,:,index_Xtp1]
        hess[:,:,index_Xtp1] = temp

        return hess

################ Finite differences routines #######################

def finiteDifference_gradient_X( fun , grad , x , delta = 1e-6, rtol=1e-6):
        # x is a 2-d array of points. The first dimension corresponds to the number of points.
        npoints = x.shape[0]
        ndim = x.shape[1]
        grad_X = grad(x)
        dF_X = np.zeros( (npoints, ndim) )
        for kk in np.arange(ndim):
            Xp = np.copy(x)
            Xp[:,kk] += delta
            Xm = np.copy(x)
            Xm[:,kk] -= delta
            Fp = fun(Xp)
            Fm = fun(Xm)
            dF_X[:,kk] = (Fp-Fm)/(2*delta)
        rerror_vec = np.linalg.norm(grad_X - dF_X, axis = 0)/np.linalg.norm(grad_X)
        aerror_vec = np.linalg.norm(grad_X - dF_X, axis = 0)
        print (grad_X - dF_X)
        #print dF_X
        maxrErr =  np.max(rerror_vec)
        maxraErr =  np.max(aerror_vec)
        iscorrect =  maxrErr <= rtol
        return iscorrect, maxrErr, maxraErr

def finiteDifference_hessian_X( fun , hess , x , delta = 1e-6, rtol=1e-6):
        # X is a 2-d array of points. The first dimension corresponds to the number of points.
        npoints = x.shape[0]
        ndim = x.shape[1]
        hess_X = hess(x)
        d2F_X = np.zeros( (npoints, ndim, ndim) )
        for kk in np.arange(ndim):
            Xp = np.copy(x)
            Xp[:,kk] += delta
            Xm = np.copy(x)
            Xm[:,kk] -= delta
            Fp = fun(Xp)
            Fm = fun(Xm)
            d2F_X[:,:,kk] = (Fp-Fm)/(2*delta)
        rerror_vec = np.zeros(npoints)
        for kk in np.arange(npoints):
            rerror_vec[kk] = np.linalg.norm( d2F_X[kk,:,:]-hess_X[kk,:,:])/np.linalg.norm(hess_X[kk,:,:])
            print(d2F_X[kk,:,:]-hess_X[kk,:,:])
            maxrErr =  np.max(rerror_vec)
        iscorrect =  maxrErr <= rtol
        return iscorrect, maxrErr

def finiteDifference1D_gradient_X( fun , grad , x , delta = 1e-6, rtol=1e-6):
        # X is a 1-d array of points.
        npoints = len(x)
        grad_X = grad(x)
        dF_X = np.zeros(  npoints   )
        Xp = np.copy(x)
        Xp += delta
        Xm = np.copy(x)
        Xm -= delta
        Fp = fun(Xp)
        Fm = fun(Xm)
        dF_X = (Fp-Fm)/(2*delta)
        rerror_vec = np.linalg.norm(grad_X - dF_X)/np.linalg.norm(grad_X)
        aerror_vec = np.linalg.norm(grad_X - dF_X)
        maxrErr =  np.max(rerror_vec)
        maxraErr =  np.max(aerror_vec)
        iscorrect =  maxrErr <= rtol
        return iscorrect, maxrErr, maxraErr

    # #Check gradient
    # fun = svDens.log_pdf
    # grad = svDens.grad_x_log_pdf
    # X = np.random.rand( 10, ndim  )
    # finiteDifference_gradient_X( fun , grad , X )

    # #Check hessian
    # fun = svDens.grad_x_log_pdf
    # hess = svDens.hess_x_log_pdf
    # X = np.random.rand( 8, ndim  )
    # finiteDifference_hessian_X( fun , hess , X )


###########################################
def generateData(nsteps, indexObs, mu = None, sigma = None, phi = None):
  numObs = len(indexObs)
  if numObs == 0:
    raise ValueError("You must prescribe observation indeces")

  #Random initial conditions
  x0 = sigma/np.sqrt(1.-phi**2)*np.random.randn(1) + mu
  #Noise observations
  noise_obs = np.random.randn(numObs)
  #Noise dynamics
  noise_dyn = np.random.randn(nsteps)
  # Allocate variables
  Xt = np.zeros(nsteps)
  Xt[0] = x0
  # Simulate
  for ii in range(nsteps-1):
    Xt[ii+1] = mu+phi*(Xt[ii]-mu) + sigma*noise_dyn[ii]
  #Collect observations
  dataObs = noise_obs*np.exp(.5*Xt[indexObs])

  return (dataObs, Xt)

def trim_density(old_dens, new_nsteps):
  old_dim = old_dens.dim
  old_nhypers = sum(old_dens.which_hyper)
  old_nsteps = old_dim - old_nhypers
  if new_nsteps >= old_nsteps:
    raise ValueError("You can only decrease the number of timestpes")
  new_dens = cp.deepcopy(old_dens)
  new_dim = new_nsteps + old_nhypers
  new_dens.dim = new_dim
  new_dens.nsteps = new_nsteps
#   if old_dens.Xt is not None:
#     new_dens.Xt = old_dens.Xt[0:new_nsteps].copy()
  new_dens.tvec = old_dens.tvec[0:new_nsteps].copy()
  #Determine number of observations to retain
  new_tobs = np.where(old_dens.tobs<=(new_nsteps-1))
  new_dens.tobs = new_tobs[0].copy() #This line would not work if we did not have any observation at all
  new_dens.indexObs = new_dens.tobs.copy()
  num_obs = len(new_dens.tobs)
  new_dens.dataObs = old_dens.dataObs[0:num_obs].copy()

  return new_dens


class StocVolHyper(DENS.Density):
    def __init__(self, nsteps, indexObs=None, dataObs=None,
                 which_hyper=(False, False, False),
                 mu=None, sigma=None, phi=None, mu_sigma=1.,
                 sigma_alpha=1., sigma_beta=10., phi_mean=3., phi_std=1., Xt=None,
                 mu_data = -.5, sigma_data = .25, phi_data = .95 ):

      dim =  nsteps + sum(which_hyper)
      super(StocVolHyper,self).__init__(dim)
      if Xt is not None:
        if len(Xt) == nsteps:
          self.Xt = Xt.copy() #True dynamic
        else:
          raise ValueError("Check length of the true dynamic")
      self.tvec = np.arange(nsteps)
      self.nsteps = nsteps

      #Check which hyper-parameters are present and assign ordering: mu,sigma,phi,X0,X1,....
      self.is_mu_hyper = which_hyper[0]
      self.is_sigma_hyper = which_hyper[1]
      self.is_phi_hyper = which_hyper[2]
      self.which_hyper = which_hyper

      counter = 0
      if self.is_mu_hyper==False:
        if not type(mu) in[float,np.float32,np.float64]:
          raise ValueError("You must prescribe a constant for mu")
        else:
          self.mu = ConstantFunction(mu)
          self.mu_const = mu
        self.mu_sigma = None
      else:
        self.index_mu = counter
        counter += 1
        self.mu = mu
        self.mu_sigma = mu_sigma #Centered normal prior on mu

      if which_hyper[1]==True:
        f_sigma = F_sigma(sigma_alpha,sigma_beta) #Inverse gamma prior on square sigma
        self.sigma = f_sigma
        self.index_sigma = counter
        counter += 1
      else:
        if not type(sigma) in[float,np.float32,np.float64]:
          raise ValueError("You must prescribe a constant for sigma")
        else:
          self.sigma = ConstantFunction(sigma)
          self.sigma_const = sigma

      if which_hyper[2]==True:
        f_phi = F_phi(phi_mean,phi_std)
        self.phi = f_phi
        self.index_phi = counter
        counter += 1
      else:
        if not type(phi) in[float,np.float32,np.float64]:
          raise ValueError("You must prescribe a constant for phi")
        else:
          self.phi = ConstantFunction(phi)
          self.phi_const = phi

      self.index_X0 = counter

      #Initialize prior densities
      self.priorDynamic_ic = PriorDynamic_IC( which_hyper=which_hyper, mu=self.mu, sigma=self.sigma, phi=self.phi )
      self.priorDynamic_t = PriorDynamic_t( which_hyper=which_hyper, mu=self.mu, sigma=self.sigma, phi=self.phi )

      #Take care of observations
      if dataObs is not None and indexObs is None:
        if len(dataObs)==nsteps:
          self.dataObs= dataObs.copy()
          self.indexObs = np.arange(nsteps) #The observation indeces are implicit in this case
        else:
          raise ValueError("You must prescribe the observation indeces")
      if dataObs is not None and indexObs is not None:
        if len(indexObs) != len(set(indexObs)):
          raise ValueError("The are repeated values in the observations indeces")
        if len(dataObs)==len(indexObs):
          self.dataObs = dataObs.copy()
          self.indexObs = indexObs.copy()
        else:
          raise ValueError("Check length of observations indeces")
      if dataObs is None:
        #Must genegenerate data in this case
        if indexObs is None:
          self.indexObs = np.arange(nsteps) #Observe everywhere by default
        else:
          if len(indexObs) != len(set(indexObs)):
            raise ValueError("The are repeated values in the observations indeces")
          else:
            self.indexObs = indexObs.copy()
        (dataObs, Xt)=generateData(nsteps, self.indexObs, mu = mu_data, sigma = sigma_data, phi = phi_data)
        self.dataObs = dataObs.copy()
        self.Xt = Xt.copy()
      self.tobs = self.tvec[self.indexObs]

    def get_MarkovFactor(self,index_time, component_map=None, hyper_map=None):
        return StocVolHyperMarkovComponent( index_time,
                                           self.priorDynamic_t, self.priorDynamic_ic,
                                           dataObs = self.dataObs, indexObs = self.indexObs,
                                           which_hyper = self.which_hyper, mu_sigma = self.mu_sigma,
                                           component_map = component_map, hyper_map=hyper_map)

    def pdf(self, x, *args, **kwargs):
        return np.exp(self.log_pdf(x))

    def log_pdf(self, x, *args, **kwargs):
        # X is a 2-d array of points.
        # The first dimension corresponds to the number of points.
        dataObs = self.dataObs
        indexObs = self.indexObs
        index_X0 = self.index_X0

        #1-Observations
        actual_indexObs = indexObs + index_X0
        logpdf  = -.5*np.sum( dataObs**2*np.exp( - x[:,actual_indexObs] ) + x[:,actual_indexObs] , axis = 1)

        #2-Initial conditions
        indexToExtract = [self.index_X0] +  list(range(self.index_X0))

#         if self.is_mu_hyper:
#           indexToExtract += [self.index_mu]
#         if self.is_sigma_hyper:
#           indexToExtract += [self.index_sigma]
#         if self.is_phi_hyper:
#           indexToExtract += [self.index_phi]

        logpdf += self.priorDynamic_ic.log_pdf( x[:, indexToExtract] )

        #3-Prior dynamic

        for ii in np.arange(self.nsteps-1):
          indexToExtract = [self.index_X0+ii , self.index_X0+ii+1] + \
                           list(range(self.index_X0))
          logpdf += self.priorDynamic_t.log_pdf( x[:,indexToExtract] )

        #4-Prior hyper-parameters
        if self.is_mu_hyper:
          logpdf += -.5*x[:,self.index_mu]**2/self.mu_sigma**2
        if self.is_sigma_hyper:
          logpdf += -.5*x[:,self.index_sigma]**2
        if self.is_phi_hyper:
          logpdf += -.5*x[:,self.index_phi]**2

        return logpdf

    def grad_x_log_pdf(self, x, *args, **kwargs):
        dataObs = self.dataObs
        indexObs = self.indexObs
        index_X0 = self.index_X0

        grad = np.zeros( x.shape )

        #1-Observations
        actual_indexObs = indexObs + index_X0
        grad[:,actual_indexObs]  = .5*dataObs**2*np.exp(-x[:,actual_indexObs]) - .5

        #2-Initial conditions
        indexToExtract = [self.index_X0] +  list(range(self.index_X0))

#         indexToExtract = [self.index_X0]
#         if self.is_sigma_hyper:
#           indexToExtract += [self.index_sigma]
#         if self.is_phi_hyper:
#           indexToExtract += [self.index_phi]

        grad_ic = self.priorDynamic_ic.grad_x_log_pdf( x[:, indexToExtract] )
        grad[:,indexToExtract] += grad_ic

        #3-Prior dynamic
        for ii in np.arange(self.nsteps-1):
          indexToExtract = [self.index_X0+ii , self.index_X0+ii+1] + \
                           list(range(self.index_X0))
          grad_t = self.priorDynamic_t.grad_x_log_pdf( x[:,indexToExtract] )
          grad[:,indexToExtract] += grad_t

        #4-Prior hyper-parameters
        if self.is_mu_hyper:
          grad[:,self.index_mu] += -x[:,self.index_mu]/self.mu_sigma**2
        if self.is_sigma_hyper:
          grad[:,self.index_sigma] += -x[:,self.index_sigma]
        if self.is_phi_hyper:
          grad[:,self.index_phi] += -x[:,self.index_phi]

        return grad

    def hess_x_log_pdf(self, x, *args, **kwargs):
        dataObs = self.dataObs
        indexObs = self.indexObs
        index_X0 = self.index_X0

        npoints, dim = x.shape
        hess = np.zeros( (npoints, dim,  dim) )

        #1-Observations
        actual_indexObs = indexObs + index_X0
        for kk in range(len(indexObs)):
          hess[:,actual_indexObs[kk],actual_indexObs[kk]]  = -.5*dataObs[kk]**2*np.exp(-x[:,actual_indexObs[kk]])

        #2-Initial conditions
        indexToExtract = [self.index_X0] +  list(range(self.index_X0))

#         indexToExtract = [self.index_X0]
#         if self.is_sigma_hyper:
#           indexToExtract += [self.index_sigma]
#         if self.is_phi_hyper:
#           indexToExtract += [self.index_phi]

        hess_ic = self.priorDynamic_ic.hess_x_log_pdf( x[:, indexToExtract] )
        ii=(slice(None),)+np.ix_(indexToExtract,indexToExtract)
        hess[ii] += hess_ic

        #3-Prior dynamic
        for ii in np.arange(self.nsteps-1):
          indexToExtract = [self.index_X0+ii , self.index_X0+ii+1] + \
                           list(range(self.index_X0))
          hess_t = self.priorDynamic_t.hess_x_log_pdf( x[:,indexToExtract] )
          ii=(slice(None),)+np.ix_(indexToExtract,indexToExtract)
          hess[ii] += hess_t

        #4-Prior hyper-parameters
        if self.is_mu_hyper:
          hess[:,self.index_mu,self.index_mu] += -1./self.mu_sigma**2
        if self.is_sigma_hyper:
          hess[:,self.index_sigma,self.index_sigma] += -1.
        if self.is_phi_hyper:
          hess[:,self.index_phi,self.index_phi] += -1.

        return hess

class F_phi(object): # ??
    def __init__(self,mean,std):
      self.mean = mean
      self.std = std
    def evaluate(self, x):
      Xh = self.mean+self.std*x
      return  2*( np.exp(Xh) ) / (1+np.exp(Xh)) - 1.
    def grad_x(self,x):
      Xh = self.mean+self.std*x
      return  2*self.std*np.exp(Xh) / ( np.exp(Xh) + 1. )**2
    def hess_x(self,x):
      Xh = self.mean+self.std*x
      return  -2*self.std**2*np.exp(Xh) * ( np.exp(Xh) - 1. ) / ( np.exp(Xh) + 1. )**3

class F_sigma(object): #Square root of an inverse gamma
    def __init__(self,alpha,beta):
      self.alpha = alpha
      self.beta = beta
      self.F_Gauss2InvGamma=F_Gauss2InvGamma(alpha,beta)
    def evaluate(self, x):
      return  np.sqrt( self.F_Gauss2InvGamma.evaluate(x) )
    def grad_x(self,x):
      return  .5*(self.F_Gauss2InvGamma.evaluate(x)**-0.5)*self.F_Gauss2InvGamma.grad_x(x)
    def hess_x(self,x):
      FX=self.F_Gauss2InvGamma.evaluate(x)
      return  -.25*(FX**-1.5)*(self.F_Gauss2InvGamma.grad_x(x)**2)+.5*(FX**-0.5)*self.F_Gauss2InvGamma.hess_x(x)

# class F_Gauss2Beta(object):
#     def __init__(self,alpha,beta):
#       self.alpha = alpha
#       self.beta = beta
#       self.std = stats.norm()
#       self.betaDist = stats.beta(alpha,beta)
#     def evaluate(self, X):
#       return self.betaDist.ppf( self.std.cdf(X) )
#     def grad_x(self,X):
#       FX = self.evaluate(X)
#       return self.std.pdf(X)/self.betaDist.pdf(FX)
#     def hess_x(self,X):
#       FX = self.evaluate(X)
#       grad_FX = self.grad_x(X)
#       a = self.alpha
#       b = self.beta
#       gamma = ( (a+b-2)*FX - (a-1) )/( FX*(FX-1)  )
#       return - grad_FX*( X + grad_FX*gamma )

class F_Gauss2InvGamma(object):
    def __init__(self,alpha,beta):
      self.alpha = alpha
      self.beta = beta
      self.std = stats.norm()
      self.invGamma = stats.invgamma(alpha, scale = beta)
    def evaluate(self, X):
      return self.invGamma.ppf( self.std.cdf(X) )
    def grad_x(self,X):
      FX = self.evaluate(X)
      return self.std.pdf(X)/self.invGamma.pdf(FX)
    def hess_x(self,X):
      FX = self.evaluate(X)
      grad_FX = self.grad_x(X)
      a = self.alpha
      b = self.beta
      gamma = (b - (a+1)*FX )/FX**2
      return - grad_FX*( X + grad_FX*gamma )

class ConstantFunction(object):
    def __init__(self,constant):
      self.constant = constant
    def __call__(self):
      return self.constant

class PriorDynamic_IC(DENS.Density):
    def __init__(self, which_hyper=(False, False, False), mu=None, sigma=None, phi=None ):

      #Local ordering of the input function: X0, Xmu, Xphi, Xsigma

      dim =  1 + sum(which_hyper)
      super(PriorDynamic_IC,self).__init__(dim)

      self.is_mu_hyper = which_hyper[0]
      self.is_sigma_hyper = which_hyper[1]
      self.is_phi_hyper = which_hyper[2]

      counter = 1

      if self.is_mu_hyper==False:
        if not isinstance(mu,ConstantFunction):
          raise ValueError("You must prescribe a constant function for mu")
        else:
          self.mu = mu
      else:
        self.index_mu = counter
        counter += 1

      if self.is_sigma_hyper==True:
        if not isinstance(sigma,F_sigma):
          raise ValueError("You must define the function f_sigma")
        self.sigma = sigma
        self.index_sigma = counter
        counter += 1
      else:
        if not isinstance(sigma,ConstantFunction):
          raise ValueError("You must prescribe a constant function for sigma")
        else:
          self.sigma = sigma

      if self.is_phi_hyper==True:
        if not isinstance(phi,F_phi):
          raise ValueError("You must define the function f_phi")
        self.phi = phi
        self.index_phi = counter
        counter += 1
      else:
        if not isinstance(phi,ConstantFunction):
          raise ValueError("You must prescribe a constant function for phi")
        else:
          self.phi = phi

    def pdf(self, x, params=None):
        return np.exp(self.log_pdf(x, params))

    def log_pdf(self, x, params=None):
        #X0, Xsigma, Xphi = extractVariables(X)

        ##### Extract Variables #####################
        X0 = x[:,0]

        if self.is_mu_hyper:
          Xmu = x[:,self.index_mu]
        else:
          Xmu = self.mu()

        if self.is_sigma_hyper:
          Xsigma = x[:,self.index_sigma]
          f_Xsigma = self.sigma.evaluate(Xsigma)
        else:
          f_Xsigma = self.sigma()

        if self.is_phi_hyper:
          Xphi = x[:,self.index_phi]
          f_Xphi = self.phi.evaluate(Xphi)
        else:
          f_Xphi = self.phi()
        ############################################

        return -.5*(1-f_Xphi**2)/f_Xsigma**2*(X0-Xmu)**2 - np.log(f_Xsigma) + 0.5 * np.log(1-f_Xphi**2)

    def grad_x_log_pdf(self, x, params=None):

        ##### Extract Variables #####################
        X0 = x[:,0]

        if self.is_mu_hyper:
          Xmu = x[:,self.index_mu]
        else:
          Xmu = self.mu()

        if self.is_sigma_hyper:
          Xsigma = x[:,self.index_sigma]
          f_Xsigma = self.sigma.evaluate(Xsigma)
          grad_f_Xsigma = self.sigma.grad_x(Xsigma)
        else:
          f_Xsigma = self.sigma()

        if self.is_phi_hyper:
          Xphi = x[:,self.index_phi]
          f_Xphi = self.phi.evaluate(Xphi)
          grad_f_Xphi = self.phi.grad_x(Xphi)
        else:
          f_Xphi = self.phi()
        ############################################

        grad = np.zeros( x.shape )

        grad[:,0] = - (1-f_Xphi**2)/f_Xsigma**2*(X0-Xmu)

        if self.is_mu_hyper:
          grad[:,self.index_mu] = (1-f_Xphi**2)/f_Xsigma**2*(X0-Xmu)

        if self.is_sigma_hyper:
          grad[:,self.index_sigma] = grad_f_Xsigma/f_Xsigma*( (1-f_Xphi**2)/f_Xsigma**2*(X0-Xmu)**2 - 1 )

        if self.is_phi_hyper:
          grad[:,self.index_phi] = f_Xphi*grad_f_Xphi/f_Xsigma**2*(X0-Xmu)**2 - f_Xphi/(1-f_Xphi**2)*grad_f_Xphi

        return grad

    def hess_x_log_pdf(self, x, params=None):

        ##### Extract Variables #####################

        X0 = x[:,0]

        if self.is_mu_hyper:
          Xmu = x[:,self.index_mu]
        else:
          Xmu = self.mu()

        if self.is_sigma_hyper:
          Xsigma = x[:,self.index_sigma]
          f_Xsigma = self.sigma.evaluate(Xsigma)
          grad_f_Xsigma = self.sigma.grad_x(Xsigma)
          hess_f_Xsigma = self.sigma.hess_x(Xsigma)
        else:
          f_Xsigma = self.sigma()

        if self.is_phi_hyper:
          Xphi = x[:,self.index_phi]
          f_Xphi = self.phi.evaluate(Xphi)
          grad_f_Xphi = self.phi.grad_x(Xphi)
          hess_f_Xphi = self.phi.hess_x(Xphi)
        else:
          f_Xphi = self.phi()


        ############################################

        hess = np.zeros( (x.shape[0], x.shape[1], x.shape[1]) )

        hess[:,0,0] = -(1-f_Xphi**2)/f_Xsigma**2

        if self.is_mu_hyper:
          hess[:,self.index_mu, self.index_mu] = -(1-f_Xphi**2)/f_Xsigma**2
          hess[:,0,self.index_mu] = (1-f_Xphi**2)/f_Xsigma**2
          hess[:,self.index_mu,0] = hess[:,0,self.index_mu]

          if self.is_sigma_hyper:
            hess[:,self.index_mu,self.index_sigma] = -2*grad_f_Xsigma/f_Xsigma**3*(1-f_Xphi**2)*(X0-Xmu)
            hess[:,self.index_sigma, self.index_mu] = hess[:,self.index_mu,self.index_sigma]

          if self.is_phi_hyper:
            hess[:,self.index_mu , self.index_phi] = -2*f_Xphi/f_Xsigma**2*grad_f_Xphi*(X0-Xmu)
            hess[:,self.index_phi,self.index_mu] = hess[:,self.index_mu , self.index_phi]



        if self.is_sigma_hyper:
          hess[:,self.index_sigma,self.index_sigma] = \
            (1-f_Xphi**2)/f_Xsigma**3*(X0-Xmu)**2*(hess_f_Xsigma-3*grad_f_Xsigma**2/f_Xsigma) + grad_f_Xsigma**2/f_Xsigma**2 - hess_f_Xsigma/f_Xsigma
          hess[:,0,self.index_sigma] = 2*grad_f_Xsigma/f_Xsigma**3*(1-f_Xphi**2)*(X0-Xmu)
          hess[:,self.index_sigma,0] = hess[:,0,self.index_sigma]

          if self.is_phi_hyper:
            hess[:,self.index_sigma,self.index_phi]= \
               -2*f_Xphi/f_Xsigma**3*grad_f_Xphi*grad_f_Xsigma*(X0-Xmu)**2
            hess[:,self.index_phi,self.index_sigma] = hess[:,self.index_sigma,self.index_phi]

        if self.is_phi_hyper:
          hess[:,self.index_phi,self.index_phi] = \
            (grad_f_Xphi**2 + f_Xphi*hess_f_Xphi)*( (X0-Xmu)**2/f_Xsigma**2-1./(1.-f_Xphi**2))-2*f_Xphi**2*grad_f_Xphi**2/(1.-f_Xphi**2)**2
          hess[:,0,self.index_phi] = 2*f_Xphi/f_Xsigma**2*grad_f_Xphi*(X0-Xmu)
          hess[:,self.index_phi,0] = hess[:,0,self.index_phi]

        return hess

class PriorDynamic_t(DENS.Density):
    def __init__(self, which_hyper=(False, False, False), mu=None, sigma=None, phi=None ):
      #Local ordering of the input function: Xt, Xtp1, Xmu, Xsigma, Xphi

      dim =  2 + sum(which_hyper)
      super(PriorDynamic_t,self).__init__(dim)

      self.is_mu_hyper = which_hyper[0]
      self.is_sigma_hyper = which_hyper[1]
      self.is_phi_hyper = which_hyper[2]

      counter = 2

      if self.is_mu_hyper==False:
        if not isinstance(mu,ConstantFunction):
          raise ValueError("You must prescribe a constant function for mu")
        else:
          self.mu = mu
      else:
        self.index_mu = counter
        counter += 1

      if self.is_sigma_hyper==True:
        if not isinstance(sigma,F_sigma):
          raise ValueError("You must define the function f_sigma")
        self.sigma = sigma
        self.index_sigma = counter
        counter += 1
      else:
        if not isinstance(sigma,ConstantFunction):
          raise ValueError("You must prescribe a constant function for sigma")
        else:
          self.sigma = sigma

      if self.is_phi_hyper==True:
        if not isinstance(phi,F_phi):
          raise ValueError("You must define the function f_phi")
        self.phi = phi
        self.index_phi = counter
        counter += 1
      else:
        if not isinstance(phi,ConstantFunction):
          raise ValueError("You must prescribe a constant function for phi")
        else:
          self.phi = phi

    def pdf(self, x, params=None):
        return np.exp(self.log_pdf(x, params))

    def log_pdf(self, x, params=None):
        #Xt, Xtp1, Xmu, Xphi, Xsigma  = extractVariables(X)

        ##### Extract Variables #####################

        Xt = x[:,0]
        Xtp1 = x[:,1]

        if self.is_mu_hyper:
          Xmu = x[:,self.index_mu]
        else:
          Xmu = self.mu()

        if self.is_sigma_hyper:
          Xsigma = x[:,self.index_sigma]
          f_Xsigma = self.sigma.evaluate(Xsigma)
        else:
          f_Xsigma = self.sigma()

        if self.is_phi_hyper:
          Xphi = x[:,self.index_phi]
          f_Xphi = self.phi.evaluate(Xphi)
        else:
          f_Xphi = self.phi()

         ############################################

        return -.5*1/f_Xsigma**2*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )**2 - np.log(f_Xsigma)

    def grad_x_log_pdf(self, x, params=None):

        ##### Extract Variables #####################

        Xt = x[:,0]
        Xtp1 = x[:,1]

        if self.is_mu_hyper:
          Xmu = x[:,self.index_mu]
        else:
          Xmu = self.mu()

        if self.is_sigma_hyper:
          Xsigma = x[:,self.index_sigma]
          f_Xsigma = self.sigma.evaluate(Xsigma)
          grad_f_Xsigma = self.sigma.grad_x(Xsigma)
        else:
          f_Xsigma = self.sigma()

        if self.is_phi_hyper:
          Xphi = x[:,self.index_phi]
          f_Xphi = self.phi.evaluate(Xphi)
          grad_f_Xphi = self.phi.grad_x(Xphi)
        else:
          f_Xphi = self.phi()


        ############################################

        grad = np.zeros( x.shape )

        grad[:,0] = f_Xphi/f_Xsigma**2*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )
        grad[:,1] = -1/f_Xsigma**2*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )

        if self.is_mu_hyper:
          grad[:,self.index_mu] = - (f_Xphi-1)/f_Xsigma**2* ( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )

        if self.is_sigma_hyper:
          grad[:,self.index_sigma] = grad_f_Xsigma/f_Xsigma * ( 1/f_Xsigma**2*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )**2    - 1 )

        if self.is_phi_hyper:
          grad[:,self.index_phi] =  grad_f_Xphi/f_Xsigma**2 * ( Xt - Xmu ) * ( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )

        return grad

    def hess_x_log_pdf(self, x, params=None):

        ##### Extract Variables #####################

        Xt = x[:,0]
        Xtp1 = x[:,1]

        if self.is_mu_hyper:
          Xmu = x[:,self.index_mu]
        else:
          Xmu = self.mu()

        if self.is_sigma_hyper:
          Xsigma = x[:,self.index_sigma]
          f_Xsigma = self.sigma.evaluate(Xsigma)
          grad_f_Xsigma = self.sigma.grad_x(Xsigma)
          hess_f_Xsigma = self.sigma.hess_x(Xsigma)
        else:
          f_Xsigma = self.sigma()

        if self.is_phi_hyper:
          Xphi = x[:,self.index_phi]
          f_Xphi = self.phi.evaluate(Xphi)
          grad_f_Xphi = self.phi.grad_x(Xphi)
          hess_f_Xphi = self.phi.hess_x(Xphi)
        else:
          f_Xphi = self.phi()


        ############################################

        hess = np.zeros( (x.shape[0], x.shape[1], x.shape[1]) )

        hess[:,0,0] = -(f_Xphi/f_Xsigma)**2
        hess[:,1,1] = -1/f_Xsigma**2
        hess[:,0,1] =  f_Xphi/f_Xsigma**2
        hess[:,1,0] = hess[:,0,1]


        if self.is_mu_hyper:
          hess[:,self.index_mu,self.index_mu] =  - ( (f_Xphi-1)/f_Xsigma )**2
          hess[:,0,self.index_mu] =  (f_Xphi*(f_Xphi-1))/f_Xsigma**2
          hess[:,self.index_mu,0] = hess[:,0,self.index_mu]
          hess[:,1,self.index_mu] =  -(f_Xphi-1)/f_Xsigma**2
          hess[:,self.index_mu,1] = hess[:,1,self.index_mu]
          if self.is_sigma_hyper:
            hess[:,self.index_mu , self.index_sigma] = \
               2*(f_Xphi-1)*grad_f_Xsigma/f_Xsigma**3*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )
            hess[:, self.index_sigma, self.index_mu] = hess[:,self.index_mu , self.index_sigma]
          if self.is_phi_hyper:
            hess[:,self.index_mu , self.index_phi] = \
              -grad_f_Xphi/f_Xsigma**2 *( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) ) + \
              (f_Xphi - 1)/f_Xsigma**2*grad_f_Xphi*(Xt - Xmu)
            hess[:,self.index_phi,self.index_mu] = hess[:,self.index_mu , self.index_phi]

        if self.is_sigma_hyper:
          hess[:,self.index_sigma,self.index_sigma] = \
            (hess_f_Xsigma/f_Xsigma - (grad_f_Xsigma/f_Xsigma)**2)*( 1/f_Xsigma**2*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )**2 - 1 ) - \
            2*grad_f_Xsigma**2/f_Xsigma**4*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )**2
          hess[:,0,self.index_sigma] = \
            -2*f_Xphi/f_Xsigma**3*grad_f_Xsigma*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )
          hess[:,self.index_sigma,0] = hess[:,0,self.index_sigma]
          hess[:,1,self.index_sigma] = \
            2*1/f_Xsigma**3*grad_f_Xsigma*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )
          hess[:,self.index_sigma,1] = hess[:,1,self.index_sigma]
          if self.is_phi_hyper:
            hess[:,self.index_sigma,self.index_phi]= \
              -2*grad_f_Xsigma*grad_f_Xphi/f_Xsigma**3*(Xt-Xmu)*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) )
            hess[:,self.index_phi,self.index_sigma] = hess[:,self.index_sigma,self.index_phi]

        if self.is_phi_hyper:
          hess[:,self.index_phi,self.index_phi] = \
            hess_f_Xphi/f_Xsigma**2*(Xt-Xmu)*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) ) - \
            (grad_f_Xphi/f_Xsigma)**2*(Xt - Xmu)**2
          hess[:,0,self.index_phi] = \
            grad_f_Xphi/f_Xsigma**2*( Xtp1 - Xmu - f_Xphi*(Xt - Xmu) ) \
            - f_Xphi * grad_f_Xphi / f_Xsigma**2*(Xt - Xmu)
          hess[:,self.index_phi,0] = hess[:,0,self.index_phi]
          hess[:,1,self.index_phi] = grad_f_Xphi/f_Xsigma**2*(Xt - Xmu)
          hess[:,self.index_phi,1] = hess[:,1,self.index_phi]

        return hess



##########################

# nsteps = 5
# # # #indexObs = np.array([1,4])
# # # #dataObs = np.random.randn(len(indexObs))
# which_hyper = (True, False, True)
# stocVol = StocVolHyper(nsteps, which_hyper=which_hyper)

# new_d = trim_density(stocVol, 3)

# new_d.dim
# stocVol.dim
# new_d.Xt
# new_d.tvec
# new_d.tobs
# new_d.dataObs

# stocVol.dim
# # # X = np.random.randn(5,stocVol.dim)
# # # stocVol.log_pdf(X)
# # # stocVol.grad_x_log_pdf(X)
# # # stocVol.hess_x_log_pdf(X)


# # # fun = stocVol.log_pdf
# # # grad_f = stocVol.grad_x_log_pdf
# # # hess_f = stocVol.hess_x_log_pdf
# # # X = np.random.randn(5,stocVol.dim)
# # # finiteDifference_gradient_X( fun , grad_f, X , delta = 1e-6, rtol=1e-6)
# # # finiteDifference_hessian_X( grad_f , hess_f, X , delta = 1e-6, rtol=1e-6)

# #Build random map
# dim =  2 + sum(which_hyper)
# order = 3
# btype = 'rbf'
# tmap = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
#         dim, order, span='full', btype='rbf')
# temp_coeff = tmap.get_coeffs()
# temp_coeff = temp_coeff + np.random.randn(len(temp_coeff))/10
# tmap.set_coeffs(temp_coeff)

# #Get Markov Component
# index_time = 1
# previous_tmap = tmap
# markov_comp = stocVol.get_MarkovFactor(index_time, previous_tmap)
# markov_comp.dim
# fun = markov_comp.log_pdf
# grad = markov_comp.grad_x_log_pdf
# hess = markov_comp.hess_x_log_pdf
# X = np.random.randn(10,markov_comp.dim)
# hess(X)

# finiteDifference_gradient_X( fun , grad, X , delta = 1e-6, rtol=1e-6)
# finiteDifference_hessian_X( grad , hess, X , delta = 1e-6, rtol=1e-6)


# X = np.random.randn(5,markov_comp.dim)
# fun(X)
# grad_f(X)
# #hess_f(X)

# X = np.random.randn(5,markov_comp.dim)
# finiteDifference_gradient_X( fun , grad_f, X , delta = 1e-6, rtol=1e-6)
# finiteDifference_hessian_X( grad_f , hess_f, X , delta = 1e-6, rtol=1e-6)


# fun = markov_comp.priorDynamic_t_composed_map.log_pdf
# grad = markov_comp.priorDynamic_t_composed_map.grad_x_log_pdf
# hess = markov_comp.priorDynamic_t_composed_map.hess_x_log_pdf
# X = np.random.randn(5,markov_comp.dim)
# fun(X)
# grad_f(X)
# #hess_f(X)

# X = np.random.randn(5,markov_comp.dim)
# finiteDifference_gradient_X( fun , grad_f, X , delta = 1e-6, rtol=1e-6)
# # finiteDifference_hessian_X( grad_f , hess_f, X , delta = 1e-6, rtol=1e-6)

# X = np.random.randn(1,markov_comp.dim)
# grad(X)
# FD.fd(fun, X, dx = 1e-6, params=None)


