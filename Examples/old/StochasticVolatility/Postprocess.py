#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import TransportMaps as TM

if not TM.MPI_SUPPORT or TM.get_mpi_rank() == 0:

    import sys, getopt
    import os.path
    import dill
    import numpy as np
    import numpy.random as npr
    import matplotlib.pyplot as plt
    import scipy.stats as scistat
    # import seaborn as sns
    # sns.set(color_codes=True)
    # import pandas as pd
    import TransportMaps.Diagnostics as DIAG
    import TransportMaps.Densities as DENS

    import StocVolHyperDensitiesDurbin as SVHDENS

    import MapGenerationHyper as MGH

    def usage():
        print("StochasticVolatility-DirectMap-Composition-Postprocess.py -i <file_name> [--nprocs=<num_processors> --trim=<trim> --no-aligned --no-random --no-var-diag --no-smooth --no-filt --output=<file_name> --nmc=<nmc> --nprocs=<num_processors> --mpi-lag=<lag>]")

    def full_usage():
        usage()

    argv = sys.argv[1:]
    IN_FNAME = None
    NMC = 5000
    trim = None
    OUT_FNAME = None
    NPROCS = None
    MPI_LAG = 0.0
    DO_ALIGNED = True
    DO_RANDOM = True
    DO_VAR_DIAG = True
    DO_SMOOTH = True
    DO_FILT = True
    # DO_MARG_TAR = True
    try:
        opts, args = getopt.getopt(argv,"hi:",["input=", "nmc=", "trim=",
                                               "nprocs=", "mpi-lag=",
                                               "no-aligned", "no-random", "no-var-diag",
                                               "no-smooth", "no-filt","output="]) #, "no-marg-tar"])
    except getopt.GetoptError:
        full_usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            full_usage()
            sys.exit()
        elif opt in ("-i", "--input"):
            IN_FNAME = arg
        elif opt in "--nmc":
            NMC = int(arg)
        elif opt in "--trim":
            trim = int(arg)
        elif opt in "--nmc":
            NMC = int(arg)
        elif opt in "--no-aligned":
            DO_ALIGNED = False
        elif opt in "--no-random":
            DO_RANDOM = False
        elif opt in "--no-var-diag":
            DO_VAR_DIAG = False
        elif opt in "--no-smooth":
            DO_SMOOTH = False
        elif opt in "--no-filt":
            DO_FILT = False
        elif opt in ("--output"):
            OUT_FNAME = arg
        elif opt in ("--nprocs="):
            NPROCS = int(arg)
        elif opt in ("--mpi-lag="):
            MPI_LAG = float(arg)
        # elif opt in "--no-marg-tar":
        #     DO_MARG_TAR = False
    if None in [IN_FNAME]:
        full_usage()
        sys.exit(3)

    # Load data
    with open(IN_FNAME, 'rb') as in_stream:
        data = dill.load(in_stream)

    # Restore data
    tm_approx_comp_list = data['tm_approx_comp_list']
    target_density = data['target_density']
    tm_approx_hyper_list = data['tm_approx_hyper_list']
    regression_order_list = data['regression_order_list']
    map_order_list = data['map_order_list']

    #Trim == effective number of timesteps considered
    if  len(tm_approx_comp_list) == 0:
        raise ValueError("No maps have been computed")
    nsteps_computed = len(tm_approx_comp_list)+1
    if trim is None: #Check that the desired maps have been computed
        if target_density.nsteps > nsteps_computed:
            #In this case, reduce the target density and compute what is possible
            trim = nsteps_computed
            print("The composition of maps is NOT complete. We will trim the target density accordingly", RuntimeWarning)


    if trim is not None:
        if trim > nsteps_computed:
            warnings.warn("You are asking for more steps that you have actually computed. We will trim the target density accordingly", RuntimeWarning)
            trim = nsteps_computed
            #raise ValueError("You only computed %d time steps" %(nsteps_computed))
        #Number of maps to consider
        n_maps2consider = trim  - 1
        #Trim maps
        tm_approx_comp_list = tm_approx_comp_list[:n_maps2consider]
        tm_approx_hyper_list = tm_approx_hyper_list[:n_maps2consider+1]
        regression_order_list = regression_order_list[:n_maps2consider]
        map_order_list = map_order_list[:n_maps2consider]
        #Trim densities
        target_density = SVHDENS.trim_density(target_density, trim)

    #Retrieve remaining variables
    dim = target_density.dim
    nsteps = target_density.nsteps
    nhyper =sum(target_density.which_hyper)
    index_X0 = target_density.index_X0
    base_density = DENS.StandardNormalDensity(dim)

    #Store data if needed
    if OUT_FNAME is not None:
        data2save = {'tm_approx_comp_list':tm_approx_comp_list,
                     'target_density':target_density,
                     'tm_approx_hyper_list':tm_approx_hyper_list,
                     'regression_order_list':regression_order_list,
                     'map_order_list':map_order_list,
                     'base_density':base_density}

    # Construct T
    tm_comp = MGH.compose_Markovian_tm(tm_approx_comp_list, nhyper )

    # Construct T^\sharp \pi_{tar}
    pull_tar = DENS.PullBackTransportMapDensity(tm_comp, target_density)

    # Construct T_\sharp \rho
    push_base = DENS.PushForwardTransportMapDensity(tm_comp, base_density)

    #--0--###### Start pool of mpi processes
    mpi_pool = None
    if NPROCS > 1:
        import_set = set()
        import_set.add( ('numpy', 'np') )
        mpi_pool = TM.get_mpi_pool()
        mpi_pool.start(NPROCS, import_set)
    try:
        #--1--###### Plot: smoothing/posterior marginals timesteps #############
        print("\nPlotting smoothing/posterior marginals \n")
        tar_samp_smooth = push_base.rvs(NMC, mpi_pool=mpi_pool)
        Xt_samp_smooth = tar_samp_smooth[:,index_X0:]
        tvec = target_density.tvec
        meanXt_smooth = np.mean(Xt_samp_smooth, axis=0)
        q5_smooth = np.percentile(Xt_samp_smooth, 5, axis=0)
        q25_smooth = np.percentile(Xt_samp_smooth, 25, axis=0)
        q40_smooth = np.percentile(Xt_samp_smooth, 40, axis=0)
        q60_smooth = np.percentile(Xt_samp_smooth, 60, axis=0)
        q75_smooth = np.percentile(Xt_samp_smooth, 75, axis=0)
        q95_smooth = np.percentile(Xt_samp_smooth, 95, axis=0)
        fig1 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{X_t|Y_{0:N}}$",
                                          title="Posterior/Smoothing marginals")
        ax1 = fig1.gca()
        #ax1.set_aspect('equal')
        #ax.set_ylim(-3,3)
        ax1.set_xlim(0,tvec[-1])
        #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
        #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
        #ax.plot(target_density.tobs, target_density.dataObs, 'or')
        ax1.plot(tvec, meanXt_smooth, '-r')
        ax1.fill_between(tvec, q40_smooth, q60_smooth, facecolor='r', alpha=0.35, linewidth = 0.0)
        ax1.fill_between(tvec, q25_smooth, q75_smooth, facecolor='r', alpha=0.25, linewidth = 0.0)
        ax1.fill_between(tvec, q5_smooth, q95_smooth, facecolor='r', alpha=0.15, linewidth = 0.0)
        #plt.legend(loc='best')
        plt.show(False)

        if OUT_FNAME is not None:
            data2save.update({'push_base':push_base,
                              'tar_samp_smooth':tar_samp_smooth,
                              'fig:smoothing_marginals_timesteps':fig1})

        #--2--###### Plot: filtering marginals timesteps #############
        print("Plotting filtering marginals \n")
        Xt_samp_filt = np.zeros((NMC,nsteps))
        dim_Markov = 2+nhyper
        nXd = DENS.StandardNormalDensity(dim_Markov)
        nXm1d = DENS.StandardNormalDensity(dim_Markov-1)
        nXd_samp = nXd.rvs(NMC)
        Xt_samp_filt[:,0] = TM.mpi_eval("evaluate", tm_approx_comp_list[0].approx_list[-1],
                                        nXd_samp, mpi_pool=mpi_pool).flatten()
        for ii,tm in enumerate(tm_approx_comp_list):
            nXm1d_samp = nXm1d.rvs(NMC)
            Xt_samp_filt[:,ii+1] = TM.mpi_eval("evaluate", tm.approx_list[-2],
                                               nXm1d_samp, mpi_pool=mpi_pool).flatten()

        meanXt_filt = np.mean(Xt_samp_filt, axis=0)
        q5_filt = np.percentile(Xt_samp_filt, 5, axis=0)
        q25_filt = np.percentile(Xt_samp_filt, 25, axis=0)
        q40_filt = np.percentile(Xt_samp_filt, 40, axis=0)
        q60_filt = np.percentile(Xt_samp_filt, 60, axis=0)
        q75_filt = np.percentile(Xt_samp_filt, 75, axis=0)
        q95_filt = np.percentile(Xt_samp_filt, 95, axis=0)
        fig2 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{X_t|Y_{0:t}}$",
                                          title='Filtering marginals')
        ax2 = fig2.gca()
        #ax2.set_aspect('equal')
        #ax.set_ylim(-3,3)
        ax2.set_xlim(0,tvec[-1])
        #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
        #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
        #ax.plot(target_density.tobs, target_density.dataObs, 'or')
        ax2.plot(tvec, meanXt_filt, '-b')
        ax2.fill_between(tvec, q40_filt, q60_filt, facecolor='b', alpha=0.35, linewidth = 0.0)
        ax2.fill_between(tvec, q25_filt, q75_filt, facecolor='b', alpha=0.25, linewidth = 0.0)
        ax2.fill_between(tvec, q5_filt, q95_filt, facecolor='b', alpha=0.15, linewidth = 0.0)
        #plt.legend(loc='best')
        plt.show(False)

        if OUT_FNAME is not None:
            data2save.update({'Xt_samp_filt':Xt_samp_filt,
                              'fig:filtering_marginals_timesteps':fig2})

        #--3--###### Plot: smoothing+filtering marginals timesteps #############
        print("Plotting smoothing + filtering marginals \n")
        fig3 = DIAG.nicePlot([],[], xlabel="time", ylabel=" ",
                                          title='Smoothing and filtering marginals')
        ax3 = fig3.gca()
        #ax3.set_aspect('equal')
        #ax.set_ylim(-3,3)
        ax3.set_xlim(0,tvec[-1])
        #if target_density.Xt is not None:  ######### FIX THIS LINE ###########
        #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
        #ax.plot(target_density.tobs, target_density.dataObs, 'or')
        ax3.plot(tvec, meanXt_filt, '-b', label = "Filtering")
        #ax3.fill_between(tvec, q40_filt, q60_filt, facecolor='b', alpha=0.25, linewidth = 0.0)
        ax3.fill_between(tvec, q25_filt, q75_filt, facecolor='b', alpha=0.15, linewidth = 0.0)
        ax3.fill_between(tvec, q5_filt, q95_filt, facecolor='b', alpha=0.05, linewidth = 0.0)

        ax3.plot(tvec, meanXt_smooth, '-r', label = "Smoothing")
        #ax3.fill_between(tvec, q40_smooth, q60_smooth, facecolor='r', alpha=0.25, linewidth = 0.0)
        ax3.fill_between(tvec, q25_smooth, q75_smooth, facecolor='r', alpha=0.15, linewidth = 0.0)
        ax3.fill_between(tvec, q5_smooth, q95_smooth, facecolor='r', alpha=0.05, linewidth = 0.0)
        plt.legend(loc='best')
        plt.show(False)

        if OUT_FNAME is not None:
            data2save.update({'fig:smoothing+filtering_marginals_timesteps':fig3})

        #--4--###### Plot: filtering marginals hyper-parameters #############
        if nhyper>0:
            print("Plotting hyper-parameters filtering marginals \n")
            hyper_samp_filt = np.zeros((NMC,nhyper,nsteps))
            nHd = DENS.StandardNormalDensity(nhyper)
            for ii,tm in enumerate(tm_approx_hyper_list[1:]):
                nHd_samp = nHd.rvs(NMC)
                hyper_samp_filt[:,:,ii+1] = tm.evaluate(nHd_samp)
            hyper_samp_filt[:,:,0] = hyper_samp_filt[:,:,1].copy() #We assimilate the two steps at the same time

            if OUT_FNAME is not None:
                data2save.update({'hyper_samp_filt':hyper_samp_filt})

            if target_density.is_mu_hyper:
                #Plot filtering marginals for mu
                index_mu = target_density.index_mu
                mu_samp_filt = hyper_samp_filt[:,index_mu,:]
                meanMu_filt = np.mean(mu_samp_filt, axis=0)
                q5mu_filt = np.percentile(mu_samp_filt, 5, axis=0)
                q25mu_filt = np.percentile(mu_samp_filt, 25, axis=0)
                q40mu_filt = np.percentile(mu_samp_filt, 40, axis=0)
                q60mu_filt = np.percentile(mu_samp_filt, 60, axis=0)
                q75mu_filt = np.percentile(mu_samp_filt, 75, axis=0)
                q95mu_filt = np.percentile(mu_samp_filt, 95, axis=0)

                fig4 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{\mu|Y_{0:t}}$",
                                          title='Filtering marginals of $\mu$')
                ax4 = fig4.gca()
                #ax4.set_aspect('equal')
                #ax.set_ylim(-3,3)
                ax4.set_xlim(0,tvec[-1])
                #If target_density.Xt is not None:  ######### FIX THIS LINE ###########
                #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
                #ax.plot(target_density.tobs, target_density.dataObs, 'or')
                ax4.plot(tvec, meanMu_filt, '-k')
                ax4.fill_between(tvec, q40mu_filt, q60mu_filt, facecolor='grey', alpha=0.35, linewidth = 0.0)
                ax4.fill_between(tvec, q25mu_filt, q75mu_filt, facecolor='grey', alpha=0.25, linewidth = 0.0)
                ax4.fill_between(tvec, q5mu_filt, q95mu_filt, facecolor='grey', alpha=0.15, linewidth = 0.0)
                #plt.legend(loc='best')
                plt.show(False)

                if OUT_FNAME is not None:
                    data2save.update({'fig:filtering_marginals_mu':fig4})

            if target_density.is_sigma_hyper:
                #Plot filtering marginals for sigma
                index_sigma = target_density.index_sigma
                sigma_samp_filt = target_density.sigma.evaluate(  hyper_samp_filt[:,index_sigma,:] )
                meanSigma_filt = np.mean(sigma_samp_filt, axis=0)
                q5sigma_filt = np.percentile(sigma_samp_filt, 5, axis=0)
                q25sigma_filt = np.percentile(sigma_samp_filt, 25, axis=0)
                q40sigma_filt = np.percentile(sigma_samp_filt, 40, axis=0)
                q60sigma_filt = np.percentile(sigma_samp_filt, 60, axis=0)
                q75sigma_filt = np.percentile(sigma_samp_filt, 75, axis=0)
                q95sigma_filt = np.percentile(sigma_samp_filt, 95, axis=0)

                fig5 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{\sigma|Y_{0:t}}$",
                                          title='Filtering marginals of $\sigma$')
                ax5 = fig5.gca()
                #ax5.set_aspect('equal')
                #ax.set_ylim(-3,3)
                ax5.set_xlim(0,tvec[-1])
                #If target_density.Xt is not None:  ######### FIX THIS LINE ###########
                #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
                #ax.plot(target_density.tobs, target_density.dataObs, 'or')
                ax5.plot(tvec, meanSigma_filt, '-k')
                ax5.fill_between(tvec, q40sigma_filt, q60sigma_filt, facecolor='grey', alpha=0.35, linewidth = 0.0)
                ax5.fill_between(tvec, q25sigma_filt, q75sigma_filt, facecolor='grey', alpha=0.25, linewidth = 0.0)
                ax5.fill_between(tvec, q5sigma_filt, q95sigma_filt, facecolor='grey', alpha=0.15, linewidth = 0.0)
                #plt.legend(loc='best')
                plt.show(False)

                if OUT_FNAME is not None:
                    data2save.update({'fig:filtering_marginals_sigma':fig5})

            if target_density.is_phi_hyper:
                #Plot filtering marginals for sigma
                index_phi = target_density.index_phi
                phi_samp_filt = target_density.phi.evaluate( hyper_samp_filt[:,index_phi,:] )
                meanPhi_filt = np.mean(phi_samp_filt, axis=0)
                q5phi_filt = np.percentile(phi_samp_filt, 5, axis=0)
                q25phi_filt = np.percentile(phi_samp_filt, 25, axis=0)
                q40phi_filt = np.percentile(phi_samp_filt, 40, axis=0)
                q60phi_filt = np.percentile(phi_samp_filt, 60, axis=0)
                q75phi_filt = np.percentile(phi_samp_filt, 75, axis=0)
                q95phi_filt = np.percentile(phi_samp_filt, 95, axis=0)

                fig6 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{\phi|Y_{0:t}}$",
                                          title='Filtering marginals of $\phi$')
                ax6 = fig6.gca()
                #ax6.set_aspect('equal')
                #ax.set_ylim(-3,3)
                ax6.set_xlim(0,tvec[-1])
                #If target_density.Xt is not None:  ######### FIX THIS LINE ###########
                #    ax.plot(tvec, target_density.Xt, '--k', label="truth")
                #ax.plot(target_density.tobs, target_density.dataObs, 'or')
                ax6.plot(tvec, meanPhi_filt, '-k')
                ax6.fill_between(tvec, q40phi_filt, q60phi_filt, facecolor='grey', alpha=0.35, linewidth = 0.0)
                ax6.fill_between(tvec, q25phi_filt, q75phi_filt, facecolor='grey', alpha=0.25, linewidth = 0.0)
                ax6.fill_between(tvec, q5phi_filt, q95phi_filt, facecolor='grey', alpha=0.15, linewidth = 0.0)
                #plt.legend(loc='best')
                plt.show(False)

                if OUT_FNAME is not None:
                    data2save.update({'fig:filtering_marginals_phi':fig6})


        #--4--###### Plot: smoothing/posterior marginals hyper-parameters #############
        if nhyper>0:
            print("Plotting hyper-parameters smoothing marginals \n")

            if target_density.is_mu_hyper:
                mu_samp_smooth = tar_samp_smooth[:,index_mu]
                mu_min = np.min(mu_samp_smooth)
                mu_min = mu_min - .1*np.abs(mu_min)
                mu_max = np.max(mu_samp_smooth)
                mu_max = mu_max + .1*np.abs(mu_max)
                mu_kde = scistat.gaussian_kde(mu_samp_smooth)
                mu_xx = np.linspace(mu_min, mu_max, 10000)
                pdf_mu_xx = mu_kde(mu_xx)
                fig7 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\mu|Y_{0:N}}$",
                                          title='Posterior/smoothing marginals of $\mu$')
                ax7 = fig7.gca()
                ax7.set_xlim(mu_min,mu_max)
                ax7.plot(mu_xx, pdf_mu_xx, '-k',linewidth=1.0)
                plt.show(False)

                if OUT_FNAME is not None:
                    data2save.update({'fig:smoothing_marginals_mu':fig7})

            if target_density.is_sigma_hyper:
                Xsigma_samp_smooth =  tar_samp_smooth[:,index_sigma]
                Xsigma_kde = scistat.gaussian_kde(Xsigma_samp_smooth) #Do the kde on the reference variable and then push forward through F_sigma.
                Xsigma_min = np.min(Xsigma_samp_smooth)
                Xsigma_min = Xsigma_min - .1*np.abs(Xsigma_min)
                Xsigma_max = np.max(Xsigma_samp_smooth)
                Xsigma_max = Xsigma_max + .1*np.abs(Xsigma_max)
                Xsigma_xx = np.linspace(Xsigma_min, Xsigma_max, 10000)
                FXsigma_xx = target_density.sigma.evaluate( Xsigma_xx )
                grad_FXsigma_xx = target_density.sigma.grad_x( Xsigma_xx )
                pdf_sigma_FXsigma = Xsigma_kde(Xsigma_xx)/grad_FXsigma_xx
                fig8 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\sigma|Y_{0:N}}$",
                                          title='Posterior/smoothing marginals of $\sigma$')
                ax8 = fig8.gca()
                sigma_max = np.max(FXsigma_xx)
                ax8.set_xlim(0.,sigma_max)
                ax8.plot(FXsigma_xx, pdf_sigma_FXsigma, '-k',linewidth=1.0)
                plt.show(False)

                if OUT_FNAME is not None:
                    data2save.update({'fig:smoothing_marginals_sigma':fig8})

            if target_density.is_phi_hyper:
                Xphi_samp_smooth =  tar_samp_smooth[:,index_phi]
                Xphi_kde = scistat.gaussian_kde(Xphi_samp_smooth) #Do the kde on the reference variable and then push forward through F_phi.
                Xphi_min = np.min(Xphi_samp_smooth)
                Xphi_min = Xphi_min - .1*np.abs(Xphi_min)
                Xphi_max = np.max(Xphi_samp_smooth)
                Xphi_max = Xphi_max + .1*np.abs(Xphi_max)
                Xphi_xx = np.linspace(Xphi_min, Xphi_max, 10000)
                FXphi_xx = target_density.phi.evaluate( Xphi_xx )
                grad_FXphi_xx = target_density.phi.grad_x( Xphi_xx )
                pdf_phi_FXphi = Xphi_kde(Xphi_xx)/grad_FXphi_xx
                fig9 = DIAG.nicePlot([],[], xlabel="", ylabel="$\pi_{\phi|Y_{0:N}}$",
                                          title='Posterior/smoothing marginals of $\phi$')
                ax9 = fig9.gca()
                phi_min = np.min(FXphi_xx)
                ax9.set_xlim(phi_min,1.)
                ax9.plot(FXphi_xx, pdf_phi_FXphi, '-k',linewidth=1.0)
                plt.show(False)
                #phi_samp_smooth = target_density.phi.evaluate( Xphi_samp_smooth )

                if OUT_FNAME is not None:
                    data2save.update({'fig:smoothing_marginals_phi':fig9})


        #--5--###### Plot: order maps in the composition #############
        fig10 = DIAG.nicePlot([],[], xlabel="iteration", ylabel="order map",
                              title='Order maps in the composition')
        ax10 = fig10.gca()
        ax10.set_xlim(0,len(map_order_list))
        ax10.set_ylim(0,1+np.max(map_order_list))
        ax10.plot(np.arange(len(map_order_list)), map_order_list, '--ko',linewidth=1.0)
        plt.show(False)

        if OUT_FNAME is not None:
            data2save.update({'fig:order_maps_composition':fig10})

        #--6--###### Plot: regression order hyper-maps #############
        fig11 = DIAG.nicePlot([],[], xlabel="iteration", ylabel="order map",
                                          title='Order regression hyper-maps')
        ax11 = fig11.gca()
        ax11.set_xlim(0,len(regression_order_list))
        ax11.set_ylim(0,1+np.max(regression_order_list))
        ax11.plot(np.arange(len(regression_order_list)), regression_order_list, '--ko',linewidth=1.0)
        plt.show(False)

        if OUT_FNAME is not None:
            data2save.update({'fig:order_regression_hypermaps':fig11})

        #--7--###### Plot: posterior data predictive #############
        #Xt_samp_smooth
        print("Plotting posterior predictive \n")
        noise_samples = np.random.randn(Xt_samp_smooth.shape[0],Xt_samp_smooth.shape[1])
        Ysmooth_samples = noise_samples*np.exp(.5*Xt_samp_smooth)

        q5y_smooth = np.percentile(Ysmooth_samples, 5, axis=0)
        q25y_smooth = np.percentile(Ysmooth_samples, 25, axis=0)
        q40y_smooth = np.percentile(Ysmooth_samples, 40, axis=0)
        q60y_smooth = np.percentile(Ysmooth_samples, 60, axis=0)
        q75y_smooth = np.percentile(Ysmooth_samples, 75, axis=0)
        q95y_smooth = np.percentile(Ysmooth_samples, 95, axis=0)
        fig12 = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{Y_y|Y_{0:N}}$",
                                          title="Data Posterior/Smoothing marginals (posterior predictive)")
        ax12 = fig12.gca()
        #ax12.set_aspect('equal')
        #ax.set_ylim(-3,3)
        ax12.set_xlim(0,tvec[-1])
        ax12.plot(target_density.tobs,target_density.dataObs,'.k', label="data")
        ax12.fill_between(tvec, q40y_smooth, q60y_smooth, facecolor='r', alpha=0.35, linewidth = 0.0)
        ax12.fill_between(tvec, q25y_smooth, q75y_smooth, facecolor='r', alpha=0.25, linewidth = 0.0)
        ax12.fill_between(tvec, q5y_smooth, q95y_smooth, facecolor='r', alpha=0.15, linewidth = 0.0)
        plt.legend(loc='best')
        plt.show(False)

        if OUT_FNAME is not None:
            data2save.update({'fig:posterior_data_predictive':fig12})

        #--8--###### Plot: marginals posterior/smoothing distribution #############
        print("Plotting posterior/smoothing marginals \n")
        fig13 = DIAG.plotAlignedMarginals(tar_samp_smooth, dimensions_vec = list(range( min(dim,15) )),
                                          mpi_pool=mpi_pool)

        if OUT_FNAME is not None:
            data2save.update({'fig:smoothing_marginals_triangularPlot':fig13})


    #     #--9--###### Plot: intensity coefficients map #############
    #     print("Plotting intensity coefficients map \n")
    #     fig14 = DIAG.plotCoefficientsMap(tm_comp)

    #     if OUT_FNAME is not None:
    #         data2save.update({'fig:intensity_coefficients_map':fig14})

    #     if DO_RANDOM:
    #         print("Plotting random conditionals")
    #         fig14 = DIAG.plotRandomConditionals(target_density, num_conditionalsXax=7, numPointsXax = 15, nprocs=NPROCS)

    #         if OUT_FNAME is not None:
    #             data2save.update({'fig:random_conditionals':fig14})

    #     if DO_ALIGNED:
    #         # dvec = list(npr.choice(np.arange(target_density.dim), min(target_density.dim,8),
    #         #                        replace=False))
    #         dvec = list(range( min(target_density.dim,10) ))
    #         print("Plotting aligned conditionals: %s" % dvec)
    #         fig15 = DIAG.plotAlignedConditionals(target_density, numPointsXax = 15, range_vec=[-5,5],
    #                                      dimensions_vec=dvec, nprocs=NPROCS,
    #                                      title='Conditionals pullback nonlinear map')

    #         if OUT_FNAME is not None:
    #             data2save.update({'fig:aligned_conditionals':fig15})

        if DO_VAR_DIAG:
            print("Calculating variance diagnostic")
            var_diag = DIAG.variance_approx_kl(base_density, pull_tar, qtype=0, qparams=NMC,
                                               mpi_pool=mpi_pool)

            if OUT_FNAME is not None:
                data2save.update({'variance_diagnostic':var_diag})
    finally:
        if mpi_pool is not None:
            mpi_pool.stop()
            mpi_pool = None

    #Save data to file!
    if OUT_FNAME is not None:
        with open(OUT_FNAME, 'wb') as out_stream:
            dill.dump(data2save, out_stream)

#     # Load data
#     with open(IN_FNAME, 'rb') as in_stream:
#         data = dill.load(in_stream)

