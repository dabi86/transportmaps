import numpy as np
import numpy.random as npr
import TransportMaps as TM
import StocVolHyperDensitiesDurbin as SVDURB
import TransportMaps.Densities.Examples.StochasticVolatility as SV

mu = -.5
phi = .95
sigma = 0.25
nsteps = 5
obsIdxs = np.arange(nsteps)

dataObs, Xt = SV.generate_data(nsteps, mu, sigma, phi)
dataObs = [ dataObs[i] if i in obsIdxs else None for i in range(nsteps) ]

for is_mu_hyper in range(2):
    for is_sigma_hyper in range(2):
        for is_phi_hyper in range(2):
        
            # Generate new version of the density
            dnew = SV.StocVolHyperDensity(
                is_mu_hyper=is_mu_hyper, is_sigma_hyper=is_sigma_hyper,
                is_phi_hyper=is_phi_hyper, 
                mu=mu, sigma=sigma, phi=phi, 
                mu_sigma=1.,
                sigma_alpha=1., sigma_beta=10.,
                phi_mean=3., phi_std=1.)
            for n in range(nsteps):
                dnew.assimilate(y=dataObs[n], Xt=Xt[n])

            # Old version of the density
            dobs = np.asarray([dataObs[i] for i in range(nsteps) if i in obsIdxs])
            dold = SVDURB.StocVolHyper(
                nsteps, obsIdxs, dobs,
                which_hyper=(is_mu_hyper, is_sigma_hyper, is_phi_hyper),
                mu=mu, sigma=sigma, phi=phi, mu_sigma=1.,
                sigma_alpha=1., sigma_beta=10., phi_mean=3., phi_std=1., Xt=None,
                mu_data = -.5, sigma_data = .25, phi_data = .95 )

            ##########
            # CHECKS #
            ##########
            # Dimension
            if dnew.dim != dold.dim:
                raise ValueError(
                    "Dimension mismatch: %d, %d, %d" % (
                        is_mu_hyper, is_sigma_hyper, is_phi_hyper))
            # log_pdf
            x = npr.randn(1,dnew.dim)
            newval = dnew.log_pdf(x)
            if dnew.pi_hyper is not None:
                newval -= dnew.pi_hyper.mean_log_pdf()
            oldval = dold.log_pdf(x).astype(float)
            if not np.allclose(newval, oldval):
                raise ValueError(
                    "log_pdf mismatch: %d, %d, %d" % (
                        is_mu_hyper, is_sigma_hyper, is_phi_hyper))
            # grad_x_log_pdf
            newval = dnew.grad_x_log_pdf(x)
            oldval = dold.grad_x_log_pdf(x).astype(float)
            if not np.allclose(newval, oldval):
                raise ValueError(
                    "grad_x_log_pdf mismatch: %d, %d, %d" % (
                        is_mu_hyper, is_sigma_hyper, is_phi_hyper))
            # hess_x_log_pdf
            newval = dnew.hess_x_log_pdf(x)
            oldval = dold.hess_x_log_pdf(x).astype(float)
            if not np.allclose(newval, oldval):
                raise ValueError(
                    "hess_x_log_pdf mismatch: %d, %d, %d" % (
                        is_mu_hyper, is_sigma_hyper, is_phi_hyper))
            ###########################
            # Check Markov components #
            ###########################
            # Define maps
            hyper_map=None
            if dnew.hyper_dim > 0:
                hyper_map = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
                    dnew.hyper_dim, 1)
                hyper_map.set_coeffs( npr.randn(hyper_map.get_n_coeffs()) )
            tmp = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(
                dnew.hyper_dim+dnew.state_dim, 1)
            comp_map = Maps.TransportMap(tmp.active_vars[dnew.hyper_dim:],
                                       tmp.approx_list[dnew.hyper_dim:])
            comp_map.set_coeffs( npr.randn(comp_map.get_n_coeffs()) )
            # Define permutation
            dcomp = dnew.hyper_dim + 2*dnew.state_dim
            Q = np.eye(dcomp)
            Q[-2,-2] = Q[-1,-1] = 0
            Q[-1,-2] = Q[-2,-1] = 1.
            perm = Maps.LinearTransportMap(np.zeros(dcomp), Q)
            for i in range(nsteps-1):
                mcdnew = dnew.get_MarkovComponent(
                    i, n=1, comp_map=comp_map, hyper_map=hyper_map)
                mcdold = dold.get_MarkovFactor(
                    i, component_map=comp_map.approx_list[0],
                    hyper_map=hyper_map)
                # Dimension
                if mcdnew.dim != mcdold.dim:
                    raise ValueError(
                        "Comp %d dimension mismatch: %d, %d, %d" % (
                            i, is_mu_hyper, is_sigma_hyper, is_phi_hyper))
                # log_pdf
                x = npr.randn(1,mcdnew.dim)
                px = perm(x)
                newval = mcdnew.log_pdf( px )
                if mcdnew.pi_hyper is not None:
                    newval -= mcdnew.pi_hyper.mean_log_pdf()
                oldval = mcdold.log_pdf(x).astype(float)
                if not np.allclose(newval, oldval):
                    raise ValueError(
                        "Comp %d log_pdf mismatch: %d, %d, %d" % (
                            i, is_mu_hyper, is_sigma_hyper, is_phi_hyper))
                # grad_x_log_pdf
                newval = perm( mcdnew.grad_x_log_pdf(px) )
                oldval = mcdold.grad_x_log_pdf(x).astype(float)
                if not np.allclose(newval, oldval):
                    raise ValueError(
                        "Comp %d grad_x_log_pdf mismatch: %d, %d, %d" % (
                            i, is_mu_hyper, is_sigma_hyper, is_phi_hyper))
                # hess_x_log_pdf
                tmp = mcdnew.hess_x_log_pdf(px)
                newval = tmp.copy()
                newval[:,-1,-1] = tmp[:,-2,-2]
                newval[:,-2,-2] = tmp[:,-1,-1]
                newval[:,-1,:-2] = tmp[:,-2,:-2]
                newval[:,-2,:-2] = tmp[:,-1,:-2]
                newval[:,:-2,-1] = tmp[:,:-2,-2]
                newval[:,:-2,-2] = tmp[:,:-2,-1]
                oldval = mcdold.hess_x_log_pdf(x).astype(float)
                if not np.allclose(newval, oldval):
                    raise ValueError(
                        "Comp %d hess_x_log_pdf mismatch: %d, %d, %d" % (
                            i, is_mu_hyper, is_sigma_hyper, is_phi_hyper))