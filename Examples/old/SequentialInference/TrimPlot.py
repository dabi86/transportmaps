#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import TransportMaps as TM
import sys, getopt
import os.path
import dill
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import scipy.stats as scistat
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS
import StocVolHyperDensitiesDurbin as SVHDENS
import MapGenerationHyperDurbin as MGH

fname = "data/SVH-DirectMap-Composition-n30-phi0.95-mu-0.5-full-rbf-npoints6000-eps1e-2-reg1e-3-analysis-trim-"

var_diag_list = []
for i in range(2, 30):
    # Load data
    IN_FNAME = fname + str(i) + ".dill"
    with open(IN_FNAME, 'rb') as in_stream:
        data = dill.load(in_stream)
    plt.show(False)
    var_diag_list.append(data['variance_diagnostic'])

plt.figure()
plt.semilogy(var_diag_list)
plt.show(False)