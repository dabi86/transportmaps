import dill
import matplotlib.pyplot as plt

with open('data/svd.dat', 'rb') as in_stream:
    l = dill.load(in_stream)

ln0 = 0
split_list = []
for s in l:
    if len(s) == ln0:
        split_list[-1].append(s)
    else:
        split_list.append([s])
        ln0 = len(s)

for sub_list in split_list:
    plt.figure()
    for s in sub_list:
        plt.semilogy(s)

plt.show(False)