#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
# import seaborn as sns
# sns.set(color_codes=True)
# import pandas as pd
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS

import StocVolHyperDensities as SVHDENS

def usage():
    print("StochasticVolatility-InverseMap-Postprocess.py -i <file_name> [--nprocs=<num_processors> --no-aligned --no-random --no-var-diag --no-smooth]")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
NMC = 10000
NPROCS = 1
DO_ALIGNED = True
DO_RANDOM = True
DO_VAR_DIAG = True
DO_SMOOTH = True
# DO_MARG_TAR = True
try:
    opts, args = getopt.getopt(argv,"hi:",["input=", "nprocs=", "nmc=",
                                           "no-aligned", "no-random", "no-var-diag",
                                           "no-smooth"]) #, "no-marg-tar"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-i", "--input"):
        IN_FNAME = arg
    elif opt in "--nprocs":
        NPROCS = int(arg)
    elif opt in "--nmc":
        NMC = int(arg)
    elif opt in "--no-aligned":
        DO_ALIGNED = False
    elif opt in "--no-random":
        DO_RANDOM = False
    elif opt in "--no-var-diag":
        DO_VAR_DIAG = False
    elif opt in "--no-smooth":
        DO_SMOOTH = False
    # elif opt in "--no-marg-tar":
    #     DO_MARG_TAR = False
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME, 'rb') as in_stream:
    data = dill.load(in_stream)

# Resetore data
tm = data['tm_approx']
laplace_tm = data['laplace_tm']
target_density = data['target_density']
base_density = DENS.StandardNormalDensity(target_density.dim)

# Construct T_\sharp L^\sharp \pi_{tar}
laplace_target_density = DENS.PullBackTransportMapDensity(laplace_tm, target_density)
pull_tar = DENS.PushForwardTransportMapDensity(tm, laplace_target_density)

# Construct L_\sharp T^\sharp \rho
notlap_push_base = DENS.PullBackTransportMapDensity(tm, base_density)
push_base = DENS.PushForwardTransportMapDensity(laplace_tm, notlap_push_base)

# Diagnostics
if DO_ALIGNED:
    dvec = list(npr.choice(np.arange(target_density.dim), min(target_density.dim,8),
                           replace=False))
    # Run diagnostics
    print("Aligned conditionals: %s" % dvec)
    DIAG.plotAlignedConditionals(pull_tar, numPointsXax = 40, range_vec=[-8,8],
                                 dimensions_vec=dvec, nprocs=NPROCS,
                                 title='Conditionals pullback nonlinear map')

if DO_RANDOM:
    print("Random conditionals")
    DIAG.plotRandomConditionals(pull_tar, num_conditionalsXax=6, numPointsXax = 10,
                                nprocs=NPROCS)

if DO_VAR_DIAG:
    var_diag = DIAG.variance_approx_kl(base_density, pull_tar, qtype=0, qparams=NMC,
                                       nprocs=(1,NPROCS))
    print("Variance Diagnostic: %e" % var_diag)

# if DO_MARG_TAR:
#     tar_samp = push_base.rvs(NMC)
#     tar_samp_marg = tar_samp[:,40:45]
#     df = pd.DataFrame(tar_samp_marg, columns=[ str(a) for a in range(40,45) ])
#     g = sns.PairGrid(df)
#     g.map_diag(sns.kdeplot)
#     g.map_offdiag(sns.kdeplot, cmap="Blues_d", n_levels=6)
#     plt.show(False)
    
if DO_SMOOTH:
    if isinstance(target_density, SVHDENS.StocVolHyper):
        nhyper = sum(target_density.which_hyper)
    else:
        nhyper = 0
    tar_samp = push_base.rvs(NMC, nprocs=NPROCS)[:,nhyper:]
    tvec = np.zeros(target_density.dim + 1)
    tvec[1:] = target_density.tvec
    mean_smooth = np.zeros(tvec.shape)
    mean_smooth[1:] = np.mean(tar_samp, axis=0)
    q10_smooth = np.zeros(tvec.shape)
    q10_smooth[1:] = np.percentile(tar_samp, 10, axis=0)
    q90_smooth = np.zeros(tvec.shape)
    q90_smooth[1:] = np.percentile(tar_samp, 90, axis=0)

    fig = DIAG.nicePlot([],[], xlabel="time", ylabel="$\pi_{t}$",
                        title='Smoothing')
    ax = fig.gca()
    ax.set_aspect('equal')
    ax.set_ylim(-3,3)
    ax.set_xlim(0,target_density.tvec[-1])
    ax.plot(target_density.tvec, target_density.Xt, '--k', label="truth")
    ax.plot(target_density.tobs, target_density.dataObs, 'or')
    ax.plot(tvec, mean_smooth, '-k')
    ax.fill_between(tvec, q10_smooth, q90_smooth, facecolor='grey', alpha=0.2)
    plt.legend(loc='best')
    plt.show(False)
