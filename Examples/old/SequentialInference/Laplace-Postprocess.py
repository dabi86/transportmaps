#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import sys, getopt
import os.path
import dill
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import TransportMaps.Diagnostics as DIAG
import TransportMaps.Densities as DENS

def usage():
    print("StochasticVolatility-Laplace-Postprocess.py -i <file_name>")

def full_usage():
    usage()

argv = sys.argv[1:]
IN_FNAME = None
DO_MEAN = True
NMC = 10000
try:
    opts, args = getopt.getopt(argv,"hi:",["input=", "nmc=", "no-mean"])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("-i", "--input"):
        IN_FNAME = arg
    elif opt in "--nmc":
        NMC = int(arg)
    elif opt in "--no-mean":
        DO_MEAN = False
if None in [IN_FNAME]:
    full_usage()
    sys.exit(3)

# Load data
with open(IN_FNAME, 'rb') as in_stream:
    data = dill.load(in_stream)

# Resetore data
target_density = data['target_density']
laplace_id = data['laplace_id']
laplace_tm = data['laplace_tm']
base_density = DENS.StandardNormalDensity(target_density.dim)

# Construct T^\sharp \pi_{tar}
pull_tar_lap_tm = DENS.PullBackTransportMapDensity(laplace_tm, target_density)

print("Laplace TM - Random conditionals")
DIAG.plotRandomConditionals(pull_tar_lap_tm, num_conditionalsXax=6, numPointsXax = 40,
                            range_vec=[-8,8], title='Laplace TM')
dvec = list(npr.choice(np.arange(target_density.dim), min(target_density.dim, 8),
                       replace=False))
dvec = list(range(9))
print("Laplace TM - Random aligned conditionals: %s" % dvec)
DIAG.plotAlignedConditionals(pull_tar_lap_tm, numPointsXax = 40, range_vec=[-8,8],
                             dimensions_vec=dvec,
                             title='Conditionals pullback linear map') #, fig=fig)
var_diag_tm = DIAG.variance_approx_kl(base_density, pull_tar_lap_tm,
                                      qtype=0, qparams=NMC)
print("Laplace TM - Variance Diagnostic: %e" % var_diag_tm)

if DO_MEAN:
    # Mean
    plt.figure()
    plt.plot(target_density.tvec, target_density.Xt, '--k')
    plt.plot(target_density.tobs, target_density.dataObs, 'ro')
    plt.plot(target_density.tvec, laplace_tm.constantTerm,'-')
    plt.show(False)
