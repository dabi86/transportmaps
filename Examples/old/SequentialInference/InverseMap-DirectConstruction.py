#!/usr/bin/env python

#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Alessio Spantini, Daniele Bigoni
# E-mail: spantini@mit.edu, dabi@mit.edu
#

from __future__ import division #Get rid of integer division
import logging
import sys, getopt
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as npr
import scipy.stats as stats
import dill
import TransportMaps as TM
import TransportMaps.FiniteDifference as FD
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG

import StochasticVolatilityDensities as SVDENS
import StocVolHyperDensities as SVHDENS

logging.basicConfig(level=logging.INFO)

#
# Stochastic volatility model with no hyperparameters
#

# BASIS TYPES
# 'poly': Hermite polynomials
# 'rbf': Radial basis functions
AVAIL_BTYPE = ['poly', 'rbf']

# SPAN APPROXIMATION
# 'full': Full order approximation
# 'total': Total order approximation
AVAIL_SPAN = ['full', 'total']

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

def usage():
    print('StochasticVolatility-InverseMap-DirectConstruction.py --dens=<file_name> ' + \
          '-s <span_type> -o <order> -q <qtype> -n <qnum>' + \
          ' [-b <batch_size> --nprocs=<num_processes> --output=<file_name> ' + \
          '--btype=<basis_type> --tol=<eps> --with-reg=<alpha>]')

def print_avail_span():
    print('Available <span_type>: %s' % AVAIL_SPAN)

def print_avail_btype():
    print('Available <btype>: %s' % AVAIL_BTYPE)

def print_avail_qtype():
    print('Available <quad_type>:')
    for qtypen, qtypename in AVAIL_QUADRATURE.items():
        print('  %d: %s' % (qtypen, qtypename))

def full_usage():
    usage()
    print_avail_span()
    print_avail_qtype()

argv = sys.argv[1:]
ORDER = None
SPAN = None
QTYPE = None
QNUM = None
BTYPE = 'poly'
REG = None
TOL = 1e-4
BATCH_SIZE = None
NPROCS = 1
DENS_FNAME = None
OUT_FNAME = None
try:
    opts, args = getopt.getopt(argv,"hm:s:o:q:n:b:",["dens=","monoton_type=",
                                                     "order=", "tol=",
                                                     "span=", "bsize=",
                                                     "qtype=", "qnum=",
                                                     "nprocs=",
                                                     "dens=", "with-reg=",
                                                     "btype=", "output="])
except getopt.GetoptError:
    full_usage()
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--dens"):
        DENS_FNAME = arg
    elif opt in ("-t", "--btype"):
        BTYPE = arg
        if BTYPE not in AVAIL_BTYPE:
            usage()
            print_avail_btype()
            sys.exit(1)
    elif opt in ("-o", "--order"):
        ORDER = int(arg)
    elif opt in ("-s", "--span"):
        SPAN = arg
        if SPAN not in AVAIL_SPAN:
            usage()
            print_avail_span()
            sys.exit(1)
    elif opt in ("-q", "--qtype"):
        QTYPE = int(arg)
        if QTYPE not in AVAIL_QUADRATURE:
            usage()
            print_avail_qtype()
            sys.exit(1)
    elif opt in ("-n", "--qnum"):
        QNUM = int(arg)
    elif opt in ("-b", "--bsize"):
        BATCH_SIZE = int(arg)
    elif opt in ("--with-reg"):
        REG = {'type': 'L2',
               'alpha': float(arg)}
    elif opt in ("--tol"):
        TOL = float(arg)
    elif opt in "--nprocs":
        NPROCS = int(arg)
    elif opt in ("--dens"):
        DENS_FNAME = arg
    elif opt in ("--output"):
        OUT_FNAME = arg
    else:
        raise RuntimeError("Input option %s not recognized." % opt)
if None in [DENS_FNAME, ORDER, SPAN, QTYPE, QNUM, OUT_FNAME]:
    full_usage()
    sys.exit(3)

################ Define density ####################
with open(DENS_FNAME,'rb') as in_stream:
    target_density = dill.load(in_stream)

# # Check gradient
# xx = stats.norm().rvs(10*target_density.dim).reshape((10,target_density.dim))
# dx = 1e-4
# gx_tar_log_pdf = target_density.grad_x_log_pdf(xx)
# fd_gx_tar_log_pdf = FD.fd(target_density.log_pdf, xx, dx, None)
# maxerr = np.max(np.abs(gx_tar_log_pdf-fd_gx_tar_log_pdf))
# if maxerr > dx:
#     raise RuntimeError("Wrong gradient!")

############### PullBack through Laplace ###########
inflation = 1.3
laplace_approx = TM.laplace_approximation( target_density )
sqrt_cov = laplace_approx.sampling_mat
cov = np.dot(sqrt_cov, sqrt_cov.T)
diag_cov = inflation * np.diag(np.diag(cov))
diag_laplace = DENS.GaussianDensity(laplace_approx.mu, sigma=diag_cov)
laplace_tm = Maps.LinearTransportMap.build_from_Gaussian(diag_laplace)
laplace_target_density = DENS.PullBackTransportMapDensity(laplace_tm, target_density)

################ Compute Map #######################
if isinstance(target_density, SVHDENS.StocVolHyper):
    nhyper = sum(target_density.which_hyper)
else:
    nhyper = 0
active_vars = []
for i in range(nhyper):
    active_vars.append( list(range(i+1)) )
active_vars.append( list(range(nhyper)) + [nhyper] )
for i in range(nhyper, target_density.dim-1):
    active_vars.append( list(range(nhyper)) + [i, i+1] )
tm_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
    target_density.dim, ORDER, span=SPAN, active_vars=active_vars, btype=BTYPE)
base_density = DENS.StandardNormalDensity(target_density.dim)
pull_base = DENS.PullBackTransportMapDensity(tm_approx, base_density)

print("Number of coefficients: %d" % tm_approx.get_n_coeffs())

qtype = QTYPE
if qtype == 0:
    qparams = QNUM # Monte Carlo
elif qtype == 3:
    qparams = [QNUM]*target_density.dim # Quadrature
ders = 1       # Use gradient

x0 = None
maxit = 1000
# x0 = np.array([ -1.38679033,   0.72113294,  -0.42397364, -15.98984796,
#                 1.90025492,  -1.11072422, -15.8003925 ,   1.93780341,
#                 -1.30974253, -16.21899711,   1.95565298,  -0.60089775,
#                 -16.28004108,   1.90926732])
# maxit = 5

log_entry_solve = pull_base.minimize_kl_divergence(laplace_target_density,
                                                   qtype=qtype,
                                                   qparams=qparams,
                                                   regularization=REG,
                                                   tol=TOL, maxit=maxit, ders=ders,
                                                   x0=x0, nprocs=NPROCS)

if OUT_FNAME is not None:
    with open(OUT_FNAME, 'wb') as out_stream:
        data = {'target_density': target_density,
                'laplace_tm': laplace_tm,
                'tm_approx': tm_approx}
        dill.dump(data, out_stream)
