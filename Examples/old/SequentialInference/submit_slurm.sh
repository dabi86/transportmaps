#!/bin/bash
#SBATCH --job-name=tm-stocVol
#SBATCH --workdir=/master/home/dabi/VC-Projects/Software/Mine/Current/transport-maps/Examples/stochasticVolatility/Durbin_Rue_testCase/
#SBATCH --output=job.%J.out
#SBATCH --error=job.%J.err
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=16
#SBATCH --mail-user=dabi@mit.edu

$*
