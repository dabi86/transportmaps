import numpy as np
import SpectralToolbox.Spectral1D as S1D
import TransportMaps as TM
import TransportMaps.Functionals as FUNC
import src.TransportMaps.Maps as MAPS
import TransportMaps.Densities as DENS
import scipy.sparse as scisp

def generate_lift(dim, tm, idx, nhyper):
    r"""

    Args:
      dim (int): dimension including hyperparameters
    """
    approx_list = []
    active_vars = []
    id_comp = FUNC.FrozenLinear(1,0.,1.)
    for d in range(nhyper):
        approx_list.append( tm.approx_list[d] )
        active_vars.append( list(range(d+1)) )

    for d in range(nhyper, dim):
        if d == idx+nhyper:
            approx_list.append( tm.approx_list[-2] )
            active_vars.append( list(range(nhyper)) + [d] )
        elif d == idx+nhyper+1:
            approx_list.append( tm.approx_list[-1] )
            active_vars.append( list(range(nhyper)) + [d-1, d] )
        else:
            approx_list.append( id_comp )
            active_vars.append( [d] )

    tm_lift = Maps.TriangularTransportMap(active_vars, approx_list)
    return tm_lift

def compose_Markovian_tm(tm_list, nhyper):
    r""" Takes list of Markovian lower triangular matrices and the target density and returns the corresponding composed transport map.
    """
    dim = len(tm_list) + 1 + nhyper

    full_tm_list = []

    for d, tm in enumerate(tm_list):
        # Build permutation map (sparse)
        perm_lin_term = scisp.csr_matrix( scisp.identity(dim) )
        #perm_lin_term = np.eye(dim)
        base_index = nhyper + d
        perm_lin_term[base_index,base_index] = 0.
        perm_lin_term[base_index+1,base_index+1] = 0.
        perm_lin_term[base_index,base_index+1] = 1.
        perm_lin_term[base_index+1,base_index] = 1.
        perm_const_term = np.zeros(dim)
        permutation_map = Maps.LinearTransportMap(perm_const_term, perm_lin_term, log_det = 0.)
        # Lift Transport Map
        tm_lift = generate_lift(dim, tm, d, nhyper)

        full_tm_list.extend( (permutation_map,tm_lift,permutation_map)  )

#         if d == 0:
#             Compositions
#             composed_map = permutation_map
#             composed_map = MAPS.CompositeTransportMap(composed_map, tm_lift)
#             composed_map = MAPS.CompositeTransportMap(composed_map, permutation_map)
#         else:
#             composed_map = MAPS.CompositeTransportMap(composed_map, permutation_map)
#             composed_map = MAPS.CompositeTransportMap(composed_map, tm_lift)
#             composed_map = MAPS.CompositeTransportMap(composed_map, permutation_map)

    composed_map = Maps.TEMP_RightCompositeTransportMap_norecursion(full_tm_list)
    return composed_map


def accurate_regression( tmap, span, btype, min_order_map = 1, tol_regression = 1e-6,
                         max_order_map = 10, max_npoints = 4000, base_d = None,
                         mpi_pool=None):
    dim = tmap.dim
    distL2 = tol_regression + 1
    map_order = min_order_map
    mquad = min( int( np.floor((max_npoints)**(1./dim)) ) , 30)
    
    if base_d is None: #Default to a standard normal
        base_d = DENS.StandardNormalDensity(dim)

    mpi_pool_list = [ None if d<2 else mpi_pool for d in range(dim) ]
    it = 0
    while distL2>tol_regression and map_order < max_order_map:
        tmap_approx = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
            dim, map_order, span=span, btype=btype)
        tmap_approx.regression(
            tmap, d=base_d, qtype=3, qparams=[mquad]*dim, tol=tol_regression*1e-1,
            mpi_pool_list=mpi_pool_list)
        #Evaluate performance regression with L2 errror
        Xquad, Wquad = base_d.quadrature(qtype = 3, qparams=[mquad+1]*dim)
        distL2 = tmap.evaluate(Xquad) - tmap_approx.evaluate(Xquad)
        distL2 = np.sqrt( np.sum ( Wquad[:,np.newaxis] * distL2**2 ) )
        #Counter iteration
        it += 1
        map_order += 1
    if map_order == max_order_map:
        message_it = "MaxOrderReached"
    else:
        message_it = it
    return tmap_approx, distL2, message_it, map_order-1
