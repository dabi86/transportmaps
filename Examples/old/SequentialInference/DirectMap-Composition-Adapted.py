#!/usr/bin/env python
#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from __future__ import division #Get rid of integer division
import getopt
import os.path
import sys
import dill
import warnings
import numpy as np
import logging

import TransportMaps as TM
import TransportMaps.Densities as DENS
import TransportMaps.Diagnostics as DIAG
import TransportMaps.FiniteDifference as FD

import MapGenerationHyperDurbin as MG

logging.basicConfig(level=logging.INFO)

# Init logger
logger = logging.getLogger(__name__)
logger.propagate = False
ch = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter("%(asctime)s %(levelname)s: %(name)s: %(message)s",
                              "%Y-%m-%d %H:%M:%S")
ch.setFormatter(formatter)
logger.addHandler(ch)

#
# Stochastic volatility model with hyper-parameters
#

# BASIS TYPES
# 'poly': Hermite polynomials
# 'rbf': Radial basis functions
AVAIL_BTYPE = ['poly', 'rbf']

# SPAN APPROXIMATION
# 'full': Full order approximation
# 'total': Total order approximation
AVAIL_SPAN = ['full', 'total']

# QUADRATURES
# 0: Monte Carlo
# 3: Gauss quadrature
AVAIL_QUADRATURE = {0: 'Monte Carlo',
                    3: 'Gauss quadrature'}

# DERS
# 1: BFGS
# 2: Newton-CG
AVAIL_DERS = {1: 'BFGS',
              2: 'Newton-CG'}

def usage():
    print('StochasticVolatilityHyper-DirectMap-Composition-Adapted.py --dens=<file_name> ' + \
          '--max-n-points=<n> ' + \
          '[--span-list=<span_type> ' + \
          '--max-hess-size <max_Hess_size> --output=<file_name> ' + \
          '--btype-list=<basis_type> --no-plotting --nprocs=<num_processors> ' + \
          '--max-order-map-regression=<order_regression> --eps-var-diag=<eps> ' + \
          '--itmax-var-diag=<itmax> --with-reg=<alpha> --ders=<ders> --mpi-lag=<lag>]')

def print_avail_span():
    print('Available <span_type>: %s' % AVAIL_SPAN)

def print_avail_btype():
    print('Available <btype>: %s' % AVAIL_BTYPE)

def print_avail_ders():
    print('Available <ders>: %s' % AVAIL_DERS)

def full_usage():
    usage()
    print_avail_span()
    print_avail_btype()
    print_avail_ders()

qtype = 3
argv = sys.argv[1:]
MONOTONE = 'intexp'
MAXNPOINTS = None
DENS_FNAME = None
OUT_FNAME = None
DO_PLOT = True
CHECK_DERS = False
EPS_VAR_DIAG = 1e-3
ITMAX_VAR_DIAG = 5
ORDER_REGRESSION = 6
MAX_HESS_SIZE = 10**7
DERS = 2
NPROCS = 1
REGULARIZATION = None
tol_average = 1e-4     # Optimization tolerance
tol_min = 1e-4
WARMSTART = False
try:
    opts, args = getopt.getopt(argv,"hm:q:n:b:",["max-n-points=",
                                                 "span-list=",
                                                 "dens=", "nprocs=", 
                                                 "btype-list=", "output=",
                                                 "no-plotting",
                                                 "check-ders",
                                                 "eps-var-diag=", "itmax-var-diag=",
                                                 "ders=", "with-reg=",
                                                 "force-warm",
                                                 "max-hess-size=",
                                                 "max-ordermap-regression="])
except getopt.GetoptError as e:
    full_usage()
    print(e)
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        full_usage()
        sys.exit()
    elif opt in ("--dens"):
        DENS_FNAME = arg
    elif opt in ("--max-n-points"):
        MAXNPOINTS = int(arg)
    elif opt in ("--btype-list"):
        BTYPE = arg.split(',')
    elif opt in ("--span-list"):
        SPAN = arg.split(',')
    elif opt in ("--output"):
        OUT_FNAME = arg
    elif opt in "--no-plotting":
        DO_PLOT = False
    elif opt in "--force-warm":
        WARMSTART = True
    elif opt in "--check-ders":
        CHECK_DERS = True
    elif opt in ("--eps-var-diag"):
        EPS_VAR_DIAG = float(arg)
    elif opt in ("--itmax-var-diag"):
        ITMAX_VAR_DIAG = int(arg)
    elif opt in ("--with-reg"):
        REGULARIZATION = {'type': 'L2',
                          'alpha': float(arg)}
    elif opt in ("--ders="):
        DERS = int(arg)
    elif opt in ("--max-hess-size="):
        MAX_HESS_SIZE = int(arg)
    elif opt in ("--max-ordermap-regression="):
        ORDER_REGRESSION = int(arg)
    elif opt in ("--nprocs="):
        NPROCS = int(arg)
    else:
        raise RuntimeError("Input option %s not recognized." % opt)

if OUT_FNAME is not None and os.path.exists(OUT_FNAME) and not WARMSTART:
    if sys.version_info[0] == 3:
        sel = input("The file %s already exists. Do you want to use it to warm start? [y/N] " % OUT_FNAME)
    else:
        sel = raw_input("The file %s already exists. Do you want to use it to warm start? [y/N] " % OUT_FNAME)
    if sel == 'y' or sel == 'Y':
        WARMSTART = True
    else:
        if sys.version_info[0] == 3:
            sel = input("Do you want to overwrite? [y/N] ")
        else:
            sel = raw_input("Do you want to overwrite? [y/N] ")
        if sel == 'y' or sel == 'Y':
            pass
        else:
            print("Terminating.")
            sys.exit(0)
if WARMSTART:
    # Restore state
    with open(OUT_FNAME, 'rb') as in_stream:
        data = dill.load(in_stream)
        target_density = data['target_density']
        tm_approx_comp_list = data['tm_approx_comp_list']
        tm_approx_hyper_list = data['tm_approx_hyper_list']
        regression_order_list = data['regression_order_list']
        var_diag_list = data['var_diag_list']
        map_order_list = data['map_order_list']
        MONOTONE = data['MONOTONE']
        BTYPE = data['BTYPE']
        SPAN = data['SPAN']
        nstart = len(tm_approx_comp_list)
else:
    tm_approx_comp_list = [] # List of 2d component maps
    map_order_list = []
    tm_approx_hyper_list = [None]
    regression_order_list = []
    var_diag_list = []
    nstart = 0

if not WARMSTART and None in [DENS_FNAME, OUT_FNAME, MONOTONE]:
    full_usage()
    sys.exit(3)

if DO_PLOT:
    import matplotlib.pyplot as plt
    from matplotlib.patches import Rectangle
    plt.ion()

if not WARMSTART:
    with open(DENS_FNAME,'rb') as in_stream:
        target_density = dill.load(in_stream)

dim = target_density.dim
nhyper = sum(target_density.which_hyper)
nsteps = target_density.nsteps
markov_dim = 2 + nhyper

if SPAN is None:
    SPAN = ['full']*nhyper + ['total']*2
if BTYPE is None:
    BTYPE = ['rbf']*nhyper + ['poly']*2

mquad = int( np.floor((MAXNPOINTS)**(1./markov_dim)) )
base_density_comp = DENS.StandardNormalDensity(markov_dim)

if DO_PLOT:
    fig_obs = plt.figure()
    ax_obs = fig_obs.add_subplot(111)
    ax_obs.plot(target_density.tobs, target_density.dataObs, 'o-')
    ax_obs.axis('equal')
    plt.show(False)

    fig_slice_map = None
    fig_pullback = None
    fig_perm_target = None

#### START POOL OF PROCESSES #####
mpi_pool = None
if NPROCS > 1:
    mpi_pool = TM.get_mpi_pool()
    mpi_pool.start(NPROCS)

# Define mpi_pool_list for regression
mpi_pool_list = [None]*3
if dim > 3:
    mpi_pool_list.append( mpi_pool )
if dim > 4:
    mpi_pool_list.append( mpi_pool )
    
try:
    ################ Compute Map list #######################
    if nstart == 0:
        # Solve for Map 0
        if DO_PLOT:
            # Plot where we are
            ax_obs.clear()
            ax_obs.plot(target_density.tobs, target_density.dataObs, 'o-')
            ax_obs.plot([target_density.tvec[1]]*2, [-3.,3.], 'k--')
            ax_obs.add_patch(Rectangle((0., -2.), target_density.tvec[1], 4.,
                                       facecolor="grey", alpha=0.4))
            ax_obs.axis('equal')
            fig_obs.canvas.draw()

        var_diag = 1.
        map_order = 0
        it = -1
        tm_approx_comp = None
        while var_diag >= EPS_VAR_DIAG and it < ITMAX_VAR_DIAG:
            it += 1
            map_order += 1
            tm_approx_comp_old = tm_approx_comp

            tm_approx_comp = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
                                                    markov_dim, map_order, span=SPAN, btype=BTYPE)
            ders = DERS
            if tm_approx_comp_old is not None:
                batch_size_list = [
                    (None, None,
                     int(np.ceil( float(MAX_HESS_SIZE) / c.get_n_coeffs()**2 )))
                    for c in tm_approx_comp.approx_list ]
                logger.info("Regression - batch_size_list: %s" % (str(batch_size_list)))
                tm_approx_comp.regression(tm_approx_comp_old, d=base_density_comp,
                                          qtype=3, qparams=[mquad]*markov_dim, tol=1e-3,
                                          batch_size_list=batch_size_list,
                                          regularization=REGULARIZATION,
                                          mpi_pool_list=mpi_pool_list)
            tm_density_comp = DENS.PushForwardTransportMapDensity(
                tm_approx_comp, base_density_comp)
            target_comp = target_density.get_MarkovFactor(0)

            if CHECK_DERS:
                X = base_density_comp.rvs( 10 )
                (success, maxerr) = FD.finiteDifference_gradient_X(
                    target_comp.log_pdf,
                    target_comp.grad_x_log_pdf, X )
                if not success:
                    raise RuntimeError("Wrong gradient. Maxerr: %e" % maxerr)
                (success, maxerr) = FD.finiteDifference_hessian_X(
                    target_comp.grad_x_log_pdf,
                    target_comp.hess_x_log_pdf, X )
                if not success:
                    raise RuntimeError("Wrong Hessian. Maxerr: %e" % maxerr)
            if DO_PLOT and it == 0:
                fig_perm_target = DIAG.plotAlignedConditionals(
                    target_comp, range_vec=[-10,10], title='Permuted target %d' % 0,
                    numPointsXax = 20, fig = fig_perm_target, mpi_pool=mpi_pool)

            logger.info("Map 0 - it %d - " % it + \
                        "Number of coefficients: %d" % tm_density_comp.get_n_coeffs())
            qparams = [mquad]*markov_dim # Quadrature
            # Solve
            x0 = tm_approx_comp.get_coeffs()
            if it < ITMAX_VAR_DIAG:
                tol = tol_average
            else:
                tol = tol_min
            batch_size = [None, None, int(np.ceil(float(MAX_HESS_SIZE) / \
                                                  tm_density_comp.get_n_coeffs()**2)) ]
            logger.info("MinKL - batch_size: %s" % str(batch_size))
            log_entry_solve = tm_density_comp.minimize_kl_divergence(
                target_comp, qtype=qtype, qparams=qparams, x0=x0,
                regularization=REGULARIZATION,
                tol=tol, ders=ders, batch_size=batch_size, mpi_pool=mpi_pool)

            # Construct T^\sharp tar
            pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                                         target_comp )
            if DO_PLOT:
                # Run diagnostics
                logger.info("Conditional Standard")
                fig_slice_map = DIAG.plotAlignedSliceMap(
                    tm_approx_comp, range_vec=[-4,4], numPointsXax = 20,
                    numCont = 30,  tickslabelsize = 10, fig = fig_slice_map,
                    mpi_pool=mpi_pool)
                fig_pullback = DIAG.plotAlignedConditionals(
                    pull_tar, title='Pullback %d' % 0, range_vec=[-10,10],
                    numPointsXax = 20, fig = fig_pullback, mpi_pool=mpi_pool)
            var_diag = DIAG.variance_approx_kl(
                base_density_comp, pull_tar, qtype=3, qparams=[mquad+1]*markov_dim,
                mpi_pool_tuple=(None,mpi_pool))
            logger.info("Map 0 - it %d - Variance diagnostic: %e" % (it, var_diag))

        if it >= ITMAX_VAR_DIAG and var_diag>EPS_VAR_DIAG:
            warnings.warn("Map not accurate enough", RuntimeWarning)

        tm_approx_comp_list.append( tm_approx_comp )
        map_order_list.append( map_order )
        var_diag_list.append(var_diag)
        if nhyper>0:
            tm_approx_hyper_list.append( Maps.TransportMap(
                tm_approx_comp.active_vars[:nhyper],
                tm_approx_comp.approx_list[:nhyper]))
        else:
            tm_approx_hyper_list.append(None)


        # Store transport maps:
        with open(OUT_FNAME, 'wb') as out_stream:
            data = {'target_density': target_density,
                    'tm_approx_comp_list': tm_approx_comp_list,
                    'tm_approx_hyper_list': tm_approx_hyper_list,
                    'regression_order_list': regression_order_list,
                    'map_order_list': map_order_list,
                    'var_diag_list':var_diag_list,
                    'MONOTONE': MONOTONE,
                    'BTYPE': BTYPE,
                    'SPAN': SPAN }
            dill.dump(data, out_stream)

    # Interate over Map n
    for n in range(max(1,nstart),nsteps-1):
        if DO_PLOT:
            # Plot where we are
            ax_obs.clear()
            ax_obs.plot(target_density.tobs,
                        target_density.dataObs, 'o-')
            ax_obs.plot([target_density.tvec[n+1]]*2, [-3.,3.], 'k--')
            ax_obs.add_patch(Rectangle((0., -3.), target_density.tvec[n+1], 6.,
                                       facecolor="grey", alpha=0.4))
            ax_obs.axis('equal')
            fig_obs.canvas.draw()
        if WARMSTART:
            tm_approx_comp_prev = tm_approx_comp_list[n-1]
            tm_approx_hyper = tm_approx_hyper_list[n]
            target_comp = target_density.get_MarkovFactor(
                n, component_map=tm_approx_comp_prev.approx_list[-2],
                hyper_map=tm_approx_hyper)
        var_diag = 1.
        map_order = 0
        it = -1
        tm_approx_comp = None
        while var_diag >= EPS_VAR_DIAG and it < ITMAX_VAR_DIAG:
            it += 1
            map_order += 1
            tm_approx_comp_old = tm_approx_comp
            tm_approx_comp = TM.Default_IsotropicIntegratedExponentialTriangularTransportMap(
                                            markov_dim, map_order, span=SPAN, btype=BTYPE)
            ders = DERS
            if tm_approx_comp_old is not None:
                batch_size_list = [
                    (None, None,
                     int(np.ceil( float(MAX_HESS_SIZE) / c.get_n_coeffs()**2 )))
                    for c in tm_approx_comp.approx_list ]
                logger.info("Regression - batch_size_list: %s" % (str(batch_size_list)))
                tm_approx_comp.regression(tm_approx_comp_old, d=base_density_comp,
                                          qtype=3, qparams=[mquad]*markov_dim, tol=1e-3,
                                          batch_size_list=batch_size_list,
                                          regularization=REGULARIZATION,
                                          mpi_pool_list=mpi_pool_list)
            tm_density_comp = DENS.PushForwardTransportMapDensity(
                tm_approx_comp, base_density_comp)

            tm_approx_comp_prev = tm_approx_comp_list[n-1]
            tm_approx_hyper = tm_approx_hyper_list[n]

            target_comp = target_density.get_MarkovFactor(
                n, component_map=tm_approx_comp_prev.approx_list[-2], hyper_map=tm_approx_hyper)

            if CHECK_DERS:
                X = base_density_comp.rvs( 10 )
                (success, maxerr) = FD.finiteDifference_gradient_X(
                    target_comp.log_pdf,
                    target_comp.grad_x_log_pdf, X )
                if not success:
                    raise RuntimeError("Wrong gradient. Maxerr: %e" % maxerr)
                (success, maxerr) = FD.finiteDifference_hessian_X(
                    target_comp.grad_x_log_pdf,
                    target_comp.hess_x_log_pdf, X )
                if not success:
                    raise RuntimeError("Wrong Hessian. Maxerr: %e" % maxerr)
            if DO_PLOT and it == 0:
                fig_perm_target = DIAG.plotAlignedConditionals(
                    target_comp, range_vec=[-10,10], title='Permuted target %d' % n,
                    numPointsXax = 20, fig = fig_perm_target, mpi_pool=mpi_pool)

            logger.info("Map %d - it %d - Number of coefficients: %d" %
                        (n,it,tm_density_comp.get_n_coeffs()))
            qparams = [mquad]*markov_dim # Quadrature
            # Solve
            x0 = tm_approx_comp.get_coeffs()
            if it < ITMAX_VAR_DIAG:
                tol = tol_average
            else:
                tol = tol_min
            batch_size = [None, None, int(np.ceil(float(MAX_HESS_SIZE) / \
                                                  tm_density_comp.get_n_coeffs()**2 )) ]
            logger.info("MinKL - batch_size: %s" % str(batch_size))
            log_entry_solve = tm_density_comp.minimize_kl_divergence(
                target_comp, qtype=qtype, qparams=qparams, x0=x0,
                regularization=REGULARIZATION, tol=tol, ders=ders,
                batch_size=batch_size, mpi_pool=mpi_pool)
            # Construct T^\sharp tar
            pull_tar = DENS.PullBackTransportMapDensity( tm_approx_comp,
                                                         target_comp )
            if DO_PLOT:
                # Run diagnostics
                logger.info("Conditional Standard")
                fig_slice_map = DIAG.plotAlignedSliceMap(
                    tm_approx_comp, range_vec=[-4,4], numPointsXax = 20, numCont = 30,
                    tickslabelsize = 10, fig = fig_slice_map, mpi_pool=mpi_pool)
                fig_pullback = DIAG.plotAlignedConditionals(
                    pull_tar, title='Pullback %d' % n, range_vec=[-10,10],
                    numPointsXax = 20, fig = fig_pullback, mpi_pool=mpi_pool)
            var_diag = DIAG.variance_approx_kl(
                base_density_comp, pull_tar, qtype=3, qparams=[mquad+1]*markov_dim,
                mpi_pool_tuple=(mpi_pool,mpi_pool))
            logger.info("Map %d - it %d - Variance diagnostic: %e" % (n,it,var_diag))

        if it >= ITMAX_VAR_DIAG:
            warnings.warn("Map not accurate enough", RuntimeWarning)

        if nhyper > 0:
        #Regression step hyper-map
        #tm_approx_hyper_next \approx tm_approx_hyper \circ tm_approx_comp_headhyper
            tm_approx_comp_headhyper = Maps.TransportMap(
                tm_approx_comp.active_vars[:nhyper],tm_approx_comp.approx_list[:nhyper])

            map2approximate = Maps.CompositeTransportMap(
                tm_approx_hyper, tm_approx_comp_headhyper)

            (tm_approx_hyper_next, distL2,
             message_it, regression_order) = MG.accurate_regression(
                 map2approximate,
                 span=SPAN[:nhyper], btype=BTYPE[:nhyper],
                 min_order_map = map_order,
                 tol_regression = 1e-6,
                 max_order_map = ORDER_REGRESSION, max_npoints = MAXNPOINTS,
                 mpi_pool=mpi_pool)
            regression_order_list.insert(n,regression_order)
            if isinstance(message_it,str):
                logger.info("Regression hyper-map: "+message_it+" dist ="+str(distL2))
        # Append approximations
        tm_approx_comp_list.append( tm_approx_comp )
        map_order_list.append( map_order )
        var_diag_list.append(var_diag)
        if nhyper>0:
            tm_approx_hyper_list.append( tm_approx_hyper_next )
        else:
            tm_approx_hyper_list.append(None)

        # Store transport maps:
        with open(OUT_FNAME, 'wb') as out_stream:
            data = {'target_density': target_density,
                    'tm_approx_comp_list': tm_approx_comp_list,
                    'tm_approx_hyper_list': tm_approx_hyper_list,
                    'regression_order_list': regression_order_list,
                    'map_order_list': map_order_list,
                    'var_diag_list':var_diag_list,
                    'MONOTONE': MONOTONE,
                    'BTYPE': BTYPE,
                    'SPAN': SPAN }
            dill.dump(data, out_stream)

    # #Check gradient
    # fun = svDens.log_pdf
    # grad = svDens.grad_x_log_pdf
    # X = np.random.rand( 10, ndim  )
    # finiteDifference_gradient_X( fun , grad , X )

    # #Check hessian
    # fun = svDens.grad_x_log_pdf
    # hess = svDens.hess_x_log_pdf
    # X = np.random.rand( 8, ndim  )
    # finiteDifference_hessian_X( fun , hess , X )

finally:
    if mpi_pool is not None:
        mpi_pool.stop()
