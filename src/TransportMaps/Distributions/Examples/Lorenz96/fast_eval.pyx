#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Authors: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

import cython
from cython cimport floating
import numpy as np
cimport numpy as np

__all__ = [
    'l96mII_evaluate',
    'l96mII_grad_x',
    'l96mII_tuple_grad_x',
    'l96mII_hess_x',
    'l96mII_action_hess_x'
]

DTYPE_INT = np.int64
ctypedef np.int64_t DTYPE_INT_t

DTYPE_FLOAT = np.float64
ctypedef np.float64_t DTYPE_FLOAT_t

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def inplace_roll(
        np.ndarray[DTYPE_FLOAT_t, ndim=2] x,  # Input
        np.ndarray[DTYPE_FLOAT_t, ndim=2] r,  # Output
        DTYPE_INT_t i,
        DTYPE_INT_t m,
        DTYPE_INT_t d):
    cdef int j, l
    for j in range(m):
        for l in range(d):
            r[j,l] = x[j, (l+i)%d]

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def inplace_w(
        np.ndarray[DTYPE_FLOAT_t, ndim=2] x,  # Input
        np.ndarray[DTYPE_FLOAT_t, ndim=2] w,  # Output
        np.ndarray[DTYPE_FLOAT_t, ndim=2] t,  # tmp
        DTYPE_INT_t J,
        DTYPE_INT_t K,
        DTYPE_INT_t m,
        DTYPE_INT_t d):
    cdef int i
    for i in range(-J, J+1):
        inplace_roll(x, t, i, m, d)
        w += t
    w /= K

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def l96mII_evaluate(
        np.ndarray[DTYPE_FLOAT_t, ndim=2] x,
        DTYPE_INT_t K,
        DTYPE_FLOAT_t F):
    cdef int m = x.shape[0]
    cdef int d = x.shape[1]
    cdef int J = (K-1) // 2
    cdef int i, j, l

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] t1 = np.zeros([m, d], dtype=DTYPE_FLOAT)
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] t2 = np.zeros([m, d], dtype=DTYPE_FLOAT)    
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] w = np.zeros([m, d], dtype=DTYPE_FLOAT)
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] out = np.zeros([m, d], dtype=DTYPE_FLOAT)

    inplace_w(x, w, t1, J, K, m, d)

    out = -x + F
    
    inplace_roll(w, t1, -2*K, m, d)
    inplace_roll(w, t2, -K, m, d)
    out -= t1 * t2
    
    for i in range(-J, J+1):
        inplace_roll(w, t1, -K+i, m, d)
        inplace_roll(x, t2, K+i, m, d)
        out += t1 * t2 / K

    return out

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def l96mII_grad_x(
        np.ndarray[DTYPE_FLOAT_t, ndim=2] x,
        DTYPE_INT_t K,
        DTYPE_FLOAT_t F):
    cdef int m = x.shape[0]
    cdef int d = x.shape[1]
    cdef int J = (K-1) // 2
    cdef int i, j, l, n, p

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] t1 = np.zeros([m, d], dtype=DTYPE_FLOAT)
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] t2 = np.zeros([m, d], dtype=DTYPE_FLOAT)    
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] w = np.zeros([m, d], dtype=DTYPE_FLOAT)
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=3] out = np.zeros([m, d, d], dtype=DTYPE_FLOAT)

    inplace_w(x, w, t1, J, K, m, d)

    for i in range(m):
        for n in range(d):
            out[i,n,n] = -1.

    inplace_roll(w, t1, -K, m, d)
    inplace_roll(w, t2, -2*K, m, d)        
    for i in range(m):
        for n in range(d):
            for p in range(n-2*K-J,n-2*K+J+1):
                out[i,n,p%d] -= t1[i,n] / K
            for p in range(n-K-J, n-K+J+1):
                out[i,n,p%d] -= t2[i,n] / K
                
    for j in range(-J, J+1):
        inplace_roll(w, t1, -K+j, m, d)
        inplace_roll(x, t2, K+j, m, d)
        for i in range(m):
            for n in range(d):
                for p in range(n-K+j-J, n-K+j+J+1):
                    out[i,n,p%d] += t2[i,n] / K**2
                out[i,n,(n+K+j)%d] += t1[i,n] / K

    return out

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def l96mII_tuple_grad_x(
        np.ndarray[DTYPE_FLOAT_t, ndim=2] x,
        DTYPE_INT_t K,
        DTYPE_FLOAT_t F):
    cdef int m = x.shape[0]
    cdef int d = x.shape[1]
    cdef int J = (K-1) // 2
    cdef int i, j, l, n, p

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] t1 = np.zeros([m, d], dtype=DTYPE_FLOAT)
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] t2 = np.zeros([m, d], dtype=DTYPE_FLOAT)    
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] w = np.zeros([m, d], dtype=DTYPE_FLOAT)
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] f = np.zeros([m, d], dtype=DTYPE_FLOAT)
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=3] gx = np.zeros([m, d, d], dtype=DTYPE_FLOAT)

    inplace_w(x, w, t1, J, K, m, d)
    
    f = -x + F
    for i in range(m):
        for n in range(d):
            gx[i,n,n] = -1.
    
    inplace_roll(w, t1, -2*K, m, d)
    inplace_roll(w, t2, -K, m, d)
    f -= t1 * t2
    for i in range(m):
        for n in range(d):
            for p in range(n-2*K-J,n-2*K+J+1):
                gx[i,n,p%d] -= t2[i,n] / K
            for p in range(n-K-J, n-K+J+1):
                gx[i,n,p%d] -= t1[i,n] / K
    
    for j in range(-J, J+1):
        inplace_roll(w, t1, -K+j, m, d)
        inplace_roll(x, t2, K+j, m, d)
        f += t1 * t2 / K
        for i in range(m):
            for n in range(d):
                for p in range(n-K+j-J, n-K+j+J+1):
                    gx[i,n,p%d] += t2[i,n] / K**2
                gx[i,n,(n+K+j)%d] += t1[i,n] / K
                
    return (f, gx)
    
    
@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def l96mII_hess_x(
        np.ndarray[DTYPE_FLOAT_t, ndim=2] x,
        DTYPE_INT_t K,
        DTYPE_FLOAT_t F):
    cdef int m = x.shape[0]
    cdef int d = x.shape[1]
    cdef int J = (K-1) // 2
    cdef int i, j, l, n, p, q
    cdef DTYPE_FLOAT_t Km2 = 1./K**2

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=4] out = np.zeros([m, d, d, d], dtype=DTYPE_FLOAT)

    for i in range(m):
        for n in range(d):
            for p in range(n-2*K-J, n-2*K+J+1):
                for q in range(n-K-J, n-K+J+1):
                    out[i,n,p%d,q%d] -= Km2
                    out[i,n,q%d,p%d] -= Km2
                    
    for j in range(-J, J+1):
        for i in range(m):
            for n in range(d):
                for p in range(n-K+j-J, n-K+j+J+1):
                    out[i,n,p%d,(n+K+j)%d] += Km2
                    out[i,n,(n+K+j)%d,p%d] += Km2

    return out

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
@cython.nonecheck(False)
def l96mII_action_hess_x(
        np.ndarray[DTYPE_FLOAT_t, ndim=2] x,
        np.ndarray[DTYPE_FLOAT_t, ndim=2] dx,
        DTYPE_INT_t K,
        DTYPE_FLOAT_t F):
    cdef int m = x.shape[0]
    cdef int d = x.shape[1]
    cdef int J = (K-1) // 2
    cdef int i, j, l, n, p, q
    cdef DTYPE_FLOAT_t Km2 = 1./K**2

    cdef np.ndarray[DTYPE_FLOAT_t, ndim=2] dxKm2 = dx * Km2
    cdef np.ndarray[DTYPE_FLOAT_t, ndim=3] out = np.zeros([m, d, d], dtype=DTYPE_FLOAT)

    for i in range(m):
        for n in range(d):
            for p in range(n-2*K-J, n-2*K+J+1):
                for q in range(n-K-J, n-K+J+1):
                    out[i,n,p%d] -= dxKm2[i,q%d]
                    out[i,n,q%d] -= dxKm2[i,p%d]

    for j in range(-J, J+1):
        for i in range(m):
            for n in range(d):
                for p in range(n-K+j-J, n-K+j+J+1):
                    out[i,n,p%d] += dxKm2[i,(n+K+j)%d]
                    out[i,n,(n+K+j)%d] += dxKm2[i,p%d]

    return out