Algorithms
==========

A number of algorithms have been and are being developed on top of the transport map framework.

.. toctree::
   :maxdepth: 2

   example-sequential-stocvol-6d.ipynb
   example-inverse-sparsity-identification.ipynb
