Installation
============

Supported systems
-----------------

* \*nix like OS (Linux, Unix, ...)
* Mac OS

Other operating systems have not been tested and they likely require a more complex procedure for the installation (this includes the Microsoft Windows family..).

We reccommend to work in a virtual environment using `virtualenv <https://virtualenv.readthedocs.io/en/latest/>`_ or `Anaconda <https://www.continuum.io/why-anaconda>`_.

Installation requirements
-------------------------

* `gcc <https://gcc.gnu.org/>`_ (or an alternative C/C++ compiler)
* `gfortran <https://gcc.gnu.org/fortran/>`_ (or an alternative Fortran compiler)
* `python3 <https://www.python.org/>`_
* `openMPI <https://www.open-mpi.org/>`_ or `mpich <https://www.mpich.org/>`_

Installing requirements on Ubuntu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. prompt:: bash

    sudo apt install gcc libc6-dev
    sudo apt install gfortran libgfortran3
    sudo apt install libopenmpi-dev

Installing requirements on Mac OS (using homebrew)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. prompt:: bash

    brew install llvm
    brew install gfortran
    brew install open-mpi


Installation
------------

First of all make sure to have the latest version of `pip <https://pypi.python.org/pypi/pip>`_ installed

.. prompt:: bash

   pip install --upgrade pip

The package and its python dependencies can be installed running the commands:

.. prompt:: bash

   pip install --upgrade TransportMaps

The packages numpy and cython must be installed separately because some dependencies do not
list then among the build requirements.

If one whish to enable some of the optional dependencies:

.. prompt:: bash

   pip install --upgrade TransportMaps[option]

These options will install the following modules:

* DOLFIN -- dolfin package for Partial Differential Equations

* SUITESPARSE -- scikit-sparse

    * This requires `scitik-sparse <https://github.com/scikit-sparse/scikit-sparse>`_ and the package `libsuitesparse-dev <https://packages.ubuntu.com/focal/libsuitesparse-dev>`_

* HMC -- Hamiltonian Monte Carlo `pyhmc <https://pythonhosted.org/pyhmc/>`_ (it requires having numpy and cython already installed)

Manual installation
~~~~~~~~~~~~~~~~~~~

If anything goes wrong with the automatic installation you can try to install from source:

.. prompt:: bash

    git clone git@bitbucket.org:dabi86/transportmaps.git
    cd transportmaps
    pip install --upgrade numpy cython
    pip install .


Running the Unit Tests
----------------------

Unit tests are available and can be run through the command:

   >>> import TransportMaps as TM
   >>> TM.tests.run_tests()

Or directly using the bash command:

   $ tmap-run-tests

The Git repository also contains a docker-compose configuration file to test the whole suite on several
versions of python.

There are >2000 unit tests, and it will take some time to run all of them.

FAQ
---

List of Frequently Asked Questions

The package ``mpi4py`` fail to install with error: missing <mpi.h>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One mpi backend must be installed on the machine (e.g. `OpenMPI <https://www.open-mpi.org/>`_, `MPICH <https://www.mpich.org/>`_) and find out where the corresponding header file <mpi.h> is located. Then the package can be installed by setting the environment variables ``CPLUS_INCLUDE_PATH`` or ``C_INCLUDE_PATH``:

.. prompt:: bash

   CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:<mpi_path> \
   pip install --upgrade mpi4py


