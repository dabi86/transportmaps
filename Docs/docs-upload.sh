#!/bin/bash

set -e

echo ""
echo "Make"
make clean
make html

echo ""
echo "Entering maintenance mode"
CMD="cd /mit/uqlab/web_scripts/transportmaps-website/;"
CMD+="mv .htaccess .htaccess.bak"
ssh athena.dialup.mit.edu "$CMD"

echo ""
echo "Backing up former version"
CMD="cd /mit/uqlab/web_scripts/transportmaps-website/;"
CMD+="mv docs docs-bak;"
CMD+="mkdir docs;"
CMD+="tar -czf docs-bak.tar.gz docs-bak --remove-files"
ssh athena.dialup.mit.edu "$CMD"

echo ""
echo "Upload"
cd build/html/ && tar -czf ../docs.tar.gz * && cd -;
scp -r build/docs.tar.gz athena.dialup.mit.edu:/mit/uqlab/web_scripts/transportmaps-website/docs/
CMD="cd /mit/uqlab/web_scripts/transportmaps-website/docs;"
CMD+="tar -xzf docs.tar.gz;"
CMD+="rm docs.tar.gz"
ssh athena.dialup.mit.edu "$CMD"


echo ""
echo "Exiting maintenance mode"
CMD="cd /mit/uqlab/web_scripts/transportmaps-website/;"
CMD+="mv .htaccess.bak .htaccess"
ssh athena.dialup.mit.edu "$CMD"
