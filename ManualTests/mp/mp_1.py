import ManualTests.mp.Parallel as p
from multiprocessing.pool import Pool
import multiprocessing

multiprocessing.set_start_method('spawn')

with Pool(5) as pool:
    p.bcast_dmem(pool, dict(a=1, b=2))
    lst_dmem = p.list_dmem(pool)
    pool.close()
    pool.join()

print(lst_dmem)
