import logging
import multiprocessing
import numpy as np
from multiprocessing.pool import Pool
from typing import Iterable, Dict, List, Tuple

logger = logging.getLogger(__name__)
_distributed_memory: dict = {}


def _set_dmem(d: dict):
    global _distributed_memory
    _distributed_memory.update(d)


def _list_dmem(*args):
    global _distributed_memory
    return list(_distributed_memory.keys())


def _get_dmem(lst_keys):
    global _distributed_memory
    return [
        _distributed_memory[k]
        for k in lst_keys
    ]


def bcast_dmem(pool: Pool, d: dict):
    n_workers = len(multiprocessing.active_children())
    pool.map(_set_dmem, [d]*n_workers, chunksize=1)


def list_dmem(pool: Pool):
    n_workers = len(multiprocessing.active_children())
    return pool.map(_list_dmem, [None]*n_workers)


def get_dmem(pool: Pool, lst_keys: Tuple[str]):
    n_workers = len(multiprocessing.active_children())
    return pool.map(_get_dmem, [lst_keys]*n_workers)


def scatter_dmem(
        pool: Pool,
        d: Dict[str, Iterable]
):
    """
    :param pool:
    :param d: dictionary of iterables to be split
        (if multi-dimensional it assumes the first dimension is the one to be split)
    :return:
    """
    n_workers = len(multiprocessing.active_children())
    lst_in = [{} for i in range(n_workers)]
    for k, v in d.items():
        for i, v_split in enumerate(np.array_split(v, n_workers)):
            lst_in[i][k] = v_split
    pool.map(_set_dmem, lst_in, chunksize=1)
