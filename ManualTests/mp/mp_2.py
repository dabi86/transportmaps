import ManualTests.mp.Parallel as p
from multiprocessing.pool import Pool
import multiprocessing
import numpy as np

multiprocessing.set_start_method('spawn')

with Pool(5) as pool:
    p.scatter_dmem(
        pool,
        dict(
            a=np.arange(8),
            b=np.arange(3)
        )
    )
    lst_dmem = p.get_dmem(pool, ('a',))
    pool.close()
    pool.join()

print(lst_dmem)
