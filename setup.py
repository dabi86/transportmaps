#
# This file is part of TransportMaps.
#
# TransportMaps is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# TransportMaps is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TransportMaps.  If not, see <http://www.gnu.org/licenses/>.
#
# Transport Maps Library
# Copyright (C) 2015-2018 Massachusetts Institute of Technology
# Uncertainty Quantification group
# Department of Aeronautics and Astronautics
#
# Author: Transport Map Team
# Website: transportmaps.mit.edu
# Support: transportmaps.mit.edu/qa/
#

from setuptools import setup  # Extension

if __name__ == '__main__':
    # import numpy
    # incdir_numpy = numpy.get_include().strip()

    setup(
        # ext_modules=[
        #     Extension(
        #         'fast_eval',
        #         sources=['src/TransportMaps/Distributions/Examples/Lorenz96/fast_eval.pyx'],
        #         # all sources are compiled into a single binary file
        #         include_dirs=[incdir_numpy],
        #         # define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_7_API_VERSION")]
        #     )
        # ]
    )
